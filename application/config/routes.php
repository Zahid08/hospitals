<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
| https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
| $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
| $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
| $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples: my-controller/index -> my_controller/index
|   my-controller/my-method -> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = true;

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
$route['api/auth/login']['post']                    = 'api/auth/login';
$route['api/auth/login/client']['post']             = 'api/auth/login_client';
$route['api/auth/logout']['post']                   = 'api/auth/logout';

$route['api/hospital']['get']                       = 'api/hospital';
$route['api/hospital/detail/(:num)']['get']         = 'api/hospital/detail/$1';
$route['api/hospital/request']['post']              = 'api/hospital/create_request';
$route['api/hospital/update/(:num)']['put']         = 'api/hospital/update/$id';
$route['api/hospital/password/(:num)']['post']      = 'api/hospital/update_password/$id';
$route['api/hospital/detail/request/(:num)']['get'] = 'api/hospital/detail_request/$1';
$route['api/hospital/total/bill/(:num)']['get']     = 'api/hospital/total_bill/?1';
$route['api/hospital/total/dependent/(:num)']['get']= 'api/hospital/total_client_dependant/$1';
$route['api/hospital/total/authorization/(:num)']['get']= 'api/hospital/total_authorization_hospital/$1';
$route['api/hospital/new/tickets/(:num)']['POST']   = 'api/hospital/create_new_tickets/$1';
$route['api/hospital/update/tickets/(:any)']['PUT'] = 'api/hospital/create_update_tickets/$1';
$route['api/hospital/tickets/(:num)']['GET'] = 'api/hospital/get_tickets/$1';
$route['api/hospital/service']['get']               = 'api/hospital/get_service_hospital';
$route['api/hospital/drug/(:num)']['get']           = 'api/hospital/get_service_drugs_hospital/$1';
$route['api/hospital/enrolle/(:num)']['get'] 		= 'api/hospital/get_enrolle_client_family/$1';
$route['api/hospital/location']['GET'] 				= 'api/hospital/get_location';

$route['api/client']['get']                         = 'api/client';
$route['api/client/detail/(:num)']['get']           = 'api/client/detail/$1';
$route['api/client/familly/(:num)']['get']          = 'api/client/client_familly/$1';
$route['api/client/dependent/(:num)']['get']        = 'api/client/client_dependant/$1';
//$route['client/request']['post']                = 'client/create_request';
$route['api/client/update/dependent/(:num)']['POST'] = 'api/client/update_dependant/$1';
$route['api/client/password/(:num)']['put']         = 'api/client/update_password/$1';
$route['api/client/request/hospital']['post']       = 'api/client/request_change_hospital';
$route['api/client/total/dependent/(:num)']['get']  = 'api/client/total_dependant/$1';
$route['api/client/total/hospital/(:num)']['get']   = 'api/client/total_hospital/$1';
$route['api/client/total/hospital/request/(:num)']['get'] = 'api/client/total_hospital_request/$1';
$route['api/client/total/dependent/request/(:num)']['get']= 'api/client/total_dependant_request/$1';
$route['api/client/new/dependent/(:num)']['POST']   = 'api/client/new_dependant/$1';
$route['api/client/provider/(:num)']['get'] = 'api/client/list_hospital/$1';
$route['api/client/enrolle/(:num)']['get'] = 'api/client/get_enrolle_client_family/$1';
$route['api/client/new/tickets/(:num)']['POST']   = 'api/client/create_new_tickets/$1';
$route['api/client/update/tickets/(:any)']['PUT']   = 'api/client/update_tickets/$1';
$route['api/client/tickets/(:num)']['GET']   = 'api/client/get_tickets/$1';
$route['api/client/subscription/detail/(:num)']['get']   = 'api/client/get_xin_subscription_detail/$1';
$route['api/client/organizations/(:num)']['GET'] = 'api/client/get_xin_organizations/$1';
$route['api/client/update/organizations/(:num)']['PUT'] = 'api/client/update_xin_organization/$1';
$route['api/client/location']['GET'] = 'api/client/get_location';
$route['api/client/update/(:num)']['POST']           = 'api/client/update_client/$1';
