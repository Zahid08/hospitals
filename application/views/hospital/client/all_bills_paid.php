<?php

/* Subscription view

*/
// echo $this->db->last_query();die; 
?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<!-- <?php //$role_resources_ids = $this->Xin_model->user_role_resource(); ?> -->

<!-- <?php //if(in_array('341',$role_resources_ids)) {?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?> -->


<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

  <div class="box-header with-border">
 <h3 class="box-title"> Filter Data by Date <?php //echo $query; ?> </h3>

  </div>

  <div class="box-body">

    <?php echo form_open('hospital/clients/all_bills_paid');?>

    <div class="row">

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">
          

        </div>

      </div>

      <div class="col-md-2">

        <div class="form-group">

          <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

        </div>

      </div>

    </div>

    <?php echo form_close(); ?> </div>
</div>

<?php //} ?>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>

    <div class="box-datatable table-responsive">

        <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

            <thead>

                <tr> 
                    <th width="10%">Bill Status</th>
                    <th width="5%">Progress</th>
                    <th width="15%">Approved by</th>
                    <th width="15%">Authorization Code</th>
                    <th width="20%">Provider</th>
                    <th width="14%">Encounter Date</th>
                    <th width="5%">Bill Amount</th>
                </tr>

                <tbody> 

                  <?php   
                    if(!empty($xin_diagnose_clients))
                    { 
                      // echo $this->db->last_query();
                      // print_r($xin_diagnose_clients);die;
                        $admin_id = $this->session->userdata;
                        $grand_total = 0;

                        foreach ($xin_diagnose_clients as $key => $value)
                        {              
                             
                         
                            $ci=& get_instance();
                            $ci->load->model('Clients_model'); 

                            $services = $ci->Clients_model->read_individual_hospital_diagnose_services($value->diagnose_id);
                            $drugs = $ci->Clients_model->read_individual_hospital_diagnose_drugs($value->diagnose_id);
                            if ($value->diagnose_bill_status == '3') {
                                $admin_info = $ci->Clients_model->read_individual_admin_info($value->diagnose_bill_approve_by);
                  ?>  
                            <tr>
                                <td>
                                    <?php if ($value->diagnose_bill_status == '2'){
                                              if ($value->diagnose_bill_approve_by == $admin_id['user_id']['user_id']){ ?>
                                                  <a onClick="alert('You can not approve bill. Bill Waiting for Head Claims!')"  class="btn btn-info">Bill Needs Second Approval</a> 
                                    <?php
                                              } else {
                                    ?>
                                                <a href="<?php echo base_url(); ?>admin/Hospital/diagnose_hospital_clients_bill_requests?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&second_approve=yes&id=<?php echo $value->diagnose_id; ?>"  class="btn btn-info">Bill Needs Second Approval</a> 

                                    <?php     }
                                          }elseif($value->diagnose_bill_status == '4') { ?>
                                        <p class="btn btn-danger">Bill Rejected</p> 
                                    <?php }elseif($value->diagnose_bill_status == '3') { ?>
                                        <p class="btn btn-success">Bill Approved</p> 
                                        <!-- <p class="btn btn-warning"> Approve Bill</p> --> 
                                    <?php }else { ?>
                                        <a href="<?php echo base_url(); ?>admin/Hospital/diagnose_hospital_clients_bill_requests?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&approve=yes&id=<?php echo $value->diagnose_id; ?>"  class="btn btn-info">Approve</a>

                                        <a href="<?php echo base_url(); ?>admin/Hospital/diagnose_hospital_clients_bill_requests?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&reject=yes&id=<?php echo $value->diagnose_id; ?>" class="btn btn-warning">Reject</a>

                                    <?php } ?>
                                </td>
                                <td>
                                  <div class="progress" style="border: 1px solid #0177bc;">
                                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php if ($value->diagnose_bill_status == '2') echo "50%"; else if ($value->diagnose_bill_status == '3') echo "100%"; else echo "0%"; ?>"><?php if ($value->diagnose_bill_status == '2') echo "50%"; else if ($value->diagnose_bill_status == '3') echo "100%"; else echo "0%"; ?>
                                    </div>
                                  </div>
                                </td>
                                <td>
                                  <?php 
                                      if($admin_info) {
                                          $n = 1;
                                          if(strlen($value->diagnose_bill_approve_by) == 1){ 
                                            echo "&#8226; ".$admin_info->first_name." ".$admin_info->last_name." "; 
                                          } else {
                                            $ids = explode(",", $value->diagnose_bill_approve_by);
                                            foreach ($ids as $id) {
                                              $admin_info = $ci->Clients_model->read_individual_admin_info($id);
                                              if (!empty($admin_info)) {
                                                 echo "&#8226; ".$admin_info->first_name." ".$admin_info->last_name."<br />";}
                                            }
                                          }
                                      } else {
                                        echo "-----"; 
                                      }
                                  ?>      
                                </td>
                                <?php
                                  $hcp = preg_replace('/\s+/', '', $value->hospital_name);
                                  $date = date("Ymd", strtotime($value->diagnose_date_time));
                                ?>
                                <td><?php if(empty($value->diagnose_generated_code)) echo "-----"; else echo "P-".$value->loc_id."-".$date."-".$value->diagnose_generated_code; ?></td>
				<td><?php if(empty($value->hospital_name)) echo "-----"; else echo $value->hospital_name; ?></td>
                                <td><?php echo $value->diagnose_date; ?></td>
                                <td>₦<?php echo number_format($value->diagnose_total_sum).".00"; ?></td>
                            </tr> 

                    <?php 
                            $grand_total = $grand_total + $value->diagnose_total_sum;
                          }
                        }
                    ?>
                            <tr>
                              <td colspan="6" style="text-align: right;"><b>Grand Total:</b> </th>
                              <td><b><?php echo $this->Xin_model->currency_sign($grand_total); ?></b></td>
                            </tr>
                <?php
                    }
                ?>
                </tbody>

            </thead>

        </table>

    </div>

  </div>

</div>
 

<script type="text/javascript">

      function loadModalView(id){
          // alert("ID is: " + id);

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#fetched_data").html(response);
            }
          });

      }

   document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
          buttons: ['csv', 'excel', 'pdf', 'print'],
          "order" : [[4,"desc"]]
        }); 
    }, false);


     
</script>