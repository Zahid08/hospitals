<?php
/* Dependant List view
*/
$xin_diagnose_clients = $xin_diagnose_clients[0];
?>
<?php $session = $this->session->userdata('client_username');?>
<?php $get_animate = $this->Xin_model->get_content_animate();?>
<div class="row <?php echo $get_animate;?>">
  
  
  <!-- fix for small devices only -->
  <div class="clearfix visible-sm-block"></div>
  
  
</div>
<div class="box <?php echo $get_animate;?>">
  
  <div class="current-tab <?php echo $get_animate;?>" aria-expanded="false">
    <div class="box">
      <div class="box-body">
        <div class="card-block">
          <?php
          echo form_open_multipart('hospital/clients/edit_diagnose?eid='.$xin_diagnose_clients->diagnose_id.'&update_diagnose=yes');
          ?>
          <div class="form-body">
            <div class="row">
              <div class="col-md-12">
                <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php endif ?>
                <?php if ($this->session->flashdata('error')): ?>
                <div class="alert alert-warning">
                  <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php endif ?>
              </div>
              <div class="col-md-11" style="width: 98.5%;">
                <div class="form-group">
                  <label for="company_name">Search Client</label>
                  <select class="form-control col-md-6 select2" name="diagnose_client">
                    <option>Select Client</option>
                    <?php
                    if(!empty($clients))
                    {
                    //print_r($clients);
                    foreach ($clients as $key => $value)
                    {
                    $ci=& get_instance();
                    $ci->load->model('Training_model');
                    $dependants = $ci->Training_model->getAll2('xin_clients_family',' client_id ='.$value->client_id.'    ');
                    ?>
                    <option value="<?php echo "LTM-".date("Y",strtotime($value->created_at))."-00".$value->client_id.":".$value->client_id; ?>" <?php if($xin_diagnose_clients->diagnose_user_type == 'C'){ if($value->client_id == $xin_diagnose_clients->diagnose_client_id) echo "selected"; else echo ""; } ?>><?php echo isset($value->name) ? ucfirst($value->name) : '';?> <?php echo isset($value->last_name) ? ucfirst($value->last_name) : ''; ?></option>
                    <?php
                    if(!empty($dependants)) {
                    foreach ($dependants as $key => $dependant)
                    {
                    ?>
                    <option value="<?php echo "LTM-".date("Y",strtotime($dependant->created_on))."-00".$value->client_id."-00".$dependant->clients_family_id.":".$dependant->clients_family_id; ?>" <?php if($xin_diagnose_clients->diagnose_user_type == 'D'){ if($dependant->clients_family_id == $xin_diagnose_clients->diagnose_client_id) echo "selected"; else echo ""; } ?>><?php echo isset($dependant->name) ? ucfirst($dependant->name) : '';?> <?php echo isset($dependant->last_name) ? ucfirst($dependant->last_name) : ''; ?></option>
                    <?php
                    }
                    }
                    }
                    }
                    ?>
                  </select>
                </div>
              </div>
            <!--  <div class="col-md-12 form-group">
                <label for="last_name">Date</label>
                <input class="form-control" value="<?= $_GET['rejection']; ?>" name="diagnose_rejection" type="text" value="">
              </div>
               <div class="col-md-12 form-group">
                <label for="last_name">Date</label>
                <input class="form-control" value="<?= $xin_diagnose_clients->diagnose_date; ?>" name="diagnose_date" type="date" value="">
              </div> -->
              <div class="col-md-6 form-group" style="width: 47.3%;">
                <label for="last_name">Select Services</label>
                <select class="form-control select2" name="diagnose_services[]" data-plugin="select_hrm" data-placeholder="Select Services" multiple>
                  <?php /*if (isset($all_locations) and !empty($all_locations)): ?>
                  <?php foreach ($all_locations as  $value): ?>
                  <?php echo "<option  ".($value->location_id == $state ? "selected" : "")."      value='".$value->location_id."'    >".$value->location_name."</option> " ?>
                  <?php endforeach ?>
                  
                  <?php endif */ ?>
                  <option value="">Select Services</option>
                  <?php
                  $ci=& get_instance();
                  $ci->load->model('Clients_model');
                  $services = $ci->Clients_model->read_individual_hospital_diagnose_services($xin_diagnose_clients->diagnose_id);
                  $drugs = $ci->Clients_model->read_individual_hospital_diagnose_drugs($xin_diagnose_clients->diagnose_id);
                  if (isset($xin_services_hospital) and !empty($xin_services_hospital)) {
                  ?>
                  
                  
                  <?php foreach ($xin_services_hospital as $key => $value): ?>
                  <option value="<?php echo isset($value->id) ? $value->id   : ''; ?> [<?php echo isset($value->service_price) ? $value->service_price : ''; ?>]"
                    <?php
                    if(!empty($services)) {
                    foreach ($services as $ser => $service) {
                    if($value->id == $service->id) echo "selected"; else echo "";
                    }
                    }
                    ?>
                  ><?php echo isset($value->service_name) ? $value->service_name : ''; ?> (<?php echo isset($value->service_price) ? $value->service_price : ''; ?>)</option>
                  <?php
                  endforeach;
                  }
                  ?>
                </select>
              </div>
              <div class="col-md-6 form-group pull-right" style="width: 520px;">
                <label for="last_name">Select Drugs</label>
                <select class="form-control select2" name="diagnose_drugs[]" data-plugin="select_hrm" data-placeholder="Select Drugs" multiple>
                  <?php /*if (isset($all_locations) and !empty($all_locations)): ?>
                  <?php foreach ($all_locations as  $value): ?>
                  <?php echo "<option  ".($value->location_id == $state ? "selected" : "")."      value='".$value->location_id."'    >".$value->location_name."</option> " ?>
                  <?php endforeach ?>
                  
                  <?php endif */ ?>
                  <option value="">Select Drugs</option>
                  <?php if (isset($all_hospital_drugs) and !empty($all_hospital_drugs)) { ?>
                  
                  
                  <?php foreach ($all_hospital_drugs as $key => $value): ?>
                  <option value="<?php echo isset($value->drug_id) ? $value->drug_id : ''; ?> [<?php echo isset($value->drug_price) ? $value->drug_price : ''; ?>]"
                    <?php
                    if(!empty($drugs)) {
                    foreach ($drugs as $dru => $drug) {
                    if($value->drug_id == $drug->drug_id) echo "selected"; else echo "";
                    }
                    }
                    ?>
                  ><?php echo isset($value->drug_name) ? $value->drug_name : ''; ?> (<?php echo isset($value->drug_price) ? $value->drug_price : ''; ?>)</option>
                  <?php
                  endforeach;
                  }
                  ?>
                </select>
              </div>
              <div class="col-md-6 form-group">
                <label for="last_name">Diagnose</label>
                <textarea class="form-control" placeholder="Diagnose" rows="4" name="diagnose_diagnose" type="text"><?= $xin_diagnose_clients->diagnose_diagnose; ?></textarea>
              </div>
              <div class="col-md-6 form-group">
                <label for="last_name">Presenting Complaints</label>
                <textarea class="form-control" placeholder="Presenting Complaints" rows="4" name="diagnose_procedure" type="text"><?= $xin_diagnose_clients->diagnose_procedure; ?></textarea>
              </div>
              <div class="col-md-6 form-group">
                <label for="last_name">Clinical Findings</label>
                <textarea class="form-control" placeholder="Clinical Findings" rows="4" name="diagnose_investigation" type="text"><?= $xin_diagnose_clients->diagnose_investigation; ?></textarea>
              </div>
              <div class="col-md-6 form-group">
                <label for="last_name">Medical Personnel</label>
                <textarea class="form-control" placeholder="Medical Personnel" rows="4" name="diagnose_medical" type="text"><?= $xin_diagnose_clients->diagnose_medical; ?></textarea>
              </div>
            </div>
            <?php if($_GET['rejection'] == "auth") { ?>
            <div class="form-actions"> <?php echo form_button(array('name' => 'hrsale_form', 'type' => 'submit', 'class' => "pull-right ".$this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '."Request For Authorization Code Again")); ?>
            </div>
            <?php } else { ?>
            <div class="form-actions"> <?php echo form_button(array('name' => 'hrsale_form', 'type' => 'submit', 'class' => "pull-right ".$this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '."Send Bill Request Again")); ?>
            </div>
            <?php } ?>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
  <style type="text/css">
  .info-box-number {
  font-size:16px !important;
  font-weight:300 !important;
  }
  </style>
  <script type="text/javascript">
  $('.select2').select2();
  function loadModalView(id){
  // alert("ID is: " + id);
  $.ajax({
  url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id',
  method   : 'post',
  dataType    : 'text',
  data     : {id : id},
  success  : function(response){
    $("#fetched_data").html(response);
  }
  });
  }
  document.addEventListener('DOMContentLoaded', function(){
  var xin_table_new = $('#xin_table_new').dataTable();
  }, false);
  </script>