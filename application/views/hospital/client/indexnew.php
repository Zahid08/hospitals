<?php

/* Dependant List view

*/
?> 

<?php $session = $this->session->userdata('client_username');?>

<?php 
//error_reporting(E_all);
$get_animate = $this->Xin_model->get_content_animate();?>



<div class="row <?php echo $get_animate;?>">



  <!-- fix for small devices only -->

  <div class="clearfix visible-sm-block"></div>




</div>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">
      <a class="btn btn-info pull-right" id="diagnose_client" href="<?php echo site_url('hospital/clients/create');?>"> <i class="fa fa fa-plus"></i>&nbsp;&nbsp; CREATE A ENCOUNTER </a>
  </div>

    <div class="current-tab <?php echo $get_animate;?>" aria-expanded="false">


  <div class="box-body">

    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-bordered" id="xin_table_new">

        <thead>

          <tr>

            <th>Status</th>
            <th>Authorization Code</th>
    <?php
      $chk = 1;
      if (!empty($xin_diagnose_clients)) {
        foreach ($xin_diagnose_clients as $key => $value):
          if((!empty($value->diagnose_reject_reason) || !empty($value->diagnose_bill_reject_reason)) && $chk == 1) {
    ?>
         <!-- <th>Reject Reason</th>-->
    <?php
            $chk = 2;
          }
        endforeach;
      }
    ?>
            <th>Reject Reason</th>
            <th>Enrollee Name</th>
            <th>Date</th>
            <th>Total Bill</th>

          </tr>

        </thead>

        <tbody>
        <?php
          if (!empty($xin_diagnose_clients)) {
          foreach ($xin_diagnose_clients as $key => $value):

            // print_r($xin_diagnose_clients);die;

            $ci=& get_instance();
            $ci->load->model('Clients_model');

            $services = $ci->Clients_model->read_individual_hospital_diagnose_services($value->diagnose_id);
            $drugs = $ci->Clients_model->read_individual_hospital_diagnose_drugs($value->diagnose_id);

        ?>
               <tr>
                  <td>
                    <!--  data-toggle="modal" data-target="#myModal" onclick="return loadModalView(<?php echo $value->diagnose_id; ?>);" -->
                      <a class="btn btn-default" data-toggle="modal" data-target="#myModal" onclick="return loadModalView(<?php echo $value->diagnose_id; ?>);"><i class="fa fa-eye"></i></a>
                      <?php if (($value->diagnose_status == '2') && ($value->diagnose_bill_status == '')){ ?>
                          <p class="btn btn-success">Authorized</p>
                          <a href="#"  class="btn btn-info" onclick="return getmodel(<?php echo $value->diagnose_id; ?>,<?php echo $value->diagnose_client_id; ?>,<?php echo $value->diagnose_hospital_id; ?>);">Send Bill</a>
                      <?php }elseif($value->diagnose_bill_status == '3') { ?>
                          <p class="btn btn-success">Authorized</p>
                          <p class="btn btn-success"> Bill Approved</p>
                      <?php }elseif($value->diagnose_status == '3') { ?>
                          <a href="<?php echo base_url(); ?>hospital/clients/edit_diagnose?id=<?php echo $value->diagnose_id; ?>&rejection=auth" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                          <p class="btn btn-danger"> Rejected</p>
                      <?php }elseif($value->diagnose_bill_status == '4') { ?>
                          <a href="<?php echo base_url(); ?>hospital/clients/edit_diagnose?id=<?php echo $value->diagnose_id; ?>&rejection=bill" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                          <p class="btn btn-success">Authorized</p>
                          <p class="btn btn-danger"> Bill Rejected</p>
                      <?php }elseif(($value->diagnose_status == '2') && (($value->diagnose_bill_status == '1') || ($value->diagnose_bill_status == '2'))) { ?>
                          <p class="btn btn-success">Authorized</p>
						  <?php
						  if($value->send_admission_status==2){
						  ?>
                          <p class="btn btn-warning"> Admission Case</p>
						  <?php }else{
							  ?>
							  <p class="btn btn-warning"> Bill Requested</p>
							  <?php
						  }?>
                      <?php }else { ?>
                          <p class="btn btn-warning"> Requested For Code</p>

                      <?php } ?>
                  </td>
                  <?php
                    $hcp = preg_replace('/\s+/', '', $value->hospital_name);
                    $date = date("Ymd", strtotime($value->diagnose_date_time));
                  ?>
                  <td><?php if(empty($value->diagnose_generated_code)) echo "-----"; else echo "P-".$value->loc_id."-".$date."-".$value->diagnose_generated_code; ?></td>
                  <td>
                  <?php
                    if($chk == 2) { if(empty($value->diagnose_reject_reason) && empty($value->diagnose_bill_reject_reason)) { echo "-----"; } else { if(!empty($value->diagnose_reject_reason)) echo '<a class="btn btn-default" data-toggle="modal" data-target="#viewReasonModal" onclick="return loadReasonModalView('.$value->diagnose_id.',1)">Auth Reason</a>'; if(!empty($value->diagnose_bill_reject_reason)) echo '<a class="btn btn-default" data-toggle="modal" data-target="#viewReasonModal" onclick="return loadReasonModalView('.$value->diagnose_id.',2)">Bill Reason</a>'; } }
                  ?>
                  </td>
                  <!-- <td><?php if(empty($value->diagnose_transaction_id)) echo "-----"; else echo $value->diagnose_transaction_id; ?></td> -->
            <?php
                  if ($value->is_capitation != '1') {

                    if($value->diagnose_user_type == 'C') {
            ?>
                  <td><?php echo $value->cname." ".$value->clname." ".$value->coname; ?></td>
            <?php
                    } else {

	  if ($value->cname != '' && $value->clname != '') { ?>
                        <td><?php echo $value->cname." ".$value->clname." ".$value->coname; ?></td>

                    <?php  } else{ ?>
                        <td><?php echo $value->dname." ".$value->dlname." ".$value->doname; ?></td>

                      <?php }?>

            <?php
                    }
                  }else{
                    $capitation = $ci->Clients_model->get_capitation_info($value->diagnose_client_id)->result();
            ?>
                    <td><?php echo $capitation[0]->name; ?></td>
            <?php
                  }
            ?>
                  <td><?php echo $value->diagnose_date; ?></td>
                  <td>₦<?php echo number_format($value->diagnose_total_sum).".00"; ?></td>
               </tr>
          <?php endforeach;

          } ?>
        </tbody>

      </table>
    </div>

  </div>

</div>

<style type="text/css">

.info-box-number {

    font-size:16px !important;

    font-weight:300 !important;

}

</style>

<script type="text/javascript">
  
  $('.select2').select2();

  var button_submit = document.getElementById("button_submit");

  function change(){
	 
    var x = document.getElementById("client").value;
    var y = x.split(':');

    if (y.length < 3) {
      console.log('Length <3');
      window.capitation = 2;
      document.getElementById("date").value = "";
      document.getElementById("correct").style.display = 'none';
      document.getElementById("wrong").style.display = 'none';
      $.ajax({
        url      : '<?php echo base_url(); ?>hospital/Clients/get_dob',
        method   : 'post',   
        dataType    : 'text',      
        data     : {id : x},
        success  : function(response){
          // $("#id_we_get_is").text(response);
          // alert(response);
          // console.log(response);
          // console.log(response.substr(0,4));
          // console.log(x)
          window.dob = response.substr(0,4);
          // $("#fetched_data").html(response);
        }
      });

      var cid = $("#client").val();

     // console.log(cid);

      $.ajax({
            url      : '<?php echo base_url(); ?>admin/Hospital/fetch_client_plan',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : cid},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#plan_name").text(response);
              //console.log(response);
              
            }
          });    
    }else{
      document.getElementById("date").value = "";
      document.getElementById("correct").style.display = 'none';
      document.getElementById("wrong").style.display = 'none';
      //console.log('Length >3');
      window.capitation = 1;
      $.ajax({
            url      : '<?php echo base_url(); ?>admin/Hospital/fetch_client_plan',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : cid},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#plan_name").text(response);
             // console.log(response);
              
            }
          });    
    }
   // console.log(capitation);
  }

  function dateChange(){
	  //alert('1');
	  
    if (capitation == 2) {
      var date = document.getElementById("date").value;
	 // alert(dob);
	  
      //console.log(date);
      date = date.substr(0,4)
	  //alert(date);
      //console.log(date);
      var wrong = document.getElementById("wrong");
      var correct = document.getElementById("correct");
	//console.log('*'+dob+'*');
	if(dob != '0000')
	{
	
      if (dob == date) {
        button_submit.disabled = false;
        console.log('same');
        correct.style.display = 'inline';
        wrong.style.display = 'none';
      }else{
        button_submit.disabled = true;
        correct.style.display = 'none';
        wrong.style.display = 'inline';
        console.log('different');
      }
	}
    }

    
    // alert("dob: "+dob);   
    // alert("date: "+date);
  }

      function loadModalView(id){
          // alert("ID is: " + id);

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id,hospital: true},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#fetched_data").html(response);
            }
          });

      }

      function loadReasonModalView(id,type) {
          // alert(id+ " " + type);

          if(type == 1) { $("#rtitle").text("Authorization Code Reject Reason") }
          if(type == 2) { $("#rtitle").text("Bill Reject Reason") }

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_reason',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id, type : type},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#fetched_reason").html(response);
            }
          });
      }
      


  document.addEventListener('DOMContentLoaded', function(){ 
      var xin_table_new = $('#xin_table_new').dataTable({
        "order": [[ 4, "desc" ]]
    });  

     // console.log($('#client').val());
      
      $("#client").change(function(){
      
      
      // alert(hid);
      //console.log(cid);
        
  });
  }, false);

function getmultiplevalue()
{
	var drugs = $('#drugs').val();
	var quantity = $('#quantity').val();
	 $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/multipledrugsfetch',
            method   : 'post',   
            dataType    : 'text',      
            data     : {drugs : drugs, quantity : quantity},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#mulitplevaluedrugs").val('₦'+response);
			 
            }
          });
}
function getmodel(value,value1,value2){
	//alert('asdf');
	$('#myModal2').show();
	
	$('#servicesmodel').select2();
	$('#drugsmore1').select2(); 
	$('#hostel_idss').val(value);
	$('#successprocess').attr('href', "<?php echo base_url(); ?>hospital/clients/index?client_id="+value1+"&hospital_id="+value2+"&bill_request=yes&id="+value);
	$('#updatesrecords').attr('href', "<?php echo base_url(); ?>hospital/clients/index?client_id="+value1+"&hospital_id="+value2+"&bill_request=yes&id="+value);
	$('#updatesrecords1').attr('href', "<?php echo base_url(); ?>hospital/clients/index?client_id="+value1+"&hospital_id="+value2+"&bill_request=yes&id="+value);
	$('#hostel_idsshref').val("<?php echo base_url(); ?>hospital/clients/index?client_id="+value1+"&hospital_id="+value2+"&bill_request=yes&id="+value);
	$('#previewbtn').attr('onclick','return loadModalView1('+value+');');
	
}
function closed() 
{
	$('#myModal2').hide();
}
function closed1()
{
	$('#myModal3').hide();
}
function adddrungsreason()
{
	var that_drugreason = $('#that_drugreason').val();
	var drug_did = $('#drug_did').val();
	var drug_hid = $('#drug_hid').val();
	var drug_cid = $('#drug_cid').val();
	var drug_mid = $('#drug_mid').val();
	
	$.ajax({
		url      : '<?php echo base_url(); ?>hospital/clients/drug_notes',
		method   : 'post',   
		dataType    : 'text',       
		data     : {that_drugreason : that_drugreason,drug_did: drug_did,drug_hid: drug_hid,drug_cid: drug_cid,drug_mid: drug_mid},
		success  : function(response){
			$('#totalbill').text(response);
		 $('#closed2').trigger('click');
		}
	  });
}
function addservicereason()
{
	var that_service_reason = $('#that_service_reason').val();
	var service_did = $('#service_did').val();
	var service_hid = $('#service_hid').val();
	var service_mid = $('#service_mid').val();
	var service_sid = $('#service_sid').val();
	
	$.ajax({
		url      : '<?php echo base_url();?>hospital/clients/service_notes',
		method   : 'post',   
		dataType    : 'text',       
		data     : {that_service_reason : that_service_reason,service_did: service_did,service_hid: service_hid,service_mid: service_mid,service_sid: service_sid},
		success  : function(response)
		{
			$('#totalbill').text(response);
			$('#closed1').trigger('click');
		}
	  });
}
function loadModalView1(id){
          // alert("ID is: " + id);

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id_1',
            method   : 'post',   
            dataType    : 'text',       
            data     : {id : id,hospital: true},
            success  : function(response){
              $("#previews").html(response);
			  $('#myModal3').show();
            }
          });

      }
	  function loadDrugReasonModal(drug_did,drug_hid,drug_cid,drug_mid){
			//alert(drug_did);
			$("#drug_reasonModal").on("show.bs.modal", function(e) {
			$("#drug_did").val(drug_did);
            $("#drug_hid").val(drug_hid);
            $("#drug_cid").val(drug_cid);
            $("#drug_mid").val(drug_mid);
            $("#redirectflage").val('2');
			$("#dugus"+drug_cid).hide();
	  });
         // $("#that_drugreason").val("");
          

          

      }
      
      function loadServiceReasonModal(service_did,service_hid,service_mid,service_sid){
	  //alert(service_mid);
          //$("#that_service_reason").val("");
	  $("#service_reasonModal").on("show.bs.modal", function(e) {
          $("#service_did").val(service_did);
          $("#service_hid").val(service_hid);
          $("#service_mid").val(service_mid);
          $("#service_sid").val(service_sid);
		  
          $("#service_flage").val('2');
		  $("#service"+service_did).hide();
	  });         

      }
	  
function adddrugsservice()
{
	var str = $("#frms").serialize();
	var services = $('#services').val();
	var drugsmore = $('#drugsmore').val();
	
	var hostel_idss = $('#hostel_idss').val();
	//alert(hostel_idss);
	 $.ajax({
		url      : '<?php echo base_url(); ?>hospital/Clients/addmorederviceanddrugs',
		method   : 'post',   
		dataType    : 'text',      
		data     : {strs : str},
		success  : function(response){
			if(response =='sucess'){
				closed();
				loadModalView1(hostel_idss);
				
				//$('#currentredirect').trigger('click');
			}
		}
	  });
} 
function adddrugsservice1()
{
	var str = $("#frms").serialize();
	var services = $('#services').val();
	var drugsmore = $('#drugsmore').val();
	var days = $('#days').val();
	
	var hostel_idss = $('#hostel_idss').val();
	//alert(hostel_idss);
	 $.ajax({
		url      : '<?php echo base_url();?>hospital/Clients/addmorederviceanddrugs1',
		method   : 'post',   
		dataType    : 'text',      
		data     : {strs : str},
		success  : function(response){
			if(response =='sucess'){
				closed();
				loadModalView1(hostel_idss);
				
				//$('#currentredirect').trigger('click');
			}
		}
	  });
} 
function showservicedrugs(){
	$('#showservicedrugs').show();
	$('#sendadmissionflage').val('1');
	$('#updatebtn').attr('onclick','return adddrugsservice();');
	$('#dayscount').hide();
} 
</script>

<style>
.modal-lg {
    width: 85% !important;
}
th.bg-dark.text-center.asd{
	width:50%;
}
.model{
	
	overflow:unset !important;
	//overflow-x: scroll !important;
}
#myModal2 span.select2.select2-container.select2-container--default.select2-container--focus.select2-container--above.select2-container--open, #myModal2 span.select2.select2-container.select2-container--default.select2-container--focus.select2-container--above,#myModal2 .select2.select2-container {width:100% !important;}
</style>
<script>
function getaddmore(value)
{
	var value1 = value -1;
	var value2 = value + 1;
	
	$.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/addmoredive',
            method   : 'post',    
            dataType    : 'text',      
            data     : {value : value},
            success  : function(response){
              //console.log(response);
			  $("#addmore").attr("onclick","return getaddmore("+value2+")");
			   
              $("#div"+value1).after(response);
			  $('#drugs'+value).select2(); 
            } 
          });
	 
}
function getaddmoremore(value)
{
	var value1 = value -1;
	var value2 = value + 1;
	 $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/addmoredivemore',
            method   : 'post',   
            dataType    : 'text',      
            data     : {value : value},
            success  : function(response){
              console.log(response);
			  $("#addmoremore").attr("onclick","return getaddmoremore("+value2+")");
			   //$('.select2').select2();
              $("#divmore"+value1).after(response);
			  $('#drugsmore'+value).select2(); 
            }
          });
}
function removediv(values){
	$('#div'+values).hide();
}
function removedivmore(values){
	$('#divmore'+values).hide();
}
$(document).ready(function() {
    $('#example').DataTable( {
        "order": [[ 4, "desc" ]]
    } );
} );
function sendadmissionrequest()
{
	$('#showservicedrugs').show();
	$('#sendadmissionflage').val('2');
	$('#updatebtn').attr('onclick','return adddrugsservice1();');
	$('#dayscount').show();
}
</script>
<style>
label.padding {
    padding-top: 40px;
}
#myModal2 .modal-content {
    max-height: 500px;
    overflow-x: scroll;
}
</style>
