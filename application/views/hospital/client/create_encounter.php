<div  class="box">
    <div class="box-header with-border">
        <div class="row">
            <div class="col-md-12 text-center">
                <img id="profile-img" src="<?php echo base_url().'uploads/profile/noimage.jpg'; ?>" alt="img" style="width: 140px;height: 105px;border-radius: 40%;display: none;">
            </div>
        </div>

    </div>
    <div class="box-body">

        <div class="card-block">
            <?php echo form_open_multipart('hospital/clients/insert_clients_diagnose');?>
            <div class="form-body">
                <input type="hidden" id="dateTime" name="datetime" >
                <div class="row">
                    <div class="col-md-12">
                        <?php if ($this->session->flashdata('success')): ?>
                            <div class="alert alert-success">
                                <?php echo $this->session->flashdata('success'); ?>
                            </div>
                        <?php endif ?>


                        <?php if ($this->session->flashdata('error')): ?>
                            <div class="alert alert-warning">
                                <?php echo $this->session->flashdata('error'); ?>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="col-md-11" style="width: 83.3%;">
                        <div class="form-group">

                            <label for="company_name">Search and Select Enrollee for Encounter</label>
                            <select class="form-control col-md-6 select2 client" id="client" name="diagnose_client" onchange="change()">
                                <option>Click Here to Select Enrollee</option>
                                <?php
                                $query = "";
                                $hospital = $this->session->userdata;

                                if(!empty($clients))
                                {

                                    //print_r($clients);
                                    foreach ($clients as $key => $value)
                                    {
                                        $ci=& get_instance();
                                        $ci->load->model('Training_model');
                                        $ci->load->model('Clients_model');


                                        if (!empty($value->org_status)) {
                                            $active = $value->org_status;
                                            if ($active != 1) {
                                                continue;
                                            }
                                        }

                                       $dependants = $ci->Training_model->getAll2('xin_clients_family',' client_id ='.$value->xin_c_id);
//                                        $query = $this->db->last_query();
                                            ?>
                                            <option value="<?php echo "PHC/".date("Y",strtotime($value->c_created_at))."/00".$value->xin_c_id.":".$value->xin_c_id; ?>"><?php echo isset($value->c_name) ? ucfirst($value->c_name) : '';?> <?php echo isset($value->c_last_name) ? ucfirst($value->c_last_name) : ''; ?> (<?php echo isset($value->xin_c_id) ? ucfirst("PHC/".date("Y",strtotime($value->c_created_at))."/00".$value->xin_c_id) : '';?>) (Principal) <?php echo "(" .$value->oranization_name. ") (".$value->subscription_plan.")";?></option>
                                            <?php

                                        if(!empty($dependants)) {
                                            foreach ($dependants as $key => $dependant)
                                            {
                                                ?>
                                                <option value="<?php echo "PHC/".date("Y",strtotime($dependant->created_on))."/00".$value->xin_c_id."/".$dependant->clients_family_id.":".$dependant->clients_family_id; ?>"><?php echo isset($dependant->name) ? ucfirst($dependant->name) : '';?> <?php echo isset($dependant->last_name) ? ucfirst($dependant->last_name) : ''; ?> (<?php echo isset($dependant->clients_family_id) ? ucfirst("PHC/".date("Y",strtotime($dependant->created_on))."/00".$value->xin_c_id."/".$dependant->clients_family_id).")" ." (". (strtolower($dependant->relation)=='wife'?"Spouse":ucfirst($dependant->relation)).") (" .$value->oranization_name. ") (".$value->subscription_plan.")": ''; ?></option>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>

                                <!-- -------------------------------------------------------- -->
                                <?php
                                // print_r($capitation);die;

                                if(!empty($capitation))
                                {
                                    foreach ($capitation as $key => $value)
                                    {
                                        $ci=& get_instance();
                                        $ci->load->model('Clients_model');


                                        if ($value->type == 'p') {
                                            ?>
                                            <option value="<?php echo "PHC/".$value->id.":".$value->id.":1"; ?>"><?php echo isset($value->name) ? ucfirst($value->name) : '';?> (<?php echo isset($value->capitation_id) ? ucfirst("PHC/".$value->capitation_id) : '';?>) (Principal-NHIS)</option>
                                            <?php
                                        }else{
                                            $principal = $ci->Clients_model->get_principal_id($value->capitation_id)->result();

                                            if (!empty($principal)) {
                                                ?>
                                                <option value="<?php echo "PHC/".$principal[0]->id."/00".$value->id.":".$value->id.":1"; ?>"><?php echo isset($value->name) ? ucfirst($value->name) : '';?> (<?php echo isset($value->id) ? ucfirst("PHC/".$principal[0]->capitation_id).")" ." (". (strtolower($value->relation)=='wife'?"Spouse":ucfirst($value->relation))."-NHIS) ": ''; ?></option>
                                            <?php       }
                                        }
                                    }
                                }

                                ?>



                            </select>
                            <span id="plan_name" class="text-primary"></span>
                        </div>
                    </div>

                    <!-- <div class="col-md-6"> -->

                    <!--                                 <div class="form-group">

                                                      <label for="company_name">Enrollee ID</label>
                                                      <input class="form-control" name="diagnose_client_id" placeholder="Client ID"   type="text" value="">

                                                    </div> -->
                    <!-- </div> -->
                     <div class="col-md-2 form-group text-center">

                        <label for="last_name">Date of Birth</label>

                        <input style="line-height: 1.42857143 !important;" class="form-control dob" name="diagnose_date_of_birth" type="date" id="date" readonly>
                        <span class="text-red" id="wrong" style="display: none;">We have different date on our database, please check again.</span>
                        <span class="text-green" id="correct" style="display: none;">Correct</span>

                    </div>
                   
                     <div class="col-md-6 form-group pull-left" style="width: 520px;">
                        <div class="row" id="ser_div1">
                            <div class="col-md-8">

                                <label for="last_name">Select Services | Procedure | Investigation</label>
                                <select class="form-control select2" name="diagnose_services[]" data-plugin="select_hrm" data-placeholder="Select Services">
                                    <?php /*if (isset($all_locations) and !empty($all_locations)): ?>
                                                <?php foreach ($all_locations as  $value): ?>

                                                    <?php echo "<option  ".($value->location_id == $state ? "selected" : "")."      value='".$value->location_id."'    >".$value->location_name."</option> " ?>
                                                <?php endforeach ?>

                                            <?php endif */ ?>
                                    <option value="">Select Services</option>

                                    <?php if (isset($xin_services_hospital) and !empty($xin_services_hospital)) { ?>


                                        <?php foreach ($xin_services_hospital as $key => $value): ?>
                                            <option value="<?php echo isset($value->id  ) ? $value->id   : ''; ?> [<?php echo isset($value->service_price) ? $value->service_price : ''; ?>]"><?php echo isset($value->service_name) ? $value->service_name : ''; ?> (₦<?php echo isset($value->service_price) ? $value->service_price : ''; ?>)</option>
                                            <?php
                                        endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                             <div class="col-md-2">
                                <label for="last_name">Quantity</label>
                                <input type="number" name="service_quantity[]" id="quantity1"  class="form-control" min="1" value="1">
                            </div>
                            <div class="col-md-2">
                                <label for="last_name" class="padding"></label>
                                <input type="button" name="addmore" id="addmore_ser" onclick="return getaddmoreService(2)" value="+" class="btn btn-success">
                            </div>   

                        </div>
                    </div>
                           
                    <div class="col-md-6 form-group pull-right" style="width: 520px;">
                        <div class="row" id="div1">
                            <div class="col-md-8">
                                <label for="last_name">Select Drug</label>
                                <select class="form-control select2" name="diagnose_drugs[]" data-plugin="select_hrm" data-placeholder="Select Drugs" id="drugs1">

                                    <option value="">Select Drug</option>
                                    <?php
                                    if (isset($all_hospital_drugs) and !empty($all_hospital_drugs)) {


                                        foreach ($all_hospital_drugs as $key => $value):


                                            $share1 = isset($value->drug_name) ? $value->drug_name : "";
                                            $share1 .= '(₦'.isset($value->drug_price) ? $value->drug_price : "".')';

                                            ?><option value="<?php echo $value->drug_id.'['.$value->drug_price.']';?>"><?php echo $value->drug_name.'(₦'.$value->drug_price.')';?></option>';
                                            <?php
                                        endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="last_name">Quantity</label>
                                <input type="number" name="quantity[]" id="quantity1"  class="form-control" min="1" value="1">
                            </div>
                            <div class="col-md-2">
                                <label for="last_name" class="padding"></label>
                                <input type="button" name="addmore" id="addmore" onclick="return getaddmore(2)" value="+" class="btn btn-success">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 form-group" style="width: 50%;">
                      <div class="row">
                        <div id="div21"></div>
                          <div class="col-md-12 mt-3">
                            <input type="button" id="addmore" onclick="addServiceDiv()" value="Add New Services | Procedure | Investigation +" class="btn btn-success form-control">  
                          </div>
                        </div>
                    </div>
                    <div class="col-md-6 form-group pull-right" style="width: 520px;">
                        <div class="row" id="div1">
                            <div id="div22"></div>
                            <div class="col-md-12 mt-3">
                                <input type="button" id="addmore" value="Add New Drug +" onclick="addmoreDrugs()" class="btn btn-success form-control">
                            </div>
                        </div>
                    </div>
                    <!--<div class="col-md-6 form-group">
                       <label for="quantity">Drug Quantity</label>
                       <input class="form-control" placeholder="Quantity" rows="5" name="quantity" id="quantity" type="number" value="1" oninput="return getmultiplevalue();">
                   </div>
                   <div class="col-md-6 form-group">
                       <label for="mulitplevaluedrugs">Multiple Drugs Value</label>
                       <input class="form-control" placeholder="Multiple Drugs Value" rows="5" name="mulitplevaluedrugs" id="mulitplevaluedrugs" type="text" value="0" readonly>
                   </div>-->
                    <div class="col-md-6 form-group">
                        <label for="last_name">Presenting Complaints</label>
                        <textarea class="form-control" placeholder="Enter Presenting Complaints" rows="3" name="diagnose_procedure" type="text"></textarea>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="last_name">Clinical Findings</label>
                        <textarea class="form-control" placeholder="Enter Clinical Findings" rows="3" name="diagnose_investigation" type="text"></textarea>
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="last_name">Diagnosis</label>
                        <textarea class="form-control" placeholder="Diagnose" rows="3" name="diagnose_diagnose" type="text"></textarea>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="last_name">Medical Personnel</label>
                        <textarea class="form-control" placeholder="Medical Personnel" rows="3" name="diagnose_medical" type="text"></textarea>
                    </div>

                </div>

                <?php if (empty($hospital_id)): ?>
                    <div class="form-actions"> <?php echo form_button(array('name' => 'hrsale_form','value'=>'1','id' => 'button_submit', 'type' => 'submit', 'class' => "pull-right ".$this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '."Request For Authorization Code")); ?>
                    </div>
                    <input type="hidden" name="hrsale_form_primary_case" value="finalMYttest">
                    <div class="form-actions"> <?php echo form_button(array('name' => 'hrsale_form','value'=>'2','id' => 'button_submit2', 'type' => 'submit', 'style' => 'margin-right:20px', 'class' => "pull-right ".$this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '."Request For Primary Case")); ?>
                    </div>
                <?php endif ?>

            </div>
            <?php echo form_close(); ?>

        </div>
    </div>
</div>
<style type="text/css">

    .info-box-number {

        font-size:16px !important;

        font-weight:300 !important;

    }

</style>
<style>
    .modal-lg {
        width: 85% !important;
    }
    th.bg-dark.text-center.asd{
        width:50%;
    }
    .model{

        overflow:unset !important;
    //overflow-x: scroll !important;
    }
    #myModal2 span.select2.select2-container.select2-container--default.select2-container--focus.select2-container--above.select2-container--open, #myModal2 span.select2.select2-container.select2-container--default.select2-container--focus.select2-container--above,#myModal2 .select2.select2-container {width:100% !important;}
</style>
<style>
    label.padding {
        padding-top: 40px;
    }
    #myModal2 .modal-content {
        max-height: 500px;
        overflow-x: scroll;
    }
</style>
<script type="text/javascript">
 window.onload = function(){
    var dateTime = document.getElementById('dateTime');
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    dateTime.value = date+' '+time;
  }
  var counter = 0;
  var counter2 = 0;
  function addServiceDiv(){
    var div2 = document.getElementById('div21');
    var serv = '<div id="q'+counter2+'"><div class="col-md-7">'+
                            '<label for="last_name">Write Services | Procedure | Investigation</label>'+
                            '<input type="text" name="s_services[]" class="form-control">'+      
                          '</div>'+
                          '<div class="col-md-3">'+
                            '<label for="last_name">Price</label>'+
                            '<input type="text" name="s_price1[]" class="form-control">'+
                          '</div>'+
                          '<div class="col-md-3">'+
                            '<label for="last_name">Quantity</label>'+
                            '<input type="text" name="s_quantity[]" class="form-control">'+
                          '</div>'+
                          '<div class="col-md-2" style="text-align: right;">'+
                            '<label for="last_name" class="padding "></label>'+
                            '<input type="button" name="addmore" id="removemore2" style="text-align:right:"  onclick=removeDrug("q'+counter2+'") value="-" class="btn btn-danger">'+
                          '</div></div>';
    $("#div21").append(serv);                          
counter2++;
  }
  function addmoreDrugs(){
    var div1 = document.getElementById('div22');
   var drug = ' <div id="'+counter+'"><div class="col-md-6">'+
                                '<label for="last_name">Drug Name</label>'+
                                '<input type="text" class="form-control" name="d_diagnose_drugs[]">'+
                            '</div>'+
                            '<div class="col-md-2">'+
                                '<label for="last_name">Quantity</label>'+
                                '<input type="number" name="d_quantity[]" id="quantity1"  class="form-control" min="1" value="1">'+
                            '</div>'+
                            '<div class="col-md-2">'+
                                '<label for="last_name">Price</label>'+
                                '<input type="text" name="d_price[]" id="quantity1"  class="form-control" min="1" value="1">'+
                            '</div>'+
                            '<div class="col-md-2">'+
                                '<label for="last_name" class="padding"></label>'+
                                '<input type="button" name="addmore" id="removemore2" style="text-align:right:" onclick="removeDrug('+counter+')" value="-" class="btn btn-danger">'+
                            '</div></div>';
     $("#div22").append(drug);
    counter++;
  }

  function removeDrug(id){
    var rem = document.getElementById(id);
    rem.remove();
  }
    $('.select2').select2();

    var button_submit = document.getElementById("button_submit");

    function change(){

        var x = document.getElementById("client").value;
        var y = x.split(':');

        if (y.length < 3) {
            console.log('Length <3');
            window.capitation = 2;
            document.getElementById("date").value = "";
            document.getElementById("correct").style.display = 'none';
            document.getElementById("wrong").style.display = 'none';
            $.ajax({
                url      : '<?php echo base_url(); ?>hospital/Clients/get_dob',
                method   : 'post',
                dataType    : 'text',
                data     : {id : x},
                success  : function(response){
                    var object=JSON.parse(response);
                    $("#date").val(object.dob);
                    if(object.img==null){
                        $("#profile-img").show();
                    }
                    else{
                        var img=object.img;
                        $("#profile-img").attr('src',img);
                        $("#profile-img").show();
                    }
                    // $("#id_we_get_is").text(response);
                    // alert(response);
                    // console.log(response);
                    // console.log(response.substr(0,4));
                    // console.log(x)
                    window.dob = response.substr(0,4);
                    // $("#fetched_data").html(response);
                }
            });

            var cid = $("#client").val();

            // console.log(cid);

            $.ajax({
                url      : '<?php echo base_url(); ?>admin/Hospital/fetch_client_plan',
                method   : 'post',
                dataType    : 'text',
                data     : {id : cid},
                success  : function(response){
                    // $("#id_we_get_is").text(response);
                    // alert(response);
                    $("#plan_name").text(response);
                    //console.log(response);

                }
            });
        }else{
            document.getElementById("date").value = "";
            document.getElementById("correct").style.display = 'none';
            document.getElementById("wrong").style.display = 'none';
            //console.log('Length >3');
            window.capitation = 1;
            $.ajax({
                url      : '<?php echo base_url(); ?>admin/Hospital/fetch_client_plan',
                method   : 'post',
                dataType    : 'text',
                data     : {id : cid},
                success  : function(response){
                    // $("#id_we_get_is").text(response);
                    // alert(response);
                    $("#plan_name").text(response);
                    // console.log(response);

                }
            });
        }
        // console.log(capitation);
    }

    function dateChange(){
        //alert('1');

        if (capitation == 2) {
            var date = document.getElementById("date").value;
            // alert(dob);

            //console.log(date);
            date = date.substr(0,4)
            //alert(date);
            //console.log(date);
            var wrong = document.getElementById("wrong");
            var correct = document.getElementById("correct");
            //console.log('*'+dob+'*');
            if(dob != '0000')
            {

                if (dob == date) {
                    button_submit.disabled = false;
                    console.log('same');
                    correct.style.display = 'inline';
                    wrong.style.display = 'none';
                }else{
                    button_submit.disabled = true;
                    correct.style.display = 'none';
                    wrong.style.display = 'inline';
                    console.log('different');
                }
            }
        }


        // alert("dob: "+dob);
        // alert("date: "+date);
    }

    function loadModalView(id){
        // alert("ID is: " + id);

        $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id',
            method   : 'post',
            dataType    : 'text',
            data     : {id : id,hospital: true},
            success  : function(response){
                // $("#id_we_get_is").text(response);
                // alert(response);
                $("#fetched_data").html(response);
            }
        });

    }

    function loadReasonModalView(id,type) {
        // alert(id+ " " + type);

        if(type == 1) { $("#rtitle").text("Authorization Code Reject Reason") }
        if(type == 2) { $("#rtitle").text("Bill Reject Reason") }

        $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_reason',
            method   : 'post',
            dataType    : 'text',
            data     : {id : id, type : type},
            success  : function(response){
                // $("#id_we_get_is").text(response);
                // alert(response);
                $("#fetched_reason").html(response);
            }
        });
    }



    document.addEventListener('DOMContentLoaded', function(){
        var xin_table_new = $('#xin_table_new').dataTable({
            "order": [[ 4, "desc" ]]
        });

        // console.log($('#client').val());

        $("#client").change(function(){


            // alert(hid);
            //console.log(cid);

        });
    }, false);

    function getmultiplevalue()
    {
        var drugs = $('#drugs').val();
        var quantity = $('#quantity').val();
        $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/multipledrugsfetch',
            method   : 'post',
            dataType    : 'text',
            data     : {drugs : drugs, quantity : quantity},
            success  : function(response){
                // $("#id_we_get_is").text(response);
                // alert(response);
                $("#mulitplevaluedrugs").val('₦'+response);

            }
        });
    }
    function getmodel(value,value1,value2){
        //alert('asdf');
        $('#myModal2').show();

        $('#servicesmodel').select2();
        $('#drugsmore1').select2();
        $('#hostel_idss').val(value);
        $('#successprocess').attr('href', "<?php echo base_url(); ?>hospital/clients/index?client_id="+value1+"&hospital_id="+value2+"&bill_request=yes&id="+value);
        $('#updatesrecords').attr('href', "<?php echo base_url(); ?>hospital/clients/index?client_id="+value1+"&hospital_id="+value2+"&bill_request=yes&id="+value);
        $('#hostel_idsshref').val("<?php echo base_url(); ?>hospital/clients/index?client_id="+value1+"&hospital_id="+value2+"&bill_request=yes&id="+value);
    }
    function closed()
    {
        $('#myModal2').hide();
    }
    function closed1()
    {
        $('#myModal3').hide();
    }
    function loadModalView1(id){
        // alert("ID is: " + id);

        $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id_1',
            method   : 'post',
            dataType    : 'text',
            data     : {id : id,hospital: true},
            success  : function(response){
                $("#previews").html(response);
                $('#myModal3').show();
            }
        });

    }
    function adddrugsservice()
    {
        var str = $("#frms").serialize();
        var services = $('#services').val();
        var drugsmore = $('#drugsmore').val();

        var hostel_idss = $('#hostel_idss').val();
        //alert(hostel_idss);
        $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/addmorederviceanddrugs',
            method   : 'post',
            dataType    : 'text',
            data     : {strs : str},
            success  : function(response){
                if(response =='sucess'){
                    closed();
                    loadModalView1(hostel_idss);

                    //$('#currentredirect').trigger('click');
                }
            }
        });
    }
    function adddrugsservice1()
    {
        var str = $("#frms").serialize();
        var services = $('#services').val();
        var drugsmore = $('#drugsmore').val();
        var days = $('#days').val();

        var hostel_idss = $('#hostel_idss').val();
        //alert(hostel_idss);
        $.ajax({
            url      : '<?php echo base_url();?>hospital/Clients/addmorederviceanddrugs1',
            method   : 'post',
            dataType    : 'text',
            data     : {strs : str},
            success  : function(response){
                if(response =='sucess'){
                    closed();
                    loadModalView1(hostel_idss);

                    //$('#currentredirect').trigger('click');
                }
            }
        });
    }
    function showservicedrugs(){
        $('#showservicedrugs').show();
    }
</script>
<script>
    function getaddmore(value)
    {
        var value1 = value -1;
        var value2 = value + 1;

        $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/addmoredive',
            method   : 'post',
            dataType    : 'text',
            data     : {value : value},
            success  : function(response){
                console.log(response);
                $("#addmore").attr("onclick","return getaddmore("+value2+")");

                $("#div"+value1).after(response);
                $('#drugs'+value).select2();
            }
        });

    }
     function getaddmoreService(value)
    {
        var value1 = value -1;
        var value2 = value + 1;

        $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/addmoreservicedive',
            method   : 'post',
            dataType    : 'text',
            data     : {value : value},
            success  : function(response){
                console.log(response);
                $("#addmore_ser").attr("onclick","return getaddmoreService("+value2+")");

                $("#ser_div"+value1).after(response);
                $('#service'+value).select2();
            }
        });

    }
    function getaddmoremore(value)
    {
        var value1 = value -1;
        var value2 = value + 1;
        $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/addmoredivemore',
            method   : 'post',
            dataType    : 'text',
            data     : {value : value},
            success  : function(response){
                console.log(response);
                $("#addmoremore").attr("onclick","return getaddmoremore("+value2+")");
                //$('.select2').select2();
                $("#divmore"+value1).after(response);
                $('#drugsmore'+value).select2();
            }
        });
    }
     function getaddmoreServicemore(value)
    {
        var value1 = value -1;
        var value2 = value + 1;
        $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/addmoredivemoreSer',
            method   : 'post',
            dataType    : 'text',
            data     : {value : value},
            success  : function(response){
                console.log(response);
                $("#addmoremore").attr("onclick","return getaddmoremore("+value2+")");
                //$('.select2').select2();
                $("#divmore"+value1).after(response);
                $('#drugsmore'+value).select2();
            }
        });
    }
    function removediv(values){
        $('#div'+values).hide();
    }
    function removeSerdiv(values){
        $('#ser_div'+values).hide();
    }
    
    function removedivmore(values){
        $('#divmore'+values).hide();
    }
    $(document).ready(function() {
        $('#example').DataTable( {
            "order": [[ 4, "desc" ]]
        } );
    } );
    function sendadmissionrequest()
    {
        $('#showservicedrugs').show();
        $('#sendadmissionflage').val('2');
        $('#updatebtn').attr('onclick','return adddrugsservice1();');
        $('#dayscount').show();
    }
</script>