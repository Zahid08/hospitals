<?php $session = $this->session->userdata('hospital_name'); ?>

<?php $hospitalinfo = $this->Clients_model->read_hospital_info($session['hospital_id']); ?>


<div class="row animated fadeInRight">

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="#">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-primary"><i class="fa fa-users"></i></span>

      <div class="info-box-content"> <span class="info-box-number"> <?php echo $total_clients + $total_dependants + $total_capitation; ?></span> <span class="info-box-number client-hr-invoice">Total Enrollees</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="https://care.precioushmo.com/app/hospital/clients/index">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-green"><i class="fa fa-hospital-o"></i></span>

      <div class="info-box-content"> <span class="info-box-number"><?php echo $total_auths + 0; ?></span> <span class="info-box-number client-hr-invoice">Total Authorizations</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="https://care.precioushmo.com/app/hospital/clients/all_bills_paid">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-purple"><i class="fa fa-medkit"></i></span>

      <div class="info-box-content"> <span class="info-box-number"><?php echo $total_bills + 0; ?></span> <span class="info-box-number client-hr-invoice">Total Bills</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

  

    <!-- /.info-box --> 

  </div>

</div>

<div class="row animated fadeInRight">

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="https://care.precioushmo.com/app/hospital/clients/index">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-yellow"><i class="fa fa-ambulance"></i></span>

      <div class="info-box-content"> <span class="info-box-number"> <?php echo $total_bills + 0; ?></span> <span class="info-box-number client-hr-invoice">Total Encounters</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="https://care.precioushmo.com/app/hospital/clients/ticket_list">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-red"><i class="fa fa-table"></i></span>

      <div class="info-box-content"> <span class="info-box-number">0</span> <span class="info-box-number client-hr-invoice">Total Complains</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="#">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-green"><i class="fa fa-tasks"></i></span>

      <div class="info-box-content"> <span class="info-box-number">0</span> <span class="info-box-number client-hr-invoice">Total Appointments</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>
  </div>

<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	
	axisX: {
		valueFormatString: "DDD"
	},
	axisY: {
		prefix: ""
	},
	toolTip: {
		shared: true
	},
	legend:{
		cursor: "pointer",
		itemclick: toggleDataSeries
	},
	data: [{
		type: "stackedBar",
		name: "Principals",
		showInLegend: "true",
		xValueFormatString: "DD, MMM",
		yValueFormatString: "$#,##0",
		dataPoints: [
			{ x: new Date(2017, 0, 30), y: 56 },
			{ x: new Date(2017, 0, 31), y: 45 },
			{ x: new Date(2017, 1, 1), y: 71 },
			{ x: new Date(2017, 1, 2), y: 41 },
			{ x: new Date(2017, 1, 3), y: 60 },
			{ x: new Date(2017, 1, 4), y: 75 },
			{ x: new Date(2017, 1, 5), y: 98 }
		]
	},
	{
		type: "stackedBar",
		name: "Dependants",
		showInLegend: "true",
		xValueFormatString: "DD, MMM",
		yValueFormatString: "$#,##0",
		dataPoints: [
			{ x: new Date(2017, 0, 30), y: 86 },
			{ x: new Date(2017, 0, 31), y: 95 },
			{ x: new Date(2017, 1, 1), y: 71 },
			{ x: new Date(2017, 1, 2), y: 58 },
			{ x: new Date(2017, 1, 3), y: 60 },
			{ x: new Date(2017, 1, 4), y: 65 },
			{ x: new Date(2017, 1, 5), y: 89 }
		]
	},
	{
		type: "stackedBar",
		name: "Authorization Codes",
		showInLegend: "true",
		xValueFormatString: "DD, MMM",
		yValueFormatString: "$#,##0",
		dataPoints: [
			{ x: new Date(2017, 0, 30), y: 48 },
			{ x: new Date(2017, 0, 31), y: 45 },
			{ x: new Date(2017, 1, 1), y: 41 },
			{ x: new Date(2017, 1, 2), y: 55 },
			{ x: new Date(2017, 1, 3), y: 80 },
			{ x: new Date(2017, 1, 4), y: 85 },
			{ x: new Date(2017, 1, 5), y: 83 }
		]
	},
	{
		type: "stackedBar",
		name: "Claims",
		showInLegend: "true",
		xValueFormatString: "DD, MMM",
		yValueFormatString: "$#,##0",
		dataPoints: [
			{ x: new Date(2017, 0, 30), y: 61 },
			{ x: new Date(2017, 0, 31), y: 55 },
			{ x: new Date(2017, 1, 1), y: 61 },
			{ x: new Date(2017, 1, 2), y: 75 },
			{ x: new Date(2017, 1, 3), y: 80 },
			{ x: new Date(2017, 1, 4), y: 85 },
			{ x: new Date(2017, 1, 5), y: 105 }
		]
	},
	{
		type: "stackedBar",
		name: "Complains",
		showInLegend: "true",
		xValueFormatString: "DD, MMM",
		yValueFormatString: "$#,##0",
		dataPoints: [
			{ x: new Date(2017, 0, 30), y: 52 },
			{ x: new Date(2017, 0, 31), y: 55 },
			{ x: new Date(2017, 1, 1), y: 20 },
			{ x: new Date(2017, 1, 2), y: 35 },
			{ x: new Date(2017, 1, 3), y: 30 },
			{ x: new Date(2017, 1, 4), y: 45 },
			{ x: new Date(2017, 1, 5), y: 25 }
		]
	}]
});
chart.render();

function toggleDataSeries(e) {
	if(typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else {
		e.dataSeries.visible = true;
	}
	chart.render();
}

}
</script>
</head>
<body>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

<style type="text/css">

.box-body {

    padding: 0 !important;

}

.info-box-number {

	font-size:16px !important;

	font-weight:300 !important;

}

</style>

<body>
<div id="chartContainer" style="height: 280px; width: 100%;"></div>
</body>