<?php

$session = $this->session->userdata('hospital_name');

$theme = $this->Xin_model->read_theme_info(1);

// set layout / fixed or static

if($theme[0]->right_side_icons=='true') {

	$icons_right = 'expanded menu-icon-right';

} else {

	$icons_right = '';

}

if($theme[0]->bordered_menu=='true') {

	$menu_bordered = 'menu-bordered';

} else {

	$menu_bordered = '';

}

$user_info = $this->Clients_model->read_hospital_info($session['hospital_id']);

// if($user_info[0]->is_active!=1) {

// 	redirect('client/auth/');

// }



?>

<?php $system = $this->Xin_model->read_setting_info(1);?>

<?php $arr_mod = $this->Xin_model->select_module_class($this->router->fetch_class(),$this->router->fetch_method()); ?>

<?php  if( isset($user_info[0]->logo_img) && $user_info[0]->logo_img!='' && $user_info[0]->logo_img!='no file') {?>

	<?php $cpimg = base_url().'uploads/clients/'.$user_info[0]->logo_img;?>

<?php } else {?>

<?php  if(isset($user_info[0]->gender) and $user_info[0]->gender=='Male') { ?>

<?php 	$de_file = base_url().'uploads/clients/default_male.jpg';?>

<?php } else { ?>

<?php 	$de_file = base_url().'uploads/clients/default_female.jpg';?>

<?php } ?>

    <?php $cpimg = $de_file;?>

<?php  } ?>

<!-- menu start-->

<section class="sidebar">

  <!-- Sidebar user panel -->

  

  <div class="user-panel">

        <div class="image text-center"></div>

        <div class="info">

          <p><?php echo $user_info[0]->hospital_name;?></p>

          <a href="<?php echo site_url('hospital/profile');?>"><i class="fa fa-user"></i></a> <a href="<?php echo site_url('hospital/logout');?>"><i class="fa fa-power-off"></i></a> </div>

      </div>

  <!-- sidebar menu: : style can be found in sidebar.less -->

  <ul class="sidebar-menu" data-widget="tree">

    <li class="<?php if(!empty($arr_mod['active']))echo $arr_mod['active'];?>"> <a href="<?php echo site_url('hospital/dashboard');?>"> <i class="fa fa-dashboard"></i> <span><?php echo $this->lang->line('dashboard_title');?></span> </a> </li>
          <li class="<?php if(!empty($hr_profile_detail))echo $hr_profile_detail;?>"> <a href="<?php echo site_url('hospital/clients/create');?>"> <i class="fa fa fa-plus"></i>&nbsp;Create Encounter </a> </li>
       <li class="<?php if(!empty($hr_profile_detail))echo $hr_profile_detail;?>"> <a href="<?php echo site_url('hospital/clients/index');?>"> <i class="fa fa-user-md"></i> <span>All Encounters</span> </a> </li>
              <li class="<?php if(!empty($hr_profile_detail))echo $hr_profile_detail;?>"> <a href="<?php echo site_url('hospital/clients/all_bills_paid');?>"> <i class="fa fa-money"></i> <span>Track Bills</span> </a> </li>
       <li class="<?php if(!empty($hr_profile_detail))echo $hr_profile_detail;?>"> <a target="_blank" href="http://www.phclive.liontech.com.ng/start"> <i class="fa fa-comments"></i> <span>Chat with Us</span> </a> </li>
       <li class="<?php if(!empty($hr_profile_detail))echo $hr_profile_detail;?>"> <a href="<?php echo site_url('hospital/clients/ticket_list');?>"> <i class="fa fa-support"></i> <span>My Complains</span> </a> </li>
    <li class="<?php if(!empty($arr_mod['hr_password_active']))echo $arr_mod['hr_password_active'];?>"> <a href="<?php echo site_url('hospital/profile?change_password=true');?>"> <i class="fa fa-lock"></i> <span><?php echo $this->lang->line('header_change_password');?></span> </a> </li>


    <li> <a href="<?php echo site_url('hospital/logout');?>"> <i class="fa fa-sign-out"></i> <span><?php echo $this->lang->line('left_logout');?></span> </a> </li>

  </ul>

</section>