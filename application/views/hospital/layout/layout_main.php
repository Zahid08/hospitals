<?php
$session = $this->session->userdata('hospital_name');

$system = $this->Xin_model->read_setting_info(1);

$layout = $this->Xin_model->system_layout();

$company_info = $this->Xin_model->read_company_setting_info(1);

$user_info = $this->Clients_model->read_hospital_info($session['hospital_id']);

//material-design

$theme = $this->Xin_model->read_theme_info(1);

// set layout / fixed or static

if($theme[0]->boxed_layout=='true') {

	$lay_fixed = 'container boxed-layout';

} else {

	$lay_fixed = '';

}

if($theme[0]->compact_menu=='true') {

	$menu_collapsed = 'menu-collapsed';

} else {

	$menu_collapsed = '';

}

if($theme[0]->flipped_menu=='true') {

	$menu_flipped = 'menu-flipped';

} else {

	$menu_flipped = '';

}

if($this->router->fetch_class() =='chat'){

	$chat_app = 'chat-application';

} else {

	$chat_app = '';

}

?>

<?php $this->load->view('hospital/components/htmlheader');?>

<body class="hrsale-layout hold-transition skin-black idebar-mini fixed">

<div class="wrapper">



  <?php $this->load->view('hospital/components/header');?>

  <!-- Left side column. contains the logo and sidebar -->

  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->

    <!-- Links -->

    <?php $this->load->view('hospital/components/left_menu');?>

    <!-- /.sidebar -->

  </aside>


<?php 

// echo "<pre>";
//     echo "string";
//     die();
     ?>

  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <?php if($this->router->fetch_class() !='dashboard' && $this->router->fetch_class() !='chat' && $this->router->fetch_class() !='calendar' && $this->router->fetch_class() !='profile'){?>

    <section class="content-header">

      <h1>

        <?php //echo $breadcrumbs;?>

        <!--<small><?php echo $breadcrumbs;?></small>-->

      </h1>

      <ol class="breadcrumb">

        <li><a href="<?php echo site_url('hospital/dashboard/');?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('xin_e_details_home');?></a></li>

        <li class="active"><?php //echo $breadcrumbs;?></li>

      </ol>

    </section>

    <?php } ?>

    <!-- Main content -->

    <section class="content">

      <!-- Small boxes (Stat box) -->

      

      <!-- /.row -->

      <!-- Main row -->

      <?php // get the required layout..?>

	   <?php echo $subview;?>

      <!-- /.row (main row) -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

  <?php $this->load->view('client/components/footer');?>

 

  <!-- Add the sidebar's background. This div must be placed

       immediately after the control sidebar -->

  <div class="control-sidebar-bg"></div>

</div>

<!-- ./wrapper -->



<!-- Layout footer -->

<?php $this->load->view('client/components/htmlfooter');?>

<!-- / Layout footer -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Enrollee Encounter Details</h3>
        </div>
        <div class="modal-body" id="fetched_data">
        </div>
        <div class="clearfix"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="viewReasonModal" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title" id="rtitle"></h3>
        </div>
        <div class="modal-body" id="fetched_data">
          <div class="col-lg-12" id="fetched_reason"></div>
        </div>
        <div class="clearfix"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      </div>
      
    </div>
	<div class="modal" id="myModal2" role="dialog" style="display: none; padding-right: 17px;">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" onclick="return closed();">X</button>
          <h3 class="modal-title">Encounter Bill Request</h3>
        </div>
        <div class="modal-body" id="fetched_data_request">
          <div class="col-lg-12">
            <div class="col-lg-4">
			<button type="button" class="btn btn-primary" onclick="return showservicedrugs();">ADD MORE DRUGS OR SERVICES </button>
			
		  </div>
		   <div class="col-lg-4">
			<button type="button" class="btn btn-warning" onclick="return sendadmissionrequest();">SEND ADMISSION REQUEST</button>
			
		  </div>
		  <div class="col-lg-4">
			<a href="#" id="successprocess"><button type="button" class="btn btn-success" onclick="return closed();" id="currentredirect">SEND CURRENT BILL REQUEST</button></a>
		  </div>
		
</div>		 
		 <div class="form-body row" style="display:none;" id="showservicedrugs">
		 <form id="frms" name="frms">
		  <div class="col-md-12" style="margin-top:10px" >
		 <div class="col-md-6">
		 <div id="ser_divmore1" class="row form-group">
 <div class="col-md-8"> 
  <label for="services">Select Services | Procedure | Investigation</label>
<select class="form-control select2 select3" name="diagnose_services_more[]" data-plugin="select_hrm" data-placeholder="Select Services" multiple id="servicesmodel"> 
   
	<option value="">Select Services</option>

<?php if (isset($xin_services_hospital) and !empty($xin_services_hospital)) { ?>


<?php foreach ($xin_services_hospital as $key => $value): ?>
	<option value="<?php echo isset($value->id  ) ? $value->id   : ''; ?> [<?php echo isset($value->service_price) ? $value->service_price : ''; ?>]"><?php echo isset($value->service_name) ? $value->service_name : ''; ?> (₦<?php echo isset($value->service_price) ? $value->service_price : ''; ?>)</option>
<?php 
endforeach;
}
?>
</select>
</div>
<div class="col-md-2"> 
		<label for="last_name">Quantity</label> 
		<input type="number" name="s_quantitymore[]" id="quantitymore1" class="form-control" min="1" value="1"> 								
</div>
<div class="col-md-2">
								<label for="last_name" class="padding"></label>
								<input type="button" name="addmore" id="addmoremore_ser" onclick="return getaddmoreServicemore(2)" value="+" class="btn btn-success">
								
								</div>
</div>

 <div class="form-group" id="dayscount" style="display:none;">
 
  <label for="days">Select Number of Days</label>
<select class="form-control" name="days" data-placeholder="Select Days" id="days"> 
   
	<option value="">Select Days</option>
	<?php
	for($k=1;$k<31;$k++)
	{
		echo '<option value="'.$k.'">'.$k.'</option>';
	}
	?>
</select>
</div>

		 </div>
		 <div class="col-md-6">
		 <div class="row form-group" id="divmore1">
								<div class="col-md-7">
                                <label for="last_name">Select Drug</label>
                                <select class="form-control select2" name="diagnose_drugs_more[]" data-plugin="select_hrm" data-placeholder="Select Drugs" id="drugsmore1"> 
                                     
                                    <option value="">Select Drug</option>
								<?php
								if (isset($all_hospital_drugs) and !empty($all_hospital_drugs)) {
                         
                     
                       foreach ($all_hospital_drugs as $key => $value):
					   
					   
                                    ?><option value="<?php echo $value->drug_id.'['.$value->drug_price.']';?>"><?php echo $value->drug_name.'(₦'.$value->drug_price.')';?></option>';
                     <?php
                            endforeach;
                        }
						?>
								</select>
								</div>
								<div class="col-md-3">
								<label for="last_name">Quantity</label>
								<input type="number" name="quantitymore[]" id="quantitymore1" class="form-control" min="1" value="1">
								</div>
								<div class="col-md-2">
								<label for="last_name" class="padding"></label>
								<input type="button" name="addmore" id="addmoremore" onclick="return getaddmoremore(2)" value="+" class="btn btn-success">
								
								</div>
								
                                </div>
		 

		 </div>
		  
                            </div>
							</br>
    <div class="col-md-6 form-group" style="width: 49.3%;">
      <div class="row">
        <div id="div21"></div>
          <div class="col-md-12 mt-3">
            <input type="button" id="addmore" onclick="addServiceDiv()" value="Add New Services | Procedure | Investgation +" class="btn btn-success form-control">  
          </div>
        </div>
    </div>
    <div class="col-md-6 form-group pull-right" style="width: 550px;">
          <div class="row" id="div1">
              <div id="div22"></div>
              <div class="col-md-12 mt-3">
                  <input type="button" id="addmore" value="Add New Drug +" onclick="addmoreDrugs()" class="btn btn-success form-control">
              </div>
          </div>
      </div>
		 <div class="col-md-12">
		 <input type="hidden" name="hostel_idss" id="hostel_idss" value="">
		<input type="hidden" name="sendadmissionflage" id="sendadmissionflage" value="1">
<button type="button" class="btn btn-success" id="updatebtn" onclick="return adddrugsservice();">UPDATE</button> &nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="btn btn-warning" id="previewbtn">PREVIEW</a></div>
</form>
		 </div>
		  </div>
                
            
        <div class="clearfix"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" onclick="return closed();">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <div class="modal fade" id="drug_reasonModal" role="dialog" style="z-index:100000 !important;">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Reasons For Removing This Drug</h3>
        </div>
        <form action="<?php echo base_url(); ?>hospital/clients/drug_notes" name="drugreason_form" id="drugreason_form" method="post">
          <div class="modal-body" id="drugfetched_data">
            <div class="form-group col-lg-12">
             
              <?php if(current_url() == 'https://liontechhmo.liontech.com.ng/app/admin/Hospital/adjusted_requests'){ ?>
              <label for="reason">Reasons Below:</label>
              <textarea class="form-control" name="that_drug_reason" id="that_drug_note" rows="7" readonly></textarea>
              <?php } else{ ?>


                  <label for="reason">Reason below:</label>
                  <textarea class="form-control" name="that_drugreason" id="that_drugreason" rows="7" placeholder="Write the reason here" required></textarea>
              <input type="hidden" name="drug_did" id="drug_did" class="form-control">
              <input type="hidden" name="drug_hid" id="drug_hid" class="form-control">
              <input type="hidden" name="drug_cid" id="drug_cid" class="form-control">
              <input type="hidden" name="drug_mid" id="drug_mid" class="form-control">
              <input type="hidden" name="redirectflage" id="redirectflage" class="form-control" value="1">
              <?php } ?> 
            </div>
          </div> 
          <div class="clearfix"></div>
          <div class="modal-footer">
            <?php if(current_url() != 'https://liontechhmo.liontech.com.ng/app/admin/Hospital/adjusted_requests'){ ?>
            <button type="button" class="btn btn-success" onclick="return adddrungsreason();">Proceed</button>
            <?php

            } ?>
            <button type="button" class="btn btn-danger" data-dismiss="modal" id="closed2">Close</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
  
   <div class="modal fade" id="service_reasonModal" role="dialog"  style="z-index:100000 !important;">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Reasons For Removing This Service</h3>
        </div>
        <form action="<?php echo base_url(); ?>hospital/clients/service_notes" name="service_reason_form" id="service_reason_form" method="post">
          <div class="modal-body" id="service_fetched_data">
            <div class="form-group col-lg-12">
              
              <?php if(current_url() == 'https://liontechhmo.liontech.com.ng/app/admin/Hospital/adjusted_requests'){ ?>
              <label for="reason">Reasons For Removing This Service:</label>
              <textarea class="form-control" name="that_service_reason" id="that_service_note" rows="7" readonly></textarea>
              <?php } else{ ?>

                  <label for="reason">Reason below:</label>
                  <textarea class="form-control" name="that_service_reason" id="that_service_reason" rows="7" placeholder="Write the reason for here" required></textarea>
              <input type="hidden" name="service_did" id="service_did" class="form-control">
              <input type="hidden" name="service_hid" id="service_hid" class="form-control">
              <input type="hidden" name="service_mid" id="service_mid" class="form-control">
              <input type="hidden" name="service_sid" id="service_sid" class="form-control">
              <input type="hidden" name="service_flage" id="service_flage" class="form-control" value="1">
	      <?php } ?> 
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="modal-footer">
            <?php if(current_url() != 'https://liontechhmo.liontech.com.ng/app/admin/Hospital/adjusted_requests'){ ?>
            <button type="button" class="btn btn-success" onclick="return addservicereason();">Proceed</button>
            <?php } ?>
            <button type="button" class="btn btn-danger" data-dismiss="modal" id="closed1">Close</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
  
  
  <div class="modal" id="myModal3" style="display: none; padding-right: 17px;">
    <div class="modal-dialog modal-lg" style="max-height:630px;overflow-x:scroll;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" onclick="return closed1();">X</button>
          <h3 class="modal-title">Encounter Bill Request</h3>
        </div>
        <div class="modal-body">
          <div class="col-lg-12">
            	 
			<div id="previews"></div>
		
			<a href="#" id="updatesrecords"><button type="button" class="btn btn-success"><b>SEND BILL REQUEST</b></button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" id="updatesrecords1"><button type="button" class="btn btn-warning"><b>SEND ADMISSION REQUEST</b></button></a></div>
			
		  </div>
                
        <div class="clearfix"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" onclick="return closed1();">Close</button>
        </div>
      </div>
    </div>
  </div>
  </div>
<div class="modal fade" id="reasconModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Removing Reason</h4>
            </div>
            <div class="modal-body">
                <p id="custom_reason">This is a small modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</div>
</body>

</html>

<script>
  var counter = 0;
  var counter2 = 0;
  function addServiceDiv(){
    var div2 = document.getElementById('div21');
    var serv = '<div id="q'+counter2+'"><div class="col-md-6">'+
                            '<label for="last_name">Add New Services </label>'+
                            '<input type="text" name="s_services[]" class="form-control">'+      
                          '</div>'+
                          '<div class="col-md-2">'+
                            '<label for="last_name">Price</label>'+
                            '<input type="text" name="s_price[]" value=1 class="form-control">'+
                          '</div>'+
                          '<div class="col-md-2">'+
                            '<label for="last_name">Quantity</label>'+
                            '<input type="text" name="s_quantity[]" value=1 class="form-control">'+
                          '</div>'+
                          '<div class="col-md-2" style="text-align: right;">'+
                            '<label for="last_name" class="padding "></label>'+
                            '<input type="button" name="addmore" id="removemore2" style="text-align:right:"  onclick=removeDrug("q'+counter2+'") value="-" class="btn btn-danger">'+
                          '</div></div>';
    $("#div21").append(serv);                          
counter2++;
  }
  function addmoreDrugs(){
    var div1 = document.getElementById('div22');
   var drug = ' <div id="'+counter+'"><div class="col-md-6">'+
                                '<label for="last_name">Add New Drug</label>'+
                                '<input type="text" class="form-control" name="d_diagnose_drugs[]">'+
                            '</div>'+
                            '<div class="col-md-2">'+
                                '<label for="last_name">Quantity</label>'+
                                '<input type="number" name="d_quantity[]" id="quantity1"  class="form-control" min="1" value="1">'+
                            '</div>'+
                            '<div class="col-md-2">'+
                                '<label for="last_name">Price</label>'+
                                '<input type="text" name="d_price[]" id="quantity1"  class="form-control" min="1" value="1">'+
                            '</div>'+
                            '<div class="col-md-2">'+
                                '<label for="last_name" class="padding"></label>'+
                                '<input type="button" name="addmore" id="removemore2" style="text-align:right:" onclick="removeDrug('+counter+')" value="-" class="btn btn-danger">'+
                            '</div></div>';
     $("#div22").append(drug);
    counter++;
  }

  function removeDrug(id){
    var rem = document.getElementById(id);
    rem.remove();
  }
    function loadDrugReasonModal(drug_did,drug_hid,drug_cid,drug_mid){
        //alert(drug_did);
        $("#drug_reasonModal").on("show.bs.modal", function(e) {
            $("#drug_did").val(drug_did);
            $("#drug_hid").val(drug_hid);
            $("#drug_cid").val(drug_cid);
            $("#drug_mid").val(drug_mid);
            $("#redirectflage").val('2');
            $("#dugus"+drug_cid).hide();
        });
        // $("#that_drugreason").val("");




    }
    function loadServiceReasonModal(service_did,service_hid,service_mid,service_sid){
        //alert(service_mid);
        //$("#that_service_reason").val("");
        $("#service_reasonModal").on("show.bs.modal", function(e) {
            $("#service_did").val(service_did);
            $("#service_hid").val(service_hid);
            $("#service_mid").val(service_mid);
            $("#service_sid").val(service_sid);

            $("#service_flage").val('2');
            $("#service"+service_did).hide();
        });

    }
    function service_note(id){
        // alert("ID is: " + id);

        $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_service_note',
            method   : 'post',
            dataType    : 'text',
            data     : {id : id},
            success  : function(response){
                $("#that_service_reason").html(response);
                $("#that_service_reason"). attr('readonly','readonly');
            }
        });

    }
    function drug_note(id){
        // alert("ID is: " + id);

        $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_drug_note',
            method   : 'post',
            dataType    : 'text',
            data     : {id : id},
            success  : function(response){
                // $("#id_we_get_is").text(response);
                //alert(response);
                $("#that_drugreason").html(response);
                $("#that_drugreason"). attr('readonly','readonly');
                //$("#reasconModal").modal('show');
            }
        });

    }



    $(document).ready(function() {

      // alert("yess its wokringg wahidddd");

      $("#diagnose_client_form").hide();
      $("#diagnose_client").click(function(){
          $("#diagnose_client_form").slideToggle();
          // $("#client").change(function() {
          // console.log("asd"+$(this).val());
        // });
      });
    $('document').on('change','#client',function(){
      alert('Change Happened');
    });


      // $("#client").change(function(){
      //   var id = $(".client").val();
      //   alert("asd");
      //   console.log('asd');
      //   console.log(id);
      //   $.ajax({
      //       url      : '<?php echo base_url(); ?>hospital/Clients/get_dob',
      //       method   : 'post',   
      //       dataType    : 'text',      
      //       data     : {id : id},
      //       success  : function(response){
      //         // $("#id_we_get_is").text(response);
      //         // alert(response);
      //         console.log('yes');
      //         console.log(response);
      //         // $("#fetched_reason").html(response);
      //       }
      //     });
      // });

      $('#client').click(function(){
        alert("asd");
      });

      $("#client").change(function(){
            
            var cid = $("#client").val();
            // alert(hid);
            console.log(cid);
            $.ajax({
                  url      : '<?php echo base_url(); ?>admin/Hospital/fetch_client_plan',
                  method   : 'post',   
                  dataType    : 'text',      
                  data     : {id : cid},
                  success  : function(response){
                    // $("#id_we_get_is").text(response);
                    // alert(response);
                    $("#plan_name").text(response);
                    console.log(response);
                    
                  }
                });      
        });
      
  });
</script>

