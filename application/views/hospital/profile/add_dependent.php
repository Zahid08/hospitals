<?php

/* Add Dependent view

*/
$ci = & get_instance();  
     
$ci->load->model('Training_model');
?>

<?php $session = $this->session->userdata('client_username');?>

<?php $user = $this->Clients_model->read_client_info($session['client_id']);?>

<?php $system = $this->Xin_model->read_setting_info(1);?>

<!-- <?php if($client_profile!='' && $client_profile!='no file') {?>

<?php $de_file = base_url().'uploads/clients/'.$client_profile;?>

<?php } else {?>

<?php if($gender=='Male') { ?>

<?php $de_file = base_url().'uploads/clients/default_male.jpg';?>

<?php } else { ?>

<?php $de_file = base_url().'uploads/clients/default_female.jpg';?>

<?php } ?> -->

<!-- <?php } ?> -->

<?php $get_animate = $this->Xin_model->get_content_animate();?>



<div class="row"> 
  	<div class="col-md-12 current-tab <?php echo $get_animate;?>" id="user_basic_info"  aria-expanded="false">

    	<div class="box">

      	<div class="box-header with-border">

        	<h3 class="box-title"> <?php echo $this->lang->line('xin_e_details_basic_info');?> </h3>

      	</div>

      	<div class="box-body">

	        <div class="card-block">

	          	<?php $attributes = array('name' => 'edit_client', 'id' => 'edit_client', 'autocomplete' => 'off', 'class'=>'m-b-1');?>

	          	<?php $hidden = array('_method' => 'save', '_token' => 1, 'ext_name' => '1');?>

	          	<?php echo form_open_multipart('client/profile/save_dependent', $attributes, $hidden);?>

		          	<div class="form-body">

			            <div class="row">
			            	<div class="col-md-12">
			            		<?php if ($this->session->flashdata('success')): ?>
									<div class="alert alert-success">
										<?php echo $this->session->flashdata('success'); ?>
									</div>
								<?php endif ?>


								<?php if ($this->session->flashdata('error')): ?>
									<div class="alert alert-warning">
										<?php echo $this->session->flashdata('error'); ?>
									</div>
								<?php endif ?>
			            	</div>
				            <div class="col-sm-6">

				                <div class="form-group">

				                  <label for="company_name">First Name</label>

				                  <input class="form-control" name="name"  placeholder="First Name"   type="text" value="">

				                </div>

				                <div class="form-group">


					                <div class="row">

					                    <div class="col-md-6 form-group">

					                        <label for="last_name">Last Name</label>

					                        <input class="form-control" placeholder="Last Name" name="last_name" type="text" value="">

					                    </div>

					                    <div class="col-md-6 form-group">

					                      	<label for="other_name">Other Name </label>

					                      	<input class="form-control" placeholder="Other Name" name="other_name" type="text" value="">

					                    </div>

					                </div>



				                  	<div class="row">

					                  	

				                  	</div>

				                </div>

				                 
				            </div>

				            <div class="col-md-6">

				            	<div class="row">
				            		<div class="col-md-6 form-group">

				                      	<label for="website">Date Of Birth</label>

				                      	<input class="form-control"  name="dob" value="" type="date">

				                    </div>


				                    <div class="col-md-6  form-group">

				                      <label for="contact_number"><?php echo $this->lang->line('xin_contact_number');?></label>

				                      <input class="form-control" placeholder="<?php echo $this->lang->line('xin_contact_number');?>" name="contact_number" type="number" value="">

				                    </div>
				            		
				            	</div>

				            	<!-- <div class="row">

									<div class="col-md-12">
						                <div class="form-group">

						                  	<label for="address">Resident Address</label>

						                  	<input class="form-control" placeholder="Resident address" name="address_1" type="text" value="">

										 
						                </div>
						            </div>
						        </div> -->

<!-- 
				                <div class="row" style="display: none;">

									<div class="col-md-12">
										<div class="form-group">
											<label for="address">Select State </label>

										 
											<select class="form-control" name="location" data-plugin="select_hrm" data-placeholder="Select State"> 
				                                
				                                <?php if (isset($all_locations) and !empty($all_locations)): ?>
				                                    <?php foreach ($all_locations as  $value): ?>
				                                        <?php echo "<option value='".$value->location_id."' >".$value->location_name."</option> " ?>
				                                    <?php endforeach ?>
				                                    
				                                <?php endif ?>
				                            </select> 
										</div>

									</div>
								</div> -->

								<div class="row">

									<div class="col-md-6"> 
										<div class="form-group">
					                      	<label for="sec">Sex</label>

					                      	<select class="form-control" name="sex" data-plugin="select_hrm" data-placeholder="Sex">

							                    <option value="">Select Sex</option> 

							                    <option value="male"  > Male </option>

							                    <option value="female"> Female </option> 
						                  	</select>
						                </div>
				                    </div>

				                    <div class="col-md-6"> 
										<div class="form-group">
					                      	<label for="sec">Relation</label>

					                      	<select class="form-control" name="relation" data-plugin="select_hrm" data-placeholder="Relation">

							                    <option value="">Select Relation</option> 

							                    <option value="wife"  > Wife </option>

							                    <option value="son"> Son </option> 
							                    <option value="daughter"> Daughter </option> 
						                  	</select>
						                </div>
				                    </div>
				                    
								</div>
				            </div>

				        </div>


			            <div class="row"> 
							<div class="col-md-12">
								<div class="form-group">
									<label for="address">Select Hospital </label>
									<?php 
										
										$subscriptions = explode(',', $subscriptions);
										$hos_names     = '';
										$subs_pkg      = '';
										foreach ($subscriptions as $key => $value) 
										{
											$subs = $ci->Training_model->getAll2('xin_subscription',' subscription_id = "'.$value.'" ');


											if (isset($subs[0]->band_types) AND !empty($subs[0]->band_types)) 
											{

												$subs_pkg .= $subs[0]->plan_name.' , ';

												$bands  = explode(',', $subs[0]->band_types);

												foreach ($bands as $key2 => $band) 
												{
													$hos_data = $ci->Training_model->getAll2('xin_hospital',' band_id = "'.$band.'" ');

													 

													if(isset($hos_data[0]->hospital_name))
													{
														$location_data = $ci->Training_model->getAll2('xin_location',' location_id = "'.$hos_data[0]->location_id.'" ');
														$location = '';

														if(isset($location_data[0]->location_name))
														{
															$location = '('.$location_data[0]->location_name.')';
														}
														
														$hos_names .= '<option value="'.$hos_data[0]->hospital_id.'" >'.$hos_data[0]->hospital_name .' '.$location.'</option>';
													}
												}
											}

											
										}

									?>
									 
									<select class="form-control" name="hospital_id" data-plugin="select_hrm" data-placeholder="Select Hospital"> 
										<?php echo $hos_names; ?>
		                            </select>
								</div>

							</div> 
			            </div>


			            <div class="row">
			            	<div class="col-md-6">

				                <fieldset class="form-group">

				                  <label for="logo"><?php echo $this->lang->line('xin_project_client_photo');?></label>

				                  <input type="file" class="form-control-file" id="client_photo" name="client_photo">

				                  <br />

				                  <small><?php echo $this->lang->line('xin_company_file_type');?></small>

				                </fieldset>

			              	</div> 
			            </div>

			              

			            <hr>
			              
			        </div>

		      		<div class="form-actions"> <?php echo form_button(array('name' => 'hrsale_form', 'type' => 'submit', 'class' => $this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '.$this->lang->line('xin_save'))); ?> 
		      		</div>

	     		<?php echo form_close(); ?> 
	     	</div>

      	</div>

    	</div>

  	</div>

 
</div>

