<?php

/* Profile view

*/

?>

<?php $session = $this->session->userdata('hospital_name');?>

<?php $user = $this->Clients_model->read_hospital_info($session['hospital_id']);?>

<?php $system = $this->Xin_model->read_setting_info(1);?>

<?php if($logo_img!='' && $logo_img!='no file') {?>

<?php $de_file = base_url().'uploads/hospital/logo/'.$logo_img;?>

<?php }else {  ?>


<?php $de_file = base_url().'uploads/clients/default_female.jpg'; ?>
 
 
<?php } ?>


<?php $get_animate = $this->Xin_model->get_content_animate();?>



<div class="row">

  <div class="col-md-3 <?php echo $get_animate;?>"> 

    

    <!-- Profile Image -->

    <div class="box box-primary">

      <div class="box-body box-profile"> <a class="" href="#" data-toggle="tab" aria-expanded="true"> <img class="profile-user-img img-responsive img-circle" src="<?php echo $de_file;?>" alt="<?php echo $user[0]->hospital_name;?>"></a>

        <h3 class="profile-username text-center"><?php echo $user[0]->hospital_name;?></h3>

        <div class="list-group">
  

          <a class="list-group-item-profile list-group-item list-group-item-action nav-tabs-link" href="#change_password" data-profile="14" data-profile-block="change_password" data-toggle="tab" aria-expanded="true" id="user_profile_14"> <i class="fa fa-key"></i> <?php echo $this->lang->line('xin_e_details_cpassword');?> </a> </div>

      </div>

      <!-- /.box-body --> 

    </div>

  </div>

 

  <div class="col-md-9 current-tab <?php echo $get_animate;?>" id="change_password"  >

    <div class="box">

      <div class="box-header with-border">

        <h3 class="box-title"> <?php echo $this->lang->line('xin_e_details_cpassword');?> </h3>

      </div>

      <div class="box-body">

        <div class="card-block">

          <?php $attributes = array('name' => 'e_change_password', 'id' => 'e_change_password', 'autocomplete' => 'off');?>

          <?php $hidden = array('u_basic_info' => 'UPDATE');?>

          <?php echo form_open('hospital/profile/change_password/', $attributes, $hidden);?>

          <?php

          $data_usr11 = array(

                'type'  => 'hidden',

                'name'  => 'hospital_id',

                'value' => $session['hospital_id'],

         );

        echo form_input($data_usr11);

        ?>

          <?php if($this->input->get('change_password')):?>

          <input type="hidden" id="change_pass" value="<?php echo $this->input->get('change_password');?>" />

          <?php endif;?>

          <div class="row">

            <div class="col-md-6">

              <div class="form-group">

                <label for="new_password"><?php echo $this->lang->line('xin_e_details_enpassword');?></label>

                <input class="form-control" placeholder="<?php echo $this->lang->line('xin_e_details_enpassword');?>" name="new_password" type="text">

              </div>

            </div>

            <div class="col-md-6">

              <div class="form-group">

                <label for="new_password_confirm" class="control-label"><?php echo $this->lang->line('xin_e_details_ecnpassword');?></label>

                <input class="form-control" placeholder="<?php echo $this->lang->line('xin_e_details_ecnpassword');?>" name="new_password_confirm" type="text">

              </div>

            </div>

          </div>

          <div class="row">

            <div class="col-md-12">

              <div class="form-group">

                <div class="form-actions"> <?php echo form_button(array('name' => 'hrsale_form', 'type' => 'submit', 'class' => $this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '.$this->lang->line('xin_save'))); ?> </div>

              </div>

            </div>

          </div>

          <?php echo form_close(); ?> </div>

      </div>

    </div>

  </div>

</div>

