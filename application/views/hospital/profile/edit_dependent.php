<?php

/* Add Dependent view

*/
$ci = & get_instance();  
     
$ci->load->model('Training_model');


$client_data  = $client_data[0];
?>

<?php $session = $this->session->userdata('client_username');?>

<?php $user = $this->Clients_model->read_client_info($session['client_id']);?>

<?php $system = $this->Xin_model->read_setting_info(1);?>

<!-- <?php if($client_profile!='' && $client_profile!='no file') {?>

<?php $de_file = base_url().'uploads/clients/'.$client_profile;?>

<?php } else {?>

<?php if($gender=='Male') { ?>

<?php $de_file = base_url().'uploads/clients/default_male.jpg';?>

<?php } else { ?>

<?php $de_file = base_url().'uploads/clients/default_female.jpg';?>

<?php } ?> -->

<!-- <?php } ?> -->

<?php $get_animate = $this->Xin_model->get_content_animate();?>



<div class="row"> 
  	<div class="col-md-12 current-tab <?php echo $get_animate;?>" id="user_basic_info"  aria-expanded="false">

    	<div class="box">

      	<div class="box-header with-border">

        	<h3 class="box-title"> <?php echo $this->lang->line('xin_e_details_basic_info');?> </h3>

      	</div>

      	<div class="box-body">

      		

	        <div class="card-block">

	          	 
	          	<?php echo form_open_multipart('client/profile/save_req_dependent' );?>

		          	<div class="form-body">

			            <div class="row">
			            	<div class="col-md-12">
			            		<?php if ($this->session->flashdata('success')): ?> 
						      		<div class="alert alert-success alert-dismissible " role="alert">
							            <?php echo $this->session->flashdata('success'); ?>
							            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							                  <span aria-hidden="true">&times;</span>
							            </button>
							      	</div> 
					    		<?php endif ?>

					    		<?php if ($this->session->flashdata('error')): ?> 
						      		<div class="alert alert-warning alert-dismissible " role="alert">
							            <?php echo $this->session->flashdata('success'); ?>
							            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							                  <span aria-hidden="true">&times;</span>
							            </button>
							      	</div> 
					    		<?php endif ?>
								 
			            	</div>
				            <div class="col-sm-6">

				                <div class="form-group">

				                  <label for="company_name">Full Name</label>

				                  <input class="form-control" name="name"  placeholder="Full Name"   type="text" value="<?php echo $client_data->name ?>">

				                  <input class="form-control" name="requester_id"   type="hidden" value="<?php echo $client_data->clients_family_id ?>">

				                </div>

				                <div class="form-group">


					                <div class="row">

					                    <div class="col-md-6 form-group">

					                        <label for="last_name">Last Name</label>

					                        <input class="form-control" placeholder="Last Name" name="last_name" type="text" value="<?php echo $client_data->last_name ?>">

					                    </div>

					                    <div class="col-md-6 form-group">

					                      	<label for="other_name">Other Name </label>

					                      	<input class="form-control" placeholder="Other Name" name="other_name" type="text" value="<?php echo $client_data->other_name ?>">

					                    </div>

					                </div>



				                  	

				                </div>

				                 
				            </div>

				            <div class="col-md-6">
				            		 	
				            	<div class="row">

				                  	<div class="col-md-6 form-group">

				                      	<label for="website">Date Of Birth</label>

				                      	<input class="form-control"  name="dob" value="<?php echo $client_data->dob ?>" type="date">

				                    </div>


				                    <div class="col-md-6  form-group">

				                      <label for="contact_number"><?php echo $this->lang->line('xin_contact_number');?></label>

				                      <input class="form-control" placeholder="<?php echo $this->lang->line('xin_contact_number');?>" name="contact_number" type="number" value="<?php echo $client_data->contact_number ?>">

				                    </div>

			                  	</div>
				               
								<div class="row">

									<div class="col-md-6"> 
										<div class="form-group">
					                      	<label for="sec">Sex</label>

					                      	<select class="form-control" name="sex" data-plugin="select_hrm" data-placeholder="Sex">

							                    <option value="">Select Sex</option> 

							                    <option value="male"     <?php if($client_data->sex == 'male'):?> selected="selected"<?php endif;?>   > Male </option>

							                    <option value="female"  <?php if($client_data->sex == 'female'):?> selected="selected"<?php endif;?> > Female </option> 
						                  	</select>
						                </div>
				                    </div>

				                    <div class="col-md-6"> 
										<div class="form-group">
					                      	<label for="sec">Relation</label>

					                      	<select class="form-control" name="relation" data-plugin="select_hrm" data-placeholder="Relation">

							                    <option value="">Select Relation</option> 

							                    <option value="wife"  <?php if($client_data->relation == 'wife'):?> selected="selected"<?php endif;?>  > Wife </option>

							                    <option value="son"  <?php if($client_data->relation == 'son'):?> selected="selected"<?php endif;?>  > Son </option> 
							                    <option value="daughter"   <?php if($client_data->relation == 'daughter'):?> selected="selected"<?php endif;?>  > Daughter </option> 
						                  	</select>
						                </div>
				                    </div>
				                    
								</div>
				            </div>

				        </div>


			            <div class="row"> 
							<div class="col-md-12">
								<div class="form-group">
									<label for="address">Select Hospital </label>
									<?php 
										
										$subscriptions = explode(',', $client_p[0]->subscription_ids);
										$hos_names     = '';
										$subs_pkg      = '';
										foreach ($subscriptions as $key => $value) 
										{
											$subs = $ci->Training_model->getAll2('xin_subscription',' subscription_id = "'.$value.'" ');


											if (isset($subs[0]->band_types) AND !empty($subs[0]->band_types)) 
											{

												$subs_pkg .= $subs[0]->plan_name.' , ';

												$bands  = explode(',', $subs[0]->band_types);

												foreach ($bands as $key2 => $band) 
												{
													$hos_data = $ci->Training_model->getAll2('xin_hospital',' band_id = "'.$band.'" ');

													 

													if(isset($hos_data[0]->hospital_name))
													{
														$location_data = $ci->Training_model->getAll2('xin_location',' location_id = "'.$hos_data[0]->location_id.'" ');
														$location = '';

														if(isset($location_data[0]->location_name))
														{
															$location = '('.$location_data[0]->location_name.')';
														}
														
														$hos_names .= '<option  

														'.($client_data->hospital_id == $hos_data[0]->hospital_id ? "selected" : "").'


														   value="'.$hos_data[0]->hospital_id.'" >

														   '.$hos_data[0]->hospital_name .' '.$location.'</option>';
													}
												}
											}

											
										}

									?>
									 
									<select class="form-control" name="hospital_id" data-plugin="select_hrm" data-placeholder="Select Hospital"> 
										<?php echo $hos_names; ?>
		                            </select>
								</div>

							</div> 
			            </div>


			            <div class="row">
			            	<div class="col-md-12">
			            		<img class="profile-user-img img-responsive img-circle" src="<?php echo base_url(); ?>uploads/clients/<?php echo $client_data->client_profile; ?>" style='margin: 0px !important;' >
			            	</div>

			            	<div class="col-md-6">

				                <fieldset class="form-group">

				                	

				                  	<label for="logo"><?php echo $this->lang->line('xin_project_client_photo');?></label>

				                  	<input type="file" class="form-control-file" id="client_photo" name="client_photo">

				                  	<br />

				                  	<small><?php echo $this->lang->line('xin_company_file_type');?></small>

				                </fieldset>

			              	</div> 
			            </div>

			              

			            <hr>
			              
			        </div>

		      		<div class="form-actions"> <?php echo form_button(array('name' => 'hrsale_form', 'type' => 'submit', 'class' => $this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> Update ')); ?> 
		      		</div>

	     		<?php echo form_close(); ?> 
	     	</div>

      	</div>

    	</div>

  	</div>

 
</div>

