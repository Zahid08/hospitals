<?php

/* Dependant List view

*/

?>

<?php $session = $this->session->userdata('client_username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>



<div class="row <?php echo $get_animate;?>">

  
   
  <!-- fix for small devices only -->

  <div class="clearfix visible-sm-block"></div>

  
 

</div>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> <?php echo $this->lang->line('xin_list_all');?> Hospitals</h3>

  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>

    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

        <thead>

          <tr>

            

            <th  style="    width: 40%;">Hospital</th>

            <th  style="    width: 40%;">Location</th>

            <th style="    width: 20%;">Action</th>

            

          </tr>

        </thead>

        <tbody>
          <?php 
          if (!empty($result)) {
             
          
          foreach ($result as $key => $value): ?>
               <?php 

               $ci=& get_instance(); 

               $hos_names     = '';
               $subs_pkg      = '';
           

               $subs = $ci->Training_model->getAll2('xin_subscription',' subscription_id = "'.$value->subscription_ids.'" ');


               if (isset($subs[0]->band_types) AND !empty($subs[0]->band_types)) 
               {
              
                    $subs_pkg .= $subs[0]->plan_name;

                    $bands  = explode(',', $subs[0]->band_types);

                    foreach ($bands as $key2 => $band) 
                    {
                         $hos_data = $ci->Training_model->getAll2('xin_hospital',' band_id = "'.$band.'" '); 

                         if(isset($hos_data[0]->hospital_name))
                         {
                              $location_data = $ci->Training_model->getAll2('xin_location',' location_id = "'.$hos_data[0]->location_id.'" ');
                              $location = '';

                              if(isset($location_data[0]->location_name))
                              {
                                   $location = '('.$location_data[0]->location_name.')';
                              }

                              ?>
                              <tr>
                                   <td><?php echo $hos_data[0]->hospital_name; ?></td>
                                   <td><?php echo $location_data[0]->location_name; ?></td>

                                   <?php if ($value->hospital_id == $hos_data[0]->hospital_id){ ?>
                                        <td><button class="btn btn-default">Selected</button></td>
                                   <?php }else { ?>
                                        <td><a href="<?php echo base_url(); ?>client/profile/hospital_list?hospital_id=<?php echo $hos_data[0]->hospital_id; ?>" class="btn btn-primary">Change Request</a></td>
                                   <?php }  ?>
                              </tr>


                              <?php
                  
                               
                         }
                    }
               } 
            
            
            ?>
           
          <?php endforeach;

          }  ?>
        </tbody>

      </table>

    </div>

  </div>

</div>

<style type="text/css">

.info-box-number {

	font-size:16px !important;

	font-weight:300 !important;

}

</style>

<script type="text/javascript">
  document.addEventListener('DOMContentLoaded', function(){ 
      var xin_table_new = $('#xin_table_new').dataTable();  
  }, false);
</script>