<?php

/* Dependant List view

*/

?>

<?php $session = $this->session->userdata('client_username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>



<div class="row <?php echo $get_animate;?>">

  
   
  <!-- fix for small devices only -->

  <div class="clearfix visible-sm-block"></div>

  
 

</div>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> <?php echo $this->lang->line('xin_list_all');?> Dependants</h3>

  </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

        <thead>

          <tr>

            <th>Name</th>
            <th>Last Name</th>
            <th>Other Name</th>
            <th>Sex</th>
            <th>Relation</th> 
            <th>Hospital</th>

          

            <th>Photo</th>

            <th>Action</th>

          </tr>

        </thead>

        <tbody>
          <?php 
          if (!empty($client_data)) { 
          foreach ($client_data as $key => $value): ?>
            <?php 

            $ci=& get_instance();
            $ci->load->model('Training_model'); 

            $hospital    = $ci->Training_model->getAll2('xin_hospital', ' hospital_id='. $value->hospital_id.' ');

            $location_name = '';

            if(isset($hospital[0]->location_id))
            {
                $location = $ci->Training_model->getAll2('xin_location', ' location_id='. $hospital[0]->location_id.' ');
                if (isset($location[0]->location_name)) 
                {
                   $location_name = ' ('.$location[0]->location_name.') ';
                }

            } 

            $location_user = $ci->Training_model->getAll2('xin_location', ' location_id='. $value->location.' ');

            ?>
               <tr>
                  <td><?php echo $value->name; ?></td>
                  <td><?php echo $value->last_name; ?></td>
                  <td><?php echo $value->other_name; ?></td>
                  <td><?php echo $value->sex; ?></td>

                  <td><?php echo ucfirst($value->relation); ?></td>

                  <td><?php echo isset($hospital[0]->hospital_name) ? $hospital[0]->hospital_name.$location_name : ''; ?></td> 

                  <td><?php echo '<img class="profile-user-img img-responsive img-circle" src="'.base_url().'uploads/clients/'.$value->client_profile.'" >'; ?></td>

                  <td>  <a class="btn icon-btn btn-xs btn-default waves-effect waves-light" href="<?php echo base_url(); ?>client/profile/edit_dependent_request?dependent_id=<?php echo $value->clients_family_id ?>"><span class="fa fa-pencil"></span></a>
                  </td>
               </tr>
          <?php endforeach;  

      		} ?>
        </tbody>

      </table>

    </div>

  </div>

</div>

<style type="text/css">

.info-box-number {

	font-size:16px !important;

	font-weight:300 !important;

}

</style>

<script type="text/javascript">
  document.addEventListener('DOMContentLoaded', function(){ 
      var xin_table_new = $('#xin_table_new').dataTable();  
  }, false);
</script>