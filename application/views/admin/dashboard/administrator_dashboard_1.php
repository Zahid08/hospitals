<?php
$session = $this->session->userdata('username');
$system = $this->Xin_model->read_setting_info(1);
$company_info = $this->Xin_model->read_company_setting_info(1);
$user = $this->Xin_model->read_employee_info($session['user_id']);
$theme = $this->Xin_model->read_theme_info(1);
?>
<?php $get_animate = $this->Xin_model->get_content_animate();?>

<div class="row mt--2 <?php echo $get_animate;?>">
<div class="col-md-12">
<div class="card full-height">
<div class="card-body">
<?php $exp_am = $this->Expense_model->get_total_expenses();?>
<?php $all_sal = total_salaries_paid();?>
<div class="row py-3">
    <div class="col-md-3 d-flex flex-column justify-content-around">
        <h6 class="fw-bold text-uppercase text-success op-8"><?php echo $this->lang->line('dashboard_total_expenses');?></h6>
        <h3 class="fw-bold"><?php echo $this->Xin_model->currency_sign($exp_am[0]->exp_amount);?></h3>
    </div>
    <div class="col-md-3 d-flex flex-column justify-content-around">
        <h6 class="fw-bold text-uppercase text-danger op-8"><?php echo $this->lang->line('dashboard_total_salaries');?></h6>
        <h3 class="fw-bold"><?php echo $this->Xin_model->currency_sign($all_sal);?></h3>
    </div>
    <div class="col-md-3 d-flex flex-column justify-content-around">
        <h6 class="fw-bold text-uppercase text-info op-8"><?php echo $this->lang->line('xin_acc_account_balances');?></h6>
        <h3 class="fw-bold"><?php echo $this->Xin_model->currency_sign(total_account_balances());?></h3>
    </div>
    <div class="col-md-3 d-flex flex-column justify-content-around">
        <h6 class="fw-bold text-uppercase text-warning op-8"><?php echo $this->lang->line('xin_hrsale_travel_expenses');?></h6>
        <h3 class="fw-bold"><?php echo $this->Xin_model->currency_sign(total_travel_expense());?></h3>
    </div>
</div>
</div>
</div>
</div>
</div>
<div class="row animated fadeInRight">
    <!-- Total All Number of Enrollees -->
    <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/clients');?>">
            <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-navy"><i class="fa fa-users"></i></span>
                <div class="info-box-content"> <span class="info-box-number"><?php echo total_clients()+total_dependants()+total_capitation_principal()+total_capitation_dependant();?></span> <span class="info-box-number client-hr-invoice">Total Enrollees</span> </div>
                <!-- /.info-box-content -->
            </div>
        </a>
        <!-- /.info-box -->
    </div>
    <!-- Total Enrollee -->
    <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/clients/');?>">
        <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
        <div class="info-box-content"> <span class="info-box-number"><?php echo total_clients();?></span> <span class="info-box-number client-hr-invoice">Total Principals</span> </div>
        <!-- /.info-box-content -->
    </div>
</a>
<!-- /.info-box -->
</div>
<!-- Total Dependant -->
<div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/ClientAccount/dependant');?>">
<div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>
<div class="info-box-content"> <span class="info-box-number"><?php echo total_dependants();?></span> <span class="info-box-number client-hr-invoice"><?php echo $this->lang->line('xin_total_dependant');?></span> </div>
<!-- /.info-box-content -->
</div>
</a>
<!-- /.info-box -->
</div>
</div>
    <div class="row animated fadeInRight">
<!-- Total Family Account -->
<div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/clients?type=family');?>">
<div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-blue"><i class="fa fa-user-circle-o"></i></span>
<div class="info-box-content"> <span class="info-box-number"><?php echo total_family_account();?></span> <span class="info-box-number client-hr-invoice"><?php echo $this->lang->line('xin_total_family_account');?></span> </div>
<!-- /.info-box-content -->
</div>
</a>
<!-- /.info-box -->
</div>
        <!-- Total Individual Account -->
        <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/clients?type=individual')?>">
                <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-navy"><i class="fa fa-user-circle"></i></span>
                    <div class="info-box-content"> <span class="info-box-number"><?php echo total_individual_account();?></span> <span class="info-box-number client-hr-invoice"><?php echo $this->lang->line('xin_total_individual_account');?></span> </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
<!-- Invoice Unpaid Amount -->
  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/invoices/all_invoices_unpaid/');?>">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-red"><i class="fa fa-credit-card"></i></span>

      <div class="info-box-content"> <span class="info-box-number"><?php echo $this->Xin_model->currency_sign(all_invoice_unpaid_amount());?></span> <span class="info-box-number client-hr-invoice">Pending D. Note Payments</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>

</div>
    <div class="row animated fadeInRight">
        <!-- Invoice Paid Amount -->
        <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/invoices/all_invoices_paid/');?>">

                <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>

                    <div class="info-box-content"> <span class="info-box-number"><?php echo $this->Xin_model->currency_sign(all_invoice_paid_amount());?></span> <span class="info-box-number client-hr-invoice">Debit Note Payments</span> </div>

                    <!-- /.info-box-content -->

                </div>

            </a>

            <!-- /.info-box -->

        </div>
<!-- Organization -->
<div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/Hospital/organizations/');?>">
<div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-red"><i class="fa fa-sitemap"></i></span>
<div class="info-box-content"> <span class="info-box-number"><?php echo total_organization();?></span> <span class="info-box-number client-hr-invoice"><?php echo $this->lang->line('xin_total_organization');?></span> </div>
<!-- /.info-box-content -->
</div>
</a>
<!-- /.info-box -->
</div>
<!-- Icon box -->
    <!-- Total Invoices About to Expire -->
    <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/invoices/invoice_expire/');?>">
        <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-black"><i class="fa fa-warning"></i></span>
        <div class="info-box-content"> <span class="info-box-number"><?php echo total_invoice_expire();?></span> <span class="info-box-number client-hr-invoice"><?php echo $this->lang->line('xin_total_invoice_expire');?></span> </div>
        <!-- /.info-box-content -->
    </div>
</a>
<!-- /.info-box -->
</div>
    </div>
    <div class="row animated fadeInRight">

<!-- Total Hospital -->
    <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/Hospital/');?>">
        <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-primary"><i class="fa fa-h-square"></i></span>
        <div class="info-box-content"> <span class="info-box-number"><?php echo total_hospital();?></span> <span class="info-box-number client-hr-invoice"><?php echo $this->lang->line('xin_total_hospital');?></span> </div>
        <!-- /.info-box-content -->
    </div>
</a>
<!-- /.info-box -->
</div>

<!-- Icon box -->
    <!-- Total Invoices -->
    <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/invoices/');?>">
        <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-yellow"><i class="fa fa-credit-card"></i></span>
        <div class="info-box-content"> <span class="info-box-number"><?php echo total_invoices();?></span> <span class="info-box-number client-hr-invoice"><?php echo $this->lang->line('xin_total_invoice');?></span> </div>
        <!-- /.info-box-content -->
    </div>
</a>
<!-- /.info-box -->
</div>
<!-- Unpaid Invoices -->
<div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/invoices/');?>">
<div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-red"><i class="fa fa-money"></i></span>
<div class="info-box-content"> <span class="info-box-number"><?php echo all_invoice_unpaid_count();?></span> <span class="info-box-number client-hr-invoice"><?php echo $this->lang->line('xin_invoice_unpaid_client');?></span> </div>
<!-- /.info-box-content -->
</div>
</a>
<!-- /.info-box -->
</div>
<!-- Total Auth Code Approved -->
<div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/Hospital/diagnose_hospital_clients_requests');?>">
<div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-navy"><i class="fa fa-thermometer-full"></i></span>
<div class="info-box-content"> <span class="info-box-number"><?php echo total_auth_code('1');?></span> <span class="info-box-number client-hr-invoice">Auth Codes Approved</span> </div>
<!-- /.info-box-content -->
</div>
</a>
<!-- /.info-box -->
</div>

<!-- Total Auth Code Pending -->
<div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/Hospital/diagnose_hospital_clients_requests');?>">
<div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-yellow"><i class="fa fa-thermometer "></i></span>
<div class="info-box-content"> <span class="info-box-number"><?php echo total_auth_code();?></span> <span class="info-box-number client-hr-invoice">Auth Code Pending</span> </div>
<!-- /.info-box-content -->
</div>
</a>
<!-- /.info-box -->
</div>

<!-- Total Bills Approved -->
<div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/Hospital/diagnose_hospital_clients_bill_requests');?>">
<div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-green"><i class="fa fa-plus-circle"></i></span>
<div class="info-box-content"> <span class="info-box-number"><?php echo total_bill('1');?></span> <span class="info-box-number client-hr-invoice">Bills Approved</span> </div>
<!-- /.info-box-content -->
</div>
</a>
<!-- /.info-box -->
</div>

<!-- Total Bills Pending -->
<div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/Hospital/diagnose_hospital_clients_bill_requests');?>">
<div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-purple"><i class="fa fa-plus-square-o"></i></span>
<div class="info-box-content"> <span class="info-box-number"><?php echo total_bill();?></span> <span class="info-box-number client-hr-invoice">Bills Pending</span> </div>
<!-- /.info-box-content -->
</div>
</a>
<!-- /.info-box -->
</div>

<!-- Total Enrollee Requests -->
<div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/Hospital/new_enrollee_requests');?>">
<div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-blue"><i class="fa fa-user-plus"></i></span>
<div class="info-box-content"> <span class="info-box-number"><?php echo total_enrollee_request();?></span> <span class="info-box-number client-hr-invoice">New Enrollee Requests</span> </div>
<!-- /.info-box-content -->
</div>
</a>
<!-- /.info-box -->
</div>
        <!-- Bill payment -->
        <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('admin/accounting/all_bills_payments');?>">
                <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-purple"><i class="fa fa-cc-mastercard"></i></span>
                    <div class="info-box-content"> <span class="info-box-number"><?php echo total_bill_payments();?></span> <span class="info-box-number client-hr-invoice"><?php echo $this->lang->line('xin_total_bill_payment');?></span> </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>

    </div>
<?php /*?><?php if($this->session->flashdata('expire_official_document')):?>
<?php $company_license = $this->Xin_model->company_license_expiry();?>
<?php foreach($company_license as $clicense):?>
<div class="alert alert-warning alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-warning"></i> <?php echo $this->lang->line('xin_title_alert');?></h4>
<?php echo $this->lang->line('xin_title_license');?> <?php echo $clicense->license_name;?> <?php echo $this->lang->line('xin_title_is_going_to_expire_soon');?></div>
<?php endforeach;?>
<?php $company_license_exp = $this->Xin_model->company_license_expired();?>
<?php foreach($company_license_exp as $clicense_exp):?>
<div class="alert alert-warning alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-warning"></i> <?php echo $this->lang->line('xin_title_alert');?></h4>
<?php echo $this->lang->line('xin_title_license');?> <?php echo $clicense_exp->license_name;?> <?php echo $this->lang->line('xin_title_is_expired');?> </div>
<?php endforeach;?>
<?php endif;?><?php */?>
<?php if($theme[0]->statistics_cards=='4' || $theme[0]->statistics_cards=='8' || $theme[0]->statistics_cards=='12'){?>
<div class="row <?php echo $get_animate;?>">
<div class="col-xl-3 col-md-3">
<div class="card mini-stat bg-primary">
<div class="card-body mini-stat-img">
<div class="mini-stat-icon"> <i class="fa fa-user float-right"></i> </div>
<div class="text-white">
<h6 class="text-uppercase mb-3"><?php echo $this->lang->line('dashboard_employees');?></h6>
<h4 class="mb-4"><a class="text-card-muted" href="<?php echo site_url('admin/employees');?>"><?php echo $this->Employees_model->get_total_employees();?></a></h4>
<span class="badge badge-info"> <?php echo $this->lang->line('xin_employees_active');?> <?php echo active_employees();?> </span><span class="ml-2"> <span class="badge bg-red"> <?php echo $this->lang->line('xin_employees_inactive');?> <?php echo inactive_employees();?> </span></span> </div>
</div>
</div>
</div>
<div class="col-xl-3 col-md-3">
<div class="card mini-stat bg-green">
<div class="card-body mini-stat-img">
<div class="mini-stat-icon"> <i class="fa fa-lock float-right"></i> </div>
<div class="text-white">
<h6 class="text-uppercase mb-3"><?php echo $this->lang->line('xin_roles');?></h6>
<h4 class="mb-4"><?php echo $this->lang->line('xin_permission');?></h4>
<a class="text-card-muted" href="<?php echo site_url('admin/roles');?>"><?php echo $this->lang->line('left_set_roles');?></a> </div>
</div>
</div>
</div>
<div class="col-xl-3 col-md-3">
<div class="card mini-stat bg-purple">
<div class="card-body mini-stat-img">
<div class="mini-stat-icon"> <i class="fa fa-calendar float-right"></i> </div>
<div class="text-white">
<h6 class="text-uppercase mb-3"><?php echo $this->lang->line('left_leave');?></h6>
<h4 class="mb-4"><?php echo $this->lang->line('xin_performance_management');?></h4>
<a class="text-card-muted" href="<?php echo site_url('admin/timesheet/leave');?>"><?php echo $this->lang->line('xin_hr_view_applications');?></a> </div>
</div>
</div>
</div>
<div class="col-xl-3 col-md-3">
<div class="card mini-stat bg-red">
<div class="card-body mini-stat-img">
<div class="mini-stat-icon"> <i class="fa fa-cog float-right"></i> </div>
<div class="text-white">
<h6 class="text-uppercase mb-3"><?php echo $this->lang->line('xin_configure_hr');?></h6>
<h4 class="mb-4"><?php echo $this->lang->line('left_settings');?></h4>
<a class="text-card-muted" href="<?php echo site_url('admin/settings');?>"><?php echo $this->lang->line('header_configuration');?></a> </div>
</div>
</div>
</div>
</div>
<?php } ?>

<?php if($system[0]->module_inquiry=='true'){?>
<div class="row <?php echo $get_animate;?>">
<div class="col-md-4 col-12">
<div class="box box-body hr-mini-state hrsalle-mini-stat">
<a class="text-card-muted" href="<?php echo site_url('admin/tickets');?>"><div class="flexbox"> <span class="fa fa-ticket text-primary font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo total_tickets();?></span> </div>
<div class="text-right"><?php echo $this->lang->line('xin_hr_total_tickets');?></div></a>
</div>
</div>
<div class="col-md-4 col-12">
<div class="box box-body hr-mini-state hrsalle-mini-stat">
<a class="text-card-muted" href="<?php echo site_url('admin/tickets');?>"><div class="flexbox"> <span class="fa fa-server text-danger font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo total_open_tickets();?></span> </div>
<div class="text-right"><?php echo $this->lang->line('xin_hr_total_open_tickets');?></div></a>
</div>
</div>
<div class="col-md-4 col-12">
<div class="box box-body hr-mini-state hrsalle-mini-stat">
<a class="text-card-muted" href="<?php echo site_url('admin/tickets');?>"><div class="flexbox"> <span class="ion ion-thumbsup text-success font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo total_closed_tickets();?></span> </div>
<div class="text-right"><?php echo $this->lang->line('xin_hr_total_closed_tickets');?></div></a>
</div>
</div>
</div>
<?php } ?>
<?php
$current_month = date('Y-m-d');
$working = $this->Xin_model->current_month_day_attendance($current_month);
$query = $this->Xin_model->all_employees_status();
$total = $query->num_rows();
// absent
$abs = $total - $working;
?>
<?php
$emp_abs = $abs / $total * 100;
$emp_work = $working / $total * 100;
?>
<?php
$emp_abs = $abs / $total * 100;
$emp_work = $working / $total * 100;
?>
<div class="row row-card-no-pd mt--2 <?php echo $get_animate;?>">
<div class="col-12 col-sm-6 col-md-3">
<div class="card">
<div class="card-body">
<div class="d-flex justify-content-between">
    <div>
        <h5><span><?php echo $this->lang->line('xin_hrsale_absent_today');?></span></h5>
        <p class="text-muted"><?php echo $this->lang->line('xin_absent');?></p>
    </div>
    <h3 class="text-info fw-bold"><?php echo $abs;?></h3>
</div>
<div class="progress progress-sm">
    <div class="progress-bar progress-bar-info w-75" role="progressbar" aria-valuenow="<?php echo $this->Xin_model->set_percentage($emp_abs);?>" aria-valuemin="8" aria-valuemax="100" style="width: <?php echo $this->Xin_model->set_percentage($emp_abs);?>%"></div>
</div>
<div class="d-flex justify-content-between mt-2">
    <p class="text-muted mb-0"><?php echo $this->lang->line('xin_hrsale_absent_status');?></p>
    <p class="text-muted mb-0"><?php echo $this->Xin_model->set_percentage($emp_abs);?>%</p>
</div>
</div>
</div>
</div>
<div class="col-12 col-sm-6 col-md-3">
<div class="card">
<div class="card-body">
<div class="d-flex justify-content-between">
    <div>
        <h5><span><?php echo $this->lang->line('xin_hrsale_present_today');?></span></h5>
        <p class="text-muted"><?php echo $this->lang->line('xin_emp_working');?></p>
    </div>
    <h3 class="text-info fw-bold"><?php echo $working;?></h3>
</div>
<div class="progress progress-sm">
    <div class="progress-bar progress-bar-info w-75" role="progressbar" aria-valuenow="<?php echo $this->Xin_model->set_percentage($emp_work);?>" aria-valuemin="8" aria-valuemax="100" style="width: <?php echo $this->Xin_model->set_percentage($emp_work);?>%"></div>
</div>
<div class="d-flex justify-content-between mt-2">
    <p class="text-muted mb-0"><?php echo $this->lang->line('xin_hrsale_present_status');?></p>
    <p class="text-muted mb-0"><?php echo $this->Xin_model->set_percentage($emp_work);?>%</p>
</div>
</div>
</div>
</div>
<div class="col-12 col-sm-6 col-md-3">
<div class="card">
<div class="card-body">
<div class="d-flex justify-content-between">
    <div>
        <h5><span><?php echo $this->lang->line('dashboard_projects');?></span></h5>
        <p class="text-muted"><?php echo $this->lang->line('xin_hrsale_project_status');?></p>
    </div>
    <?php $completed_proj = $this->Project_model->complete_projects();?>
    <?php $proj = $this->Xin_model->get_all_projects();
    if($proj < 1) {
    $proj_percnt = 0;
    } else {
    $proj_percnt = $completed_proj / $proj * 100;
    }
    ?>
    <h3 class="text-info fw-bold"><a class="text-card-mduted" href="<?php echo site_url('admin/project');?>"><?php echo $this->Xin_model->get_all_projects();?></a></h3>
</div>
<div class="progress progress-sm">
    <div class="progress-bar progress-bar-info w-75" role="progressbar" aria-valuenow="<?php echo $this->Xin_model->set_percentage($proj_percnt);?>" aria-valuemin="8" aria-valuemax="100" style="width: <?php echo $this->Xin_model->set_percentage($proj_percnt);?>%"></div>
</div>
<div class="d-flex justify-content-between mt-2">
    <p class="text-muted mb-0"><?php echo $this->lang->line('xin_completed');?></p>
    <p class="text-muted mb-0"><?php echo $this->Xin_model->set_percentage($proj_percnt);?>%</p>
</div>
</div>
</div>
</div>
<div class="col-12 col-sm-6 col-md-3">
<div class="card">
<div class="card-body">
<div class="d-flex justify-content-between">
    <div>
        <h5><span><?php echo $this->lang->line('xin_tasks');?></span></h5>
        <p class="text-muted"><?php echo $this->lang->line('xin_hrsale_task_status');?></p>
    </div>
    <?php $completed_tasks = completed_tasks();?>
    <?php $task_all = $this->Xin_model->get_all_tasks();
    if($task_all < 1) {
    $task_percnt = 0;
    } else {
    $task_percnt = $completed_tasks / $task_all * 100;
    }
    ?>
    <h3 class="text-info fw-bold"><a class="text-card-mduted" href="<?php echo site_url('admin/timesheet/tasks');?>"><?php echo $this->Xin_model->get_all_tasks();?></a></h3>
</div>
<div class="progress progress-sm">
    <div class="progress-bar progress-bar-info w-75" role="progressbar" aria-valuenow="<?php echo $this->Xin_model->set_percentage($task_percnt);?>" aria-valuemin="8" aria-valuemax="100" style="width: <?php echo $this->Xin_model->set_percentage($task_percnt);?>%"></div>
</div>
<div class="d-flex justify-content-between mt-2">
    <p class="text-muted mb-0"><?php echo $this->lang->line('xin_completed');?></p>
    <p class="text-muted mb-0"><?php echo $this->Xin_model->set_percentage($task_percnt);?>%</p>
</div>
</div>
</div>
</div>
</div>
<div class="row <?php echo $get_animate;?>">
<div class="col-sm-6 col-lg-3">
<div class="card p-3">
<div class="d-flex align-items-center">
<span class="stamp-hrsale-4 stamp-hrsale-md bg-hrsale-secondary mr-3">
    <i class="fa fa-user"></i>
</span>
<div>
    <h5 class="mb-1"><b><a href="<?php echo site_url('admin/employees');?>"><?php echo $this->Employees_model->get_total_employees();?> <small><?php echo $this->lang->line('xin_people');?></small></a></b></h5>
    <small class="text-muted"><span class="badge badge-info"> <?php echo $this->lang->line('xin_employees_active');?> <?php echo active_employees();?> </span><span class="ml-2"> <span class="badge bg-red"> <?php echo $this->lang->line('xin_employees_inactive');?> <?php echo inactive_employees();?> </span></span></small>
</div>
</div>
</div>
</div>
<div class="col-sm-6 col-lg-3">
<div class="card p-3">
<div class="d-flex align-items-center">
<span class="stamp-hrsale-4 stamp-hrsale-md bg-hrsale-success-4 mr-3">
    <i class="fa fa-lock"></i>
</span>
<div>
    <h5 class="mb-1"><b><a href="<?php echo site_url('admin/roles');?>"> <?php echo $this->lang->line('xin_roles');?> <small><?php echo $this->lang->line('xin_permission');?></small></a></b></h5>
    <small class="text-muted"><?php echo $this->lang->line('left_set_roles');?></small>
</div>
</div>
</div>
</div>
<div class="col-sm-6 col-lg-3">
<div class="card p-3">
<div class="d-flex align-items-center">
<span class="stamp-hrsale-4 stamp-hrsale-md bg-hrsale-danger-4 mr-3">
    <i class="fa fa-calendar"></i>
</span>
<div>
    <h5 class="mb-1"><b><a href="<?php echo site_url('admin/timesheet/leave');?>"> <?php echo $this->lang->line('left_leave');?> <small><?php echo $this->lang->line('xin_performance_management');?></small></a></b></h5>
    <small class="text-muted"><?php echo $this->lang->line('xin_hr_view_applications');?></small>
</div>
</div>
</div>
</div>
<div class="col-sm-6 col-lg-3">
<div class="card p-3">
<div class="d-flex align-items-center">
<span class="stamp-hrsale-4 stamp-hrsale-md bg-hrsale-warning-4 mr-3">
    <i class="fa fa-cog"></i>
</span>
<div>
    <h5 class="mb-1"><b><a href="<?php echo site_url('admin/settings');?>"> <?php echo $this->lang->line('xin_configure_hr');?> <small><?php echo $this->lang->line('left_settings');?></small></a></b></h5>
    <small class="text-muted"><?php echo $this->lang->line('header_configuration');?></small>
</div>
</div>
</div>
</div>
</div>
<div class="row <?php echo $get_animate;?>">
<div class="col-md-6">
<div class="box">
<div class="box-header with-border">
<h3 class="box-title"><?php echo $this->lang->line('xin_employee_department_txt');?></h3>
</div>
<div class="box-body">
<div class="box-block">
    <div class="col-md-7">
        <div class="overflow-scrolls" style="overflow:auto; height:200px;">
            <div class="table-responsive">
                <table class="table mb-0 table-dashboard">
                    <tbody>
                        <?php $c_color = array('#00A5A8','#FF4558','#16D39A','#8A2BE2','#D2691E','#6495ED','#DC143C','#006400','#556B2F','#9932CC','#00A5A8','#FF4558','#16D39A','#8A2BE2','#D2691E','#6495ED','#DC143C','#006400','#556B2F','#9932CC','#00A5A8','#FF4558','#16D39A','#8A2BE2','#D2691E','#6495ED','#DC143C','#006400','#556B2F','#9932CC','#00A5A8','#FF4558','#16D39A','#8A2BE2','#D2691E','#6495ED','#DC143C','#006400','#556B2F','#9932CC','#00A5A8','#FF4558','#16D39A','#8A2BE2','#D2691E','#6495ED','#DC143C','#006400','#556B2F','#9932CC','#00A5A8','#FF4558','#16D39A','#8A2BE2','#D2691E','#6495ED','#DC143C','#006400','#556B2F','#9932CC','#00A5A8','#FF4558','#16D39A','#8A2BE2','#D2691E','#6495ED','#DC143C','#006400','#556B2F','#9932CC','#00A5A8','#FF4558','#16D39A','#8A2BE2','#D2691E','#6495ED','#DC143C','#006400','#556B2F','#9932CC');?>
                        <?php $j=0;foreach($this->Department_model->all_departments() as $department) { ?>
                        <?php
                        $condition = "department_id =" . "'" . $department->department_id . "'";
                        $this->db->select('*');
                        $this->db->from('xin_employees');
                        $this->db->where($condition);
                        $query = $this->db->get();
                        // check if department available
                        if ($query->num_rows() > 0) {
                        ?>
                        <tr>
                            <td><div style="width:4px;border:5px solid <?php echo $c_color[$j];?>;"></div></td>
                            <td><?php echo htmlspecialchars_decode($department->department_name);?> (<?php echo $query->num_rows();?>)</td>
                        </tr>
                        <?php $j++; } ?>
                        <?php  } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <canvas id="employee_department" height="200" width="" style="display: block;  height: 200px;"></canvas>
    </div>
</div>
</div>
</div>
</div>
<div class="col-md-6">
<div class="box">
<div class="box-header with-border">
<h3 class="box-title"><?php echo $this->lang->line('xin_employee_designation_txt');?></h3>
</div>
<div class="box-body">
<div class="box-block">
    <div class="col-md-7">
        <div class="overflow-scrolls" style="overflow:auto; height:200px;">
            <div class="table-responsive">
                <table class="table mb-0 table-dashboard">
                    <tbody>
                        <?php $c_color2 = array('#9932CC','#556B2F','#16D39A','#DC143C','#D2691E','#8A2BE2','#FF976A','#FF4558','#00A5A8','#6495ED','#9932CC','#556B2F','#16D39A','#DC143C','#D2691E','#8A2BE2','#FF976A','#FF4558','#00A5A8','#6495ED','#9932CC','#556B2F','#16D39A','#DC143C','#D2691E','#8A2BE2','#FF976A','#FF4558','#00A5A8','#6495ED','#9932CC','#556B2F','#16D39A','#DC143C','#D2691E','#8A2BE2','#FF976A','#FF4558','#00A5A8','#6495ED','#9932CC','#556B2F','#16D39A','#DC143C','#D2691E','#8A2BE2','#FF976A','#FF4558','#00A5A8','#6495ED','#9932CC','#556B2F','#16D39A','#DC143C','#D2691E','#8A2BE2','#FF976A','#FF4558','#00A5A8','#6495ED','#9932CC','#556B2F','#16D39A','#DC143C','#D2691E','#8A2BE2','#FF976A','#FF4558','#00A5A8','#6495ED','#9932CC','#556B2F','#16D39A','#DC143C','#D2691E','#8A2BE2','#FF976A','#FF4558','#00A5A8','#6495ED');?>
                        <?php $k=0;foreach($this->Designation_model->all_designations() as $designation) { ?>
                        <?php
                        $condition1 = "designation_id =" . "'" . $designation->designation_id . "'";
                        $this->db->select('*');
                        $this->db->from('xin_employees');
                        $this->db->where($condition1);
                        $query1 = $this->db->get();
                        // check if department available
                        if ($query1->num_rows() > 0) {
                        ?>
                        <tr>
                            <td><div style="width:4px;border:5px solid <?php echo $c_color2[$k];?>;"></div></td>
                            <td><?php echo htmlspecialchars_decode($designation->designation_name);?> (<?php echo $query1->num_rows();?>)</td>
                        </tr>
                        <?php $k++; } ?>
                        <?php  } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <canvas id="employee_designation" height="200" width="" style="display: block; height: 200px;"></canvas>
    </div>
</div>
</div>
</div>
</div>
</div>
<?php if($theme[0]->dashboard_calendar == 'true'):?>
<?php $this->load->view('admin/calendar/calendar_hr');?>
<?php endif; ?>