<?php

/* Subscription view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php if(in_array('341',$role_resources_ids)) {?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>


<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>


<?php } ?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"> Filter Results By Selecting a Hospital and Organization</h3>

    </div>

    <div class="<?php echo $get_animate;?>" style="">

      <div class="box-body">

            <?php echo form_open('admin/ClientAccount/viewall_new');?>

            <div class="row">
              <div class="col-md-3">
                  <select name="hospital_name" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="Choose Hospital">

                    <option value=""></option>

                    <?php 
                      foreach($all_hospital as $hospital) { 
					  $slect = '';
					  if($hospital->hospital_id == $hospital_id){
						  $slect = ' selected ';
					  }
                    ?>

                        <option value="<?php echo $hospital->hospital_id;?>" <?php echo $slect;?>><?php echo $hospital->hospital_name;?></option>

                    <?php } ?>

                  </select>
              </div>
			  <div class="col-md-3">
                  <select name="organization_name" id="select2-demo-61" class="form-control select2" data-plugin="select_hrm" data-placeholder="Choose Organization">

                    <option value=""></option>

                    <?php 
                      foreach($all_organization as $organization) {
						$slect1 = '';
					  if($organization['id'] == $organization_id){
						  $slect1 = ' selected ';
					  }						  
                    ?>

                        <option value="<?php echo $organization['id'];?>" <?php echo $slect1;?>><?php echo $organization['name'];?></option>

                    <?php } ?>

                  </select>
              </div>
              <div class="col-md-2">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php if($from_date !=""){echo $from_date;}else{echo date('Y-m-d');}?>">

                </div>

              </div>

              <div class="col-md-2">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php if($to_date !=""){echo $to_date;}else{echo date('Y-m-d');}?>">

                </div>

              </div>

              <div class="col-md-2">

                <div class="form-group">

                  <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

                </div>

              </div>

            </div>

            <?php echo form_close(); ?> 
        </div>

    </div>

  </div>

</div>
<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

        <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

            <thead>

                <tr> 
                    <th><?php echo $this->lang->line('xin_action');?></th>

                    <th>First</th>
                    <th>Last</th>
                    <th>Other</th>
                    <th>Address</th>
                    <th>Hospital</th>
                    <th>Organization</th>
                    <th>Location</th>
                    <th>Account</th>
                </tr>

                <tbody> 
                    <?php   
                    if(!empty($clients))
                    { 
                        foreach ($clients as $key => $value)
                        {              
                             
                         

                            $ci=& get_instance();
                            $ci->load->model('Training_model'); 


                            $diseases = explode(',', $value->diseases);
                            $disease_list = '';
                            foreach ($diseases as $disease) {
                               $disease_list .= '<p>'.ucfirst( $disease).'</p>';
                            }


                            // $bands    = $ci->Training_model->getAll2('xin_bands', ' band_id='. $value->band_id.' ');

                            $location = $ci->Training_model->getAll2('xin_location', ' location_id= "'. $value->state.'" ');
                            
                                
                            ?>  
                            <tr>
                                <td align="center">
                                    <!-- <button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light edit_detect"  ><span class="fa fa-pencil"></span></button> -->


                                    <!--<span data-toggle="tooltip" data-placement="top" title="" data-original-title="View Details"><a href="<?php echo base_url(); ?>admin/ClientAccount/detailAccount/<?php echo $value->client_id; ?>"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"><span class="fa fa-arrow-circle-right"></span></button></a></span>-->

                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Details"><a href="<?php echo base_url(); ?>admin/ClientAccount/editAccountnew/<?php echo $value->client_id; ?>"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"><span class="fa fa-pencil"></span></button></a></span>

                                    <!--<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                        <button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal2"   ><span class="fa fa-trash"><a href="<?php echo base_url(); ?>admin/ClientAccount/deleteAccount/<?php echo $value->client_id; ?>"></span></button>
                                    </span> --> 
                                </td>

                                <td><?php echo isset($value->name) ? ucfirst($value->name) : ''; ?> </td>

                                <td><?php echo isset($value->last_name) ? ucfirst($value->last_name) : ''; ?> </td>

                                <td><?php echo isset($value->other_name) ? $value->other_name : ''; ?> </td>

                                <td><?php echo isset($value->address_1) ? $value->address_1 : ''; ?></td>
								<td><?php echo isset($value->hospital_id) ? $this->Clients_model->get_hospital_info_name($value->hospital_id) : '';  ?></td>
								<td><?php echo isset($value->company_name) ? $this->Clients_model->get_organization_info_name($value->company_name) : ''; ?></td>
                                <td><?php echo isset($location[0]->location_name) ? $location[0]->location_name : ''; ?></td>

                                <td><?php echo isset($value->ind_family) ? ucfirst($value->ind_family) : ''; ?></td> 
                               
                            </tr> 

                            <?php 
                        }
                    }
                    ?>
                </tbody>

            </thead>

        </table>
        

        <?php echo $pagination; ?>

    </div>

  </div>

</div>
 

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
            dom: 'lBfrtip',
            "buttons": ['csv', 'excel', 'pdf', 'print'],
            "paging":   false,
            "info":     false,
            "ordering": false,
        });
 
    }, false);

     
</script>
 






 