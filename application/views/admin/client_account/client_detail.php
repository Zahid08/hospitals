<?php

/* Gospital Details view

*/

$ncolmd = 4;
$colmd = 4;

?>

<?php $session = $this->session->userdata('username');?>

<?php $system = $this->Xin_model->read_setting_info(1);?>

<?php //$default_currency = $this->Xin_model->read_currency_con_info($system[0]->default_currency_id);?>

<?php

// $eid = $this->uri->segment(4);

// $eresult = $this->Employees_model->read_employee_information($eid);

?>

<?php

$ar_sc = explode('- ',$system[0]->default_currency_symbol);

$sc_show = $ar_sc[1];

?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>  
 
<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<div class="row">

    <div class="col-md-12">

        <div class="nav-tabs-custom mb-4">

            <ul class="nav nav-tabs">

                <li class="nav-item active"> <a class="nav-link active show" data-toggle="tab" href="#user_basic_info">Principal Enrollee Information</a> </li>

                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#xin_profile_picture">Dependants Information </a> </li> 

            </ul>

           

            <div class="tab-content">
                

                <div class="tab-pane <?php echo $get_animate;?> active" id="xin_general">

                <div class="card-body">

                    <div class="card overflow-hidden">

                        <div class="row no-gutters row-bordered row-border-light"> 
                            <div class="col-md-12">

                                <div class="tab-content">

                                    <div class="tab-pane active current-tab  " id="user_basic_info">

                                        

                                        <div class="box-body">

                                            
                                            <div class="bg-white">


                                                <div>
                                                    <div class="row"> 
                                                        <div class="col-md-12">
                                                            <?php if ($this->session->flashdata('success')): ?>
                                                                <div class="alert alert-success  alert-dismissible">
                                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                                    <?php echo $this->session->flashdata('success'); ?>
                                                                </div>
                                                            <?php endif ?>

                                                             <?php if ($this->session->flashdata('error')): ?>
                                                                <div class="alert alert-warning  alert-dismissible">
                                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                                    <?php echo $this->session->flashdata('error'); ?>
                                                                </div>
                                                            <?php endif ?>
                                                        </div>


                                                        <div class="col-md-2 animated fadeInRight">  

                                                            <div class="box box-primary">

                                                                <div class="box-body box-profile"> 
                                                                    <a class="" href="#" data-toggle="tab" aria-expanded="true"> 
                                                                        

                                                                        <?php if (isset($clients[0]->client_profile ) and !empty($clients[0]->client_profile    )){ ?>
                                                                            <?php
                                                                                echo '<img    src=" '.base_url().'uploads/clients/'.$clients[0]->client_profile   .' "     class="profile-user-img img-responsive img-circle"      >';
                                                                            ?>
                                                                        <?php } else{ ?> 

                                                                            <?php
                                                                                echo '<img    src=" '.base_url().'uploads/clients/default_female.jpg"     class="profile-user-img img-responsive img-circle"      >';
                                                                            ?>

                                                                        <?php } ?> 
                                                                    </a>

                                                                    <h3 class="profile-username text-center">Profile</h3> 
                                                                </div> 
                                                            </div> 
                                                        </div> 




                                                        <div class="col-md-10">

                                                            <div class="box-header with-border"  style="    padding-left: 0px !important;">

                                                                <h3 class="box-title"> <?php echo $this->lang->line('xin_e_details_basic_info');?> </h3>

                                                            </div>

                                                            <div class="row">
                                                                

                                                                <?php 
                                                                    $ci=& get_instance();
                                                                    $ci->load->model('Training_model'); 

                                                                    // $bands    = $ci->Training_model->getAll2('xin_bands', ' band_id='. $all_hospital_detail[0]->band_id.' ');

                                                                    // $location = $ci->Training_model->getAll2('xin_location', ' location_id='. $all_hospital_detail[0]->location_id.' ');

                                                                ?>
                                                                <div class="col-md-4">

                                                                    <div class="form-group">

                                                                        <label for="first_name">First Name - </label>
                                                                        <?php echo $clients[0]->name ?>
                                                                        

                                                                    </div>

                                                                </div>

                                                                <div class="col-md-4">

                                                                    <div class="form-group">

                                                                        <label for="last_name" class="control-label">Last Name - </label>

                                                                        <?php echo isset($clients[0]->last_name) ? $clients[0]->last_name : ''; ?>
                                                                    </div>

                                                                </div>

                                                                <div class="col-md-4">

                                                                    <div class="form-group">

                                                                        <label for="employee_id">Other Name - </label>

                                                                        <?php echo isset($clients[0]->other_name) ? $clients[0]->other_name : ''; ?>

                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <div class="row"> 
                                                                <div class="col-md-4">

                                                                    <div class="form-group">

                                                                        <label for="username">Email - </label>

                                                                        <?php echo isset($clients[0]->email) ? $clients[0]->email : ''; ?>
                                                                    </div> 
                                                                </div>

                                                                <div class="col-md-4">

                                                                    <div class="form-group">

                                                                        <label for="email" class="control-label">Phone Number - </label> 
                                                                        <?php echo isset($clients[0]->contact_number) ? $clients[0]->contact_number : ''; ?>
                                                                    </div> 
                                                                </div>

                                                                <div class="col-md-4"> 
                                                                    <div class="form-group">

                                                                        <label for="email" class="control-label">Date of Birth - </label> 
                                                                        <?php echo isset($clients[0]->dob) ? $clients[0]->dob : ''; ?>
                                                                    </div> 
                                                                </div> 
                                                            </div>

                                                            <hr>

                                                            <div class="box-header with-border" style="    padding-left: 0px !important;">

                                                                <h3 class="box-title"> Personal Information </h3>

                                                            </div>


                                                            <div class="row"> 
                                                                <div class="col-md-4">

                                                                    <div class="form-group">

                                                                        <label for="username">Sex - </label>

                                                                        <?php echo isset($clients[0]->sex) ? ucfirst($clients[0]->sex) : ''; ?>
                                                                    </div> 
                                                                </div>

                                                                <div class="col-md-4">

                                                                    <div class="form-group">

                                                                        <label for="email" class="control-label">Marital Status - </label> 
                                                                        <?php echo isset($clients[0]->marital_status) ? ucfirst($clients[0]->marital_status) : ''; ?>
                                                                    </div> 
                                                                </div>

                                                                <div class="col-md-4"> 
                                                                    <div class="form-group">

                                                                        <label for="email" class="control-label">Account type - </label> 
                                                                        <?php echo isset($clients[0]->ind_family) ? ucfirst($clients[0]->ind_family) : ''; ?>
                                                                    </div> 
                                                                </div>

                                                                
                                                            </div>




                                                            <hr>

                                                            <div class="box-header with-border" style="    padding-left: 0px !important;">

                                                                <h3 class="box-title">Medical History</h3>

                                                            </div>



             
                                                            <div class="row"> 
                                                                <div class="col-md-4">

                                                                    <div class="form-group">

                                                                        <label for="username">Diseases: </label>
                                                                        <?php 

                                                                            $diseases = explode(',', $clients[0]->diseases);
                                                                            $disease_list = '';
                                                                            foreach ($diseases as $disease) {
                                                                               $disease_list .= '<p>'.ucfirst( $disease).'</p>';
                                                                            }


                                                                        ?>
                                                                        <?php echo isset($disease_list) ? $disease_list : ''; ?>
                                                                    </div> 
                                                                </div>

                                                                <div class="col-md-4">

                                                                    <div class="form-group">

                                                                        <label for="email" class="control-label">Disease Comment - </label> 
                                                                        <?php echo isset($clients[0]->disease_comment) ? ucfirst($clients[0]->disease_comment) : ''; ?>
                                                                    </div> 
                                                                </div>

                                                                 
                                                            </div>
                                                                        
                                                        </div>
                                                    </div>
                                                </div>

                                                
 
                                                 

                                            </div> 
                                        </div> 
                                    </div>




                                    <div class="tab-pane" id="xin_profile_picture"  >  
                                        <div class="row"> 
                                            <div class="col-md-12">
                                                <div class="table-responsive"> 
                                                    <table class="datatables-demo table table-striped table-bordered"  id="xin_table_new">
                                                        <thead>
                                                            <tr>
                                                                <th> Action</th>
                                                                <th> First Name</th>
                                                                <th> Last Name</th>
                                                                <th> Other Name</th>
                                                                <th> DOB</th>
                                                                <th> Phone</th>
                                                                <th> Gender</th>
                                                                <th> Relation</th>                  
                                                                <th> Provider</th>
                                                            </tr> 
                                                        </thead>    
                                                        
                                                        <tbody>
                                                        <?php if (isset($dependents_list) and !empty($dependents_list)) { ?>
                                                        	 
                                                       
                                                        <?php foreach ($dependents_list as $key => $value):  
                                                            $ci=& get_instance();
                                                            $ci->load->model('Training_model'); 

                                                            // $client_data    =  $ci->Training_model->getAll2('xin_clients', ' client_id='. $value->client_id.' ');

                                                            // if (isset($client_data[0]->hospital_id) and !empty($client_data[0]->hospital_id)) 
                                                            // {
                                                            //     $current_hospi  =  $ci->Training_model->getAll2('xin_hospital', ' hospital_id='. $client_data[0]->hospital_id.' ');
                                                            // }
 

                                                            $hospital   =  $ci->Training_model->getAll2('xin_hospital', ' hospital_id='. $value->hospital_id.' ');
                                                            
                                                                
                                                            ?>  
                                                            <tr>
                                                                <td> 
                                                                </td>

                                 

                                                                <td><?php echo isset($value->name) ? $value->name : ''; ?> </td>
                                                                <td><?php echo isset($value->last_name) ? $value->last_name : ''; ?> </td>
                                                                <td><?php echo isset($value->other_name) ? $value->other_name : ''; ?> </td>
                                                                <td><?php echo isset($value->dob) ? $value->dob : ''; ?> </td>

                                                                <td><?php echo isset($value->contact_number) ? $value->contact_number : ''; ?> </td>
                                                                <td><?php echo isset($value->sex) ? $value->sex : ''; ?> </td>
                                                                <td><?php echo isset($value->relation) ? $value->relation : ''; ?> </td>
                                                                 

                                                                <td><?php echo isset($hospital[0]->hospital_name) ? $hospital[0]->hospital_name : ''; ?> </td>

                                                              
                                                               
                                                            </tr> 

                             
                                                        <?php endforeach ?>
                                                    <?php	} ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div> 
                                        </div>    
                        			</div>


                        			<div class="tab-pane" id="xin_services"  >

                                        <div class="box-body">

                                            <div class="row no-gutters row-bordered row-border-light">

                                            <div class="col-md-12">

                                            <div class="tab-content">

                                                <div class="tab-pane   active" id="profile-picture">

                                                <div class="box-body pb-2">

                                                   
                                                    <div class="bg-white">

                                                        <div class="row">

                                                            <div class=" ">
                                                                 
                                                                  <h3 class="box-title">Import CSV file only</h3>
                                                                
                                                                
                                                                    <p class="card-text">The first line in downloaded csv file should remain as it is. Please do not change the order of columns in csv file.</p>
                                                                    <p class="card-text">The correct column order is (Service Name, Drug Price) and you must follow the csv file, otherwise you will get an error while importing the csv file.</p>
                                                                    <h6><a href="<?php echo base_url(); ?>/uploads/csv/sample-csv-services.csv" class="btn btn-primary"> <i class="fa fa-download"></i> Download sample File </a></h6>
                                                                 
                                                            </div>
 
                                                            <?php $attributes = array('name' => 'import_attendance', 'id' => 'xin-form', 'autocomplete' => 'off');?>

                                                            <?php $hidden = array('hospital_id' => $all_hospital_detail[0]->hospital_id);?>

                                                            <?php echo form_open_multipart('admin/Hospital/import_hospital_services', $attributes, $hidden);?>

                                                            <div class="row">

                                                              <div class="col-md-4">

                                                                <div class="form-group">

                                                                  <fieldset class="form-group">

                                                                    <label for="logo"><?php echo $this->lang->line('xin_employee_upload_file');?></label>

                                                                    <input type="file" class="form-control-file" id="file" name="file">

                                                                    <small><?php echo $this->lang->line('xin_employee_imp_allowed_size');?></small>

                                                                  </fieldset>

                                                                </div>

                                                              </div>

                                                            </div>

                                                            <div class="mt-1">

                                                              <div class="form-actions box-footer"> <?php echo form_button(array('name' => 'hrsale_form', 'type' => 'submit', 'class' => $this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '.$this->lang->line('xin_save'))); ?> </div>

                                                            </div>

                                                            <?php echo form_close(); ?> 
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="table-responsive"> 
                                                                <table class="datatables-demo table table-striped table-bordered"  id="xin_table_new2">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Action </th>
                                                                            <th>Service Name </th>
                                                                            <th>Price </th>
                                                                        </tr> 
                                                                    </thead>    
                                                                    
                                                                    <tbody>
                                                                    <?php if (isset($xin_services_hospital) and !empty($xin_services_hospital)) { ?>
                                                                    	 
                                                                   
                                                                    <?php foreach ($xin_services_hospital as $key => $value): ?>
                                                                        <tr>
                                                                            <td>
                                                                                <button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light edit_detect2" data-toggle="modal" data-target=".edit-modal-data3" 


                                                                                data-ticket_id2="<?php echo isset($value->id	) ? $value->id	 : ''; ?>"
                                                                                data-service_name="<?php echo isset($value->service_name) ? $value->service_name : ''; ?>"
                                                                                data-service_price="<?php echo isset($value->service_price) ? $value->service_price : ''; ?>"
                                                                                 
                                                                                ><span class="fa fa-pencil"></span></button>


                                                                                 
                                                                                <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                                                                    <button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete2" data-toggle="modal" data-target=".delete-modal3" data-record-id="<?php echo isset($value->id) ? $value->id : ''; ?>"><span class="fa fa-trash"></span></button>
                                                                                </span>  
                                                                            </td>

                                                                            <td><?php echo isset($value->service_name) ? $value->service_name : ''; ?> </td>

                                                                            <td><?php echo isset($value->service_price) ? $value->service_price : ''; ?> </td>

                                                                           
                                                                        </tr> 
                                                                    <?php endforeach ?>
                                                                <?php	} ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="form-action box-footer">   </div>

                                                </div>
                     

                                            	</div>

                                   			</div>

                                			</div>

                                			</div>

                            			</div>
                        			</div>
                 
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                </div> 
         

            </div>

        </div>

    </div>

</div>
 


<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable();
        var xin_table_new2 = $('#xin_table_new2').dataTable();
 
 

    }, false);

     
</script>
 
 



 

 