<?php

/* Subscription view

*/
 
?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php if(in_array('561',$role_resources_ids)) {?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>


<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>

<?php if (in_array('646',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title">Dependant Account Creation</h3>

      <div class="box-tools pull-right"> <a class="text-dark collapsed" data-toggle="collapse" href="#add_form" aria-expanded="false">

        <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span> Create New Dependant Account</button>

        </a> </div>

    </div>

    <div id="add_form" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">

        <?php $attributes = array('name' => 'add_client', 'id' => 'xin-form2', 'autocomplete' => 'off');?>

        <?php $hidden = array('user_id' => $session['user_id']);?>

        <?php echo form_open('admin/ClientAccount/add_dependant', $attributes, $hidden);?>

        <div class="form-body">

        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <label for="company_name">Select Organization</label>                  

                    <select class="form-control custom" name="organization" id="organization" data-plugin="xin_select" data-placeholder="Select Organization">

                        <option value="">Select Organization</option>

                        <?php foreach($all_organizations as $organization) {?>

                        <option value="<?php echo $organization->id;?>"> <?php echo $organization->name;?></option>

                        <?php } ?>

                    </select> 
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="company_name">Select Principal</label>

                    <select class="form-control custom" name="principal" data-plugin="xin_select" data-placeholder="Select Principal" id="principal">

                        <option value="">Select Principal</option>

                    </select> 
                </div>
            </div>

            <div class="col-md-6">

                <div class="form-group">

                    <label for="client_name">First Name</label>

                    <input class="form-control" placeholder="First Name" name="name" type="text">

                </div>
            </div>

            <div class="col-md-6">

                <div class="form-group">

                    <label for="client_name">Last Name</label>

                    <input class="form-control" placeholder="Enrollee Name" name="last_name" type="text">

                </div>
            </div>

            <div class="col-md-6">

                <div class="form-group">

                    <label for="client_name">Other Name</label>

                    <input class="form-control" placeholder="Enrollee Name" name="other_name" type="text">

                </div>
            </div>
	        <div class="col-md-6 ">

                <div class="form-group">

                	<label for="last_name">Date Of Birth</label>

                    <input type="text" class="form-control date" placeholder="Select a date" required="" name="dob">

                </div>

            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="contact_number">Phone Number</label>

                    <input class="form-control" placeholder="Phone Number" name="contact_number" type="text">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="subscription_ids">Sex</label> 

                    <select   class="form-control" name="sex" data-plugin="xin_select" data-placeholder="Select Gender">

                        <option value="">Select Gender</option> 

                        <option value="M">Male</option>
                        <option value="F">Female</option>
 
                    </select> 
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="subscription_ids">Relation</label> 

                    <select   class="form-control" name="relation" data-plugin="xin_select" data-placeholder="Select Relation">

                        <option value="">Select Relation</option> 

                        <option value="spouse">Spouse</option>
                        <option value="son">Son</option>
                        <option value="daughter">Daughter</option>
 
                    </select> 
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="company_name">Select Providers</label>

                    <select class="form-control custom" name="hospital" data-plugin="xin_select" data-placeholder="Select Providers" id="provider">

                        <option value="">Select Providers</option>

                    </select> 
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="company_name">Select Location</label>

                    <select class="form-control" name="location" data-plugin="xin_select" data-placeholder="Select Location">

                        <option value="">Select Location</option>
                        <?php foreach ($all_locations as $location): ?>
		                        <option value="<?= $location->location_id ?>"><?= $location->location_name; ?></option>                    	
                        <?php endforeach ?>

                    </select> 
                </div>
            </div>

            <div class="col-md-6">

                <fieldset class="form-group">

                    <label for="logo"><?php echo $this->lang->line('xin_project_client_photo');?></label>

                    <input type="file" class="form-control-file" id="client_photo" name="client_photo">

                    <small><?php echo $this->lang->line('xin_company_file_type');?></small>

                </fieldset>

            </div>
            <div class="col-md-6">

                <div class="form-group">

                    <label for="client_name">Address</label>

                    <textarea name="address" cols="10" rows="3" class="form-control"></textarea>

                </div>
            </div> 
        </div>
 
        </div>

        <div class="form-actions box-footer">

            <button type="submit" class="btn btn-primary"> <i class="fa fa-check-square-o"></i> Create Account </button>

        </div>

        <?php echo form_close(); ?> </div>

    </div>

  </div>

</div>
<?php endif ?>

<?php } ?>

<?php if (in_array('647',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
    <div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

        <h3 class="box-title">Bulk Dependant Upload</h3>

        <div class="box-tools pull-right"> 
            <a class="text-dark collapsed" data-toggle="collapse" href="#upload" aria-expanded="false">

                <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span> Open</button>

            </a> 
        </div>

    </div>

    <div id="upload" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">

        <a href="<?php echo base_url(); ?>/uploads/csv/sample-csv-dependant.csv" class="btn btn-primary"> <i class="fa fa-download"></i> Download sample File </a> 

        <?php $attributes = array('name' => 'import_attendance', 'id' => 'xin-forms', 'autocomplete' => 'off');?>

            <?php echo form_open_multipart('admin/ClientAccount/import_dependant', $attributes);?>

            <div class="row">
                <?php if ($this->session->flashdata('success')): ?>
                    <div class="alert alert-success  alert-dismissible" style="margin: 20px 12px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif ?>

                 <?php if ($this->session->flashdata('error')): ?>
                    <div class="alert alert-warning  alert-dismissible" style="margin: 20px 12px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif ?>

                <div class="col-md-4 form-group"> 

                    <fieldset class="form-group">

                        <label for="logo"><?php echo $this->lang->line('xin_employee_upload_file');?></label>

                        <input type="file" class="form-control-file" id="file" name="file">

                        <small><?php echo $this->lang->line('xin_employee_imp_allowed_size');?></small>

                    </fieldset> 
                </div>

            </div>

            <div class="mt-1">

              <div class="form-actions box-footer"> <?php echo form_button(array( 'type' => 'submit', 'class' => $this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '.$this->lang->line('xin_save'))); ?> </div>

            </div>

            <?php echo form_close(); ?>
    </div>

    </div>

  </div>

</div>    

<?php endif ?>
<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"> Filter Results By Selecting a Principal Enrollee</h3>

    </div>

    <div class="<?php echo $get_animate;?>" style="">

      <div class="box-body">

            <?php echo form_open('admin/ClientAccount/dependant');?>

            <div class="row">
              <div class="col-md-3">
                  <select name="client_id" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="Choose a Principal">

                    <option value=""></option>

                    <?php 
                      foreach($all_clients as $client) { 
                    ?>

                        <option value="<?php echo $client->client_id;?>"><?php echo $client->name;?></option>

                    <?php } ?>

                  </select>
              </div> 
              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

                </div>

              </div>

              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">

                </div>

              </div> 

              <div class="col-md-2">

                <div class="form-group">

                  <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

                </div>

              </div>

            </div>

            <?php echo form_close(); ?> 
        </div>

    </div>

  </div>

</div>


<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

        <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

            <thead>

                <tr> 
                    <th width="5%">Action</th>
                    <th width="10%">Dependant ID</th>
                    <th width="20%">Dependant Name</th>
                    <th width="20%">Principal Name</th>
                    <th width="23%">Provider</th>
                    <th width="5%">Relation</th>
                    <th width="5%">Age</th>
                    <th width="10%">Created</th>
                </tr>

                <tbody> 
                    <?php   
                    if(!empty($dependants))
                    { 
                        // print_r($dependants);die;
                        $no=1;
                        $cid = array();
                        foreach ($dependants as $key => $value)
                        {  
                            $ci=& get_instance();
                            $ci->load->model('Training_model'); 

                            if (!empty($cid)) {
                                if ($cid[0] == $value->client_id) {
                                    $did++;
                                }else{
                                    $cid[0] = $value->client_id;
                                    $did=1;
                                }
                            }else{
                                $cid[0] = $value->client_id;
                                $did = 1;
                            }

                            // echo $value->name."\n";
                            // continue;

                            $location = $ci->Training_model->getAll2('xin_location', ' location_id= "'. $value->location.'" ');
                            
                            $client_name =  $ci->Training_model->getAll2('xin_clients', ' client_id= "'. $value->client_id.'" ');

                            $hospital =  $ci->Training_model->getAll2('xin_hospital', ' hospital_id= "'. $value->hospital_id.'" ');
                            
                            $encounter =  $ci->Training_model->getAll2('xin_clients_diagnose', ' diagnose_client_id= "'. $value->client_id.'" ');

                            $total_bill = array();

                            if (!empty($encounter)) {
                                foreach ($encounter as $e) {
                                   array_push($total_bill, $e->diagnose_total_sum);
                                }
                            }

                            ?>  
                            <tr>
                                <td>
                                    <span data-toggle="tooltip" data-placement="top" title="Tiitle">
                                        <button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light edit_client_detail "  data-toggle="modal" data-target=".edit-modal-data2"
                                    
                                    data-name = "<? echo $value->name; ?>"
                                    data-last= "<? echo $value->last_name; ?>"
                                    data-other= "<? echo $value->other_name; ?>"
                                    data-orga= "<? echo $value->name; ?>"
                                    data-address= "<? echo $value->address_1; ?>"
                                    data-location= "<? echo $value->location; ?>"
                                    data-hospital= "<? echo $value->hospital_id; ?>"
                                    data-relation= "<? echo $value->relation; ?>"
                                    data-sex= "<? echo $value->sex; ?>"
                                    data-dob= "<? echo $value->dob; ?>"
                                    data-phone= "<? echo $value->contact_number; ?>"
                                    data-profile= "<? echo $value->client_profile; ?>"



                        ><span class="fa fa-pencil"></span></button></span>

                                </td>
                                <td>PHC/<?php echo date_format(date_create($value->created_on),"Y")."/0".str_pad($value->client_id, "0",STR_PAD_LEFT).'/'.str_pad($did,1,'0',STR_PAD_LEFT); ?></td>
                                <td><?php echo $value->name." ".$value->last_name." ".$value->other_name; ?> </td>

                                <td><?php echo isset($client_name[0]->name) ? ucfirst($client_name[0]->name)." ".$client_name[0]->last_name : ''; ?> </td>

                                <td><?php echo isset($hospital[0]->hospital_name) ? $hospital[0]->hospital_name : ''; ?> </td>

                                <td><?php echo $value->relation; ?></td> 

                                <td><?php echo date_diff(date_create("$value->dob"), date_create('today'))->y ; ?></td>
                                                                                              
                                <td><?php echo $value->created_on; ?></td> 
                            </tr> 

                            <?php 
                        $no++;
                        }
                    }
                    ?>
                </tbody>

            </thead>

        </table>

    </div>

  </div>

</div>
 
<div class="modal fadeInRight edit-modal-data2 animated" id="edit-modal-data" role="dialog" aria-labelledby="edit-modal-data" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="ajax_modal">
        <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                <h4 class="modal-title" id="edit-modal-data"><i class="icon-pencil7"></i>Dependant Enrollee Information Update</h4>
            </div>
            <form action="<?php echo base_url() ?>admin/clients/update" name="edit_client" id="edit_client" autocomplete="off" class="m-b-1" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="EDIT">
                <input type="hidden" name="_token"  id="_token">
                <input type="hidden" name="ext_name" value="Shaleena">
                <input type="hidden" name="csrf_hrsale" value="467ba7724bc88bcd9af14ffeb6201eff">                  



                <div class="modal-body">
                   <div class="row">

                        <div class="col-md-12 text-center">

                            <fieldset class="form-group">

                                <label for="logo"><?php echo $this->lang->line('xin_project_client_photo');?></label><br>

                                <input type="file" style="display: none;" class="form-control-file" id="client_photo" name="client_photo">
                                <img id="profile" onclick="openFileUpload()"><br>
                                <small><?php echo $this->lang->line('xin_company_file_type');?></small>

                            </fieldset>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="company_name">Select Organization</label>                  

                                <select class="form-control custom" name="organization" onchange="disableOther(this.value)" id="organization" data-plugin="xin_select" data-placeholder="Select Organization">

                                    <option value="">Select Organization</option>

                                    <?php foreach($all_organizations as $organization) {?>

                                    <option value="<?php echo $organization->id;?>"> <?php echo $organization->name;?></option>

                                    <?php } ?>

                                </select> 
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="company_name">Select Principal</label>

                                <select class="form-control custom" id="principal2" onchange="fetchP(this.value)" name="principal" data-plugin="xin_select" data-placeholder="Select Principal">

                                    <option value="">Select Principal</option>

                                </select> 
                            </div>
                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label for="client_name">First Name</label>

                                <input class="form-control" id="first_name" placeholder="First Name" name="name" type="text">

                            </div>
                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label for="client_name">Last Name</label>

                                <input class="form-control" id="last_name" placeholder="Enrollee Name" name="last_name" type="text">

                            </div>
                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label for="client_name">Other Name</label>

                                <input class="form-control" id="others" placeholder="Enrollee Name" name="other_name" type="text">

                            </div>
                        </div>
                        <div class="col-md-6 ">

                            <div class="form-group">

                                <label for="last_name">Date Of Birth</label>

                                <input type="text" id="dob" class="form-control date" placeholder="Select a date" required="" name="dob">

                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="contact_number">Phone Number</label>

                                <input class="form-control" id="phone" placeholder="Phone Number" name="contact_number" type="text">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subscription_ids">Sex</label> 

                                <select   class="form-control" id="sex" name="sex" data-plugin="xin_select" data-placeholder="Select Gender">

                                    <option value="">Select Gender</option> 

                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
             
                                </select> 
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subscription_ids">Relation</label> 

                                <select   class="form-control" id="relation" name="relation" data-plugin="xin_select" data-placeholder="Select Relation">

                                    <option value="">Select Relation</option> 

                                    <option value="spouse">Spouse</option>
                                    <option value="son">Son</option>
                                    <option value="daughter">Daughter</option>
             
                                </select> 
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="company_name">Select Providers</label>

                                <select class="form-control custom" id="hospital" name="hospital" data-plugin="xin_select" data-placeholder="Select Providers" >

                                    <option value="">Select Providers</option>

                                </select> 
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="company_name">Select Location</label>

                                <select class="form-control" id="location" name="location" data-plugin="xin_select" data-placeholder="Select Location">

                                    <option value="">Select Location</option>
                                    <?php foreach ($all_locations as $location): ?>
                                            <option value="<?= $location->location_id ?>"><?= $location->location_name; ?></option>                     
                                    <?php endforeach ?>

                                </select> 
                            </div>
                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label for="client_name">Address</label>

                                <textarea name="address" id="address" cols="10" rows="3" class="form-control"></textarea>

                            </div>
                        </div> 
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    function openFileUpload(){
        var clientPhoto = document.getElementById('client_photo');
        clientPhoto.click();
    }
    function disableOther(value ){
        var principal = document.getElementById('principal2');
      $.ajax({
            url : '<?php  echo base_url(); ?>admin/ClientAccount/fetch_principal/',
            type: 'GET',
            data: {'organization': value},
            success:function(response){
                principal.innerHTML = response;
            }

      });
    }
    function fetchP(value ){
          var hospital = document.getElementById('hospital');
      $.ajax({
            url : '<?php  echo base_url(); ?>admin/ClientAccount/fetch_hospital/',
            type: 'GET',
            data: {'principal': value},
            success:function(response){
                hospital.innerHTML = response;
            }

      });
    }

    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
            dom: 'lBfrtip',
            "buttons": ['csv', 'excel', 'pdf', 'print'],
            "order" : [[6,"desc"]]
        });
 
    }, false);

     
</script>
 

            <script type="text/javascript">

                document.addEventListener('DOMContentLoaded', function(){ 

                    $('[data-plugin="select_hrm"]').select2($(this).attr('data-options'));
                    $('[data-plugin="select_hrm"]').select2({ width:'100%' });   

                    /* Edit data */
                    $("#edit_client").submit(function(e){
                        var fd = new FormData(this);
                        var obj = $(this), action = obj.attr('name');
                        fd.append("is_ajax", 2);
                        fd.append("edit_type", 'client');
                        fd.append("form", action);
                        e.preventDefault();
                        $('.save').prop('disabled', true);
                        $.ajax({
                            url: e.target.action,
                            type: "POST",
                            data:  fd,
                            contentType: false,
                            cache: false,
                            processData:false,
                            success: function(JSON)
                            {
                                if (JSON.error != '') {
                                    toastr.error(JSON.error);
                                    $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                                    $('.save').prop('disabled', false);
                                } else {
                                    // On page load: datatable
                                    var xin_table = $('#xin_table').dataTable({
                                        aaSorting: [[4,'desc']],
                                        "bDestroy": true,
                                        "ajax": {
                                            url : "<?php  echo base_url(); ?>admin/clients/clients_list",
                                            type : 'GET'
                                        },
                                        dom: 'lBfrtip',
                                        "buttons": ['csv', 'excel', 'pdf', 'print'], // colvis > if needed
                                        "fnDrawCallback": function(settings){
                                        $('[data-toggle="tooltip"]').tooltip();          
                                        }
                                    });
                                    xin_table.api().ajax.reload(function(){ 
                                        toastr.success(JSON.result);
                                    }, true);
                                    $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                                    $('.edit-modal-data2').modal('toggle');
                                    $('.save').prop('disabled', false);
                                }
                            },
                            error: function() 
                            {
                                toastr.error(JSON.error);
                                $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                                $('.save').prop('disabled', false);
                            }           
                       });
                    });





                    $('body').on('click', '.edit_client_detail', function() { 
  
                        var name     = $(this).attr('data-name');
                        var lastName     = $(this).attr('data-last');
                        var others     = $(this).attr('data-other');
                        var orga     = $(this).attr('data-orga');  
                        var address     = $(this).attr('data-address');
                        var location     = $(this).attr('data-location');
                        var hospital     = $(this).attr('data-hospital');
                        var relation     = $(this).attr('data-relation');
                        var sex     = $(this).attr('data-sex');
                        var dob    = $(this).attr('data-dob');
                        var phone    = $(this).attr('data-phone');
                        var profile    = $(this).attr('data-profile');
                        
                        // var subs_array = detail_pkg_edit.split("--------")
                        // $('#subscription_ids').val(0).trigger('change');
                        // for (var i = 1; i < subs_array.length; i++) 
                        // {
                        //     // $("#band_edit option[value="+bnd_pkg_array[i]+"]").prop("selected", false).parent().trigger("change");
                        //     $('#subscription_ids option[value="'+subs_array[i]+'"]').attr("selected", "selected");
                        //     // $('#band_edit').attr(bnd_pkg_array[i]).trigger('change')
                                 
                        // }

                        $('#first_name').val(name); 
                        $('#last_name').val(lastName); 
                        $('#organization').val(orga).trigger('change'); 
                        $('#others').val(others).trigger('change'); 
                        // $('#ind_family').val(family).trigger('change'); 
                        document.getElementById('address').innerHTML = address ;
                        $('#location').val(location).trigger('change');
                        $('#hospital').val(hospital).trigger('change');
                        $('#relation').val(relation).trigger('change');
                        $('#sex').val(sex).trigger('change');
                        $('#dob').val(dob);
                        $('#phone').val(phone);
                        var profiles = document.getElementById('profile');
                        if(profile != ''){
                        profiles.src = profile;                            
                        }
                        
                    }) 
                }, false);
            </script>






 