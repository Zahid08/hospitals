<?php

/* Subscription view
    
*/
?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>
<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('594',$role_resources_ids) || in_array('605',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>

<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> Filter Utilization by Selecting a Date Range<?php //echo $query; ?> </h3>

  </div>

  <div class="box-body">

    <?php echo form_open('admin/organization/organization_encounter');?>

    <div class="row">

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-2">

        <div class="form-group">

          <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

        </div>

      </div>

    </div>

    <?php echo form_close(); ?> </div>
</div>



<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">
    <h3 class="box-title">
      <?php echo (isset($to_date) AND isset($from_date)) ? "Showing utilization for all organizations from $from_date to $to_date" : ''; ?>
    </h3>
  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>

    <div class="box-datatable table-responsive">

        <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

            <thead>

                <tr> 
                    <th>Organization</th>
                    <th>Paid</th>
                    <th>Unpaid</th>
                    <th>Payment</th>
                    <th>Unpaid</th>
                    <th>Balance</th>
                    <th>Used</th>
                    <th>Percentage</th>
                    <th>Status</th>
                </tr>

                <tbody> 
                  <?php   
                    if(!empty($all_organization))
                    { 
                        $admin_id = $this->session->userdata;
                        $count = 1;
                        // print_r($all_organization);die;
                        foreach ($all_organization as $key => $value)
                        {
                            $ci=& get_instance();
                            $ci->load->model('Invoices_model'); 
                            $ci->load->model('Clients_model'); 

                            if (isset($to_date)) {
                              $paid_invoices = $ci->Invoices_model->get_invoice_organization_status($value->id,'paid',$from_date,$to_date);
                              $unpaid_invoices = $ci->Invoices_model->get_invoice_organization_status($value->id,'unpaid',$from_date,$to_date);
                            }else{
                              $paid_invoices = $ci->Invoices_model->get_invoice_organization_status($value->id,'paid');
                              $unpaid_invoices = $ci->Invoices_model->get_invoice_organization_status($value->id,'unpaid');
                            }
                    

                            $total_paid = $paid_invoices->num_rows();
                            $total_unpaid = $unpaid_invoices->num_rows();

                            $amount_paid = array();
                            $amount_unpaid = array();

                            foreach ($paid_invoices->result() as $p) {
                                array_push($amount_paid, $p->grand_total);
                            }

                            foreach ($unpaid_invoices->result() as $p) {
                                array_push($amount_unpaid, $p->grand_total);
                            }

                            // get organization expenses
                            $total = array();
                            if (isset($to_date)) {
                              $client = $this->Clients_model->get_clients_encounter_by_organization($value->id,$from_date,$to_date)->result();
                            }else{
                              $client = $this->Clients_model->get_clients_encounter_by_organization($value->id)->result();
                            }
                            
                            if (!empty($client)) {
                              foreach ($client as $c) {
                                array_push($total, $c->total_sum);
                              }
                            }

                            $organization_expense = array_sum($total);
                            if (array_sum($amount_paid) == 0) {
                              $balance = 0;
                              $percentage = 0;
                            }else{
                              $balance = array_sum($amount_paid)-$organization_expense;
                              $percentage = $organization_expense/array_sum($amount_paid)*100;
                            }
                  ?>  
                            <tr>
                              <th><?php echo $value->name ?></th>
                              <th><?php echo $total_paid ?></th>
                              <th><?php echo $total_unpaid ?></th>
                              <th><?php echo $this->Xin_model->currency_sign(array_sum($amount_paid)); ?></th>
                              <th><?php echo $this->Xin_model->currency_sign(array_sum($amount_unpaid)); ?></th>
                              <th><?php echo $this->Xin_model->currency_sign($balance) ?></th>
                              <th><?php echo $this->Xin_model->currency_sign($organization_expense); ?></th>
                              <th><?php echo number_format(($percentage),2)."%"; ?></th>
                              <th><?php 
                                if ($percentage > 50) {
                                  echo "<p class=\"text-red\">LOSS</p>";
                                }else{
                                  echo "<p class=\"text-green\">PROFIT</p>";
                                }?>
                              </th>
                            </tr> 

                            <?php 
                          $count++;
                        }
                        }                    
                    ?>
                </tbody>  

            </thead>

        </table>

    </div>

  </div>

</div>
 
<?php }else{
  redirect('admin/dashboard','refresh');
} ?>
<script type="text/javascript">
      function loadModalView(id){
          // alert("ID is: " + id);

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#fetched_data").html(response);
            }
          });

      }

      // function loadModalViewAll(from,to,hid){
      //     // alert("ID is: " + from);
      //     console.log(from);
      //     $.ajax({
      //       url      : '<?php //echo base_url(); ?>hospital/Clients/fetch_diagnose_all',
      //       method   : 'post',   
      //       dataType    : 'text',      
      //       data     : {id : "<?php //echo $ids; ?>", from: from, to: to,hid: hid},
      //       success  : function(response){
      //         // $("#id_we_get_is").text(response);
      //         // alert(response);
      //         $("#fetched_data").html(response);
      //       }
      //     });

      $('#printPDF').click(function(){
          $('#fetched_data').print();
        });

    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
            "buttons": ['csv', 'excel', 'pdf', 'print'],
            "order": [[ 7, "desc" ]]
        }); 
    }, false);

     
</script>
 






 