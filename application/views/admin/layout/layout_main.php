<?php
$this->load->helper('url');
$page_url=current_url();

$session = $this->session->userdata('username');

$system = $this->Xin_model->read_setting_info(1);

$company_info = $this->Xin_model->read_company_setting_info(1);

$layout = $this->Xin_model->system_layout();

$user_info = $this->Xin_model->read_user_info($session['user_id']);

//material-design

$theme = $this->Xin_model->read_theme_info(1);

// set layout / fixed or static

if($user_info[0]->fixed_header=='fixed_layout_hrsale') {

	$fixed_header = 'fixed';

} else {

	$fixed_header = '';

}

if($user_info[0]->boxed_wrapper=='boxed_layout_hrsale') {

	$boxed_wrapper = 'layout-boxed';

} else {

	$boxed_wrapper = '';

}

if($user_info[0]->compact_sidebar=='sidebar_layout_hrsale') {

	$compact_sidebar = 'sidebar-collapse';

} else {

	$compact_sidebar = '';

}

/*

if($this->router->fetch_class() =='chat'){

	$chat_app = 'chat-application';

} else {

	$chat_app = '';

}*/

$role_user = $this->Xin_model->read_user_role_info($user_info[0]->user_role_id);

if(!is_null($role_user)){

	$role_resources_ids = explode(',',$role_user[0]->role_resources);

} else {

	$role_resources_ids = explode(',',0);	

}

?>

<?php $this->load->view('admin/components/htmlheader');

?>

<body class="hrsale-layout hold-transition sidebar-mini skin-black <?php echo $fixed_header;?> <?php echo $boxed_wrapper;?> <?php echo $compact_sidebar;?>">

<div class="wrapper">

<?php if($theme[0]->theme_option == 'template_1'):?>

  <?php $this->load->view('admin/components/header');?>

<?php else:?>

	<?php $this->load->view('admin/components/header_template2');?>

<?php endif;?>  

  <!-- Left side column. contains the logo and sidebar -->

  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->

    <!-- Links -->

    <?php //$this->load->view('admin/components/left_menu');?>

    <?php if($theme[0]->theme_option == 'template_1'):?>

	  <?php $this->load->view('admin/components/left_menu');?>

    <?php else:?>

        <?php $this->load->view('admin/components/left_menu_template2');?>

    <?php endif;?>

    <!-- /.sidebar -->

    <div class="sidebar-footer">

		<?php  if(in_array('60',$role_resources_ids)) { ?>

        <!-- item-->

        <?php } else {?>

        <a href="<?php echo site_url('admin/dashboard');?>" class="link" data-toggle="tooltip" title="" data-original-title="<?php echo $this->lang->line('dashboard_title');?>"><i class="fa fa-dashboard"></i></a>

        <?php } ?>

		<!-- item-->

		<a href="<?php echo site_url('admin/profile');?>" class="link" data-toggle="tooltip" title="" data-original-title="<?php echo $this->lang->line('header_my_profile');?>"><i class="fa fa-user"></i></a>

		<!-- item-->

		<a href="<?php echo site_url('admin/logout');?>" class="link" data-toggle="tooltip" title="" data-original-title="<?php echo $this->lang->line('header_sign_out');?>"><i class="fa fa-power-off"></i></a>

	</div>

  </aside>

  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <?php if($this->router->fetch_class() !='dashboard' && $this->router->fetch_class() !='chat' && $this->router->fetch_class() !='calendar' && $this->router->fetch_class() !='profile'){?>

    <section class="<?php echo $theme[0]->page_header;?> content-header">

      <h1>

        <?php echo $breadcrumbs;?>

        <!--<small><?php echo $breadcrumbs;?></small>-->

        <div class="row breadcrumbs-hr-top">

              <div class="breadcrumb-wrapper col-xs-12">

            
              </div>

            </div>

      </h1>

      <img id="hrload-img" src="<?php echo base_url()?>skin/img/loading.gif" style="">

	<style type="text/css">

    #hrload-img {

        display: none;

        z-index: 87896969;

        float: right;

        margin-right: 25px;

        margin-top: -32px;

    }

    </style>

      <?php if($user_info[0]->user_role_id==1): ?>

      <ol class="breadcrumb">

      </ol>

      <?php endif;?>

    </section>

    <?php } ?>

    <!-- Main content -->

    <section class="content">

      <!-- Small boxes (Stat box) -->

      

      <!-- /.row -->

      <!-- Main row -->

      <?php // get the required layout..?>

	   <?php echo $subview;?>

      <!-- /.row (main row) -->



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

  <?php $this->load->view('admin/components/footer');?>

 

  <!-- Add the sidebar's background. This div must be placed

       immediately after the control sidebar -->

  <div class="control-sidebar-bg"></div>

</div>

<!-- ./wrapper -->



<!-- Layout footer -->

<?php $this->load->view('admin/components/htmlfooter');?>

<!-- / Layout footer -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Encounter View</h3>
        </div>
        <div class="modal-body" id="fetched_data">
        </div>
      
      <script>
        function printDiv() 
        {

          var divToPrint=document.getElementById('fetched_data');

          var newWin=window.open('','Print-Window');

          newWin.document.open();

          newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

          newWin.document.close();

          setTimeout(function(){newWin.close();},10);

        }
      </script>
        <div class="clearfix"></div>
        <div class="modal-footer">
			<?php
//			if($page_url == "https://liontechhmo.liontech.com.ng/app/admin/Hospital/diagnose_hospital_clients_bill_requests"){
//			?>

<!--			--><?php //} ?>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="reasonModal" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Write a Reason for Rejecting This Request</h3>
        </div>
        <form action="<?php echo base_url(); ?>admin/Hospital/diagnose_hospital_clients_requests" name="reason_form" id="reason_form" method="post">
          <div class="modal-body" id="fetched_data">
            <div class="form-group col-lg-12">
              <label for="reason">Write it below:</label>
              <textarea class="form-control" name="that_reason" id="that_reason" rows="7" placeholder="Define the reason for Rejection" required></textarea>
              <input type="hidden" name="did" id="did" class="form-control">
              <input type="hidden" name="hid" id="hid" class="form-control">
              <input type="hidden" name="cid" id="cid" class="form-control">
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Proceed To Reject</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
  
    <!-- Drug Reason Modal -->
    
  <div class="modal fade" id="drug_reasonModal" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Reasons For Removing This Drug</h3>
        </div>
        <form action="<?php echo base_url(); ?>admin/Hospital/drug_notes" name="drugreason_form" id="drugreason_form" method="post">
          <div class="modal-body" id="drugfetched_data">
            <div class="form-group col-lg-12">
             
              <?php if(current_url() == 'https://liontechhmo.liontech.com.ng/app/admin/Hospital/adjusted_requests' || current_url() == 'https://liontechhmo.liontech.com.ng/app/admin/Hospital/diagnose_hospital_clients_requests'){ ?>
              <label for="reason">Reasons Below:</label>
              <textarea class="form-control" name="that_drug_reason" id="that_drug_note" rows="7" ></textarea>
              <?php } else{ ?>
              
              <label for="reason">Reason below:</label>
              <textarea class="form-control" name="that_drugreason" id="that_drugreason" rows="7" placeholder="Write the reason here" required></textarea>
              <input type="hidden" name="drug_did" id="drug_did" class="form-control">
              <input type="hidden" name="drug_hid" id="drug_hid" class="form-control">
              <input type="hidden" name="drug_cid" id="drug_cid" class="form-control">
              <input type="hidden" name="drug_mid" id="drug_mid" class="form-control">
              <input type="hidden" name="redirectflage" id="redirectflage" class="form-control" value="1">
              <?php } ?> 
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="modal-footer">
            <?php if(current_url() != 'https://liontechhmo.liontech.com.ng/app/admin/Hospital/adjusted_requests'){ ?>
            <button type="submit" class="btn btn-success">Proceed</button>
            <?php } ?>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
  
    <!-- Service Reason Modal -->
    
  <div class="modal fade" id="service_reasonModal" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Reasons For Removing This Service</h3>
        </div>
        <form action="<?php echo base_url(); ?>admin/Hospital/service_notes" name="service_reason_form" id="service_reason_form" method="post">
          <div class="modal-body" id="service_fetched_data">
            <div class="form-group col-lg-12">
              
              <?php if(current_url() == 'https://liontechhmo.liontech.com.ng/app/admin/Hospital/adjusted_requests'){ ?>
              <label for="reason">Reasons For Removing This Service:</label>
              <textarea class="form-control" name="that_service_reason" id="that_service_note" rows="7" ></textarea>
              <?php } else{ ?>
              <label for="reason">Reason below:</label>
              <textarea class="form-control" name="that_service_reason" id="that_service_reason" rows="7" placeholder="Write the reason here" required></textarea>
              <input type="hidden" name="service_did" id="service_did" class="form-control">
              <input type="hidden" name="service_hid" id="service_hid" class="form-control">
              <input type="hidden" name="service_mid" id="service_mid" class="form-control">
              <input type="hidden" name="service_sid" id="service_sid" class="form-control">
              <input type="hidden" name="service_flage" id="service_flage" class="form-control" value="1">
	      <?php } ?> 
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="modal-footer">
            <?php if(current_url() != 'https://liontechhmo.liontech.com.ng/app/admin/Hospital/adjusted_requests'){ ?>
            <button type="submit" class="btn btn-success">Proceed</button>
            <?php } ?>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
    
  <!-- Modal -->
  <div class="modal fade" id="billReasonModal" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Write Reasons For Rejecting Hospital Bill</h3>
        </div>
        <form action="<?php echo base_url(); ?>admin/Hospital/diagnose_hospital_clients_bill_requests" name="reason_form" id="reason_form" method="post">
          <div class="modal-body" id="fetched_data">
            <div class="form-group col-lg-12">
              <label for="reason">Write the reason for Bill Rejection:</label>
              <textarea class="form-control" name="that_reason" id="that_reason" rows="7" placeholder="Write the reason for Rejection" required></textarea>
              <input type="hidden" name="bdid" id="bdid" class="form-control">
              <input type="hidden" name="bhid" id="bhid" class="form-control">
              <input type="hidden" name="bcid" id="bcid" class="form-control">
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Proceed To Reject</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="noteModal" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Comment On This Bill For Next Approver</h3>
        </div>
        <form action="<?php echo base_url(); ?>admin/Hospital/diagnose_hospital_clients_bill_requests" name="reason_form" id="reason_form" method="post">
          <div class="modal-body" id="fetched_data">
            <div class="form-group col-lg-12">
              <label for="reason">Write Note below:</label>
              <textarea class="form-control" name="that_note" id="that_reason" rows="7" placeholder="Write your notes here" required></textarea>
              <input type="hidden" name="nbdid" id="nbdid" class="form-control">
              <input type="hidden" name="nbhid" id="nbhid" class="form-control">
              <input type="hidden" name="nbcid" id="nbcid" class="form-control">
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Save Note</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
  <div class="modal" id="admissioncountdaysModal1" role="dialog" style="display:none;">
    <div style="width: 800px;height: 500px;overflow-y: auto;" class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" onclick="return closedmodel1();">&times;</button>
          <h3 class="modal-title">View Hospitality Reports</h3>
        </div>
        
          <div class="modal-body" id="fetched_data_comment">
		  
          </div>
          <div class="clearfix"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" onclick="return closedmodel1();">Close</button>
          </div>
       
      </div>
      
    </div>
  </div>
  <div class="modal" id="admissioncountdaysModal" role="dialog" style="display:none;">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" onclick="return closedmodel();">&times;</button>
          <h3 class="modal-title">Write an Admission Report</h3>
        </div>
        <form action="<?php echo base_url();?>admin/Hospital/admissioncomment" name="reason_form" id="admission_form" method="post">
          <div class="modal-body" id="fetched_data_admission">
		  <div class="col-md-12">
            <div class="form-group col-md-5">
					
              <label for="reason"> Select a Date:</label>
			  <input type="date" class="form-control form_datetime" name="fromdate" id="fromdate">
             
            </div>
			      <!-- <div class="form-group col-md-6"> 
					
              <label for="reason">To Date:</label>
			  <input type="date" class="form-control dob" name="todate" id="todate">
             
            </div> -->
			<div class="form-group col-md-12">
				<label for="comment">Write your report here:</label>
				<textarea name="comment" id="comment" rows="7" class="form-control"></textarea>
				<input name="diagonal_ids" id="diagonal_ids" type="hidden">
			</div>       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      <a href="#" id="invalid_case" class="btn btn-danger">INVALIDATE ADMISSION CASE</a>

          </div>
          </div>
          <div class="clearfix"></div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Save Report</button>
            <button type="button" class="btn btn-danger" onclick="return closedmodel();">Close</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="viewNoteModal" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title" id="rtitle">Displaying Comments Written For this Bill</h3>
        </div>
        <div class="modal-body" id="fetched_data">
          <div class="col-lg-12" id="fetched_note"></div>
        </div>
        <div class="clearfix"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
 <div class="modal" id="editprofile" role="dialog" style="display:none;">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" onclick="return closed();">&times;</button>
          <h3 class="modal-title">Edit Dependant Profile</h3>
        </div>
        <form action="<?php echo base_url();?>admin/ClientAccount/familyclientupdate" name="editprofile_form" id="editprofile_form" method="post">
          <div class="modal-body" id="editprofile_data">
            
          </div>
          <div class="clearfix"></div>
          <div class="modal-footer">
           
            <button type="submit" class="btn btn-success" >Update Profile</button>
            
            <button type="button" class="btn btn-danger" onclick="return closed();">Close</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
  <div class="modal fade" id="optionalItemModal" role="dialog" style=" padding-left: 17px;">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h3 class="modal-title">Reasons For Removing</h3>
        </div>
          <div class="modal-body" id="drugfetched_data">
            <div class="form-group col-lg-12">
            <label for="reason">Reasons Below:</label>
              <textarea class="form-control" id="reasonModalInput" rows="7"></textarea>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="modal-footer">
            <button onclick="saveReason()" class="btn btn-success">Proceed</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" id="closeModal">Close</button>
          </div>
      </div>
    </div>
  </div>
<script type="text/javascript">

function drug_note(id){
          // alert("ID is: " + id);

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_drug_note',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              // $("#id_we_get_is").text(response);
               //alert(response);
              $("#that_drug_note").html(response);
            }
          });

      }
      

function service_note(id){
          // alert("ID is: " + id);

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_service_note',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              // $("#id_we_get_is").text(response);
               //alert(response);
              $("#that_service_note").html(response);
            }
          });

      }


$(document).on('click', '.drug_close', function(){ 
  $('.drug_row').each(function() {
    $(this).hide();
  });
});
	
</script>

<script type="text/javascript">
    function loadDrugReasonModal(drug_did,drug_hid,drug_cid,drug_mid){
        //alert(drug_did);
        $("#drug_reasonModal").on("show.bs.modal", function(e) {
            $("#drug_did").val(drug_did);
            $("#drug_hid").val(drug_hid);
            $("#drug_cid").val(drug_cid);
            $("#drug_mid").val(drug_mid);
            $("#redirectflage").val('2');
            $("#dugus"+drug_cid).hide();
        });
        // $("#that_drugreason").val("");




    }
    function loadServiceReasonModal(service_did,service_hid,service_mid,service_sid){
        //alert(service_mid);
        //$("#that_service_reason").val("");
        $("#service_reasonModal").on("show.bs.modal", function(e) {
            $("#service_did").val(service_did);
            $("#service_hid").val(service_hid);
            $("#service_mid").val(service_mid);
            $("#service_sid").val(service_sid);

            $("#service_flage").val('2');
            $("#service"+service_did).hide();
        });

    }
$(document).on('click', '.service_close', function(){ 
  $('.service_row').each(function() {
    $(this).hide();
  });     
});

</script>
  
<script type="text/javascript">
$(document).ready(function(){

  $("#select_hospital").change(function(){
      var hid = $("#select_hospital").val();
      // alert(hid);
      $.ajax({
            url      : '<?php echo base_url(); ?>admin/Hospital/request_client',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : hid},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#select_client").html(response);
              console.log(response);
              $("#select_client").trigger("change");
              //drugs
              $.ajax({
                    url      : '<?php echo base_url(); ?>admin/Hospital/request_drug',
                    method   : 'post',   
                    dataType    : 'text',      
                    data     : {id : hid},
                    success  : function(response){
                      // $("#id_we_get_is").text(response);
                      // alert(response);
                      $("#select_drug").html(response);
                      console.log(response);
                    }
                  });   

              //Services
              $.ajax({
                    url      : '<?php echo base_url(); ?>admin/Hospital/request_service',
                    method   : 'post',   
                    dataType    : 'text',      
                    data     : {id : hid},
                    success  : function(response){
                      // $("#id_we_get_is").text(response);
                      // alert(response);
                      $("#select_service").html(response);
                      console.log(response);
                    }
                  });
            }
          });      
  });

  $("#select_client").change(function(){
      var cid = $("#select_client").val();
      // alert(hid);
      console.log("client id: "+cid);
      $.ajax({
            url      : '<?php echo base_url(); ?>admin/Hospital/fetch_client_plan',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : cid},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#plan_name").text(response);
              console.log(response);
              
            }
          });      
  });
});

</script>

<!-- Script for change invoice status -->
<script type="text/javascript">
    $('[data-plugin="xin_select"]').select2($(this).attr('data-options'));
    $('[data-plugin="xin_select"]').select2({ width:'100%' });

    $(document).ready(function(){  
        var status = $('#invoiceStatus');
        if(status.val() == 0){
              $('#amount').hide();     
              $('#bank').hide();     
          }else if(status.val() > 1){
              $('#amount').show();     
              $('#amount').prop('required',true);     
              $('#bank').hide();
          }else{
              $('#amount').hide();     
              $('#bank').show();
              $('#bank').prop('required',true);
              $('#buttonChange').hide();
          }
        status.change(function(){
          if(status.val() == 0){
              $('#amount').hide();     
              $('#bank').hide();     
          }else if(status.val() > 1){
              $('#amount').show();     
              $('#amount').prop('required',true);     
              $('#bank').hide();
          }else{
              $('#amount').hide();     
              $('#bank').show();
              $('#bank').prop('required',true);
          }
        });
    });
</script>


<?php 
if (isset($_SESSION['success_invoice'])) {
  echo "
    <script>
      toastr.success('".$_SESSION['success_invoice']."');
    </script>
  ";
} else if (isset($_SESSION['failed_invoice'])) {
  echo "
    <script>
      toastr.error('".$_SESSION['failed_invoice']."');
    </script>
  ";
} else if (isset($_SESSION['numberempty_invoice'])) {
  echo "
    <script>
      toastr.error('".$_SESSION['numberempty_invoice']."');
    </script>
  ";
} else if (isset($_SESSION['dateempty_invoice'])) {
  echo "
    <script>
      toastr.error('".$_SESSION['dateempty_invoice']."');
    </script>
  ";
} else if (isset($_SESSION['duedateempty_invoice'])) {
  echo "
    <script>
      toastr.error('".$_SESSION['duedateempty_invoice']."');
    </script>
  ";
} else if (isset($_SESSION['startdateempty_invoice'])) {
  echo "
    <script>
      toastr.error('".$_SESSION['startdateempty_invoice']."');
    </script>
  ";
} else if (isset($_SESSION['enddateempty_invoice'])) {
  echo "
    <script>
      toastr.error(".$_SESSION['enddateempty_invoice'].");
    </script>
  ";
} else if (isset($_SESSION['update_invoice'])) {
  echo "
    <script>
      toastr.success(".$_SESSION['update_invoice'].");
    </script>
  ";
}
?>
</body>
</html>
<script>

    $(document).on('click','#flag-btn',function(e){
        e.preventDefault();
        var flag=$(this).data('id');
        var html='';
        $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/flaged_bill',
            method   : 'post',
            dataType    : 'text',
            data     : {did : $("#diag-id").val(),isFlag:$(this).data('id')},
            success  : function(response){
                var data=JSON.parse(response);
                if(data.status==true){
                    toastr.success("Bill Status is Updated Successfully");
                    if(flag==0){
                        html+='<button type="button" data-id="1" class="btn btn-danger" id="flag-btn">FLAG ENROLLEE</button>';
                    }
                    else {
                        html+='<button type="button" data-id="0" class="btn btn-success" id="flag-btn">UNFLAG ENROLLEE</button>';
                    }
                    $("#custom_flag_btn_div").html(html);
                }
                else{
                    toastr.error("error............");
                }

            }
        });
    });
function getinfodiv()
{
	$('#infodiv').show();
	$('#getinfodiv').attr("onclick","return removeinfodive();");
}
function removeinfodive(){
	$('#getinfodiv').attr("onclick","return getinfodiv();");
	$('#infodiv').hide();
}
</script>