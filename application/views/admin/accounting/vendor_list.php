<?php

/* Accounting > New Expense view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('574',$role_resources_ids) || in_array('593',$role_resources_ids) || $user_info[0]->user_role_id==1 ) {?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"><?php echo $this->lang->line('xin_add_new');?> <?php echo $this->lang->line('xin_vendor_title');?></h3>

      <div class="box-tools pull-right"> <a class="text-dark collapsed" data-toggle="collapse" href="#add_form" aria-expanded="false">

        <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span> <?php echo $this->lang->line('xin_add_new');?></button>

        </a> </div>

    </div>

    <div id="add_form" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">

        <?php $attributes = array('name' => 'add_vendor', 'id' => 'xin-form', 'autocomplete' => 'off');?>

        <?php $hidden = array('_user' => $session['user_id']);?>

        <?php echo form_open('admin/accounting/add_vendors', $attributes, $hidden);?>

        <div class="bg-white">

          <div class="box-block">

            <div class="row">

              <div class="col-md-7">

                <div class="row">

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="month_year"><?php echo $this->lang->line('xin_vendor_name');?></label>

                      <input class="form-control" name="vendor_name" type="text" placeholder="<?php echo $this->lang->line('xin_vendor_name');?>">

                    </div>

                  </div>

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="expense_date"><?php echo $this->lang->line('xin_phone_number');?></label>

                      <input class="form-control" name="phone_number" type="text" placeholder="<?php echo $this->lang->line('xin_phone_number');?>">

                    </div>

                  </div>

                </div>

                <div class="row">

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="month_year"><?php echo $this->lang->line('xin_address');?></label>

                      <input class="form-control" name="address" type="text" placeholder="<?php echo $this->lang->line('xin_address');?>">

                    </div>

                  </div>

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="expense_date"><?php echo $this->lang->line('xin_rc_number');?></label>

                      <input class="form-control" name="rc_number" type="text" placeholder="<?php echo $this->lang->line('xin_rc_number');?>">

                    </div>

                  </div>

                </div>

              </div>

              <div class="col-md-5">

                <div class="form-group">

                  <label for="description"><?php echo $this->lang->line('xin_description');?></label>

                  <textarea class="form-control textarea" placeholder="<?php echo $this->lang->line('xin_description');?>" name="description" cols="30" rows="5" id="description"></textarea>

                </div>

              </div>

            </div>

            <div class="form-actions box-footer">

              <button type="submit" class="btn btn-primary"> <i class="fa fa-check-square-o"></i> <?php echo $this->lang->line('xin_save');?> </button>

            </div>

          </div>

        </div>

        <?php echo form_close(); ?> </div>

    </div>

  </div>

</div>



<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table">

        <thead>

          <tr>

            <th><?php echo $this->lang->line('xin_action');?></th>

            <th><?php echo $this->lang->line('xin_vendor_name');?></th>

            <th><?php echo $this->lang->line('xin_phone_number');?></th>

            <th><?php echo $this->lang->line('xin_address');?></th>

            <th><?php echo $this->lang->line('xin_rc_number');?></th>

            <th><?php echo $this->lang->line('xin_description');?></th>

          </tr>

        </thead>

      </table>

    </div>

  </div>

</div>


<?php }else{
  redirect('admin/dashboard','refresh');
} ?>