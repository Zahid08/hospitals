<?php
/* Accounting > Income Report view
*/
?>
<?php $session = $this->session->userdata('username');?>
<?php $get_animate = $this->Xin_model->get_content_animate();?>
<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('651',$role_resources_ids) || $user_info[0]->user_role_id==1) { ?>

<div class="box <?php echo $get_animate;?>">
  <div class="box-header with-border">
    <h3 class="box-title">Trial Balance  <a id="export" class="btn btn-secondary" href="<?php echo site_url('') ?>admin/Accounting/pdf3">Export to PDF</a></h3>
  </div>
  <div class="box-body">
    <div class="box-datatable table-responsive">
      <table class="datatables-demo table table-striped table-bordered" id="xin_table">
        <thead>
          <tr>
            <th>Account ID</th>
            <th>Account Description</th>
            <th>Debit Amount</th>
            <th>Credit Amount</th>
          </tr>
        </thead>
        <tfoot id="get_footer">
        </tfoot>
      </table>
    </div>
  </div>
</div>
<?php  }else{
  redirect('admin/dashboard','refresh');
} ?>