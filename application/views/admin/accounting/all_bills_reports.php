<?php

/* Accounting > New Expense view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php $hospital_info = $this->Clients_model->get_hospital_info($hospital_id)->result(); ?>

<?php if(in_array('533',$role_resources_ids) || in_array('585',$role_resources_ids) || $user_info[0]->user_role_id==1 ) {?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"> Filter Results By Selecting a Provider and Date Range</h3>

    </div>

    <div class="<?php echo $get_animate;?>" style="">

      <div class="box-body">

            <?php echo form_open('admin/accounting/all_bills_reports');?>

            <div class="row">
              <div class="col-md-3">
                  <select name="hospital_name" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="Choose Hospital" required>

                    <option value=""></option>

                    <?php 
                      foreach($all_hospital as $hospital) { 
                    ?>

                        <option value="<?php echo $hospital->hospital_id;?>"><?php echo $hospital->hospital_name;?></option>

                    <?php } ?>

                  </select>
              </div>
              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>" required>

                </div>

              </div>

              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>" required>

                </div>

              </div>

              <div class="col-md-2">

                <div class="form-group">

                  <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

                </div>

              </div>

            </div>

            <?php echo form_close(); ?> 
        </div>

    </div>

  </div>

</div>



<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> Bill Payments <?php echo !empty($hospital_info) ? " of ".$hospital_info[0]->hospital_name : ''; ?> 
    <?php echo (isset($from) AND isset($to)) ? "from ".$from." to ".$to : ""; ?> | Total Amount paid: <span id="total_amount"></span></h3>

  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>
    
    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

        <thead>

          <tr>

            <th>Status</th>

            <th>Transaction ID</th>

            <th>Provider</th>

            <th>Amount</th>

            <th>Account</th>

            <th>Method</th>

            <th>Ref</th>

            <th>Description</th>

            <th>Date</th>

          </tr>

        </thead>
        <tbody>
          
                  <?php   
                    $total = array();
                    if(!empty($all_bill_payments))
                    { 
                        $admin_id = $this->session->userdata;
                        // print_r($all_bill_payments);die;
                        foreach ($all_bill_payments as $key => $value)
                        {              
                             
                            $admin_info = array();

                            $ci=& get_instance();
                            $ci->load->model('Clients_model'); 

                            $details = $ci->Clients_model->view_individual_hospital_diagnose_clients($value->newbill_tid);
                            // print_r($details);die;
                            $hcp = strtoupper($details["loc_id"]);
                            $date = date("Ymd", strtotime($details["diagnose_date_time"]));

                            if($hospital_id && $from && $to) {
                              if($hospital_id == $details["diagnose_hospital_id"] && $value->newbill_date >= $from && $value->newbill_date <= $to   ) {
                  ?>  
                              <tr>
                                  <td><p class="btn btn-success">Transaction Done</p></td>
                                  <td><?php if(empty($details["diagnose_transaction_id"])) echo "-----"; else echo "TC-".$hcp."-".$date."-".$details["diagnose_transaction_id"]; ?></td>
                                  <td><?php echo $details["hospital_name"]; ?></td>
                                  <td>₦<?php echo number_format($details["diagnose_total_sum"]).".00"; ?></td>
                                  <td><?php echo $value->account_name; ?></td>
                                  <td><?php echo $value->method_name; ?></td>
                                  <td><?php echo $value->newbill_ref; ?></td>
                                  <td><?php echo $value->newbill_description; ?></td>
                                  <td><?php echo $value->newbill_date; ?></td>
                              </tr> 

                  <?php 
                              array_push($total, $details["diagnose_total_sum"]);
                              }
                            }
                        }
                    }
                    if(!empty($all_bulk_bill_payments))
                    { 
                      foreach($all_bulk_bill_payments as $bulk)
                      {
                        // echo $bulk->newbill_date;
                        if ($bulk->newbill_date >= $from && $bulk->newbill_date <= $to) {

                        

                        // print_r($bulk);continue;
                        $bulk_id = $this->Finance_model->get_bulk_id_from_bill($bulk->newbill_tid);

                        $account_name = $this->Finance_model->read_bankcash_information($bulk->newbill_account);

                        if (!empty($account_name)) {
                          $account_name = $account_name[0]->account_name;
                        }else{
                          $account_name = "--";
                        }

                        $payment_method = $this->Xin_model->read_payment_method($bulk->newbill_paymentmethod);

                        if (!empty($payment_method)) {
                          $method_name = $payment_method[0]->method_name;
                        }else{
                          $method_name = "--";
                        }
                        
                        if (!empty($bulk_id)) {
                          $bulk_id = explode(",", $bulk_id[0]->bulk_id);
                          foreach($bulk_id as $diagnose)
                          {
                            $diagnose = $this->Clients_model->get_diagnose($diagnose)->result();

                            // print_r($diagnose);continue;

                            if (!empty($diagnose)) {
                              if ($hospital_id != $diagnose[0]->diagnose_hospital_id) {
                                continue;
                              }
                              $hospital = $this->Clients_model->get_hospital_info($diagnose[0]->diagnose_hospital_id)->result();

                              if (!empty($hospital)) {
                                $hcp = strtoupper($hospital[0]->loc_id);
                                $hospital_name = $hospital[0]->hospital_name;
                              }else{
                                $hospital_name = "--";
                                $hcp = "---";
                              }

                              $date = date("Ymd", strtotime($diagnose[0]->diagnose_date_time));

                            ?>
                              <tr>
                                  <td><p class="btn btn-success">Transaction Done</p></td>
                                  <td><?php if(empty($diagnose[0]->diagnose_transaction_id)) echo "-----"; else echo "TC-".$hcp."-".$date."-".$diagnose[0]->diagnose_transaction_id; ?></td>
                                  <td><?php echo $hospital_name; ?></td>
                                  <td><?php echo $this->Xin_model->currency_sign($diagnose[0]->diagnose_total_sum); ?></td>
                                  <td><?php echo $account_name; ?></td>
                                  <td><?php echo $method_name; ?></td>
                                  <td><?php echo $bulk->newbill_ref; ?></td>
                                  <td><?php echo $bulk->newbill_description; ?></td>
                                  <td><?php echo $bulk->newbill_date; ?></td>
                              </tr>

                            <?php
                              array_push($total, $diagnose[0]->diagnose_total_sum);
                            }

                          }
                        }

                      }
                      } 
                    }
                  ?>
        </tbody>

      </table>

    </div>

  </div>

</div>
<?php }else{
  redirect('admin/dashboard','refresh');
} ?>
<script type="text/javascript">
    

    document.addEventListener('DOMContentLoaded', function(){ 
    $('#total_amount').text('<?php echo $this->Xin_model->currency_sign(array_sum($total)); ?>');
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
          buttons: ['csv', 'excel', 'pdf', 'print']
        }); 
    }, false);


</script>

