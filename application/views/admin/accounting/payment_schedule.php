<?php

/* Subscription view
    
*/
?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>


<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>
<?php

      $role_user = $this->Xin_model->read_user_role_info($user_info[0]->user_role_id);

      if(!is_null($role_user)){

        $role_resources_ids = explode(',',$role_user[0]->role_resources);

      } else {

        $role_resources_ids = explode(',',0); 

      }

?>
<?php if(in_array('574',$role_resources_ids) || in_array('575',$role_resources_ids) || in_array('576',$role_resources_ids) || in_array('577',$role_resources_ids) || in_array('578',$role_resources_ids) || $user_info[0]->user_role_id==1 ) {?>


<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> Filter Data by Selecting a Date Range<?php //echo $query; ?> </h3>

  </div>

  <div class="box-body">

    <?php echo form_open('admin/accounting/payment_schedule');?>

    <div class="row">

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-2">

        <div class="form-group">

          <button type="submit" id="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

        </div>

      </div>

    </div>


    <?php echo form_close(); ?> </div>
</div>


<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border headerbox">

    <h3 class="box-title"> All Payment Schedule <span id="total"></span> <a id="export" class="btn btn-secondary">Export to PDF</a> </h3>
  
  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>

    <div class="box-datatable table-responsive">

        <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

            <thead>

                <tr> 
                  <th>Enrollee</th>
                  <th>Diagnose</th>
                  <th>Encounter</th>
                  <th>Provider</th>
                  <th>Account</th>
                  <th>Bank</th>
                  <th>Bill</th>
                </tr>

            </thead>

            <tbody>
              <?php
              $ci=& get_instance();
              $ci->load->model('Clients_model');  
              
              $no = 1;$html = '';$total = array();
              if (!empty($xin_diagnose_clients)) {
              
              foreach ($xin_diagnose_clients as $fetched_result) {
                $hospital = $ci->Clients_model->get_hospital_info($fetched_result->diagnose_hospital_id)->result();

                if (!empty($hospital)) {
                  // echo "got one";

                  $bank_name = is_null($hospital[0]->bank_name) ? '--' : strtoupper($hospital[0]->bank_name);
                  $bank_account = is_null($hospital[0]->bank_account) ? '--' : strtoupper($hospital[0]->bank_account);
                  $hospital_name = ucwords($hospital[0]->hospital_name);
                }else{
                  // echo "not one";
                  $bank_name = '--';
                  $bank_account = '--';
                }
                $html .= 
                    "<tr>
                        <td>";
                    if($fetched_result->diagnose_user_type == 'C') 
                    { 
                      $html .= $fetched_result->cname." ".$fetched_result->clname." ".$fetched_result->coname; 
                    } else 
                    { 
                      $html .= $fetched_result->dname." ".$fetched_result->dlname." ".$fetched_result->doname; 
                    }
                    $html .=  
                    "</td>
                        <td>".$fetched_result->diagnose_diagnose."</td>
                        <td>".$fetched_result->diagnose_date."</td>
                        <td>".$hospital_name."</td>
                        <td>".$bank_account."</td>
                        <td>".$bank_name."</td>
                        <td>".$this->Xin_model->currency_sign($fetched_result->diagnose_total_sum)."</td>
                    </tr>";
                $no++;
                array_push($total, $fetched_result->diagnose_total_sum);
                } 
 
                echo $html;
            }
              ?>
            </tbody>
            <tfoot>
              <?php  
                 $html2 = "<tr>
                    <td colspan='6' align='center'><b>Total</b></td>
                    <td style='display: none;'></td>
                    <td style='display: none;'></td>
                    <td style='display: none;'></td>
                    <td style='display: none;'></td>
                    <td style='display: none;'></td>
                    <td><b>".$this->Xin_model->currency_sign(array_sum($total))."</b></td>
                  </tr>
                ";
                echo $html2;
              ?>
            </tfoot>

        </table>

    </div>

  </div>

</div>
<?php }else{
  redirect('admin/dashboard','refresh');
} ?> 

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){ 
      console.log('lodaded');
         

        $('#total').text(' | TOTAL = <?php echo $this->Xin_model->currency_sign(array_sum($total)); ?>')
        $('.headerbox a').attr('href',"#");

        $('#from_date').change(function(){
          var from = $('#from_date').val();
          var to = $('#to_date').val();
          $('.headerbox a').attr('href',"<?php echo site_url('')?>admin/accounting/pdf2?from="+from+"&to="+to);
        });

        

        // var d = new Date();

        // var month = d.getMonth()+1;
        // var day = d.getDate();

        // var output = d.getFullYear() + '-' +
        //     ((''+month).length<2 ? '0' : '') + month + '-' +
        //     ((''+day).length<2 ? '0' : '') + day;

        // if(from == output){
        //   $('.headerbox a').hide();
        // }else{
        //   $('.headerbox a').show();

        // }

        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
          "buttons": ['pdf', 'print']
        });
    }, false);

     
</script>
 






 