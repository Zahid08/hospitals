<?php
/* Accounting > Income Report view
*/
?>
<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('649',$role_resources_ids) || $user_info[0]->user_role_id==1) { ?>


<div class="box mb-4 <?php echo $get_animate;?>">
  <div class="box-header with-border">
    <h3 class="box-title"> Filter data by selecting the date range</h3>
  </div>
  <div class="box-body">
    <div class="row">
      
      <div class="col-md-3">
        <div class="form-group">
          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>" style="border-radius: 1.25rem">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>" style="border-radius: 1.25rem">
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <button id="fetch" type="submit" class="btn btn-primary save"><?php echo $this->lang->line('xin_get');?></button>
        </div>
      </div>
    </div>
    </div>
</div>
<div class="box <?php echo $get_animate;?>">
  <div class="box-header with-border">
  </div>
  <div class="box-body">
    <div class="box-datatable table-responsive">
      <table class="datatables-demo table table-striped table-bordered w-50" id="xin_table" width="50%">
        <thead>
          <tr>
            <th width="20%">Type</th>
            <th width="20%">Value</th>
          </tr>
        </thead>
        <tbody>
          
        </tbody>
        <tfoot id="get_footer">

        </tfoot>
      </table>
    </div>
  </div>
</div>

<?php }else{
  redirect('admin/dashboard','refresh');
} ?>
<script>
  $(document).ready(function(){
    var xin_table = $('#xin_table').dataTable({
      dom: 'lBfrtip',
      "buttons": ['copy', 'csv', 'excel', 'pdf', 'print'], // colvis > if needed
    });
  })
</script>