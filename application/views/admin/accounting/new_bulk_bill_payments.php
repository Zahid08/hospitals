<?php

/* Accounting > New Expense view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('574',$role_resources_ids) || in_array('584',$role_resources_ids) || $user_info[0]->user_role_id==1 ) {?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title">Bulk Bills Payment Processor</h3>

      <div class="box-tools pull-right"> <a class="text-dark collapsed" data-toggle="collapse" href="#add_form2" aria-expanded="false">

        <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span> Process New Bulk Bill</button>

        </a> </div>

    </div>

    <div id="add_form2" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">

        <?php $attributes = array('name' => 'add_bill', 'id' => 'xin-bill-form', 'autocomplete' => 'off','enctype'=>"multipart/form-data");?>

        <?php $hidden = array('_user' => $session['user_id']);?>

        <?php echo form_open('admin/accounting/add_new_bulk_bill', $attributes, $hidden);?>

        <div class="bg-white">

          <div class="box-block">

            <div class="row">

              <div class="col-md-7">

                <div class="row">

                  <div class="col-md-6">
                    <div class="form-group">

                      <label for="bank_cash_id">Transaction ID <?php //print_r($transaction_amount); ?> <span id="amm_bulk_balance" style="display:none; font-weight:600; color:#F00;"></span></label>
                      <input type="hidden" name="deduct_amount2" class="form-control" id="deduct_amount2">
                      <select name="diagnose_transaction_id" id="select2-demo-6" class="transaction-bulk-amount form-control" data-plugin="select_hrm" data-placeholder="Select Transaction ID">

                        <option value=""></option>

                        <?php 
                          $ci =& get_instance();
                          $ci->load->model('Clients_model');
                          foreach($bulk_transaction_amount as $amount) { 

                            $date = date("Ymd", strtotime($amount->created_at));
                            $total = array();
                            $ids = explode(",", $amount->bulk_id);
                            foreach($ids as $id){
                              $diagnose = $this->Clients_model->get_diagnose($id)->result();
                              array_push($total, $diagnose[0]->diagnose_total_sum);
                            }
                            $total = array_sum($total);
                        ?>

                        <option value="<?php echo $amount->id;?>" amount-bulk-id="<?php echo $total;?>"><?php echo "TC-BULK-".$date."-".$amount->id." id: ".implode(",", $ids);?></option>

                        <?php } ?>

                      </select>

                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">

                      <label for="bank_cash_id"><?php echo $this->lang->line('xin_acc_account');?> <span id="acc_balance" style="display:none; font-weight:600; color:#F00;"></span></label>

                      <select name="bank_cash_id" id="select2-demo-6" class="from-account form-control" data-plugin="select_hrm" data-placeholder="<?php echo $this->lang->line('xin_acc_choose_account_type');?>">

                        <option value=""></option>

                        <?php foreach($all_bank_cash as $bank_cash) {?>

                        <option value="<?php echo $bank_cash->bankcash_id;?>" account-balance="<?php echo $bank_cash->account_balance;?>"><?php echo $bank_cash->account_name;?></option>

                        <?php } ?>

                      </select>

                    </div>
                  </div>
                </div>

                <div class="row">

                  <!-- <div class="col-md-6">

                    <div class="form-group">

                      <label for="month_year"><?php echo $this->lang->line('xin_amount');?></label>

                      <input class="form-control" name="amount" id="amount" type="text" placeholder="<?php echo $this->lang->line('xin_amount');?>" disabled>

                    </div>

                  </div> -->

                  <div class="col-md-12">

                    <div class="form-group">

                      <label for="expense_date"><?php echo $this->lang->line('xin_e_details_date');?></label>

                      <input class="form-control date" placeholder="<?php echo date('Y-m-d');?>" name="bill_date" type="text">

                    </div>

                  </div>

                </div>

                <div class="row">

                    <div class="col-md-6">

                      <div class="form-group">

                        <label for="payment_method"><?php echo $this->lang->line('xin_payment_method');?></label>

                        <select name="payment_method" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="<?php echo $this->lang->line('xin_choose_payment_method');?>">

                          <option value=""></option>

                          <?php foreach($get_all_payment_method as $payment_method) {?>

                          <option value="<?php echo $payment_method->payment_method_id;?>"> <?php echo $payment_method->method_name;?></option>

                          <?php } ?>

                        </select>

                      </div>

                    </div>

                    <div class="col-md-6">

                      <div class="form-group">

                        <label for="expense_reference"><?php echo $this->lang->line('xin_acc_ref_no');?></label>

                        <input class="form-control" placeholder="<?php echo $this->lang->line('xin_acc_ref_example');?>" name="bill_reference" type="text">

                        <br />

                      </div>

                    </div>

                </div>

              </div>

              <div class="col-md-5">

                <div class="form-group">

                  <label for="description"><?php echo $this->lang->line('xin_description');?></label>

                  <textarea class="form-control textarea" placeholder="<?php echo $this->lang->line('xin_description');?>" name="description" cols="30" rows="5" id="description"></textarea>

                </div>

                <div class='form-group'>

                  <fieldset class="form-group">

                    <label for="logo"><?php echo $this->lang->line('xin_acc_attach_file');?></label>

                    <input type="file" class="form-control-file" id="bill_file" name="transaction_file">

                  </fieldset>

                </div>

              </div>

            </div>

            <!-- <div class="row">

              <div class="col-md-3">

                <div class="form-group">

                  <label for="payment_method"><?php echo $this->lang->line('xin_payment_method');?></label>

                  <select name="payment_method" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="<?php echo $this->lang->line('xin_choose_payment_method');?>">

                    <option value=""></option>

                    <?php foreach($get_all_payment_method as $payment_method) {?>

                    <option value="<?php echo $payment_method->payment_method_id;?>"> <?php echo $payment_method->method_name;?></option>

                    <?php } ?>

                  </select>

                </div>

              </div>

              <div class="col-md-3">

                <div class="form-group">

                  <label for="expense_reference"><?php echo $this->lang->line('xin_acc_ref_no');?></label>

                  <input class="form-control" placeholder="<?php echo $this->lang->line('xin_acc_ref_example');?>" name="expense_reference" type="text">

                  <br />

                </div>

              </div>

            </div> -->

            <div class="form-actions box-footer">

              <button type="submit" class="btn btn-primary"> <i class="fa fa-check-square-o"></i> Process Bill</button>

            </div>

          </div>

        </div>

        <?php echo form_close(); ?> </div>

    </div>

  </div>

</div>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> Filter Data By Date<?php //echo $query; ?> </h3>

  </div>

  <div class="box-body">

    <?php echo form_open('admin/accounting/new_bulk_bill_payments');?>

    <div class="row">

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-2">

        <div class="form-group">

          <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

        </div>

      </div>

    </div>

    <?php echo form_close(); ?> </div>
</div>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title">All Bulks Payment Processed </h3>

  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>
    
    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table_new2">

        <thead>

          <tr>

            <th>Bulk ID</th>

            <th>Bill Count</th>

            <th>Amount</th>

            <th>Account</th>

            <th>Payment Method</th>

            <th>Ref. ID</th>

            <th>Description</th>

            <th>Date</th>

          </tr>

        </thead>
        <tbody>
          
                  <?php   
                    if(!empty($all_bulk_bill_payments))
                    { 

                        $admin_id = $this->session->userdata;

                        foreach ($all_bulk_bill_payments as $key => $value)
                        {              
                             
                            $admin_info = array();

                            $ci=& get_instance();
                            $ci->load->model('Clients_model'); 

                            $bulk = $ci->Clients_model->get_bulk($value->newbill_tid)->result();

                            $ids = explode(",", $bulk[0]->bulk_id);
                            $bill_count = count($ids);

                            $total = array();

                            foreach($ids as $id)
                            {
                              $diagnose = $ci->Clients_model->get_diagnose($id)->result();
                              array_push($total, $diagnose[0]->diagnose_total_sum);
                            }

                            $total = array_sum($total);

                            // $details = $ci->Clients_model->view_individual_hospital_diagnose_clients($value->newbill_tid);

                            // $hcp = preg_replace('/\s+/', '', $details["hospital_name"]);
                            // $date = date("Ymd", strtotime($details["diagnose_date_time"]));

                  ?>  
                            <tr>
                                <td><?php echo $value->newbill_tid ?></td>
                                <td><?php echo $bill_count; ?>  <a class="btn btn-default" data-toggle="modal" data-target="#myModal" onclick="return loadModalView('<?php echo $bulk[0]->bulk_id; ?>');"><i class="fa fa-eye"></i></a></td>
                                <td><?php echo $this->Xin_model->currency_sign($total); ?></td>
                                <td><?php echo $value->account_name; ?></td>
                                <td><?php echo $value->method_name; ?></td>
                                <td><?php echo $value->newbill_ref; ?></td>
                                <td><?php echo $value->newbill_description; ?></td>
                                <td><?php echo $value->newbill_date; ?></td>
                            </tr> 

                  <?php 
                        }
                    }
                  ?>
        </tbody>

      </table>

    </div>

  </div>

</div>
<?php }else{
  redirect('admin/dashboard','refresh');
} ?>
<script type="text/javascript">
    function loadModalView(id){
          // alert("ID is: " + id);

          $.ajax({
            url      : '<?php echo base_url(); ?>admin/Accounting/fetch_bills_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#fetched_data").html(response);
            }
          });

    }

    function loadModalView2(id){
          // alert("ID is: " + id);
          console.log('asd');

          $.ajax({
            url      : '<?php echo base_url(); ?>admin/Accounting/fetch_premium_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#fetched_data").html(response);
            }
          });

    }

    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable();
        var xin_table_new2 = $('#xin_table_new2').dataTable({
          dom: 'lBfrtip',
          buttons: ['csv', 'excel', 'pdf', 'print']
        }); 
    }, false);

</script>

