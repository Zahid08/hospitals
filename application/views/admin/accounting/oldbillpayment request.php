<?php

/* Subscription view
    
*/
?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>


<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>
<?php

      $role_user = $this->Xin_model->read_user_role_info($user_info[0]->user_role_id);

      if(!is_null($role_user)){

        $role_resources_ids = explode(',',$role_user[0]->role_resources);

      } else {

        $role_resources_ids = explode(',',0); 

      }

?>
<?php if(in_array('574',$role_resources_ids) || in_array('575',$role_resources_ids) || in_array('576',$role_resources_ids) || in_array('577',$role_resources_ids) || in_array('578',$role_resources_ids) || $user_info[0]->user_role_id==1 ) {?>


<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> Filter Data by Selecting a Provider & a Date Range<?php //echo $query; ?> </h3>

  </div>

  <div class="box-body">

    <?php echo form_open('admin/accounting/bill_payment_requests');?>

    <div class="row">

      <div class="col-md-3">
          <select name="hospital_id" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="Select a Provider">

            <option value=""></option>

            <?php 
              foreach($all_hospital as $hospital) { 
            ?>

                <option value="<?php echo $hospital->hospital_id;?>"><?php echo $hospital->hospital_name;?></option>

            <?php } ?>

          </select>
      </div>
      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-2">

        <div class="form-group">

          <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

        </div>

      </div>

    </div>

    <?php echo form_close(); ?> </div>
</div>


<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">
  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>

    <div class="box-datatable table-responsive">

        <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

            <thead>

                <tr> 
                  <?php if(in_array('575',$role_resources_ids) OR in_array('577',$role_resources_ids) OR in_array('578',$role_resources_ids) || $user_info[0]->user_role_id==1) { ?>
                    <th>Approve All</th>
                    <th><?php echo $this->lang->line('xin_action');?></th>
                  <?php } ?>
                    <th>App or Rej By</th>
                    <th>Transaction ID</th>
                    <th>Provider</th>
                    <th>Date</th>
                    <th>Bill</th>
                </tr>

                <tbody> 
                  <?php   
                    $ids = array();
                    if(!empty($xin_diagnose_clients))
                    { 
                        // print_r($xin_diagnose_clients);die;
                        $admin_id = $this->session->userdata;
                        $count = 1;
                        foreach ($xin_diagnose_clients as $key => $value)
                        {              

                            // push all id to one array
                            array_push($ids, $value->diagnose_id);

                            $admin_info = array();

                            $ci=& get_instance();
                            $ci->load->model('Clients_model'); 

                            $services = $ci->Clients_model->read_individual_hospital_diagnose_services($value->diagnose_id);
                            $drugs = $ci->Clients_model->read_individual_hospital_diagnose_drugs($value->diagnose_id);

                            if ($value->diagnose_finance_status != '') {
                                $admin_info = $ci->Clients_model->read_individual_admin_info($value->diagnose_bill_approve_by);
                                // echo $this->db->last_query()."<br />";
                                // print_r($admin_info);
                                // echo "<br />";
                  ?>  
                            <tr>
                              <?php if(in_array('575',$role_resources_ids) OR in_array('577',$role_resources_ids) || $user_info[0]->user_role_id==1) { ?>
                                <?php if($count == 1 && $key <= 9) { ?>
                                  <td rowspan="<?php echo count($xin_diagnose_clients); ?>"> 
                                    <a href="<?php echo base_url(); ?>admin/accounting/bill_payment_requests?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&approve_all=yes&id=<?php echo $value->diagnose_id; ?>&from=<?php echo $_POST['from_date']; ?>&to=<?php echo $_POST['to_date']; ?>"  class="btn btn-info">Approve All</a>
                                    <a class="btn btn-default" data-toggle="modal" data-target="#myModal" onclick="return loadModalViewAll('<?php  echo $from; ?>','<?php  echo $to; ?>','<?php  echo $hid; ?>');"><i class="fa fa-eye">View</i></a>
                                  </td> 
                                <?php $count = 2; } ?>
                                <td>
                                    
                                    <?php if($value->diagnose_finance_status == '2') { ?>
                                        <p class="btn btn-success">Bill Approved</p> 
                                        <!-- <p class="btn btn-warning"> Approve Payment Request</p> --> 
                                    <?php }else { ?>
                                      <?php if (in_array('575',$role_resources_ids) OR in_array('577',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
                                        <a href="<?php echo base_url(); ?>admin/accounting/bill_payment_requests?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&approve=yes&id=<?php echo $value->diagnose_id; ?>"  class="btn btn-info">Approve Bill</a>
                                        
                                      <?php endif ?>

                                        <!-- <a href="<?php echo base_url(); ?>admin/Hospital/diagnose_hospital_clients_bill_requests?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&reject=yes&id=<?php echo $value->diagnose_id; ?>" class="btn btn-warning">Reject</a> -->

                                    <?php } ?>
                                    <a class="btn btn-default m-3" data-toggle="modal" data-target="#myModal" onclick="return loadModalView(<?php echo $value->diagnose_id; ?>);"><i class="fa fa-eye"></i></a>
                                </td>
                              <?php } ?>
                                
                                <td>
                                  <?php 
                                      if($admin_info) {
                                          $n = 1;
                                          if(strlen($value->diagnose_bill_approve_by) == 1){ 
                                            echo "&#8226; ".$admin_info->first_name." ".$admin_info->last_name." "; 
                                          } else {
                                            $ids = explode(",", $value->diagnose_bill_approve_by);
                                            foreach ($ids as $id) {
                                              $admin_info = $ci->Clients_model->read_individual_admin_info($id);
                                              if (!empty($admin_info)) {
                                                 echo "&#8226; ".$admin_info->first_name." ".$admin_info->last_name."<br />";}
                                            }
                                          }
                                      } else {
                                        echo "-----"; 
                                      }
                                  ?>      
                                </td>
                                <?php
                                  $hcp = preg_replace('/\s+/', '', $value->hospital_name);
                                  $date = date("Ymd", strtotime($value->diagnose_date_time));
                                ?>
                                <td><?php if(empty($value->diagnose_transaction_id)) echo "-----"; else echo "TC-".$value->loc_id."-".$date."-".$value->diagnose_transaction_id; ?></td>
                                <td><?php if(empty($value->hospital_name)) echo "-----"; else echo $value->hospital_name; ?></td>
                                <td><?php echo $value->diagnose_date; ?></td>
                                <td>₦<?php echo number_format($value->diagnose_total_sum).".00"; ?></td>
                            </tr> 

                            <?php 
                          }
                        }

                        // print_r($ids);die;
                    }
                        if (!empty($ids)) {
                          $ids = implode(",", $ids);
                        }else{
                          $ids = "";
                        }
                    ?>
                </tbody>

            </thead>

        </table>

    </div>

  </div>

</div>
<?php }else{
  redirect('admin/dashboard','refresh');
} ?> 

<script type="text/javascript">
      function loadModalView(id){
          // alert("ID is: " + id);

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#fetched_data").html(response);
            }
          });

      }

      function loadModalViewAll(from,to,hid){
          // alert("ID is: " + from);
          console.log(from);
          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_all',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : "<?php echo $ids; ?>", from: from, to: to,hid: hid},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#fetched_data").html(response);
            }
          });

      $('#printPDF').click(function(){
          $('#fetched_data').print();
        });
      }

    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
          "buttons": ['csv', 'excel', 'pdf', 'print']
        }); 
    }, false);

     
</script>
 






 