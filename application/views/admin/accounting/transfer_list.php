<?php
/* Accounting > New Expense view
*/
?>
<?php $session = $this->session->userdata('username');?>
<?php $get_animate = $this->Xin_model->get_content_animate();?>
<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>
<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>
<?php if(in_array('574',$role_resources_ids) || in_array('592',$role_resources_ids) || $user_info[0]->user_role_id==1 ) {?>
<div class="box mb-4 <?php echo $get_animate;?>">
  <div id="accordion">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo $this->lang->line('xin_add_new');?> <?php echo $this->lang->line('xin_acc_transfer');?></h3>
    </div>
    <div id="add_form" class="collapse in add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">
      <div class="box-body">
        <?php $attributes = array('name' => 'add_transfer', 'id' => 'xin-form', 'autocomplete' => 'off');?>
        <?php $hidden = array('_user' => $session['user_id']);?>
        <?php echo form_open('admin/accounting/add_transfer', $attributes, $hidden);?>
        <div class="bg-white">
          <div class="box-block">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group" id="select_type">
                  <label for="bank_cash_id">Select a type</label>
                  <select name="select_type" id="select2-demo-6" class="form-control type" data-plugin="select_hrm" data-placeholder="Select a type">
                    <option value=""></option>
                    <option value="1" seleted>Bank Account</option>
                    <option value="2">Ledger</option>
                  </select>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group" id="from_account">
                  <label for="bank_cash_id"><?php echo $this->lang->line('xin_acc_from_account');?> <span id="acc_balance" style="display:none; font-weight:600; color:#F00;"></span></label>
                  <select name="from_bank_cash_id" id="select2-demo-6" class="from-account form-control" data-plugin="select_hrm" data-placeholder="<?php echo $this->lang->line('xin_acc_choose_account_type');?>">
                    <option value=""></option>
                    <?php foreach($all_bank_cash as $bank_cash) {?>
                    <option value="<?php echo $bank_cash->bankcash_id;?>" account-balance="<?php echo number_format($bank_cash->account_balance);?>"><?php echo $bank_cash->account_name;?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group" id="from_ledger">
                  <label for="bank_cash_id">From Ledger <span id="led_balance" style="display:none; font-weight:600; color:#F00;"></span></label>
                  <select name="from_ledger_id" id="select2-demo-6" class="from-ledger form-control" data-plugin="select_hrm" data-placeholder="Select ledger type">
                    <option value=""></option>
                    <?php foreach($all_income_ledger as $income_ledger) {?>
                    <option value="<?php echo $income_ledger->id;?>-i" ledger-balance="<?php echo number_format($income_ledger->balance);?>"><?php echo $income_ledger->name;?> - Income</option>
                    <?php } ?>
                    <?php foreach($all_expense_ledger as $expense_ledger) {?>
                    <option value="<?php echo $expense_ledger->expense_type_id;?>-e" ledger-balance="<?php echo $expense_ledger->balance;?>"><?php echo $expense_ledger->name;?> - Expense</option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="transfer_date"><?php echo $this->lang->line('xin_e_details_date');?></label>
                  <input class="form-control date" placeholder="<?php echo date('Y-m-d');?>" readonly name="transfer_date" type="text">
                </div>
                <div class="form-group">
                  <label for="payment_method"><?php echo $this->lang->line('xin_payment_method');?></label>
                  <select name="payment_method" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="<?php echo $this->lang->line('xin_choose_payment_method');?>">
                    <option value=""></option>
                    <?php foreach($get_all_payment_method as $payment_method) {?>
                    <option value="<?php echo $payment_method->payment_method_id;?>"> <?php echo $payment_method->method_name;?></option>
                    <?php } ?>
                  </select>
                  <input type="hidden" name="account_balance" id="account_balance" value="" />
                  <input type="hidden" name="ledger_balance" id="ledger_balance" value="" />
                </div>
              </div>
              <div class="col-md-3" >
                <div class="form-group" id="to_account">
                  <label for="bank_cash_id"><?php echo $this->lang->line('xin_acc_to_account');?></label>
                  <select name="to_bank_cash_id" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="<?php echo $this->lang->line('xin_acc_choose_account_type');?>">
                    <option value=""></option>
                    <?php foreach($all_bank_cash as $bank_cash) {?>
                    <option value="<?php echo $bank_cash->bankcash_id;?>"><?php echo $bank_cash->account_name;?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group" id="to_ledger">
                  <label for="bank_cash_id">To Ledger</label>
                  <select name="to_ledger_id" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="Select ledger type">
                    <option value=""></option>
                    <option value="" disabled>Income</option>
                    <?php foreach($all_income_ledger as $income_ledger) {?>
                    <option value="<?php echo $income_ledger->id;?>-i" ><?php echo $income_ledger->name;?> - Income</option>
                    <?php } ?>
                    <?php foreach($all_expense_ledger as $expense_ledger) {?>
                    <option value="<?php echo $expense_ledger->expense_type_id;?>-e" ><?php echo $expense_ledger->name;?> - Expense</option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="month_year"><?php echo $this->lang->line('xin_amount');?></label>
                  <input class="form-control" name="amount" type="text" placeholder="<?php echo $this->lang->line('xin_amount');?>">
                </div>
                <div class="form-group">
                  <label for="transfer_reference"><?php echo $this->lang->line('xin_acc_ref_no');?></label>
                  <input class="form-control" placeholder="<?php echo $this->lang->line('xin_acc_ref_example');?>" name="transfer_reference" type="text">
                  <br />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="description"><?php echo $this->lang->line('xin_description');?></label>
                  <textarea class="form-control textarea" placeholder="<?php echo $this->lang->line('xin_description');?>" name="description" cols="30" rows="5" id="description"></textarea>
                </div>
              </div>
            </div>
            <div class="form-actions box-footer">
              <button type="submit" class="btn btn-primary"> <i class="fa fa-check-square-o"></i> <?php echo $this->lang->line('xin_save');?> </button>
            </div>
          </div>
        </div>
        <?php echo form_close(); ?> </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function(){
    $('#from_account').hide();
    $('#to_account').hide();
    $('#from_ledger').hide();
    $('#to_ledger').hide();

    $('#select_type').change(function(){
      var choice = $('.type').val();
      // console.log(choice);
      if(choice == 1){
        $('#from_account').show();
        $('#to_account').show();
        $('#from_ledger').hide();
        $('#to_ledger').hide();
        $('#from_ledger').val("");
        $('#to_ledger').val("");
        // console.log('1');
      }else{
        $('#from_account').hide();
        $('#to_account').hide();
        $('#from_account').val("");
        $('#to_account').val("");
        $('#from_ledger').show();
        $('#to_ledger').show();
        // console.log('2');
      }
    });
  });
</script>
<?php }else{
  redirect('admin/dashboard','refresh');
} ?>