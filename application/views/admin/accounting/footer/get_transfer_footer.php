<?php 
	// $transfer = $this->Finance_model->get_transfer_search($from_date,$to_date);
	if ($type == 'bank') {
		//bank account
		$transfer = $this->Finance_model->get_transfer_search($this->input->get('from_date'),$this->input->get('to_date'))->result();
	}else{
		//ledger
		$transfer = $this->Finance_model->get_transfer_search($this->input->get('from_date'),$this->input->get('to_date'),$type,'i')->result();
		$transfer2 = $this->Finance_model->get_transfer_search($this->input->get('from_date'),$this->input->get('to_date'),$type,'e')->result();
	}
?>
<?php
$total_amount = 0;
foreach($transfer as $r) {
	// amount
	$total_amount += $r->amount;
}

if (isset($transfer2)) {
	foreach($transfer2 as $r) {
	// amount
	$total_amount += $r->amount;
}	
}
?>

<tr>
  <th colspan="5">&nbsp;</th>
  <th><?php echo $this->lang->line('xin_acc_total');?>: <?php echo $this->Xin_model->currency_sign($total_amount);?></th>
</tr>
