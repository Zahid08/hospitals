<?php
/* Accounting > Income Report view
*/
?>
<?php $session = $this->session->userdata('username');?>
<?php $get_animate = $this->Xin_model->get_content_animate();?>
<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('652',$role_resources_ids) || $user_info[0]->user_role_id==1) { ?>
<div class="box mb-4 <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> Select Desired Financial Year</h3>

  </div>

  <div class="box-body">

    <?php echo form_open('admin/accounting/general_ledger_report');?>

    <div class="row">

      <div class="col-md-3">

        <div class="form-group">
  
          <select name="" id="year" class="form-control" data-plugin="select_hrm" data-placeholder="Select a year">
            <option value=""></option>
            <?php  
              $year_from = 1970;
              $year = date('Y');
              for($i = $year;$i >= $year_from;$i--)
              {
                echo "<option value=\"".$i."\">".$i."</option>";
              }
            ?>
          </select>

        </div>

      </div>

      <div class="col-md-2">

        <div class="form-group">
          <a id="export" class="btn btn-primary" href="#">Fetch Data</a>
          <!-- <button type="button" name="get_filter_result" class="btn btn-primary save">Fetch Data</button> -->

        </div>

      </div>

    </div>

    <?php echo form_close(); ?> </div>
</div>


<div class="box <?php echo $get_animate;?>">
  <div class="box-header with-border">
    <h3 class="box-title">General Ledger</h3>
  </div>
  <div class="box-body">
    <div class="box-datatable table-responsive">
      <table class="datatables-demo table table-striped table-bordered" id="xin_table">
        <thead>
          <tr>
            <th>Account ID</th>
            <th>Account Description</th>
            <th>Debit Amount</th>
            <th>Credit Amount</th>
            <th>Ending Balance</th>
          </tr>
        </thead>
        <tfoot id="get_footer">
        </tfoot>
      </table>
    </div>
  </div>
</div>
<?php }else{
  redirect('admin/dashboard','refresh');
} ?>

<script>
  $(document).ready(function(){
    console.log('ready!');

    $('#year').change(function(){
      var year = $('#year').val()
      $('#export').attr("href","<?php echo site_url('admin/Accounting/pdf4/') ?>"+year);
    });

  });
</script>
    