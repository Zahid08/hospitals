<?php

/* Subscription view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('618',$role_resources_ids) || in_array('619',$role_resources_ids)|| $user_info[0]->user_role_id==1) {?>



<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

        <h3 class="box-title">Create a New Health Plan  </h3>

        <div class="box-tools pull-right"> 
            <a class="text-dark collapsed" data-toggle="collapse" href="#add_form" aria-expanded="false">

                <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span> Create New Health Plan </button>

            </a> 
        </div>

    </div>

    <div id="add_form" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">

        <?php $attributes = array('name' => 'add_subscription', 'id' => 'xin-form2', 'autocomplete' => 'off');?>

        <?php $hidden = array('_user' => $session['user_id']);?>

        <?php echo form_open('admin/Subscription/add_subscription', $attributes, $hidden);?>

        <div class="bg-white">

            <div class="box-block">

                <div class="row"> 
                    <div class="col-md-6"> 
                        <div class="row"> 
                            <div class="col-md-12">  
                                <div class="form-group"> 
                                    <label for="company_name">Health Plan</label> 
                                    <input class="form-control" required="" placeholder="Plan Name" name="plan_name" type="text" value=""> 
                                </div>  
                            </div> 
                        </div>   
                    </div> 

                    <div class="col-md-6"> 
                        <div class="row"> 
                            <div class="col-md-12">  
                                <div class="form-group"> 
                                    <label for="company_name">Plan Cost</label> 
                                    <input class="form-control" required  placeholder="Plan Cost" name="plan_cost" type="number" value=""> 
                                </div>  
                            </div> 
                        </div>   
                    </div> 

                    <div class="col-md-12"> 
                        <div class="form-group"> 
                            <label for="training_type">Band Type</label>

                            <select multiple="" class="form-control" name="band_id[]" data-plugin="select_hrm" data-placeholder="Band Type"> 
                                <option value=""></option> 
                                <?php if (isset($all_bands) and !empty($all_bands)): ?>
                                    <?php foreach ($all_bands as $value): ?>
                                        <?php echo "<option value='".$value->band_id."' >".$value->band_name."</option> " ?>
                                    <?php endforeach ?> 
                                <?php endif ?>
                            </select> 
                        </div> 
                    </div> 

                    <div class="col-md-12" id="new_benifit"> 
                        <div class="row"> 
                            <div class="col-md-11">  
                                <div class="form-group"> 
                                    <label for="company_name">Benefit Package</label> 
                                    <!-- <input class="form-control"   name="benifit[]" type="text" value=""> -->
                                    <select  multiple="" class="form-control" required name="benifit[]" data-plugin="select_hrm" data-placeholder="Benefit Package"> 
                                    <option value=""></option> 
                                    <?php if (!empty($all_subscriptions_details_list)): ?>
                                        <?php 
                                        foreach ($all_subscriptions_details_list as $value){
                                            $dd[] = $value['subscription_benifit'];
                                        }
                                         ?>  
                                        <?php foreach (array_unique($dd) as $value): ?>
                                            <?php echo "<option value='".$value."' >".$value."</option> " ?>
                                        <?php endforeach ?> 
                                    <?php endif ?>
                                    </select>   
                                </div>  
                            </div> 
                            <div class="col-md-1">
                                <i class="fa fa-plus add_clicked" style="color: #0F7FFF; padding-top: 50%; font-size: 29px;"></i>
                            </div>
                        </div>   
                    </div> 

                </div>

                
                
                <div class="form-actions box-footer"> 
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-check-square-o"></i> <?php echo $this->lang->line('xin_save');?> </button> 
                </div>

            </div>

        </div>

        <?php echo form_close(); ?> </div>

    </div>

  </div>

</div>



<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

        <thead>

          <tr>

            <th><?php echo $this->lang->line('xin_action');?></th>

            <th>Health Plan</th>

            <th><i class="fa fa-dollar"></i>Cost</th>

            <th>Plan Benefits</th>

            <th>Bands</th>
            
            <th>Created</th>

             
          </tr>
            <tbody>
              
          
                <?php 
                    if(!empty($all_subscriptions))
                    { 
                        foreach ($all_subscriptions as $key => $value)
                        {  
                            $detail_pkg       =  '';
                            $detail_pkg_edit  =  '';

                            $ci=& get_instance();
                            $ci->load->model('Training_model'); 

                            $detail = $ci->Training_model->getAll2('xin_subscription_detail', ' subscription_detail_master_id='. $value->subscription_id.' ');
                            if(!empty($detail))
                            {   

                                foreach ($detail as $benifit) 
                                {
                                    $detail_pkg .= '<p>  '. $benifit->subscription_benifit.'</p>';
                                    $detail_pkg_edit .= '--------'. $benifit->subscription_benifit.'';
                                } 
                            }


                            $bnd_pkg       =  '';
                            $bnd_pkg_edit  =  '';

                            $bands_all_id  = explode(',', $value->band_types);

                            if(!empty($bands_all_id))
                            {   

                                foreach ($bands_all_id as $bands) 
                                {
                                    $bands_dt = $ci->Training_model->getAll2('xin_bands', ' band_id='. $bands.' ');
                                    if(isset($bands_dt[0]->band_name))
                                    {
                                        $bnd_pkg      .= '<p>  '. $bands_dt[0]->band_name.'</p>';
                                        $bnd_pkg_edit .= '--------'. $bands_dt[0]->band_id.'';
                                    }
                                    
                                } 
                            }
                                
                            ?>  
                            <tr>
                                <td> 
                                    <button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light edit_detect" data-toggle="modal" data-target=".edit-modal-data2" 

                                    data-ticket_id="<?php echo isset($value->subscription_id) ? $value->subscription_id : ''; ?>"
                                    data-detail_pkg_edit="<?php echo isset($detail_pkg_edit) ? $detail_pkg_edit : ''; ?>"
                                    data-plan_name="<?php echo isset($value->plan_name) ? $value->plan_name : ''; ?>"
                                    data-plan_cost="<?php echo isset($value->plan_cost) ? $value->plan_cost : ''; ?>"
                                    data-bnd_pkg_edit="<?php echo isset($bnd_pkg_edit) ? $bnd_pkg_edit : ''; ?>"




                                    ><span class="fa fa-pencil"></span></button>

                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                        <button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal2" data-record-id="<?php echo isset($value->subscription_id) ? $value->subscription_id : ''; ?>"><span class="fa fa-trash"></span></button>
                                    </span>   
                                </td>
                                <td><?php echo isset($value->plan_name) ? $value->plan_name : ''; ?> </td>
                                <td><?php echo isset($value->plan_cost) ? ' ₦ '.$value->plan_cost : ''; ?></td> 
                                <td><?php echo isset( $detail_pkg) ?  $detail_pkg : ''; ?></td> 
                                <td><?php echo isset( $bnd_pkg) ?  $bnd_pkg : ''; ?></td> 
                                <td><?php echo isset($value->created_on) ?  strftime('%d-%m-%Y',strtotime($value->created_on)) : ''; ?></td> 
                                
                               
                            </tr> 

                            <?php 
                        }
                    }
                ?>
            </tbody>
        </thead>

      </table>

    </div>

  </div>

</div>
<?php }else{
    redirect('admin/dashboard','refresh');
} ?>
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){ 

         $('#xin_table_new').dataTable();

        $('.add_clicked').on('click',function(){
            var add_row =   `<div class="col-md-12"> 
                                <div class="row"> 
                                    <div class="col-md-11">  
                                        <div class="form-group"> 
                                            <label for="company_name">Benefit Package</label> 
                                            <input class="form-control" name="benifit[]" type="text" value=""> 
                                        </div>  
                                    </div> 
                                    <div class="col-md-1">
                                        <i class="fa fa-trash  delete_benifit" style="color: #ff0d05; padding-top: 50%; font-size: 29px;"></i>
                                    </div>
                                </div>   
                            </div>`;

            $('#new_benifit').after(add_row)
        }) 



        $('.add_clicked_edit').on('click',function(){
            var add_row =   `<div class="col-md-12 removeable_divs"> 
                                <div class="row"> 
                                    <div class="col-md-11">  
                                        <div class="form-group"> 
                                            <label for="company_name">Benefit Package</label> 
                                            <input class="form-control" required   name="benifit[]" type="text" value=""> 
                                        </div>  
                                    </div> 
                                    <div class="col-md-1">
                                        <i class="fa fa-trash  delete_benifit" style="color: #ff0d05; padding-top: 50%; font-size: 29px;"></i>
                                    </div>
                                </div>   
                            </div>`;

            $('#new_benifit_edit').after(add_row)
        }) 


        $('body').on('click', '.delete_benifit', function() {
         
            $(this).parent().parent().parent().remove();
        })


         $("#xin-form2").submit(function(e){
            var fd = new FormData(this);
            var obj = $(this), action = obj.attr('name');
            fd.append("is_ajax", 1);
            fd.append("add_type", 'training');
            fd.append("form", action);
            e.preventDefault();
            $('.icon-spinner3').show();
            $('.save').prop('disabled', true);
            $.ajax({
                url: e.target.action,
                type: "POST",
                data:  fd,
                contentType: false,
                cache: false,
                processData:false,
                success: function(JSON)
                {
                    if (JSON.error != '') {
                        toastr.error(JSON.error);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                            $('.save').prop('disabled', false);
                            $('.icon-spinner3').hide();
                    } else {
                        toastr.success(JSON.result);
                        setTimeout(function(){
                           window.location.reload(1);
                        }, 5000);
                        $('.icon-spinner3').hide();
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                        $('#xin-form2')[0].reset(); // To reset form fields
                        $('.add-form').removeClass('in');
                        $('.select2-selection__rendered').html('--Select--');
                        $('.save').prop('disabled', false);
                    }
                },
                error: function() 
                {
                    toastr.error(JSON.error);
                    $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                    $('.icon-spinner3').hide();
                    $('.save').prop('disabled', false);
                }           
           });
        });



        $("#delete_record2").submit(function(e){
            /*Form Submit*/
            e.preventDefault();
                var obj = $(this), action = obj.attr('name');
                $.ajax({
                    type: "POST",
                    url: e.target.action,
                    data: obj.serialize()+"&is_ajax=2&form="+action,
                    cache: false,
                    success: function (JSON) {
                        if (JSON.error != '') {
                            toastr.error(JSON.error);
                            $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                        } else {
                            $('.delete-modal2').modal('toggle');
                             
                            toastr.success(JSON.result);
                            setTimeout(function(){
                               window.location.reload(1);
                            }, 5000);
                            $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);                 
                        }
                    }
                });
        });


         ////////////////edit  
        jQuery("#ajx_company").change(function(){
            jQuery.get(base_url+"/get_employees/"+jQuery(this).val(), function(data, status){
                jQuery('#employee_ajx').html(data);
            });
        });
        
        $('[data-plugin="select_hrm"]').select2($(this).attr('data-options'));
        $('[data-plugin="select_hrm"]').select2({ width:'100%' });      

            /* Edit data */
        $("#edit_ticket2").submit(function(e){
            var fd = new FormData(this);
            var obj = $(this), action = obj.attr('name');
            fd.append("is_ajax", 1);
            fd.append("edit_type", 'ticket');
            fd.append("form", action);
            e.preventDefault();
            $('.icon-spinner3').show();
            $('.save').prop('disabled', true);
            $.ajax({
                url: e.target.action,
                type: "POST",
                data:  fd,
                contentType: false,
                cache: false,
                processData:false,
                success: function(JSON)
                {
                    if (JSON.error != '') {
                        toastr.error(JSON.error);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                            $('.save').prop('disabled', false);
                            $('.icon-spinner3').hide();
                    } else {
                        // On page load: datatable
                        // var xin_table = $('#xin_table').dataTable({
                        //     "bDestroy": true,
                        //     "ajax": {
                        //         url : "https://hmo.liontech.com.ng/admin/tickets/ticket_list",
                        //         type : 'GET'
                        //     },
                        //     "fnDrawCallback": function(settings){
                        //     $('[data-toggle="tooltip"]').tooltip();          
                        //     }
                        // });
                        
                            toastr.success(JSON.result);

                            setTimeout(function(){
                               window.location.reload(1);
                            }, 4000);
                        
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                        $('.icon-spinner3').hide();
                        $('.edit-modal-data2').modal('toggle');
                        $('.save').prop('disabled', false);
                    }
                },
                error: function() 
                {
                    toastr.error(JSON.error);
                    $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                    $('.icon-spinner3').hide();
                    $('.save').prop('disabled', false);
                }           
            });
        });
        

        $('.edit_detect').on('click',function(){

            $('.removeable_divs').remove();

            var detail_pkg_edit  =  $(this).attr('data-detail_pkg_edit');
            var plan_name        =  $(this).attr('data-plan_name');
            var plan_cost        =  $(this).attr('data-plan_cost');
            var id               =  $(this).attr('data-ticket_id');

            var bnd_pkg_edit     =  $(this).attr('data-bnd_pkg_edit');
            var bnd_pkg_array = bnd_pkg_edit.split("--------")
            $('#band_edit').val(0).trigger('change');
            for (var i = 1; i < bnd_pkg_array.length; i++) 
            {
                // $("#band_edit option[value="+bnd_pkg_array[i]+"]").prop("selected", false).parent().trigger("change");
                $('#band_edit option[value="'+bnd_pkg_array[i]+'"]').attr("selected", "selected");
                // $('#band_edit').attr(bnd_pkg_array[i]).trigger('change')
                     
            }


            var pkg_detail_array = detail_pkg_edit.split("--------")
            for (var i = 1; i < pkg_detail_array.length; i++) 
            {
                if( i== 1)
                {
                    $('#benifit1').val(pkg_detail_array[i]) 
                } else {
                    var add_row =   `<div class="col-md-12 removeable_divs"> 
                                <div class="row"> 
                                    <div class="col-md-11">  
                                        <div class="form-group"> 
                                            <label for="company_name">Benefit Package</label> 
                                            <input class="form-control" required   name="benifit[]" type="text" value="`+pkg_detail_array[i]+`"> 
                                        </div>  
                                    </div> 
                                    <div class="col-md-1">
                                        <i class="fa fa-trash  delete_benifit" style="color: #ff0d05; padding-top: 50%; font-size: 29px;"></i>
                                    </div>
                                </div>   
                            </div>`;
                    $('#new_benifit_edit').after(add_row)

                }    
            } 
            $('#plan_name').val(plan_name);
            $('#plan_cost').val(plan_cost); 
            $('#subs_id').val(id);
        }) 
        // $('body').on("click",'ul.select2-choices', function() {
        //     alert()
        //     $("ul.select2-choices li .select2-search-choice-close").click();
        // });
    }, false);
 


 
    

     

     
 
</script>

<div class="modal fade delete-modal2 animated in" role="dialog" aria-hidden="true"  >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"> 
                <button type="button" aria-label="Close" data-dismiss="modal" class="close"><span aria-hidden="true">×</span></button>
                <strong class="modal-title">Are you sure you want to delete this record?</strong> 
            </div>
            <div class="alert alert-danger">
                <strong>Record deleted can't be restored!!!</strong>
            </div>
            <form action="<?php echo base_url(); ?>admin/Subscription/delete/" name="delete_record" id="delete_record2" autocomplete="off" role="form" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="">
                <input type="hidden" name="csrf_hrsale" value="288e255c59f6126650e28264e712db77">                                                                                                                         
 
                <div class="modal-footer"> 
                    <input type="hidden" name="token_type" value="0" id="token_type">
            
                    <button type="button" data-dismiss="modal" class="btn btn-secondary"><i class="fa fa fa-check-square-o"></i> Close</button>
     
                    <button name="hrsale_form" type="submit" class="btn btn-primary"><i class="fa fa fa-check-square-o"></i> Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>









<!-- edit -->

<div class="modal fade edit-modal-data2 animated in" id="edit-modal-data" role="dialog" aria-labelledby="edit-modal-data" aria-hidden="true"  >
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="ajax_modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                <h4 class="modal-title" id="edit-modal-data">Edit HealthCare Plans</h4>
            </div>

            <form action="<?php echo base_url(); ?>admin/Subscription/update_subscription" name="edit_ticket" id="edit_ticket2" autocomplete="off" class="m-b-1" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="EDIT">
                <input type="hidden" name="_token" value="">
                <input type="hidden" name="subs_id" id="subs_id" value="">
                <input type="hidden" name="ext_name" value="1">
                <input type="hidden" name="csrf_hrsale" value="95edc3390e40ab2fc8a739f96b5ee8aa">
                <div class="modal-body">
                    <div class="box-block">

                        <div class="row"> 
                            <div class="col-md-6"> 
                                <div class="row"> 

                                    <div class="col-md-12">  
                                        <div class="form-group"> 
                                            <label for="company_name">Plan Name</label> 
                                            <input class="form-control" required="" placeholder="Plan Name" id="plan_name" name="plan_name" type="text" value=""> 
                                        </div>  
                                    </div> 
                                </div>   
                            </div> 

                            <div class="col-md-6"> 
                                <div class="row"> 
                                    <div class="col-md-12">  
                                        <div class="form-group"> 
                                            <label for="company_name">Plan Cost</label> 
                                            <input class="form-control" required  placeholder="Plan Cost" id="plan_cost" name="plan_cost" type="number" value=""> 
                                        </div>  
                                    </div> 
                                </div>   
                            </div> 


                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="training_type">Band Type</label>

                                    <select id="band_edit" multiple="" class="form-control" name="band_id[]" data-plugin="select_hrm" data-placeholder="Band Type"> 
                                        <option value=""></option> 
                                        <?php if (isset($all_bands) and !empty($all_bands)): ?>
                                            <?php foreach ($all_bands as $value): ?>
                                                <?php echo "<option value='".$value->band_id."' >".$value->band_name."</option> " ?>
                                            <?php endforeach ?> 
                                        <?php endif ?>
                                    </select> 
                                </div> 
                            </div> 



                            <div class="col-md-12" id="new_benifit_edit"> 
                                <div class="row"> 
                                    <div class="col-md-11">  
                                        <div class="form-group"> 
                                            <label for="company_name">Benefit Package</label> 
                                            <input class="form-control" required   name="benifit[]" type="text" value="" id="benifit1"> 
                                        </div>  
                                    </div> 
                                    <div class="col-md-1">
                                        <i class="fa fa-plus add_clicked_edit" style="color: #0F7FFF; padding-top: 50%; font-size: 29px;"></i>
                                    </div>
                                </div>   
                            </div> 

                        </div>

                        
                     

                    </div>                     
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form> 
        </div>
    </div>
</div>