<?php
// Create Invoice Page
$system_setting = $this->Xin_model->read_setting_info(1);


?>
<?php $get_animate = $this->Xin_model->get_content_animate();?>
<div class="row <?php echo $get_animate;?>">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"> <?php echo $this->lang->line('xin_invoice_create');?> </h3>
      </div>
      <div class="box-body" aria-expanded="true" style="">
        <div class="row m-b-1">
          <div class="col-md-12">
            <?php $attributes = array('name' => 'create_invoice', 'id' => 'xin-form', 'autocomplete' => 'off', 'class' => 'form');?>
            <?php $hidden = array('user_id' => 0);?>
            <?php echo form_open('admin/invoices/create_new_invoice', $attributes, $hidden);?>
            <?php $inv_info = last_client_invoice_info(); $linv = $inv_info + 1;?>
            <div class="bg-white">
              <div class="box-block">
                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="invoice_date"><?php echo $this->lang->line('xin_invoice_number');?></label>
                      <input class="form-control" placeholder="<?php echo $this->lang->line('xin_invoice_number');?>" name="invoice_number" type="text" value="INV-<?php echo '000'.$linv;?>">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="project">Organization</label>
                      <select class="form-control" name="project" id="select_organization" data-plugin="xin_select" data-placeholder="Select an Organization">
                        <option value="">Select Organization</option>
                        <?php if(!empty($all_organizations)) { foreach($all_organizations as $org) { ?>
                        <option value="<?php echo $org->id?>"><?php echo $org->name?></option>
                        <?php } } ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="invoice_date"><?php echo $this->lang->line('xin_invoice_date');?></label>
                      <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_invoice_date');?>" readonly name="invoice_date" type="text" value="">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="invoice_due_date"><?php echo $this->lang->line('xin_invoice_due_date');?></label>
                      <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_invoice_due_date');?>" readonly name="invoice_due_date" type="text" value="">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="invoice_start_date"><?php echo $this->lang->line('xin_invoice_start_date');?></label>
                      <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_invoice_start_date');?>" readonly name="invoice_start_date" type="text" value="">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="invoice_end_date"><?php echo $this->lang->line('xin_invoice_end_date');?></label>
                      <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_invoice_end_date');?>" readonly name="invoice_end_date" type="text" value="">
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-9">
                    <center>
                      <div class="table-responsive">
                      <table class="table table-striped mx-auto">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Enrollees</th>
                            <th>Health Plan</th>
                            <th>Total</th>
                          </tr>
                        </thead>
                        <tbody id="invoiceRow">
                          
                        </tbody>
                      </table>
                    </div>
                    </center>  
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <div class="hrsale-item-values">
                        <div data-repeater-list="items">
                          <div data-repeater-item="">
                          </div>
                        </div>
                      </div>
                      <div id="item-list"></div>
                      <?php

                      $ar_sc = explode('- ',$system_setting[0]->default_currency_symbol);

                      $sc_show = $ar_sc[1];

                      ?>
                      <input type="hidden" class="items-sub-total" name="items_sub_total" value="0" />
                      <input type="hidden" class="items-tax-total" name="items_tax_total" value="0" />
                      <div class="row">
                        <div class="col-md-7 col-sm-12 text-xs-center text-md-left">&nbsp; </div>
                        <div class="col-md-5 col-sm-12">
                          <div class="table-responsive">
                            <table class="table">
                              <tbody>
                                <tr>
                                  <td><?php echo $this->lang->line('xin_title_sub_total2');?></td>
                                  <td class="text-xs-right"><?php echo $sc_show;?> <span class="sub_total">0</span></td>
                                </tr>
                                <!-- <tr>
                                  <td><?php echo $this->lang->line('xin_title_tax_c');?></td>
                                  <td class="text-xs-right"><?php echo $sc_show;?> <span class="tax_total">0</span></td>
                                </tr> -->
                                <tr>
                                  <td colspan="2" style="border-bottom:1px solid #dddddd; padding:0px !important; text-align:left"><table class="table table-bordered">
                                    <tbody>
                                      <tr>
                                        <td width="30%" style="border-bottom:1px solid #dddddd; text-align:left"><strong><?php echo $this->lang->line('xin_discount_type');?></strong></td>
                                        <td style="border-bottom:1px solid #dddddd; text-align:center"><strong><?php echo $this->lang->line('xin_discount');?></strong></td>
                                        <td style="border-bottom:1px solid #dddddd; text-align:left"><strong><?php echo $this->lang->line('xin_discount_amount');?></strong></td>
                                      </tr>
                                      <tr>
                                        <td><div class="form-group">
                                          <select name="discount_type" class="form-control discount_type">
                                            <option value="1"> <?php echo $this->lang->line('xin_flat');?></option>
                                            <option value="2"> <?php echo $this->lang->line('xin_percent');?></option>
                                          </select>
                                        </div></td>
                                        <td align="right"><div class="form-group">
                                          <input style="text-align:right" type="text" name="discount_figure" class="form-control discount_figure" value="0" data-valid-num="required">
                                        </div></td>
                                        <td align="right"><div class="form-group">
                                          <input type="text" style="text-align:right" readonly name="discount_amount" value="0" class="discount_amount form-control">
                                        </div></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
                                </tr>
                                <input type="hidden" class="fgrand_total" name="fgrand_total" value="0" />
                                <tr>
                                  <td><?php echo $this->lang->line('xin_grand_total');?></td>
                                  <td class="text-xs-right"><?php echo $sc_show;?> <span class="grand_total">0</span></td>
                                </tr>
                              </tbody>
                              
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="form-group col-xs-12 mb-2 file-repeaters"> </div>
                      <div class="row">
                        <div class="col-lg-12">
                          <label for="invoice_note"><?php echo $this->lang->line('xin_invoice_note');?></label>
                          <textarea name="invoice_note" class="form-control"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="invoice-footer">
                  <div class="row">
                    <div class="col-md-7 col-sm-12">
                      <p>&nbsp;</p>
                    </div>
                    <div class="col-md-5 col-sm-12 text-xs-center">
                      <button type="submit" name="invoice_submit" class="btn btn-primary pull-right my-1" style="margin-right: 5px;"><i class="fa fa fa-check-square-o"></i> <?php echo $this->lang->line('xin_submit_invoice');?></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php echo form_close(); ?> </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
// $("#select_organization").on('change', function(){
//   // alert("jquery working "+this.value);
//   var id = this.value;
//   $.ajax({
//     url      : '<?php echo base_url(); ?>admin/Invoices/get_all_clients_of_organization',
//     method   : 'post',
//     dataType    : 'text',
//     data     : {id : id},
//     success  : function(response){
//       // $("#id_we_get_is").text(response);
//       // alert(response);
//       $("#org_clients").html(response);
//     }
//   });
// });
// $("#org_clients").on('change', function(){
//   // alert("jquery working "+this.value);
//   var id = this.value;
//   $.ajax({
//     url      : '<?php echo base_url(); ?>admin/Invoices/get_client_subscription_amount',
//     method   : 'post',
//     dataType    : 'text',
//     data     : {id : id},
//     success  : function(response){
//       // $("#id_we_get_is").text(response);
//       alert(response);
//       $("#sub_total").val(response);
//     }
//   });
// });
});
function get_clients(n) {
  var id = $("#select_organization").val();
  $.ajax({
    url      : '<?php echo base_url(); ?>admin/Invoices/get_all_clients_of_organization',
    method   : 'post',
    dataType    : 'text',
    data     : {id : id},
    success  : function(response){
      // $("#id_we_get_is").text(response);
      // alert(response);
      for(var i = 1; i <= n; i++) {
        $("#org_clients"+i).html(response);
      }
    }
  });
}
function client_amount(id,val) {
// alert(id);
// alert("jquery working "+this.value);
var id = id;
$.ajax({
url      : '<?php echo base_url(); ?>admin/Invoices/get_client_subscription_amount',
method   : 'post',
dataType    : 'text',
data     : {id : id},
success  : function(response){
// $("#id_we_get_is").text(response);
// alert(response);
$("#sub_total"+val).val(response);
}
});
}
// function myfunction() {
//   alert("my fucntion calledd ehre");
// }
</script>