<?php
/* Invoice view
*/
?>
<?php $session = $this->session->userdata('username');?>
<?php $system_setting = $this->Xin_model->read_setting_info(1);?>
<?php
// echo $company_name;die;

$client_name = $name;
$client_contact_number = $contact_number;
$client_company_name = $client_company_name;
$client_website_url = $website_url;
$client_address_1 = $address_1;
$client_address_2 = $address_2;
//$client_country = $countryid;
$client_city = $city;
$client_zipcode = $zipcode;
$country = $this->Xin_model->read_country_info($countryid);
if(!is_null($country)){
$client_country = $country[0]->country_name;
} else {
  $client_country = '--';
}
?>
<?php $get_animate = $this->Xin_model->get_content_animate();?>
<div class="row">
  <div class="col-xs-12"> &nbsp; <small class="pull-right">
    <div class="btn-group pull-right" role="group" style="margin-top:2px">
      <?php $inv_record = get_invoice_transaction_record($invoice_id);?>
      <?php if ($status == 0) { ?>
      <a href="<?php echo site_url('admin/invoices/edit/'.$invoice_id);?>" class="btn btn-default btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <?php echo $this->lang->line('xin_edit');?></a>
      <?php } ?>
      <a href="<?php echo site_url('admin/invoices/pdf/'.$invoice_id);?>" class="btn btn-default btn-sm" target="_blank"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>PDF</a>
      <button type="button" id="print-invoice" class="btn btn-vk btn-sm print-invoice"><i class="fa fa-print" aria-hidden="true"></i> <?php echo $this->lang->line('xin_print');?></button>
     </div>
  </small> </div>
  <!-- /.col -->
</div>
<div class="invoice  <?php echo $get_animate;?>" style="margin:10px 10px;">
  <div id="print_invoice_hr">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header"> <i class="fa fa-globe"></i> <?php echo $company_name;?> <small class="pull-right">Date: <?php echo date('d-m-Y');?></small> </h2>
      </div>
      <!-- /.col -->
    </div>
    
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col"> <?php echo $this->lang->line('xin_title_from');?>
        <address>
          <strong><?php echo $company_name;?></strong><br>
          <?php echo $company_address;?><br>
          <?php echo $company_zipcode;?>, <?php echo $company_city;?><br>
          <?php echo $company_country;?><br />
          <?php echo $this->lang->line('xin_phone');?>: <?php echo $company_phone;?>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col"> <?php echo $this->lang->line('xin_title_to');?>
        <address>
          <strong><?php echo $client_name;?></strong><br>
          <?php echo $client_company_name;?><br>
          <?php echo $client_address_1.' '.$client_address_2.' '.$client_city;?><br>
          <?php echo $this->lang->line('xin_phone');?>: <?php echo $client_contact_number;?><br>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col"> <b><?php echo $this->lang->line('xin_invoice_no');?> <?php echo $invoice_number;?></b><br>
        <br>
        <b><?php echo $this->lang->line('xin_e_details_date');?>:</b> <?php echo $this->Xin_model->set_date_format($invoice_date);?><br>
        <b><?php echo $this->lang->line('xin_payment_due');?>:</b> <?php echo $this->Xin_model->set_date_format($invoice_due_date);?><br />
        <div id="status"></div>
      </div>

      <!-- /.col -->
    </div>
    <!-- /.row -->
   
    <!-- Table row -->
    <!-- <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th class="py-3"> # </th>
              <th class="py-3"> <?php echo $this->lang->Line('xin_title_item');?> </th>
              <th class="py-3"> <?php echo $this->lang->line('xin_title_tax_rate');?> </th>
              <th class="py-3"> <?php echo $this->lang->line('xin_title_qty_hrs');?> </th>
              <th class="py-3"> <?php echo $this->lang->line('xin_title_unit_price');?> </th>
              <th class="py-3"> <?php echo $this->lang->line('xin_title_sub_total');?> </th>
            </tr>
          </thead>
          <tbody>
            <?php
                    $ar_sc = explode('- ',$system_setting[0]->default_currency_symbol);
                    $sc_show = $ar_sc[1];
            ?>
            <?php $prod = array(); $i=1; foreach($this->Invoices_model->get_invoice_items($invoice_id) as $_item):?>
            <tr>
              <td class="py-3"><div class="font-weight-semibold"><?php echo $i;?></div></td>
              <td class="py-3" style="width:"><div class="font-weight-semibold"><?php echo $_item->item_name;?></div></td>
              <td class="py-3"><strong><?php echo $this->Xin_model->currency_sign($_item->item_tax_rate);?></strong></td>
              <td class="py-3"><strong><?php echo $_item->item_qty;?></strong></td>
              <td class="py-3"><strong><?php echo $this->Xin_model->currency_sign($_item->item_unit_price);?></strong></td>
              <td class="py-3"><strong><?php echo $this->Xin_model->currency_sign($_item->item_sub_total);?></strong></td>
            </tr>
            <?php endforeach;?>
          </tbody>
        </table>
      </div> -->
      <!-- /.col -->
    <!-- </div> -->
    <!-- /.row -->
    
    <div class="row">
      <!-- /.col -->
      <div class="col-xs-6">
        <?php if($invoice_note == ''):?>
        &nbsp;
        <?php else:?>
        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;"> <?php echo $invoice_note;?> </p>
        <?php endif;?>
      </div>
      <div class="col-lg-6">
        <div class="table-responsive">
          <table class="table">
            <tbody>
              <tr>
                <th style="width:50%"><?php echo $this->lang->line('xin_title_sub_total');?>:</th>
                <td><?php echo $this->Xin_model->currency_sign($sub_total_amount);?></td>
              </tr>
              <tr>
                <th><?php echo $this->lang->line('xin_title_tax_c');?></th>
                <td><?php echo $this->Xin_model->currency_sign($total_tax);?></td>
              </tr>
              <tr>
                <th><?php echo $this->lang->line('xin_discount');?>:</th>
                <td><?php echo $this->Xin_model->currency_sign($total_discount);?></td>
              </tr>
              <tr>
                <th><?php echo $this->lang->line('xin_acc_total');?>:</th>
                <td><?php echo $this->Xin_model->currency_sign($grand_total);?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
</div>

<!-- Edit Invoice Status Modal -->
<div class="modal fade" id="editInvoiceStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Change Invoice Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?= base_url() ?>admin/invoices/changeStatus">
          <input type="text" name="id" value="<?= $invoice_id; ?>" hidden>
          <input type="text" name="grand_total" value="<?= $grand_total; ?>" hidden>
          <div class="form-group">
            <label for="invoiceStatus">Invoice Status</label>
            <select name="statusInvoice" id="invoiceStatus" class="form-control">
              <!-- Status invoice will show -->
              <option value="0" <?php if($status == 0) echo "selected";if($status == 1) echo "disabled";?>>Unpaid</option class="form-control">
              <option value="1" <?php if($status == 1) echo "selected"; ?> class="form-control">Paid</option>
            </select>
          </div>

          <div class="form-group" style="display: none;" id="bank">
            <label for="bankAccount">Select Bank Account</label>
            <select name="bank" id="bankAccount" class="form-control">
              <option value="1" class="form-control">Ecobank</option>
              <option value="2" class="form-control">FCMB</option>
              <option value="3" class="form-control">GT Bank</option>
            </select>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div> 

<script>  
    $(document).ready(function(){
      if(<?php echo $status; ?> == 0){
              //Add change button for modal
              $_status = '<span class="label label-danger"><?php echo $this->lang->line('xin_payroll_unpaid'); ?></span><a href="#" role="button" data-toggle="modal" data-target="#editInvoiceStatus" id="buttonChange">  Change</a>';
            } else if(<?php echo $status; ?> == 1) 
            {
              $_status = '<span class="label label-success"><?php echo $this->lang->line('xin_payment_paid'); ?></span><a href="#" role="button" data-toggle="modal" data-target="#editInvoiceStatus" id="buttonChange">  Change</a>';
            } else {
              $_status = '<span class="label label-info">Pending</span><a href="#" role="button" data-toggle="modal" data-target="#editInvoiceStatus" id="buttonChange">  Change</a>';
            }
            $("#status").html($_status);
    });

</script>