<?php

/* Client view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']); ?>

<?php if(in_array('556',$role_resources_ids) || in_array('557',$role_resources_ids) || in_array('558',$role_resources_ids) || in_array('559',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>


<!-- <div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title">Enrollee Account Creation</h3>

      <div class="box-tools pull-right"> <a class="text-dark collapsed" data-toggle="collapse" href="#add_form" aria-expanded="false">

        <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span> Create New Enrollee Account</button>

        </a> </div>

    </div>

    <div id="add_form" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">

        <?php $attributes = array('name' => 'add_client', 'id' => 'xin-form', 'autocomplete' => 'off');?>

        <?php $hidden = array('user_id' => $session['user_id']);?>

        <?php echo form_open('admin/clients/add_client', $attributes, $hidden);?>

        <div class="form-body">

        <div class="row">

            <div class="col-md-6">

                <div class="form-group">

                    <label for="client_name">Enrollee Name</label>

                    <input class="form-control" placeholder="Enrollee Name" name="name" type="text">

                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="company_name">Select organization</label>
                    <select class="form-control" name="company_name" data-plugin="xin_select" data-placeholder="Select Organization">

                        <option value="">Select Organization</option>

                        <?php foreach($all_organizations as $organization) {?>

                        <option value="<?php echo $organization->id;?>"> <?php echo $organization->name;?></option>

                        <?php } ?>

                    </select> 
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="contact_number">Phone Number</label>

                    <input class="form-control" placeholder="Phone Number" name="contact_number" type="text">
                </div>
            </div>

               

            <div class="col-md-6">
                <div class="form-group">
                    <label for="website"><?php echo $this->lang->line('xin_employee_password');?></label>

                    <input class="form-control" placeholder="<?php echo $this->lang->line('xin_employee_password');?>" name="password" type="text">
                </div>
            </div>
        </div>
         <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="company_name">Gender</label>
                    <select class="form-control" id="c_gender" name="gender" data-plugin="xin_select" data-placeholder="Select Gender">
                        <option value="">Select Gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="company_name">DOB</label>
                    <input class="form-control" placeholder="DOB" name="dob" type="date"  id="c_dob">
                </div>
            </div>
        </div>
         <div class="row"> 
            <div class="col-md-6">
                <div class="form-group">

                    <label for="email"><?php echo $this->lang->line('xin_email');?></label>

                    <input class="form-control" placeholder="<?php echo $this->lang->line('xin_email');?>" name="email" type="email">
                </div>
            </div>

                 

            <div class="col-md-6">
                <div class="form-group">
                    <label for="subscription_ids">Select Healthcare Plan</label>

                  

                    <select   class="form-control" name="subscription_ids[]" data-plugin="xin_select" data-placeholder="Select Healthcare Plan">

                        <option value="">Select Subscription</option>

                        <?php foreach($all_subscriptions as $subscription) {?>

                        <option value="<?php echo $subscription->subscription_id;?>"> <?php echo $subscription->plan_name;?></option>

                        <?php } ?>

                    </select> 
                </div>
            </div>     
        </div>



        <div class="row"> 

            <div class="col-md-6">
                <div class="form-group">
                    <label for="subscription_ids">Select Account type</label> 

                    <select   class="form-control" name="ind_family" data-plugin="xin_select" data-placeholder="Select Individual or Family">

                        <option value="">Select Account type</option> 

                        <option value="individual">Individual  </option>
                        <option value="family">Family  </option>
 
                    </select> 
                </div>
            </div> 



            <div class="col-md-3">

                <fieldset class="form-group">

                    <label for="logo"><?php echo $this->lang->line('xin_project_client_photo');?></label>

                    <input type="file" class="form-control-file" id="client_photo" name="client_photo">

                    <small><?php echo $this->lang->line('xin_company_file_type');?></small>

                </fieldset>

            </div>

        </div>
 
 
        </div>

        <div class="form-actions box-footer">

            <button type="submit" class="btn btn-primary"> <i class="fa fa-check-square-o"></i> Create Account </button>

        </div>

        <?php echo form_close(); ?> </div>

    </div>

  </div>

</div>
 -->

<?php if (in_array('559',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>  
    <style type="text/css">
        .sorting_1{
            visibility: hidden;
        }
    </style>
<div class="box <?php echo $get_animate;?>">

    <div class="box-header with-border"> 

         <div class="box-header with-border">

        <h3 class="box-title">Send Notification</h3>

        <div class="box-tools pull-right"> 
            <a class="text-dark collapsed" data-toggle="collapse" href="#upload" aria-expanded="false">

                <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span> Open</button>

            </a> 
        </div>

    </div>

    <div id="upload" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">
    <div class="row">
        <div class="col-md-6">
            <textarea class="form-control"> Send Message </textarea>
        </div>
    </div> 

    </div>

    </div>

  </div>

</div>   
<?php endif ?>
    
<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table">

        <thead>

            <tr> 
                <!-- <th><?php echo $this->lang->line('xin_action');?></th> -->

                <th width="10%">Enrollee ID</th>
                
                <th width="20%">Enrollee</th>

                <th width="15%">Organization</th>

                <th width="10%">Plan</th>

                <th width="10%">Email Address</th>

                <th width="10%">Phone</th> 
                
                <th width="5%">Status</th>
                
            </tr>

        </thead>

      </table>

    </div>

  </div>

</div>

    <div class="modal fadeInRight edit-modal-data2 animated" id="edit-modal-data" role="dialog" aria-labelledby="edit-modal-data" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" id="ajax_modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    <h4 class="modal-title" id="edit-modal-data"><i class="icon-pencil7"></i> Edit Enrollee</h4>
                </div>
                <form action="<?php echo base_url() ?>admin/clients/update" name="edit_client" id="edit_client" autocomplete="off" class="m-b-1" method="post" accept-charset="utf-8">
                    <input type="hidden" name="_method" value="EDIT">
                    <input type="hidden" name="_token"  id="_token">
                    <input type="hidden" name="ext_name" value="Shaleena">
                    <input type="hidden" name="csrf_hrsale" value="467ba7724bc88bcd9af14ffeb6201eff">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">First Name</label>
                                    <input class="form-control" placeholder="Enrollee Name" name="name" type="text"  id="c_name">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">Last Name</label>
                                    <input class="form-control" placeholder="Enrollee Last Name" name="last_name" type="text"  id="c_lname">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">Other Name</label>
                                    <input class="form-control" placeholder="Enrollee Other Name" name="other_name" type="text"  id="c_oname">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">Gender</label>
                                    <select class="form-control" id="c_gender" name="gender" data-plugin="xin_select" data-placeholder="Select Gender">
                                        <option value="">Select Gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">DOB</label>
                                    <input class="form-control" placeholder="DOB" name="dob" type="date"  id="c_dob">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">Marital Status</label>
                                    <select class="form-control" id="c_marital" name="marital_status" data-plugin="xin_select" data-placeholder="Select Marital Status">
                                        <option value="">Select Status</option>
                                        <option value="single">Single</option>
                                        <option value="married">Married</option>
                                        <option value="divorced">Divorced</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">Organization Name</label>
                                    <select class="form-control" id="c_orga" name="company_name" data-plugin="xin_select" data-placeholder="Select Organization">
                                        <option value="">Select Organization</option>
                                        <?php foreach($all_organization as $organization) {?>
                                        <option value="<?php echo $organization->id;?>"> <?php echo $organization->name;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="contact_number">Phone Number</label>
                                    <input class="form-control" placeholder="Phone Number" name="contact_number" type="text" id="c_phone" value="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input class="form-control" id="c_email" placeholder="Email" name="email" type="email" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">Address</label>
                                    <input class="form-control" id="c_address" placeholder="address" name="address_1" type="text" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">State</label>
                                    <select class="form-control" id="c_state" name="state" data-plugin="xin_select" data-placeholder="Select State">
                                        <option value="">Select State</option>
                                        <?php foreach($all_state as $state) {?>
                                            <option value="<?php echo $state->location_id;?>"> <?php echo $state->location_name;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subscription_ids">Select Healthcare Plan</label>
                                <select id="c_subs" class="form-control" name="subscription_ids[]" data-plugin="xin_select" data-placeholder="Select Healthcare Plan">
                                    <option value="">Select Healthcare Plan</option>
                                    <?php foreach($all_subscription as $subscription) {?>
                                    <option value="<?php echo $subscription->subscription_id;?>"> <?php echo $subscription->plan_name;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subscription_ids">Select Hospital</label>
                                <select id="c_hospital" class="form-control" name="hospital_id" data-plugin="xin_select" data-placeholder="Select Hospital">
                                    <option value="">Select Hospital</option>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="status" class="control-label">Status</label>
                                <select class="form-control  select2" name="status" id="c_status"  data-placeholder="Status" >
                                    <option value="0">Inactive</option>
                                    <option value="1" >Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            
                            <div class="form-group">
                                <label for="subscription_ids">Select Account type</label>
                                <select   class="form-control" id="ind_family" name="ind_family" data-plugin="xin_select" data-placeholder="Select Account type">
                                    <option value="">Select Account type</option>
                                    <option value="individual">Individual  </option>
                                    <option value="family">Family  </option>
                                    
                                </select>
                            </div>
                            
                        </div>
                        <div class="col-sm-6">
                            <fieldset class="form-group">
                                
                                
                                <label for="logo">Profile Photo</label>
                                <small id="upload-text">Upload files only: gif,png,jpg,jpeg</small>
                                <input type="file" class="form-control-file" name="client_photo">
                                
                                <span class="avatar box-48 mr-0-5"> <img id="clientPhoto" class="user-image-hr46 ui-w-100 rounded-circle" alt="No photo"> </span>
                                
                            </fieldset>
                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label for="diseases">Diseases</label>

                                <div class="row disease">
                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Sickle Cell Disease" id="sickle">   Sickle Cell Disease    
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Kidney Disease" id="kidney">   Kidney Disease  
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Epilepsy" id="epilepsy">   Epilepsy
                                    </div>

                                    <div class="col-md-3 form-group" style="font-size: small;">
                                        <input type="checkbox" name="diseases[]" value="Cancer(Prostate-Cervical)" id="cancer">   Cancer(Prostate,Cervical)    
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Diaberes Mellitus" id="diabetes">   Diaberes Mellitus
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Asthma" id="asthma">   Asthma
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="HIV-AIDS" id="hiv">   HIV/AIDS  
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Surgeries" id="surgeries">   Surgeries
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Hypertension" id="hypertension">   Hypertension
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Cataract" id="cataract">   Cataract  
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Goiter" id="goiter">   Goiter  
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Peptic Ulcer" id="peptic">   Peptic Ulcer
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Hepatitis" id="hepatitis">   Hepatitis    
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Glaucoma" id="glaucoma">   Glaucoma  
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Tuberculosis" id="tuberculosis">   Tuberculosis
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Hemorrhoids" id="hemorrhoids">   Hemorrhoids    
                                    </div>
                                    
                                </div>                                

                            </div> 
                    </div>

                    <div class="row">
                        <div  class="col-md-12">
                                <div class="form-group">
                                    <label>Disease Comment</label>
                                    <textarea class="form-control" rows="5" name="comment" id="comment"></textarea>
                                </div>
                            </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save" id="update">Update</button>
                </div>
            </form>
            <script type="text/javascript">
            document.addEventListener('DOMContentLoaded', function(){
            $('[data-plugin="select_hrm"]').select2($(this).attr('data-options'));
            $('[data-plugin="select_hrm"]').select2({ width:'100%' });
            /* Edit data */
            $("#edit_client").submit(function(e){
                var fd = new FormData(this);
                var obj = $(this), action = obj.attr('name');
                fd.append("is_ajax", 2);
                fd.append("edit_type", 'client');
                fd.append("form", action);
                e.preventDefault();
                $('.save').prop('disabled', true);
                $.ajax({
                    url: e.target.action,
                    type: "POST",
                    data:  fd,
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(JSON)
                    {
                        if (JSON.error != '') {
                            toastr.error(JSON.error);
                            $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                            $('.save').prop('disabled', false);
                        } else {
                    // On page load: datatable
                            var xin_table = $('#xin_table').dataTable({
                                "order": [[5,'desc']],
                                "bDestroy": true,
                                "ajax": {
                                url : "<?php  echo base_url(); ?>admin/clients/clients_list",
                                type : 'GET'
                                },
                                dom: 'lBfrtip',
                                "buttons": ['csv', 'excel', 'pdf', 'print'], // colvis > if needed
                                "fnDrawCallback": function(settings){
                                    $('[data-toggle="tooltip"]').tooltip();
                                }
                            });
                            xin_table.api().ajax.reload(function(){
                                toastr.success(JSON.result);
                            }, true);
                            $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                            $('.edit-modal-data2').modal('toggle');
                            $('.save').prop('disabled', false);
                        }
                    },
                    error: function()
                    {
                        toastr.error(JSON.error);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                        $('.save').prop('disabled', false);
                    }
                });
            });


            function get_clients(n) {
              var id = n;
              $.ajax({
                url      : '<?php echo base_url(); ?>admin/Hospital/get_hospital_by_subs',
                method   : 'post',
                dataType    : 'text',
                data     : {id : id},
                success  : function(response){
                  // $("#id_we_get_is").text(response);
                  // alert(response);
                  $('#c_hospital').html(response);
                  console.log(response);
                }
              });
            }
            $('body').on('click', '.edit_client_detail', function() {
                $('.disease input[type="checkbox"]').attr("checked",false);

                var c_name     = $(this).attr('data-c_name');
                var c_lname     = $(this).attr('data-c_lname');
                var c_oname     = $(this).attr('data-c_oname');
                var c_orga     = $(this).attr('data-c_orga');
                var c_hospital     = $(this).attr('data-c_hospital');
                var c_email    = $(this).attr('data-email');
                var c_phone    = $(this).attr('data-phone');
                var c_status   = $(this).attr('data-status');
                var c_gender   = $(this).attr('data-gender');
                var c_address   = $(this).attr('data-address');
                var c_dob   = $(this).attr('data-dob');
                var c_subs   = $(this).attr('data-c_subs');
                var c_marital   = $(this).attr('data-marital');
                var c_state   = $(this).attr('data-state');
                var clientPhoto   = $(this).attr('data-client_photo');
                var _token     = $(this).attr('data-client_id');
                var detail_pkg_edit     = $(this).attr('data-detail_pkg_edit');
                var ind_family     = $(this).attr('data-ind_family');
                var diseases     = $(this).attr('data-diseases');
                var comment     = $(this).attr('data-comment');
                var ve = $(this).attr('data-ve');

                console.log(c_name);

                var diseases = diseases.split(",");
                console.log(diseases);

                if(jQuery.inArray("Sickle Cell Disease",diseases) !== -1){
                    $('#sickle').attr("checked","checked")
                    console.log('got sickle');
                }

                if(jQuery.inArray("Kidney Disease",diseases)!== -1){
                    $('#kidney').attr("checked","checked")
                    // console.log('got kidney');
                }

                if(jQuery.inArray("Cancer(Prostate,Cervical)",diseases) !== -1){
                    $('#cancer').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Epilepsy",diseases) !== -1){
                    $('#epilepsy').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Diaberes Mellitus",diseases) !== -1){
                    $('#diabetes').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Asthma",diseases) !== -1){
                    $('#asthma').attr("checked","checked")
                    console.log('got asthma');
                }

                if(jQuery.inArray("HIV-AIDS",diseases) !== -1){
                    $('#hiv').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Surgeries",diseases) !== -1){
                    $('#surgeries').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Hypertension",diseases) !== -1){
                    $('#hypertension').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Cataract",diseases) !== -1){
                    $('#cataract').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Goiter",diseases) !== -1){
                    $('#goiter').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Peptic Ulcer",diseases) !== -1){
                    $('#peptic').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Hepatitis",diseases) !== -1){
                    $('#hepatitis').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Glaucoma",diseases) !== -1){
                    $('#glaucoma').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Tuberculosis",diseases) !== -1){
                    $('#tuberculosis').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Hemorrhoids",diseases) !== -1){
                    $('#hemorrhoids').attr("checked","checked")
                    // console.log('got cancer');
                }

                var subs_array = detail_pkg_edit.split("--------")
                $('#subscription_ids').val(0).trigger('change');
                for (var i = 1; i < subs_array.length; i++)
                {
                // $("#band_edit option[value="+bnd_pkg_array[i]+"]").prop("selected", false).parent().trigger("change");
                $('#subscription_ids option[value="'+subs_array[i]+'"]').attr("selected", "selected");
                // $('#band_edit').attr(bnd_pkg_array[i]).trigger('change')
                
                }
                var profile = 'https://hmo.liontech.com.ng/uploads/clients/'+clientPhoto;

                get_clients(c_hospital);


                $('#c_name').val(c_name);
                $('#c_lname').val(c_lname);
                $('#c_oname').val(c_oname);
                $('#c_orga').val(c_orga).trigger('change');
                $('#c_status').val(c_status).trigger('change');
                $('#ind_family').val(ind_family).trigger('change');
                $('#c_email').val(c_email);
                $('#c_phone').val(c_phone);
                $('#c_gender').val(c_gender).trigger('change');
                $('#c_address').val(c_address);
                $('#comment').text(comment);
                $('#c_dob').val(c_dob).trigger('change');
                $('#c_marital').val(c_marital).trigger('change');
                $('#c_subs').val(c_subs).trigger('change');
                $('#c_state').val(c_state).trigger('change');
                var imageSrc = document.getElementById('clientPhoto')
                imageSrc.src = " <?php echo base_url() ?>uploads/clients/" + clientPhoto;
                // alert(profile);
                $('#_token').val(_token);

            })

            $('body').on('change', '#c_subs', function() { 
                var sub_id = $(this).val();
                get_clients(sub_id);
            })
            }, false);
            
            </script>
        </div>
    </div>
<script type="text/javascript">
    setInterval(function(){    
    var sort = document.getElementsByClassName('sorting_1');
    for (var i = 0; i < sort.length; i++) {
        sort[i].remove();
    }
         }, 1000);
</script>
<?php 
    }else{
        redirect('admin/dashboard','refresh');
    } 

?>
