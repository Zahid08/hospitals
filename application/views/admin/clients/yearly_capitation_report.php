<?php

/* Accounting > New Expense view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('610',$role_resources_ids) || in_array('617',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>


<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"> Filter Results By Selecting a Date Range </h3>

    </div>

    <div class="<?php echo $get_animate;?>" style="">

      <div class="box-body">

            <?php $year = date('Y'); $start = 2000; ?>

            <?php echo form_open('admin/clients/yearly_capitation_report');?>

            <div class="row">
              <div class="col-md-3">
                <div class="form-group">  
                    <input type="text" class="form-control date" placeholder="Select a date" required="required" name="from" id="date_invoice_from" style="border-radius: 1.5rem" readonly>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">  
                    <input type="text" class="form-control date" placeholder="Select a date" required="required" name="to" id="date_invoice_to" style="border-radius: 1.5rem" readonly>
                </div>
              </div>

              <div class="col-md-2">

                <div class="form-group">

                  <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

                </div>

              </div>

            </div>

            <?php echo form_close(); ?> 
        </div>

    </div>

  </div>

</div>



<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> NHIS Capitation from <?php echo $from." to ".$to;?> | Total Beneficiaries: <span id="total"></span> | Total Amount: <span id="total_amount"></span></h3>

  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>
    
    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

        <thead>

          <tr>

                <th>HCP Code</th>

                <th>Hospital</th>

                <th>Principals</th>

                <th>Dependants</th>

                <th>Ext Dep</th> 
                
                <th>Voluntary</th>

                <th>Beneficiaries</th>

          </tr>

        </thead>
        <tbody>
          
                  <?php   
                    if(!empty($all_hospital))
                    { 

                        $admin_id = $this->session->userdata;
                        $total = array();
                        // print_r($all_hospital->result());die;
                        foreach ($all_hospital as $key => $value)
                        {              

                            $ci=& get_instance();
                            $ci->load->model('Clients_model');

                            if ($from == '' AND $to == '') {
                              $principal = $ci->Clients_model->get_all_capitation_hospital2($value->hcp_code,'p')->num_rows(); 
                              $dependant = $ci->Clients_model->get_all_capitation_hospital2($value->hcp_code,'d')->num_rows(); 
                              $ext_dependant = $ci->Clients_model->get_all_capitation_hospital2($value->hcp_code,'d',1)->num_rows(); 
                            }else{
                              $principal = $ci->Clients_model->get_all_capitation_hospital3($value->hcp_code,'p',0,$from,$to)->num_rows(); 
                              $dependant = $ci->Clients_model->get_all_capitation_hospital3($value->hcp_code,'d',0,$from,$to)->num_rows(); 
                              $ext_dependant = $ci->Clients_model->get_all_capitation_hospital3($value->hcp_code,'d',1,$from,$to)->num_rows(); 
                            }

                            array_push($total, $principal);
                            array_push($total, $dependant);
                            array_push($total, $ext_dependant);

                  ?>  
                              <tr>
                                <td><?php echo $value->hcp_code != '' ? $value->hcp_code : "-"; ?></td> 
                                <td><?php echo $value->hospital_name != '' ? $value->hospital_name : "-"; ?></td> 
                                <td><?php echo $principal; ?></td> 
                                <td><?php echo $dependant; ?></td> 
                                <td><?php echo $ext_dependant; ?></td> 
                                <td><?php echo "0"; ?></td> 
                                <td><?php echo $principal+$dependant+$ext_dependant; ?></td> 
                              </tr> 

                  <?php 
                          }
                    }
                  ?>
        </tbody>

      </table>

    </div>

  </div>

</div>
<?php }else{
  redirect('admin/dashboard','refresh');
} ?>
<script type="text/javascript">
  $(document).ready(function(){
    $('#total').text('<?php echo array_sum($total); ?>');    
    $('#total_amount').text('<?php echo $this->Xin_model->currency_sign(array_sum($total)*5000); ?>');    
  })


    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
            "buttons": ['csv', 'excel', 'pdf', 'print']
        }); 
    }, false);

</script>

<script type="text/javascript">
  function loadModalView(id){
          // alert("ID is: " + id);
          $.ajax({
            url      : '<?php echo base_url(); ?>admin/Clients/fetch_profile_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              $("#client_profile").html(response);
            }
          });

      }
</script>

 <!-- Modal -->
  <div class="modal fade" id="myModalProfile" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Client's Profile</h3>
        </div>
        <div class="modal-body" id="client_profile">

        </div>
      
        <div class="clearfix"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>