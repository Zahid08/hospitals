<?php

/* Accounting > New Expense view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('594',$role_resources_ids) || in_array('608',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>




<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">


  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>
    
    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

        <thead>

          <tr>

            <th>Enrollee</th>

            <th>Type</th>

            <th>Age</th>

            <th>Subscription</th>

            <th>Provider</th>

            <th>Organization</th>

          </tr>

        </thead>
        <tbody>
          <?php   
            if(!empty($all_clients))
            { 
              // print_r($all_clients->result());die;

                $admin_id = $this->session->userdata;

                // print_r($all_clients);die;
                foreach ($all_clients as $key => $value)
                {              
                    $admin_info = array();

                    $ci=& get_instance();
                    $ci->load->model('Clients_model'); 
                    
                    $client = $ci->Clients_model->read_client_info($value);

                    $client = $client[0];

                    $subs = $this->Clients_model->get_clients_subscription($client->subscription_ids)->result();

                    // print_r($subs);die;

                    $hospital = $this->Clients_model->get_hospital_info($client->hospital_id)->result();

                    $organization = $this->Clients_model->get_organization_info($client->company_name)->result();
                    // $subs = $this->Clients_model->read_client_info($value->diagnose_client_id);

                    // print_r($subs);die;

                    // $plan = $this->Clients_model->get_clients_subscription($subs[0]->subscription_ids)->result();

                    // $percentage = $value->diagnose_total_sum/$plan[0]->plan_cost*100;
                    // array_push($total_amount, $value->diagnose_total_sum);
                    // array_push($total_percentage, $percentage);
                    // print_r(!empty($value->diagnose_date));die;
                    // $hospital = $ci->Clients_model->get_hospital_info($value->hospital_id)->result();
                  ?>  
                      <tr>
                          <td><?php echo $client->name ; ?></td>
                          <td><?php echo "Principal" ?></td>
                          <td><?php echo date_diff(date_create($client->dob), date_create('today'))->y; ?></td>
                          <td><?php echo !empty($subs) ? $subs[0]->plan_name : '-' ; ?></td>
                          <td><?php echo !empty($hospital) ? $hospital[0]->hospital_name : '-'; ?></td>
                          <td><?php echo !empty($organization) ? $organization[0]->name : '-' ; ?></td>        
                      </tr> 

          <?php 
              } 
            }

            if(!empty($all_dependants))
            { 
              // print_r($all_clients->result());die;

                $admin_id = $this->session->userdata;

                // print_r($all_clients);die;
                foreach ($all_dependants as $key => $value)
                {              
                    $admin_info = array();

                    $ci=& get_instance();
                    $ci->load->model('Clients_model'); 
                    
                    $client = $ci->Clients_model->read_dependant_info($value);

                    $client = $client[0];

                    $c = $ci->Clients_model->read_client_info($client->client_id);
                    $c = $c[0];

                    $subs = $this->Clients_model->get_clients_subscription($c->subscription_ids)->result();

                    $hospital = $this->Clients_model->get_hospital_info($c->hospital_id)->result();

                    $organization = $this->Clients_model->get_organization_info($c->company_name)->result();

                    // $subs = $this->Clients_model->read_client_info($value->diagnose_client_id);

                    // print_r($subs);die;

                    // $plan = $this->Clients_model->get_clients_subscription($subs[0]->subscription_ids)->result();

                    // $percentage = $value->diagnose_total_sum/$plan[0]->plan_cost*100;
                    // array_push($total_amount, $value->diagnose_total_sum);
                    // array_push($total_percentage, $percentage);
                    // print_r(!empty($value->diagnose_date));die;
                    // $hospital = $ci->Clients_model->get_hospital_info($value->hospital_id)->result();
                  ?>  
                      <tr>
                          <td><?php echo $client->name ; ?></td>
                          <td><?php echo "Dependant"; ?></td>
                          <td><?php echo date_diff(date_create($client->dob), date_create('today'))->y; ?></td>
                          <td><?php echo !empty($subs) ? $subs[0]->plan_name : ''; ?></td>
                          <td><?php echo !empty($hospital) ? $hospital[0]->hospital_name : '-'; ?></td>
                          <td><?php echo !empty($organization) ? $organization[0]->name : '-' ; ?></td>            
                      </tr> 

          <?php 
              } 
            }
          ?>
        </tbody>

      </table>

    </div>

  </div>

</div>
<?php }else{
  redirect('admin/dashboard','refresh');
} ?>
<script type="text/javascript">
    

    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
          "buttons": ['csv', 'excel', 'pdf', 'print']
        }); 
    }, false);

    $('#total').text('<?php //echo $this->Xin_model->currency_sign(array_sum($total_amount)); ?>');
    $('#percentage').text('<?php //echo number_format(array_sum($total_percentage),2)."%"; ?>')

</script>

