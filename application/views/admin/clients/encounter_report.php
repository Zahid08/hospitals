<?php

/* Accounting > New Expense view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php 
  if (isset($client_info)) {
    $plan = $this->Clients_model->get_clients_subscription($client_info[0]->subscription_ids)->result(); 
  if (!empty($plan)) {
    $plan_name = $plan[0]->plan_name;
  }else{
    $plan_name = "--";
  }
  }
?>

<?php if(in_array('594',$role_resources_ids) || in_array('607',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"> Filter Results By Selecting an Enrollee</h3>

    </div>

    <div class="<?php echo $get_animate;?>" style="">

      <div class="box-body">

            <?php echo form_open('admin/clients/encounter_report');?>

            <div class="row">
              <div class="col-md-3">
                  <select name="client_id" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="Choose a Client">

                    <option value=""></option>

                    <?php 
                      foreach($all_clients as $client) { 
                    ?>

                        <option value="<?php echo $client->client_id;?>"><?php echo $client->name." ".$client->last_name;?></option>

                    <?php } ?>

                  </select>
              </div>

              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

                </div>

              </div>

              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">

                </div>

              </div>

              <div class="col-md-2">

                <div class="form-group">

                  <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

                </div>

              </div>

            </div>

            <?php echo form_close(); ?> 
        </div>

    </div>

  </div>

</div>



<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <?php if (isset($client_info)): ?>
      <h3 class="box-title">Medical history <?php if(isset($client_info[0]->name)) echo "for ".$client_info[0]->name; ?> | SPENT : <span id="total"></span> | USAGE: <span id="percentage"></span> | PLAN: <?php echo $plan_name ?></h3>
    <?php else: ?>
      <h3 class="box-title">Medical history</h3>

    <?php endif ?>

  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>
    
    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

        <thead>

          <tr>

            <th>Encounter Date</th>

            <th></th>

            <th>Investigation</th>

            <th>Diagnosis</th>

            <th>Procedure</th>

            <th>Drugs</th>
            
            <th>Services</th>
            
            <th>Amount</th>

            <th>Usage</th>

          </tr>

        </thead>
        <tbody>
                  <?php   
                    if(!empty($all_encounter))
                    { 

                        $admin_id = $this->session->userdata;
                        $total_amount = array();
                        $total_percentage = array();
                        // print_r($all_encounter);die;
                        foreach ($all_encounter->result() as $key => $value)
                        {              
                            $admin_info = array();

                            $ci=& get_instance();
                            $ci->load->model('Clients_model'); 

                            $diagnose_drug = $this->Clients_model->get_drugs_diagnose($value->diagnose_id)->result();
                            $diagnose_service = $this->Clients_model->get_services_diagnose($value->diagnose_id)->result();

                            // print_r($diagnose_service);die;
                            $drug_name = array();
                            foreach ($diagnose_drug as $drug) {
                                $drug_info = $this->Clients_model->get_drugs_info($drug->diagnose_drugs_id)->result();
                                array_push($drug_name, $drug_info[0]->drug_name);
                            }

                            $service_name = array();
                            foreach ($diagnose_service as $service) {
                                $service_info = $this->Clients_model->get_services_info($service->diagnose_services_id)->result();
                                array_push($service_name, $service_info[0]->service_name);
                            }

                            $drug_name = implode(",", $drug_name);
                            $service_name = implode(",", $service_name);
                            
                            $subs = $this->Clients_model->read_client_info($value->diagnose_client_id);

                            // print_r($subs);die;

                            $plan = $this->Clients_model->get_clients_subscription($subs[0]->subscription_ids)->result();

                            $percentage = $value->diagnose_total_sum/$plan[0]->plan_cost*100;
                            array_push($total_amount, $value->diagnose_total_sum);
                            array_push($total_percentage, $percentage);
                            // print_r(!empty($value->diagnose_date));die;
                            // $hospital = $ci->Clients_model->get_hospital_info($value->hospital_id)->result();
                          ?>  
                              <tr>
                                  <td><?php echo $value->diagnose_date ; ?></td>
                                  <td><?php echo '-'; ?></td>
                                  <td><?php echo $value->diagnose_investigation ; ?></td>
                                  <td><?php echo $value->diagnose_diagnose ; ?></td>
                                  <td><?php echo $value->diagnose_procedure ; ?></td>
                                  <td><?php echo $drug_name ; ?></td>
                                  <td><?php echo $service_name ?></td>
                                  <td><?php echo $this->Xin_model->currency_sign($value->diagnose_total_sum);  ;?></td>
                                  <td><?php echo number_format($percentage,2)."%";  ;?></td>
                                  
                              </tr> 

                  <?php 
                          }
                    }
                  ?>
        </tbody>

      </table>

    </div>

  </div>

</div>
<?php }else{
  redirect('admin/dashboard','refresh');
} ?>
<script type="text/javascript">
    

    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
          "buttons": ['csv', 'excel', 'pdf', 'print']
        }); 
    }, false);

    $('#total').text('<?php echo $this->Xin_model->currency_sign(array_sum($total_amount)); ?>');
    $('#percentage').text('<?php echo number_format(array_sum($total_percentage),2)."%"; ?>')

</script>

