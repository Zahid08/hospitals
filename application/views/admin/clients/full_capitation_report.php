<?php

/* Accounting > New Expense view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php $cost = $this->Xin_model->capitation_cost();  ?>

<?php if(in_array('610',$role_resources_ids) || in_array('615',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>

  <div class="row">
  <div class="col-md-3 col-12">
    <div class="box box-body bg-hr-primary">
      <div class="flexbox"> <span class="fa fa-ticket text-primary font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo total_capitation_principal();?></span> </div>
      <div class="text-right"><?php echo $this->lang->line('xin_hr_total_capitation_principal');?></div>
    </div>
  </div>
  <div class="col-md-3 col-12">
    <div class="box box-body bg-hr-danger">
      <div class="flexbox"> <span class="fa fa-server text-danger font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo total_capitation_dependant();?></span> </div>
      <div class="text-right"><?php echo $this->lang->line('xin_hr_total_capitation_dependant');?></div>
    </div>
  </div>
  <div class="col-md-3 col-12">
    <div class="box box-body bg-hr-success">
      <div class="flexbox"> <span class="ion ion-thumbsup text-success font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo total_capitation_principal()+total_capitation_dependant();?></span> </div>
      <div class="text-right"><?php echo $this->lang->line('xin_hr_total_capitation_beneficiaries');?></div>
    </div>
  </div>
  <div class="col-md-3 col-12">
    <div class="box box-body bg-hr-success">
      <div class="flexbox"> <span class="ion ion-thumbsup text-success font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo $this->Xin_model->currency_sign((total_capitation_principal()+total_capitation_dependant())*$cost[0]->cost);?></span> </div>
      <div class="text-right">NHIS Revenue Income</div>
    </div>
  </div>
</div>


<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"> Filter Results By Selecting a Date Range </h3>

    </div>

    <div class="<?php echo $get_animate;?>" style="">

      <div class="box-body">

            <?php echo form_open('admin/clients/full_capitation_report');?>

            <div class="row">
              <div class="col-md-3">
                <input type="text" class="form-control date" placeholder="Select a date" required="" name="from">
              </div>

              <div class="col-md-3">
                <input type="text" class="form-control date" placeholder="Select a date" required="" name="to">
              </div>
              <!-- 
              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

                </div>

              </div>

              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">

                </div>

              </div> -->

              <div class="col-md-2">

                <div class="form-group">

                  <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

                </div>

              </div>

            </div>

            <?php echo form_close(); ?> 
        </div>

    </div>

  </div>

</div>



<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>
    
    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

        <thead>

          <tr>

                <th>HCP Code</th>

                <th>Hospital Name</th>

                <th>Principal</th>

                <th>Dependants</th>

                <th>Ext Dep</th> 
                
                <th>Voluntary</th>

                <th>Beneficiaries</th>

          </tr>

        </thead>
        <tbody>
          
                  <?php   
                    if(!empty($all_hospital))
                    { 

                        $admin_id = $this->session->userdata;

                        // print_r($all_hospital->result());die;
                        foreach ($all_hospital as $key => $value)
                        {              

                            $ci=& get_instance();
                            $ci->load->model('Clients_model');

                            $principal = $ci->Clients_model->get_all_capitation_hospital2($value->hcp_code,'p')->num_rows(); 
                            $dependant = $ci->Clients_model->get_all_capitation_hospital2($value->hcp_code,'d')->num_rows(); 
                            $ext_dependant = $ci->Clients_model->get_all_capitation_hospital2($value->hcp_code,'d',1)->num_rows(); 

                  ?>  
                              <tr>
                                <td><?php echo $value->hcp_code != '' ? $value->hcp_code : "-"; ?></td> 
                                <td><?php echo $value->hospital_name != '' ? $value->hospital_name : "-"; ?></td> 
                                <td><?php echo $principal; ?></td> 
                                <td><?php echo $dependant; ?></td> 
                                <td><?php echo $ext_dependant; ?></td> 
                                <td><?php echo "0"; ?></td> 
                                <td><?php echo $principal+$dependant+$ext_dependant; ?></td> 
                              </tr> 

                  <?php 
                          }
                    }
                  ?>
        </tbody>

      </table>

    </div>

  </div>

</div>
<?php }else{
  redirect('admin/dashboard','refresh');
} ?>
<script type="text/javascript">
    

    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
            "buttons": ['csv', 'excel', 'pdf', 'print']
        }); 
    }, false);

</script>

<script type="text/javascript">
  function loadModalView(id){
          // alert("ID is: " + id);
          $.ajax({
            url      : '<?php echo base_url(); ?>admin/Clients/fetch_profile_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              $("#client_profile").html(response);
            }
          });

      }
</script>

 <!-- Modal -->
  <div class="modal fade" id="myModalProfile" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Client's Profile</h3>
        </div>
        <div class="modal-body" id="client_profile">

        </div>
      
        <div class="clearfix"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>