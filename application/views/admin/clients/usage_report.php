<?php

/* Accounting > New Expense view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('594',$role_resources_ids) || in_array('609',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"> Filter Results By Selecting a Date Range</h3>

    </div>

    <div class="<?php echo $get_animate;?>" style="">

      <div class="box-body">

            <?php echo form_open('admin/clients/usage_report');?>

            <div class="row">
      
              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>" required>

                </div>

              </div>

              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>" required>

                </div>

              </div>

              <div class="col-md-2">

                <div class="form-group">

                  <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

                </div>

              </div>

            </div>

            <?php echo form_close(); ?> 
        </div>

    </div>

  </div>

</div>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">
  	<h3 class="box-title">
  		<?php echo (isset($to_date) AND isset($from_date)) ? "Showing usage of enrollees from $from_date to $to_date" : ''; ?>
  	</h3>
  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>
    
    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

        <thead>

          <tr>

            <th>Enrollee Name</th>

            <th>Type</th>

            <th>Age</th>

            <th>Health Plan</th>

            <th>Provider</th>

            <th>Organization</th>

            <th>Amount</th>
            
            <th>Percentage</th>

          </tr>

        </thead>
        <tbody>
          <?php   
            if(!empty($all_clients))
            { 
              // print_r($all_clients->result());die;

                $admin_id = $this->session->userdata;
                // print_r($all_clients);die;

                // print_r($all_clients);die;
                foreach ($all_clients as $key => $value)
                {              
                    $admin_info = array();

                    $ci=& get_instance();
                    $ci->load->model('Clients_model'); 
                    
                    $client = $ci->Clients_model->read_client_info($value);

                    $client = $client[0];

                    $subs = $this->Clients_model->get_clients_subscription($client->subscription_ids)->result();

                    // print_r($subs);die;

                    $hospital = $this->Clients_model->get_hospital_info($client->hospital_id)->result();

                    $organization = $this->Clients_model->get_organization_info($client->company_name)->result();

                    if (isset($from_date) AND isset($to_date)) {
	                    $encounter = $this->Clients_model->get_clients_encounter($value,$from_date,$to_date)->result();
                    }else{
	                    $encounter = $this->Clients_model->get_clients_encounter($value)->result();
                    }


                    $total = array();

                    foreach ($encounter as $enc) {
                      array_push($total, $enc->diagnose_total_sum);
                    }

                    $percentage = array_sum($total)/$subs[0]->plan_cost*100;


                    if (!is_null($client->other_name) OR $client->other_name =='') {
                      $other_name = " (".$client->other_name.")";
                    }else{
                      $other_name = "";
                    }

                    // echo $percentage."\n";continue;

                    // print_r($total);die;


                    // print_r($encounter);die;
                    
                  ?>  
                      <tr>
                          <td><?php echo $client->name." ".$client->last_name.$other_name; ?></td>
                          <td><?php echo "Principal" ?></td>
                          <td><?php echo date_diff(date_create($client->dob), date_create('today'))->y; ?></td>
                          <td><?php echo !empty($subs) ? $subs[0]->plan_name : '-' ; ?></td>
                          <td><?php echo !empty($hospital) ? $hospital[0]->hospital_name : '-'; ?></td>
                          <td><?php echo !empty($organization) ? $organization[0]->name : '-' ; ?></td>
                          <td><?php echo $this->Xin_model->currency_sign(array_sum($total)); ?></td>        
                          <td><?php echo number_format($percentage,2)."%"; ?></td>        
                      </tr> 

          <?php 
              } 
            }

            if(!empty($all_dependants))
            { 
              // print_r($all_clients->result());die;

                $admin_id = $this->session->userdata;

                // print_r($all_clients);die;
                foreach ($all_dependants as $key => $value)
                {              
                    $admin_info = array();

                    $ci=& get_instance();
                    $ci->load->model('Clients_model'); 
                    
                    $client = $ci->Clients_model->read_dependant_info($value);

                    $client = $client[0];

                    $c = $ci->Clients_model->read_client_info($client->client_id);
                    $c = $c[0];

                    $subs = $this->Clients_model->get_clients_subscription($c->subscription_ids)->result();

                    $hospital = $this->Clients_model->get_hospital_info($c->hospital_id)->result();

                    $organization = $this->Clients_model->get_organization_info($c->company_name)->result();

                    $total = array();

                    if (isset($from_date) AND isset($to_date)) {
	                    $encounter = $this->Clients_model->get_dependants_encounter($value,$from_date,$to_date)->result();
                    }else{
	                    $encounter = $this->Clients_model->get_dependants_encounter($value)->result();
                    }

                    foreach ($encounter as $enc) {
                      array_push($total, $enc->diagnose_total_sum);
                    }

                    $percentage = array_sum($total)/$subs[0]->plan_cost*100;

                    if (!is_null($client->other_name) OR $client->other_name =='') {
                      $other_name = " (".$client->other_name.")";
                    }else{
                      $other_name = "";
                    }
                  ?>  
                      <tr>
                          <td><?php echo $client->name." ".$client->last_name.$other_name ; ?></td>
                          <td><?php echo "Dependant"; ?></td>
                          <td><?php echo date_diff(date_create($client->dob), date_create('today'))->y; ?></td>
                          <td><?php echo !empty($subs) ? $subs[0]->plan_name : ''; ?></td>
                          <td><?php echo !empty($hospital) ? $hospital[0]->hospital_name : '-'; ?></td>
                          <td><?php echo !empty($organization) ? $organization[0]->name : '-' ; ?></td> 
                          <td><?php echo $this->Xin_model->currency_sign(array_sum($total)); ?></td>        
                          <td><?php echo number_format($percentage,2)."%"; ?></td>              
                      </tr> 

          <?php 
              } 
            }
          ?>
        </tbody>

      </table>

    </div>

  </div>

</div>
<?php }else{
  redirect('admin/dashboard','refresh');
} ?>
<script type="text/javascript">
    

    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
          "buttons": ['csv', 'excel', 'pdf', 'print'],
          "order": [[ 7, "desc" ]]
        }); 
    }, false);

    $('#total').text('<?php //echo $this->Xin_model->currency_sign(array_sum($total_amount)); ?>');
    $('#percentage').text('<?php //echo number_format(array_sum($total_percentage),2)."%"; ?>')

</script>

