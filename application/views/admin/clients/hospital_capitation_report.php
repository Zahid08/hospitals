<?php

/* Accounting > New Expense view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('610',$role_resources_ids) || in_array('614',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"> Filter Results By Selecting a Provider </h3>

    </div>

    <div class="<?php echo $get_animate;?>" style="">

      <div class="box-body">

            <?php echo form_open('admin/clients/hospital_capitation_reports');?>

            <div class="row">
              <div class="col-md-3">
                  <select name="hospital_name" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="Choose Hospital">

                    <option value=""></option>

                    <?php 
                      foreach($all_hospital as $hospital) { 
                    ?>

                        <option value="<?php echo $hospital->hospital_id;?>"><?php echo $hospital->hospital_name;?></option>

                    <?php } ?>

                  </select>
              </div><!-- 
              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

                </div>

              </div>

              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">

                </div>

              </div> -->

              <div class="col-md-2">

                <div class="form-group">

                  <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

                </div>

              </div>

            </div>

            <?php echo form_close(); ?> 
        </div>

    </div>

  </div>

</div>



<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>
    
    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

        <thead>

          <tr>

                <th>Capitation ID</th>

                <th>Hospital Name</th>

                <th>Type</th>

                <th>Dependant</th> 
                
                <th>Principal</th>

                <th>Relation</th> 

                <th>Gender</th>

                <th>Age</th> 

          </tr>

        </thead>
        <tbody>
          
                  <?php   
                    if(!empty($all_clients))
                    { 

                        $admin_id = $this->session->userdata;
                        
                        foreach ($all_clients->result() as $key => $value)
                        {              
                            $dob = explode("/", $value->dob);

                            $age = date_diff(date_create($dob[0]."-".$dob[1]."-".$dob[2]), date_create('today'))->y;

                            $ci=& get_instance();
                            $ci->load->model('Training_model'); 

                            $hospital = $this->Training_model->getAll2('xin_hospital', ' hcp_code = "' . $value->hcp_code . '" ');
                            if ($value->type == 'p') {
                              $type = 'principal';
                              $principal_name = '-';
                              $relation = '-';
                            }else{
                              $type = 'dependant';
                              $principal_name = $value->principal_name;
                              $relation = $value->relation;
                            }

                            if ($value->gender == 'F') {
                              $gender = 'female';
                            }else{
                              $gender = 'male';
                            }

                            // $organization = $ci->Clients_model->get_organization_info($value->company_name)->result();
                            // $subs = $ci->Clients_model->get_clients_subscription($value->subscription_ids)->result();
                  ?>  
                              <tr>
                                  <td><?php echo $value->capitation_id; ?></td>
                                  <td><?php echo $hospital[0]->hospital_name; ?></td>
                                  <td><?php echo $type; ?></td>
                                  <td><?php echo $value->name; ?></td>
                                  <td><?php echo $principal_name; ?></td>
                                  <td><?php echo $relation; ?></td>
                                  <td><?php echo $gender; ?></td>
                                  <td><?php echo $age ?></td>  
                              </tr> 

                  <?php 
                          }
                    }
                  ?>
        </tbody>

      </table>

    </div>

  </div>

</div>
<?php }else{
  redirect('admin/dashboard','refresh');
} ?>
<script type="text/javascript">
    

    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
            "buttons": ['csv', 'excel', 'pdf', 'print']
        }); 
    }, false);

</script>

<script type="text/javascript">
  function loadModalView(id){
          // alert("ID is: " + id);
          $.ajax({
            url      : '<?php echo base_url(); ?>admin/Clients/fetch_profile_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              $("#client_profile").html(response);
            }
          });

      }
</script>

 <!-- Modal -->
  <div class="modal fade" id="myModalProfile" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Client's Profile</h3>
        </div>
        <div class="modal-body" id="client_profile">

        </div>
      
        <div class="clearfix"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>