<?php
/* Accounting > Income Report view
*/
?>
<?php $session = $this->session->userdata('username');?>
<?php $get_animate = $this->Xin_model->get_content_animate();?>
<div class="box mb-4 <?php echo $get_animate;?>">
  <div class="box-header with-border">
	  <h3 class="box-title">View Enrollees Data by Selecting an Organization</h3>
  </div>
  <div class="box-body">
    <?php $attributes = array('name' => 'clients_report', 'id' => 'hrm-form', 'autocomplete' => 'off');?>
    <?php $hidden = array('re_user_id' => $session['user_id']);?>
    <?php echo form_open('admin/clients/clients_report', $attributes, $hidden);?>
    <?php
		$data = array(
		  'name'        => 'user_id',
		  'id'          => 'user_id',
		  'type'        => 'hidden',
		  'value' => $session['user_id'],
		  'class'       => 'form-control',
		);
	echo form_input($data);
	?>
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
			<select class="form-control" id="company_name" name="company_name" data-plugin="xin_select" data-placeholder="Select an Organization">
				<option value="">Select Organization</option>
				<?php foreach($all_organizations as $organization) {?>
					<option value="<?php echo $organization->id;?>"> <?php echo $organization->name;?></option>
				<?php } ?>
			</select>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>
        </div>
      </div>
    </div>
    <?php echo form_close(); ?> </div>
</div>
<div class="box <?php echo $get_animate;?>">
  <div class="box-header with-border">
  </div>
  <div class="box-body">
    <div class="box-datatable table-responsive">
      <table class="datatables-demo table table-striped table-bordered" id="xin_table">
        <thead>
          <tr>
			  <th>Enrollee ID</th>
			  <th>Enrollee Name</th>
			  <th>Provider</th>
			  <th>Healthcare Plan</th>
			  <th>Account type</th>
			  <th>Email Address</th>
			  <th>Phone</th>
        <th>Created At</th>
		  </tr>
        </thead>
        <tfoot id="get_footer">
        </tfoot>
      </table>
    </div>
  </div>
</div>
