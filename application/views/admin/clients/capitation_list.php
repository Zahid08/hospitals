<?php

/* Client view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>
<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php $cost = $this->Xin_model->capitation_cost();  ?>

<?php if(in_array('610',$role_resources_ids) || in_array('611',$role_resources_ids) || in_array('612',$role_resources_ids) || in_array('613',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>


<?php if (in_array('656', $role_resources_ids) || $user_info[0]->user_role_id == 1): ?>
    

<div class="row">
  <div class="col-md-3 col-12">
    <div class="box box-body bg-hr-primary">
      <div class="flexbox"> <span class="fa fa-ticket text-primary font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo total_capitation_principal();?></span> </div>
      <div class="text-right"><?php echo $this->lang->line('xin_hr_total_capitation_principal');?></div>
    </div>
  </div>
  <div class="col-md-3 col-12">
    <div class="box box-body bg-hr-danger">
      <div class="flexbox"> <span class="fa fa-server text-danger font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo total_capitation_dependant();?></span> </div>
      <div class="text-right"><?php echo $this->lang->line('xin_hr_total_capitation_dependant');?></div>
    </div>
  </div>
  <div class="col-md-3 col-12">
    <div class="box box-body bg-hr-success">
      <div class="flexbox"> <span class="ion ion-thumbsup text-success font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo total_capitation_principal()+total_capitation_dependant();?></span> </div>
      <div class="text-right"><?php echo $this->lang->line('xin_hr_total_capitation_beneficiaries');?></div>
    </div>
  </div>
  <div class="col-md-3 col-12">
    <div class="box box-body bg-hr-success">
      <div class="flexbox"> <span class="ion ion-thumbsup text-success font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo $this->Xin_model->currency_sign((total_capitation_principal()+total_capitation_dependant())*$cost[0]->cost);?></span> </div>
      <div class="text-right">NHIS Revenue Income</div>
    </div>
  </div>
</div>
<?php endif ?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"> Filter Results By Selecting a Date Range </h3>

    </div>

    <div class="<?php echo $get_animate;?>" style="">

      <div class="box-body">

            <div class="row">
              <div class="col-md-3">
                <div class="form-group">  
                    <input type="text" class="form-control date" placeholder="Select a date" required="required" name="from" id="date_invoice_from" style="border-radius: 1.5rem" readonly>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">  
                    <input type="text" class="form-control date" placeholder="Select a date" required="required" name="to" id="date_invoice_to" style="border-radius: 1.5rem" readonly>
                </div>
              </div>
      
              <div class="col-md-2">

                <div class="form-group">

                  <button type="submit" name="get_filter_result" id="submit_invoice_date" class="btn btn-primary save">Fetch Data</button>

                </div>

              </div>

            </div>

        </div>

    </div>

  </div>

</div>


<div class="box <?php echo $get_animate;?>">
    <?php if (in_array('613',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
        <div class="box-header with-border">

        <h3 class="box-title">NHIS Enrollees</h3>

        <div class="box-tools pull-right"> 
            <a class="text-dark collapsed" data-toggle="collapse" href="#upload" aria-expanded="false">

                <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span>BULK NHIS DATA UPLOAD</button>

            </a> 
        </div>

    </div>

    <div id="upload" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">
     
        <a href="<?php echo base_url(); ?>/uploads/csv/sample-csv-capitation.csv" class="btn btn-primary"> <i class="fa fa-download"></i> Download sample File </a> 

        <?php $attributes = array('name' => 'import_attendance', 'id' => 'xin-form2', 'autocomplete' => 'off');?>

             

            <?php echo form_open_multipart('admin/clients/import_capitation', $attributes);?>

            <div class="row">
                <?php if ($this->session->flashdata('success')): ?>
                    <div class="alert alert-success  alert-dismissible" style="margin: 20px 12px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif ?>

                 <?php if ($this->session->flashdata('error')): ?>
                    <div class="alert alert-warning  alert-dismissible" style="margin: 20px 12px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif ?>

                <div class="col-md-4 form-group"> 

                    <fieldset class="form-group">

                        <label for="logo"><?php echo $this->lang->line('xin_employee_upload_file');?></label>

                        <input type="file" class="form-control-file" id="file" name="file">

                        <small><?php echo $this->lang->line('xin_employee_imp_allowed_size');?></small>

                    </fieldset> 
                </div>

            </div>

            <div class="mt-1">

              <div class="form-actions box-footer"> <?php echo form_button(array('name' => 'hrsale_form', 'type' => 'submit', 'class' => $this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '.$this->lang->line('xin_save'))); ?> </div>

            </div>

  </div>
            <?php echo form_close(); ?>

     </div>
  
   
    <?php endif ?>
    

  <div class="box-body">

    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table2">

        <thead>

            <tr> 
                <?php if (in_array('655', $role_resources_ids) || $user_info[0]->user_role_id == 1) {?>
                    <th><?php echo $this->lang->line('xin_action');?></th>
                <?php } ?>

                <th>NHIS ID</th>

                <th>Provider</th>

                <th>Type</th>

                <th>Dependant</th> 
                
                <th>Principal</th>

                <th>Relation</th> 

                <th>Gender</th>

                <th>Age</th> 

                
            </tr>

        </thead>

      </table>

    </div>

  </div>

</div>

<div class="modal fadeInRight edit-modal-data2 animated" id="edit-modal-data" role="dialog" aria-labelledby="edit-modal-data" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="ajax_modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                <h4 class="modal-title" id="edit-modal-data"><i class="icon-pencil7"></i> Edit Capitation</h4>
            </div>
            <form action="<?php echo base_url() ?>admin/clients/capitation_update" name="edit_client" id="edit_client" autocomplete="off" class="m-b-1" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="EDIT">
                <input type="hidden" name="_token"  id="_token">
                <input type="hidden" name="ext_name" value="Shaleena">
                <input type="hidden" name="csrf_hrsale" value="467ba7724bc88bcd9af14ffeb6201eff">                  

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company_name">Capitation ID</label>
                                <input class="form-control" name="capitation_id" type="text"  id="c_capitation-id">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company_name">Hospital</label>
                                <select class="form-control" id="c_hospital-id" name="hospital_id" data-plugin="xin_select" data-placeholder="Select Hospital">
                                    <?php foreach ($all_hospitals as $hospital): ?>
                                        <option value="<?php echo $hospital->hospital_id ?>"><?php echo $hospital->hcp_code." - ".$hospital->hospital_name ?></option>
                                    <?php endforeach ?>
                                </select> 
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company_name">Name</label>
                                <input class="form-control" name="name" type="text"  id="c_name">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company_name">Type</label>
                                <select class="form-control" id="c_type" name="type" data-plugin="xin_select" data-placeholder="Select Type">
                                    <option value="p">Principal</option>
                                    <option value="d">Dependant</option>
                                </select> 
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company_name">Principal Name</label>
                                <input class="form-control" name="pname" type="text"  id="c_pname">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company_name">Relation</label>
                                <input class="form-control" name="relation" type="text"  id="c_relation">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company_name">DOB</label>
                                <input class="form-control" name="dob" type="text"  id="c_dob">
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save">Update</button>
                </div>
            </form>
<?php }else{
    redirect('admin/dashboard','refresh');
} ?>

            <script type="text/javascript">

                document.addEventListener('DOMContentLoaded', function(){ 

                    $('[data-plugin="select_hrm"]').select2($(this).attr('data-options'));
                    $('[data-plugin="select_hrm"]').select2({ width:'100%' });   

                    /* Edit data */
                    $("#edit_client").submit(function(e){
                        var fd = new FormData(this);
                        var obj = $(this), action = obj.attr('name');
                        fd.append("is_ajax", 2);
                        fd.append("edit_type", 'client');
                        fd.append("form", action);
                        e.preventDefault();
                        $('.save').prop('disabled', true);
                        $.ajax({
                            url: e.target.action,
                            type: "POST",
                            data:  fd,
                            contentType: false,
                            cache: false,
                            processData:false,
                            success: function(JSON)
                            {
                                if (JSON.error != '') {
                                    toastr.error(JSON.error);
                                    $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                                    $('.save').prop('disabled', false);
                                } else {
                                    // On page load: datatable
                                    var xin_table = $('#xin_table').dataTable({
                                        aaSorting: [[4,'desc']],
                                        "bDestroy": true,
                                        "ajax": {
                                            url : "<?php  echo base_url(); ?>admin/clients/clients_list",
                                            type : 'GET'
                                        },
                                        dom: 'lBfrtip',
                                        "buttons": ['csv', 'excel', 'pdf', 'print'], // colvis > if needed
                                        "fnDrawCallback": function(settings){
                                        $('[data-toggle="tooltip"]').tooltip();          
                                        }
                                    });
                                    xin_table.api().ajax.reload(function(){ 
                                        toastr.success(JSON.result);
                                    }, true);
                                    $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                                    $('.edit-modal-data2').modal('toggle');
                                    $('.save').prop('disabled', false);
                                }
                            },
                            error: function() 
                            {
                                toastr.error(JSON.error);
                                $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                                $('.save').prop('disabled', false);
                            }           
                       });
                    });





                    $('body').on('click', '.edit_client_detail', function() { 
  
                        var c_capitation_id     = $(this).attr('data-capitation_id');
                        var c_hospital_id     = $(this).attr('data-hospital_id'); 
                        var c_type    = $(this).attr('data-type');
                        var c_name    = $(this).attr('data-name');
                        var c_pname   = $(this).attr('data-pname');
                        var c_relation   = $(this).attr('data-relation');
                        var c_gender   = $(this).attr('data-gender');
                        var c_dob   = $(this).attr('data-dob');
                        var _token     = $(this).attr('data-id');

                        $('#c_capitation-id').val(c_capitation_id); 
                        $('#c_hospital-id').val(c_hospital_id).trigger('change'); 
                        $('#c_type').val(c_type).trigger('change');
                        $('#c_name').val(c_name); 
                        $('#c_pname').val(c_pname); 
                        $('#c_relation').val(c_relation); 
                        $('#c_gender').val(c_gender); 
                        $('#c_dob').val(c_dob).trigger('change'); 
                        $('#_token').val(_token);
                        
                    }) 
                }, false);
             
            </script>
        </div>
    </div>
</div>
