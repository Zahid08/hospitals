<?php

/* Accounting > New Expense view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('594',$role_resources_ids) || in_array('596',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"> Filter Results By Selecting a Principal </h3>

    </div>

    <div class="<?php echo $get_animate;?>" style="">

      <div class="box-body">

            <?php echo form_open('admin/clients/client_dependants_report');?>

            <div class="row">
              <div class="col-md-3">
                  <select name="client_id" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="Choose a Principal">

                    <option value=""></option>

                    <?php 
                      foreach($all_clients as $client) { 
                    ?>

                        <option value="<?php echo $client->client_id;?>"><?php echo $client->name;?></option>

                    <?php } ?>

                  </select>
              </div><!-- 
              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

                </div>

              </div>

              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">

                </div>

              </div> -->

              <div class="col-md-2">

                <div class="form-group">

                  <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

                </div>

              </div>

            </div>

            <?php echo form_close(); ?> 
        </div>

    </div>

  </div>

</div>



<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">
    <h3 class="box-title">
      <?php 
      if (isset($client_id)): 
        $client = $this->Clients_model->get_clients_by_id($client_id)->result();
        echo !empty($client) ? "Showing dependants of ".$client[0]->name." ".$client[0]->last_name : '';
    ?> 
    <?php endif ?>
    </h3>
  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>
    
    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

        <thead>

          <tr>

            <th>Dependant ID</th>

            <th>Enrollee Name</th>

            <th>Age</th>

            <th>Relation</th>

            <th>Contact Number</th>

            <th>Provider</th>

            <th>Address</th>

          </tr>

        </thead>
        <tbody>
                  <?php   
                    if(!empty($all_dependants))
                    { 
                      // print_r($all_dependants->result());die;
                        $admin_id = $this->session->userdata;
                        // print_r($all_dependants);die;
                        $no = 1;
                        // $dates = array();
                        foreach ($all_dependants->result() as $key => $value)
                        {              
                            $age = date_diff(date_create($value->dob), date_create('today'))->y;
                            $admin_info = array();

                            $ci=& get_instance();
                            $ci->load->model('Clients_model'); 

                            $hospital = $ci->Clients_model->get_hospital_info($value->hospital_id)->result();
                          ?>  
                              <tr>
                                  <td>PHC/<?php echo date_format(date_create($value->created_on),"Y")."/".str_pad($client_id, 3, "0",STR_PAD_LEFT)."/".$no; ?></td>
                                  <td><?php echo $value->name." ".$value->last_name." ".$value->other_name; ?></td>
                                  <td><?php echo $age; ?></td>
                                  <td><?php echo $value->relation; ?></td>
                                  <td><?php echo $value->contact_number; ?></td>
                                  <td><?php echo $hospital[0]->hospital_name; ?></td>
                                  <td><?php echo $value->address_1 ?></td>
                                  
                              </tr> 

                  <?php 
                         $no++; }
                         // print_r($dates);
                    }
                  ?>
        </tbody>

      </table>

    </div>

  </div>

</div>
<?php }else{
  redirect('admin/dashboard','refresh');
} ?>
<script type="text/javascript">
    

    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
            "buttons": ['csv', 'excel', 'pdf', 'print']
        }); 
    }, false);

</script>

