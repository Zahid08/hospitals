<?php

/* Accounting > New Expense view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('594',$role_resources_ids) || in_array('595',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"> Filter Results By Selecting a Hospital </h3>

    </div>

    <div class="<?php echo $get_animate;?>" style="">

      <div class="box-body">

            <?php echo form_open('admin/clients/hospital_reports');?>

            <div class="row">
              <div class="col-md-3">
                  <select name="hospital_name" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="Choose Hospital">

                    <option value=""></option>

                    <?php 
                      foreach($all_hospital as $hospital) { 
                    ?>

                        <option value="<?php echo $hospital->hospital_id;?>"><?php echo $hospital->hospital_name;?></option>

                    <?php } ?>

                  </select>
              </div>
              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

                </div>

              </div>

              <div class="col-md-3">

                <div class="form-group">

                  <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">

                </div>

              </div>

              <div class="col-md-2">

                <div class="form-group">

                  <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

                </div>

              </div>

            </div>

            <?php echo form_close(); ?> 
        </div>

    </div>

  </div>

</div>



<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">
     <h3 class="box-title"><?php if(isset($hospital_name)) echo "Enrollee Data for ".$hospital_name[0]->hospital_name; ?> <?php if(isset($from) AND isset($to)) echo " from ".$from." to ".$to; ?></h3> 
  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>
    
    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

        <thead>

          <tr>
            <th>Sr. No.</th>
            <th>Enrollee ID</th>

            <th>Enrollee Name</th>

            <th>Contact</th>

            <th>Organization</th>

            <th>Plan</th>

            <th>Age</th>
            <th>Relation</th>

            <th>Type</th>

            <th>Action</th>

          </tr>

        </thead>
        <tbody>
          
                  <?php   
                    if(!empty($all_clients))
                    { 

                        $admin_id = $this->session->userdata;
                        // print_r($all_clients);die;
						$count = 1;
                        foreach ($all_clients->result() as $key => $value)
                        {              
                            $age = date_diff(date_create($value->dob), date_create('today'))->y;
                            $admin_info = array();

                            $ci=& get_instance();
                            $ci->load->model('Clients_model'); 

                            $organization = $ci->Clients_model->get_organization_info($value->company_name)->result();
                            $subs = $ci->Clients_model->get_clients_subscription($value->subscription_ids)->result();

                            $date = strtotime($value->created_at);
                            $year = date('Y',$date);
                            $date = date('j F Y',$date);

                            // Client ID

                            $client_id = str_pad($value->client_id, 3, '0', STR_PAD_LEFT);
                            $client_id = "PHC/".$year."/".$client_id;
                  ?>  
                              <tr>
                                  <td><?php echo $count; ?></td>
                                  <td><?php echo $client_id; ?></td>
                                  <td><?php echo $value->name ." ".$value->last_name." ".$value->other_name; ?></td>
                                  <td><?php echo $value->contact_number; ?></td>
                                  <td><?php echo $organization[0]->name; ?></td>
                                  <td><?php echo $subs[0]->plan_name; ?></td>
                                  <td><?php echo $age ?></td>
                                  <td><?php echo 'PRINCIPAL'; ?></td>
                                  <td><?php echo $value->ind_family ?></td>
                                  <td><button data-toggle="modal" data-target="#myModalProfile" onclick="return loadModalView(<?php echo $value->client_id ?>)" class="btn btn-default"><i class="fa fa-eye"></i></button></td>
                                  
                              </tr> 
									
                  <?php 
				  $count++;
				  $familydeatils = $this->Clients_model->get_all_clients_hospital_date_range_new_family($value->hospital_id,$value->client_id)->result();
				  //echo '<pre>';
				 // echo $value->client_id;
				  //print_R($familydeatils);
					if($familydeatils){
					foreach($familydeatils as $fd)
					{
					   $clientidsfamily = "PHC/".date("Y",strtotime($fd->created_on))."/00".$value->client_id."/".$fd->clients_family_id;
					   
					   $age1 = date_diff(date_create($fd->dob), date_create('today'))->y;
					  ?>
					   <tr>
						  <td><?php echo $count; ?></td>
						  <td><?php echo $clientidsfamily; ?></td>
						  <td><?php echo $fd->name ." ".$fd->last_name." ".$fd->other_name; ?></td>
						  <td><?php echo $fd->contact_number; ?></td>
						  <td><?php echo $organization[0]->name; ?></td>
						  <td><?php echo $subs[0]->plan_name; ?></td>
						  <td><?php echo $age1;?></td>
						  <td><?php echo strtoupper($fd->relation);?></td>
						  <td><?php echo $value->ind_family ?></td>
						  <td><button data-toggle="modal" data-target="#myModalProfile" onclick="return loadModalViewfamily(<?php echo $fd->clients_family_id;?>)" class="btn btn-default"><i class="fa fa-eye"></i></button></td>
						  
					  </tr> 
					  <?php
					  $count++;
					  }
				  }
                          }
                    }
                  ?>
        </tbody>

      </table>

    </div>

  </div>

</div>
<?php }else{
  redirect('admin/dashboard','refresh');
} ?>
<script type="text/javascript">
    

    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
            "buttons": ['csv', 'excel', 'pdf', 'print']
        }); 
    }, false);

</script>

<script type="text/javascript">
  function loadModalView(id){
          // alert("ID is: " + id);
          $.ajax({
            url      : '<?php echo base_url(); ?>admin/Clients/fetch_profile_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              $("#client_profile").html(response);
            }
          });

      }
	  function loadModalViewfamily(id){
          // alert("ID is: " + id);
          $.ajax({
            url      : '<?php echo base_url(); ?>admin/Clients/fetch_profile_id_family',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              $("#client_profile").html(response);
            }
          });

      }
	  
</script>

 <!-- Modal -->
  <div class="modal fade" id="myModalProfile" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Enrollee Profile</h3>
        </div>
        <div class="modal-body" id="client_profile">

        </div>
      
        <div class="clearfix"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>