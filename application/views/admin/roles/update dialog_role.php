<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if(isset($_GET['jd']) && isset($_GET['role_id']) && $_GET['data']=='role'){

$role_resources_ids = explode(',',$role_resources);

?>

<div class="modal-header">

  <?php echo form_button(array('aria-label' => 'Close', 'data-dismiss' => 'modal', 'type' => 'button', 'class' => 'close', 'content' => '<span aria-hidden="true">×</span>')); ?>

  <h4 class="modal-title" id="edit-modal-data"><?php echo $this->lang->line('xin_role_editrole');?></h4>

</div>

<?php $attributes = array('name' => 'edit_role', 'id' => 'edit_role', 'autocomplete' => 'off','class' => '"m-b-1');?>

<?php $hidden = array('_method' => 'EDIT', 'ext_name' => $role_name, '_token' => $role_id);?>

<?php echo form_open('admin/roles/update/'.$role_id, $attributes, $hidden);?>

  <div class="modal-body">

    <div class="row">

      <div class="col-md-4">

        <div class="row">

          <div class="col-md-12">

            <div class="form-group">

              <label for="role_name"><?php echo $this->lang->line('xin_role_name');?></label>

              <input class="form-control" placeholder="<?php echo $this->lang->line('xin_role_name');?>" name="role_name" type="text" value="<?php echo $role_name;?>">

            </div>

          </div>

        </div>

        <div class="row">

        	<input type="checkbox" name="role_resources[]" value="0" checked style="display:none;"/>

          <div class="col-md-12">

            <div class="form-group">

              <label for="role_access"><?php echo $this->lang->line('xin_role_access');?></label>

              <select class="form-control custom-select" id="role_access_modal" name="role_access" data-plugin="select_hrm" data-placeholder="<?php echo $this->lang->line('xin_role_access');?>">

                <option value="">&nbsp;</option>

                <option value="1" <?php if($role_access==1):?> selected="selected" <?php endif;?>><?php echo $this->lang->line('xin_role_all_menu');?></option>

                <option value="2" <?php if($role_access==2):?> selected="selected" <?php endif;?>><?php echo $this->lang->line('xin_role_cmenu');?></option>

              </select>

            </div>

          </div>

        </div>

        <div class="row">

            <div class="col-md-12">

            <p><strong><?php echo $this->lang->line('xin_role_note_title');?></strong></p>

            <p><?php echo $this->lang->line('xin_role_note1');?></p>

            <p><?php echo $this->lang->line('xin_role_note2');?></p>

            </div>

          </div>

      </div>

      <div class="col-md-4">

        <div class="row">

          <div class="col-md-12">

            <div class="form-group">

              <label for="resources">Roles Control Panel</label>

              <div id="all_resources">

                <div class="demo-section k-content">

                  <div>

                    <div id="treeview_m1"></div>

                  </div>

                </div>

              </div>

            </div>

          </div>

        </div>

      </div>

      <div class="col-md-4">

        <div class="row">

          <div class="col-md-12">

            <div class="form-group">

              <div id="all_resources">

                <div class="demo-section k-content">

                  <div>

                    <div id="treeview_m2"></div>

                  </div>

                </div>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

  <div class="modal-footer">

    <?php echo form_button(array('data-dismiss' => 'modal', 'type' => 'button', 'class' => 'btn btn-secondary', 'content' => '<i class="fa fa fa-check-square-o"></i> '.$this->lang->line('xin_close'))); ?> <?php echo form_button(array('name' => 'hrsale_form', 'type' => 'submit', 'class' => $this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '.$this->lang->line('xin_update'))); ?> 

  </div>

<?php echo form_close(); ?>

<script type="text/javascript">

 $(document).ready(function(){

		

		$('[data-plugin="select_hrm"]').select2($(this).attr('data-options'));

		$('[data-plugin="select_hrm"]').select2({ width:'100%' });	 

		 $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({

		  checkboxClass: 'icheckbox_minimal-blue',

		  radioClass   : 'iradio_minimal-blue'

		});



		/* Edit data */

		$("#edit_role").submit(function(e){

		e.preventDefault();

			var obj = $(this), action = obj.attr('name');

			$('.save').prop('disabled', true);

			

			$.ajax({

				type: "POST",

				url: e.target.action,

				data: obj.serialize()+"&is_ajax=1&edit_type=role&form="+action,

				cache: false,

				success: function (JSON) {

					if (JSON.error != '') {

						toastr.error(JSON.error);

						$('input[name="csrf_hrsale"]').val(JSON.csrf_hash);

						$('.save').prop('disabled', false);

					} else {

						// On page load: datatable

						var xin_table = $('#xin_table').dataTable({

							"bDestroy": true,

							"ajax": {

								url : "<?php echo site_url("admin/roles/role_list") ?>",

								type : 'GET'

							},

							dom: 'lBfrtip',

							"buttons": ['csv', 'excel', 'pdf', 'print'], // colvis > if needed

							"fnDrawCallback": function(settings){

							$('[data-toggle="tooltip"]').tooltip();          

							}

						});

						xin_table.api().ajax.reload(function(){ 

							toastr.success(JSON.result);

						}, true);

						$('input[name="csrf_hrsale"]').val(JSON.csrf_hash);

						$('.edit-modal-data').modal('toggle');

						$('.save').prop('disabled', false);

					}

				}

			});

		});

	});	

  </script>

  <script>



jQuery("#treeview_m1").kendoTreeView({

checkboxes: {

checkChildren: true,

//template: "<label class='custom-control custom-checkbox'><input type='checkbox' #= item.check# class='#= item.class #' name='role_resources[]' value='#= item.value #'  /><span class='custom-control-indicator'></span><span class='custom-control-description'>#= item.text #</span><span class='custom-control-info'>#= item.add_info #</span></label>"

/*template: "<label class='custom-control custom-checkbox'><input type='checkbox' #= item.check# class='#= item.class #' name='role_resources[]' value='#= item.value #'><span class='custom-control-label'>#= item.text # <small>#= item.add_info #</small></span></label>"*/

template: "<label><input type='checkbox' #= item.check# class='#= item.class #' name='role_resources[]' value='#= item.value #'> #= item.text #</label>"

},

check: onCheck,

dataSource: [



{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('let_staff');?>",  add_info: "", check: "<?php if(isset($_GET['role_id'])) { if(in_array('103',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", value: "103",  items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('dashboard_employees');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "13", check: "<?php if(isset($_GET['role_id'])) { if(in_array('13',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "13", check: "<?php if(isset($_GET['role_id'])) { if(in_array('13',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "201", check: "<?php if(isset($_GET['role_id'])) { if(in_array('201',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_edit');?>", value: "202", check: "<?php if(isset($_GET['role_id'])) { if(in_array('202',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_delete');?>", value: "203", check: "<?php if(isset($_GET['role_id'])) { if(in_array('203',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hrsale_custom_fields');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "393",  items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "393",check: "<?php if(isset($_GET['role_id'])) { if(in_array('393',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "394",check: "<?php if(isset($_GET['role_id'])) { if(in_array('394',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_edit');?>", value: "395",check: "<?php if(isset($_GET['role_id'])) { if(in_array('395',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_delete');?>", value: "396",check: "<?php if(isset($_GET['role_id'])) { if(in_array('396',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},		

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_set_employees_salary');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "351", check: "<?php if(isset($_GET['role_id'])) { if(in_array('351',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_import_employees');?>",  add_info: "<?php echo $this->lang->line('xin_import_employees');?>", value: "92", check: "<?php if(isset($_GET['role_id'])) { if(in_array('92',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_employees_directory');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "88", check: "<?php if(isset($_GET['role_id'])) { if(in_array('88',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_employees_exit');?>",  add_info: "<?php echo $this->lang->line('xin_view_update');?>", value: "23", check: "<?php if(isset($_GET['role_id'])) { if(in_array('23',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "23", check: "<?php if(isset($_GET['role_id'])) { if(in_array('23',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "204", check: "<?php if(isset($_GET['role_id'])) { if(in_array('204',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_edit');?>", value: "205", check: "<?php if(isset($_GET['role_id'])) { if(in_array('205',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_delete');?>", value: "206", check: "<?php if(isset($_GET['role_id'])) { if(in_array('206',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_employees_exit').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "231", check: "<?php if(isset($_GET['role_id'])) { if(in_array('231',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_e_details_exp_documents');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "400",check: "<?php if(isset($_GET['role_id'])) { if(in_array('400',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_employees_last_login');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "22", check: "<?php if(isset($_GET['role_id'])) { if(in_array('22',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	//

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hr');?>",  add_info: "", check: "<?php if(isset($_GET['role_id'])) { if(in_array('12',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", value: "12",  items: [

	

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_awards');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "14", check: "<?php if(isset($_GET['role_id'])) { if(in_array('14',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "14", check: "<?php if(isset($_GET['role_id'])) { if(in_array('14',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "207", check: "<?php if(isset($_GET['role_id'])) { if(in_array('207',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "208", check: "<?php if(isset($_GET['role_id'])) { if(in_array('208',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "209", check: "<?php if(isset($_GET['role_id'])) { if(in_array('209',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_awards').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view').' '.$this->lang->line('left_awards');?>", value: "232", check: "<?php if(isset($_GET['role_id'])) { if(in_array('232',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_transfers');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "15", check: "<?php if(isset($_GET['role_id'])) { if(in_array('15',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "15", check: "<?php if(isset($_GET['role_id'])) { if(in_array('15',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "210", check: "<?php if(isset($_GET['role_id'])) { if(in_array('210',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "211", check: "<?php if(isset($_GET['role_id'])) { if(in_array('211',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "212", check: "<?php if(isset($_GET['role_id'])) { if(in_array('212',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_transfers').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "233", check: "<?php if(isset($_GET['role_id'])) { if(in_array('233',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_resignations');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "16", check: "<?php if(isset($_GET['role_id'])) { if(in_array('16',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "16", check: "<?php if(isset($_GET['role_id'])) { if(in_array('16',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "213", check: "<?php if(isset($_GET['role_id'])) { if(in_array('213',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "214", check: "<?php if(isset($_GET['role_id'])) { if(in_array('214',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "215", check: "<?php if(isset($_GET['role_id'])) { if(in_array('215',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_resignations').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "234", check: "<?php if(isset($_GET['role_id'])) { if(in_array('234',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_travels');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "17", check: "<?php if(isset($_GET['role_id'])) { if(in_array('17',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "17", check: "<?php if(isset($_GET['role_id'])) { if(in_array('17',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "216", check: "<?php if(isset($_GET['role_id'])) { if(in_array('216',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "217", check: "<?php if(isset($_GET['role_id'])) { if(in_array('217',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "218", check: "<?php if(isset($_GET['role_id'])) { if(in_array('218',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_travels').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "235", check: "<?php if(isset($_GET['role_id'])) { if(in_array('235',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_promotions');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "18", check: "<?php if(isset($_GET['role_id'])) { if(in_array('18',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "18", check: "<?php if(isset($_GET['role_id'])) { if(in_array('18',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "219", check: "<?php if(isset($_GET['role_id'])) { if(in_array('219',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "220", check: "<?php if(isset($_GET['role_id'])) { if(in_array('220',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "221", check: "<?php if(isset($_GET['role_id'])) { if(in_array('221',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_promotions').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "236", check: "<?php if(isset($_GET['role_id'])) { if(in_array('236',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_complaints');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "19", check: "<?php if(isset($_GET['role_id'])) { if(in_array('19',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "19", check: "<?php if(isset($_GET['role_id'])) { if(in_array('19',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "222", check: "<?php if(isset($_GET['role_id'])) { if(in_array('222',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "223", check: "<?php if(isset($_GET['role_id'])) { if(in_array('223',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "224", check: "<?php if(isset($_GET['role_id'])) { if(in_array('224',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_complaints').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "237", check: "<?php if(isset($_GET['role_id'])) { if(in_array('237',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_warnings');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "20", check: "<?php if(isset($_GET['role_id'])) { if(in_array('20',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "20", check: "<?php if(isset($_GET['role_id'])) { if(in_array('20',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "225", check: "<?php if(isset($_GET['role_id'])) { if(in_array('225',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "226", check: "<?php if(isset($_GET['role_id'])) { if(in_array('226',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "227", check: "<?php if(isset($_GET['role_id'])) { if(in_array('227',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_warnings').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "238", check: "<?php if(isset($_GET['role_id'])) { if(in_array('238',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_terminations');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "21", check: "<?php if(isset($_GET['role_id'])) { if(in_array('21',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "21", check: "<?php if(isset($_GET['role_id'])) { if(in_array('21',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "228", check: "<?php if(isset($_GET['role_id'])) { if(in_array('228',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "229", check: "<?php if(isset($_GET['role_id'])) { if(in_array('229',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "230", check: "<?php if(isset($_GET['role_id'])) { if(in_array('230',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_terminations').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "239", check: "<?php if(isset($_GET['role_id'])) { if(in_array('239',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]}

	]},

	

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_organization');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('2',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "", value:"2", items: [

	// sub 1

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_department');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "3", check: "<?php if(isset($_GET['role_id'])) { if(in_array('3',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "3", check: "<?php if(isset($_GET['role_id'])) { if(in_array('3',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "240", check: "<?php if(isset($_GET['role_id'])) { if(in_array('240',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "241", check: "<?php if(isset($_GET['role_id'])) { if(in_array('241',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "242", check: "<?php if(isset($_GET['role_id'])) { if(in_array('242',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_designation');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "4", check: "<?php if(isset($_GET['role_id'])) { if(in_array('4',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "4", check: "<?php if(isset($_GET['role_id'])) { if(in_array('4',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "243", check: "<?php if(isset($_GET['role_id'])) { if(in_array('243',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "244", check: "<?php if(isset($_GET['role_id'])) { if(in_array('244',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "245", check: "<?php if(isset($_GET['role_id'])) { if(in_array('245',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_designation').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "249",check: "<?php if(isset($_GET['role_id'])) { if(in_array('249',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_company');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "5", check: "<?php if(isset($_GET['role_id'])) { if(in_array('5',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "5", check: "<?php if(isset($_GET['role_id'])) { if(in_array('5',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "246", check: "<?php if(isset($_GET['role_id'])) { if(in_array('246',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "247", check: "<?php if(isset($_GET['role_id'])) { if(in_array('247',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "248", check: "<?php if(isset($_GET['role_id'])) { if(in_array('248',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_location');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "6", check: "<?php if(isset($_GET['role_id'])) { if(in_array('6',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "6", check: "<?php if(isset($_GET['role_id'])) { if(in_array('6',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "250", check: "<?php if(isset($_GET['role_id'])) { if(in_array('250',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "251", check: "<?php if(isset($_GET['role_id'])) { if(in_array('251',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "252", check: "<?php if(isset($_GET['role_id'])) { if(in_array('252',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_announcements');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "11", check: "<?php if(isset($_GET['role_id'])) { if(in_array('11',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "11", check: "<?php if(isset($_GET['role_id'])) { if(in_array('11',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "254", check: "<?php if(isset($_GET['role_id'])) { if(in_array('254',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "255", check: "<?php if(isset($_GET['role_id'])) { if(in_array('255',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "256", check: "<?php if(isset($_GET['role_id'])) { if(in_array('256',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_announcements').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "257", check: "<?php if(isset($_GET['role_id'])) { if(in_array('257',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_policies');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "9", check: "<?php if(isset($_GET['role_id'])) { if(in_array('9',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "9", check: "<?php if(isset($_GET['role_id'])) { if(in_array('9',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "258", check: "<?php if(isset($_GET['role_id'])) { if(in_array('258',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "259", check: "<?php if(isset($_GET['role_id'])) { if(in_array('259',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "260", check: "<?php if(isset($_GET['role_id'])) { if(in_array('260',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_org_chart_title');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "96", check: "<?php if(isset($_GET['role_id'])) { if(in_array('96',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},	

	]}, // sub 1 end

	

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_assets');?>",  add_info: "", check: "<?php if(isset($_GET['role_id'])) { if(in_array('24',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", value: "24",  items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_assets');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "25", check: "<?php if(isset($_GET['role_id'])) { if(in_array('25',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "25", check: "<?php if(isset($_GET['role_id'])) { if(in_array('25',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "262", check: "<?php if(isset($_GET['role_id'])) { if(in_array('262',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "263", check: "<?php if(isset($_GET['role_id'])) { if(in_array('263',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "264", check: "<?php if(isset($_GET['role_id'])) { if(in_array('264',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('xin_assets').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "265", check: "<?php if(isset($_GET['role_id'])) { if(in_array('265',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_acc_category');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "26", check: "<?php if(isset($_GET['role_id'])) { if(in_array('26',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "26", check: "<?php if(isset($_GET['role_id'])) { if(in_array('26',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "266", check: "<?php if(isset($_GET['role_id'])) { if(in_array('266',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "267", check: "<?php if(isset($_GET['role_id'])) { if(in_array('267',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "268", check: "<?php if(isset($_GET['role_id'])) { if(in_array('268',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hr_events_meetings');?>",  check: "<?php if(isset($_GET['role_id'])) { if(in_array('97',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "", value: "97",  items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hr_events');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "98", check: "<?php if(isset($_GET['role_id'])) { if(in_array('98',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "98", check: "<?php if(isset($_GET['role_id'])) { if(in_array('98',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "269", check: "<?php if(isset($_GET['role_id'])) { if(in_array('269',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "270", check: "<?php if(isset($_GET['role_id'])) { if(in_array('270',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "271", check: "<?php if(isset($_GET['role_id'])) { if(in_array('271',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('xin_hr_events').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "272", check: "<?php if(isset($_GET['role_id'])) { if(in_array('272',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hr_meetings');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "99", check: "<?php if(isset($_GET['role_id'])) { if(in_array('99',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "99", check: "<?php if(isset($_GET['role_id'])) { if(in_array('99',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "273", check: "<?php if(isset($_GET['role_id'])) { if(in_array('273',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "274", check: "<?php if(isset($_GET['role_id'])) { if(in_array('274',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "275", check: "<?php if(isset($_GET['role_id'])) { if(in_array('275',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('xin_hr_meetings').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "276", check: "<?php if(isset($_GET['role_id'])) { if(in_array('276',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_timesheet');?>",  add_info: "", check: "<?php if(isset($_GET['role_id'])) { if(in_array('27',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", value: "27",  items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_attendance');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "28", check: "<?php if(isset($_GET['role_id'])) { if(in_array('28',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "28", check: "<?php if(isset($_GET['role_id'])) { if(in_array('28',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_timesheet').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "397", check: "<?php if(isset($_GET['role_id'])) { if(in_array('397',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_month_timesheet_title');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "10", check: "<?php if(isset($_GET['role_id'])) { if(in_array('10',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "10", check: "<?php if(isset($_GET['role_id'])) { if(in_array('10',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('xin_month_timesheet_title').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "253",check: "<?php if(isset($_GET['role_id'])) { if(in_array('253',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	{ id: "", class: "role-checkbox", text: "<?php echo $this->lang->line('xin_attendance_timecalendar');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "261",check: "<?php if(isset($_GET['role_id'])) { if(in_array('261',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_date_wise_attendance');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "29", check: "<?php if(isset($_GET['role_id'])) { if(in_array('29',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "29", check: "<?php if(isset($_GET['role_id'])) { if(in_array('29',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_date_wise_attendance').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "381", check: "<?php if(isset($_GET['role_id'])) { if(in_array('381',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_update_attendance');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "30", check: "<?php if(isset($_GET['role_id'])) { if(in_array('30',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "30", check: "<?php if(isset($_GET['role_id'])) { if(in_array('30',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "277", check: "<?php if(isset($_GET['role_id'])) { if(in_array('277',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "278", check: "<?php if(isset($_GET['role_id'])) { if(in_array('278',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "279", check: "<?php if(isset($_GET['role_id'])) { if(in_array('279',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_upd_company_attendance').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "310", check: "<?php if(isset($_GET['role_id'])) { if(in_array('310',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_overtime_request');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "401", check: "<?php if(isset($_GET['role_id'])) { if(in_array('401',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "401", check: "<?php if(isset($_GET['role_id'])) { if(in_array('401',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "402", check: "<?php if(isset($_GET['role_id'])) { if(in_array('402',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "403", check: "<?php if(isset($_GET['role_id'])) { if(in_array('403',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_import_attendance');?>",  add_info: "<?php echo $this->lang->line('xin_attendance_import');?>", value: "31", check: "<?php if(isset($_GET['role_id'])) { if(in_array('31',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_office_shifts');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "7", check: "<?php if(isset($_GET['role_id'])) { if(in_array('7',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "7", check: "<?php if(isset($_GET['role_id'])) { if(in_array('7',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "280", check: "<?php if(isset($_GET['role_id'])) { if(in_array('280',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "281", check: "<?php if(isset($_GET['role_id'])) { if(in_array('281',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "282", check: "<?php if(isset($_GET['role_id'])) { if(in_array('282',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_change_default');?>",  add_info: "<?php echo $this->lang->line('xin_role_change_default');?>", value: "2822", check: "<?php if(isset($_GET['role_id'])) { if(in_array('2822',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_office_shifts').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "311", check: "<?php if(isset($_GET['role_id'])) { if(in_array('311',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_holidays');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "8", check: "<?php if(isset($_GET['role_id'])) { if(in_array('8',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "8", check: "<?php if(isset($_GET['role_id'])) { if(in_array('8',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "283", check: "<?php if(isset($_GET['role_id'])) { if(in_array('283',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "284", check: "<?php if(isset($_GET['role_id'])) { if(in_array('284',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "285", check: "<?php if(isset($_GET['role_id'])) { if(in_array('285',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_leaves');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "46", check: "<?php if(isset($_GET['role_id'])) { if(in_array('46',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "46", check: "<?php if(isset($_GET['role_id'])) { if(in_array('46',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "287", check: "<?php if(isset($_GET['role_id'])) { if(in_array('287',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "288", check: "<?php if(isset($_GET['role_id'])) { if(in_array('288',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "289", check: "<?php if(isset($_GET['role_id'])) { if(in_array('289',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_leaves').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "290", check: "<?php if(isset($_GET['role_id'])) { if(in_array('290',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_1st_level_approval').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "286", check: "<?php if(isset($_GET['role_id'])) { if(in_array('286',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_2nd_level_approval').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "312", check: "<?php if(isset($_GET['role_id'])) { if(in_array('312',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_recruitment');?>",  add_info: "", check: "<?php if(isset($_GET['role_id'])) { if(in_array('48',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", value: "48",  items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_job_posts');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "49", check: "<?php if(isset($_GET['role_id'])) { if(in_array('49',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "49", check: "<?php if(isset($_GET['role_id'])) { if(in_array('49',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "291", check: "<?php if(isset($_GET['role_id'])) { if(in_array('291',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "292", check: "<?php if(isset($_GET['role_id'])) { if(in_array('292',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "293", check: "<?php if(isset($_GET['role_id'])) { if(in_array('293',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_jobs_listing');?> <small><?php echo $this->lang->line('left_frontend');?></small>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "50", check: "<?php if(isset($_GET['role_id'])) { if(in_array('50',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_job_candidates');?>",  add_info: "<?php echo $this->lang->line('xin_update_status_delete');?>", value: "51", check: "<?php if(isset($_GET['role_id'])) { if(in_array('51',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "51", check: "<?php if(isset($_GET['role_id'])) { if(in_array('51',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_dwn_resume');?>",  add_info: "<?php echo $this->lang->line('xin_role_dwn_resume');?>", value: "294", check: "<?php if(isset($_GET['role_id'])) { if(in_array('294',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_delete');?>", value: "295", check: "<?php if(isset($_GET['role_id'])) { if(in_array('295',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_view_own');?>",  add_info: "<?php echo $this->lang->line('xin_role_view_own');?>", value: "387", check: "<?php if(isset($_GET['role_id'])) { if(in_array('387',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_job_interviews');?>",  add_info: "<?php echo $this->lang->line('xin_add_delete');?>", value: "52", check: "<?php if(isset($_GET['role_id'])) { if(in_array('52',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "52", check: "<?php if(isset($_GET['role_id'])) { if(in_array('52',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "296", check: "<?php if(isset($_GET['role_id'])) { if(in_array('296',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "297", check: "<?php if(isset($_GET['role_id'])) { if(in_array('297',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_view_own');?>",  add_info: "<?php echo $this->lang->line('xin_role_view_own');?>", value: "388", check: "<?php if(isset($_GET['role_id'])) { if(in_array('388',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_payroll');?>",  add_info: "", check: "<?php if(isset($_GET['role_id'])) { if(in_array('32',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", value: "32",  items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_generate_payslip');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "36", check: "<?php if(isset($_GET['role_id'])) { if(in_array('36',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "36",check: "<?php if(isset($_GET['role_id'])) { if(in_array('36',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "313",check: "<?php if(isset($_GET['role_id'])) { if(in_array('313',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_generate_company_payslips').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "314",check: "<?php if(isset($_GET['role_id'])) { if(in_array('314',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_payment_history');?>",  add_info: "<?php echo $this->lang->line('xin_view_payslip');?>", value: "37", check: "<?php if(isset($_GET['role_id'])) { if(in_array('37',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "37", check: "<?php if(isset($_GET['role_id'])) { if(in_array('37',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_payment_history').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "391", check: "<?php if(isset($_GET['role_id'])) { if(in_array('391',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	]},

	

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_performance');?>",  add_info: "", check: "<?php if(isset($_GET['role_id'])) { if(in_array('40',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", value: "40",  items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_performance_indicator');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "41", check: "<?php if(isset($_GET['role_id'])) { if(in_array('41',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "41", check: "<?php if(isset($_GET['role_id'])) { if(in_array('41',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "298", check: "<?php if(isset($_GET['role_id'])) { if(in_array('298',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "299", check: "<?php if(isset($_GET['role_id'])) { if(in_array('299',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "300", check: "<?php if(isset($_GET['role_id'])) { if(in_array('300',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').'<br>'.$this->lang->line('left_performance_indicator').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "301", check: "<?php if(isset($_GET['role_id'])) { if(in_array('301',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_performance_appraisal');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "42",check: "<?php if(isset($_GET['role_id'])) { if(in_array('42',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "42", check: "<?php if(isset($_GET['role_id'])) { if(in_array('42',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "302", check: "<?php if(isset($_GET['role_id'])) { if(in_array('302',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "303", check: "<?php if(isset($_GET['role_id'])) { if(in_array('303',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "304", check: "<?php if(isset($_GET['role_id'])) { if(in_array('304',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').'<br>'.$this->lang->line('left_performance_appraisal').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "305", check: "<?php if(isset($_GET['role_id'])) { if(in_array('305',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_performance_kpi');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "372",check: "<?php if(isset($_GET['role_id'])) { if(in_array('372',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('kpi_report');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "373",check: "<?php if(isset($_GET['role_id'])) { if(in_array('373',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_tickets');?>",  check: "<?php if(isset($_GET['role_id'])) { if(in_array('43',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "<?php echo $this->lang->line('xin_create_edit_view_delete');?>", value: "43",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "43", check: "<?php if(isset($_GET['role_id'])) { if(in_array('43',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "306", check: "<?php if(isset($_GET['role_id'])) { if(in_array('306',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "307", check: "<?php if(isset($_GET['role_id'])) { if(in_array('307',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "308", check: "<?php if(isset($_GET['role_id'])) { if(in_array('308',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_tickets').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "309", check: "<?php if(isset($_GET['role_id'])) { if(in_array('309',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "Project Manager",  add_info: "", check: "<?php if(isset($_GET['role_id'])) { if(in_array('104',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", value: "104",  items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_projects');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "44", check: "<?php if(isset($_GET['role_id'])) { if(in_array('44',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "44", check: "<?php if(isset($_GET['role_id'])) { if(in_array('44',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "315", check: "<?php if(isset($_GET['role_id'])) { if(in_array('315',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "316", check: "<?php if(isset($_GET['role_id'])) { if(in_array('316',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "317", check: "<?php if(isset($_GET['role_id'])) { if(in_array('317',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_projects').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "318", check: "<?php if(isset($_GET['role_id'])) { if(in_array('318',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_tasks');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "45", check: "<?php if(isset($_GET['role_id'])) { if(in_array('45',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "45", check: "<?php if(isset($_GET['role_id'])) { if(in_array('45',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "319", check: "<?php if(isset($_GET['role_id'])) { if(in_array('319',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "320", check: "<?php if(isset($_GET['role_id'])) { if(in_array('320',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "321", check: "<?php if(isset($_GET['role_id'])) { if(in_array('321',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_tasks').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "322", check: "<?php if(isset($_GET['role_id'])) { if(in_array('322',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},
	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hr_goal_tracking');?>",  check: "<?php if(isset($_GET['role_id'])) { if(in_array('106',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "", value: "106",  items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hr_goal_tracking');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "107", check: "<?php if(isset($_GET['role_id'])) { if(in_array('107',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "107", check: "<?php if(isset($_GET['role_id'])) { if(in_array('107',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "334", check: "<?php if(isset($_GET['role_id'])) { if(in_array('334',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "335", check: "<?php if(isset($_GET['role_id'])) { if(in_array('335',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "336", check: "<?php if(isset($_GET['role_id'])) { if(in_array('336',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hr_goal_tracking_type');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "108", check: "<?php if(isset($_GET['role_id'])) { if(in_array('108',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "108", check: "<?php if(isset($_GET['role_id'])) { if(in_array('108',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "338", check: "<?php if(isset($_GET['role_id'])) { if(in_array('338',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "339", check: "<?php if(isset($_GET['role_id'])) { if(in_array('339',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "340", check: "<?php if(isset($_GET['role_id'])) { if(in_array('340',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	]},

	

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_files_manager');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "47", check: "<?php if(isset($_GET['role_id'])) { if(in_array('47',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},


	//Finance
	{ id: "", class: "role-checkbox",text: "Finance", add_info: "",value: "574", check: "<?php if (isset($_GET['role_id'])) {echo in_array('574',$role_resources_ids) ? 'checked' : '';} ?>", items: [

	{ id: "", class: "role-checkbox", text: "Bill Payment Requests",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "575",check: "<?php if (isset($_GET['role_id'])) {echo in_array('575',$role_resources_ids) ? 'checked' : '';} ?>", items:[
		{ id: "", class: "role-checkbox", text: "View",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "576",check: "<?php if (isset($_GET['role_id'])) {echo in_array('576',$role_resources_ids) ? 'checked' : '';} ?>"},	
		{ id: "", class: "role-checkbox", text: "Approve",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "577",check: "<?php if (isset($_GET['role_id'])) {echo in_array('577',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Reject",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "578",check: "<?php if (isset($_GET['role_id'])) {echo in_array('578',$role_resources_ids) ? 'checked' : '';} ?>"},
	]},

	{ id: "", class: "role-checkbox", text: "All Bills Approved",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "579",check: "<?php if (isset($_GET['role_id'])) {echo in_array('579',$role_resources_ids) ? 'checked' : '';} ?>",items:[
		{ id: "", class: "role-checkbox", text: "View",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "580",check: "<?php if (isset($_GET['role_id'])) {echo in_array('580',$role_resources_ids) ? 'checked' : '';} ?>"},	
		{ id: "", class: "role-checkbox", text: "Approve",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "581",check: "<?php if (isset($_GET['role_id'])) {echo in_array('581',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Reject",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "582",check: "<?php if (isset($_GET['role_id'])) {echo in_array('582',$role_resources_ids) ? 'checked' : '';} ?>"},
	]},

	{ id: "", class: "role-checkbox", text: "Single Bills Payment",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "583",check: "<?php if (isset($_GET['role_id'])) {echo in_array('583',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Bulk Bills Payment",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "584",check: "<?php if (isset($_GET['role_id'])) {echo in_array('584',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Payment Schedule",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "636",check: "<?php if (isset($_GET['role_id'])) {echo in_array('636',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Provider Bills Reports",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "585",check: "<?php if (isset($_GET['role_id'])) {echo in_array('585',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Paid Debit Note",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "586",check: "<?php if (isset($_GET['role_id'])) {echo in_array('586',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Unpaid Debit Note",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "587",check: "<?php if (isset($_GET['role_id'])) {echo in_array('587',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Debit Note about to expire",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "588",check: "<?php if (isset($_GET['role_id'])) {echo in_array('588',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Payees",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "589",check: "<?php if (isset($_GET['role_id'])) {echo in_array('589',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Payers",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "590",check: "<?php if (isset($_GET['role_id'])) {echo in_array('590',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Tax Type",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "591",check: "<?php if (isset($_GET['role_id'])) {echo in_array('591',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Journal",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "592",check: "<?php if (isset($_GET['role_id'])) {echo in_array('592',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Vendors",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "593",check: "<?php if (isset($_GET['role_id'])) {echo in_array('593',$role_resources_ids) ? 'checked' : '';} ?>"},
	
	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_invoices_title');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "121", check: "<?php if(isset($_GET['role_id'])) { if(in_array('121',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_create');?>",  add_info: "<?php echo $this->lang->line('xin_role_create');?>", value: "120", check: "<?php if(isset($_GET['role_id'])) { if(in_array('120',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "121", check: "<?php if(isset($_GET['role_id'])) { if(in_array('121',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "328", check: "<?php if(isset($_GET['role_id'])) { if(in_array('328',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "329", check: "<?php if(isset($_GET['role_id'])) { if(in_array('329',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_acc_invoice_payments');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "330",check: "<?php if(isset($_GET['role_id'])) { if(in_array('330',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_invoice_tax_type');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "122", check: "<?php if(isset($_GET['role_id'])) { if(in_array('122',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "122", check: "<?php if(isset($_GET['role_id'])) { if(in_array('122',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "331", check: "<?php if(isset($_GET['role_id'])) { if(in_array('331',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "332", check: "<?php if(isset($_GET['role_id'])) { if(in_array('332',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "333", check: "<?php if(isset($_GET['role_id'])) { if(in_array('333',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},
	]},
	
{ id: "", class: "role-checkbox-modal",text: "<?php echo $this->lang->line('xin_acc_accounts');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('71',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "",value: "71",  items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_acc_account_list');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('72',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "72",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "72", check: "<?php if(isset($_GET['role_id'])) { if(in_array('72',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "352", check: "<?php if(isset($_GET['role_id'])) { if(in_array('352',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "353", check: "<?php if(isset($_GET['role_id'])) { if(in_array('353',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "354", check: "<?php if(isset($_GET['role_id'])) { if(in_array('354',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_acc_account_balances');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('73',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "73",},

	]},

	{ id: "", class: "role-checkbox-modal",text: "<?php echo $this->lang->line('xin_acc_transactions');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('74',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "",value: "74",  items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_acc_deposit');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('75',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "75",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "75", check: "<?php if(isset($_GET['role_id'])) { if(in_array('75',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "355", check: "<?php if(isset($_GET['role_id'])) { if(in_array('355',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "356", check: "<?php if(isset($_GET['role_id'])) { if(in_array('356',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "357", check: "<?php if(isset($_GET['role_id'])) { if(in_array('357',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_acc_expense');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('76',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "76",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "76", check: "<?php if(isset($_GET['role_id'])) { if(in_array('76',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "358", check: "<?php if(isset($_GET['role_id'])) { if(in_array('358',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "359", check: "<?php if(isset($_GET['role_id'])) { if(in_array('359',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "360", check: "<?php if(isset($_GET['role_id'])) { if(in_array('360',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},


	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_acc_view_transactions');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('78',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "<?php echo $this->lang->line('xin_view');?>", value: "78",},

	

	]},
	//Finance
	
	{ id: "", class: "role-checkbox-modal",text: "Financial Reporting", check: "<?php if(isset($_GET['role_id'])) { if(in_array('82',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "",value: "82",  items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_acc_account_statement');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('83',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "<?php echo $this->lang->line('xin_view');?>", value: "83"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_acc_expense_reports');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('84',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "<?php echo $this->lang->line('xin_view');?>", value: "84",},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_acc_income_reports');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('85',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "<?php echo $this->lang->line('xin_view');?>", value: "85",},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_acc_transfer_report');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('86',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "86",},
	{ id: "", class: "role-checkbox", text: "Profit & Loss",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "649",check: "<?php if (isset($_GET['role_id'])) {echo in_array('649',$role_resources_ids) ? 'checked' : '';} ?>"},
	{ id: "", class: "role-checkbox", text: "Staff Income",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "650",check: "<?php if (isset($_GET['role_id'])) {echo in_array('650',$role_resources_ids) ? 'checked' : '';} ?>"},
	{ id: "", class: "role-checkbox", text: "Trial Balance",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "651",check: "<?php if (isset($_GET['role_id'])) {echo in_array('651',$role_resources_ids) ? 'checked' : '';} ?>"},
	
	]},
]

});



jQuery("#treeview_m2").kendoTreeView({

checkboxes: {

checkChildren: true,

//template: "<label class='custom-control custom-checkbox'><input type='checkbox' #= item.check# class='#= item.class #' name='role_resources[]' value='#= item.value #'  /><span class='custom-control-indicator'></span><span class='custom-control-description'>#= item.text #</span><span class='custom-control-info'>#= item.add_info #</span></label>"

/*template: "<label class='custom-control custom-checkbox'><input type='checkbox' #= item.check# class='#= item.class #' name='role_resources[]' value='#= item.value #'><span class='custom-control-label'>#= item.text # <small>#= item.add_info #</small></span></label>"*/

template: "<label><input type='checkbox' #= item.check# class='#= item.class #' name='role_resources[]' value='#= item.value #'> #= item.text #</label>"

},

check: onCheck,

dataSource: [

//

{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_training');?>",  add_info: "", value: "53", check: "<?php if(isset($_GET['role_id'])) { if(in_array('53',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", items: [

{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_training_list');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('54',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "<?php echo $this->lang->line('xin_add_edit_view_delete_role_info');?>", value: "54",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "54", check: "<?php if(isset($_GET['role_id'])) { if(in_array('54',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "341", check: "<?php if(isset($_GET['role_id'])) { if(in_array('341',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "342", check: "<?php if(isset($_GET['role_id'])) { if(in_array('342',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "343", check: "<?php if(isset($_GET['role_id'])) { if(in_array('343',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo '<small>'.$this->lang->line('xin_role_view').' '.$this->lang->line('left_training').'</small>';?>",  add_info: "<?php echo $this->lang->line('xin_role_view');?>", value: "344", check: "<?php if(isset($_GET['role_id'])) { if(in_array('344',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_training_type');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('55',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "55",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "55", check: "<?php if(isset($_GET['role_id'])) { if(in_array('55',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "345", check: "<?php if(isset($_GET['role_id'])) { if(in_array('345',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "346", check: "<?php if(isset($_GET['role_id'])) { if(in_array('346',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "347", check: "<?php if(isset($_GET['role_id'])) { if(in_array('347',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_trainers_list');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('56',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "56",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "56", check: "<?php if(isset($_GET['role_id'])) { if(in_array('56',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "348", check: "<?php if(isset($_GET['role_id'])) { if(in_array('348',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "349", check: "<?php if(isset($_GET['role_id'])) { if(in_array('349',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "350", check: "<?php if(isset($_GET['role_id'])) { if(in_array('350',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

]},

{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_system');?>",  add_info: "", check: "<?php if(isset($_GET['role_id'])) { if(in_array('57',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",value: "57",  items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_settings');?>",  add_info: "<?php echo $this->lang->line('xin_view_update');?>", value: "60", check: "<?php if(isset($_GET['role_id'])) { if(in_array('60',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_constants');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "61", check: "<?php if(isset($_GET['role_id'])) { if(in_array('61',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox", text: "<?php echo $this->lang->line('xin_acc_payment_gateway');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "118", check: "<?php if(isset($_GET['role_id'])) { if(in_array('118',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_db_backup');?>",  add_info: "<?php echo $this->lang->line('xin_create_delete_download');?>", value: "62", check: "<?php if(isset($_GET['role_id'])) { if(in_array('62',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('left_email_templates');?>",  add_info: "<?php echo $this->lang->line('xin_update');?>", value: "63", check: "<?php if(isset($_GET['role_id'])) { if(in_array('63',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_setup_modules');?>",  add_info: "<?php echo $this->lang->line('xin_update');?>", value: "93", check: "<?php if(isset($_GET['role_id'])) { if(in_array('93',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('hd_changelog');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "87", check: "<?php if(isset($_GET['role_id'])) { if(in_array('87',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_lang_settings');?>",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "89", check: "<?php if(isset($_GET['role_id'])) { if(in_array('89',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "89", check: "<?php if(isset($_GET['role_id'])) { if(in_array('89',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "370", check: "<?php if(isset($_GET['role_id'])) { if(in_array('370',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "371", check: "<?php if(isset($_GET['role_id'])) { if(in_array('371',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"}

	]},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_notify_top');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "90", check: "<?php if(isset($_GET['role_id'])) { if(in_array('90',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('header_apply_jobs');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "91", check: "<?php if(isset($_GET['role_id'])) { if(in_array('91',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_theme_settings');?>",  add_info: "<?php echo $this->lang->line('xin_theme_settings');?>", value: "94", check: "<?php if(isset($_GET['role_id'])) { if(in_array('94',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hr_calendar_title');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "95", check: "<?php if(isset($_GET['role_id'])) { if(in_array('95',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal",text: "<?php echo $this->lang->line('xin_hr_report_title');?>", check: "<?php if(isset($_GET['role_id'])) { if(in_array('110',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>", add_info: "",value: "110", items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hr_reports_payslip');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "111", check: "<?php if(isset($_GET['role_id'])) { if(in_array('111',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hr_reports_attendance_employee');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "112", check: "<?php if(isset($_GET['role_id'])) { if(in_array('112',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hr_reports_training');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "113", check: "<?php if(isset($_GET['role_id'])) { if(in_array('113',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hr_reports_projects');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "114", check: "<?php if(isset($_GET['role_id'])) { if(in_array('114',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hr_reports_tasks');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "115", check: "<?php if(isset($_GET['role_id'])) { if(in_array('115',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hr_report_user_roles');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "116", check: "<?php if(isset($_GET['role_id'])) { if(in_array('116',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_hr_report_employees');?>",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "117", check: "<?php if(isset($_GET['role_id'])) { if(in_array('117',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},

	
	//Auth menu
	{ id: "", class: "role-checkbox",text: "Auth Menu", add_info: "",value: "516", check: "<?php if (isset($_GET['role_id'])) {echo in_array('516',$role_resources_ids) ? 'checked' : '';} ?>"  ,items: [

	{ id: "", class: "role-checkbox", text: "Authorization Codes",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "517", check: "<?php if (isset($_GET['role_id'])) {echo in_array('517',$role_resources_ids) ? 'checked' : '';} ?>", items: [
		{ id: "", class: "role-checkbox", text: "View",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "518", check: "<?php if (isset($_GET['role_id'])) {echo in_array('518',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Approve/Reject",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "519", check: "<?php if (isset($_GET['role_id'])) {echo in_array('519',$role_resources_ids) ? 'checked' : '';} ?>"}	
	]},
	
	{ id: "", class: "role-checkbox", text: "Request Code",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "522", check: "<?php if (isset($_GET['role_id'])) {echo in_array('522',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Backend Code Request",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "523", check: "<?php if (isset($_GET['role_id'])) {echo in_array('523',$role_resources_ids) ? 'checked' : '';} ?>"},
	
	{ id: "", class: "role-checkbox", text: "Backend Code Approver",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "653"},
	
	{ id: "", class: "role-checkbox", text: "All Authorization",  add_info: "<?php echo $this->lang->line('xin_view');?>",value: "524", check: "<?php if (isset($_GET['role_id'])) {echo in_array('524',$role_resources_ids) ? 'checked' : '';} ?>"},
	
	{ id: "", class: "role-checkbox", text: "All Reject Codes",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "521", check: "<?php if (isset($_GET['role_id'])) {echo in_array('521',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Adjusted Request",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "638",check: "<?php if (isset($_GET['role_id'])) {echo in_array('638',$role_resources_ids) ? 'checked' : '';} ?>",items:[
			{ id: "", class: "role-checkbox", text: "View",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "639",check: "<?php if (isset($_GET['role_id'])) {echo in_array('639',$role_resources_ids) ? 'checked' : '';} ?>"},
			{ id: "", class: "role-checkbox", text: "Delete Drugs",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "640",check: "<?php if (isset($_GET['role_id'])) {echo in_array('640',$role_resources_ids) ? 'checked' : '';} ?>"},
			{ id: "", class: "role-checkbox", text: "Delete Services",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "641",check: "<?php if (isset($_GET['role_id'])) {echo in_array('641',$role_resources_ids) ? 'checked' : '';} ?>"},
		]},

	]},

	//Provider Bills
	{ id: "", class: "role-checkbox",text: "Provider Bills", add_info: "",value: "525", check: "<?php if (isset($_GET['role_id'])) {echo in_array('525',$role_resources_ids) ? 'checked' : '';} ?>",  items: [

	{ id: "", class: "role-checkbox", text: "Bill Requests",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "526", check: "<?php if (isset($_GET['role_id'])) {echo in_array('526',$role_resources_ids) ? 'checked' : '';} ?>", items:[

		{ id: "", class: "role-checkbox", text: "View",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "527",check: "<?php if (isset($_GET['role_id'])) {echo in_array('527',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "First Approve",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "528",check: "<?php if (isset($_GET['role_id'])) {echo in_array('528',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Second Approve",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "529",check: "<?php if (isset($_GET['role_id'])) {echo in_array('529',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Reject",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "530",check: "<?php if (isset($_GET['role_id'])) {echo in_array('530',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Add/View Note",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "654",check: "<?php if (isset($_GET['role_id'])) {echo in_array('654',$role_resources_ids) ? 'checked' : '';} ?>"},

	]},

	{ id: "", class: "role-checkbox", text: "Rejected Bill Requests",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "531",check: "<?php if (isset($_GET['role_id'])) {echo in_array('531',$role_resources_ids) ? 'checked' : '';} ?>"},
	
	{ id: "", class: "role-checkbox", text: "All Bills Approved",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "532",check: "<?php if (isset($_GET['role_id'])) {echo in_array('532',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Provider Bills Report",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "533",check: "<?php if (isset($_GET['role_id'])) {echo in_array('533',$role_resources_ids) ? 'checked' : '';} ?>"},
	
	]},
	//Provider Bills

	//Enrollee Requests
	{ id: "", class: "role-checkbox",text: "Enrollee Requests", add_info: "",value: "534",check: "<?php if (isset($_GET['role_id'])) {echo in_array('534',$role_resources_ids) ? 'checked' : '';} ?>",  items: [

	{ id: "", class: "role-checkbox", text: "Provider Change Requests",  add_info: "<?php echo $this->lang->line('xin_view');?>",check: "<?php if (isset($_GET['role_id'])) {echo in_array('535',$role_resources_ids) ? 'checked' : '';} ?>", value: "535", items:[

		{ id: "", class: "role-checkbox", text: "View",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "536",check: "<?php if (isset($_GET['role_id'])) {echo in_array('536',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Approve",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "537",check: "<?php if (isset($_GET['role_id'])) {echo in_array('537',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Reject",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "538",check: "<?php if (isset($_GET['role_id'])) {echo in_array('538',$role_resources_ids) ? 'checked' : '';} ?>"},

	]},
	
	{ id: "", class: "role-checkbox", text: "All Approved",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "643",check: "<?php if (isset($_GET['role_id'])) {echo in_array('643',$role_resources_ids) ? 'checked' : '';} ?>"},
	{ id: "", class: "role-checkbox", text: "All Rejected",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "644",check: "<?php if (isset($_GET['role_id'])) {echo in_array('644',$role_resources_ids) ? 'checked' : '';} ?>"},
	

	{ id: "", class: "role-checkbox", text: "Dependant Requests",  add_info: "<?php echo $this->lang->line('xin_view');?>",check: "<?php if (isset($_GET['role_id'])) {echo in_array('539',$role_resources_ids) ? 'checked' : '';} ?>", value: "539", items: [
		{ id: "", class: "role-checkbox", text: "View",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "540",check: "<?php if (isset($_GET['role_id'])) {echo in_array('540',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Approve",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "541",check: "<?php if (isset($_GET['role_id'])) {echo in_array('541',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Reject",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "542",check: "<?php if (isset($_GET['role_id'])) {echo in_array('542',$role_resources_ids) ? 'checked' : '';} ?>"},
	]},
	
	{ id: "", class: "role-checkbox", text: "New Enrollee Request",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "543",check: "<?php if (isset($_GET['role_id'])) {echo in_array('543',$role_resources_ids) ? 'checked' : '';} ?>",items: [
		{ id: "", class: "role-checkbox", text: "View",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "544",check: "<?php if (isset($_GET['role_id'])) {echo in_array('544',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Approve",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "545",check: "<?php if (isset($_GET['role_id'])) {echo in_array('545',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Reject",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "546",check: "<?php if (isset($_GET['role_id'])) {echo in_array('546',$role_resources_ids) ? 'checked' : '';} ?>"},
	]},
	
	]},
	//Enrollee Requests

	//Memo Area
	{ id: "", class: "role-checkbox",text: "Memo Area", add_info: "",value: "547",check: "<?php if (isset($_GET['role_id'])) {echo in_array('547',$role_resources_ids) ? 'checked' : '';} ?>",  items: [

	{ id: "", class: "role-checkbox", text: "Memo List",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "548",check: "<?php if (isset($_GET['role_id'])) {echo in_array('548',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Memo Approval",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "549",check: "<?php if (isset($_GET['role_id'])) {echo in_array('549',$role_resources_ids) ? 'checked' : '';} ?>", items: [
		{ id: "", class: "role-checkbox", text: "View",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "550",check: "<?php if (isset($_GET['role_id'])) {echo in_array('550',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Approve",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "551",check: "<?php if (isset($_GET['role_id'])) {echo in_array('551',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Reject",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "552",check: "<?php if (isset($_GET['role_id'])) {echo in_array('552',$role_resources_ids) ? 'checked' : '';} ?>"},
	]},
	
	{ id: "", class: "role-checkbox", text: "Memo Approved",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "553",check: "<?php if (isset($_GET['role_id'])) {echo in_array('553',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "All Memo Info",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "626",check: "<?php if (isset($_GET['role_id'])) {echo in_array('553',$role_resources_ids) ? 'checked' : '';} ?>"},
	
	]},
	//Memo Area

	//Dashboard
	{ id: "", class: "role-checkbox",text: "Dashboard", add_info: "",value: "554",check: "<?php if (isset($_GET['role_id'])) {echo in_array('554',$role_resources_ids) ? 'checked' : '';} ?>",  items: [

	{ id: "", class: "role-checkbox", text: "Total Principal",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "555",check: "<?php if (isset($_GET['role_id'])) {echo in_array('555',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Total Dependant",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "563",check: "<?php if (isset($_GET['role_id'])) {echo in_array('563',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Total Individual Accounts",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "564",check: "<?php if (isset($_GET['role_id'])) {echo in_array('564',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Total Family Accounts",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "565",check: "<?php if (isset($_GET['role_id'])) {echo in_array('565',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Debit Note Payments",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "566",check: "<?php if (isset($_GET['role_id'])) {echo in_array('566',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Pending D. Note Payments",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "567",check: "<?php if (isset($_GET['role_id'])) {echo in_array('567',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Total Bills Payment",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "568",check: "<?php if (isset($_GET['role_id'])) {echo in_array('568',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Total Organizataions",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "569",check: "<?php if (isset($_GET['role_id'])) {echo in_array('569',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "D. Note Expiring in 90 Days",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "570",check: "<?php if (isset($_GET['role_id'])) {echo in_array('570',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Total Providers",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "571",check: "<?php if (isset($_GET['role_id'])) {echo in_array('571',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Total Debit Notes",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "572",check: "<?php if (isset($_GET['role_id'])) {echo in_array('572',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Unpaid Debit Notes",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "573",check: "<?php if (isset($_GET['role_id'])) {echo in_array('573',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Total Auth Code Approved",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "627",check: "<?php if (isset($_GET['role_id'])) {echo in_array('627',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Total Auth Code Pending",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "628",check: "<?php if (isset($_GET['role_id'])) {echo in_array('628',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Total Bills Approved",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "629",check: "<?php if (isset($_GET['role_id'])) {echo in_array('629',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Total Bills Pending",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "630",check: "<?php if (isset($_GET['role_id'])) {echo in_array('630',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Total Enrollee Requests",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "631",check: "<?php if (isset($_GET['role_id'])) {echo in_array('631',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Total Number of All Enrollees",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "632",check: "<?php if (isset($_GET['role_id'])) {echo in_array('632',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Complains",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "633",check: "<?php if (isset($_GET['role_id'])) {echo in_array('633',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Balances",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "634",check: "<?php if (isset($_GET['role_id'])) {echo in_array('633',$role_resources_ids) ? 'checked' : '';} ?>"},
	
	]},
	//Dashboard

	//Enrollee Area
	{ id: "", class: "role-checkbox",text: "Enrollee Area", add_info: "",value: "556",check: "<?php if (isset($_GET['role_id'])) {echo in_array('556',$role_resources_ids) ? 'checked' : '';} ?>",  items: [
	{ id: "", class: "role-checkbox-modal", text: "Principal Account",  add_info: "<?php echo $this->lang->line('xin_add_edit_delete_role_info');?>", value: "119", check: "<?php if(isset($_GET['role_id'])) { if(in_array('119',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>",items: [

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_enable');?>",  add_info: "<?php echo $this->lang->line('xin_role_enable');?>", value: "119", check: "<?php if(isset($_GET['role_id'])) { if(in_array('119',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_add');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "323", check: "<?php if(isset($_GET['role_id'])) { if(in_array('323',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_edit');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "324", check: "<?php if(isset($_GET['role_id'])) { if(in_array('324',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	{ id: "", class: "role-checkbox-modal", text: "<?php echo $this->lang->line('xin_role_delete');?>",  add_info: "<?php echo $this->lang->line('xin_role_add');?>", value: "325", check: "<?php if(isset($_GET['role_id'])) { if(in_array('325',$role_resources_ids)): echo 'checked'; else: echo ''; endif; }?>"},

	]},
	
	{ id: "", class: "role-checkbox", text: "Principal Enrollee",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "557",check: "<?php if (isset($_GET['role_id'])) {echo in_array('557',$role_resources_ids) ? 'checked' : '';} ?>", items: [
		{ id: "", class: "role-checkbox", text: "View",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "558",check: "<?php if (isset($_GET['role_id'])) {echo in_array('558',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Upload",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "559",check: "<?php if (isset($_GET['role_id'])) {echo in_array('559',$role_resources_ids) ? 'checked' : '';} ?>"},		
	]},
	
	{ id: "", class: "role-checkbox", text: "Dependant Account",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "561"},

	{ id: "", class: "role-checkbox", text: "Dependant Account",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "561",check: "<?php if (isset($_GET['role_id'])) {echo in_array('561',$role_resources_ids) ? 'checked' : '';} ?>", items: [

		{ id: "", class: "role-checkbox", text: "Create",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "646",check: "<?php if (isset($_GET['role_id'])) {echo in_array('646',$role_resources_ids) ? 'checked' : '';} ?>"},
		{ id: "", class: "role-checkbox", text: "Upload",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "647",check: "<?php if (isset($_GET['role_id'])) {echo in_array('647',$role_resources_ids) ? 'checked' : '';} ?>"},		

	]},
	{ id: "", class: "role-checkbox", text: "Enrollee Profiles",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "562",check: "<?php if (isset($_GET['role_id'])) {echo in_array('562',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Enrollee Report",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "560",check: "<?php if (isset($_GET['role_id'])) {echo in_array('560',$role_resources_ids) ? 'checked' : '';} ?>"},
	
	]},
	
	//Enrollee Area

	//Bussiness Reports
	{ id: "", class: "role-checkbox",text: "HMO Data Report", add_info: "",value: "594",check: "<?php if (isset($_GET['role_id'])) {echo in_array('594',$role_resources_ids) ? 'checked' : '';} ?>",  items: [

	{ id: "", class: "role-checkbox", text: "Provider Report",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "595",check: "<?php if (isset($_GET['role_id'])) {echo in_array('595',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Principals & Dependants",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "596",check: "<?php if (isset($_GET['role_id'])) {echo in_array('596',$role_resources_ids) ? 'checked' : '';} ?>"},

	// { id: "", class: "role-checkbox", text: "Clients Report",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "597",check: "<?php if (isset($_GET['role_id'])) {echo in_array('597',$role_resources_ids) ? 'checked' : '';} ?>", items:[
	// 	{ id: "", class: "role-checkbox", text: "View",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "598",check: "<?php if (isset($_GET['role_id'])) {echo in_array('598',$role_resources_ids) ? 'checked' : '';} ?>"},	
	// 	{ id: "", class: "role-checkbox", text: "Upload",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "599",check: "<?php if (isset($_GET['role_id'])) {echo in_array('599',$role_resources_ids) ? 'checked' : '';} ?>"}
	// ]},

	{ id: "", class: "role-checkbox", text: " Enrollees by Organization",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "600",check: "<?php if (isset($_GET['role_id'])) {echo in_array('600',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "All Hospital Profiles",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "601",check: "<?php if (isset($_GET['role_id'])) {echo in_array('601',$role_resources_ids) ? 'checked' : '';} ?>",items:[
		{ id: "", class: "role-checkbox", text: "View",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "602",check: "<?php if (isset($_GET['role_id'])) {echo in_array('602',$role_resources_ids) ? 'checked' : '';} ?>"},	
		{ id: "", class: "role-checkbox", text: "Upload",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "603",check: "<?php if (isset($_GET['role_id'])) {echo in_array('603',$role_resources_ids) ? 'checked' : '';} ?>"}
	]},

	{ id: "", class: "role-checkbox", text: " Organization Encounter",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "604",check: "<?php if (isset($_GET['role_id'])) {echo in_array('604',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: " Organizations Usage",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "605",check: "<?php if (isset($_GET['role_id'])) {echo in_array('605',$role_resources_ids) ? 'checked' : '';} ?>"},

	// { id: "", class: "role-checkbox", text: "Expired Debit Note Reports",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "606",check: "<?php if (isset($_GET['role_id'])) {echo in_array('606',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Encounter Reports",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "607",check: "<?php if (isset($_GET['role_id'])) {echo in_array('607',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Enrollees With Zero Usage",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "608",check: "<?php if (isset($_GET['role_id'])) {echo in_array('608',$role_resources_ids) ? 'checked' : '';} ?>"},

	{ id: "", class: "role-checkbox", text: "Enrollees With High Usage",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "609",check: "<?php if (isset($_GET['role_id'])) {echo in_array('609',$role_resources_ids) ? 'checked' : '';} ?>"},
	
	]},
	//Bussiness Reports

	//Capitation Report
	{ id: "", class: "role-checkbox", text: "NHIS Capitation",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "610",check: "<?php if (isset($_GET['role_id'])) {echo in_array('610',$role_resources_ids) ? 'checked' : '';} ?>", items:[
		{ id: "", class: "role-checkbox", text: " NHIS Data Creation",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "611",check: "<?php if (isset($_GET['role_id'])) {echo in_array('611',$role_resources_ids) ? 'checked' : '';} ?>",items:[
			{ id: "", class: "role-checkbox", text: "View",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "612",check: "<?php if (isset($_GET['role_id'])) {echo in_array('612',$role_resources_ids) ? 'checked' : '';} ?>"},
			{ id: "", class: "role-checkbox", text: "Upload",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "613",check: "<?php if (isset($_GET['role_id'])) {echo in_array('613',$role_resources_ids) ? 'checked' : '';} ?>"},
		]},

		{ id: "", class: "role-checkbox", text: "NHIS Reports by Providers",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "614",check: "<?php if (isset($_GET['role_id'])) {echo in_array('614',$role_resources_ids) ? 'checked' : '';} ?>"},

		{ id: "", class: "role-checkbox", text: "Full NHIS Reports",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "615",check: "<?php if (isset($_GET['role_id'])) {echo in_array('615',$role_resources_ids) ? 'checked' : '';} ?>"},

		{ id: "", class: "role-checkbox", text: " NHIS Encounter",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "616",check: "<?php if (isset($_GET['role_id'])) {echo in_array('616',$role_resources_ids) ? 'checked' : '';} ?>"},

		{ id: "", class: "role-checkbox", text: " NHIS Finance Reports",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "617",check: "<?php if (isset($_GET['role_id'])) {echo in_array('617',$role_resources_ids) ? 'checked' : '';} ?>"},
	]},
	//Capitation Report

	//Advance Settings
	{ id: "", class: "role-checkbox", text: "Advance Settings",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "618",check: "<?php if (isset($_GET['role_id'])) {echo in_array('618',$role_resources_ids) ? 'checked' : '';} ?>", items:[

		{ id: "", class: "role-checkbox", text: " HealthCare Plan Setup",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "619",check: "<?php if (isset($_GET['role_id'])) {echo in_array('619',$role_resources_ids) ? 'checked' : '';} ?>"},

		{ id: "", class: "role-checkbox", text: "Bands Setup",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "620",check: "<?php if (isset($_GET['role_id'])) {echo in_array('620',$role_resources_ids) ? 'checked' : '';} ?>"},

		{ id: "", class: "role-checkbox", text: "Locations Setup",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "621",check: "<?php if (isset($_GET['role_id'])) {echo in_array('621',$role_resources_ids) ? 'checked' : '';} ?>"},

		{ id: "", class: "role-checkbox", text: "Organization Profiles",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "622", check: "<?php if (isset($_GET['role_id'])) {echo in_array('622',$role_resources_ids) ? 'checked' : '';} ?>", items:[
			{ id: "", class: "role-checkbox", text: "View",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "623",check: "<?php if (isset($_GET['role_id'])) {echo in_array('623',$role_resources_ids) ? 'checked' : '';} ?>"},
			{ id: "", class: "role-checkbox", text: "Upload",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "624",check: "<?php if (isset($_GET['role_id'])) {echo in_array('624',$role_resources_ids) ? 'checked' : '';} ?>"},
		]},

		{ id: "", class: "role-checkbox", text: " Business Type Setup",  add_info: "<?php echo $this->lang->line('xin_view');?>", value: "625",check: "<?php if (isset($_GET['role_id'])) {echo in_array('625',$role_resources_ids) ? 'checked' : '';} ?>"},
	]},
	//Advance Settings

]

});

		

// show checked node IDs on datasource change

function onCheck() {

var checkedNodes = [],

treeView = jQuery("#treeview").data("kendoTreeView"),

message;

//checkedNodeIds(treeView.dataSource.view(), checkedNodes);

jQuery("#result").html(message);

}

$(document).ready(function(){

	$("#role_access_modal").change(function(){

		var sel_val = $(this).val();

		if(sel_val=='1') {

			$('.role-checkbox-modal').prop('checked', true);

		} else {

			$('.role-checkbox-modal').prop("checked", false);

		}

	});

});

</script>

<?php }

?>

