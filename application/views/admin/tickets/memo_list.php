<?php

/*

* Tickets view

*/

$session = $this->session->userdata('username');
// print_r($session);die;
?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_employee_info($session['user_id']);?>

<?php if (in_array('547',$role_resources_ids) || in_array('548',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
  

<div class="row">

  <div class="col-md-4 col-12">

    <div class="box box-body bg-hr-primary">

      <div class="flexbox"> <span class="fa fa-ticket text-primary font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo total_memos($session['user_id']);?></span> </div>

      <div class="text-right"><?php echo $this->lang->line('xin_hr_total_memos');?></div>

    </div>

  </div>

  <div class="col-md-4 col-12">

    <div class="box box-body bg-hr-danger">

      <div class="flexbox"> <span class="fa fa-server text-danger font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo total_open_memos($session['user_id']);?></span> </div>

      <div class="text-right"><?php echo $this->lang->line('xin_hr_total_open_memos');?></div>

    </div>

  </div>

  <div class="col-md-4 col-12">

    <div class="box box-body bg-hr-success">

      <div class="flexbox"> <span class="ion ion-thumbsup text-success font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo total_closed_memos($session['user_id']);?></span> </div>

      <div class="text-right"><?php echo $this->lang->line('xin_hr_total_closed_memos');?></div>

    </div>

  </div>

</div>

<?php if(in_array('306',$role_resources_ids)) {?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"><?php echo $this->lang->line('xin_create_new_memo');?></h3>

      <div class="box-tools pull-right"> <a class="text-dark collapsed" data-toggle="collapse" href="#add_form" aria-expanded="false">

        <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span> <?php echo $this->lang->line('xin_add_new');?></button>

        </a> </div>

    </div>

    <div id="add_form" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">

        <?php $attributes = array('name' => 'add_ticket', 'id' => 'xin-form', 'autocomplete' => 'off');?>

        <?php $hidden = array('user_id' => $session['user_id']);?>

        <?php echo form_open('admin/tickets/add_memo', $attributes, $hidden);?>

        <div class="bg-white">

          <div class="box-block">

            <div class="row">

              <div class="col-md-6">

                <div class="form-group">

                  <label for="task_name"><?php echo $this->lang->line('xin_subject');?></label>

                  <input class="form-control" placeholder="<?php echo $this->lang->line('xin_subject');?>" name="subject" type="text" value="">

                </div>

                <div class="row">

                  <?php $colmd = 'col-md-6';?>

                  <?php if(!in_array('384',$role_resources_ids)) { ?>

                  <?php $colmd = 'col-md-6';?>

                  <?php } else {?>

                  <?php $colmd = 'col-md-12'; ?>

                  <input type="hidden" name="employee_id" value="<?php echo $session['user_id'];?>" />

                  <?php } ?>

                  <div class="col-md-12">
                    
                  </div>

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="ticket_priority" class="control-label"><?php echo $this->lang->line('xin_p_category');?></label>

                      <select name="memo_category" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="<?php echo $this->lang->line('xin_select_category');?>">

                        <option value=""></option>

                        <?php foreach ($memo_types as $memo): ?>
                          <option value="<?php echo $memo->id ?>"><?php echo $memo->name ?></option>
                        <?php endforeach ?>

                      </select>

                    </div>

                  </div>

                  <div class="col-md-6">
                      
                      <div class="form-group">
                      
                        <label for="abc" class="control-label">Attachment</label>

                        <input type="file" name="attachment" placeholder="">
                      </div>

                    </div>

                </div>

              </div>

              <div class="col-md-6">

                <div class="form-group">

                  <label for="description"><?php echo $this->lang->line('xin_memo_description');?></label>

                  <textarea class="form-control textarea" placeholder="<?php echo $this->lang->line('xin_memo_description');?>" name="description" cols="30" rows="5" id="description"></textarea>

                </div>

              </div>

            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">

                      <label for="ticket_priority" class="control-label"><?php echo $this->lang->line('xin_p_employee');?></label>

                      <select name="to" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="<?php echo $this->lang->line('xin_select_employee');?>">

                        <option value=""></option>

                        <?php foreach ($all_other_employees as $employee): ?>
                          <option value="<?php echo $employee->user_id ?>"><?php echo $employee->first_name ?> <?php echo $employee->last_name ?></option>
                        <?php endforeach ?>

                      </select>

                    </div>                
              </div>
            </div>

            <?php $count_module_attributes = $this->Custom_fields_model->count_tickets_module_attributes();?>

            <?php if($count_module_attributes > 0):?>

            <div class="row">

              <?php $module_attributes = $this->Custom_fields_model->tickets_hrsale_module_attributes();?>

              <?php foreach($module_attributes as $mattribute):?>

              <?php if($mattribute->attribute_type == 'date'){?>

              <div class="col-md-4">

                <div class="form-group">

                  <label for="<?php echo $mattribute->attribute;?>"><?php echo $mattribute->attribute_label;?></label>

                  <input class="form-control date" placeholder="<?php echo $mattribute->attribute_label;?>" name="<?php echo $mattribute->attribute;?>" type="text">

                </div>

              </div>

              <?php } else if($mattribute->attribute_type == 'select'){?>

              <div class="col-md-4">

                <?php $iselc_val = $this->Custom_fields_model->get_attribute_selection_values($mattribute->custom_field_id);?>

                <div class="form-group">

                  <label for="<?php echo $mattribute->attribute;?>"><?php echo $mattribute->attribute_label;?></label>

                  <select class="form-control" name="<?php echo $mattribute->attribute;?>" data-plugin="select_hrm" data-placeholder="<?php echo $mattribute->attribute_label;?>">

                    <?php foreach($iselc_val as $selc_val) {?>

                    <option value="<?php echo $selc_val->attributes_select_value_id?>"><?php echo $selc_val->select_label?></option>

                    <?php } ?>

                  </select>

                </div>

              </div>

              <?php } else if($mattribute->attribute_type == 'multiselect'){?>

              <div class="col-md-4">

                <?php $imulti_selc_val = $this->Custom_fields_model->get_attribute_selection_values($mattribute->custom_field_id);?>

                <div class="form-group">

                  <label for="<?php echo $mattribute->attribute;?>"><?php echo $mattribute->attribute_label;?></label>

                  <select multiple="multiple" class="form-control" name="<?php echo $mattribute->attribute;?>[]" data-plugin="select_hrm" data-placeholder="<?php echo $mattribute->attribute_label;?>">

                    <?php foreach($imulti_selc_val as $multi_selc_val) {?>

                    <option value="<?php echo $multi_selc_val->attributes_select_value_id?>"><?php echo $multi_selc_val->select_label?></option>

                    <?php } ?>

                  </select>

                </div>

              </div>

              <?php } else if($mattribute->attribute_type == 'textarea'){?>

              <div class="col-md-8">

                <div class="form-group">

                  <label for="<?php echo $mattribute->attribute;?>"><?php echo $mattribute->attribute_label;?></label>

                  <input class="form-control" placeholder="<?php echo $mattribute->attribute_label;?>" name="<?php echo $mattribute->attribute;?>" type="text">

                </div>

              </div>

              <?php } else if($mattribute->attribute_type == 'fileupload'){?>

              <div class="col-md-4">

                <div class="form-group">

                  <label for="<?php echo $mattribute->attribute;?>"><?php echo $mattribute->attribute_label;?></label>

                  <input class="form-control-file" name="<?php echo $mattribute->attribute;?>" type="file">

                </div>

              </div>

              <?php } else { ?>

              <div class="col-md-4">

                <div class="form-group">

                  <label for="<?php echo $mattribute->attribute;?>"><?php echo $mattribute->attribute_label;?></label>

                  <input class="form-control" placeholder="<?php echo $mattribute->attribute_label;?>" name="<?php echo $mattribute->attribute;?>" type="text">

                </div>

              </div>

              <?php }	?>

              <?php endforeach;?>

            </div>

            <?php endif;?>

            <div class="form-actions box-footer">

              <button type="submit" class="btn btn-primary"> <i class="fa fa-check-square-o"></i> <?php echo $this->lang->line('xin_save');?> </button>

            </div>

          </div>

        </div>

        <?php echo form_close(); ?> </div>

    </div>

  </div>

</div>

<?php } ?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"> Filter Results By Selecting a Date Range </h3>

    </div>

    <div class="<?php echo $get_animate;?>" style="">

      <div class="box-body">

            <div class="row">
              <div class="col-md-3">
                <div class="form-group">  
                    <input type="text" class="form-control date" placeholder="Select a date" required="" name="from" id="date_invoice_from" style="border-radius: 1.5rem">
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">  
                    <input type="text" class="form-control date" placeholder="Select a date" required="" name="to" id="date_invoice_to" style="border-radius: 1.5rem">
                </div>
              </div>


              <div class="col-md-2">

                <div class="form-group">

                  <button type="submit" name="get_filter_result" id="submit_date" class="btn btn-primary save">Fetch Data</button>

                </div>

              </div>

            </div>

        </div>

    </div>

  </div>

</div>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table">

        <thead>

          <tr class="xin-bg-dark">

            <th><?php echo $this->lang->line('xin_action');?></th>

            <th><?php echo $this->lang->line('xin_memo_approval');?></th>

            <th><?php echo $this->lang->line('xin_subject');?></th>

            <th><?php echo $this->lang->line('xin_memo_category');?></th>

            <th><?php echo $this->lang->line('xin_memo_description');?></th>

            <th><?php echo $this->lang->line('xin_memo_file');?></th>

            <th><i class="fa fa-calendar"></i> <?php echo $this->lang->line('xin_e_details_date');?></th>

          </tr>

        </thead>

      </table>

    </div>

  </div>

</div>

<?php 
else:
  redirect('admin/dashboard');
endif ?>
