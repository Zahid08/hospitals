<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if(isset($_GET['jd']) && isset($_GET['ticket_id']) && $_GET['data']=='ticket'){
?>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo $this->lang->line('xin_close');?>"> <span aria-hidden="true">×</span> </button>
  <h4 class="modal-title" id="edit-modal-data">Edit Memo</h4>
</div>
    <?php $attributes = array('name' => 'add_ticket', 'id' => 'edit_ticket', 'autocomplete' => 'off', 'method'=>'post','enctype'=>"multipart/form-data");?>

        <?php echo form_open('admin/tickets/update_memo', $attributes);?>

        <?php 
          $ci =& get_instance(); 
          $ci->load->model('Tickets_model');
          $ci->load->model('Xin_model');
          $memo = $ci->Tickets_model->read_memo_information($_GET['ticket_id']);
          $memo_types = $ci->Xin_model->get_memo_type()->result();
        ?>

  <div class="modal-body">

            <div class="row">

              <div class="col-md-6">

                <div class="form-group">

                  <label for="task_name"><?php echo $this->lang->line('xin_subject');?></label>
                  <input type="text" name="id" value="<?php echo $memo[0]->id; ?>" hidden>
                  <input class="form-control" placeholder="<?php echo $this->lang->line('xin_subject');?>" name="subject" type="text" value="<?php echo $memo[0]->subject; ?>">

                </div>

                <div class="row">

                  <div class="col-md-6">

                    <div class="form-group">

                      <label for="ticket_priority" class="control-label"><?php echo $this->lang->line('xin_p_category');?></label>

                      <select name="memo_category" id="select2-demo-6" class="form-control" data-plugin="select_hrm" data-placeholder="<?php echo $this->lang->line('xin_select_category');?>">

                        <option value=""></option>

                        <?php foreach ($memo_types as $type): ?>
                          <option value="<?php echo $type->id ?>" <?php echo $memo[0]->category == $type->id ? "selected":""; ?>><?php echo $type->name ?></option>
                        <?php endforeach ?>

                      </select>

                    </div>

                  </div>

                  <div class="col-md-6">
                      
                      <div class="form-group">
                      
                        <label for="abc" class="control-label">Attachment</label>

                        <input type="file" name="attachment" placeholder="">
                        <?php if ($memo[0]->file != 'no_file') { ?>
                          <a target="_blank" href="<?php echo site_url('/uploads/memo/').$memo[0]->file ?>"><i class="fa fa-download"></i></a>
                        <?php } ?>
                      </div>

                    </div>

                </div>

              </div>

              <div class="col-md-6">

                <div class="form-group">

                  <label for="description"><?php echo $this->lang->line('xin_memo_description');?></label>

                  <textarea class="form-control textarea" placeholder="<?php echo $this->lang->line('xin_memo_description');?>" name="description" cols="30" rows="5" id="description"><?php echo $memo[0]->description; ?></textarea>

                </div>

              </div>

            </div>



          </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line('xin_close');?></button>
    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('xin_update');?></button>
  </div>
  <?php echo form_close(); ?>
<script type="text/javascript">
 $(document).ready(function(){
					
		//$('#description2').trumbowyg();
		jQuery("#ajx_company").change(function(){
			jQuery.get(base_url+"/get_employees/"+jQuery(this).val(), function(data, status){
				jQuery('#employee_ajx').html(data);
			});
		});
		
		$('[data-plugin="select_hrm"]').select2($(this).attr('data-options'));
		$('[data-plugin="select_hrm"]').select2({ width:'100%' });	 	

		/* Edit data */
		$("#edit_ticket").submit(function(e){
		console.log('submitted');
    var fd = new FormData(this);
		var obj = $(this), action = obj.attr('name');
		fd.append("is_ajax", 1);
		fd.append("edit_type", 'ticket');
		fd.append("form", action);
		e.preventDefault();
		$('.icon-spinner3').show();
		$('.save').prop('disabled', true);
		$.ajax({
			url: e.target.action,
			type: "POST",
			data:  fd,
			contentType: false,
			cache: false,
			processData:false,
			success: function(JSON)
			{
        console.log(JSON);
        console.log('success');
				if (JSON.error != '') {
					toastr.error(JSON.error);
					$('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
						$('.save').prop('disabled', false);
						$('.icon-spinner3').hide();
				} else {
					// On page load: datatable
					var xin_table = $('#xin_table').dataTable({
						"bDestroy": true,
						"ajax": {
							url : "<?php echo site_url("admin/tickets/memo_list") ?>",
							type : 'GET'
						},
						"fnDrawCallback": function(settings){
						$('[data-toggle="tooltip"]').tooltip();          
						}
					});
					xin_table.api().ajax.reload(function(){ 
						toastr.success(JSON.result);
					}, true);
					$('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
					$('.icon-spinner3').hide();
					$('.edit-modal-data').modal('toggle');
					$('.save').prop('disabled', false);
				}
			},
			error: function() 
			{
				toastr.error(JSON.error);
				$('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
				$('.icon-spinner3').hide();
				$('.save').prop('disabled', false);
			} 	        
	   });
	});
	});	
  </script>
<?php }
?>
