<?php

/*

* Tickets view

*/

$session = $this->session->userdata('username');

?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_employee_info($session['user_id']);?>

<?php if (in_array('547',$role_resources_ids) || in_array('553',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table3">

        <thead>

          <tr class="xin-bg-dark">

            <th width="25%"><?php echo $this->lang->line('xin_action');?></th>

            <th><?php echo $this->lang->line('xin_memo_approval');?></th>

            <th><?php echo $this->lang->line('xin_subject');?></th>

            <th><?php echo $this->lang->line('xin_memo_category');?></th>

            <th><?php echo $this->lang->line('xin_memo_description');?></th>

            <th><?php echo $this->lang->line('xin_memo_file');?></th>

            <th><i class="fa fa-calendar"></i> <?php echo $this->lang->line('xin_e_details_date');?></th>

          </tr>

        </thead>

        <tbody>
          <?php if (!empty($memos)): ?>
            <?php 
              foreach ($memos as $key => $value): 
              switch ($value->approval) {
                case '0':
                  $status = 'Untreated';
                  break;

                case '1':
                  $status = 'Action Needed';
                  break;
                
                case '2':
                  $status = 'Pending';
                  break;

                case '3':
                  $status = 'Rejected';
                  break;

                case '4':
                  $status = 'Treated';
                  break;
                default:
                  # code...
                  break;
              }

              $ci=& get_instance();
              $ci->load->model('Tickets_model');

              $category = $this->Tickets_model->get_memo_category($value->category)->result();

              $file = "<a target=\"_blank\" href=\"".site_url('/uploads/memo/').$value->file."\"><i class=\"fa fa-download\"></i></a>";
            ?>
              <tr>  
                <td>
                  <?php if ($value->approval == 1): ?>
                    <a href="memo_approved_treat?treat=4&memo_id=<?php echo $value->id ?>" class="btn btn-primary">Treat</a>
                    <a href="memo_approved_treat?treat=2&memo_id=<?php echo $value->id ?>" class="btn btn-warning">Pending</a>
                    <a href="memo_approved_treat?treat=3&memo_id=<?php echo $value->id ?>" class="btn btn-danger">Reject</a>
                    <?php elseif($value->approval == 2): ?>
                    <a href="#" class="btn btn-warning">Pending</a>
                    <?php elseif($value->approval == 3) : ?>
                    <a href="#" class="btn btn-danger">Rejected</a>
                    <?php elseif($value->approval == 4) : ?>
                    <a href="#" class="btn btn-primary">Treated</a>
                  <?php endif ?>
                </td>
                <td><?php echo $status ?></td>
                <td><?php echo $value->subject ?></td>
                <td><?php echo $category[0]->name ?></td>
                <td><?php echo $value->description ?></td>
                <td>
                  <?php 
                    if($file == 'no_file') {
                      echo "no file";
                    }else{
                      echo $file; 
                    } 
                  ?>      
                </td>
                <td><?php echo $value->created_at ?></td>
              </tr>
              <script>
                $(document).ready(function(){

                  $.ajax({

                    url : base_url+"/read_employee/"+<?php echo $value->id; ?>,

                    type: "GET",

                    success: function (response) {

                      if(response) {

                        $("#send_to<?php echo $value->id ?>").html(response);

                      }

                    }

                  });

                });
              </script>
            <?php endforeach ?>
          <?php endif ?>
        </tbody>

      </table>

    </div>

  </div>

</div>

<?php endif; ?>