<?php

/*

* Tickets view

*/

$session = $this->session->userdata('username');

?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_employee_info($session['user_id']);?>

<?php if (in_array('547',$role_resources_ids) || in_array('549',$role_resources_ids) || in_array('550',$role_resources_ids) || in_array('551',$role_resources_ids) || in_array('552',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
  

<div class="box <?php echo $get_animate;?>">


  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table2">

        <thead>

          <tr class="xin-bg-dark">
            
            <?php if (in_array('547',$role_resources_ids) || in_array('549',$role_resources_ids)  || in_array('551',$role_resources_ids) || in_array('552',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
            <th width="20%"><?php echo $this->lang->line('xin_action');?></th>
              
            <?php endif ?>

            <th><?php echo $this->lang->line('xin_memo_approval');?></th>

            <th><?php echo $this->lang->line('xin_subject');?></th>

            <th><?php echo $this->lang->line('xin_memo_category');?></th>

            <th><?php echo $this->lang->line('xin_memo_description');?></th>

            <th><?php echo $this->lang->line('xin_memo_file');?></th>

            <th>Sent To</th>

            <th><i class="fa fa-calendar"></i> <?php echo $this->lang->line('xin_e_details_date');?></th>

          </tr>

        </thead>

        <tbody>
          <?php if (!empty($memos)): ?>
            <?php 
              foreach ($memos as $key => $value): 
              switch ($value->approval) {
                case '0':
                  $status = 'Untreated';
                  break;

                case '1':
                  $status = 'Action Needed';
                  break;
                
                case '2':
                  $status = 'Pending';
                  break;

                case '3':
                  $status = 'Rejected';
                  break;

                case '4':
                  $status = 'Treated';
                  break;
                default:
                  # code...
                  break;
              }

              $ci=& get_instance();
              $ci->load->model('Tickets_model');

              $category = $this->Tickets_model->get_memo_category($value->category);

              if (!is_null($category)) {
                $category = $category->result();
                $category = $category[0]->name;
              }else{
                $category = '-';
              }

              $file = "<a target=\"_blank\" href=\"".site_url('/uploads/memo/').$value->file."\"><i class=\"fa fa-download\"></i></a>";
            ?>
              <tr>
                <td>
                  <?php
                    $send_to = explode(",", $value->approved_by);
                    if (in_array($session['user_id'], $send_to)): 
                  ?>
                    Approved
                    <?php elseif($value->approval == '3'): ?>
                    Rejected
                  <?php else: ?>
                  <form action="next_approval" method="post">
                    <input type="hidden" name="memo_id" value="<?php echo $value->id; ?>">
                    <input type="hidden" name="id" value="<?php echo $session['user_id']; ?>">
                    <?php if (in_array('547',$role_resources_ids) || in_array('549',$role_resources_ids)  || in_array('551',$role_resources_ids) || in_array('552',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
              
                    <?php if (count($send_to)< 3): ?>
                    <select name="send_to" id="send_to<?php echo $value->id; ?>" class="form-control mb-3" data-plugin="select_hrm">
                        <option value=""></option>
                      </select>
                    
                    <button class="btn btn-primary" type="submit" name="approve">Approve</button>
                    <?php else: ?>
                    <button class="btn btn-primary" type="submit" name="f_approve">Final Approve</button>
                    <?php endif ?>
                    <button class="btn btn-warning" type="submit" name="reject">Reject</button>
                  <?php endif ?>
                <?php endif ?>
                </td>
                  </form>
                <td><?php echo $status ?></td>
                <td><?php echo $value->subject ?></td>
                <td><?php echo $category ?></td>
                <td><?php echo $value->description ?></td>
                <td>
                  <?php 
                    if($file == 'no_file') {
                      echo "no file";
                    }else{
                      echo $file; 
                    } 
                  ?>      
                </td>
                <td><?php  
                  if (!is_null($value->rejected_by)) {
                    echo "-";
                  }elseif ($value->approval != 0){
                    $send_to = explode(",", $value->send_to);
                    $id = $session['user_id'];

                    $check = array_search($id, $send_to);

                    if(!is_null($check)) {
                      $index = $check+1;
                    }else{
                      $index = 0;
                    }

                    $CI =& get_instance();
                    $CI->load->model('Xin_model');

                    $employee = $CI->Xin_model->read_employee_info($send_to[$index]);
                    // echo $send_to[$index];
                    echo $employee[0]->first_name." ".$employee[0]->last_name;

                  }else{
                    echo "-";
                  }
                ?></td>
                <td><?php echo $value->created_at ?></td>
              </tr>
              <script>
                $(document).ready(function(){

                  $.ajax({

                    url : base_url+"/read_employee/"+<?php echo $value->id; ?>,

                    type: "GET",

                    success: function (response) {

                      if(response) {

                        $("#send_to<?php echo $value->id ?>").html(response);

                      }

                    }

                  });

                  <?php if (isset($_SESSION['result'])) {
                  ?>

                    var result = '<?php $_SESSION['result'] ?>';

                    if(result == 'fail'){
                      toastr.error("Fail, try again.");
                    }else if(result == 'success'){
                      toastr.success("Success.");
                    }

                  <?php
                  } ?>

                });
              </script>
            <?php endforeach ?>
          <?php endif ?>
        </tbody>

      </table>

    </div>

  </div>

</div>
<?php
  else:
    redirect('admin/dashboard','refresh');
 endif; 
 ?>