<?php

/*

* Tickets view

*/

$session = $this->session->userdata('username');
// print_r($session);die;
?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_employee_info($session['user_id']);?>

<?php if (in_array('547',$role_resources_ids) || in_array('626',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"> Filter Results By Selecting a Status or Date Range  </h3>

    </div>

    <div class="<?php echo $get_animate;?>" style="">

      <div class="box-body">

            <div class="row">
              <div class="col-md-3">
                  <select name="category_name" id="select2-demo-6" class="form-control cat" data-plugin="select_hrm" data-placeholder="Choose Status" required>
                    <option value=""></option>
                    <option value="0">Untreated</option>
                    <option value="1">Action Needed</option>
                    <option value="2">Pending</option>
                    <option value="3">Rejected</option>
                    <option value="4">Treated</option>

                  </select>
              </div>
              <div class="col-md-3">
                <div class="form-group">  
                    <input type="text" class="form-control date" placeholder="Select a date" required="" name="from" id="date_invoice_from" style="border-radius: 1.5rem">
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">  
                    <input type="text" class="form-control date" placeholder="Select a date" required="" name="to" id="date_invoice_to" style="border-radius: 1.5rem">
                </div>
              </div>


              <div class="col-md-2">

                <div class="form-group">

                  <button type="submit" name="get_filter_result" id="submit_date" class="btn btn-primary save">Fetch Data</button>

                </div>

              </div>

            </div>

        </div>

    </div>

  </div>

</div>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> <?php echo $this->lang->line('xin_list_all');?> <?php echo $this->lang->line('left_memos');?> </h3>

  </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table">

        <thead>

          <tr class="xin-bg-dark">

            <th><?php echo $this->lang->line('xin_memo_approval');?></th>

            <th><?php echo $this->lang->line('xin_subject');?></th>

            <th><?php echo $this->lang->line('xin_memo_category');?></th>

            <th><?php echo $this->lang->line('xin_memo_description');?></th>

            <th><?php echo $this->lang->line('xin_memo_file');?></th>

            <th><i class="fa fa-calendar"></i> <?php echo $this->lang->line('xin_e_details_date');?></th>

          </tr>

        </thead>

      </table>

    </div>

  </div>

</div>

<?php 
else:
  redirect('admin/dashboard');
endif ?>
