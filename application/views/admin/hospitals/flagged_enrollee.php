<?php

/* Subscription view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource();?>

<?php if(in_array('705',$role_resources_ids)) {?>

    <?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>
    <?php

    $role_user = $this->Xin_model->read_user_role_info($user_info[0]->user_role_id);

    if(!is_null($role_user)){

        $role_resources_ids = explode(',',$role_user[0]->role_resources);

    } else {

        $role_resources_ids = explode(',',0);

    }

    ?>

    <style type="text/css">
        .fa{
            cursor: pointer;
        }
    </style>
    <div class="box mb-4 <?php echo $get_animate;?>">

        <div class="box-header with-border">


            <h3 class="box-title"> Filter Data by Date <?php //echo $query; ?> </h3>

        </div>

        <div class="box-body">

            <?php echo form_open('admin/Hospital/flag_enrollee');?>

            <div class="row">

                <div class="col-md-3">

                    <div class="form-group">

                        <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

                    </div>

                </div>

                <div class="col-md-3">

                    <div class="form-group">

                        <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">

                    </div>

                </div>

                <div class="col-md-2">

                    <div class="form-group">

                        <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>
                    </div>

                </div>

            </div>

            <?php echo form_close(); ?> </div>
    </div>

<?php } ?>

<div class="box <?php echo $get_animate;?>">

    <div class="box-header with-border">
    </div>

    <div class="box-body">

        <?php if ($this->session->flashdata('success')): ?>

            <div class="alert alert-success alert-dismissible " role="alert">
                <?php echo $this->session->flashdata('success'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif ?>

        <div class="box-datatable table-responsive">

            <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

                <thead>

                <tr>
		    <th width="4%"><?php echo $this->lang->line('xin_action');?></th>
                    <th>Enrollee Name</th>
                    <th width="25%">Name of Provider</th>
                    <th width="15%">Encounter(s)</th>
                    <th width="14%">First Encounter</th>
                    <th width="14%">Last Encounter</th>
                    <th width="5%">Bills</th>
                </tr>

                <tbody>
                <?php  if(!empty($flags)) {
                    $ci=& get_instance();
                    $ci->load->model('Clients_model');
                    foreach($flags  as $key => $value) {
                        $data= $ci->Training_model->getClientInfo($value->diagnose_client_id,$value->diagnose_hospital_id);
                        if(empty($data['name'])){
                            continue;
                        }
                        ?>
                        <tr>
                         <td>
		         <p class="btn btn-danger">Flagged</p>
                            </td>
                            <td>
                                <?php   echo $data['name']; ?>
                            </td>
                            <td>
                                <?php   echo $data['hospital']; ?>
                            </td>
                            <td>
                               <b> <?php  echo $data['total'];  ?> </b>Encounter(s)
                            </td>
                            <td>
                                <?php  echo $data['start'];  ?>
                            </td>
                            <td>
                                <?php  echo $data['last'];  ?>
                            </td>
                            <td>
                                <?php  echo "₦".$data['amount'];  ?>
                            </td>
                        </tr>

                    <?php }
                }  ?>
                </tbody>

                </thead>

            </table>

        </div>

    </div>

</div>



<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){
        var xin_table_new = $('#xin_table_new').dataTable({
            dom: 'lBfrtip',
            buttons: ['csv', 'excel', 'pdf', 'print'],
            "order": [[ 5, "desc" ]]
        });
    }, false);


</script>







