<?php

/* Subscription view

*/
 
?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php if(in_array('341',$role_resources_ids) || in_array('502',$role_resources_ids) || in_array('534',$role_resources_ids) || in_array('539',$role_resources_ids) || in_array('540',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>


<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

   
</div>

<?php } ?>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> Dependant Change Requests  </h3>

  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>

    <div class="box-datatable table-responsive">

        <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

            <thead>

                <tr> 
                    <?php if (in_array('502',$role_resources_ids) || in_array('534',$role_resources_ids) || in_array('539',$role_resources_ids) || in_array('541',$role_resources_ids) || in_array('542',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
                    <th><?php echo $this->lang->line('xin_action');?></th>
                        
                    <?php endif ?>

                    <th> Name</th>
                    <th> Last Name</th>
                    <th> Other Name</th>
                    <th> DOB</th>
                    <th> Phone</th>
                    <th> Gender</th>
                    <th> Relation</th>                  
                    <th> Provider</th>  
                </tr>

                <tbody> 
                    <?php   
                    if(!empty($all_requests))
                    { 
                        foreach ($all_requests as $key => $value)
                        {              
                             
                         
                            $ci=& get_instance();
                            $ci->load->model('Training_model'); 

                            // $client_data    =  $ci->Training_model->getAll2('xin_clients', ' client_id='. $value->client_id.' ');

                            // if (isset($client_data[0]->hospital_id) and !empty($client_data[0]->hospital_id)) 
                            // {
                            //     $current_hospi  =  $ci->Training_model->getAll2('xin_hospital', ' hospital_id='. $client_data[0]->hospital_id.' ');
                            // }

                            


                            $hospital   =  $ci->Training_model->getAll2('xin_hospital', ' hospital_id='. $value->hospital_id.' ');
                            
                                
                            ?>  
                            <tr>
                                <?php if (in_array('534',$role_resources_ids) || in_array('539',$role_resources_ids) || in_array('541',$role_resources_ids) || in_array('542',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
                        
                                <td>
                                    <?php if ($value->status == 'approved'){ ?>
                                        <p class="btn btn-success"> Approved</p> 

                                     <?php }elseif($value->status == 'rejected') { ?>
                                        <p class="btn btn-danger"> Rejected</p> 
                                    <?php }else { ?>
                                        <?php if (in_array('534',$role_resources_ids) || in_array('539',$role_resources_ids) || in_array('541',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
                                        <a href="<?php echo base_url(); ?>admin/Hospital/change_dependent_requests?clients_family_id=<?php echo $value->clients_family_id; ?>&requester_id=<?php echo $value->requester_id; ?>&approve=yes"  class="btn btn-info">Approve</a>
                        
                                    <?php endif ?>
                                    <?php if (in_array('502',$role_resources_ids) || in_array('534',$role_resources_ids) || in_array('539',$role_resources_ids) || in_array('542',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
                                        <a href="<?php echo base_url(); ?>admin/Hospital/change_dependent_requests?reject=yes&clients_family_id=<?php echo $value->clients_family_id; ?>" class="btn btn-warning">Reject</a>
                        
                                    <?php endif ?>


                                    <?php } ?>
                                </td>
                                <?php endif ?>

 

                                <td><?php echo isset($value->name) ? $value->name : ''; ?> </td>
                                <td><?php echo isset($value->last_name) ? $value->last_name : ''; ?> </td>
                                <td><?php echo isset($value->other_name) ? $value->other_name : ''; ?> </td>
                                <td><?php echo isset($value->dob) ? $value->dob : ''; ?> </td>

                                <td><?php echo isset($value->contact_number) ? $value->contact_number : ''; ?> </td>
                                <td><?php echo isset($value->sex) ? $value->sex : ''; ?> </td>
                                <td><?php echo isset($value->relation) ? $value->relation : ''; ?> </td>
                                 

                                <td><?php echo isset($hospital[0]->hospital_name) ? $hospital[0]->hospital_name : ''; ?> </td>

                              
                               
                            </tr> 

                            <?php 
                        }
                    }
                    ?>
                </tbody>

            </thead>

        </table>

    </div>

  </div>

</div>
 

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
          buttons: ['csv', 'excel', 'pdf', 'print']
        }); 
    }, false);

     
</script>
 






 