<?php

/* Subscription view

*/
 
?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php if(in_array('341',$role_resources_ids)) {?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>


<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

   
</div>

<?php } ?>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>

    <div class="box-datatable table-responsive">

        <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

            <thead>

                <tr> 
                    <th width="20%"><?php echo $this->lang->line('xin_action');?></th>

                    <th>New Provider</th>
                    <th>Enrollee</th>
                    <th>Phone Number</th> 
                    <th>Address</th>  
                    <th>Current Provider</th>
                    <th width="15%">Date</th>  
                </tr>

                <tbody> 
                    <?php   
                    if(!empty($all_requests))
                    { 
                        // print_r($all_requests);die;
                        foreach ($all_requests as $key => $value)
                        {              
                             
                         
                            $ci=& get_instance();
                            $ci->load->model('Training_model'); 

                            $client_data    =  $ci->Training_model->getAll2('xin_clients', ' client_id='. $value->client_id.' ');

                            if (isset($client_data[0]->hospital_id) and !empty($client_data[0]->hospital_id)) 
                            {
                                $current_hospi  =  $ci->Training_model->getAll2('xin_hospital', ' hospital_id='. $client_data[0]->hospital_id.' ');
                            }

                            


                            $change_hospi   =  $ci->Training_model->getAll2('xin_hospital', ' hospital_id='. $value->hospital_id.' ');
                            
                                
                            ?>  
                            <tr>
                                <td>
                                    <?php if ($value->status == 'approved'){ ?>
                                        <p class="btn btn-success"> Approved</p> 

                                     <?php }elseif($value->status == 'rejected') { ?>
                                        <p class="btn btn-danger"> Rejected</p> 
                                    <?php }else { ?>
                                        <a href="<?php echo base_url(); ?>admin/Hospital/change_hospital_requests?client_id=<?php echo $value->client_id; ?>&hospital_id=<?php echo $value->hospital_id; ?>&approve=yes&id=<?php echo $value->change_hospital_request_id; ?>"  class="btn btn-info">Approve</a>

                                        <a href="<?php echo base_url(); ?>admin/Hospital/change_hospital_requests?reject=yes&id=<?php echo $value->change_hospital_request_id; ?>" class="btn btn-warning">Reject</a>

                                    <?php } ?>
                                </td>


                                <td><?php echo isset($change_hospi[0]->hospital_name) ? $change_hospi[0]->hospital_name : ''; ?> </td>


                                <td><?php echo isset($client_data[0]->name) ? $client_data[0]->name : ''; ?> </td>

                                <td><?php echo isset($client_data[0]->contact_number) ? $client_data[0]->contact_number : ''; ?> </td>

                                <td><?php echo isset($client_data[0]->address_1) ? $client_data[0]->address_1 : ''; ?> </td>

                                <td><?php echo isset($current_hospi[0]->hospital_name) ? $current_hospi[0]->hospital_name : ''; ?> </td>
                                
                                <td><?php echo $value->created_on; ?></td>
                                 
                               
                            </tr> 

                            <?php 
                        }
                    }
                    ?>
                </tbody>

            </thead>

        </table>

    </div>

  </div>

</div>
 

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
          buttons: ['csv', 'excel', 'pdf', 'print'],
          "order": [[6,"dsc"]]
        }); 
    }, false);

     
</script>
 






 