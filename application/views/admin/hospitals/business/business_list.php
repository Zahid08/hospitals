<?php

/* Subscription view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('618',$role_resources_ids) || in_array('625',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>

<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

        <h3 class="box-title">Add New Business Types </h3>

        <div class="box-tools pull-right"> 
            <a class="text-dark collapsed" data-toggle="collapse" href="#add_form" aria-expanded="false">

                <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span> Add New Business Types </button>

            </a> 
        </div>

    </div>

    <div id="add_form" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">

        <?php $attributes = array('name' => 'add_subscription', 'id' => 'xin-form2', 'autocomplete' => 'off');?>

        <?php $hidden = array('_user' => $session['user_id']);?>

        <?php echo form_open('admin/Hospital/add_business_type', $attributes, $hidden);?>

        <div class="bg-white">

            <div class="box-block">

                <div class="row"> 
                    <div class="col-md-6"> 
                        <div class="row"> 
                            <div class="col-md-12">  
                                <div class="form-group"> 
                                    <label for="company_name">Business Type</label> 
                                    <input class="form-control" required="" placeholder="Business Type" name="business_name" type="text" value=""> 
                                </div>  
                            </div> 
                        </div>   
                    </div> 

                   <!--  <div class="col-md-6"> 
                        <div class="form-group"> 
                            <label for="training_type">Location</label>

                            <select class="form-control" name="training_type" data-plugin="select_hrm" data-placeholder="Location"> 
                                <option value=""></option> 
                            </select> 
                        </div> 
                    </div>  -->
                </div>


               
<!-- 
                <div class="row"> 
                    <div class="col-md-6"> 
                        <div class="form-group"> 
                            <label for="training_type">locations Type</label>

                            <select class="form-control" name="training_type" data-plugin="select_hrm" data-placeholder="Band Type"> 
                                <option value=""></option> 
                            </select> 
                        </div> 
                    </div>  
                    <div class="col-md-6"> 
                        <div class="row"> 
                            <div class="col-md-12">  
                                <div class="form-group"> 
                                    <label for="company_name">Tarrif </label> 
                                    <input class="form-control" required="" placeholder="Tarrif " name="plan_name" type="text" value=""> 
                                </div>  
                            </div> 
                        </div>   
                    </div> 
                     

                </div>
 -->
                
                
                <div class="form-actions box-footer"> 
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-check-square-o"></i> <?php echo $this->lang->line('xin_save');?> </button> 
                </div>

            </div>

        </div>

        <?php echo form_close(); ?> </div>

    </div>

  </div>

</div>



<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">
  </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

      	<table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

        	<thead>

          		<tr>

		            <th><?php echo $this->lang->line('xin_action');?></th>

		            <th>Business Type</th>

		            <th>Created On</th>
 
             
          		</tr>
          	</thead>

          	<tbody>      
          		<?php 
                    if(!empty($all_business))
                    { 
                        foreach ($all_business as $key => $value)
                        {           
                 			?>
		                    <tr>
		                        <td>
		                        	<button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light edit_detect" data-toggle="modal" data-target=".edit-modal-data2" data-ticket_id="<?php echo isset($value->business_id) ? $value->business_id : ''; ?>" 
                                        data-business_name="<?php echo isset($value->business_name) ? $value->business_name : ''; ?>" ><span class="fa fa-pencil"></span></button>

                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                        <button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal2" data-record-id="<?php echo isset($value->business_id) ? $value->business_id : ''; ?>"><span class="fa fa-trash"></span></button>
                                    </span>   
		                        </td>
		                        <td><?php echo isset($value->business_name) ? $value->business_name : ''; ?> </td>
		                        <td><?php echo isset( $value->created_on) ?  strftime('%d-%m-%Y',strtotime($value->created_on)) : ''; ?></td>  
		                    </tr> 

                			<?php   
                		}
                	}

                ?>
            </tbody> 
      	</table>

    </div>

  </div>

</div>
<?php }else{
    redirect('admin/dashboard','refresh');
} ?>
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable();


        $("#xin-form2").submit(function(e){
			var fd = new FormData(this);
			var obj = $(this), action = obj.attr('name');
			fd.append("is_ajax", 1);
			fd.append("add_type", 'training');
			fd.append("form", action);
			e.preventDefault();
			$('.icon-spinner3').show();
			$('.save').prop('disabled', true);
			$.ajax({
				url: e.target.action,
				type: "POST",
				data:  fd,
				contentType: false,
				cache: false,
				processData:false,
				success: function(JSON)
				{
					if (JSON.error != '') {
						toastr.error(JSON.error);
						$('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
							$('.save').prop('disabled', false);
							$('.icon-spinner3').hide();
					} else {
						toastr.success(JSON.result);
						setTimeout(function(){
						   window.location.reload(1);
						}, 5000);
						$('.icon-spinner3').hide();
						$('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
						$('#xin-form2')[0].reset(); // To reset form fields
						$('.add-form').removeClass('in');
						$('.select2-selection__rendered').html('--Select--');
						$('.save').prop('disabled', false);
					}
				},
				error: function() 
				{
					toastr.error(JSON.error);
					$('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
					$('.icon-spinner3').hide();
					$('.save').prop('disabled', false);
				} 	        
		   });
		});


        $("#delete_record2").submit(function(e){
            /*Form Submit*/
            e.preventDefault();
            var obj = $(this), action = obj.attr('name');
            $.ajax({
                type: "POST",
                url: e.target.action,
                data: obj.serialize()+"&is_ajax=2&form="+action,
                cache: false,
                success: function (JSON) {
                    if (JSON.error != '') {
                        toastr.error(JSON.error);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                    } else {
                        $('.delete-modal2').modal('toggle');
                         
                        toastr.success(JSON.result);
                        setTimeout(function(){
                           window.location.reload(1);
                        }, 5000);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);                 
                    }
                }
            });
        });









             /* Edit data */
        $("#edit_ticket2").submit(function(e){
            var fd = new FormData(this);
            var obj = $(this), action = obj.attr('name');
            fd.append("is_ajax", 1);
            fd.append("edit_type", 'ticket');
            fd.append("form", action);
            e.preventDefault();
            $('.icon-spinner3').show();
            $('.save').prop('disabled', true);
            $.ajax({
                url: e.target.action,
                type: "POST",
                data:  fd,
                contentType: false,
                cache: false,
                processData:false,
                success: function(JSON)
                {
                    if (JSON.error != '') {
                        toastr.error(JSON.error);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                            $('.save').prop('disabled', false);
                            $('.icon-spinner3').hide();
                    } else {
                        // On page load: datatable
                        // var xin_table = $('#xin_table').dataTable({
                        //     "bDestroy": true,
                        //     "ajax": {
                        //         url : "https://hmo.liontech.com.ng/admin/tickets/ticket_list",
                        //         type : 'GET'
                        //     },
                        //     "fnDrawCallback": function(settings){
                        //     $('[data-toggle="tooltip"]').tooltip();          
                        //     }
                        // });
                        
                            toastr.success(JSON.result);

                            setTimeout(function(){
                               window.location.reload(1);
                            }, 4000);
                        
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                        $('.icon-spinner3').hide();
                        $('.edit-modal-data2').modal('toggle');
                        $('.save').prop('disabled', false);
                    }
                },
                error: function() 
                {
                    toastr.error(JSON.error);
                    $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                    $('.icon-spinner3').hide();
                    $('.save').prop('disabled', false);
                }           
            });
        });
        

        $('.edit_detect').on('click',function(){
            var business_name  = $(this).attr('data-business_name');
            var id             = $(this).attr('data-ticket_id');
            $('#business_name').val(business_name);
            $('#business_id').val(id);
        })
 
 
    }, false);

     
</script>

<div class="modal fade delete-modal2 animated in" role="dialog" aria-hidden="true"  >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"> 
                <button type="button" aria-label="Close" data-dismiss="modal" class="close"><span aria-hidden="true">×</span></button>
                <strong class="modal-title">Are you sure you want to delete this record?</strong> 
            </div>
            <div class="alert alert-danger">
                <strong>Record deleted can't be restored!!!</strong>
            </div>
            <form action="https://hmo.liontech.com.ng/admin/Hospital/delete_business" name="delete_record" id="delete_record2" autocomplete="off" role="form" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="">
                <input type="hidden" name="csrf_hrsale" value="288e255c59f6126650e28264e712db77">                                                                                                                         
 
                <div class="modal-footer">
        
                    <input type="hidden" name="token_type" value="0" id="token_type">
            
                    <button type="button" data-dismiss="modal" class="btn btn-secondary"><i class="fa fa fa-check-square-o"></i> Close</button>
     
                    <button name="hrsale_form" type="submit" class="btn btn-primary"><i class="fa fa fa-check-square-o"></i> Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- edit -->

<div class="modal fade edit-modal-data2 animated in" id="edit-modal-data" role="dialog" aria-labelledby="edit-modal-data" aria-hidden="true"  >
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="ajax_modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                <h4 class="modal-title" id="edit-modal-data">Edit Business Type</h4>
            </div>

            <form action="<?php echo base_url(); ?>admin/Hospital/update_business" name="edit_ticket" id="edit_ticket2" autocomplete="off" class="m-b-1" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="EDIT">
                <input type="hidden" name="_token" value="">
                <input type="hidden" name="business_id" id="business_id" value="">
                <input type="hidden" name="ext_name" value="1">
                <input type="hidden" name="csrf_hrsale" value="95edc3390e40ab2fc8a739f96b5ee8aa">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group"> 
                                <label for="company_name">Business Type</label> 
                                <input class="form-control" required="" placeholder="Business Type" name="business_name" id="business_name" type="text" value=""> 
                            </div>  
                        </div> 
                    </div>
                    <div class="row">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form> 
        </div>
    </div>
</div>