<?php

/* Gospital Details view

*/

$ncolmd = 4;
$colmd = 4;

?>

<?php $session = $this->session->userdata('username');?>

<?php $system = $this->Xin_model->read_setting_info(1);?>

<?php //$default_currency = $this->Xin_model->read_currency_con_info($system[0]->default_currency_id);?>

<?php

// $eid = $this->uri->segment(4);

// $eresult = $this->Employees_model->read_employee_information($eid);

?>

<?php

$ar_sc = explode('- ',$system[0]->default_currency_symbol);

$sc_show = $ar_sc[1];

?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>  
 
<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>


<div class="row">

    <div class="col-md-12">

        <div class="nav-tabs-custom mb-4">

            <ul class="nav nav-tabs">

                <li class="nav-item active"> <a class="nav-link active show" data-toggle="tab" href="#user_basic_info">Profile Information</a> </li>

                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#xin_profile_picture"><?php echo $all_hospital_detail[0]->hospital_name ?> Drugs Tarrif</a> </li> 


                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#xin_services"><?php echo $all_hospital_detail[0]->hospital_name ?> Services Tarrif</a> </li> 

            </ul>

           

            <div class="tab-content">
                

                <div class="tab-pane <?php echo $get_animate;?> active" id="xin_general">

                <div class="card-body">

                    <div class="card overflow-hidden">

                        <div class="row no-gutters row-bordered row-border-light"> 
                            <div class="col-md-12">

                                <div class="tab-content">

                                    <div class="tab-pane active current-tab  " id="user_basic_info">

                                        <div class="box-header with-border">

                                            <h3 class="box-title"> <?php echo $this->lang->line('xin_e_details_basic_info');?> </h3>

                                        </div>

                                        <div class="box-body">

                                            
                                            <div class="bg-white">

                                                <div class="row">
                                                    <?php if ($this->session->flashdata('success')): ?>
                                                        <div class="alert alert-success  alert-dismissible">
                                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                            <?php echo $this->session->flashdata('success'); ?>
                                                        </div>
                                                    <?php endif ?>

                                                     <?php if ($this->session->flashdata('error')): ?>
                                                        <div class="alert alert-warning  alert-dismissible">
                                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                            <?php echo $this->session->flashdata('error'); ?>
                                                        </div>
                                                    <?php endif ?>

                                                    <?php 
                                                        $ci=& get_instance();
                                                        $ci->load->model('Training_model'); 

                                                        $bands    = $ci->Training_model->getAll2('xin_bands', ' band_id='. $all_hospital_detail[0]->band_id.' ');

                                                        $location = $ci->Training_model->getAll2('xin_location', ' location_id='. $all_hospital_detail[0]->location_id.' ');

                                                    ?>
                                                    <div class="col-md-4">

                                                        <div class="form-group">

                                                            <label for="first_name">Name</label>
                                                            <?php echo $all_hospital_detail[0]->hospital_name ?>
                                                            

                                                        </div>

                                                    </div>

                                                    <div class="col-md-4">

                                                        <div class="form-group">

                                                            <label for="last_name" class="control-label">Location</label>

                                                            <?php echo isset($location[0]->location_name) ? $location[0]->location_name : ''; ?>
                                                        </div>

                                                    </div>

                                                    <div class="col-md-4">

                                                        <div class="form-group">

                                                            <label for="employee_id">Band Type</label>

                                                            <?php echo isset($bands[0]->band_name) ? $bands[0]->band_name : ''; ?>

                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="row">

                                                    

                                                    <div class="col-md-4">

                                                        <div class="form-group">

                                                            <label for="username">Email</label>

                                                            <?php echo $all_hospital_detail[0]->email ?>
                                                        </div>

                                                    </div>

                                                    <div class="col-md-4">

                                                        <div class="form-group">

                                                            <label for="email" class="control-label">Phone</label> 
                                                            <?php echo $all_hospital_detail[0]->phone ?>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="row"> 
                                                    <div class="col-md-4">

                                                        <div class="form-group">

                                                            <?php if (isset($all_hospital_detail[0]->logo_img) and !empty($all_hospital_detail[0]->logo_img)): ?>
                                                                <?php
                                                                    echo '<img    src=" '.base_url().'uploads/hospital/logo/'.$all_hospital_detail[0]->logo_img.' ">';
                                                                ?>
                                                            <?php endif ?> 

                                                        </div>

                                                    </div> 
                                                </div> 

                                            </div> 
                                        </div> 
                                    </div>

                                    <div class="tab-pane" id="xin_profile_picture"  >

                                        <div class="box-body">

                                            <div class="row no-gutters row-bordered row-border-light">

                                            <div class="col-md-12">

                                            <div class="tab-content">

                                                <div class="tab-pane   active" id="profile-picture">

                                                <div class="box-body pb-2">

                                                   
                                                    <div class="bg-white">

                                                        <div class="row">

                                                            <div class="">
                                                                 
                                                                  <h3 class="box-title">Import CSV file only</h3>
                                                                
                                                                
                                                                    <p class="card-text">The first line in downloaded csv file should remain as it is. Please do not change the order of columns in csv file.</p>
                                                                    <p class="card-text">The correct column order is (Drug Name, Drug Price) and you must follow the csv file, otherwise you will get an error while importing the csv file.</p>
                                                                    <h6><a href="<?php echo base_url(); ?>/uploads/csv/sample-csv-drugs.csv" class="btn btn-primary"> <i class="fa fa-download"></i> Download sample File </a></h6>

                                                                <?php echo form_open_multipart('admin/Hospital/add_drugs');?>
                                                                <button type="submit"  class="btn btn-success float-right"> Save Drug</button>
                                                                   <input name="price" placeholder="Drug Price" min="1" style="margin-bottom: 5px;width: 30%;float: right;" class="form-control" type="number" title="Fill Drug price field" required>
                                                                    <input name="title" style="margin-bottom: 5px;width: 30%;float: right;" class="form-control" placeholder="Drugs Name" title="Fill Drug title field" type="text" required>
                                                                    <input type="hidden" name="hospital_id" value="<?php echo $all_hospital_detail[0]->hospital_id;  ?>">

                                                                </form>
                                                            </div>

 
                                                            <?php $attributes = array('name' => 'import_attendance', 'id' => 'xin-form', 'autocomplete' => 'off');?>

                                                            <?php $hidden = array('hospital_id' => $all_hospital_detail[0]->hospital_id);?>

                                                            <?php echo form_open_multipart('admin/Hospital/import_drugs', $attributes, $hidden);?>

                                                            <div class="row">

                                                              <div class="col-md-4">

                                                                <div class="form-group">

                                                                  <fieldset class="form-group">

                                                                    <label for="logo"><?php echo $this->lang->line('xin_employee_upload_file');?></label>

                                                                    <input type="file" class="form-control-file" id="file" name="file">

                                                                    <small><?php echo $this->lang->line('xin_employee_imp_allowed_size');?></small>

                                                                  </fieldset>

                                                                </div>

                                                              </div>

                                                            </div>

                                                            <div class="mt-1">

                                                              <div class="form-actions box-footer"> <?php echo form_button(array('name' => 'hrsale_form', 'type' => 'submit', 'class' => $this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '.$this->lang->line('xin_save'))); ?> </div>

                                                            </div>
                                                            <?php echo form_close(); ?>
                                                            <?php echo form_open_multipart('admin/Hospital/delete_drugs');?>
                                                            <button  id="del_drugs" style="position: fixed;top: 370px;" type="submit"  class="btn btn-danger float-right">DELETE ALL DRUGS</button>
                                                            <input type="hidden" name="hospital_id" value="<?php echo $all_hospital_detail[0]->hospital_id;  ?>">
                                                            </form>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="table-responsive"> 
                                                                <table class="datatables-demo table table-striped table-bordered"  id="xin_table_new">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Action </th>
                                                                            <th>Drugs Name </th>
                                                                            <th>Price </th>
                                                                        </tr> 
                                                                    </thead>    
                                                                    
                                                                    <tbody>
                                                                    <?php if (isset($all_hospital_drugs) and !empty($all_hospital_drugs)) { ?>
                                                                    	 
                                                                   
                                                                    <?php foreach ($all_hospital_drugs as $key => $value): ?>
                                                                        <tr>
                                                                            <td>
                                                                                <button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light edit_detect" data-toggle="modal" data-target=".edit-modal-data2" 


                                                                                data-ticket_id="<?php echo isset($value->drug_id) ? $value->drug_id : ''; ?>"
                                                                                data-drug_name="<?php echo isset($value->drug_name) ? $value->drug_name : ''; ?>"
                                                                                data-drug_price="<?php echo isset($value->drug_price) ? $value->drug_price : ''; ?>"
                                                                                 
                                                                                ><span class="fa fa-pencil"></span></button>


                                                                                 
                                                                                <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                                                                    <button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal2" data-record-id="<?php echo isset($value->drug_id) ? $value->drug_id : ''; ?>"><span class="fa fa-trash"></span></button>
                                                                                </span>  
                                                                            </td>

                                                                            <td><?php echo isset($value->drug_name) ? $value->drug_name : ''; ?> </td>

                                                                            <td><?php echo isset($value->drug_price) ? ' ₦'.$value->drug_price : ''; ?> </td>

                                                                           
                                                                        </tr> 
                                                                    <?php endforeach ?>
                                                                <?php	} ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="form-action box-footer">   </div>

                                                </div>
                     

                                            	</div>

                                   			</div>

                                			</div>

                                			</div>

                            			</div>
                        			</div>


                        			<div class="tab-pane" id="xin_services"  >

                                        <div class="box-body">

                                            <div class="row no-gutters row-bordered row-border-light">

                                            <div class="col-md-12">

                                            <div class="tab-content">

                                                <div class="tab-pane   active" id="profile-picture">

                                                <div class="box-body pb-2">

                                                   
                                                    <div class="bg-white">

                                                        <div class="row">

                                                            <div class=" ">
                                                                 
                                                                  <h3 class="box-title">Import CSV file only</h3>
                                                                
                                                                
                                                                    <p class="card-text">The first line in downloaded csv file should remain as it is. Please do not change the order of columns in csv file.</p>
                                                                    <p class="card-text">The correct column order is (Service Name, Service Price) and you must follow the csv file, otherwise you will get an error while importing the csv file.</p>
                                                                    <h6><a href="<?php echo base_url(); ?>/uploads/csv/sample-csv-services.csv" class="btn btn-primary"> <i class="fa fa-download"></i> Download sample File </a></h6>
                                                                <?php echo form_open_multipart('admin/Hospital/add_services');?>
                                                                <button type="submit"  class="btn btn-success float-right"> Save Service</button>
                                                                <input name="price" placeholder="Service Price" min="1" style="margin-bottom: 5px;width: 30%;float: right;" class="form-control" type="number" title="Fill Service price field" required>
                                                                <input name="title" style="margin-bottom: 5px;width: 30%;float: right;" class="form-control" placeholder="Service Name" title="Fill Service title field" type="text" required>
                                                                <input type="hidden" name="hospital_id" value="<?php echo $all_hospital_detail[0]->hospital_id;  ?>">

                                                                </form>

                                                            </div>
 
                                                            <?php $attributes = array('name' => 'import_attendance', 'id' => 'xin-form', 'autocomplete' => 'off');?>

                                                            <?php $hidden = array('hospital_id' => $all_hospital_detail[0]->hospital_id);?>
                                                            <!-- Modal -->

                                                            <?php echo form_open_multipart('admin/Hospital/import_hospital_services', $attributes, $hidden);?>

                                                            <div class="row">

                                                              <div class="col-md-4">

                                                                <div class="form-group">

                                                                  <fieldset class="form-group">

                                                                    <label for="logo"><?php echo $this->lang->line('xin_employee_upload_file');?></label>

                                                                    <input type="file" class="form-control-file" id="file" name="file">

                                                                    <small><?php echo $this->lang->line('xin_employee_imp_allowed_size');?></small>

                                                                  </fieldset>

                                                                </div>

                                                              </div>

                                                            </div>

                                                            <div class="mt-1">

                                                              <div class="form-actions box-footer"> <?php echo form_button(array('name' => 'hrsale_form', 'type' => 'submit', 'class' => $this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '.$this->lang->line('xin_save'))); ?> </div>

                                                            </div>

                                                            <?php echo form_close(); ?>
                                                            <?php echo form_open_multipart('admin/Hospital/delete_serviceAll');?>
                                                            <button style="position: fixed;top: 370px;" type="submit"  class="btn btn-danger float-right">DELETE ALL SERVICES</button>
                                                            <input type="hidden" name="hospital_id" value="<?php echo $all_hospital_detail[0]->hospital_id;  ?>">
                                                            </form>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="table-responsive"> 
                                                                <table class="datatables-demo table table-striped table-bordered"  id="xin_table_new2">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Action </th>
                                                                            <th>Service Name </th>
                                                                            <th width="20%">Price </th>
                                                                        </tr> 
                                                                    </thead>    
                                                                    
                                                                    <tbody>
                                                                    <?php if (isset($xin_services_hospital) and !empty($xin_services_hospital)) { ?>
                                                                    	 
                                                                   
                                                                    <?php foreach ($xin_services_hospital as $key => $value): ?>
                                                                        <tr>
                                                                            <td>
                                                                                <button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light edit_detect2" data-toggle="modal" data-target=".edit-modal-data3" 


                                                                                data-ticket_id2="<?php echo isset($value->id	) ? $value->id	 : ''; ?>"
                                                                                data-service_name="<?php echo isset($value->service_name) ? $value->service_name : ''; ?>"
                                                                                data-service_price="<?php echo isset($value->service_price) ? $value->service_price : ''; ?>"
                                                                                 
                                                                                ><span class="fa fa-pencil"></span></button>


                                                                                 
                                                                                <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                                                                    <button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete2" data-toggle="modal" data-target=".delete-modal3" data-record-id="<?php echo isset($value->id) ? $value->id : ''; ?>"><span class="fa fa-trash"></span></button>
                                                                                </span>  
                                                                            </td>

                                                                            <td><?php echo isset($value->service_name) ? $value->service_name : ''; ?> </td>

                                                                            <td><?php echo isset($value->service_price) ? ' ₦'.$value->service_price : ''; ?> </td>

                                                                           
                                                                        </tr> 
                                                                    <?php endforeach ?>
                                                                <?php	} ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="form-action box-footer">   </div>

                                                </div>
                     

                                            	</div>

                                   			</div>

                                			</div>

                                			</div>

                            			</div>
                        			</div>
                 
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                </div>




            </div>

        </div>

    </div>

</div>



<script type="text/javascript">
//    function delDrug(){
//        var r = confirm("All Drugs Are deleted Permanently ");
//        if (r == true) {
//          document.getElementById('del_drug').submit();
//        } else {
//
//        }
//    }
    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable();
        var xin_table_new2 = $('#xin_table_new2').dataTable();

        $("#xin-form2").submit(function(e){
            var fd = new FormData(this);
            var obj = $(this), action = obj.attr('name');
            fd.append("is_ajax", 1);
            fd.append("add_type", 'training');
            fd.append("form", action);
            e.preventDefault();
            $('.icon-spinner3').show();
            $('.save').prop('disabled', true);
            $.ajax({
                url: e.target.action,
                type: "POST",
                data:  fd,
                contentType: false,
                cache: false,
                processData:false,
                success: function(JSON)
                {
                    if (JSON.error != '') {
                        toastr.error(JSON.error);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                            $('.save').prop('disabled', false);
                            $('.icon-spinner3').hide();
                    } else {
                        toastr.success(JSON.result);
                        setTimeout(function(){
                           window.location.reload(1);
                        }, 5000);
                        $('.icon-spinner3').hide();
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                        $('#xin-form2')[0].reset(); // To reset form fields
                        $('.add-form').removeClass('in');
                        $('.select2-selection__rendered').html('--Select--');
                        $('.save').prop('disabled', false);
                    }
                },
                error: function() 
                {
                    toastr.error(JSON.error);
                    $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                    $('.icon-spinner3').hide();
                    $('.save').prop('disabled', false);
                }           
           });
        });


        $("#delete_record2").submit(function(e){
            /*Form Submit*/
            e.preventDefault();
            var obj = $(this), action = obj.attr('name');
            $.ajax({
                type: "POST",
                url: e.target.action,
                data: obj.serialize()+"&is_ajax=2&form="+action,
                cache: false,
                success: function (JSON) {
                    if (JSON.error != '') {
                        toastr.error(JSON.error);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                    } else {
                        $('.delete-modal2').modal('toggle');
                         
                        toastr.success(JSON.result);
                        setTimeout(function(){
                           window.location.reload(1);
                        }, 5000);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);                 
                    }
                }
            });
        });




             /* Edit data */
        $("#edit_ticket2").submit(function(e){
            var fd = new FormData(this);
            var obj = $(this), action = obj.attr('name');
            fd.append("is_ajax", 1);
            fd.append("edit_type", 'ticket');
            fd.append("form", action);
            e.preventDefault();
            $('.icon-spinner3').show();
            $('.save').prop('disabled', true);
            $.ajax({
                url: e.target.action,
                type: "POST",
                data:  fd,
                contentType: false,
                cache: false,
                processData:false,
                success: function(JSON)
                {
                    if (JSON.error != '') {
                        toastr.error(JSON.error);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                            $('.save').prop('disabled', false);
                            $('.icon-spinner3').hide();
                    } else {
                        // On page load: datatable
                        // var xin_table = $('#xin_table').dataTable({
                        //     "bDestroy": true,
                        //     "ajax": {
                        //         url : "https://hmo.liontech.com.ng/admin/tickets/ticket_list",
                        //         type : 'GET'
                        //     },
                        //     "fnDrawCallback": function(settings){
                        //     $('[data-toggle="tooltip"]').tooltip();          
                        //     }
                        // });
                        
                            toastr.success(JSON.result);

                            setTimeout(function(){
                               window.location.reload(1);
                            }, 4000);
                        
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                        $('.icon-spinner3').hide();
                        $('.edit-modal-data2').modal('toggle');
                        $('.save').prop('disabled', false);
                    }
                },
                error: function() 
                {
                    toastr.error(JSON.error);
                    $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                    $('.icon-spinner3').hide();
                    $('.save').prop('disabled', false);
                }           
            });
        });
        




        $('.edit_detect').on('click',function(){ 
            var drug_nam      = $(this).attr('data-drug_name');
            var drug_price    = $(this).attr('data-drug_price'); 
            var drug_id_edit  = $(this).attr('data-ticket_id'); 


            $('#drug_nam').val(drug_nam); 
            $('#drug_price').val(drug_price);
            $('#drug_id_edit').val(drug_id_edit);
             
        })


        $('.edit_detect2').on('click',function(){ 
            var service_name      = $(this).attr('data-service_name');
            var service_price    = $(this).attr('data-service_price'); 
            var service_id  = $(this).attr('data-ticket_id2'); 
 

            $('#service_name').val(service_name); 
            $('#service_price').val(service_price);
            $('#service_id_edit').val(service_id);
             
        })

        $('.delete').on('click',function(){
            var record_id  = $(this).attr('data-record-id');
            $('#record_id').val(record_id); 
        }) 

        $('.delete2').on('click',function(){
            var record_id  = $(this).attr('data-record-id');
            $('#record_id2').val(record_id); 
        }) 



        // $("#edit_ticket2").submit(function(e){
        //     var fd = new FormData(this);
        //     var obj = $(this), action = obj.attr('name');
        //     fd.append("is_ajax", 1);
        //     fd.append("edit_type", 'ticket');
        //     fd.append("form", action);
        //     e.preventDefault();
        //     $('.icon-spinner3').show();
        //     $('.save').prop('disabled', true);
        //     $.ajax({
        //         url: e.target.action,
        //         type: "POST",
        //         data:  fd,
        //         contentType: false,
        //         cache: false,
        //         processData:false,
        //         success: function(JSON)
        //         {
        //             if (JSON.error != '') {
        //                 toastr.error(JSON.error);
        //                 $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
        //                     $('.save').prop('disabled', false);
        //                     $('.icon-spinner3').hide();
        //             } else {
        //                 // On page load: datatable
        //                 // var xin_table = $('#xin_table').dataTable({
        //                 //     "bDestroy": true,
        //                 //     "ajax": {
        //                 //         url : "https://hmo.liontech.com.ng/admin/tickets/ticket_list",
        //                 //         type : 'GET'
        //                 //     },
        //                 //     "fnDrawCallback": function(settings){
        //                 //     $('[data-toggle="tooltip"]').tooltip();          
        //                 //     }
        //                 // });
                        
        //                     toastr.success(JSON.result);

        //                     setTimeout(function(){
        //                        window.location.reload(1);
        //                     }, 4000);
                        
        //                 $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
        //                 $('.icon-spinner3').hide();
        //                 $('.edit-modal-data2').modal('toggle');
        //                 $('.save').prop('disabled', false);
        //             }
        //         },
        //         error: function() 
        //         {
        //             toastr.error(JSON.error);
        //             $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
        //             $('.icon-spinner3').hide();
        //             $('.save').prop('disabled', false);
        //         }           
        //     });
        // });
        

    }, false);

     
</script>
<div class="modal fade delete-modal2 animated in" role="dialog" aria-hidden="true"  >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"> 
                <button type="button" aria-label="Close" data-dismiss="modal" class="close"><span aria-hidden="true">×</span></button>
                <strong class="modal-title">Are you sure you want to delete this record?</strong> 
            </div>
            <div class="alert alert-danger">
                <strong>Record deleted can't be restored!!!</strong>
            </div>
            <form action="<?php echo base_url(); ?>admin/Hospital/delete_drug" name="delete_record" id="delete_record2" autocomplete="off" role="form" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" id="record_id" value="">
                <input type="hidden" name="csrf_hrsale" value="288e255c59f6126650e28264e712db77">                                                                                                                         
 
                <div class="modal-footer">
        
                    <input type="hidden" name="token_type" value="0" id="token_type">
            
                    <button type="button" data-dismiss="modal" class="btn btn-secondary"><i class="fa fa fa-check-square-o"></i> Close</button>
     
                    <button name="hrsale_form" type="submit" class="btn btn-primary"><i class="fa fa fa-check-square-o"></i> Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="modal fade delete-modal3 animated in" role="dialog" aria-hidden="true"  >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"> 
                <button type="button" aria-label="Close" data-dismiss="modal" class="close"><span aria-hidden="true">×</span></button>
                <strong class="modal-title">Are you sure you want to delete this record?</strong> 
            </div>
            <div class="alert alert-danger">
                <strong>Record deleted can't be restored!!!</strong>
            </div>
            <form action="<?php echo base_url(); ?>admin/Hospital/delete_service" name="delete_record" id="delete_record2" autocomplete="off" role="form" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" id="record_id2" value="">
                <input type="hidden" name="csrf_hrsale" value="288e255c59f6126650e28264e712db77">                                                                                                                         
 
                <div class="modal-footer">
        
                    <input type="hidden" name="token_type" value="0" id="token_type">
            
                    <button type="button" data-dismiss="modal" class="btn btn-secondary"><i class="fa fa fa-check-square-o"></i> Close</button>
     
                    <button name="hrsale_form" type="submit" class="btn btn-primary"><i class="fa fa fa-check-square-o"></i> Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>





<!-- edit -->

<div class="modal fade edit-modal-data2 animated in" id="edit-modal-data" role="dialog" aria-labelledby="edit-modal-data" aria-hidden="true"  >
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="ajax_modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                <h4 class="modal-title" id="edit-modal-data">Edit Drug</h4>
            </div>

            <form action="<?php echo base_url(); ?>admin/Hospital/update_drug" name="edit_ticket" id="edit_ticket2" autocomplete="off" class="m-b-1" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="EDIT">
                <input type="hidden" name="_token" value="">
                <input type="hidden" name="drug_id_edit" id="drug_id_edit" value="">
                <input type="hidden" name="ext_name" value="1">
                <input type="hidden" name="csrf_hrsale" value="95edc3390e40ab2fc8a739f96b5ee8aa">
                <div class="modal-body">
                    <div class="box-block"> 
                        <div class="row"> 
                            <div class="col-md-6"> 
                                <div class="row"> 
                                    <div class="col-md-12">  
                                        <div class="form-group"> 
                                            <label for="company_name">Drug Name</label> 
                                            <input class="form-control" required="" placeholder="Drug Name" id="drug_nam" name="drug_nam" type="text" value=""> 
                                        </div>  
                                    </div> 
                                </div>   
                            </div> 
                            <div class="col-md-6"> 
                                <div class="row"> 
                                    <div class="col-md-12">  
                                        <div class="form-group"> 
                                            <label for="company_name">Price </label> 
                                            <input class="form-control" required="" placeholder="Price " name="drug_price" id="drug_price" type="text" value=""> 
                                        </div>  
                                    </div> 
                                </div>   
                            </div> 
                           
                        </div> 

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form> 
        </div>
    </div>
</div>



<div class="modal fade edit-modal-data3 animated in" id="edit-modal-data" role="dialog" aria-labelledby="edit-modal-data" aria-hidden="true"  >
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="ajax_modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                <h4 class="modal-title" id="edit-modal-data">Edit Services</h4>
            </div>

            <form action="<?php echo base_url(); ?>admin/Hospital/update_services" name="edit_ticket" id="edit_ticket2" autocomplete="off" class="m-b-1" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="EDIT">
                <input type="hidden" name="_token" value="">
                <input type="hidden" name="service_id_edit" id="service_id_edit" value="">
                <input type="hidden" name="ext_name" value="1">
                <input type="hidden" name="csrf_hrsale" value="95edc3390e40ab2fc8a739f96b5ee8aa">
                <div class="modal-body">
                    <div class="box-block">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="company_name">Service Name</label>
                                            <input class="form-control" required="" placeholder="Service Name" id="service_name" name="service_name" type="text" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="company_name">Price </label>
                                            <input class="form-control" required="" placeholder="Price " name="service_price" id="service_price" type="text" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>