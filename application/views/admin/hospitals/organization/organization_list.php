<?php

/* Subscription view

*/

?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('622',$role_resources_ids) || in_array('623',$role_resources_ids) || in_array('624',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>

<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

        <h3 class="box-title">Create New Organizations  </h3>

        <div class="box-tools pull-right"> 
            <a class="text-dark collapsed" data-toggle="collapse" href="#add_form" aria-expanded="false">

                <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span> Create New Organization</button>

            </a> 
        </div>

    </div>

    <div id="add_form" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">

        <?php $attributes = array('name' => 'add_subscription', 'id' => 'xin-form2', 'autocomplete' => 'off');?>

        <?php $hidden = array('_user' => $session['user_id']);?>

        <?php echo form_open_multipart('admin/Hospital/add_organization', $attributes, $hidden);?>

        <div class="bg-white">

            <div class="box-block">

                <div class="row"> 
                    <div class="col-md-6"> 
                        <div class="row"> 
                            <div class="col-md-12">  
                                <div class="form-group"> 
                                    <label for="company_name">Organization Name</label> 
                                    <input class="form-control" required="" placeholder="Organization Name" name="name" type="text" value=""> 
                                </div>  
                            </div> 
                        </div>   
                    </div> 

                    <div class="col-md-6"> 
                        <div class="form-group"> 
                            <label for="training_type">State</label>

                            <select class="form-control" name="location_id" data-plugin="select_hrm" data-placeholder="Location"> 
                                <option value=""></option> 
                                <?php if (isset($all_locations) and !empty($all_locations)): ?>
                                    <?php foreach ($all_locations as  $value): ?>
                                        <?php echo "<option value='".$value->location_id."' >".$value->location_name."</option> " ?>
                                    <?php endforeach ?>
                                    
                                <?php endif ?>
                            </select> 
                        </div> 
                    </div> 
                </div>


               

                <div class="row">
                    <div class="col-md-6"> 
                        <div class="row"> 
                            <div class="col-md-12">  
                                <div class="form-group"> 
                                    <label for="company_name">HR Name </label> 
                                    <input class="form-control" required="" placeholder="Contact Person " name="contact_person" type="text" value=""> 
                                </div>  
                            </div> 
                        </div>   
                    </div>  

                    <div class="col-md-6"> 
                        <div class="row"> 
                            <div class="col-md-12">  
                                <div class="form-group"> 
                                    <label for="company_name">Address  </label> 
                                    <input class="form-control" required="" placeholder="Address of org" name="rc_number" type="text" value=""> 
                                </div>  
                            </div> 
                        </div>   
                    </div>  


                    <div class="col-md-6"> 
                        <div class="form-group"> 
                            <label for="training_type">Type Business</label>

                            <select class="form-control" name="type_business" data-plugin="select_hrm" data-placeholder="Business Type"> 
                                <option value=""></option> 

                                <?php if (isset($all_business) and !empty($all_business)): ?>
                                    <?php foreach ($all_business as  $value): ?>
                                        <?php echo "<option value='".$value->business_id."' >".$value->business_name."</option> " ?>
                                    <?php endforeach ?>
                                    
                                <?php endif ?>
                            </select> 
                        </div> 
                    </div>  
                    
                     
                    <div class="col-md-6"> 
                        <div class="form-group"> 
                            <label for="training_type">Logo</label>

                            <input type="file" class="form-control-file" id="logo" name="logo">
                        </div> 
                    </div>



                </div>
                    <div class="row">
                        <div class="col-md-6"> 
                        <div class="form-group"> 
                            <label for="training_type">Commencement Date</label>

                            <input type="text" class="form-control date" id="comm_date" name="comm_date">
                        </div> 
                        </div>
                    </div>

                
                
                <div class="form-actions box-footer"> 
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-check-square-o"></i> <?php echo $this->lang->line('xin_save');?> </button> 
                </div>

            </div>

        </div>

        <?php echo form_close(); ?> </div>

    </div>

  </div>

</div>

<?php if (in_array('624',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
    
<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

        <h3 class="box-title">Organization Profiles Bulk Upload</h3>

        <div class="box-tools pull-right"> 
            <a class="text-dark collapsed" data-toggle="collapse" href="#upload" aria-expanded="false">

                <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span> Open</button>

            </a> 
        </div>

    </div>

    <div id="upload" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">

        <a href="<?php echo base_url(); ?>/uploads/csv/sample-csv-organizations.csv" class="btn btn-primary"> <i class="fa fa-download"></i> Download sample File </a> 

        <?php $attributes = array('name' => 'import_attendance', 'id' => 'xin-forms', 'autocomplete' => 'off');?>

            <?php echo form_open_multipart('admin/hospital/import_organization', $attributes, $hidden);?>

            <div class="row">
                <?php if ($this->session->flashdata('success')): ?>
                    <div class="alert alert-success  alert-dismissible" style="margin: 20px 12px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif ?>

                 <?php if ($this->session->flashdata('error')): ?>
                    <div class="alert alert-warning  alert-dismissible" style="margin: 20px 12px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif ?>

                <div class="col-md-4 form-group"> 

                    <fieldset class="form-group">

                        <label for="logo"><?php echo $this->lang->line('xin_employee_upload_file');?></label>

                        <input type="file" class="form-control-file" id="file" name="file">

                        <small><?php echo $this->lang->line('xin_employee_imp_allowed_size');?></small>

                    </fieldset> 
                </div>

            </div>

            <div class="mt-1">

              <div class="form-actions box-footer"> <?php echo form_button(array( 'type' => 'submit', 'class' => $this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '.$this->lang->line('xin_save'))); ?> </div>

            </div>

            <?php echo form_close(); ?>
    </div>

    </div>

  </div>

</div>

<?php endif ?>

<?php } ?>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

      	<table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

        	<thead>

          		<tr>

		            <th><?php echo $this->lang->line('xin_action');?></th>

		            <th>Organization</th>
                    <th>Location </th>
                    <th>Commencement</th>
                    <th>Status</th>
		            <th>Created</th>
 
             
          		</tr>
          	</thead>

          	<tbody>      
          		<?php 
                    if(!empty($all_organizations))
                    { 
                        foreach ($all_organizations as $key => $value)
                        {   
                            $ci=& get_instance();
                            $ci->load->model('Training_model'); 

                            $business_is    = $ci->Training_model->getAll2('business_type', ' business_id='. $value->type_business.' ');

                            $location = $ci->Training_model->getAll2('xin_location', ' location_id='. $value->location_id.' ');    
                 			?>
		                    <tr>
		                        <td>
		                        	<button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light edit_detect " data-toggle="modal" data-target=".edit-modal-data2" data-ticket_id="<?php echo isset($value->id) ? $value->id : ''; ?>"   

		                        	data-name="<?php echo isset($value->name) ? $value->name : ''; ?>"
		                        	data-location="<?php echo isset($value->location_id) ? $value->location_id : ''; ?>"
		                        	data-person="<?php echo isset($value->contact_person) ? $value->contact_person : ''; ?>"
		                        	data-rc_number="<?php echo isset($value->rc_number) ? $value->rc_number : ''; ?>"
		                        	data-business_name="<?php echo isset($value->type_business) ? $value->type_business : ''; ?>"
                                    data-comm_date="<?php echo isset($value->comm_date) ? $value->comm_date : ''; ?>"
		                        	data-logo_name="<?php echo isset($value->logo_name) ? $value->logo_name : ''; ?>" ><span class="fa fa-pencil"></span></button>

		                        	<span data-toggle="tooltip" data-placement="top" title="" data-original-title="View Details"><a href="<?php echo base_url(); ?>admin/Hospital/organization_detail/<?php echo $value->id; ?>"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"><span class="fa fa-arrow-circle-right"></span></button></a></span>

                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                        <button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal2" data-record-id="<?php echo isset($value->id) ? $value->id : ''; ?>"><span class="fa fa-trash"></span></button>
                                    </span>   
		                        </td>

		                        <td><?php echo isset($value->name) ? $value->name : ''; ?> </td>

                                <td><?php echo isset($location[0]->location_name) ? $location[0]->location_name : ''; ?></td> 

                                <td><?php echo isset($value->comm_date) ? $value->comm_date : ''; ?> </td>

                                <td><?php 
                                    if (isset($value->status)) {
                                        switch($value->status) {
                                            case '0':
                                                ?>
                                                    <button class="btn btn-warning">Inactive</button>      
                                                    <button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light edit_status_detect" data-toggle="modal" data-target=".edit-status-modal-data" data-id="<?php echo isset($value->id) ? $value->id : ''; ?>"   
                                                    data-status="<?php echo isset($value->status) ? $value->status : ''; ?>"
                                                    ><span class="fa fa-pencil"></span></button>
                                                <?php
                                                break;
                                            case '1':
                                                ?>
                                                    <button class="btn btn-primary">Active</button>      
                                                    <button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light edit_status_detect" data-toggle="modal" data-target=".edit-status-modal-data" data-id="<?php echo isset($value->id) ? $value->id : ''; ?>"   
                                                    data-status="<?php echo isset($value->status) ? $value->status : ''; ?>"
                                                    ><span class="fa fa-pencil"></span></button>
                                                <?php
                                                break;
                                            
                                            
                                        }
                                    }

                                ?> </td>

		                        <td><?php echo isset( $value->created_on) ?  strftime('%d-%m-%Y',strtotime($value->created_on)) : ''; ?></td>  
		                    </tr> 

                			<?php   
                		}
                	}

                ?>
            </tbody> 
      	</table>

    </div>

  </div>

</div>
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){ 

        $('.edit_status_detect').on('click',function(){
            var id = $(this).attr('data-id');
            var status = $(this).attr('data-status');

            if(status == 1){
                $('#status').val('active');
                $('#act').attr('selected','selected');
                $('#text_status').text('Active');
                $('#id').val(id);
            }else{
                $('#status').val('inactive');
                $('#inact').attr('selected','selected');
                $('#text_status').text('Inactive');
                $('#id').val(id);

            }


            console.log('inactivate id: '+id+' status:'+status);
        });

        var xin_table_new = $('#xin_table_new').dataTable({
            dom: 'lBfrtip',
            "buttons": ['csv', 'excel', 'pdf', 'print'], // colvis > if needed
            "fnDrawCallback": function(settings){
            $('[data-toggle="tooltip"]').tooltip();          
            }
        });


        $("#xin-form2").submit(function(e){
			var fd = new FormData(this);
			var obj = $(this), action = obj.attr('name');
			fd.append("is_ajax", 1);
			fd.append("add_type", 'training');
			fd.append("form", action);
			e.preventDefault();
			$('.icon-spinner3').show();
			$('.save').prop('disabled', true);
			$.ajax({
				url: e.target.action,
				type: "POST",
				data:  fd,
				contentType: false,
				cache: false,
				processData:false,
				success: function(JSON)
				{
					if (JSON.error != '') {
						toastr.error(JSON.error);
						$('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
							$('.save').prop('disabled', false);
							$('.icon-spinner3').hide();
					} else {
						toastr.success(JSON.result);
						setTimeout(function(){
						   window.location.reload(1);
						}, 5000);
						$('.icon-spinner3').hide();
						$('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
						$('#xin-form2')[0].reset(); // To reset form fields
						$('.add-form').removeClass('in');
						$('.select2-selection__rendered').html('--Select--');
						$('.save').prop('disabled', false);
					}
				},
				error: function() 
				{
					toastr.error(JSON.error);
					$('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
					$('.icon-spinner3').hide();
					$('.save').prop('disabled', false);
				} 	        
		   });
		});




        $("#delete_record2").submit(function(e){
            /*Form Submit*/
            e.preventDefault();
            var obj = $(this), action = obj.attr('name');
            $.ajax({
                type: "POST",
                url: e.target.action,
                data: obj.serialize()+"&is_ajax=2&form="+action,
                cache: false,
                success: function (JSON) {
                    if (JSON.error != '') {
                        toastr.error(JSON.error);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                    } else {
                        $('.delete-modal2').modal('toggle');
                         
                        toastr.success(JSON.result);
                        setTimeout(function(){
                           window.location.reload(1);
                        }, 5000);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);                 
                    }
                }
            });
        });






     /* Edit data */
        $("#edit_ticket2").submit(function(e){
            var fd = new FormData(this);
            var obj = $(this), action = obj.attr('name');
            fd.append("is_ajax", 1);
            fd.append("edit_type", 'ticket');
            fd.append("form", action);
            e.preventDefault();
            $('.icon-spinner3').show();
            $('.save').prop('disabled', true);
            $.ajax({
                url: e.target.action,
                type: "POST",
                data:  fd,
                contentType: false,
                cache: false,
                processData:false,
                success: function(JSON)
                {
                    if (JSON.error != '') {
                        toastr.error(JSON.error);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                            $('.save').prop('disabled', false);
                            $('.icon-spinner3').hide();
                    } else {
                        // On page load: datatable
                        // var xin_table = $('#xin_table').dataTable({
                        //     "bDestroy": true,
                        //     "ajax": {
                        //         url : "https://hmo.liontech.com.ng/admin/tickets/ticket_list",
                        //         type : 'GET'
                        //     },
                        //     "fnDrawCallback": function(settings){
                        //     $('[data-toggle="tooltip"]').tooltip();          
                        //     }
                        // });
                        
                            toastr.success(JSON.result);

                            setTimeout(function(){
                               window.location.reload(1);
                            }, 4000);
                        
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                        $('.icon-spinner3').hide();
                        $('.edit-modal-data2').modal('toggle');
                        $('.save').prop('disabled', false);
                    }
                },
                error: function() 
                {
                    toastr.error(JSON.error);
                    $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                    $('.icon-spinner3').hide();
                    $('.save').prop('disabled', false);
                }           
            });
        });
        

        $('.edit_detect').on('click',function(){
            var name          = $(this).attr('data-name');
            var location      = $(this).attr('data-location');
            var person        = $(this).attr('data-person');
            var rc_number     = $(this).attr('data-rc_number');
            var business_name = $(this).attr('data-business_name');
            var comm_date     = $(this).attr('data-comm_date');
            var logo_name     = $(this).attr('data-logo_name');
            var id            = $(this).attr('data-ticket_id');
            
            $('#name').val(name);
            $('#location').val(location).trigger('change');
            $('#name').val(name);
            $('#organization_id').val(id);
            $('#contact_person').val(person);
            $('#rc_number').val(rc_number);
            $('#comm_date').val(comm_date);
            $('#business_name').val(business_name).trigger('change');
            $('#logo_name').attr('src','<?php echo base_url(); ?>uploads/organization/logo/' +logo_name);
        })

        

 
    }, false);

     
</script>


<div class="modal fade delete-modal2 animated in" role="dialog" aria-hidden="true"  >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"> 
                <button type="button" aria-label="Close" data-dismiss="modal" class="close"><span aria-hidden="true">×</span></button>
                <strong class="modal-title">Are you sure you want to delete this record?</strong> 
            </div>
            <div class="alert alert-danger">
                <strong>Record deleted can't be restored!!!</strong>
            </div>
            <form action="<?php echo base_url(); ?>admin/Hospital/delete_organization" name="delete_record" id="delete_record2" autocomplete="off" role="form" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="">
                <input type="hidden" name="csrf_hrsale" value="288e255c59f6126650e28264e712db77">                                                                                                                         
 
                <div class="modal-footer">
        
                    <input type="hidden" name="token_type" value="0" id="token_type">
            
                    <button type="button" data-dismiss="modal" class="btn btn-secondary"><i class="fa fa fa-check-square-o"></i> Close</button>
     
                    <button name="hrsale_form" type="submit" class="btn btn-primary"><i class="fa fa fa-check-square-o"></i> Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- edit -->

<div class="modal fade edit-modal-data2 animated in" id="edit-modal-data" role="dialog" aria-labelledby="edit-modal-data" aria-hidden="true"  >
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="ajax_modal">
        <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                <h4 class="modal-title" id="edit-modal-data">Edit Organization Profile</h4>
            </div>

            <form action="<?php echo base_url(); ?>admin/Hospital/update_organization" name="edit_ticket" id="edit_ticket2" autocomplete="off" class="m-b-1" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="EDIT">
                <input type="hidden" name="_token" value="">
                <input type="hidden" name="organization_id" id="organization_id" value="">
                <input type="hidden" name="ext_name" value="1">
                <input type="hidden" name="csrf_hrsale" value="95edc3390e40ab2fc8a739f96b5ee8aa">
                <div class="modal-body"> 
	                <div class="row"> 
	                    <div class="col-md-6"> 
	                        <div class="row"> 
	                            <div class="col-md-12">  
	                                <div class="form-group"> 
	                                    <label for="company_name">Organization Name</label> 
	                                    <input class="form-control" required="" placeholder="Organization Name" name="name" type="text" value="" id="name"> 
	                                </div>  
	                            </div> 
	                        </div>   
	                    </div> 

	                    <div class="col-md-6"> 
	                        <div class="form-group"> 
	                            <label for="training_type">Location</label>

	                            <select class="form-control" id="location" name="location_id" data-plugin="select_hrm" data-placeholder="Location"> 
	                                <option value=""></option> 
	                                <?php if (isset($all_locations) and !empty($all_locations)): ?>
	                                    <?php foreach ($all_locations as  $value): ?>
	                                        <?php echo "<option value='".$value->location_id."' >".$value->location_name."</option> " ?>
	                                    <?php endforeach ?>
	                                    
	                                <?php endif ?>
	                            </select> 
	                        </div> 
	                    </div> 
	                </div>  

	                <div class="row">
	                    <div class="col-md-6"> 
	                        <div class="row"> 
	                            <div class="col-md-12">  
	                                <div class="form-group"> 
	                                    <label for="company_name">Contact Person </label> 
	                                    <input class="form-control" required="" placeholder="Contact Person " id="contact_person" name="contact_person" type="text" value=""> 
	                                </div>  
	                            </div> 
	                        </div>   
	                    </div>  

	                    <div class="col-md-6"> 
	                        <div class="row"> 
	                            <div class="col-md-12">  
	                                <div class="form-group"> 
	                                    <label for="company_name">RC Number  </label> 
	                                    <input class="form-control" required="" placeholder="RC Number" name="rc_number" id="rc_number" type="text" value=""> 
	                                </div>  
	                            </div> 
	                        </div>   
	                    </div>  


	                    <div class="col-md-6"> 
	                        <div class="form-group"> 
	                            <label for="training_type">Type Business</label>

	                            <select class="form-control" name="type_business" data-plugin="select_hrm" data-placeholder="Business Type" id="business_name"> 
	                                <option value=""></option> 

	                                <?php if (isset($all_business) and !empty($all_business)): ?>
	                                    <?php foreach ($all_business as  $value): ?>
	                                        <?php echo "<option value='".$value->business_id."' >".$value->business_name."</option> " ?>
	                                    <?php endforeach ?>
	                                    
	                                <?php endif ?>
	                            </select> 
	                        </div> 
	                    </div>  
                        <div class="col-md-6"> 
                            <div class="form-group"> 
                                <label for="training_type">Commencement Date</label>

                                <input type="text" class="form-control date" id="comm_date" name="comm_date">
                            </div> 
                        </div>
	                     
	                    <div class="col-md-12"> 
	                        <div class="form-group"> 
	                            <label for="training_type">Logo</label>
	                            <br>
	                            <img src="" id="logo_name">
	                            <input type="file" class="form-control-file" id="logo" name="logo">
	                        </div> 
	                    </div>
	                </div> 
                 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form> 
        </div>
    </div>
</div>

<!-- edit -->

<div class="modal fade edit-status-modal-data animated in" id="edit-status-modal-data" role="dialog" aria-labelledby="edit-status-modal-data" aria-hidden="true"  >
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="ajax_modal">
        <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                <h4 class="modal-title" id="edit-modal-data">Edit Organization Status</h4>
            </div>

            <form action="<?php echo base_url(); ?>admin/Hospital/update_organization_status" name="edit_ticket" id="edit_ticket2" autocomplete="off" class="m-b-1 status-form" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="EDIT">
                <input type="hidden" name="_token" value="">
                <input type="hidden" name="organization_id" id="organization_id" value="">
                <input type="hidden" name="ext_name" value="1">
                <input type="hidden" name="csrf_hrsale" value="95edc3390e40ab2fc8a739f96b5ee8aa">
                <input type="hidden" name="id" id="id">
                <div class="modal-body"> 

                    <div class="row">
                        <div class="col-md-6"> 
                            <div class="form-group"> 
                                <label for="training_type">Status: <span id="text_status"></span></label>

                                <select class="form-control" name="status" data-plugin="select_hrm" data-placeholder="Choose to change" id="status"> 
                                    <option value=""></option> 
                                    <option value="1" id="act">Active</option>
                                    <option value="0" id="inact">Inactive</option>
                                </select> 
                            </div> 
                        </div>  
                         
                    </div> 
                 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form> 
        </div>
    </div>
</div>