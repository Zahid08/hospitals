<?php

/* Subscription view

*/
 
?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>
<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('594',$role_resources_ids) || in_array('601',$role_resources_ids) || in_array('602',$role_resources_ids) || in_array('603',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>



<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

        <h3 class="box-title">Create New Provider  </h3>

        <div class="box-tools pull-right"> 
            <a class="text-dark collapsed" data-toggle="collapse" href="#add_form" aria-expanded="false">

                <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span> Create New Provider</button>

            </a> 
        </div>

    </div>

    <div id="add_form" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">

        <?php $attributes = array('name' => 'add_subscription', 'id' => 'xin-form2', 'autocomplete' => 'off');?>

        <?php $hidden = array('_user' => $session['user_id']);?>

        <?php echo form_open('admin/Hospital/add_hospital', $attributes, $hidden);?>

        <div class="bg-white">

            <div class="box-block">

                <div class="row"> 
                    <div class="col-md-6"> 
                        <div class="row"> 
                            <div class="col-md-12">  
                                <div class="form-group"> 
                                    <label for="company_name">Provider Name</label> 
                                    <input class="form-control" required="" placeholder="Hospital Name" name="hospital_name" type="text" value=""> 
                                </div>  
                            </div> 
                        </div>   
                    </div> 

                    <div class="col-md-6"> 
                        <div class="form-group"> 
                            <label for="training_type">Location</label>

                            <select class="form-control select2" name="location_id" data-plugin="select_hrm" data-placeholder="Location"> 
                                <option value=""></option> 
                                <?php if (isset($all_locations) and !empty($all_locations)): ?>
                                    <?php foreach ($all_locations as  $value): ?>
                                        <?php echo "<option value='".$value->location_id."' >".$value->location_name."</option> " ?>
                                    <?php endforeach ?>
                                    
                                <?php endif ?>
                            </select> 
                        </div> 
                    </div> 
                </div>


               

                <div class="row"> 
                    <div class="col-md-6"> 
                        <div class="form-group"> 
                            <label for="training_type">Band Type</label>

                            <select class="form-control" name="band_id" data-plugin="select_hrm" data-placeholder="Band Type"> 
                                <option value=""></option> 
                                <?php if (isset($all_bands) and !empty($all_bands)): ?>
                                    <?php foreach ($all_bands as $value): ?>
                                        <?php echo "<option value='".$value->band_id."' >".$value->band_name."</option> " ?>
                                    <?php endforeach ?> 
                                <?php endif ?>
                            </select> 
                        </div> 
                    </div>  

                    <div class="col-md-6"> 
                        <div class="row"> 
                            <div class="col-md-12">  
                                <div class="form-group"> 
                                    <label for="company_name">Email </label> 
                                    <input class="form-control" required="" placeholder="Email " name="email" type="email" value=""> 
                                </div>  
                            </div> 
                        </div>   
                    </div> 
                </div>
                <div class="row"> 
                    

                    <div class="col-md-6"> 
                        <div class="row"> 
                            <div class="col-md-12">  
                                <div class="form-group"> 
                                    <label for="company_name">Password </label> 
                                    <input class="form-control" required="" placeholder="Password " name="password" type="text" value=""> 
                                </div>  
                            </div> 
                        </div>   
                    </div>

                    <div class="col-md-6"> 
                        <div class="row"> 
                            <div class="col-md-12">  
                                <div class="form-group"> 
                                    <label for="company_name">Phone </label> 
                                    <input class="form-control" required="" placeholder="Phone " name="phone" type="text" value=""> 
                                </div>  
                            </div> 
                        </div>   
                    </div>

                    <div class="col-md-6"> 
                        <div class="form-group"> 
                            <label for="training_type">Address</label>

                            <textarea class="form-control" id="address" name="address" required=""></textarea>
                        </div> 
                    </div>
                     
                    <div class="col-md-6"> 
                        <div class="form-group"> 
                            <label for="training_type">Provider Logo</label>

                            <input type="file" class="form-control-file" id="logo" name="logo">
                        </div> 
                    </div>

                    
                </div>

                <div class="row">
                    <div class="col-md-6"> 
                        <div class="row"> 
                            <div class="col-md-12">  
                                <div class="form-group"> 
                                    <label for="company_name">Bank Name </label> 
                                    <input class="form-control" required="" placeholder="Bank Name " name="bank_name" type="text" value=""> 
                                </div>  
                            </div> 
                        </div>   
                    </div>
                    <div class="col-md-6"> 
                        <div class="row"> 
                            <div class="col-md-12">  
                                <div class="form-group"> 
                                    <label for="company_name">Bank Account Number</label> 
                                    <input class="form-control" required="" placeholder="Bank Account Number " name="bank_account" type="text" value=""> 
                                </div>  
                            </div> 
                        </div>   
                    </div>
                </div>

                
                
                <div class="form-actions box-footer"> 
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-check-square-o"></i> <?php echo $this->lang->line('xin_save');?> </button> 
                </div>

            </div>

        </div>

        <?php echo form_close(); ?> </div>

    </div>

  </div>

</div>

<?php if (in_array('603',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
    <div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

        <h3 class="box-title">Hospital Bulk Upload</h3>

        <div class="box-tools pull-right"> 
            <a class="text-dark collapsed" data-toggle="collapse" href="#upload" aria-expanded="false">

                <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span> Open</button>

            </a> 
        </div>

    </div>

    <div id="upload" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">

        <a href="<?php echo base_url(); ?>/uploads/csv/sample-csv-hospitals.csv" class="btn btn-primary"> <i class="fa fa-download"></i> Download sample File </a> 

        <?php $attributes = array('name' => 'import_attendance', 'id' => 'xin-forms', 'autocomplete' => 'off');?>

            <?php echo form_open_multipart('admin/hospital/import_hospitals', $attributes, $hidden);?>

            <div class="row">
                <?php if ($this->session->flashdata('success')): ?>
                    <div class="alert alert-success  alert-dismissible" style="margin: 20px 12px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif ?>

                 <?php if ($this->session->flashdata('error')): ?>
                    <div class="alert alert-warning  alert-dismissible" style="margin: 20px 12px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif ?>

                <div class="col-md-4 form-group"> 

                    <fieldset class="form-group">

                        <label for="logo"><?php echo $this->lang->line('xin_employee_upload_file');?></label>

                        <input type="file" class="form-control-file" id="file" name="file">

                        <small><?php echo $this->lang->line('xin_employee_imp_allowed_size');?></small>

                    </fieldset> 
                </div>

            </div>

            <div class="mt-1">

              <div class="form-actions box-footer"> <?php echo form_button(array( 'type' => 'submit', 'class' => $this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '.$this->lang->line('xin_save'))); ?> </div>

            </div>

            <?php echo form_close(); ?>
    </div>

    </div>

  </div>

</div>    
<?php endif ?>





<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

        <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

            <thead>

                <tr> 
                    <th width="10%"><?php echo $this->lang->line('xin_action');?></th>
                    <th width="8%">HCP ID</th>
                    <th>Provider</th>
                    <th>Phone</th>
                    <th width="7%">Band</th>
                    <th>Location</th>
                   <!-- <th>Loc ID</th> -->
                    <th width="12%">Bank</th>
                    <th width="8%">Account</th>
                </tr>
            </thead>

                <tbody> 
                    <?php   
                    if(!empty($all_hospital))
                    { 
                        foreach ($all_hospital as $key => $value)
                        {              
                             
                        
                            $detail_pkg = '';

                            $ci=& get_instance();
                            $ci->load->model('Training_model'); 

                            $bands    = $ci->Training_model->getAll2('xin_bands', ' band_id='. $value->band_id.' ');

                            $location = $ci->Training_model->getAll2('xin_location', ' location_id='. $value->location_id.' ');
                            
                                
                            ?>  
                            <tr>
                                <td>
                                    <button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light edit_detect" data-toggle="modal" data-target=".edit-modal-data2" 


                                    data-ticket_id="<?php echo isset($value->hospital_id) ? $value->hospital_id : ''; ?>"
                                    data-band_id="<?php echo isset($value->band_id) ? $value->band_id : ''; ?>"
                                    data-location="<?php echo isset($value->location_id) ? $value->location_id : ''; ?>"
                                    data-hospital_name="<?php echo isset($value->hospital_name) ? $value->hospital_name : ''; ?>"
                                    data-email="<?php echo isset($value->email) ? $value->email : ''; ?>"
                                    data-phone="<?php echo isset($value->phone) ? $value->phone : ''; ?>"

                                    data-bank_name="<?php echo isset($value->bank_name) ? $value->bank_name : ''; ?>"

                                    data-bank_account="<?php echo isset($value->bank_account) ? $value->bank_account : ''; ?>"
                                     
                                    data-logo_img="<?php echo isset($value->logo_img) ? $value->logo_img : ''; ?>"





                                        ><span class="fa fa-pencil"></span></button>


                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="View Details"><a href="<?php echo base_url(); ?>admin/Hospital/details/<?php echo $value->hospital_id; ?>"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"><span class="fa fa-arrow-circle-right"></span></button></a></span>

                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                        <button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal2" data-record-id="<?php echo isset($value->hospital_id) ? $value->hospital_id : ''; ?>"><span class="fa fa-trash"></span></button>
                                    </span>  
                                </td>

                                <td><?php echo isset($value->hospital_id) ? "HCP".str_pad($value->hospital_id, 3, '0', STR_PAD_LEFT) : ''; ?> </td>

                                <td><?php echo isset($value->hospital_name) ? $value->hospital_name : ''; ?> </td>

                                <td><?php echo isset($value->phone) ? $value->phone : ''; ?> </td>

                                <td><?php echo isset($bands[0]->band_name) ? $bands[0]->band_name : ''; ?></td> 

                                <td><?php echo isset($location[0]->location_name) ? $location[0]->location_name : ''; ?></td> 
                                
                              <!--  <td><?php echo isset($value->loc_id) ? strtoupper($value->loc_id) : ''; ?></td> -->
                                <td><?php echo isset($value->bank_name) ? strtoupper($value->bank_name) : ''; ?></td> 
                                <td><?php echo isset($value->bank_account) ? strtoupper($value->bank_account) : ''; ?></td> 
                                
                                <!-- <td><?php echo isset( $value->tarrif) ?  ' ₦ '. $value->tarrif : ''; ?></td>  

                                <td>
                                    <?php if (isset($value->logo_img) and !empty($value->logo_img)): ?>
                                        <?php
                                            echo '<a style="cursor:pointer" download href=" '.base_url().'uploads/hospital/logo/'.$value->logo_img.' ">View </a>';
                                        ?>
                                    <?php endif ?> 
                                </td>-->
                                
                               
                            </tr> 

                            <?php 
                        }
                    }
                    ?>
                </tbody>


        </table>

    </div>

  </div>

</div>
<?php }else{
    redirect('admin/dashboard','refresh');
} ?> 

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
            dom: 'lBfrtip',
            "buttons": ['csv', 'excel', 'pdf', 'print']
        });

        $("#xin-form2").submit(function(e){
            var fd = new FormData(this);
            var obj = $(this), action = obj.attr('name');
            fd.append("is_ajax", 1);
            fd.append("add_type", 'training');
            fd.append("form", action);
            e.preventDefault();
            $('.icon-spinner3').show();
            $('.save').prop('disabled', true);
            $.ajax({
                url: e.target.action,
                type: "POST",
                data:  fd,
                contentType: false,
                cache: false,
                processData:false,
                success: function(JSON)
                {
                    if (JSON.error != '') {
                        toastr.error(JSON.error);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                            $('.save').prop('disabled', false);
                            $('.icon-spinner3').hide();
                    } else {
                        toastr.success(JSON.result);
                        setTimeout(function(){
                           window.location.reload(1);
                        }, 5000);
                        $('.icon-spinner3').hide();
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                        $('#xin-form2')[0].reset(); // To reset form fields
                        $('.add-form').removeClass('in');
                        $('.select2-selection__rendered').html('--Select--');
                        $('.save').prop('disabled', false);
                    }
                },
                error: function() 
                {
                    toastr.error(JSON.error);
                    $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                    $('.icon-spinner3').hide();
                    $('.save').prop('disabled', false);
                }           
           });
        });


        $("#delete_record2").submit(function(e){
            /*Form Submit*/
            e.preventDefault();
            var obj = $(this), action = obj.attr('name');
            $.ajax({
                type: "POST",
                url: e.target.action,
                data: obj.serialize()+"&is_ajax=2&form="+action,
                cache: false,
                success: function (JSON) {
                    if (JSON.error != '') {
                        toastr.error(JSON.error);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                    } else {
                        $('.delete-modal2').modal('toggle');
                         
                        toastr.success(JSON.result);
                        setTimeout(function(){
                           window.location.reload(1);
                        }, 5000);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);                 
                    }
                }
            });
        });




             /* Edit data */
        $("#edit_ticket2").submit(function(e){
            var fd = new FormData(this);
            var obj = $(this), action = obj.attr('name');
            fd.append("is_ajax", 1);
            fd.append("edit_type", 'ticket');
            fd.append("form", action);
            e.preventDefault();
            $('.icon-spinner3').show();
            $('.save').prop('disabled', true);
            $.ajax({
                url: e.target.action,
                type: "POST",
                data:  fd,
                contentType: false,
                cache: false,
                processData:false,
                success: function(JSON)
                {
                    if (JSON.error != '') {
                        toastr.error(JSON.error);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                            $('.save').prop('disabled', false);
                            $('.icon-spinner3').hide();
                    } else {
                        // On page load: datatable
                        // var xin_table = $('#xin_table').dataTable({
                        //     "bDestroy": true,
                        //     "ajax": {
                        //         url : "https://hmo.liontech.com.ng/admin/tickets/ticket_list",
                        //         type : 'GET'
                        //     },
                        //     "fnDrawCallback": function(settings){
                        //     $('[data-toggle="tooltip"]').tooltip();          
                        //     }
                        // });
                        
                            toastr.success(JSON.result);

                            setTimeout(function(){
                               window.location.reload(1);
                            }, 4000);
                        
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                        $('.icon-spinner3').hide();
                        $('.edit-modal-data2').modal('toggle');
                        $('.save').prop('disabled', false);
                    }
                },
                error: function() 
                {
                    toastr.error(JSON.error);
                    $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                    $('.icon-spinner3').hide();
                    $('.save').prop('disabled', false);
                }           
            });
        });
        




        $('.edit_detect').on('click',function(){
            console.log('clicked');
            $('#edit_ticket2').find("input[type=text], textarea").val(""); 

            var band_id          = $(this).attr('data-band_id');
            var location         = $(this).attr('data-location');
            var hospital_name    = $(this).attr('data-hospital_name');
            var email            = $(this).attr('data-email');
            var phone            = $(this).attr('data-phone');
            var bank_name            = $(this).attr('data-bank_name');
            var bank_account            = $(this).attr('data-bank_account');
            // var tarrif           = $(this).attr('data-tarrif');
            var logo_img         = $(this).attr('data-logo_img');
            var id               = $(this).attr('data-ticket_id');
 
            console.log(hospital_name);

            $('#hospital_id').val(id); 
            $('#band_id').val(band_id).trigger('change');
            $('#location_se').val(location).trigger('change');
            $('#hospital_name').val(hospital_name);
            $('#email').val(email);
            $('#phone').val(phone);
            $('#bank_name').val(bank_name);
            $('#bank_account').val(bank_account); 
            $('#logo_img').attr('src','<?php echo base_url(); ?>uploads/hospital/logo/' +logo_img);
        })




    }, false);

     
</script>


<div class="modal fade delete-modal2 animated in" role="dialog" aria-hidden="true"  >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"> 
                <button type="button" aria-label="Close" data-dismiss="modal" class="close"><span aria-hidden="true">×</span></button>
                <strong class="modal-title">Are you sure you want to delete this record?</strong> 
            </div>
            <div class="alert alert-danger">
                <strong>Record deleted can't be restored!!!</strong>
            </div>
            <form action="<?php echo base_url(); ?>admin/Hospital/delete_hospital" name="delete_record" id="delete_record2" autocomplete="off" role="form" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="">
                <input type="hidden" name="csrf_hrsale" value="288e255c59f6126650e28264e712db77">                                                                                                                         
 
                <div class="modal-footer">
        
                    <input type="hidden" name="token_type" value="0" id="token_type">
            
                    <button type="button" data-dismiss="modal" class="btn btn-secondary"><i class="fa fa fa-check-square-o"></i> Close</button>
     
                    <button name="hrsale_form" type="submit" class="btn btn-primary"><i class="fa fa fa-check-square-o"></i> Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>









<!-- edit -->

<div class="modal fade edit-modal-data2 animated in" id="edit-modal-data" role="dialog" aria-labelledby="edit-modal-data" aria-hidden="true"  >
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="ajax_modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                <h4 class="modal-title" id="edit-modal-data">Edit Hospital</h4>
            </div>

            <form action="<?php echo base_url(); ?>admin/Hospital/update_hospital" name="edit_ticket" id="edit_ticket2" autocomplete="off" class="m-b-1" method="post" accept-charset="utf-8">
                <input type="hidden" name="_method" value="EDIT">
                <input type="hidden" name="_token" value="">
                <input type="hidden" name="hospital_id" id="hospital_id" value="">
                <input type="hidden" name="ext_name" value="1">
                <input type="hidden" name="csrf_hrsale" value="95edc3390e40ab2fc8a739f96b5ee8aa">
                <div class="modal-body">
                    <div class="box-block"> 
                        <div class="row"> 
                            <div class="col-md-6"> 
                                <div class="row"> 
                                    <div class="col-md-12">  
                                        <div class="form-group"> 
                                            <label for="company_name">Hospital Name</label> 
                                            <input class="form-control" required="" placeholder="Hospital Name" id="hospital_name" name="hospital_name" type="text" value=""> 
                                        </div>  
                                    </div> 
                                </div>   
                            </div> 

                            <div class="col-md-6"> 
                                <div class="form-group"> 
                                    <label for="training_type">Location</label>

                                    <select class="form-control" name="location_id" data-plugin="select_hrm" data-placeholder="Location" id="location_se"> 
                                        <option value=""></option> 
                                        <?php if (isset($all_locations) and !empty($all_locations)): ?>
                                            <?php foreach ($all_locations as  $value): ?>
                                                <?php echo "<option value='".$value->location_id."' >".$value->location_name."</option> " ?>
                                            <?php endforeach ?>
                                            
                                        <?php endif ?>
                                    </select> 
                                </div> 
                            </div> 
                        </div>


                       

                        <div class="row"> 
                            <div class="col-md-6"> 
                                <div class="form-group"> 
                                    <label for="training_type">Band Type</label>

                                    <select class="form-control" name="band_id" data-plugin="select_hrm" data-placeholder="Band Type" id="band_id"> 
                                        <option value=""></option> 
                                        <?php if (isset($all_bands) and !empty($all_bands)): ?>
                                            <?php foreach ($all_bands as $value): ?>
                                                <?php echo "<option value='".$value->band_id."' >".$value->band_name."</option> " ?>
                                            <?php endforeach ?> 
                                        <?php endif ?>
                                    </select> 
                                </div> 
                            </div>  

                            <div class="col-md-6"> 
                                <div class="row"> 
                                    <div class="col-md-12">  
                                        <div class="form-group"> 
                                            <label for="company_name">Email </label> 
                                            <input class="form-control" required="" placeholder="Email " name="email" id="email" type="email" value=""> 
                                        </div>  
                                    </div> 
                                </div>   
                            </div> 

                        </div>
                        <div class="row"> 
                            
                           
                            <div class="col-md-6"> 
                                <div class="row"> 
                                    <div class="col-md-12">  
                                        <div class="form-group"> 
                                            <label for="company_name">Phone </label> 
                                            <input class="form-control" required="" placeholder="Phone" id="phone" name="phone" type="text" value=""> 
                                        </div>  
                                    </div> 
                                </div>   
                            </div> 
                             
                            <div class="col-md-6"> 
                                <div class="form-group"> 
                                    <label for="training_type">Logo</label>
                                    <img src="" id="logo_img">
                                    <input type="file" class="form-control-file" id="logo" name="logo">
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6"> 
                                <div class="row"> 
                                    <div class="col-md-12">  
                                        <div class="form-group"> 
                                            <label for="company_name">Bank Name </label> 
                                            <input class="form-control" id="bank_name" required="" placeholder="Bank Name " name="bank_name" type="text" value=""> 
                                        </div>  
                                    </div> 
                                </div>   
                            </div>
                            <div class="col-md-6"> 
                                <div class="row"> 
                                    <div class="col-md-12">  
                                        <div class="form-group"> 
                                            <label for="company_name">Bank Account Number</label> 
                                            <input class="form-control" id="bank_account" required="" placeholder="Bank Account Number " name="bank_account" type="text" value=""> 
                                        </div>  
                                    </div> 
                                </div>   
                            </div>
                        </div>
 

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form> 
        </div>
    </div>
</div>