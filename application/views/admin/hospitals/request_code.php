<?php

/* Subscription view

*/
 
?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>
<?php

      $role_user = $this->Xin_model->read_user_role_info($user_info[0]->user_role_id);

      if(!is_null($role_user)){

        $role_resources_ids = explode(',',$role_user[0]->role_resources);

      } else {

        $role_resources_ids = explode(',',0); 

      }

?>


<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box" id="diagnose_client_form">

        <div class="box-body">

            <div class="card-block">
              <?php echo form_open_multipart('admin/Hospital/insert_clients_diagnose_admin');?>
                  
                    <div class="form-body">

                        <div class="row">
                            <div class="col-md-12">
                                <?php if ($this->session->flashdata('success')): ?>
                                    <div class="alert alert-success">
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                <?php endif ?>


                                <?php if ($this->session->flashdata('error')): ?>
                                    <div class="alert alert-warning">
                                        <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                <?php endif ?>
                            </div>
                              
                            <!-- Hospital List -->
                              <div class="col-md-11" style="width: 98.5%;">
                                <div class="form-group">

                                  <label for="company_name">Search list of Providers</label>
                                  <select id="select_hospital" class="form-control col-md-6 select2" name="hospital" data-plugin="xin_select">
                                     <option>Select Provider</option> 
                        <?php   
                            $query = "";
                            $hospital = $this->session->userdata;

                            if(!empty($hospitals))
                            { 
                               foreach ($hospitals as $key => $value) {
                            ?>
                                <option value="<?php echo $value->hospital_id ?>"><?php echo $value->hospital_name ?></option>
                        <?php
                               }
                            }
                        ?> 
                                  </select>
                                </div>
                            </div>
                            <!-- Client List -->
                            <div class="col-md-11" style="width: 98.5%;">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label for="company_name">Search Enrollee</label>
                                            <select id="select_client" class="form-control col-md-6 select2" name="diagnose_client" data-plugin="xin_select">
                                                <option>Select Enrollee</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <img id="profile-img" src="<?php echo base_url().'uploads/profile/noimage.jpg'; ?>" alt="img" style="width: 115px;height: 85px;border-radius: 50%;display: none;">
                                    </div>
                                </div>

                            </div>

                            <!-- Client List -->
                            <div class="col-md-11" style="width: 98.5%;">
                                <span id="plan_name" class="text-primary"></span>
                            </div>
                            <!-- <div class="col-md-6">

                                <div class="form-group">

                                  <label for="company_name">Enrollee ID</label>
                                  <input class="form-control" name="diagnose_client_id" placeholder="Client ID"   type="text" value="">

                                </div>
                            </div> -->
                             <div class="col-md-6 form-group">

                                <label for="last_name">Date</label>

                                <input class="form-control" name="diagnose_date" type="date" value="<?php echo Date('Y-m-d'); ?>" readonly>

                            </div>
                            <div class="col-md-6 form-group">

                                <label for="last_name">Date Of Birth</label>

                                <input class="form-control" id="date" name="diagnose_date_of_birth" type="date" value="" readonly>

                            </div>
                            <div class="col-md-6 form-group" style="width: 49.3%;">
                                <div class="row" id="ser_div1">
                                    <div class="col-md-8">
                                        <label for="last_name">Select Services | Procedure | Investigation</label>
                                        <select id="select_service" class="form-control select2" name="diagnose_services[]" data-plugin="select_hrm" data-placeholder="Select Services" > 
                                            <option value="">Select Services</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
	                                <label for="last_name">Quantity</label>
	                                <input type="number" name="service_quantity[]" id="quantity1" class="form-control" min="1" value="1">
	                            </div>
	                            <div class="col-md-2">
	                                <label for="last_name" style="padding-top: 40px;" class="padding"></label>
	                                <input type="button" name="addmore" id="addmore_ser" onclick="return getaddmoreService(2)" value="+" class="btn btn-success">
	                            </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6 form-group pull-right" style="width: 550px;">
                                   <div class="row" id="drug_div1">
                                    <div class="col-md-8">
                                         <label for="last_name">Select Drugs</label>
                                            <select id="select_drug" class="form-control select2" name="diagnose_drugs[]" data-plugin="select_hrm" data-placeholder="Select Drugs" >
                                                <option value="">Select Drugs</option>
                                            </select>
                                    </div>
                                    <div class="col-md-2">
	                                <label for="last_name">Quantity</label>
	                                <input type="number" name="quantity[]" id="quantity1" class="form-control" min="1" value="1">
	                            </div>
	                            <div class="col-md-2">
	                                <label for="last_name" style="padding-top: 40px;" class="padding"></label>
	                                <input type="button" name="addmore" id="addmore" onclick="return getaddmore(2)" value="+" class="btn btn-success">
	                            </div>
                                  </div>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="last_name">Diagnosis</label>
                                <textarea class="form-control" placeholder="Diagnosis" rows="4" name="diagnose_diagnose" type="text"></textarea>
                            </div>
                             <div class="col-md-6 form-group">
                                <label for="last_name">Presenting Complaint</label>
                                <textarea class="form-control" placeholder="Presenting Complaint" rows="4" name="diagnose_procedure" type="text"></textarea>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="last_name">Clinical Findings</label>
                                <textarea class="form-control" placeholder="Clinical Findings" rows="4" name="diagnose_investigation" type="text"></textarea>
                            </div>
                           
                             <div class="col-md-6 form-group">
                                <label for="last_name">Medical Personnel</label>
                                <textarea class="form-control" placeholder="Medical Personnel" rows="4" name="diagnose_medical" type="text"></textarea>
                            </div>
                            <input type="hidden" id="dateTime" name="datetime" >
                    </div>



                    <?php if (empty($hospital_id)): ?> 
                    <div class="form-actions"> <?php echo form_button(array('name' => 'hrsale_form', 'type' => 'submit', 'class' => "pull-right ".$this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '."Create Encounter")); ?> 
                    </div>
                    <?php endif ?>

            </div>
                <?php echo form_close(); ?> 

        </div>
        </div>
        </div>

<script type="text/javascript">
  window.onload = function(){
    var dateTime = document.getElementById('dateTime');
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    dateTime.value = date+' '+time;
  }
    $(document).ready(function() {
        $("#select_client").change(function(e) {
            e.preventDefault();
            var x=$("#select_client").val();
            var y = x.split(':');
            if (y.length < 3) {
                console.log('Length <3');
                window.capitation = 2;
                $.ajax({
                    url      : '<?php echo base_url(); ?>hospital/Clients/get_dob',
                    method   : 'post',
                    dataType    : 'text',
                    data     : {id : x},
                    success  : function(response){
                        var object=JSON.parse(response);
                        console.log(object);
                        $("#date").val(object.dob);
                        if(object.img==null){
                            $("#profile-img").show();
                        }
                        else{
                            var img=object.img;
                            $("#profile-img").attr('src',img);
                            $("#profile-img").show();
                        }
                        // $("#id_we_get_is").text(response);
                        // alert(response);
                        // console.log(response);
                        // console.log(response.substr(0,4));
                        // console.log(x)
                        window.dob = response.substr(0,4);
                        // $("#fetched_data").html(response);
                    }
                });

                var cid = $("#client").val();

                // console.log(cid);

                $.ajax({
                    url      : '<?php echo base_url(); ?>admin/Hospital/fetch_client_plan',
                    method   : 'post',
                    dataType    : 'text',
                    data     : {id : cid},
                    success  : function(response){
                        // $("#id_we_get_is").text(response);
                        // alert(response);
                        $("#plan_name").text(response);
                        //console.log(response);

                    }
                });
            }
        })
    })
//    function change(){
//
//        var x = document.getElementById("select_client").value;
//        var y = x.split(':');
//
//        if (y.length < 3) {
//            console.log('Length <3');
//            window.capitation = 2;
//            document.getElementById("date").value = "";
//            document.getElementById("correct").style.display = 'none';
//            document.getElementById("wrong").style.display = 'none';
//            $.ajax({
//                url      : '<?php //echo base_url(); ?>//hospital/Clients/get_dob',
//                method   : 'post',
//                dataType    : 'text',
//                data     : {id : x},
//                success  : function(response){
//                    var object=JSON.parse(response);
//                    console.log(object);
//                    $("#date").val(object.dob);
//                    if(object.img==null){
//                        $("#profile-img").show();
//                    }
//                    else{
//                        var img="<?php //echo base_url().'uploads/profile/'; ?>//"+object.img;
//                        $("#profile-img").attr('src',img);
//                    }
//                    // $("#id_we_get_is").text(response);
//                    // alert(response);
//                    // console.log(response);
//                    // console.log(response.substr(0,4));
//                    // console.log(x)
//                    window.dob = response.substr(0,4);
//                    // $("#fetched_data").html(response);
//                }
//            });
//
//            var cid = $("#client").val();
//
//            // console.log(cid);
//
//            $.ajax({
//                url      : '<?php //echo base_url(); ?>//admin/Hospital/fetch_client_plan',
//                method   : 'post',
//                dataType    : 'text',
//                data     : {id : cid},
//                success  : function(response){
//                    // $("#id_we_get_is").text(response);
//                    // alert(response);
//                    $("#plan_name").text(response);
//                    //console.log(response);
//
//                }
//            });
//        }else{
//            document.getElementById("date").value = "";
//            document.getElementById("correct").style.display = 'none';
//            document.getElementById("wrong").style.display = 'none';
//            //console.log('Length >3');
//            window.capitation = 1;
//            $.ajax({
//                url      : '<?php //echo base_url(); ?>//admin/Hospital/fetch_client_plan',
//                method   : 'post',
//                dataType    : 'text',
//                data     : {id : cid},
//                success  : function(response){
//                    // $("#id_we_get_is").text(response);
//                    // alert(response);
//                    $("#plan_name").text(response);
//                    // console.log(response);
//
//                }
//            });
//        }
//        // console.log(capitation);
//    }
      function loadModalView(id){
          // alert("ID is: " + id);

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#fetched_data").html(response);
            }
          });

      }

      function loadReasonModal(id,hid,cid){
          // alert("ID is: " + id);

          $("#that_reason").val("");
          $("#bdid").val(id);
          $("#bhid").val(hid);
          $("#bcid").val(cid);
      }

      function loadNoteModal(id,hid,cid){
          // alert("ID is: " + id);

          $("#that_note").val("");
          $("#nbdid").val(id);
          $("#nbhid").val(hid);
          $("#nbcid").val(cid);
      }

      function loadViewNoteModal(id,type) {
          // alert(id+ " " + type);

          // if(type == 3) { $("#rtitle").text("Note") }

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_reason',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id, type : type},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#fetched_note").html(response);
            }
          });
      }
      
       function getaddmoreService(value)
    {
    	var x=$("#select_hospital").val();
        var value1 = value -1;
        var value2 = value + 1;

        $.ajax({
            url      : '<?php echo base_url(); ?>admin/Hospital/addmoreservicedive',
            method   : 'post',
            dataType    : 'text',
            data     : {value : value,hid:x},
            success  : function(response){
                console.log(response);
                $("#addmore_ser").attr("onclick","return getaddmoreService("+value2+")");

                $("#ser_div"+value1).after(response);
                $('#select_service'+value).select2();
            }
        });

    }
    function getaddmore(value)
    {
    	var x=$("#select_hospital").val();
        var value1 = value -1;
        var value2 = value + 1;

        $.ajax({
            url      : '<?php echo base_url(); ?>admin/Hospital/addmoredive',
            method   : 'post',
            dataType    : 'text',
            data     : {value : value,hid:x},
            success  : function(response){
                console.log(response);
                $("#addmore").attr("onclick","return getaddmore("+value2+")");

                $("#drug_div"+value1).after(response);
                $('#drugs'+value).select2();
            }
        });

    }
    function removeSerdiv(values){
        $('#ser_div'+values).hide();
    }
    function removediv(values){
        $('#drug_div'+values).hide();
    }
      
    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable(); 
    }, false);

     
</script>
 






 