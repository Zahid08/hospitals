<?php

/* Subscription view

*/
 
?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php if(in_array('700',$role_resources_ids)) {?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>
<?php

      $role_user = $this->Xin_model->read_user_role_info($user_info[0]->user_role_id);

      if(!is_null($role_user)){

        $role_resources_ids = explode(',',$role_user[0]->role_resources);

      } else {

        $role_resources_ids = explode(',',0); 

      }

?>

<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

  <div class="box-header with-border">

   
    <h3 class="box-title"> Filter Data by Date <?php //echo $query; ?> </h3>

  </div>

  <div class="box-body">

    <?php echo form_open('admin/Hospital/admission_request');?>

    <div class="row">

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-2">

        <div class="form-group">

          <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>
        </div>

      </div>

    </div>

    <?php echo form_close(); ?> </div>
</div>

<?php } ?>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">
  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>

    <div class="box-datatable table-responsive">

        <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

            <thead>

                <tr> 
                  <?php if(in_array('701',$role_resources_ids)) { ?>
                 <th width="22%"><?php echo $this->lang->line('xin_action');?></th>
                  <?php } ?>
                    <th>App or Rej by</th>
                    <th>Auth. Code</th>
                    <th>Provider</th>
                    <th width="3%">Days</th>
                    <th>Date</th>
                    <th>Bill</th>
                </tr>

                <tbody> 
                    <?php   
                    if(!empty($xin_diagnose_clients))
                    { 
                        foreach ($xin_diagnose_clients as $key => $value)
                        {              

                            //print_r($value);
							/* if ($value->diagnose_status == '2' || $value->diagnose_status == '3' || $value->diagnose_status == '4') {
                              continue;
                            } */
                             
                            $ci=& get_instance();
                            $ci->load->model('Clients_model'); 

                            $services = $ci->Clients_model->read_individual_hospital_diagnose_services($value->diagnose_id);
                            $drugs = $ci->Clients_model->read_individual_hospital_diagnose_drugs($value->diagnose_id);
                            $admin_info = $ci->Clients_model->read_individual_admin_info($value->admission_status_approve_by);
                                
                            ?>  
                            <tr>
                              <?php if(in_array('701',$role_resources_ids)) { ?>
                                <td>
                                    <a class="btn btn-default" data-toggle="modal" data-target="#myModal" onclick="return loadModalView(<?php echo $value->diagnose_id; ?>);"><i class="fa fa-eye"></i></a>
                                    <?php /* if ($value->diagnose_status == '2'){ ?>
                                        <p class="btn btn-success">Code Generated</p>
                                    <?php }else if(($value->diagnose_status == '3') || ($value->diagnose_status == '4')) { ?>
                                        <p class="btn btn-danger"> Rejected</p> 
                                    <?php }else {  */?>
                                        <a href="<?php echo base_url(); ?>admin/Hospital/admission_request?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&approve=yes&id=<?php echo $value->diagnose_id; ?>"  class="btn btn-info">Approve</a>

                                        <a data-toggle="modal" data-target="#reasonModal" onclick="return loadReasonModal(<?php echo $value->diagnose_id.",".$value->diagnose_hospital_id.",".$value->diagnose_client_id; ?>);" class="btn btn-danger">Reject</a>
                                        <!-- <a href="<?php echo base_url(); ?>admin/Hospital/diagnose_hospital_clients_requests?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&reject=yes&id=<?php echo $value->diagnose_id; ?>" class="btn btn-warning">Reject</a> -->

                                    <?php //} ?>
                                </td>
                              <?php } ?>
                                <td><?php if($admin_info) echo $admin_info->first_name." ".$admin_info->last_name; else echo "Pending Approval"; ?></td>
                                <?php
                                  $hcp = preg_replace('/\s+/', '', $value->hospital_name);
                                  $date = date("Ymd", strtotime($value->diagnose_date_time));
                                ?>
                                <td>
                                  <?php 
                                    if(empty($value->diagnose_generated_code)) echo "-----"; 
                                    else echo "P-".$value->loc_id."-".$date."-".$value->diagnose_generated_code;
                                  ?>    
                                </td>
                                <td><?php if(empty($value->hospital_name)) echo "-----"; else echo $value->hospital_name; ?></td>
                               	<td><select class="form-control" onchange="saveDayValue(this.value, <?php echo $value->diagnose_id; ?>)">
                               		<?php echo '<option selected>'.$value->days.'</option>'; 
                               				for ($i=1; $i <= 31 ; $i++) { 
                               					echo '<option value="'.$i.'">'.$i.'</option>';
                               				}

                               		?>
                               	</select></td>
                               	
                                  <td><?php echo  date("Y-m-d h:i A", strtotime($value->diagnose_date_time)); ?></td>
                                  <td>₦<?php echo number_format($value->diagnose_total_sum).".00"; ?></td>
                            </tr> 

                            <?php 
                        }
                    }
                    ?>
                </tbody>

            </thead>

        </table>

    </div>

  </div>

</div>
 

<script type="text/javascript">

	function saveDayValue(value,id){
     $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/update_days_request',
            method   : 'post',
            dataType    : 'json',         
            data     : {id : id , value : value},
            success  : function(response){
              console.log(response);
              alert('Admission day(s) updated successfully');
            },error(){
              
            }
          });
	}
      function loadModalView(id){
          // alert("ID is: " + id);

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
			  $("#admissioncase").attr('onclick','return markasadmissioncase('+id+');');
              $("#fetched_data").html(response);
            }
          });

      }

      //line total
  function cal_line_sum(id,addId,digId){

    $('#id_line_cal_'+id).text(((parseFloat($('#idserve_'+id).val())*parseFloat($('#serv_price_'+id).val()))).toFixed(2));
    
    var tot = 0;
    var totori = 0;
    for(var i= 0;i<$('#serv_count').val();i++){
      tot += (parseFloat($('#idserve_'+i).val())*parseFloat($('#serv_price_'+i).val()));
      totori += (parseFloat($('#idserve_orig_'+i).val())*parseFloat($('#serv_price_'+i).val()));

    }
    
     var alltot = parseFloat($('#totalBill').val());
     console.log(alltot+'--'+totori+'---'+tot);
     $('#totalBillText').text(((alltot-totori)+tot).toFixed(2));

     update_service_qty(id,addId,digId);

  }
  //updating service qty
  function update_service_qty(id,addId,digId){

    var idserve = $('#idserve_'+id).val();
    var serv_price = $('#serv_price_'+id).val();

    $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/update_service_qty',
            method   : 'post',
            data     : {
                        qty : idserve,
                        digId : digId
                        },
            success  : function(response){
              console.log(response);
              alert('updated successfully');
            },error(){
              
            }
          });
  }

  //line total
  function cal_green_line_sum(id,addId){

// $('#id_line_cal_'+id).text(((parseFloat($('#idserve_'+id).val())*parseFloat($('#serv_price_'+id).val()))).toFixed(2));

var tot = 0;
var totori = 0;
var fullnames = "";
var fullQty = "";
for(var i= 0;i<$('#serv_green_count').val();i++){
  // tot += (parseFloat($('#idserve_'+i).val())*parseFloat($('#serv_price_'+i).val()));
  // totori += (parseFloat($('#idserve_orig_'+i).val())*parseFloat($('#serv_price_'+i).val()));

      var name = $('#serv_green_value_'+i).val();
      var qty = $('#idserve_green_'+i).val();
      var price = $('#serv_green_price_'+i).val();
      var string = qty+'_'+qty;
      fullnames += name+',';
      fullQty += string+',';



}

 update_service_green_qty(addId,fullnames,fullQty);

}

function update_service_green_qty(id,fullnames,fullQty){

$.ajax({
        url      : '<?php echo base_url(); ?>hospital/Clients/update_service_green_qty',
        method   : 'post',
        data     : {
                    id : id,
                    fullQty : fullQty,
                    fullnames : fullnames
                    },
        success  : function(response){
          console.log(response);
          alert('updated successfully');
        },error(){
          
        }
      });
}

	function markasadmissioncase(ids)
	{
		
	}	
      function loadReasonModal(id,hid,cid){
          // alert("ID is: " + id);

          $("#that_reason").val("");
          $("#did").val(id);
          $("#hid").val(hid);
          $("#cid").val(cid);

          // $.ajax({
          //   url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id',
          //   method   : 'post',   
          //   dataType    : 'text',      
          //   data     : {id : id},
          //   success  : function(response){
          //     // $("#id_we_get_is").text(response);
          //     // alert(response);
          //     $("#fetched_data").html(response);
          //   }
          // });

      }
      
      
      function loadDrugReasonModal(drug_did,drug_hid,drug_cid,drug_mid){
     //alert(drug_did);
	  $("#drug_reasonModal").on("show.bs.modal", function(e) {
   	    $("#drug_did").val(drug_did);
            $("#drug_hid").val(drug_hid);
            $("#drug_cid").val(drug_cid);
            $("#drug_mid").val(drug_mid);
	  });
         // $("#that_drugreason").val("");
          

          

      }
      
      function loadServiceReasonModal(service_did,service_hid,service_mid,service_sid){
	  //alert(service_mid);
          //$("#that_service_reason").val("");
	  $("#service_reasonModal").on("show.bs.modal", function(e) {
          $("#service_did").val(service_did);
          $("#service_hid").val(service_hid);
          $("#service_mid").val(service_mid);
          $("#service_sid").val(service_sid);
	  });         

      }
      
    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
          buttons: ['csv', 'excel', 'pdf', 'print'],
          "order": [[ 5, "desc" ]]
        }); 
    }, false);

     
</script>
 






 