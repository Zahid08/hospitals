<?php
/* Subscription view
*/

?>
<?php $session = $this->session->userdata('username');?>
<?php $get_animate = $this->Xin_model->get_content_animate();?>
<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>
<?php if(in_array('502',$role_resources_ids) || in_array('534',$role_resources_ids) || in_array('543',$role_resources_ids) || in_array('544',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>
<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>
<style type="text/css">
.fa{
cursor: pointer;
}
</style>
<div class="box mb-4 <?php echo $get_animate;?>">
    
</div>
<?php } ?>
<div class="box mb-4 <?php echo $get_animate;?>">

  <div class="box-header with-border">

   
    <h3 class="box-title"> Filter Data by Date <?php //echo $query; ?> </h3>

  </div>

  <div class="box-body">

    <?php echo form_open('admin/Hospital/new_enrollee_requests');?>

    <div class="row">

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>" required>

        </div>

      </div>

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>" required>

        </div>

      </div>

      <div class="col-md-2">

        <div class="form-group">

          <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>
        </div>

      </div>

    </div>

    <?php echo form_close(); ?> </div>
</div>
<div class="box <?php echo $get_animate;?>">
    <div class="box-header with-border">
    </div>
    <div class="box-body">
        <?php if ($this->session->flashdata('success')): ?>
        <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif ?>
        <div class="box-datatable table-responsive">
            <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">
                <thead>
                    <tr>
                        <?php if (in_array('502',$role_resources_ids) || in_array('534',$role_resources_ids) || in_array('543',$role_resources_ids) || in_array('545',$role_resources_ids) || in_array('546',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
                        <th width="17%"><?php echo $this->lang->line('xin_action');?></th>
                            
                        <?php endif ?>
                        <th>Enrollee Name</th>
                        <th>Email Address</th>
                        <th>Phone Number</th>
                        <th>Status</th>
                        <th>Enrollment Date</th>
                    </tr>
                    <tbody>
                        <?php
                        if(!empty($all_requests))
                        {
                            // print_r($all_requests->result());die;
                        $ci=& get_instance();
                        $ci->load->model('Training_model');
                        $detail_pkg_edit = '';
                        $detail_pkg = '';
                        $detail = $ci->Training_model->getAll2('xin_subscription','');
                        if (!empty($detail)) {
                        foreach ($detail as $benifit) {
                        $detail_pkg .= '<p>  ' . $benifit->plan_name . '</p>';
                        $detail_pkg_edit .= '--------' . $benifit->subscription_id . '';
                        }
                        }
                        // print_r($detail_pkg_edit);die;
                        foreach ($all_requests->result() as $key => $value)
                        {
                        
                        ?>
                        <tr>
                            <?php if (in_array('502',$role_resources_ids) || in_array('534',$role_resources_ids) || in_array('543',$role_resources_ids) || in_array('545',$role_resources_ids) || in_array('546',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
                            <td>
                                <?php if($value->approve_code == '2') { ?>
                                <p class="btn btn-danger"> Rejected</p>
                                <?php }else { ?>
                                <?php if (in_array('502',$role_resources_ids) || in_array('534',$role_resources_ids) || in_array('543',$role_resources_ids) || in_array('545',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
                                <a href="<?php echo base_url(); ?>admin/Hospital/new_enrollee_requests?client_id=<?php echo $value->client_id; ?>&approve=yes"  class="btn btn-info">Approve</a>
                            
                                <?php endif ?>
                                <?php if (in_array('502',$role_resources_ids) || in_array('534',$role_resources_ids) || in_array('543',$role_resources_ids) || in_array('546',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
                                <a href="<?php echo base_url(); ?>admin/Hospital/new_enrollee_requests?client_id=<?php echo $value->client_id; ?>&reject=yes" class="btn btn-danger">Reject</a>
                            
                        <?php endif ?>
                                <?php } ?>
                            </td>
                            
                            <?php endif ?>
                            <td><?php echo isset($value->name) ? $value->name : ''; ?> </td>
                            <td><?php echo isset($value->email) ? $value->email : ''; ?> </td>
                            
                            <td><?php echo isset($value->contact_number) ? $value->contact_number : ''; ?> </td>
                            <td>
                                <?php echo "<span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" . $ci->lang->line('xin_edit') ."\"><button type=\"button\" class=\"btn icon-btn btn-xs btn-default waves-effect waves-light edit_client_detail \"  data-toggle=\"modal\" data-target=\".edit-modal-data2\"
                                data-client_id = \"".$value->client_id."\"
                                data-c_name    = \"".$value->name."\"
                                data-c_lname    = \"".$value->last_name."\"
                                data-c_oname    = \"".$value->other_name."\"
                                data-c_orga    = \"".$value->company_name."\"
                                data-c_hospital    = \"".$value->hospital_id."\"
                                data-c_subs    = \"".$value->subscription_ids."\"
                                data-email     = \"".$value->email."\"
                                data-phone     = \"".$value->contact_number."\"
                                data-status    = \"".$value->is_active."\"
                                data-gender    = \"".$value->sex."\"
                                data-address    = \"".$value->address_1."\"
                                data-dob    = \"".$value->dob."\"
                                data-marital    = \"".$value->marital_status."\"
                                data-diseases    = \"".$value->diseases."\"
                                data-comment    = \"".$value->disease_comment."\"
                                data-state    = \"".$value->state."\"
                                data-client_profile    = \"".$value->client_profile."\"
                                data-detail_pkg_edit    = \"" . $detail_pkg_edit . "\"
                                data-ind_family    = \"" . $value->ind_family . "\"
                                data-ve = \"view\">
                                <span class=\"fa fa-eye\"></span></button></span>"; ?>
                                <?php echo "<span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" . $ci->lang->line('xin_edit') ."\"><button type=\"button\" class=\"btn icon-btn btn-xs btn-default waves-effect waves-light edit_client_detail \"  data-toggle=\"modal\" data-target=\".edit-modal-data2\"
                                data-client_id = \"".$value->client_id."\"
                                data-c_name    = \"".$value->name."\"
                                data-c_lname    = \"".$value->last_name."\"
                                data-c_oname    = \"".$value->other_name."\"
                                data-c_orga    = \"".$value->company_name."\"
                                data-c_hospital    = \"".$value->hospital_id."\"
                                data-c_subs    = \"".$value->subscription_ids."\"
                                data-email     = \"".$value->email."\"
                                data-phone     = \"".$value->contact_number."\"
                                data-status    = \"".$value->is_active."\"
                                data-gender    = \"".$value->sex."\"
                                data-address    = \"".$value->address_1."\"
                                data-dob    = \"".$value->dob."\"
                                data-marital    = \"".$value->marital_status."\"
                                data-diseases    = \"".$value->diseases."\"
                                data-comment    = \"".$value->disease_comment."\"
                                data-state    = \"".$value->state."\"
                                data-client_profile    = \"".$value->client_profile."\"
                                data-detail_pkg_edit    = \"" . $detail_pkg_edit . "\"
                                data-ind_family    = \"" . $value->ind_family . "\"
                                data-ve = \"edit\">
                                <span class=\"fa fa-pencil\"></span></button></span>"; ?>
                                    
                                </td>
                                
                                <td><?php echo $value->created_at; ?></td>
                            </tr>
                            <?php
                            }
                            }
                            ?>
                        </tbody>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
          buttons: ['csv', 'excel', 'pdf', 'print']
        }); 
    }, false);
    
    </script>
    
    <div class="modal fadeInRight edit-modal-data2 animated" id="edit-modal-data" role="dialog" aria-labelledby="edit-modal-data" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" id="ajax_modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    <h4 class="modal-title" id="edit-modal-data"><i class="icon-pencil7"></i> Edit Enrollee</h4>
                </div>
                <form action="<?php echo base_url() ?>admin/hospital/new_client_update" name="edit_client" id="edit_client" autocomplete="off" class="m-b-1" method="post" accept-charset="utf-8">
                    <input type="hidden" name="_method" value="EDIT">
                    <input type="hidden" name="_token"  id="_token">
                    <input type="hidden" name="ext_name" value="Shaleena">
                    <input type="hidden" name="csrf_hrsale" value="467ba7724bc88bcd9af14ffeb6201eff">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">First Name</label>
                                    <input class="form-control" placeholder="Enrollee Name" name="name" type="text"  id="c_name">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">Last Name</label>
                                    <input class="form-control" placeholder="Enrollee Last Name" name="last_name" type="text"  id="c_lname">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">Other Name</label>
                                    <input class="form-control" placeholder="Enrollee Other Name" name="other_name" type="text"  id="c_oname">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">Gender</label>
                                    <select class="form-control" id="c_gender" name="gender" data-plugin="xin_select" data-placeholder="Select Gender">
                                        <option value="">Select Gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">DOB</label>
                                    <input class="form-control" placeholder="DOB" name="dob" type="date"  id="c_dob">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">Marital Status</label>
                                    <select class="form-control" id="c_marital" name="marital_status" data-plugin="xin_select" data-placeholder="Select Marital Status">
                                        <option value="">Select Status</option>
                                        <option value="single">Single</option>
                                        <option value="married">Married</option>
                                        <option value="divorced">Divorced</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">Organization Name</label>
                                    <select class="form-control" id="c_orga" name="company_name" data-plugin="xin_select" data-placeholder="Select Organization">
                                        <option value="">Select Organization</option>
                                        <?php foreach($all_organization as $organization) {?>
                                        <option value="<?php echo $organization->id;?>"> <?php echo $organization->name;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="contact_number">Phone Number</label>
                                    <input class="form-control" placeholder="Phone Number" name="contact_number" type="text" id="c_phone" value="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input class="form-control" id="c_email" placeholder="Email" name="email" type="email" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">Address</label>
                                    <input class="form-control" id="c_address" placeholder="address" name="address_1" type="text" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company_name">State</label>
                                    <select class="form-control" id="c_state" name="state" data-plugin="xin_select" data-placeholder="Select State">
                                        <option value="">Select State</option>
                                        <?php foreach($all_state as $state) {?>
                                            <option value="<?php echo $state->location_id;?>"> <?php echo $state->location_name;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subscription_ids">Select Healthcare Plan</label>
                                <select id="c_subs" class="form-control" name="subscription_ids[]" data-plugin="xin_select" data-placeholder="Select Healthcare Plan">
                                    <option value="">Select Healthcare Plan</option>
                                    <?php foreach($all_subscription as $subscription) {?>
                                    <option value="<?php echo $subscription->subscription_id;?>"> <?php echo $subscription->plan_name;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subscription_ids">Select Hospital</label>
                                <select id="c_hospital" class="form-control" name="hospital_id" data-plugin="xin_select" data-placeholder="Select Hospital">
                                    <option value="">Select Hospital</option>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="status" class="control-label">Status</label>
                                <select class="form-control  select2" name="status" id="c_status"  data-placeholder="Status" >
                                    <option value="0">Inactive</option>
                                    <option value="1" >Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            
                            <div class="form-group">
                                <label for="subscription_ids">Select Account type</label>
                                <select   class="form-control" id="ind_family" name="ind_family" data-plugin="xin_select" data-placeholder="Select Account type">
                                    <option value="">Select Account type</option>
                                    <option value="individual">Individual  </option>
                                    <option value="family">Family  </option>
                                    
                                </select>
                            </div>
                            
                        </div>
                        <div class="col-sm-6">
                            <fieldset class="form-group">
                                
                                
                                <label for="logo">Profile Photo</label>
                                <small id="upload-text">Upload files only: gif,png,jpg,jpeg</small>
                                <input type="file" class="form-control-file" id="c_client_profile" name="client_photo">
                                
                                <span class="avatar box-48 mr-0-5"> <img id="c_client_profile" class="user-image-hr46 ui-w-100 rounded-circle" alt="No photo"> </span>
                                
                            </fieldset>
                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label for="diseases">Diseases</label>

                                <div class="row disease">
                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Sickle Cell Disease" id="sickle">   Sickle Cell Disease    
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Kidney Disease" id="kidney">   Kidney Disease  
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Epilepsy" id="epilepsy">   Epilepsy
                                    </div>

                                    <div class="col-md-3 form-group" style="font-size: small;">
                                        <input type="checkbox" name="diseases[]" value="Cancer(Prostate-Cervical)" id="cancer">   Cancer(Prostate,Cervical)    
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Diaberes Mellitus" id="diabetes">   Diaberes Mellitus
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Asthma" id="asthma">   Asthma
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="HIV-AIDS" id="hiv">   HIV/AIDS  
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Surgeries" id="surgeries">   Surgeries
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Hypertension" id="hypertension">   Hypertension
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Cataract" id="cataract">   Cataract  
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Goiter" id="goiter">   Goiter  
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Peptic Ulcer" id="peptic">   Peptic Ulcer
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Hepatitis" id="hepatitis">   Hepatitis    
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Glaucoma" id="glaucoma">   Glaucoma  
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Tuberculosis" id="tuberculosis">   Tuberculosis
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <input type="checkbox" name="diseases[]" value="Hemorrhoids" id="hemorrhoids">   Hemorrhoids    
                                    </div>
                                    
                                </div>                                

                            </div> 
                    </div>

                    <div class="row">
                        <div  class="col-md-12">
                                <div class="form-group">
                                    <label>Disease Comment</label>
                                    <textarea class="form-control" rows="5" name="comment" id="comment"></textarea>
                                </div>
                            </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save" id="update">Update</button>
                </div>
            </form>
            <script type="text/javascript">
            document.addEventListener('DOMContentLoaded', function(){
            $('[data-plugin="select_hrm"]').select2($(this).attr('data-options'));
            $('[data-plugin="select_hrm"]').select2({ width:'100%' });
            /* Edit data */
            $("#edit_client").submit(function(e){
                var fd = new FormData(this);
                var obj = $(this), action = obj.attr('name');
                fd.append("is_ajax", 2);
                fd.append("edit_type", 'client');
                fd.append("form", action);
                e.preventDefault();
                $('.save').prop('disabled', true);
                $.ajax({
                    url: e.target.action,
                    type: "POST",
                    data:  fd,
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(JSON)
                    {
                        if (JSON.error != '') {
                            toastr.error(JSON.error);
                            $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                            $('.save').prop('disabled', false);
                        } else {
                    // On page load: datatable
                            var xin_table = $('#xin_table').dataTable({
                                "order": [[5,'desc']],
                                "bDestroy": true,
                                "ajax": {
                                url : "<?php  echo base_url(); ?>admin/clients/clients_list",
                                type : 'GET'
                                },
                                dom: 'lBfrtip',
                                "buttons": ['csv', 'excel', 'pdf', 'print'], // colvis > if needed
                                "fnDrawCallback": function(settings){
                                    $('[data-toggle="tooltip"]').tooltip();
                                }
                            });
                            xin_table.api().ajax.reload(function(){
                                toastr.success(JSON.result);
                            }, true);
                            $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                            $('.edit-modal-data2').modal('toggle');
                            $('.save').prop('disabled', false);
                        }
                    },
                    error: function()
                    {
                        toastr.error(JSON.error);
                        $('input[name="csrf_hrsale"]').val(JSON.csrf_hash);
                        $('.save').prop('disabled', false);
                    }
                });
            });


            function get_clients(n) {
              var id = n;
              $.ajax({
                url      : '<?php echo base_url(); ?>admin/Hospital/get_hospital_by_subs',
                method   : 'post',
                dataType    : 'text',
                data     : {id : id},
                success  : function(response){
                  // $("#id_we_get_is").text(response);
                  // alert(response);
                  $('#c_hospital').html(response);
                  console.log(response);
                }
              });
            }



            $('body').on('click', '.edit_client_detail', function() {
                $('.disease input[type="checkbox"]').attr("checked",false);

                var c_name     = $(this).attr('data-c_name');
                var c_lname     = $(this).attr('data-c_lname');
                var c_oname     = $(this).attr('data-c_oname');
                var c_orga     = $(this).attr('data-c_orga');
                var c_hospital     = $(this).attr('data-c_hospital');
                var c_email    = $(this).attr('data-email');
                var c_phone    = $(this).attr('data-phone');
                var c_status   = $(this).attr('data-status');
                var c_gender   = $(this).attr('data-gender');
                var c_address   = $(this).attr('data-address');
                var c_dob   = $(this).attr('data-dob');
                var c_subs   = $(this).attr('data-c_subs');
                var c_marital   = $(this).attr('data-marital');
                var c_state   = $(this).attr('data-state');
                var c_client_profile   = $(this).attr('data-client_profile');
                var _token     = $(this).attr('data-client_id');
                var detail_pkg_edit     = $(this).attr('data-detail_pkg_edit');
                var ind_family     = $(this).attr('data-ind_family');
                var diseases     = $(this).attr('data-diseases');
                var comment     = $(this).attr('data-comment');
                var ve = $(this).attr('data-ve');

                console.log(c_name);

                var diseases = diseases.split(",");
                console.log(diseases);

                if(jQuery.inArray("Sickle Cell Disease",diseases) !== -1){
                    $('#sickle').attr("checked","checked")
                    console.log('got sickle');
                }

                if(jQuery.inArray("Kidney Disease",diseases)!== -1){
                    $('#kidney').attr("checked","checked")
                    // console.log('got kidney');
                }

                if(jQuery.inArray("Cancer(Prostate,Cervical)",diseases) !== -1){
                    $('#cancer').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Epilepsy",diseases) !== -1){
                    $('#epilepsy').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Diaberes Mellitus",diseases) !== -1){
                    $('#diabetes').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Asthma",diseases) !== -1){
                    $('#asthma').attr("checked","checked")
                    console.log('got asthma');
                }

                if(jQuery.inArray("HIV-AIDS",diseases) !== -1){
                    $('#hiv').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Surgeries",diseases) !== -1){
                    $('#surgeries').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Hypertension",diseases) !== -1){
                    $('#hypertension').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Cataract",diseases) !== -1){
                    $('#cataract').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Goiter",diseases) !== -1){
                    $('#goiter').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Peptic Ulcer",diseases) !== -1){
                    $('#peptic').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Hepatitis",diseases) !== -1){
                    $('#hepatitis').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Glaucoma",diseases) !== -1){
                    $('#glaucoma').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Tuberculosis",diseases) !== -1){
                    $('#tuberculosis').attr("checked","checked")
                    // console.log('got cancer');
                }

                if(jQuery.inArray("Hemorrhoids",diseases) !== -1){
                    $('#hemorrhoids').attr("checked","checked")
                    // console.log('got cancer');
                }




                
                var subs_array = detail_pkg_edit.split("--------")
                $('#subscription_ids').val(0).trigger('change');
                for (var i = 1; i < subs_array.length; i++)
                {
                // $("#band_edit option[value="+bnd_pkg_array[i]+"]").prop("selected", false).parent().trigger("change");
                $('#subscription_ids option[value="'+subs_array[i]+'"]').attr("selected", "selected");
                // $('#band_edit').attr(bnd_pkg_array[i]).trigger('change')
                
                }
                var profile = 'https://hmo.liontech.com.ng/uploads/clients/'+c_client_profile;

                get_clients(c_hospital);


                $('#c_name').val(c_name);
                $('#c_lname').val(c_lname);
                $('#c_oname').val(c_oname);
                $('#c_orga').val(c_orga).trigger('change');
                $('#c_status').val(c_status).trigger('change');
                $('#ind_family').val(ind_family).trigger('change');
                $('#c_email').val(c_email);
                $('#c_phone').val(c_phone);
                $('#c_gender').val(c_gender).trigger('change');
                $('#c_address').val(c_address);
                $('#comment').text(comment);
                $('#c_dob').val(c_dob).trigger('change');;
                $('#c_marital').val(c_marital).trigger('change');;
                $('#c_subs').val(c_subs).trigger('change');;
                $('#c_state').val(c_state).trigger('change');
                // alert(profile);
                $('#c_client_profile').attr('src',profile).trigger('change');
                $('#_token').val(_token);

                if(ve !== "edit"){
                    $('.modal-title').text("View Enrollee Data");
                    $('#update').hide();
                    $('#edit_client input').attr("readonly","readonly");          
                    $('#edit_client select').attr("disabled",true);          
                    $('#edit_client textarea').attr("readonly",true);          
                    $('.disease input[type="checkbox"]').attr("disabled",true);          
                    $('#c_client_profile').hide();          
                    $('#upload-text').hide();          
                }else{
                    $('.modal-title').text("Edit Enrollee Data");
                    $('#update').show();
                    $('.disease input[type="checkbox"]').attr("disabled",false);          
                    $('#edit_client input').attr("readonly",false);          
                    $('#edit_client select').attr("disabled",false);          
                    $('#edit_client textarea').attr("readonly",false);          
                    $('#c_client_profile').show();          
                    $('#upload-text').show();          
                }

            })

            $('body').on('change', '#c_subs', function() { 
                var sub_id = $(this).val();
                get_clients(sub_id);
            })
            }, false);
            
            </script>
        </div>
    </div>
</div>