<?php

/* Subscription view

*/
 
?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if(in_array('644',$role_resources_ids) || $user_info[0]->user_role_id==1) { ?>

<?php //if(in_array('341',$role_resources_ids) || in_array('502',$role_resources_ids) || in_array('534',$role_resources_ids) || in_array('535',$role_resources_ids) || in_array('536',$role_resources_ids) || $user_info[0]->user_role_id==1) {?>




<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

   
</div>

<?php }else{
    redirect('admin/dashboard');
} ?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div class="box-header with-border">

   
    <h3 class="box-title"> Filter Data by Date <?php //echo $query; ?> </h3>

  </div>

  <div class="box-body">

    <?php echo form_open('admin/Hospital/change_hospital_requests');?>

    <div class="row">

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-2">

        <div class="form-group">

          <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>
        </div>

      </div>

    </div>

    <?php echo form_close(); ?> </div>
</div>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>

    <div class="box-datatable table-responsive">

        <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

            <thead>

                <tr>
                  <?php if (in_array('534',$role_resources_ids) || in_array('535',$role_resources_ids) || in_array('537',$role_resources_ids) || in_array('538',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
                    <th width="10%"><?php echo $this->lang->line('xin_action');?></th>
                     
                   <?php endif ?> 
                     <th width="20%">Rejected by</th>
                    <th width="20%">New Provider</th>
                    <th width="20%">Enrollee</th>
                    <th>Phone</th> 
                    <th width="0%"></th>  
                    <th width="20%">Old Provider</th>
                    <th>Reason</th>
                    <th width="13%">Date</th>  
                </tr>

                <tbody> 
                    <?php   
                    if(!empty($all_requests))
                    { 
                        // print_r($all_requests);die;
                        foreach ($all_requests as $key => $value)
                        {              
                            if ($value->status !='rejected')  {
                              continue;
                            }
                         
                            $ci=& get_instance();
                            $ci->load->model('Training_model'); 

                            $client_data    =  $ci->Training_model->getAll2('xin_clients', ' client_id='. $value->client_id.' ');

                            if (isset($client_data[0]->hospital_id) and !empty($client_data[0]->hospital_id)) 
                            {
                                $current_hospi  =  $ci->Training_model->getAll2('xin_hospital', ' hospital_id='. $client_data[0]->hospital_id.' ');
                            }

                            


                            $change_hospi   =  $ci->Training_model->getAll2('xin_hospital', ' hospital_id='. $value->hospital_id.' ');
                            
                                
                            ?>  
                            <tr>
                              <?php if (in_array('534',$role_resources_ids) || in_array('535',$role_resources_ids) || in_array('537',$role_resources_ids) || in_array('538',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
                     
                                <td>
                                    <?php if ($value->status == 'approved'){ ?>
                                        <p class="btn btn-success"> Approved</p> 

                                     <?php }elseif($value->status == 'rejected') { ?>
                                        <p class="btn btn-danger"> Rejected</p> 
                                    <?php }else { ?>
                                        <?php if (in_array('534',$role_resources_ids) || in_array('535',$role_resources_ids) || in_array('537',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
                                        <a href="<?php echo base_url(); ?>admin/Hospital/change_hospital_requests?client_id=<?php echo $value->client_id; ?>&hospital_id=<?php echo $value->hospital_id; ?>&approve=yes&id=<?php echo $value->change_hospital_request_id; ?>"  class="btn btn-info">Approve</a>
                     
                                        <?php endif ?>

                                        <?php if (in_array('534',$role_resources_ids) || in_array('535',$role_resources_ids)  || in_array('538',$role_resources_ids) || $user_info[0]->user_role_id==1): ?>
                                        <a href="<?php echo base_url(); ?>admin/Hospital/change_hospital_requests?reject=yes&id=<?php echo $value->change_hospital_request_id; ?>" class="btn btn-warning">Reject</a>
                     
                                        <?php endif ?>


                                    <?php } ?>
                                </td>

                              <?php endif ?>
                                <td>
                                    <?php if(!empty($value->aprove_reject_by))  {

                                        echo $value->aprove_reject_by;
                                    } ?>

                                </td>
                                <td><?php echo isset($change_hospi[0]->hospital_name) ? $change_hospi[0]->hospital_name : ''; ?> </td>


                                <td><?php echo isset($client_data[0]->name) ? $client_data[0]->name." ".$client_data[0]->last_name : ''; ?> </td>

                                <td><?php echo isset($client_data[0]->contact_number) ? $client_data[0]->contact_number : ''; ?> </td>

                                <td></td>

                                <td><?php echo isset($current_hospi[0]->hospital_name) ? $current_hospi[0]->hospital_name : ''; ?> </td>
                                <td><a class="btn btn-primary" data-toggle="modal" data-target="#reasonModal" onclick="return loadModalView('<?= $value->reason ?>')">View</a></td>

                                <td><?php echo $value->created_on; ?></td>
                                 
                               
                            </tr> 

                            <?php 
                        }
                    }
                    ?>
                </tbody>

            </thead>

        </table>

    </div>

  </div>

</div>
 
 <!-- Modal -->
  <div class="modal fade" id="reasonModal" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Reason</h3>
        </div>
        <form>
          <div class="modal-body" id="fetched_data">
            <div class="form-group col-lg-12">
              <label for="reason">The reason to change:</label>
              <textarea class="form-control" name="that_reason" id="that_reason" rows="7" placeholder="Define the reason for to change" required readonly></textarea>
              <input type="hidden" name="id" id="did" class="form-control">
            </div>
          </div>
          <div class="clearfix"></div>
        </form>
      </div>
      
    </div>
  </div>

<script type="text/javascript">
    function loadModalView(reason){
          $("#that_reason").text(reason);
      }
    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
          buttons: ['csv', 'excel', 'pdf', 'print'],
          "order": [[6,"dsc"]]
        }); 
    }, false);

     
</script>
 






 