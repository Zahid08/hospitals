<?php

/* Subscription view

*/
 
?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>
<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>

<?php if($user_info[0]->user_role_id==1 ||in_array('525',$role_resources_ids) ||in_array('526',$role_resources_ids) ||  in_array('527',$role_resources_ids)) {?>

<?php

      $role_user = $this->Xin_model->read_user_role_info($user_info[0]->user_role_id);

      if(!is_null($role_user)){

        $role_resources_ids = explode(',',$role_user[0]->role_resources);

      } else {

        $role_resources_ids = explode(',',0); 

      }

?>


<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> Filter Data by Date <?php //echo $query; ?> </h3>

  </div>

  <div class="box-body">

    <?php echo form_open('admin/Hospital/diagnose_hospital_clients_bill_requests');?>

    <div class="row">

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-2">

        <div class="form-group">

          <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

        </div>

      </div>
        <div class="col-md-2">

            <div class="form-group">
                <?php if($user_info[0]->user_role_id==1 ||in_array('525',$role_resources_ids) ||in_array('526',$role_resources_ids) ||  in_array('528',$role_resources_ids) ||  in_array('529',$role_resources_ids) ||  in_array('530',$role_resources_ids) ||  in_array('531',$role_resources_ids) ||  in_array('532',$role_resources_ids) ||  in_array('533',$role_resources_ids)) { ?>
                <button type="submit" name="all_final_approve" value="all_final_approve" class="btn btn-danger save"><i class="fa fa fa-check-square-o"></i><b> APPROVE ALL BILLS</b></button>
                <?php   } ?>
            </div>

        </div>

    </div>

    <?php echo form_close(); ?> </div>
</div>

<?php } ?>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>

    <div class="box-datatable table-responsive">

        <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

            <thead>

                <tr> 
                  <?php if($user_info[0]->user_role_id==1 ||in_array('525',$role_resources_ids) ||in_array('526',$role_resources_ids) ||  in_array('528',$role_resources_ids) ||  in_array('529',$role_resources_ids) ||  in_array('530',$role_resources_ids) ||  in_array('531',$role_resources_ids) ||  in_array('532',$role_resources_ids) ||  in_array('533',$role_resources_ids)) { ?>
                    <th width="25%"><?php echo $this->lang->line('xin_action');?></th>
                  <?php } ?>
                    <th width="4%">Progress</th>
                    <th width="15%">App or Rej by</th>
                    <th width="17%">Auth. Code</th>
                    <th>Provider</th>
                   <!--<th>Age</th>-->
                    <th width="10%">Date</th>
                    <th width="3%">Bill</th>
                </tr>

                <tbody> 
                  <?php   
                    if(!empty($xin_diagnose_clients))
                    { 

                        $admin_id = $this->session->userdata;

                        foreach ($xin_diagnose_clients as $key => $value)
                        {              
                          if ($value->diagnose_bill_status == '3' || $value->diagnose_bill_status == '4' || empty($value->diagnose_generated_code)) {
                            continue;
                          }
                             
                            $admin_info = array();

                            $ci=& get_instance();
                            $ci->load->model('Clients_model'); 

                            $services = $ci->Clients_model->read_individual_hospital_diagnose_services($value->diagnose_id);
                            $drugs = $ci->Clients_model->read_individual_hospital_diagnose_drugs($value->diagnose_id);

                            if ($value->diagnose_bill_status != '') {
                                $admin_info = $ci->Clients_model->read_individual_admin_info($value->diagnose_bill_approve_by);
                                // echo $this->db->last_query()."<br />";
                                // print_r($admin_info);
                                // echo "<br />";
                  ?>  
                            <tr>
                              <?php if($user_info[0]->user_role_id==1 ||in_array('525',$role_resources_ids) ||in_array('526',$role_resources_ids) ||  in_array('528',$role_resources_ids) ||  in_array('529',$role_resources_ids) ||  in_array('530',$role_resources_ids) ||  in_array('531',$role_resources_ids) ||  in_array('532',$role_resources_ids) ||  in_array('533',$role_resources_ids)) { ?>
                                <td>
                                    <a class="btn btn-default" data-toggle="modal" data-target="#myModal" onclick="return loadModalView(<?php echo $value->diagnose_id; ?>);"><i class="fa fa-eye"></i></a>
                                    <?php if ($value->diagnose_bill_status == '2'){
                                              if ($value->diagnose_bill_approve_by == $admin_id['user_id']['user_id']){ ?>
                                                  <a onClick="alert('You have already approved bill. Awaiting final Approval!')"  class="btn btn-info">Final Approval Needed</a> 
                                                  <?php if (in_array('654', $role_resources_ids) || $user_info[0]->user_role_id == 1): ?>
                                                    
                                            <?php if($value->diagnose_bill_note != '') { ?>
                                                  <a data-toggle="modal" data-target="#viewNoteModal" onclick="return loadViewNoteModal(<?php echo $value->diagnose_id; ?>,3);" class="btn btn-warning"><i class="fa fa-eye"></i>View Note</a>
                                            <?php } ?>
                                                  <?php endif ?>
                                    <?php
                                              } else {
                                                if ($user_info[0]->user_role_id==1 ||in_array('525',$role_resources_ids) ||in_array('526',$role_resources_ids) ||  in_array('529',$role_resources_ids)) {
                                    ?>
                                                  <a href="<?php echo base_url(); ?>admin/Hospital/diagnose_hospital_clients_bill_requests?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&second_approve=yes&id=<?php echo $value->diagnose_id; ?>"  class="btn btn-info"> Approve Bill</a> 
                                              <?php 
                                                }

                                                if($user_info[0]->user_role_id==1 ||in_array('525',$role_resources_ids) ||in_array('526',$role_resources_ids) ||  in_array('530',$role_resources_ids)){
                                              ?>
                                                <a data-toggle="modal" data-target="#billReasonModal" onclick="return loadReasonModal(<?php echo $value->diagnose_id.",".$value->diagnose_hospital_id.",".$value->diagnose_client_id; ?>);" class="btn btn-danger">Reject</a>
                                              <?php } ?>
                                            <?php if($value->diagnose_bill_note != '') { ?>
                                                  <a data-toggle="modal" data-target="#viewNoteModal" onclick="return loadViewNoteModal(<?php echo $value->diagnose_id; ?>,3);" class="btn btn-warning"><i class="fa fa-eye"></i>View Note</a>
                                                
                                            <?php } ?>

                                    <?php    
                                            }
                                          }elseif($value->diagnose_bill_status == '4') { ?>
                                        <p class="btn btn-danger"> Rejected</p> 
                                    <?php }elseif($value->diagnose_bill_status == '3') { ?>
                                        <p class="btn btn-success">Bill Approved</p> 
                                        <!-- <p class="btn btn-warning"> Approve Bill</p> --> 
                                    <?php 
                                      }else { 
                                      if($user_info[0]->user_role_id==1 ||in_array('525',$role_resources_ids) ||in_array('526',$role_resources_ids) ||  in_array('528',$role_resources_ids)){
                                    ?>
                                        <a href="<?php echo base_url(); ?>admin/Hospital/diagnose_hospital_clients_bill_requests?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&approve=yes&id=<?php echo $value->diagnose_id; ?>"  class="btn btn-info">Approve</a>
                                    <?php }?>
                                    <?php if (in_array('654', $role_resources_ids) || $user_info[0]->user_role_id == 1): ?>
                                    <?php if($value->diagnose_bill_note == '') { ?>
                                        <a data-toggle="modal" data-target="#noteModal" onclick="return loadNoteModal(<?php echo $value->diagnose_id.",".$value->diagnose_hospital_id.",".$value->diagnose_client_id; ?>);" class="btn btn-warning">Write Note</a>
                                    <?php } else { ?>
                                        <a data-toggle="modal" data-target="#viewNoteModal" onclick="return loadViewNoteModal(<?php echo $value->diagnose_id; ?>,3);" class="btn btn-warning"><i class="fa fa-eye"></i>View Note</a>
                                    <?php } ?>
                                    <?php endif; ?>
                                        <!-- <a href="<?php echo base_url(); ?>admin/Hospital/diagnose_hospital_clients_bill_requests?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&reject=yes&id=<?php echo $value->diagnose_id; ?>" class="btn btn-warning">Reject</a> -->

                                    <?php } ?>
                                </td>
                              <?php } ?>
                                <td>
                                  <div class="progress" style="border: 1px solid #0177bc;">
                                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php if ($value->diagnose_bill_status == '2') echo "50%"; else if ($value->diagnose_bill_status == '3') echo "100%"; else echo "0%"; ?>"><?php if ($value->diagnose_bill_status == '2') echo "50%"; else if ($value->diagnose_bill_status == '3') echo "100%"; else echo "0%"; ?>
                                    </div>
                                  </div>
                                </td>
                                <td>
                                  <?php 
                                      if($admin_info) {
                                          $n = 1;
                                          if(strlen($value->diagnose_bill_approve_by) == 1){ 
                                            echo "&#8226; ".$admin_info->first_name." ".$admin_info->last_name." "; 
                                          } else {
                                            $ids = explode(",", $value->diagnose_bill_approve_by);
                                            foreach ($ids as $id) {
                                              $admin_info = $ci->Clients_model->read_individual_admin_info($id);
                                              if (!empty($admin_info)) {
                                                 echo "&#8226; ".$admin_info->first_name." ".$admin_info->last_name."<br />";}
                                            }
                                          }
                                      } else {
                                        echo "Pending Approval"; 
                                      }
                                  ?>      
                                </td>
                                <?php
                                  $hcp = preg_replace('/\s+/', '', $value->hospital_name);
                                  $date = date("Ymd", strtotime($value->diagnose_date_time));
                                ?>
                                <td><?php if(empty($value->diagnose_generated_code)) echo "-----"; else echo "P-".$value->loc_id."-".$date."-".$value->diagnose_generated_code; ?></td>
                                <td><?php if(empty($value->hospital_name)) echo "-----"; else echo $value->hospital_name; ?></td>	         
                                 <td><?php echo  date("Y-m-d H:i A", strtotime($value->diagnose_date_time)); ?></td>
                                <td>₦<?php echo number_format($value->diagnose_total_sum).".00"; ?></td>
                            </tr> 

                            <?php 
                          }
                        }
                    }
                    ?>
                </tbody>

            </thead>

        </table>

    </div>

  </div>

</div>
 

<script type="text/javascript">

      function loadModalView(id){
          // alert("ID is: " + id);

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#fetched_data").html(response);
            }
          });

      }

      function loadReasonModal(id,hid,cid){
          // alert("ID is: " + id);

          $("#that_reason").val("");
          $("#bdid").val(id);
          $("#bhid").val(hid);
          $("#bcid").val(cid);
      }

      function loadNoteModal(id,hid,cid){
          // alert("ID is: " + id);

          $("#that_note").val("");
          $("#nbdid").val(id);
          $("#nbhid").val(hid);
          $("#nbcid").val(cid);
      }

      function loadViewNoteModal(id,type) {
          // alert(id+ " " + type);

          // if(type == 3) { $("#rtitle").text("Note") }

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_reason',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id, type : type},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
              $("#fetched_note").html(response);
            }
          });
      }
      
    document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
          buttons: ['csv', 'excel', 'pdf', 'print'],
          "order": [[ 5, "desc" ]]
        }); 
    }, false);

     
</script>
 






 