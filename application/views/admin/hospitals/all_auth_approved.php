<?php

/* Subscription view

*/
 
?>

<?php $session = $this->session->userdata('username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?>

<?php $user_info = $this->Xin_model->read_user_info($session['user_id']);?>


<style type="text/css">
    .fa{
        cursor: pointer;
    }
</style>
<div class="box mb-4 <?php echo $get_animate;?>">

  <div class="box-header with-border">

     <h3 class="box-title"> Filter Data by Date <?php //echo $query; ?> </h3>

  </div>

  <div class="box-body">

    <?php echo form_open('admin/Hospital/all_auth_approved');?>

    <div class="row">

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_frm_date');?>" readonly id="from_date" name="from_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-3">

        <div class="form-group">

          <input class="form-control date" placeholder="<?php echo $this->lang->line('xin_e_details_to_date');?>" readonly id="to_date" name="to_date" type="text" value="<?php echo date('Y-m-d')?>">

        </div>

      </div>

      <div class="col-md-2">

        <div class="form-group">

          <button type="submit" name="get_filter_result" class="btn btn-primary save">Fetch Data</button>

        </div>

      </div>

    </div>

    <?php echo form_close(); ?> </div>
</div>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>

    <div class="box-datatable table-responsive">

        <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

            <thead>

                <tr> 
                    <th width="20%"><?php echo $this->lang->line('xin_action');?></th>
                    <th width="15%">App or Rej by</th>
                    <th width="20%">Auth. Code</th>
                    <th width="25%">Provider</th>
                    <th width="18%">Date</th>
                    <th width="4%">Bill</th>
                </tr>

                <tbody> 
                    <?php   
                    if(!empty($xin_diagnose_clients))
                    { 
                        foreach ($xin_diagnose_clients as $key => $value)
                        {              
                             
                            $ci=& get_instance();
                            $ci->load->model('Clients_model'); 

                            $services = $ci->Clients_model->read_individual_hospital_diagnose_services($value->diagnose_id);
                            $drugs = $ci->Clients_model->read_individual_hospital_diagnose_drugs($value->diagnose_id);
                            $admin_info = $ci->Clients_model->read_individual_admin_info($value->diagnose_status_approve_by);

                            if ($value->diagnose_status == '2') {
                                
                            ?>  
                            <tr>
                                <td>
                                    <a class="btn btn-default" data-toggle="modal" data-target="#myModal" onclick="return loadModalView(<?php echo $value->diagnose_id; ?>);"><i class="fa fa-eye"></i></a>
                                    <?php if ($value->diagnose_status == '2'){ ?>
                                        <p class="btn btn-success">Code Approved</p> 

                                    <?php }else if(($value->diagnose_status == '3') || ($value->diagnose_status == '4')) { ?>
                                        <p class="btn btn-danger"> Rejected</p> 
                                    <?php }else { ?>
                                        <a href="<?php echo base_url(); ?>admin/Hospital/diagnose_hospital_clients_requests?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&approve=yes&id=<?php echo $value->diagnose_id; ?>"  class="btn btn-info">Approve</a>

                                        <a data-toggle="modal" data-target="#reasonModal" onclick="return loadReasonModal(<?php echo $value->diagnose_id.",".$value->diagnose_hospital_id.",".$value->diagnose_client_id; ?>);" class="btn btn-warning">Reject</a>
                                        <!-- <a href="<?php echo base_url(); ?>admin/Hospital/diagnose_hospital_clients_requests?client_id=<?php echo $value->diagnose_client_id; ?>&hospital_id=<?php echo $value->diagnose_hospital_id; ?>&reject=yes&id=<?php echo $value->diagnose_id; ?>" class="btn btn-warning">Reject</a> -->

                                    <?php } ?>
                                </td>
                                <td><?php if(!empty($admin_info)) echo $admin_info->first_name." ".$admin_info->last_name; else echo "Primary Case"; ?></td>
                                <?php
                                  $hcp = preg_replace('/\s+/', '', $value->hospital_name);
                                  $date = date("Ymd", strtotime($value->diagnose_date_time));
                                ?> 
                                <td><?php if(empty($value->diagnose_generated_code)) echo "-----"; else echo "P-".$value->loc_id."-".$date."-".$value->diagnose_generated_code; ?></td>
                                <td><?php if(empty($value->hospital_name)) echo "-----"; else echo $value->hospital_name; ?></td>
 				<td><?php echo date("Y-m-d H:i A", strtotime($value->diagnose_date_time)); ?></td>
                                  <td>₦<?php echo number_format($value->diagnose_total_sum).".00"; ?></td>
                            </tr> 

                            <?php
                            } 
                        }
                    }
                    ?>
                </tbody>

            </thead>

        </table>

    </div>

  </div>

</div>
 

<script type="text/javascript">

      function loadModalView(id){
          // alert("ID is: " + id);

          $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id',
            method   : 'post',   
            dataType    : 'text',      
            data     : {id : id},
            success  : function(response){
              // $("#id_we_get_is").text(response);
              // alert(response);
				
				$("#fetched_data").html(response);
				$('#servicesmodel').select2();
				$('#drugsmore1').select2(); 
            }
          });

      }
function adddrugsservice()
{
	var str = $("#frms").serialize();
	var services = $('#services').val();
	var drugsmore = $('#drugsmore').val();
	
	var hostel_idss = $('#hostel_idss').val();
	//alert(hostel_idss);
	 $.ajax({
		url      : '<?php echo base_url(); ?>hospital/Clients/addmorederviceanddrugs',
		method   : 'post',   
		dataType    : 'text',      
		data     : {strs : str},
		success  : function(response){
			if(response =='sucess'){
				//closed();
				
				loadModalView(hostel_idss);
				
				//$('#currentredirect').trigger('click');
			}
		}
	  });
} 
function closed(){
	$('#myModal').hide();
}
      function loadReasonModal(id,hid,cid){
          // alert("ID is: " + id);

          $("#that_reason").val("");
          $("#did").val(id);
          $("#hid").val(hid);
          $("#cid").val(cid);

          // $.ajax({
          //   url      : '<?php echo base_url(); ?>hospital/Clients/fetch_diagnose_id',
          //   method   : 'post',   
          //   dataType    : 'text',      
          //   data     : {id : id},
          //   success  : function(response){
          //     // $("#id_we_get_is").text(response);
          //     // alert(response);
          //     $("#fetched_data").html(response);
          //   }
          // });

      }

      document.addEventListener('DOMContentLoaded', function(){ 
        var xin_table_new = $('#xin_table_new').dataTable({
          dom: 'lBfrtip',
          buttons: ['csv', 'excel', 'pdf', 'print'],
          "order": [[ 4, "desc" ]]
        }); 
    }, false);

 function loadDrugReasonModal(drug_did,drug_hid,drug_cid,drug_mid){
     //alert(drug_did);
	  $("#drug_reasonModal").on("show.bs.modal", function(e) {
   	    $("#drug_did").val(drug_did);
            $("#drug_hid").val(drug_hid);
            $("#drug_cid").val(drug_cid);
            $("#drug_mid").val(drug_mid);
            $("#redirectflage").val('2');
	  });
         // $("#that_drugreason").val("");
          

          

      }
      
      function loadServiceReasonModal(service_did,service_hid,service_mid,service_sid){
	  //alert(service_mid);
          //$("#that_service_reason").val("");
	  $("#service_reasonModal").on("show.bs.modal", function(e) {
          $("#service_did").val(service_did);
          $("#service_hid").val(service_hid);
          $("#service_mid").val(service_mid);
          $("#service_sid").val(service_sid);
          $("#service_flage").val('2');
	  });         

      }
     function getmoredurd(){
		 $('#drugsservices').show();
	 }
	 function getaddmoremore(value,hospitalids)
{
	var value1 = value -1;
	var value2 = value + 1;
	 $.ajax({
            url      : '<?php echo base_url(); ?>hospital/Clients/addmoredivemore1',
            method   : 'post',   
            dataType    : 'text',      
            data     : {'value' : value,'hospitalids':hospitalids},
            success  : function(response){
              console.log(response);
			  $("#addmoremore").attr("onclick","return getaddmoremore("+value2+","+hospitalids+")");
			   //$('.select2').select2();
              $("#divmore"+value1).after(response);
			  $('#drugsmore'+value).select2(); 
            }
          });
}
function removedivmore(values){
	$('#divmore'+values).hide();
}
</script>
 <style>
label.padding {
    padding-top: 40px;
}
</style>






 