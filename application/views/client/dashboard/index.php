<?php $session = $this->session->userdata('client_username'); ?>

<?php $clientinfo = $this->Clients_model->read_client_info($session['client_id']); ?>



<div class="box-widget widget-user-2">

  <div class="widget-user-header">

      <div class="row">
          <div class="col-md-6">
              <h4 class="widget-user-username welcome-hrsale-user">Welcome back, <?php echo $clientinfo[0]->name;?>!</h4>

              <h5 class="widget-user-desc welcome-hrsale-user-text">Today is <?php echo date('l, j F Y');?></h5>
          </div>

          <div  class="col-md-6">
              <h4 class="widget-user-username welcome-hrsale-user" style="text-align: right;"> Your plan is <?php echo $subs_pkg; ?> Plan</h4>
          </div>
      </div>

    
 
  </div>

</div>

<div class="row animated fadeInRight">

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('client/profile/dependants_list');?>">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-primary"><i class="fa fa-users"></i></span>

      <div class="info-box-content"> <span class="info-box-number"><?php echo $total_dependent; ?></span> <span class="info-box-number client-hr-invoice">Total Dependants</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('client/profile/hospital_list');?>">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-green"><i class="fa fa-hospital-o"></i></span>

      <div class="info-box-content"> <span class="info-box-number"><?php echo $total_hospital; ?></span> <span class="info-box-number client-hr-invoice">Total Providers</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('client/profile/change_dependent_list');?>">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-purple"><i class="fa fa-medkit"></i></span>

      <div class="info-box-content"> <span class="info-box-number"> <?php echo $total_dependent_req; ?></span> Dependent Requests</div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>
  </div>

<div class="row animated fadeInRight">

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="#">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-yellow"><i class="fa fa-ambulance"></i></span>

      <div class="info-box-content"> <span class="info-box-number"> 0</span> <span class="info-box-number client-hr-invoice">Total Encounters</span> </div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>

  
  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="<?php echo site_url('client/profile/hospital_list');?>">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-red"><i class="fa fa-h-square"></i></span>

      <div class="info-box-content"> <span class="info-box-number"><?php echo $total_hospital_req; ?></span> Change Provider Requests</div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>

  <div class="col-xl-4 col-md-4 col-12 hr-mini-state"> <a class="text-muted" href="#">

    <div class="info-box hrsalle-mini-stat"> <span class="info-box-icon bg-green"><i class="fa fa-tasks"></i></span>

      <div class="info-box-content"> <span class="info-box-number">0</span>Total Complains</div>

      <!-- /.info-box-content --> 

    </div>

    </a> 

    <!-- /.info-box --> 

  </div>
</div>

<script type="text/javascript">
window.onload = function () {
	var chart = new CanvasJS.Chart("chartContainer",
	{
		title: {
		},
		axisY: {
			includeZero: false,
			interval: 10,
		},
		axisX: {
			interval: 1,
			intervalType: "month",
			valueFormatString: "MMM"
			},
		data: [
		{
			type: "rangeColumn",
			color: "#0b926c",
			dataPoints: [
				{ x: new Date(2020,00,01), y: [-9.7, 10.4] },  // y: [Low ,High]
				{ x: new Date(2020,01,01), y: [-8.7, 16.5] },
				{ x: new Date(2020,02,01), y: [-3.5, 19.4] },
				{ x: new Date(2020,03,01), y: [5.0, 22.6] },
				{ x: new Date(2020,04,01), y: [7.9, 29.5] },
				{ x: new Date(2020,05,01), y: [9.2, 34.7] },
				{ x: new Date(2020,06,01), y: [7.3, 30.5] },
				{ x: new Date(2020,07,01), y: [4.4, 25.5] },
				{ x: new Date(2020,08,01), y: [-3.1, 20.4] },
				{ x: new Date(2020,09,01), y: [-5.2, 15.4] },
				{ x: new Date(2020,10,01), y: [-13.5, 9.8] },
				{ x: new Date(2020,11,01), y: [-15.5, 5.8] }
			]
		}
		]
	});
	chart.render();
}
</script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

<style type="text/css">

.box-body {

    padding: 0 !important;

}

.info-box-number {

	font-size:16px !important;

	font-weight:300 !important;

}

</style>
<body>
<div id="chartContainer" style="height: 180px; width: 100%;">
</div>
</body>
