<?php

/* Dependant List view

*/

?>

<?php $session = $this->session->userdata('client_username');?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>



<div class="row <?php echo $get_animate;?>">

  
   
  <!-- fix for small devices only -->

  <div class="clearfix visible-sm-block"></div>

  
 

</div>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title">All Providers</h3>

  </div>

  <div class="box-body">

    <?php if ($this->session->flashdata('success')): ?>

      <div class="alert alert-success alert-dismissible " role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
            </button>
      </div> 
    <?php endif ?>

    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table_new">

        <thead>

          <tr>

            

            <th  style="    width: 40%;">Provider Name</th>

            <th  style="    width: 40%;">Location</th>

            <th style="    width: 20%;">Action</th>

            

          </tr>

        </thead>

        <tbody>
          <?php 
          if (!empty($result)) {
             
          
          foreach ($result as $key => $value): ?>
               <?php 

               $ci=& get_instance(); 

               $hos_names     = '';
               $subs_pkg      = '';
           

               $subs = $ci->Training_model->getAll2('xin_subscription',' subscription_id = "'.$value->subscription_ids.'" ');

               if (isset($subs[0]->band_types) AND !empty($subs[0]->band_types)) 
               {
              
                    $subs_pkg .= $subs[0]->plan_name;

                    $bands  = explode(',', $subs[0]->band_types);

                    $hospital_all = array();
                    foreach ($bands as $key2 => $band) 
                    {
                         $hos_data = $ci->Training_model->getAll2('xin_hospital',' band_id = "'.$band.'" '); 

                         foreach ($hos_data as $hos) {
                           
                          if(isset($hos->hospital_name))
                          {
                            array_push($hospital_all, $hos);

                              $location_data = $ci->Training_model->getAll2('xin_location',' location_id = "'.$hos->location_id.'" ');
                              $location = '';

                              if(isset($location_data[0]->location_name))
                              {
                                   $location = '('.$location_data[0]->location_name.')';
                              }

                              ?>
                              <tr>
                                   <td><?php echo $hos->hospital_name; ?></td>
                                   <td><?php echo $location_data[0]->location_name; ?></td>

                                   <?php if ($value->hospital_id == $hos->hospital_id){ ?>
                                        <td><button class="btn btn-default">Current Provider</button></td>
                                   <?php }else { ?>
                                        <td>
                                          <a class="btn btn-primary" data-toggle="modal" data-target="#reasonModal" onclick="return loadReasonModal(<?= $hos->hospital_id ?>)">Choose this Provider</a>
                                        </td>
                                   <?php }  ?>
                              </tr>


                              <?php
                  
                               
                         }
                      }
                    }
                    // echo json_encode($hospital_all);
                    // print_r($hospital_all);
                    // foreach($hospital_all as $h)
                    // {
                    //   echo $h->hospital_id." - ".$h->hospital_name."\n<br>";
                    // }
                    // die;
               } 
            
            
            ?>
           
          <?php endforeach;

          }  ?>
        </tbody>

      </table>

    </div>

  </div>

</div>

<!-- Modal -->
  <div class="modal fade" id="reasonModal" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Reason</h3>
        </div>
        <form action="<?php echo base_url(); ?>client/profile/hospital_list" name="reason_form" id="reason_form" method="post">
          <div class="modal-body" id="fetched_data">
            <div class="form-group col-lg-12">
              <label for="reason">Define the reason to change:</label>
              <textarea class="form-control" name="that_reason" id="that_reason" rows="7" placeholder="Define the reason for to change" required></textarea>
              <input type="hidden" name="id" id="did" class="form-control">
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Proceed Change</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>

<style type="text/css">

.info-box-number {

	font-size:16px !important;

	font-weight:300 !important;

}

</style>

<script type="text/javascript">
  function loadReasonModal(id){
      // alert("ID is: " + id);
      
      $("#that_reason").val("");
      $("#did").val(id);
      
  }

  document.addEventListener('DOMContentLoaded', function(){ 

      var xin_table_new = $('#xin_table_new').dataTable();  
  }, false);
</script>