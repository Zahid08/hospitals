<?php

/*

* Tickets view

*/

$session = $this->session->userdata('username');

?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>

<!-- <?php $role_resources_ids = $this->Xin_model->user_role_resource(); ?> -->

<!-- <?php $user_info = $this->Xin_model->read_employee_info($session['user_id']);?> -->



<!-- <div class="row">

  <div class="col-md-4 col-12">

    <div class="box box-body bg-hr-primary">

      <div class="flexbox"> <span class="fa fa-ticket text-primary font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo total_tickets();?></span> </div>

      <div class="text-right"><?php echo $this->lang->line('xin_hr_total_tickets');?></div>

    </div>

  </div>

  <div class="col-md-4 col-12">

    <div class="box box-body bg-hr-danger">

      <div class="flexbox"> <span class="fa fa-server text-danger font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo total_open_tickets();?></span> </div>

      <div class="text-right"><?php echo $this->lang->line('xin_hr_total_open_tickets');?></div>

    </div>

  </div>

  <div class="col-md-4 col-12">

    <div class="box box-body bg-hr-success">

      <div class="flexbox"> <span class="ion ion-thumbsup text-success font-size-50"></span> <span class="font-size-40 font-weight-400"><?php echo total_closed_tickets();?></span> </div>

      <div class="text-right"><?php echo $this->lang->line('xin_hr_total_closed_tickets');?></div>

    </div>

  </div>

</div> -->

<?php //if(in_array('306',$role_resources_ids)) {?>

<div class="box mb-4 <?php echo $get_animate;?>">

  <div id="accordion">

    <div class="box-header with-border">

      <h3 class="box-title"><?php echo $this->lang->line('xin_create_new_ticket');?></h3>

      <div class="box-tools pull-right"> <a class="text-dark collapsed" data-toggle="collapse" href="#add_form" aria-expanded="false">

        <button type="button" class="btn btn-xs btn-primary"> <span class="ion ion-md-add"></span> <?php echo $this->lang->line('xin_add_new');?></button>

        </a> </div>

    </div>

    <div id="add_form" class="collapse add-form <?php echo $get_animate;?>" data-parent="#accordion" style="">

      <div class="box-body">

        <?php $attributes = array('name' => 'add_ticket', 'id' => 'xin-form', 'autocomplete' => 'off');?>

        <?php $hidden = array('user_id' => $session['user_id']);?>

        <?php echo form_open('client/profile/add_ticket', $attributes, $hidden);?>

        <div class="bg-white">

          <div class="box-block">

            <div class="row">

              <div class="col-md-6">

                <div class="form-group">

                  <label for="task_name"><?php echo $this->lang->line('xin_subject');?></label>

                  <input class="form-control" placeholder="<?php echo $this->lang->line('xin_subject');?>" name="subject" type="text" value="">

                </div>

              </div>

              <div class="col-md-6">

                <div class="form-group">

                  <label for="description"><?php echo $this->lang->line('xin_ticket_description');?></label>

                  <textarea class="form-control textarea" placeholder="<?php echo $this->lang->line('xin_ticket_description');?>" name="description" cols="30" rows="5" id="description"></textarea>

                </div>

              </div>

            </div>

            <div class="form-actions box-footer">

              <button type="submit" class="btn btn-primary"> <i class="fa fa-check-square-o"></i> Submit Complain </button>

            </div>

          </div>

        </div>

        <?php echo form_close(); ?> </div>

    </div>

  </div>

</div>

<?php //} ?>

<div class="box <?php echo $get_animate;?>">

  <div class="box-header with-border">

    <h3 class="box-title"> <?php echo $this->lang->line('xin_list_all');?> <?php echo $this->lang->line('left_tickets');?> </h3>

  </div>

  <div class="box-body">

    <div class="box-datatable table-responsive">

      <table class="datatables-demo table table-striped table-bordered" id="xin_table">

        <thead>

          <tr class="xin-bg-dark">

            <th><?php echo $this->lang->line('xin_action');?></th>

            <th><?php echo $this->lang->line('xin_ticket_code');?></th>

            <th><?php echo $this->lang->line('xin_subject');?></th>

            <th><?php echo $this->lang->line('xin_ticket_description');?></th>

            <th><i class="fa fa-calendar"></i> <?php echo $this->lang->line('xin_e_details_date');?></th>
            
            <th></i> <?php echo $this->lang->line('xin_ticket_status')?></th>

          </tr>

        </thead>
      <tbody>
        <?php if (!empty($all_ticket)): ?>        
          <?php foreach ($all_ticket as $key => $value): ?>
          <tr>
            <td><?php echo "<span data-toggle=\"tooltip\" data-placement=\"top\" title=\"".$this->lang->line('xin_delete')."\"><button type=\"button\" class=\"btn icon-btn btn-xs btn-danger waves-effect waves-light delete-ch\" data-toggle=\"modal\" data-target=\".delete-modal-abc\" data-record-id=\"". $value->ticket_id . "\"><span class=\"fa fa-trash\"></span></button></span>'" ?></td>  
            <td><?php echo $value->ticket_code ?></td>
            <td><?php echo $value->subject ?></td>
            <td><?php echo $value->description ?></td>
            <td><?php echo $value->created_at ?></td>
            <td>
              <?php 
                if($value->ticket_status == 1){
                  echo "Open";
                }else{
                  echo "Closed";
                } 
              ?>      
            </td>
          </tr>
          <?php endforeach ?>
        <?php endif ?>
      </tbody>

      </table>


    </div>

  </div>

</div>

<div class="modal fadeInLeft delete-modal-abc animated in" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> <button type="button" aria-label="Close" data-dismiss="modal" class="close"><span aria-hidden="true">×</span></button>
 <strong class="modal-title">Are you sure you want to delete this record?</strong> </div>
      <div class="alert alert-danger">
        <strong>Record deleted can't be restored!!!</strong>
      </div>
        <form action="https://hmo.liontech.com.ng/client/profile/ticket_list" name="delete_record" id="delete_record" autocomplete="off" role="form" method="post" accept-charset="utf-8">
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" name="_token" value="000">
        <input type="hidden" name="id" id="id-ch">

 
            <div class="modal-footer">
        
    <input type="hidden" name="token_type" value="0" id="token_type">
        
    <button type="button" data-dismiss="modal" class="btn btn-secondary"><i class="fa fa fa-check-square-o"></i> Close</button>
 
    <button name="hrsale_form" type="submit" class="btn btn-primary"><i class="fa fa fa-check-square-o"></i> Confirm</button>
  </div></form>
    </div>
  </div>
</div>