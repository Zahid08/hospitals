<?php

/* Profile view

*/
$ci = & get_instance();  
     
$ci->load->model('Training_model');
?>

<?php $session = $this->session->userdata('client_username');?>

<?php $user = $this->Clients_model->read_client_info($session['client_id']);?>

<?php $system = $this->Xin_model->read_setting_info(1);?>

<?php if($client_profile!='' && $client_profile!='no file') {?>

<?php $de_file = base_url().'uploads/clients/'.$client_profile;?>

<?php } else {?>

<?php if($gender=='Male') { ?>

<?php $de_file = base_url().'uploads/clients/default_male.jpg';?>

<?php } else { ?>

<?php $de_file = base_url().'uploads/clients/default_female.jpg';?>

<?php } ?>

<?php } ?>

<?php $get_animate = $this->Xin_model->get_content_animate();?>


<?php 
	$limit1  = '';
	$class_d = '';
	// echo $hospital_id;
	// die();
	if(!empty($dob))
	{
		$limit1   =  ' disabled readonly ';
		$class_d  =  ' rung_me ';
	}

?>
<style type="text/css">
	.rung_me {
		background-color: #940024 !important;
		color: white;
	}
</style>

<div class="row">

  <div class="col-md-3 <?php echo $get_animate;?>"> 

    

    <!-- Profile Image -->

    <div class="box box-primary">

      <div class="box-body box-profile"> <a class="" href="#" data-toggle="tab" aria-expanded="true"> <img class="profile-user-img img-responsive img-circle" src="<?php echo $de_file;?>" alt="<?php echo $user[0]->name;?>"></a>

        <h3 class="profile-username text-center"><?php echo $user[0]->name;?></h3>
           

        <div class="list-group"> 
          <a class="list-group-item-profile list-group-item list-group-item-action nav-tabs-link" href="#user_basic_info" data-profile="1" data-profile-block="user_basic_info" data-toggle="tab" aria-expanded="true" id="user_profile_1"> <i class="fa fa-user"></i> Enrollee Profiling</a>
        </div>

      </div>

      <!-- /.box-body --> 

    </div>

  </div>

  	<div class="col-md-9 current-tab <?php echo $get_animate;?>" id="user_basic_info"  aria-expanded="false">

    	<div class="box">

      	<div class="box-header with-border">

        	<?php 

			    if(empty($hospital_id))
				{
					?>
				 	<p class="form-control rung_me"> HI <?php echo $name; ?>, KINDLY NOTE THAT YOU CAN NOT EDIT YOUR DATA AFTER SAVING, PLEASE TAKE YOUR TIME.</p>
					<?php 

				}

        	?>
        	
      	</div>

      	<div class="box-body">

	        <div class="card-block">

	          	<?php $attributes = array('name' => 'edit_client', 'id' => 'edit_client', 'autocomplete' => 'off', 'class'=>'m-b-1');?>

	          	<?php $hidden = array('_method' => 'EDIT', '_token' => $client_id, 'ext_name' => $name);?>

	          	<?php echo form_open_multipart('client/profile/update/'.$client_id, $attributes, $hidden);?>

		          	<div class="form-body">

			            <div class="row">
			            	<div class="col-md-12">
			            		<?php if ($this->session->flashdata('success')): ?>
									<div class="alert alert-success">
										<?php echo $this->session->flashdata('success'); ?>
									</div>
								<?php endif ?>


								<?php if ($this->session->flashdata('error')): ?>
									<div class="alert alert-warning">
										<?php echo $this->session->flashdata('error'); ?>
									</div>
								<?php endif ?>
			            	</div>
				            <div class="col-sm-6">

				                <div class="form-group">

				                  <label for="company_name">First Name</label>

				                  <input class="form-control  <?php echo $class_d; ?>   " name="name" <?php echo $limit1; ?>  placeholder="<?php echo $this->lang->line('xin_client_name');?>"   type="text" value="<?php echo $name;?>">

				                </div>

				                <div class="form-group">


					                <div class="row">

					                    <div class="col-md-6 form-group">

					                        <label for="last_name">Last Name</label>

					                        <input class="form-control"  <?php echo $limit1; ?>   placeholder="Last Name" name="last_name" type="text" value="<?php echo $last_name;?>">

					                    </div>

					                    <div class="col-md-6 form-group">

					                      	<label for="other_name">Other Name </label>

					                      	<input class="form-control"  <?php echo $limit1; ?>   placeholder="Other Name" name="other_name" type="text" value="<?php echo $other_name;?>">

					                    </div>

					                </div>



				                  	<div class="row">

					                    <div class="col-md-6">

					                      	<label for="company_name">Organization Name</label>

					                      	<input class="form-control rung_me "  disabled readonly  type="text" value="<?php echo $company_name;?>">

					                    </div>

					                    <div class="col-md-6">

					                      <label for="contact_number">Phone Number</label>

					                      <input class="form-control"  <?php echo $limit1; ?>   placeholder="Phone Number" name="contact_number" type="number" value="<?php echo $contact_number;?>">

					                    </div>

				                  	</div>

				                </div>

				                <div class="form-group">

				                  	<div class="row">

					                    <div class="col-md-6">

					                      	<label for="email"><?php echo $this->lang->line('xin_email');?></label>

					                      	<input class="form-control"  <?php echo $limit1; ?>   placeholder="<?php echo $this->lang->line('xin_email');?>" name="email" type="email" value="<?php echo $email;?>">

					                    </div>

					                    <div class="col-md-6">

					                      	<label for="website">Date Of Birth</label>

					                      	<input class="form-control"  <?php echo $limit1; ?>   name="dob" value="<?php echo $dob;?>" type="date">

					                    </div>

				                  	</div>

				                </div> 
				            </div>

				            <div class="col-md-6">
				            	<div class="row">

									<div class="col-md-12">
						                <div class="form-group">

						                  	<label for="address">Resident Address</label>

						                  	<input class="form-control"  <?php echo $limit1; ?>   placeholder="Resident address" name="address_1" type="text" value="<?php echo $address_1;?>">

										 
						                </div>
						            </div>
						        </div>


				                <div class="row">

									<div class="col-md-12">
										<div class="form-group">
											<label for="address">Select State </label>

											 
											<select class="form-control" data-plugin="xin_select"  <?php echo $limit1; ?>   name="location" data-plugin="select_hrm" data-placeholder="Select State"> 
												<?php if (isset($all_locations) and !empty($all_locations)): ?>
				                                    <?php foreach ($all_locations as  $value): ?>

				                                        <?php echo "<option  ".($value->location_id == $state ? "selected" : "")."      value='".$value->location_id."'    >".$value->location_name."</option> " ?>
				                                    <?php endforeach ?>
				                                    
				                                <?php endif ?>
				                            </select>
										</div>

									</div>
								</div>


								<div class="row"> 
									<div class="col-md-12">
										<div class="form-group">
											<label for="address">Select Your Provider </label>
											<?php 
												// print_r($subscriptions);die;
												$subscriptions = explode(',', $subscriptions);
												$hos_names     = '';
												$subs_pkg      = '';
												foreach ($subscriptions as $key => $value) 
												{

													$subs = $ci->Training_model->getAll2('xin_subscription',' subscription_id = "'.$value.'" ');
 

													if (isset($subs[0]->band_types) AND !empty($subs[0]->band_types)) 
													{
														
														$subs_pkg .= $subs[0]->plan_name;

														$bands  = explode(',', $subs[0]->band_types);

														// print_r($bands);die;

														foreach ($bands as $key2 => $band) 
														{
															$hos_data = $ci->Training_model->getAll2('xin_hospital',' band_id = "'.$band.'" ');

															foreach ($hos_data as $hos) {

																if(isset($hos->hospital_name))
																{
																	$location_data = $ci->Training_model->getAll2('xin_location',' location_id = "'.$hos->location_id.'" ');
																	$location = '';

																	if(isset($location_data[0]->location_name))
																	{
																		$location = '('.$location_data[0]->location_name.')';
																	}
																	
																	$hos_names .= '<option 

																	 '.($hospital_id == $hos->hospital_id ? "selected" : "").'

																	    value="'.$hos->hospital_id.'" >'.$hos->hospital_name .' '.$location.'</option>';
																}
															}
														}
													}

													
												}

											?>
											 
											<select class="form-control" name="hospital_id" data-plugin="select_hrm" data-placeholder="Select Hospital"  <?php echo $limit1; ?> data-plugin="xin_select" > 
												<?php echo $hos_names; ?>
				                            </select>
										</div>

									</div>
								</div>



								<div class="row">

									<div class="col-md-6"> 
										<div class="form-group">
					                      	<label for="sec">Sex</label>

					                      	<select class="form-control"  <?php echo $limit1; ?>   name="sex" data-plugin="select_hrm" data-placeholder="Sex">

							                    <option value="">Select Sex</option> 

							                    <option value="male" <?php if($sex== 'male'):?> selected="selected"<?php endif;?> > Male </option>

							                    <option value="female" <?php if($sex== 'female'):?> selected="selected"<?php endif;?> > Female </option> 
						                  	</select>
						                </div>
				                    </div>


				                    <div class="col-md-6">
				                    	<div class="form-group">
					                      	<label for="website">Marital status </label>

					                      	<select   <?php echo $limit1; ?>    class="form-control" name="marital_status" data-plugin="select_hrm" data-placeholder="Marital status">

							                    <option value="">Select Marital Status</option> 

							                    <option value="single" <?php if($marital_status == 'single'):?> selected="selected"<?php endif;?> > Single </option>

							                    <option value="married" <?php if($marital_status == 'married'):?> selected="selected"<?php endif;?> > Married </option>

							                    <option value="divorced" <?php if($marital_status == 'divorced'):?> selected="selected"<?php endif;?> > Divorced </option> 
						                  	</select>
						                </div>
				                    </div> 
								</div>


								<div class="row">
									<?php   
										

										$client_data  =  $ci->Training_model->getAll2('xin_clients_family',' client_id = "'.$client_id.'" ');

										if (!empty($client_data)) 
										{
											foreach ($client_data as $key => $value) 
											{
												echo "<div class='col-md-3' >";
													echo "<img class='profile-user-img img-responsive img-circle' src='".base_url()."uploads/clients/".$value->client_profile."'  >";
												echo "</div >";
											}
										}
										 
									?>
								</div>
				            </div>

				        </div>

				        <?php if (empty($hospital_id)): ?> 
				            <div class="row">
				            	<div class="col-md-6">

					                <fieldset class="form-group">

					                  <label for="logo"><?php echo $this->lang->line('xin_project_client_photo');?></label>

					                  <input type="file" class="form-control-file" id="client_photo" name="client_photo">

					                  <br />

					                  <small><?php echo $this->lang->line('xin_company_file_type');?></small>

					                </fieldset>

				              	</div> 


				              	<div class="col-md-6">

					                <?php if($client_profile!='' || $client_profile!='no-file'){?>

					                <span class="avatar box-40 mr-0-5"> <img class="d-block ui-w-40 rounded-circle" src="<?php echo base_url();?>uploads/clients/<?php echo $client_profile;?>" alt="" width="50px"> </span>

					                <?php } ?>

				              	</div>	
				            </div>
			            <?php endif ?>

			            

			            <?php if (empty($hospital_id)): ?> 
			            <hr>
			            <div class="row">
			            	<div class="col-md-12">
			            		<h4 style=" text-align: center;">Medical History/Health Declaration</h4>
					           	<p>Have you or anyone mentioned in this form been diagnosed with any of these diseases below? Please tick as appropriate</p>   

					           	<div class="row">
					           		<div class="col-md-3 form-group">
				           				<input type="checkbox" name="diseases[]" value="Sickle Cell Disease" >   Sickle Cell Disease	
					           		</div>
					           		<div class="col-md-3 form-group">
				           				<input type="checkbox" name="diseases[]" value="Kidney Disease" >   Kidney Disease	
					           		</div>

					           		<div class="col-md-3 form-group">
				           				<input type="checkbox" name="diseases[]" value="Epilepsy" >   Epilepsy
					           		</div>

					           		<div class="col-md-3 form-group" style="font-size: small;">
			           					<input type="checkbox" name="diseases[]" value="Cancer(Prostate-Cervical)" >   Cancer(Prostate,Cervical)	
					           		</div>


					           		<div class="col-md-3 form-group">
				           				<input type="checkbox" name="diseases[]" value="Diaberes Mellitus" >   Diaberes Mellitus
					           		</div>


					           		<div class="col-md-3 form-group">
				           				<input type="checkbox" name="diseases[]" value="Asthma" >   Asthma
					           		</div>


					           		<div class="col-md-3 form-group">
				           				<input type="checkbox" name="diseases[]" value="HIV-AIDS" >   HIV/AIDS	
					           		</div>


					           		<div class="col-md-3 form-group">
				           				<input type="checkbox" name="diseases[]" value="Surgeries" >   Surgeries
					           		</div>


					           		<div class="col-md-3 form-group">
				           				<input type="checkbox" name="diseases[]" value="Hypertension" >   Hypertension
					           		</div>


					           		<div class="col-md-3 form-group">
				           				<input type="checkbox" name="diseases[]" value="Cataract" >   Cataract	
					           		</div>

					           		<div class="col-md-3 form-group">
				           				<input type="checkbox" name="diseases[]" value="Goiter" >   Goiter	
					           		</div>

					           		<div class="col-md-3 form-group">
				           				<input type="checkbox" name="diseases[]" value="Peptic Ulcer" >   Peptic Ulcer
					           		</div>

					           		<div class="col-md-3 form-group">
				           				<input type="checkbox" name="diseases[]" value="Hepatitis" >   Hepatitis	
					           		</div>


					           		<div class="col-md-3 form-group">
			           					<input type="checkbox" name="diseases[]" value="Glaucoma" >   Glaucoma	
					           		</div>

					           		<div class="col-md-3 form-group">
				           				<input type="checkbox" name="diseases[]" value="Tuberculosis" >   Tuberculosis
					           		</div>

					           		<div class="col-md-3 form-group">
				           				<input type="checkbox" name="diseases[]" value="Hemorrhoids" >   Hemorrhoids	
					           		</div>
					           		
					           	</div>
					           
					           		 
					           

			              	</div> 

			              	<div  class="col-md-12">
			              		<div class="form-group">
			              			<label>If any is ticked kindly details here</label>
			              			<textarea class="form-control" rows="5" name="comment"></textarea>
			              		</div>
			              	</div>
			               


			               	<div  class="col-md-12">
			              		<h4  style=" text-align: center;">Membership Declaration</h4>
			              		<ol>
			              			<li>I certify that all the information provided in this form is true to the best of my knowledge.</li>
			              			<li>I declare that any false information given by me in the above questionnaire or the non-disclosure of a fact if identified later will render the membership null and void.</li>
			              			<li>I agree to abide by the terms and conditions governing membership and access to the health benefits covered by the plan in which i have enrolled.</li>
			              			<li>I hereby give my permission to care givers to release any information requested by Precious Health Care Ltd,with respect to any clain or delivery of medical services on my account or that of my dependants for the sake of quality control,statistical and legal analysis.</li>
			              		</ol>
			              	</div>
			            </div>
			            <?php endif ?>

			            <?php if ($ind_family == 'family'): ?>
			      <div class="row">

			            	<div class="col-md-4">  
        	<a  class="btn btn-info  " href="<?php echo base_url() ?>client/profile/add_dependant">Add your Dependant</a>
				              	</div>   
				            </div>
			            <?php endif ?>
			            

			            <hr>
			            <div class="row">

			            	<div class="col-md-4">

				                <div class="form-group">

				                  	<label for="pkg">Subscription Package</label>

				                  	<input  style="background-color: #940024;color: white;"  type="text" class="form-control" disabled="" readonly=""  value="<?php if(isset($subs_pkg)){ echo $subs_pkg; } ?>"> 

				                </div>

			              	</div>
			            </div> 
			            <div class="row">
			              	<div class="col-md-3">

				                <div class="form-group">
			 						<button class="btn btn-primary " disabled="">Upgrade</button>
				                </div>

			              	</div> 
			            </div>

			        </div>

			        <?php if (empty($hospital_id)): ?> 
		      		<div class="form-actions"> <?php echo form_button(array('name' => 'hrsale_form', 'type' => 'submit', 'class' => $this->Xin_model->form_button_class(), 'content' => '<i class="fa fa fa-check-square-o"></i> '.$this->lang->line('xin_save'))); ?> 
		      		</div>
		      		<?php endif ?>

	     		<?php echo form_close(); ?> 
	     	</div>

      	</div>

    	</div>

  	</div>

  	<div class="col-md-9 current-tab <?php echo $get_animate;?>" id="medical_history" style="display:none;">

	    <div class="box">

	      	<div class="box-header with-border">

		        <h3 class="box-title"> <?php echo $this->lang->line('xin_e_details_cpassword');?> </h3>

	      	</div>

	      	<div class="box-body">

		        <div class="card-block">

		          	<?php $attributes = array('name' => 'e_change_password', 'id' => 'e_change_password', 'autocomplete' => 'off');?>

		          	<?php $hidden = array('u_basic_info' => 'UPDATE');?>

		          	<?php echo form_open('client/profile/change_password/', $attributes, $hidden);?>

			          	<?php

			          	$data_usr11 = array(

			                'type'  => 'hidden',

			                'name'  => 'client_id',

			                'value' => $session['client_id'],

			         	);

		        		echo form_input($data_usr11);

		        		?>

		          		<?php if($this->input->get('change_password')):?>

		          			<input type="hidden" id="change_pass" value="<?php echo $this->input->get('change_password');?>" />

		          		<?php endif;?>

			          	<div class="row">

			             

			             
			          	</div>

			          	<div class="row">

			            

			          	</div>

		          	<?php echo form_close(); ?> 
	          	</div>

	      	</div>

	    </div>

  	</div>

</div>

