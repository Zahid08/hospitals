<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Invoices_model extends CI_Model {

 

    public function __construct()

    {

        parent::__construct();

        $this->load->database();

    }

 

	public function get_invoices()

	{

	  return $this->db->get("xin_hrsale_invoices");

	}

	public function get_invoices_paid($from=null,$to=null)
	{

		if (!is_null($from) AND !is_null($to)) {
			$this->db->where("status = 1 AND created_at BETWEEN '$from' AND '$to'");
			return $this->db->get('xin_hrsale_invoices');
		
		}else{
		 	return $this->db->get_where("xin_hrsale_invoices",array('status'=>'1'));

		}


	}

	public function get_invoices_paid_before($from)
	{
		$this->db->where("status = 1 AND created_at < '$from'");
		return $this->db->get('xin_hrsale_invoices');

	}

	public function get_invoices_unpaid()
	{

	  return $this->db->get_where("xin_hrsale_invoices",array('status'=>'0'));

	}

	public function get_invoices_expire()

	{
		$expire_date = date_add(date_create(date("Y-m-d")),date_interval_create_from_date_string('90 days'));
		// print_r();die;
		$expire_date = date_format($expire_date,"Y-m-d");
		$sql = 'SELECT * FROM `xin_hrsale_invoices` where invoice_end_date <= ? AND status=0';
		$binds = array($expire_date);
		$query = $this->db->query($sql, $binds);
		return $query;

	}

	public function get_invoices_date_range($from,$to,$org=null)

	{
		if ($org != null) {
			$sql = 'SELECT * FROM `xin_hrsale_invoices` where company_name = ? AND invoice_date BETWEEN ? AND ?';
			$binds = array($org,$from,$to);
		}else{
			$sql = 'SELECT * FROM `xin_hrsale_invoices` where invoice_date BETWEEN ? AND ?';
			$binds = array($from,$to);
		}
		$query = $this->db->query($sql, $binds);
		return $query;

	}

	public function get_invoices_date_range_paid($from,$to)

	{
		$sql = 'SELECT * FROM `xin_hrsale_invoices` where status = 1 AND invoice_date BETWEEN ? AND ?';
		$binds = array($from,$to);
		$query = $this->db->query($sql, $binds);
		return $query;

	}

	public function get_invoices_date_range_paid2($from,$to)

	{
		$sql = 'SELECT * FROM `xin_hrsale_invoices` where status = 1 AND created_at BETWEEN ? AND ?';
		$binds = array($from,$to);
		$query = $this->db->query($sql, $binds);
		return $query;

	}

	public function get_invoices_date_range_unpaid($from,$to)

	{
		$sql = 'SELECT * FROM `xin_hrsale_invoices` where status = 0 AND invoice_date BETWEEN ? AND ?';
		$binds = array($from,$to);
		$query = $this->db->query($sql, $binds);
		return $query;

	}

	public function get_invoices_info($id)
	{

	  return $this->db->get_where("xin_hrsale_invoices",array('invoice_id'=>$id));

	}


	public function get_invoices_organization($id)
	{

	  return $this->db->get_where("xin_hrsale_invoices",array('company_name'=>$id));

	}

	public function get_invoice_organization_status($id,$status,$from=null,$to=null)
	{
		
		if (is_null($from) AND is_null($to)) {
			if ($status == 'paid') {
				return $this->db->get_where("xin_hrsale_invoices",array('company_name'=>$id,'status'=>1));
			}else{
				return $this->db->get_where("xin_hrsale_invoices",array('company_name'=>$id,'status'=>0));
			}
		}else{
			$from = $from." 00:00:01";
			$to = $to." 23:59:59";
			if ($status == 'paid') {
				$this->db->where("company_name = '$id' AND status = 1 AND created_at BETWEEN '$from' AND '$to'");
				return $this->db->get("xin_hrsale_invoices");
			}else{
				$this->db->where("company_name = '$id' AND status = 0 AND created_at BETWEEN '$from' AND '$to'");
				return $this->db->get("xin_hrsale_invoices");
			}
		}

	}

	public function get_location_info($id)
	{

	  return $this->db->get_where("xin_location",array('location_id'=>$id));

	}

	public function get_last_invoice($id)
	{
		$sql = 'SELECT * FROM `xin_hrsale_invoices` where company_name = ? ORDER BY created_at DESC LIMIT 1';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		return $query;

	}

	public function get_invoice_filtered($id,$from,$to)
	{
		$sql = 'SELECT * FROM `xin_hrsale_invoices` where company_name = ? AND (invoice_date BETWEEN ? AND ?) ';
		$binds = array($id,$from,$to);
		$query = $this->db->query($sql, $binds);
		// echo $this->db->last_query();die;
		return $query;
	}

	public function get_prev_invoice($comp_id,$date)
	{
		$sql = 'SELECT * FROM `xin_hrsale_invoices` where company_name = ? AND created_at  < ? ORDER BY created_at DESC LIMIT 1';
		$binds = array($comp_id,$date);
		$query = $this->db->query($sql, $binds);
		return $query;

	}


	public function get_client_number($id){
		$client = $this->db->get_where("xin_clients",array('company_name'=>$id));
		return $client->num_rows();		
	}


	public function get_taxes() {

	  return $this->db->get("xin_tax_types");

	}

	 

	public function get_employee_project_invoices($id) {

		

		$sql = 'SELECT * FROM xin_hrsale_invoices WHERE project_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

	 	return $query;

	}

	public function read_invoice_info($id) {

	

		$condition = "invoice_id =" . "'" . $id . "'";

		$this->db->select('*');

		$this->db->from('xin_hrsale_invoices');

		$this->db->where($condition);

		$this->db->limit(1);

		$query = $this->db->get();

		

		if ($query->num_rows() == 1) {

			return $query->result();

		} else {

			return null;

		}

	}

	

	public function read_invoice_items_info($id) {

	

		$condition = "invoice_item_id =" . "'" . $id . "'";

		$this->db->select('*');

		$this->db->from('xin_hrsale_invoices_items');

		$this->db->where($condition);

		$this->db->limit(1);

		$query = $this->db->get();

		

		if ($query->num_rows() == 1) {

			return $query->result();

		} else {

			return null;

		}

	}

	

	public function read_tax_information($id) {

	

		$condition = "tax_id =" . "'" . $id . "'";

		$this->db->select('*');

		$this->db->from('xin_tax_types');

		$this->db->where($condition);

		$this->db->limit(1);

		$query = $this->db->get();

		

		if ($query->num_rows() == 1) {

			return $query->result();

		} else {

			return null;

		}

	}

	

	public function get_invoice_items($id) {

	 	

		$sql = 'SELECT * FROM xin_hrsale_invoices_items WHERE invoice_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds); 

		

  	     return $query->result();

	}	

	public function get_client_invoices($id) {

	 	

		$sql = 'SELECT * FROM xin_hrsale_invoices WHERE client_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds); 

  	     return $query;

	}

	public function get_client_payment_invoices($id) {

	 	

		$sql = 'SELECT * FROM xin_finance_transaction WHERE client_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds); 

  	     return $query;

	}

	public function get_client_invoice_payments_all() {

	 	

		$sql = 'SELECT * FROM xin_finance_transaction WHERE invoice_id != ""';

		$query = $this->db->query($sql); 

  	     return $query;

	}

	public function get_all_paid_invoices() {

		$sql = 'SELECT * FROM xin_hrsale_invoices WHERE status = 1';

		$query = $this->db->query($sql); 

  	     return $query;

	}

	// last 4 projects

	public function last_five_client_invoices($id)

	{

	     $sql = 'SELECT * FROM xin_hrsale_invoices where client_id = ? order by invoice_id desc limit ?';

		 $binds = array($id,5);

		 $query = $this->db->query($sql, $binds); 

		 

  	  	  return $query->result();

	}

	

	// Function to add record in table

	public function add_invoice_record($data){

		$this->db->insert('xin_hrsale_invoices', $data);

		if ($this->db->affected_rows() > 0) {

			return $this->db->insert_id();

		} else {

			return false;

		}

	}

	

	// Function to add record in table

	public function add_invoice_items_record($data){

		$this->db->insert('xin_hrsale_invoices_items', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}

	

	// Function to add record in table

	public function add_tax_record($data){

		$this->db->insert('xin_tax_types', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}

	

	// Function to update record in table

	public function update_tax_record($data, $id){

		$this->db->where('tax_id', $id);

		if( $this->db->update('xin_tax_types',$data)) {

			return true;

		} else {

			return false;

		}		

	}

	

	// Function to Delete selected record from table

	public function delete_record($id){

		$this->db->where('invoice_id', $id);

		$this->db->delete('xin_hrsale_invoices');

		

	}

	

	// Function to Delete selected record from table

	public function delete_invoice_items($id){

		$this->db->where('invoice_id', $id);

		$this->db->delete('xin_hrsale_invoices_items');

		

	}

	

	// Function to Delete selected record from table

	public function delete_invoice_items_record($id){

		$this->db->where('invoice_item_id', $id);

		$this->db->delete('xin_hrsale_invoices_items');

		

	}

	

	// Function to Delete selected record from table

	public function delete_tax_record($id){

		$this->db->where('tax_id', $id);

		$this->db->delete('xin_tax_types');

		

	}

	

	// Function to update record in table

	public function update_invoice_record($data, $id){

		// print_r($data);die;
		$this->db->where('invoice_id', $id);


		if( $this->db->update('xin_hrsale_invoices',$data)) {

			return true;

		} else {

			return false;

		}		

	}

	

	// Function to update record in table

	public function update_invoice_items_record($data, $id){

		$this->db->where('invoice_item_id', $id);

		if( $this->db->update('xin_hrsale_invoices_items',$data)) {

			return true;

		} else {

			return false;

		}		

	}

	

}

?>