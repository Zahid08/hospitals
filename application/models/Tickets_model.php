<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Tickets_model extends CI_Model { 

    public function __construct()

    {

        parent::__construct();

        $this->load->database();

    }

 

	public function get_tickets() {

	  return $this->db->get("xin_support_tickets");

	}

	public function get_tickets_date_range($from,$to,$eid = null) {
		
		if($eid != null){
			$sql = 'SELECT * FROM xin_support_tickets WHERE employee_id = ? AND created_at BETWEEN ? AND ?';
			$binds = array($eid,$from,$to);

		}else{
			$sql = 'SELECT * FROM xin_support_tickets WHERE created_at BETWEEN ? AND ?';
			$binds = array($from,$to);

		}
		$query = $this->db->query($sql,$binds);
		// echo $this->db->last_query();die;
		if ($query->num_rows() > 0) {

			return $query;

		} else {

			return null;

		}

	}

	public function get_memos() {

	  return $this->db->get("xin_support_memo");

	}

	public function get_memo_approved() {

		$sql = 'SELECT * FROM xin_support_memo WHERE approval >= 1';

		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			return $query;

		} else {

			return null;

		}

	}

	public function get_memo_category_info($cat,$from = null, $to = null) {

		if ($from != null && $to != null) {
			$from = $from." 00:00:01";
			$to = $to." 23:59:59";
			$sql = 'SELECT * FROM xin_support_memo WHERE approval = ? AND created_at BETWEEN ? AND ?';
			$binds = array($cat,$from,$to);
		}else{
			$sql = 'SELECT * FROM xin_support_memo WHERE approval = ?';
			$binds = array($cat);
		}

		$query = $this->db->query($sql,$binds);

		if ($query->num_rows() > 0) {

			return $query;

		} else {

			return null;

		}

	}

	public function get_memos_id($id) {

	  $sql = 'SELECT * FROM xin_support_memo WHERE id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		

		if ($query->num_rows() > 0) {

			return $query;

		} else {

			return null;

		}

	}

	public function get_memo_category($id) {

	  $sql = 'SELECT * FROM xin_memo_type WHERE id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		

		if ($query->num_rows() > 0) {

			return $query;

		} else {

			return null;

		}

	}

	public function get_employees_except($id) {

		$option = '';
		$count = 1;
		foreach ($id as $i) {
			if ($count == 1) {
				$option .= " user_id != $i";
			}else{
				$option .= " AND user_id != $i";
			}
			$count++;
		}

		// print_r($option);die;

	  $sql = 'SELECT * FROM xin_employees WHERE '.$option;

		$query = $this->db->query($sql);

		// echo $this->db->last_query();die;

		
		if ($query->num_rows() > 0) {

			return $query;

		} else {

			return null;

		}

	}

	public function get_memos_by_id($id) {

	  $sql = 'SELECT * FROM xin_support_memo WHERE made_by = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		

		if ($query->num_rows() > 0) {

			return $query;

		} else {

			return null;

		}

	}

	public function get_memos_by_id_date_range($id,$from,$to) {

	  $sql = 'SELECT * FROM xin_support_memo WHERE made_by = ? AND created_at BETWEEN ? AND ?';

		$binds = array($id,$from,$to);

		$query = $this->db->query($sql, $binds);

		

		if ($query->num_rows() > 0) {

			return $query;

		} else {

			return null;

		}

	}

	public function get_memos_to_id($id) {

	  $sql = 'SELECT * FROM xin_support_memo WHERE FIND_IN_SET(?,send_to)';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		// echo $this->db->last_query();die;	
		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return null;

		}

	}

	 

	public function read_ticket_information($id) 
	{

		$sql = 'SELECT * FROM xin_support_tickets WHERE ticket_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return null;

		}

	}

	public function read_memo_information($id) 
	{

		$sql = 'SELECT * FROM xin_support_memo WHERE id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return null;

		}

	}

	

	

	// Function to add record in table

	public function add($data){

		$this->db->insert('xin_support_tickets', $data);

		if ($this->db->affected_rows() > 0) {

			return $this->db->insert_id();

		} else {

			return false;

		}

	}

	// Function to add record in table

	public function add_memo($data){

		$this->db->insert('xin_support_memo', $data);

		if ($this->db->affected_rows() > 0) {

			return $this->db->insert_id();

		} else {

			return false;

		}

	}

	

	// Function to add record in table

	public function add_comment($data){

		$this->db->insert('xin_tickets_comments', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}

	

	// Function to add record in table

	public function add_new_attachment($data){

		$this->db->insert('xin_tickets_attachment', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}

	

	// Function to Delete selected record from table

	public function delete_record($id){

		$this->db->where('ticket_id', $id);

		$this->db->delete('xin_support_tickets');
	}

	public function delete_memo($id){

		$this->db->where('id', $id);

		$this->db->delete('xin_support_memo');
	}

	

	// Function to Delete selected record from table

	public function delete_comment_record($id){

		$this->db->where('comment_id', $id);

		$this->db->delete('xin_tickets_comments');

		

	}

	

	// Function to Delete selected record from table

	public function delete_attachment_record($id){

		$this->db->where('ticket_attachment_id', $id);

		$this->db->delete('xin_tickets_attachment');

		

	}

	

	public function get_employee_tickets($id) {

	 	

		$sql = 'SELECT * FROM xin_support_tickets WHERE employee_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		return $query;

	}

	public function get_company_tickets($id) {

	 	

		$sql = 'SELECT * FROM xin_support_tickets WHERE company_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		return $query;

	}

	

	// Function to update record in table

	public function update_record($data, $id){

		$this->db->where('ticket_id', $id);

		if( $this->db->update('xin_support_tickets',$data)) {

			return true;

		} else {

			return false;

		}		

	}

	

	// Function to update record in table

	public function assign_ticket_user($data, $id){

		$this->db->where('ticket_id', $id);

		if( $this->db->update('xin_support_tickets',$data)) {

			return true;

		} else {

			return false;

		}		

	}

	

	// Function to update record in table

	public function update_status($data, $id){

		$this->db->where('ticket_id', $id);

		if( $this->db->update('xin_support_tickets',$data)) {

			return true;

		} else {

			return false;

		}		

	}

	// Function to update record in table

	public function update_memo($data, $id){

		$this->db->where('id', $id);

		if( $this->db->update('xin_support_memo',$data)) {

			return true;

		} else {

			return false;

		}		

	}

	

	// Function to update record in table

	public function update_note($data, $id){

		$this->db->where('ticket_id', $id);

		if( $this->db->update('xin_support_tickets',$data)) {

			return true;

		} else {

			return false;

		}		

	}

	

	// get comments

	public function get_comments($id) {

	

		$sql = 'SELECT * FROM xin_tickets_comments WHERE ticket_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		

		return $query;

	}

	

	// get attachments

	public function get_attachments($id) {

	

		$sql = 'SELECT * FROM xin_tickets_attachment WHERE ticket_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		

		return $query;

	}

	

	// get all ticket users

	public function read_ticket_users_information($id) {

	

		$sql = 'SELECT * FROM xin_support_tickets WHERE ticket_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		

		return $query->result();

	}

}

?>