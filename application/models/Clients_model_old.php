<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Clients_model extends CI_Model

	{



    public function __construct()

    {

        parent::__construct();

        $this->load->database();

    }



	public function get_clients() {

	  return $this->db->get("xin_clients");

	}



	public function get_all_clients() {

	  $query = $this->db->get("xin_clients");

	  return $query->result();

	}



	public function read_client_info($id) {



		$sql = 'SELECT * FROM xin_clients WHERE client_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}



	public function read_hospital_info($id) {



		$sql = 'SELECT * FROM xin_hospital WHERE hospital_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}



	// Read data using username and password

	public function login($data) {



		$sql = 'SELECT * FROM xin_clients WHERE email = ? AND is_active = ?';

		$binds = array($data['username'],1);

		$query = $this->db->query($sql, $binds);



	    $options = array('cost' => 12);

		$password_hash = password_hash($data['password'], PASSWORD_BCRYPT, $options);

		if ($query->num_rows() > 0) {

			$rw_password = $query->result();

			if(password_verify($data['password'],$rw_password[0]->client_password)){

				return true;

			} else {

				return false;

			}

		} else {

			return false;

		}

	}



	// Read data using username and password

	public function Hospital_login($data)
	{



		$sql = 'SELECT * FROM `xin_hospital` WHERE email = "'.$data['username'].'" AND  password = "'.$data['password'].'" ';

		// $binds = array($data['username'],1);

		// $query = $this->db->query($sql, $binds);		
		$query = $this->db->query($sql);



	 	//    $options = array('cost' => 12);

		// $password_hash = password_hash($data['password'], PASSWORD_BCRYPT, $options);

		if ($query->num_rows() > 0)
		{

			// $rw_password = $query->result();

			// if(password_verify($data['password'],$rw_password[0]->client_password)){

				return true;

			// } else {

			// 	return false;

			// }

		} else {

			return false;

		}

	}



	// get single user > by email

	public function read_client_info_byemail($email) {



		$sql = 'SELECT * FROM xin_clients WHERE email = ?';

		$binds = array($email);

		$query = $this->db->query($sql, $binds);



		return $query;

	}



	// Read data from database to show data in admin page

	public function read_client_information($username)
	{



		$sql = 'SELECT * FROM xin_clients WHERE email = ?';

		$binds = array($username);

		$query = $this->db->query($sql, $binds);



		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}




	public function read_hospital_information($data)
	{


		$username = $data['username'];
		$password = $data['password'];
		$sql = 'SELECT * FROM `xin_hospital` WHERE email = "'.$username.'" AND  password = "'.$password.'" ';

		// $binds = array($username);

		// $query = $this->db->query($sql, $binds);
		$query = $this->db->query($sql);



		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}



	// Function to add record in table

	public function add($data){

		$this->db->insert('xin_clients', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}

	// Function to add record in table /* Wahid */

	public function add_diagnose_client($data,$services_ids,$drugs_ids){

		$this->db->insert('xin_clients_diagnose', $data);
		$insert_id = $this->db->insert_id();
		// echo "qeruy wasss:".$this->db->last_query();
		// die;
		if ($this->db->affected_rows() > 0) {

			foreach ($services_ids as $key => $value) {
				$data = array(
					'diagnose_mains_id'         => $insert_id,
					'diagnose_services_id'         => $value
				);
				$this->db->insert('xin_clients_diagnose_services', $data);
			}
			foreach ($drugs_ids as $key => $value) {
				$data = array(
					'diagnose_maind_id'         => $insert_id,
					'diagnose_drugs_id'         => $value
				);
				$this->db->insert('xin_clients_diagnose_drugs', $data);
			}

			return true;

		} else {

			return false;

		}

	}
	public function edit_diagnose_client($data,$services_ids,$drugs_ids,$id){

		$this->db->where('diagnose_id', $id);
    	$this->db->update('xin_clients_diagnose', $data);
		// $this->db->insert('xin_clients_diagnose', $data);
		$insert_id = $id;
		// echo "qeruy wasss:".$this->db->last_query();
		// die;
		if ($this->db->affected_rows() > 0) {

			if(empty($services_ids)){
				$this->db->delete('xin_clients_diagnose_services', array('diagnose_mains_id' => $id));
			} else {
				$this->db->delete('xin_clients_diagnose_services', array('diagnose_mains_id' => $id));
				foreach ($services_ids as $key => $value) {
					$data = array(
						'diagnose_mains_id'         => $insert_id,
						'diagnose_services_id'         => $value
					);
					$this->db->insert('xin_clients_diagnose_services', $data);
				}
			}
			if(empty($drugs_ids)){
				$this->db->delete('xin_clients_diagnose_drugs', array('diagnose_maind_id' => $id));
			} else {
				$this->db->delete('xin_clients_diagnose_drugs', array('diagnose_maind_id' => $id));
				foreach ($drugs_ids as $key => $value) {
					$data = array(
						'diagnose_maind_id'         => $insert_id,
						'diagnose_drugs_id'         => $value
					);
					$this->db->insert('xin_clients_diagnose_drugs', $data);
				}
			}

			return true;

		} else {

			return false;

		}

	}
	// Read data from database to show data in admin page

	public function read_individual_admin_info($id)
	{

		if(strlen($id) == 1) {

			$sql = "SELECT * FROM xin_employees WHERE user_id = '".$id."'";

			$query = $this->db->query($sql);



			if ($query->num_rows() > 0) {

				return $query->row();

			} else {

				return false;

			}
		} else {

			$sql = "SELECT * FROM xin_employees WHERE FIND_IN_SET(user_id,'".$id."')";

			$query = $this->db->query($sql);



			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		}

	}

	public function get_filter_result($from,$to)
	{
		$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND (diagnose_date >= '".$from."' AND diagnose_date <= '".$to."') ORDER BY C1.diagnose_id DESC";

		$query = $this->db->query($sql);



		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}
	}
	public function read_individual_hospital_diagnose_clients($hid,$edit_id)
	{

		if($edit_id) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_id = '".$edit_id."'";

			$query = $this->db->query($sql);

			if ($query->num_rows() > 0) {

				return $query->row();

			} else {

				return false;

			}
		}

		if($hid) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id ORDER BY C1.diagnose_id DESC";

			$query = $this->db->query($sql);



			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		} else {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id ORDER BY C1.diagnose_id DESC";

			$query = $this->db->query($sql);



			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		}

	}

	public function read_individual_hospital_diagnose_services($id)
	{



		$sql = "SELECT * FROM xin_services_hospital S1 INNER JOIN xin_clients_diagnose_services S2 ON S2.diagnose_services_id = S1.id WHERE S2.diagnose_mains_id = '".$id."'";

		$query = $this->db->query($sql);



		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}

	public function read_individual_hospital_diagnose_drugs($id)
	{



		$sql = "SELECT * FROM xin_hospital_drugs D1 INNER JOIN xin_clients_diagnose_drugs D2 ON D2.diagnose_drugs_id = D1.drug_id WHERE D2.diagnose_maind_id = '".$id."'";

		$query = $this->db->query($sql);



		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}

	public function view_individual_hospital_diagnose_clients($id)
	{

		if($id) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_id = '".$id."'";

			$query = $this->db->query($sql);



			if ($query->num_rows() > 0) {

				return $query->row_array();

			} else {

				return false;

			}
		}
	}

	// Function to update record in table

	public function get_last_auth_code() {

		$sql = "SELECT diagnose_generated_code FROM xin_clients_diagnose WHERE diagnose_generated_code != '' ORDER BY diagnose_id DESC LIMIT 1";

		// $binds = array($id);

		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			return $query->row()->diagnose_generated_code;

		} else {

			return false;

		}

	}

	public function get_last_tcid_code() {

		$sql = "SELECT diagnose_transaction_id FROM xin_clients_diagnose WHERE diagnose_transaction_id != '' ORDER BY diagnose_id DESC LIMIT 1";

		// $binds = array($id);

		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			return $query->row()->diagnose_transaction_id;

		} else {

			return false;

		}

	}

	public function diagnose_reject_reason($status,$reason,$id){

		$data = array();
		$admin_id = $this->session->userdata;

		if($status == 3) {
			$data = array(
			    'diagnose_status'      		 => $status,
			    'diagnose_reject_reason'     => $reason,
			    'diagnose_status_approve_by' => $admin_id['user_id']['user_id']
			);
		} else {
			$data = array(
			    'diagnose_bill_status'      		 => $status,
			    'diagnose_bill_reject_reason'     => $reason,
			    'diagnose_bill_approve_by' => $admin_id['user_id']['user_id']
			);
		}
		// print_r($data); die;

		$this->db->where('diagnose_id', $id);
		if( $this->db->update('xin_clients_diagnose',$data)) {
			// echo $this->db->last_query();
			// die;
			return true;

		} else {

			return false;

		}

	}

	public function update_auth_status_again($status,$id){

		$data = array();
		$admin_id = $this->session->userdata;

		$data = array(
		    'diagnose_status'      		 => $status,
		    'diagnose_status_approve_by' => NULL
		);

		$this->db->where('diagnose_id', $id);
		if( $this->db->update('xin_clients_diagnose',$data)) {
			// echo $this->db->last_query();
			// die;
			return true;

		} else {

			return false;

		}

	}

	public function update_diagnose_status_record($status,$code,$id){

		$data = array();
		$admin_id = $this->session->userdata;

		if($code){
			$data = array(
				'diagnose_generated_code'	 => $code,
			    'diagnose_status'      		 => $status,
			    'diagnose_status_approve_by' => $admin_id['user_id']['user_id']
			);
		} else if($status == '3') {
			$data = array(
			    'diagnose_status'      		  => $status,
			    'diagnose_status_approve_by' => $admin_id['user_id']['user_id']
			);
		} else if($status == '1') {
			$data = array(
			    'diagnose_bill_status'        => '1'
			);
		}

		$this->db->where('diagnose_id', $id);

		if( $this->db->update('xin_clients_diagnose',$data)) {
			// echo $this->db->last_query();
			// die;
			return true;

		} else {

			return false;

		}

	}

	public function update_finance_bill_status($status,$code,$id){

		$data = array();
		$admin_id = $this->session->userdata;
		//$var['hospital_id']['hospital_id'];

		$data = array(
		    'diagnose_transaction_id'     => $code,
		    'diagnose_finance_status'     => $status,
		    'diagnose_finance_approve_by' => $admin_id['user_id']['user_id']
		);

		$this->db->where('diagnose_id', $id);

		if( $this->db->update('xin_clients_diagnose',$data)) {

			return true;

		} else {

			return false;

		}

	}

	public function update_diagnose_bill_status_record($status,$id){

		$data = array();
		$admin_id = $this->session->userdata;
		//$var['hospital_id']['hospital_id'];
			// echo "conrolere wahid isasdfa si: ". $id;
			// die;

		$this->db->where('diagnose_id', $id);
	    $query=$this->db->get('xin_clients_diagnose');
	    $result=$query->row();
	    // print_r($result);
	    // die;

	    if($status == '2') {
			$data = array(
			    'diagnose_bill_status' => $status,
			    'diagnose_bill_approve_by' => $admin_id['user_id']['user_id']
			);
	    } else if($status == '3') {

	    	$ids = $result->diagnose_bill_approve_by.",".$admin_id['user_id']['user_id'];

			$data = array(
			    'diagnose_bill_status'     => $status,
			    'diagnose_finance_status'  => '1',
			    'diagnose_bill_approve_by' => $ids
			);
	    } else if($status == '4') {
			$data = array(
			    'diagnose_bill_status' => $status,
			    'diagnose_bill_approve_by' => $admin_id['user_id']['user_id']
			);
	    }

		// $data = array( 
		//     'diagnose_bill_status' => $status,
		//     'diagnose_bill_approve_by' => $var['user_id']['user_id']
		// );

		$this->db->where('diagnose_id', $id);

		if( $this->db->update('xin_clients_diagnose',$data)) {

			return true;

		} else {

			return false;

		}

	}

	public function total_clients($hid)
	{
		$sql = "SELECT * FROM xin_clients WHERE hospital_id = '".$hid."'";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		} else {
			return false;
		}
	}

	public function total_dependants($hid)
	{
		$sql = "SELECT * FROM xin_clients_family WHERE hospital_id = '".$hid."'";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		} else {
			return false;
		}
	}

	public function total_auths()
	{
		$sql = "SELECT * FROM xin_clients_diagnose WHERE diagnose_status = '2'";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		} else {
			return false;
		}
	}

	public function total_bills()
	{
		$sql = "SELECT * FROM xin_clients_diagnose WHERE diagnose_bill_status = '3'";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		} else {
			return false;
		}
	}

	public function select_all_transaction_ids()
	{
		$sql = "SELECT * FROM xin_clients_diagnose D,xin_hospital H WHERE diagnose_transaction_id != '' AND D.diagnose_hospital_id = H.hospital_id AND amount_deduct_status = '0'";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	/* Wahid */


	public function add_dependant($data){

		$this->db->insert('xin_clients_family', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}



	public function add_dependant2($data){

		$this->db->insert('xin_clients_family_req', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}



	// Function to Delete selected record from table

	public function delete_record($id){

		$this->db->where('client_id', $id);

		$this->db->delete('xin_clients');



	}



	// Function to update record in table

	public function update_record($data, $id){

		$this->db->where('client_id', $id);

		if( $this->db->update('xin_clients',$data)) {

			return true;

		} else {

			return false;

		}

	}



	public function update_record_hospital($data, $id){

		$this->db->where('hospital_id', $id);

		if( $this->db->update('xin_hospital',$data)) {

			return true;

		} else {

			return false;

		}

	}

	function get_clients_search($type_id) {
		$sql = 'SELECT * FROM `xin_clients` where company_name = ?';
		$binds = array($type_id);
		$query = $this->db->query($sql, $binds);
		return $query;
	}
}

?>
