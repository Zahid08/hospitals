<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Clients_model extends CI_Model

	{ 

		public function __construct()
		{

			parent::__construct();

			$this->load->database();

		} 

	public function get_clients($id = null) {

		if (is_null($id)) {
		  	return $this->db->get("xin_clients");
		}

	}

	public function get_capitation($from = null, $to = null) {

		if (!is_null($from) && !is_null($to)) {
			$from = $from." 00:00:01";
			$to = $to." 23:59:59";
			$this->db->where("created_at BETWEEN '$from' AND '$to'");
		}

		return $this->db->get("xin_clients_capitation");

	}

	public function get_capitation2($from = null, $to = null) {

		if (!is_null($from) && !is_null($to)) {
			$this->db->where("created_at BETWEEN '$from' AND '$to'");
		}

		return $this->db->get("xin_clients_capitation");

	}

	public function get_capitation2_before($from) {

		if (!is_null($from)) {
			$this->db->where("created_at < '$from'");
		}

		return $this->db->get("xin_clients_capitation");

	}

	public function get_clients_by_type($type) {

	  return $this->db->get_where("xin_clients",array('ind_family'=>$type));

	}

	public function get_clients_by_id($type) {

	  return $this->db->get_where("xin_clients",array('client_id'=>$type));

	}

	public function get_all_clients_hospital($hid) {

	  return $this->db->get_where("xin_clients",array('hospital_id'=>$hid));

	}

	public function get_all_clients_hospital_date_range($hid,$from,$to) {

		$from = $from." 00:00:01";
		$to = $to." 23:59:59";

		$this->db->where("hospital_id = '$hid' AND created_at BETWEEN '$from' AND '$to'");
		 //AND created_at BETWEEN '$from' AND '$to'
		return $this->db->get("xin_clients");
	 	// return $this->db->get_where("xin_clients",array('hospital_id'=>$hid));

	}
	public function get_all_clients_hospital_date_range_new($hid,$from,$to) {

		$from = $from." 00:00:01";
		$to = $to." 23:59:59";

		$this->db->where("hospital_id = '$hid' AND created_at BETWEEN '$from' AND '$to'");
		 //AND created_at BETWEEN '$from' AND '$to'
		return $this->db->get("xin_clients");
	 	// return $this->db->get_where("xin_clients",array('hospital_id'=>$hid));

	}
	public function get_all_clients_hospital_date_range_new_family($hid,$clientid) {

		$from = $from." 00:00:01";
		$to = $to." 23:59:59";

		$this->db->where("hospital_id = '$hid'");
		$this->db->where("client_id = '$clientid'");
		 //AND created_at BETWEEN '$from' AND '$to'
		return $this->db->get("xin_clients_family");
	 	// return $this->db->get_where("xin_clients",array('hospital_id'=>$hid));

	}
	public function get_all_clients_capitation_hospital($hid = null,$type) {

		if ($type == 'p') {
	 		return $this->db->get_where("xin_clients_capitation",array('hcp_code'=>$hid,'type'=>'p'));
		}else{
	 		return $this->db->get_where("xin_clients_capitation",array('hcp_code'=>$hid,'type'=>'d'));
		}

	}

	public function get_all_capitation_hospital($hid) {

	  return $this->db->get_where("xin_clients_capitation",array('hcp_code'=>$hid));

	}

	public function get_all_capitation_hospital2($hcp,$type,$dep = 0) {

	if ($type == 'p') {
	  return $this->db->get_where("xin_clients_capitation",array('hcp_code'=>$hcp,'type'=>'p'));
	}else{
		if ($dep != 1) {
			return $this->db->get_where("xin_clients_capitation",array('hcp_code'=>$hcp,'type'=>'d','is_extra'=>null));
		}else{
			return $this->db->get_where("xin_clients_capitation",array('hcp_code'=>$hcp,'type'=>'d','is_extra'=>'1'));
		}
	}


	}

	public function get_all_capitation_hospital3($hcp,$type,$dep = 0,$from,$to) {

	$from = $from." 00:00:01";
	$to = $to." 23:59:59";	

	// echo $year." - ".$from." - ".$to;die;	

	if ($type == 'p') {
		$sql = 'SELECT * FROM xin_clients_capitation WHERE hcp_code = ? AND type = ? AND created_at BETWEEN ? AND ?';
		$binds = array($hcp,$type,$from,$to);
	  // return $this->db->get_where("xin_clients_capitation",array('hcp_code'=>$hcp,'type'=>'p','created_at'=>"BETWEEN $from AND $to"));
	}else{
		if ($dep != 1) {
			$sql = 'SELECT * FROM xin_clients_capitation WHERE hcp_code = ? AND type = ? AND is_extra IS NULL AND created_at BETWEEN ? AND ?';
			$binds = array($hcp,$type,$from,$to);
			// return $this->db->get_where("xin_clients_capitation",array('hcp_code'=>$hcp,'type'=>'d','is_extra'=>null,'created_at'=>"BETWEEN $from AND $to"));
		}else{
			$sql = 'SELECT * FROM xin_clients_capitation WHERE hcp_code = ? AND type = ? AND is_extra = 1 AND created_at BETWEEN ? AND ?';
			$binds = array($hcp,$type,$from,$to);
			// return $this->db->get_where("xin_clients_capitation",array('hcp_code'=>$hcp,'type'=>'d','is_extra'=>'1','created_at'=>"BETWEEN $from AND $to"));
		}
	}

	$query = $this->db->query($sql, $binds);		

	return $query;

	}

	public function get_all_clients_organization($hid,$from=null,$to=null) {
		if (is_null($from) AND is_null($to)) {
			
			return $this->db->get_where("xin_clients",array('company_name'=>$hid));
		
		}else{
		
			$from = $from." 00:00:01";
			$to = $to." 23:59:59";
			$this->db->where("company_name = '$hid' AND created_at BETWEEN '$from' AND '$to'");
			return $this->db->get('xin_clients');
		
		}


	}

	public function get_all_clients_dependants_organization($id) {

	  return $this->db->get_where("xin_clients_family",array('client_id'=>$id));

	}

	public function get_all_clients_dependants($cid,$sort=null) {

		if (is_null($sort)) {
			return $this->db->get_where("xin_clients_family",array('client_id'=>$cid));
		}else{
			$this->db->where("client_id = '$cid' order by created_on asc");
			return $this->db->get("xin_clients_family");

		}


	}

	public function get_all_clients_dependants_hospital($hid) {

	  return $this->db->get_where("xin_clients_family",array('hospital_id'=>$hid));

	}

		

	public function get_all_clients() {

	  $query = $this->db->get("xin_clients");

	  return $query->result();

	}

	 

	public function read_client_info($id) {

	

		$sql = 'SELECT * FROM xin_clients WHERE client_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}

	public function get_principal_by_organization($id) {

		$sql = "SELECT * FROM xin_clients WHERE company_name = ? AND ind_family = 'family'";

		$binds = array($id);

		$query = $this->db->query($sql, $binds);		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}

	public function read_capitation_client_info($id) {

	

		$sql = 'SELECT * FROM xin_clients_capitation WHERE id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}

	public function read_dependant_info($id) {

		$sql = 'SELECT * FROM xin_clients_family WHERE clients_family_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}



	public function read_hospital_info($id) {

	

		$sql = 'SELECT * FROM xin_hospital WHERE hospital_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}

	public function read_hospital_by_band($id) {

		$sql = 'SELECT * FROM xin_hospital WHERE band_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}
	
	public function fetch_drug_note($id) {

	

		$sql = 'SELECT * FROM drug_notes_adjustments WHERE drug_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}
	
	public function fetch_service_note($id) {

	

		$sql = 'SELECT * FROM service_notes_adjustments WHERE service_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}

	

	// Read data using username and password

	public function login($data) {

	

		$sql = 'SELECT * FROM xin_clients WHERE email = ? AND is_active = ?';

		$binds = array($data['username'],1);

		$query = $this->db->query($sql, $binds);		

		

	    $options = array('cost' => 12);

		$password_hash = password_hash($data['password'], PASSWORD_BCRYPT, $options);

		if ($query->num_rows() > 0) {

			$rw_password = $query->result();

			if(password_verify($data['password'],$rw_password[0]->client_password)){

				return true;

			} else {

				return false;

			}

		} else {

			return false;

		}

	}



	// Read data using username and password

	public function Hospital_login($data) 
	{

	

		$sql = 'SELECT * FROM `xin_hospital` WHERE email = "'.$data['username'].'" AND  password = "'.$data['password'].'" ';

		// $binds = array($data['username'],1);

		// $query = $this->db->query($sql, $binds);		
		$query = $this->db->query($sql);		

		

	 	//    $options = array('cost' => 12);

		// $password_hash = password_hash($data['password'], PASSWORD_BCRYPT, $options);

		if ($query->num_rows() > 0) 
		{

			// $rw_password = $query->result();

			// if(password_verify($data['password'],$rw_password[0]->client_password)){

				return true;

			// } else {

			// 	return false;

			// }

		} else {

			return false;

		}

	}

	

	// get single user > by email

	public function read_client_info_byemail($email) {

	

		$sql = 'SELECT * FROM xin_clients WHERE email = ?';

		$binds = array($email);

		$query = $this->db->query($sql, $binds);

		

		return $query;

	}

	

	// Read data from database to show data in admin page

	public function read_client_information($username) 
	{

	

		$sql = 'SELECT * FROM xin_clients WHERE email = ?';

		$binds = array($username);

		$query = $this->db->query($sql, $binds);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}
	public function updateDays($did){
			$this->db->where('diagnose_id',$did);
            $this->db->from('xin_clients_diagnose');
            $query= $this->db->get();
            if($query->num_rows()!=0){
            	$result=$query->result();
                $date = new DateTime($result[0]->diagnose_date);
                $day=$result[0]->days;
                if(empty($day)){
                	return true;
				}
                $now = new DateTime();
                $date1=date_create("2013-03-15");
                $date2=date_create("2013-12-12");
                $diff=date_diff($date,$now);
                $days=$diff->format("%a");
            	 if($day==0){
            	 	return true;
				 }
				 else{
            	 	if($day<$days){
            	 		$d=0;
					}
					else{
            	 		$d=$day-$days;
					}
                     $data['days']=abs($d);
                     $this->db->where('diagnose_id', $did);

                     if( $this->db->update('xin_clients_diagnose',$data)) {

                         return true;

                     } else {

                         return false;

                     }
                 }
			}


	}




	public function read_hospital_information($data) 
	{

	
		$username = $data['username'];
		$password = $data['password'];
		$sql = 'SELECT * FROM `xin_hospital` WHERE email = "'.$username.'" AND  password = "'.$password.'" ';

		// $binds = array($username);

		// $query = $this->db->query($sql, $binds);
		$query = $this->db->query($sql);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}

	

	// Function to add record in table
public function admissioncomment($data){

		$this->db->insert('admission_approve_comment', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}
	public function add_drug($data){
        $this->db->insert('xin_hospital_drugs', $data);

        if ($this->db->affected_rows() > 0) {

            return true;

        } else {

            return false;

        }
	}
        public function add_services($data){
            $this->db->insert('xin_services_hospital', $data);

            if ($this->db->affected_rows() > 0) {

                return true;

            } else {

                return false;

            }
        }
        public  function del_drug($id){
            $this->db->where('hospital_id', $id);
            if($this->db->delete('xin_hospital_drugs')){
            	return true;
			}
			else{
            	return false;
			}
		}
        public  function del_service($id){
            $this->db->where('hospital_id', $id);
            if($this->db->delete('xin_services_hospital')){
                return true;
            }
            else{
                return false;
            }
        }
	public function add($data){

		$this->db->insert('xin_clients', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}

	// Function to add record in table

	public function add_capitation($data){

		$this->db->insert('xin_clients_capitation', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}
	
	// Function to add record in drug_notes table

	public function insert_drug_notes($data){

		$this->db->insert('drug_notes_adjustments', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}	
	
	// Function to Delete drug record from table

//	public function delete_drug($id){

//		$this->db->where('diagnose_autod_id', $id);

//		$this->db->delete('xin_clients_diagnose_drugs');		

//	}
	
	// Function to Update drug record from table

	public function modify_drug_status($id){
		 
		$data = array(
        	  'diagnose_drug_status' => '1'
        	
		);

		$this->db->where('diagnose_autod_id', $id);

		if( $this->db->update('xin_clients_diagnose_drugs',$data)) {

			return true;

		} else {

			return false;

		}		

	}
	
	// Function to update bill in table

	public function update_bill($amount, $id){
	
		$data = array (
		        'diagnose_total_sum' => $amount,
		        'bill_updated_status' => '1'		        
		);

		$this->db->where('diagnose_id', $id);
		if( $this->db->update('xin_clients_diagnose', $data)) {	
		
			return true;

		} else {

			return false;

		}	

	}

	//flag or flag the
      public function update_bill_status($isFlag,$did){
		if($did){
			  $this->db->where('diagnose_id',$did);
			  $query=$this->db->get('xin_clients_diagnose');
			 $result=$query->row();
            $data = array (
                'is_flag' =>$isFlag,
            );
            $this->db->where('diagnose_client_id', $result->diagnose_client_id);
            if( $this->db->update('xin_clients_diagnose', $data)) {

                return true;

            } else {

                return false;

            }

        }
		else{
			return false;
		}
	  }

	// Function to add record in service_notes table

	public function insert_service_notes($data){

		$this->db->insert('service_notes_adjustments', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}

	// Function to Update drug record from table

	public function modify_service_status($id){
		 
		$data = array(
        	  'diagnose_service_status' => '1'
        	
		);

		$this->db->where('diagnose_autos_id', $id);

		if( $this->db->update('xin_clients_diagnose_services',$data)) {

			return true;

		} else {

			return false;

		}		

	}
	public function getoneinfo($hostelids){
		$sql = 'SELECT extra_services,extra_drug,diagnose_total_sum,diagnose_multiple_drugs_value,diagnose_quantity FROM `xin_clients_diagnose` WHERE diagnose_id = "'.$hostelids.'"';
		$query = $this->db->query($sql);
		return $query->row();
	}
	// Function to add record in table /* Wahid */

	public function add_diagnose_client($data,$services_ids,$drugs_ids){

		$this->db->insert('xin_clients_diagnose', $data);
		$insert_id = $this->db->insert_id();
		// echo "qeruy wasss:".$this->db->last_query();
		// die;
		if ($this->db->affected_rows() > 0) {

			if($services_ids){
			foreach ($services_ids as $key => $value) {
				$data = array(
					'diagnose_mains_id'         => $insert_id,
					'diagnose_services_id'         => $value
				);
				$this->db->insert('xin_clients_diagnose_services', $data);
			}
			}
			if($drugs_ids){
			foreach ($drugs_ids as $key => $value) {
				$data = array(
					'diagnose_maind_id'         => $insert_id,
					'diagnose_drugs_id'         => $value
				);
				$this->db->insert('xin_clients_diagnose_drugs', $data);
			}
			}
			return true;

		} else {

			return false;

		}

	}
	public function add_diagnose_client1($data,$services_ids,$drugs_ids,$quantity,$service_quantity){

		$this->db->insert('xin_clients_diagnose', $data);
		$insert_id = $this->db->insert_id();
		// echo "qeruy wasss:".$this->db->last_query();
		// die;
		if ($this->db->affected_rows() > 0) {

			if($services_ids){
			foreach ($services_ids as $key => $value) {
				$data = array(
					'diagnose_mains_id'         => $insert_id,
					'diagnose_quantity'         => $service_quantity[$key],
					'diagnose_services_id'         => $value,
				);
				$this->db->insert('xin_clients_diagnose_services', $data);
			}
			}
			if($drugs_ids){
			foreach ($drugs_ids as $key => $value) {
				$data = array(
					'diagnose_maind_id'         => $insert_id,
					'diagnose_quantity'         => $quantity[$key],
					'diagnose_drugs_id'         => $value
				);
				$this->db->insert('xin_clients_diagnose_drugs', $data);
			}
			}
			return true;

		} else {

			return false;

		}

	}
	public function edit_diagnose_client($data,$services_ids,$drugs_ids,$id){

		$this->db->where('diagnose_id', $id);
    	$this->db->update('xin_clients_diagnose', $data);
		// $this->db->insert('xin_clients_diagnose', $data);
		$insert_id = $id;
		// echo "qeruy wasss:".$this->db->last_query();
		// die;
		if ($this->db->affected_rows() > 0) {

			if(empty($services_ids)){
				$this->db->delete('xin_clients_diagnose_services', array('diagnose_mains_id' => $id)); 
			} else {
				$this->db->delete('xin_clients_diagnose_services', array('diagnose_mains_id' => $id)); 
				foreach ($services_ids as $key => $value) {
					$data = array(
						'diagnose_mains_id'         => $insert_id,
						'diagnose_services_id'         => $value
					);
					$this->db->insert('xin_clients_diagnose_services', $data);
				}
			}
			if(empty($drugs_ids)){
				$this->db->delete('xin_clients_diagnose_drugs', array('diagnose_maind_id' => $id)); 
			} else {
				$this->db->delete('xin_clients_diagnose_drugs', array('diagnose_maind_id' => $id));
				foreach ($drugs_ids as $key => $value) {
					$data = array(
						'diagnose_maind_id'         => $insert_id,
						'diagnose_drugs_id'         => $value
					);
					$this->db->insert('xin_clients_diagnose_drugs', $data);
				}
			}

			return true;

		} else {

			return false;

		}

	}
	// Read data from database to show data in admin page

	public function read_individual_admin_info($id) 
	{

		if(strlen($id) == 1) {

			$sql = "SELECT * FROM xin_employees WHERE user_id = '".$id."'";

			$query = $this->db->query($sql);

			

			if ($query->num_rows() > 0) {

				return $query->row();

			} else {

				return false;

			}
		} else {

			$sql = "SELECT * FROM xin_employees WHERE FIND_IN_SET(user_id,'".$id."')";

			$query = $this->db->query($sql);

			

			if ($query->num_rows() > 0) {

				return $query->row();

			} else {

				return false;

			}
		}

	}
  public function get_flag_filter_result($from,$to){
      $sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND (diagnose_date >= '".$from."' AND diagnose_date <= '".$to."') AND is_flag=1 ORDER BY C1.diagnose_id DESC";

      $query = $this->db->query($sql);



      if ($query->num_rows() > 0) {

          return $query->result();

      } else {

          return false;

      }
  }
	public function get_filter_result($from,$to) 
	{
		 $sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND (diagnose_date >= '".$from."' AND diagnose_date <= '".$to."')   and C1.diagnose_status = 2  ORDER BY C1.diagnose_id DESC";

		$query = $this->db->query($sql);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}
	}
    public function filter_read_individual_hospital_diagnose_clients($hid,$from,$to)
    {
        //$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND (diagnose_date >= '".$from."' AND diagnose_date <= '".$to."') ORDER BY C1.diagnose_id DESC";
        $sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id AND (C1.diagnose_date >= '".$from."' AND C1.diagnose_date <= '".$to."') ORDER BY C1.diagnose_date_time DESC";
        $query = $this->db->query($sql);



        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }
    }
	public function get_adjusted_filter_result($from,$to) 
	{
		$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND (diagnose_date >= '".$from."' AND diagnose_date <= '".$to."') AND C1.bill_updated_status = '1' ORDER BY C1.diagnose_id DESC";

		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}
	}

	public function get_filter_result_transaction_approved($from,$to) 
	{
		$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_transaction_id IS NOT NULL AND (diagnose_date >= '".$from."' AND diagnose_date <= '".$to."') ORDER BY C1.diagnose_id DESC";

		$query = $this->db->query($sql);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}
	}

	public function get_filter_result_hospital($from,$to) 
	{
		$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.send_admission_status !='2' AND C1.is_admin IS NULL AND (diagnose_date >= '".$from."' AND diagnose_date <= '".$to."') ORDER BY C1.diagnose_date_time DESC";

		$query = $this->db->query($sql);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}
	}
public function get_filter_result_hospital_admission($from,$to) 
	{
		$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.send_admission_status='2' AND C1.admission_status=0 AND C1.is_admin IS NULL AND (C1.diagnose_date >= '".$from."' AND C1.diagnose_date <= '".$to."') ORDER BY C1.diagnose_date_time DESC";

		$query = $this->db->query($sql);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}
	}
	public function get_filter_invalid_adminssion_case($from,$to){
        $sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.is_invalid='1'  AND (C1.diagnose_date >= '".$from."' AND C1.diagnose_date <= '".$to."') ORDER BY C1.diagnose_date_time DESC";

        $query = $this->db->query($sql);



        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }
	}
	public function get_filter_result_hospital_admission_case($from,$to) 
	{
		$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.send_admission_status='2' AND C1.admission_status=1 AND C1.is_admin IS NULL AND (C1.diagnose_date >= '".$from."' AND C1.diagnose_date <= '".$to."') ORDER BY C1.diagnose_date_time DESC";

		$query = $this->db->query($sql);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}
	}
	public function get_filter_result_admin($from,$to) 
	{
		$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.is_admin = 1 AND (diagnose_date >= '".$from."' AND diagnose_date <= '".$to."') ORDER BY C1.diagnose_date_time DESC";

		$query = $this->db->query($sql);


		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}
	}

	public function get_filter_result_of_hospital($hid,$from,$to) 
	{
		$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = '".$hid."' AND C1.diagnose_hospital_id = '".$hid."' AND diagnose_finance_status != '' AND (diagnose_date >= '".$from."' AND diagnose_date <= '".$to."') ORDER BY C1.diagnose_id DESC";

		$query = $this->db->query($sql);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}
	}
	
	public function read_individual_hospital_diagnose_clients($hid,$edit_id) 
	{

		if($edit_id) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_date_time = '".$edit_id."'  and C1.diagnose_status = 2 ";

			$query = $this->db->query($sql);

			if ($query->num_rows() > 0) {

				return $query->row();

			} else {

				return false;

			}
		}

		if($hid) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id and C1.diagnose_status = 2  ORDER BY C1.diagnose_date_time DESC";
//diagnose_date_time
			$query = $this->db->query($sql);

			// echo $this->db->last_query();die;
			

			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		} else {

			//$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id ORDER BY C1.diagnose_date_time DESC";
            $sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id and C1.diagnose_status = 2   ORDER BY C1.diagnose_date_time DESC";
			$query = $this->db->query($sql);
			
			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		}

	}
	public function all_flag_bill(){
        $sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND is_flag=1 ORDER BY C1.diagnose_date_time DESC";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }
	}
	public function read_adjusted_individual_hospital_diagnose_clients($hid,$edit_id) 
	{

		if($edit_id) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_date_time = '".$edit_id."' AND C1.bill_updated_status = '1'";

			$query = $this->db->query($sql);

			if ($query->num_rows() > 0) {

				return $query->row();

			} else {

				return false;

			}
		}

		if($hid) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id AND C1.bill_updated_status = '1' ORDER BY C1.diagnose_date_time DESC";

			$query = $this->db->query($sql);

			// echo $this->db->last_query();die;
			

			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		} else {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.bill_updated_status = '1' ORDER BY C1.diagnose_date_time DESC";

			$query = $this->db->query($sql);
			
			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		}

	}

	public function read_individual_hospital_diagnose_clients2($hid,$cid) 
	{

		if($hid) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '$hid' AND H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_id = '$cid' ORDER BY C1.diagnose_date_time DESC";

			$query = $this->db->query($sql);

			// echo $this->db->last_query();die;
			

			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		} else {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id ORDER BY C1.diagnose_date_time DESC";

			$query = $this->db->query($sql);
			
			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		}

	}

	public function read_individual_hospital_diagnose_clients_transaction_approved($hid,$edit_id) 
	{

		if($edit_id) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_date_time = '".$edit_id."'";

			$query = $this->db->query($sql);

			if ($query->num_rows() > 0) {

				return $query->row();

			} else {

				return false;

			}
		}

		if($hid) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id ORDER BY C1.diagnose_date_time DESC";

			$query = $this->db->query($sql);

			// echo $this->db->last_query();die;
			

			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		} else {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_transaction_id IS NOT NULL ORDER BY C1.diagnose_date_time DESC";

			$query = $this->db->query($sql);
			
			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		}

	}

	public function read_individual_hospital_diagnose_clients_hospital($hid,$edit_id) 
	{

		if($edit_id) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_id = '".$edit_id."' AND C1.send_admission_status !=2";

			$query = $this->db->query($sql);

			if ($query->num_rows() > 0) {

				return $query->row();

			} else {

				return false;

			}
		}

		if($hid) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."'  AND C1.send_admission_status !=2 AND H.hospital_id = C1.diagnose_hospital_id ORDER BY C1.diagnose_date_time DESC";

			$query = $this->db->query($sql);

			

			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		} else {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.send_admission_status !=2 AND C1.is_admin IS NULL ORDER BY C1.diagnose_date_time DESC";

			$query = $this->db->query($sql);

			

			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		}

	}
	public function read_individual_hospital_diagnose_clients_hospital_adminssion_case($hid,$edit_id) 
	{

		if($edit_id) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_id = '".$edit_id."' AND C1.send_admission_status=2 AND C1.admission_status=1";

			$query = $this->db->query($sql);

			if ($query->num_rows() > 0) {

				return $query->row();

			} else {

				return false;

			}
		}

		if($hid) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id AND C1.send_admission_status=2  AND C1.admission_status=1 ORDER BY C1.diagnose_date_time DESC";

			$query = $this->db->query($sql);

			

			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		} else {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.send_admission_status='2' AND C1.admission_status=1 AND C1.is_admin IS NULL ORDER BY C1.diagnose_date_time DESC";

			$query = $this->db->query($sql);

			

			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		}

	}
	public function invalid_adminssion_case(){
        $sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.is_invalid='1'  ORDER BY C1.diagnose_date_time DESC";

        $query = $this->db->query($sql);



        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }
	}
public function read_individual_hospital_diagnose_clients_hospital_adminssion($hid,$edit_id) 
	{

		if($edit_id) {
            $sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_id = '".$edit_id."' AND C1.send_admission_status=2 AND C1.admission_status=0";
			//$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_id = '".$edit_id."' AND C1.send_admission_status=2 AND C1.admission_status=1";

			$query = $this->db->query($sql);

			if ($query->num_rows() > 0) {

				return $query->row();

			} else {

				return false;

			}
		}

		if($hid) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id AND C1.send_admission_status=2  AND C1.admission_status=0 ORDER BY C1.diagnose_date_time DESC";

			$query = $this->db->query($sql);

			

			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		} else {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.send_admission_status='2' AND C1.admission_status=0 AND C1.is_admin IS NULL ORDER BY C1.diagnose_date_time DESC";

			$query = $this->db->query($sql);

			

			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		}

	}
	
	public function read_individual_hospital_diagnose_clients_admin($hid,$edit_id) 
	{

		if($edit_id) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_id = '".$edit_id."'";

			$query = $this->db->query($sql);

			if ($query->num_rows() > 0) {

				return $query->row();

			} else {

				return false;

			}
		}

		if($hid) {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE C1.diagnose_hospital_id = '".$hid."' AND H.hospital_id = C1.diagnose_hospital_id ORDER BY C1.diagnose_date_time DESC";

			$query = $this->db->query($sql);

			

			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		} else {

			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.is_admin = 1 ORDER BY C1.diagnose_date_time DESC";

			$query = $this->db->query($sql);

			

			if ($query->num_rows() > 0) {

				return $query->result();

			} else {

				return false;

			}
		}

	}

	public function read_individual_hospital_diagnose_services($id) 
	{

	

		$sql = "SELECT * FROM xin_services_hospital S1 INNER JOIN xin_clients_diagnose_services S2 ON S2.diagnose_services_id = S1.id WHERE S2.diagnose_mains_id = '".$id."' and S2.diagnose_service_status = '0'";

		$query = $this->db->query($sql);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}
	
	public function read_individual_hospital_diagnose_drugs($id) 
	{

		$sql = "SELECT * FROM xin_hospital_drugs D1 INNER JOIN xin_clients_diagnose_drugs D2 ON D2.diagnose_drugs_id = D1.drug_id WHERE D2.diagnose_maind_id = '".$id."' and D2.diagnose_drug_status = '0'";

		$query = $this->db->query($sql);

		if($query->num_rows() > 0) 
		{
			return $query->result();
		} 
		else 
{
			return false;
		}

	}
	
	public function read_individual_hospital_diagnose_actual_drugs($id) 
	{

	

		$sql = "SELECT * FROM xin_hospital_drugs D1 INNER JOIN xin_clients_diagnose_drugs D2 ON D2.diagnose_drugs_id = D1.drug_id WHERE D2.diagnose_maind_id = '".$id."'";

		$query = $this->db->query($sql);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}


	public function read_individual_hospital_diagnose_actual_services($id) 
	{

	

		$sql = "SELECT * FROM xin_services_hospital S1 INNER JOIN xin_clients_diagnose_services S2 ON S2.diagnose_services_id = S1.id WHERE S2.diagnose_mains_id = '".$id."'";

		$query = $this->db->query($sql);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}

	
	public function drugs_sum($id) 
	{

	

		$sql = "SELECT D1.drug_id,SUM(D1.drug_price)*D2.diagnose_quantity as d_id FROM xin_hospital_drugs D1 INNER JOIN xin_clients_diagnose_drugs D2 ON D2.diagnose_drugs_id = D1.drug_id WHERE D2.diagnose_maind_id = '".$id."' and D2.diagnose_drug_status = '0'";

		$query = $this->db->query($sql);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}
	
	public function service_sum($id) 
	{

	

		$sql = "SELECT S1.id,SUM(S1.service_price) as s_id FROM xin_services_hospital S1 INNER JOIN xin_clients_diagnose_services S2 ON S2.diagnose_services_id = S1.id WHERE S2.diagnose_mains_id = '".$id."' and S2.diagnose_service_status = '0'";

		$query = $this->db->query($sql);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}

    public  function flagEnrollee(){
        $this->db->select('*');
        $this->db->from('xin_clients_diagnose');
        $this->db->where('is_flag',1);
        $this->db->group_by('diagnose_client_id');
        $query = $this->db->get();
        if($query->num_rows() != 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    public  function filterEnrolle($from,$to){
        $this->db->select('*');
        $this->db->from('xin_clients_diagnose');
        $this->db->where('xin_clients_diagnose.diagnose_date >= ',$from);
        $this->db->where('xin_clients_diagnose.diagnose_date <= ',$to);
        $this->db->where('xin_clients_diagnose.is_flag',1);
        $this->db->group_by('diagnose_client_id');
        $query = $this->db->get();
        if($query->num_rows() != 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }


 public function get_total_bill($cid){
     $this->db->select('xin_clients_diagnose.diagnose_id');
     $this->db->from('xin_clients_diagnose');
     $this->db->where(array('diagnose_client_id'=>$cid,'diagnose_bill_status'=>3));
     $query = $this->db->get();
     return $query->num_rows();

 }


	public function actual_drugs_sum($id) 
	{

	

		$sql = "SELECT D1.drug_id,SUM(D1.drug_price) as d_id FROM xin_hospital_drugs D1 INNER JOIN xin_clients_diagnose_drugs D2 ON D2.diagnose_drugs_id = D1.drug_id WHERE D2.diagnose_maind_id = '".$id."'";

		$query = $this->db->query($sql);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}
	
	public function actual_service_sum($id) 
	{

	

		$sql = "SELECT S1.id,SUM(S1.service_price) as s_id FROM xin_services_hospital S1 INNER JOIN xin_clients_diagnose_services S2 ON S2.diagnose_services_id = S1.id WHERE S2.diagnose_mains_id = '".$id."'";

		$query = $this->db->query($sql);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}

	}


















	

	public function view_individual_hospital_diagnose_clients($id,$is_capitation = null,$client_id = null) 
	{

		if($id) {

			if ($is_capitation == 1) {
				
				$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname,C4.name as cap_name FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id LEFT JOIN xin_clients_capitation C4 ON C4.id = $client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_id = '".$id."'";
			}else{

				$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_id = '".$id."'";
			}


			$query = $this->db->query($sql);

			// echo $this->db->last_query();die;

			if ($query->num_rows() > 0) {

				return $query->row_array();

			} else {

				return false;

			}
		}
	}

	public function view_all_individual_hospital_diagnose_clients($from = null, $to = null) 
	{
		if (!is_null($from) && !is_null($to)) {
			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_finance_status = '2' AND C1.diagnose_date BETWEEN '$from' AND '$to'";
		}else{
			$sql = "SELECT *,C3.name AS dname, C3.last_name AS dlname, C3.other_name AS doname,C2.name AS cname, C2.last_name AS clname, C2.other_name AS coname FROM xin_hospital H,xin_clients_diagnose C1 LEFT JOIN xin_clients C2 ON C2.client_id = C1.diagnose_client_id LEFT JOIN xin_clients_family C3 ON C3.clients_family_id = C1.diagnose_client_id WHERE H.hospital_id = C1.diagnose_hospital_id AND C1.diagnose_finance_status = '2'";
		}


		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return false;

		}
	}

	// Function to update record in table

	public function get_last_auth_code() {

		$sql = "SELECT diagnose_generated_code FROM xin_clients_diagnose WHERE diagnose_generated_code != '' ORDER BY diagnose_id DESC LIMIT 1";

		// $binds = array($id);

		$query = $this->db->query($sql);		

		if ($query->num_rows() > 0) {

			return $query->row()->diagnose_generated_code;

		} else {

			return false;

		}

	}

	public function get_last_tcid_code() {

		$sql = "SELECT diagnose_transaction_id FROM xin_clients_diagnose WHERE diagnose_transaction_id != '' ORDER BY diagnose_id DESC LIMIT 1";

		// $binds = array($id);

		$query = $this->db->query($sql);		

		if ($query->num_rows() > 0) {

			return $query->row()->diagnose_transaction_id;

		} else {

			return false;

		}

	}
   public function InvalidCase($did){
       $this->db->where('diagnose_id',$did);
       $data = array(
           'admission_status'=> 0,
           'days'     => null,
		   'is_invalid'=>1,
		   'diagnose_bill_status'=>null,
		   'diagnose_status'=>2
       );
       $this->db->update('xin_clients_diagnose',$data);
      return true;
   }
   
   public function diagnose_reject_reason_new($status,$reason,$id){
		
		$data = array();
		$admin_id = $this->session->userdata;

		$this->db->where('diagnose_id', $id);
	    $query=$this->db->get('xin_clients_diagnose');
	    $result=$query->row();

		if($status == 3) {

	    	$reasons = $result->diagnose_reject_reason.",".$reason;

			$data = array( 
			    'diagnose_status'      		 => 2,
			    'diagnose_reject_reason'     => $reasons,
			    'diagnose_status_approve_by' => $admin_id['user_id']['user_id'],
			    'diagnose_bill_status' => NULL,
			    "send_admission_status" => 1
			);
		} else if($status == 4) {

	    	$reasons = $result->diagnose_bill_reject_reason.",".$reason;

			$data = array( 
			    'diagnose_bill_status'      		 => $status,
			    'diagnose_bill_reject_reason'     => $reasons,
			    'diagnose_bill_approve_by' => $admin_id['user_id']['user_id'],
			    'diagnose_bill_note'      		 => '',
			);
		} else {
			$data = array( 
			    'diagnose_bill_note'      		 => $reason,
			);
		}
		// print_r($data); die;

		$this->db->where('diagnose_id', $id);
		if( $this->db->update('xin_clients_diagnose',$data)) {
			// echo $this->db->last_query();
			// die;
			return true;

		} else {

			return false;

		}		

	}
	public function diagnose_reject_reason($status,$reason,$id){
		
		$data = array();
		$admin_id = $this->session->userdata;

		$this->db->where('diagnose_id', $id);
	    $query=$this->db->get('xin_clients_diagnose');
	    $result=$query->row();

		if($status == 3) {

	    	$reasons = $result->diagnose_reject_reason.",".$reason;

			$data = array( 
			    'diagnose_status'      		 => $status,
			    'diagnose_reject_reason'     => $reasons,
			    'diagnose_status_approve_by' => $admin_id['user_id']['user_id']
			);
		} else if($status == 4) {

	    	$reasons = $result->diagnose_bill_reject_reason.",".$reason;

			$data = array( 
			    'diagnose_bill_status'      		 => $status,
			    'diagnose_bill_reject_reason'     => $reasons,
			    'diagnose_bill_approve_by' => $admin_id['user_id']['user_id'],
			    'diagnose_bill_note'      		 => '',
			);
		} else {
			$data = array( 
			    'diagnose_bill_note'      		 => $reason,
			);
		}
		// print_r($data); die;

		$this->db->where('diagnose_id', $id);
		if( $this->db->update('xin_clients_diagnose',$data)) {
			// echo $this->db->last_query();
			// die;
			return true;

		} else {

			return false;

		}		

	}

	public function update_auth_status_again($status,$id){
		
		$data = array();
		$admin_id = $this->session->userdata;

		$data = array( 
		    'diagnose_status'      		 => $status,
		    'diagnose_status_approve_by' => NULL
		);

		$this->db->where('diagnose_id', $id);
		if( $this->db->update('xin_clients_diagnose',$data)) {
			// echo $this->db->last_query();
			// die;
			return true;

		} else {

			return false;

		}		

	}

	public function update_diagnose_status_record_admission($status,$code,$id,$code2=null){
		$data = array();
		$admin_id = $this->session->userdata;

		// echo $status;die;
		
		/* if($code){
			$data = array( 
				'diagnose_generated_code'	 => $code,
			    'diagnose_status'      		 => $status,
			    'diagnose_status_approve_by' => $admin_id['user_id']['user_id'],
			    'is_notify_2' 				 => 1
			);
		} else  */if($status == '3') {
			$data = array( 
			    'diagnose_status'      		  => $status,
			    'diagnose_status_approve_by' => $admin_id['user_id']['user_id'],
				'is_notify_2' 				 => 1
			);
		} else if($status == '1') {
			$data = array(
			    'admission_status'        => '1',
				'admission_status_approve_by' =>$admin_id['user_id']['user_id'],
			    'is_notify_2' 				 => 1,
                'admision_code'=>$code2,
			);
		}

		// print_r($data);die;

		$this->db->where('diagnose_id', $id);

		if( $this->db->update('xin_clients_diagnose',$data)) {
			 //echo $this->db->last_query();
			// die;
			return true;

		} else {

			return false;

		}		

	}
	
	public function updateFinalPrice($id,$price){
		$data = ["diagnose_total_sum" => $price];
		$this->db->where('diagnose_id', $id);

		if( $this->db->update('xin_clients_diagnose',$data)) {
			// echo $this->db->last_query();
			// die;
			return true;

		} else {

			return false;

		}	
	}
	
public function update_diagnose_status_record($status,$code,$id){
		$data = array();
		$admin_id = $this->session->userdata;

		// echo $status;die;
		
		if($code){
			$data = array( 
				'diagnose_generated_code'	 => $code,
			    'diagnose_status'      		 => $status,
			    'diagnose_status_approve_by' => $admin_id['user_id']['user_id'],
			    'is_notify_2' 				 => 1
			);
		} else if($status == '3') {
			$data = array( 
			    'diagnose_status'      		  => $status,
			    'diagnose_status_approve_by' => $admin_id['user_id']['user_id'],
				'is_notify_2' 				 => 1
			);
		} else if($status == '1') {
			$data = array(
			    'diagnose_bill_status'        => '1',
			    'is_notify_2' 				 => 1
			);
		}

		// print_r($data);die;

		$this->db->where('diagnose_id', $id);

		if( $this->db->update('xin_clients_diagnose',$data)) {
			// echo $this->db->last_query();
			// die;
			return true;

		} else {

			return false;

		}		

	}
	public function update_finance_bill_status($status,$code,$id){
		
		$data = array();
		$admin_id = $this->session->userdata;
		//$var['hospital_id']['hospital_id'];

		$data = array( 
		    'diagnose_transaction_id'     => $code,
		    'diagnose_finance_status'     => $status,
		    'diagnose_finance_approve_by' => $admin_id['user_id']['user_id']
		);

		$this->db->where('diagnose_id', $id);

		if( $this->db->update('xin_clients_diagnose',$data)) {

			return true;

		} else {

			return false;

		}		

	}

	public function update_finance_bill_status_bulk($status,$code,$id,$bulk){
		
		$data = array();
		$admin_id = $this->session->userdata;
		//$var['hospital_id']['hospital_id'];

		$data = array( 
			'bulk_id' => $bulk,
		    'diagnose_transaction_id'     => $code,
		    'diagnose_finance_status'     => $status,
		    'diagnose_finance_approve_by' => $admin_id['user_id']['user_id']
		);

		$this->db->where('diagnose_id', $id);

		if( $this->db->update('xin_clients_diagnose',$data)) {

			return true;

		} else {

			return false;

		}		

	}

	public function update_diagnose_bill_status_record($status,$id){
		
		$data = array();
		$admin_id = $this->session->userdata;
		//$var['hospital_id']['hospital_id'];
			// echo "conrolere wahid isasdfa si: ". $id;
			// die;ff

		$this->db->where('diagnose_id', $id);
	    $query=$this->db->get('xin_clients_diagnose');
	    $result=$query->row();
	    // print_r($result);
	    // die;

	    if($status == '2') {
			$data = array( 
			    'diagnose_bill_status' => $status,
			    'diagnose_bill_approve_by' => $admin_id['user_id']['user_id']
			);
	    } else if($status == '3') {

	    	$ids = $result->diagnose_bill_approve_by.",".$admin_id['user_id']['user_id'];

			$data = array( 
			    'diagnose_bill_status'     => $status,
			    'diagnose_finance_status'  => '1',
			    'diagnose_bill_approve_by' => $ids
			);
	    } else if($status == '4') {
			$data = array( 
			    'diagnose_bill_status' => $status,
			    'diagnose_bill_approve_by' => $admin_id['user_id']['user_id']
			);
	    }

		// $data = array( 
		//     'diagnose_bill_status' => $status,
		//     'diagnose_bill_approve_by' => $var['user_id']['user_id']
		// );

		$this->db->where('diagnose_id', $id);

		if( $this->db->update('xin_clients_diagnose',$data)) {

			return true;

		} else {

			return false;

		}		

	}

	public function total_clients($hid) 
	{
		$sql = "SELECT * FROM xin_clients WHERE hospital_id = '".$hid."'";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		} else {
			return false;
		}
	}

	public function total_dependants($hid) 
	{
		$sql = "SELECT * FROM xin_clients_family WHERE hospital_id = '".$hid."'";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		} else {
			return false;
		}
	}

	public function total_auths() 
	{
		$sql = "SELECT * FROM xin_clients_diagnose WHERE diagnose_status = '2'";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		} else {
			return false;
		}
	}

	public function total_bills() 
	{
		$sql = "SELECT * FROM xin_clients_diagnose WHERE diagnose_bill_status = '3'";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		} else {
			return false;
		}
	}

	public function select_all_transaction_ids() 
	{
		$sql = "SELECT * FROM xin_clients_diagnose D,xin_hospital H WHERE diagnose_transaction_id != '' AND D.diagnose_hospital_id = H.hospital_id AND amount_deduct_status = '0' AND bulk_id IS NULL";
		$query = $this->db->query($sql);
		// echo $this->db->last_query();die;
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	public function getcomment($id) 
	{
		$sql = "SELECT * FROM admission_approve_comment WHERE diagnose_ids='".$id."'";
		$query = $this->db->query($sql);
		// echo $this->db->last_query();die;
		if ($query->num_rows() > 0) {
           $row = $query->result();
		  // print_r($row);
		   $html = '<table class="table table-border" border="1"><tbody>
		   <tr>
		   <th style="width: 110px;">From Date</th>
		 
		   <th>Comment</th>
		   </tr>';
		   foreach($row as $rows){
			   //echo $rows->fromdate;
			   $html .= '<tr>
		   <th style="width: 110px;">'.$rows->fromdate.'</th>
		   <th>'.$rows->comment.'</th>
		   </tr>';
		   }
		   $html .='</tbody>
		   </table>';
		   echo $html;
		} else {
			return false;
		}
	}
	public function select_all_bulk_transaction_ids2() 
	{
		$sql = "SELECT * FROM xin_clients_diagnose D,xin_hospital H WHERE diagnose_transaction_id != '' AND D.diagnose_hospital_id = H.hospital_id AND amount_deduct_status = '0'";
		$query = $this->db->query($sql);
		// echo $this->db->last_query();die;
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function select_all_bulk_transaction_ids($hospital_id) 
	{
		$sql = "SELECT * FROM xin_clients_diagnose D,xin_hospital H WHERE diagnose_transaction_id != '' AND D.diagnose_hospital_id = H.hospital_id AND amount_deduct_status = '0' AND diagnose_hospital_id = $hospital_id AND is_bulk = 1";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	/* Wahid */


	public function add_dependant($data){

		$this->db->insert('xin_clients_family', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}

	public function add_bulk($ids){
		$data = array(
			'bulk_id' => $ids,
			'created_at' => date('Y-m-d H:i:s')
		);

		$this->db->insert('xin_clients_diagnose_bulk', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}



	public function add_dependant2($data){

		$this->db->insert('xin_clients_family_req', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}

	

	// Function to Delete selected record from table

	public function delete_record($id){

		$this->db->where('client_id', $id);

		$this->db->delete('xin_clients');

		

	}

	

	// Function to update record in table

	public function update_record($data, $id){

		$this->db->where('client_id', $id);

		if( $this->db->update('xin_clients',$data)) {

			return true;

		} else {

			return false;

		}		

	}

	// Function to update record in table

	public function update_capitation_record($data, $id){

		$this->db->where('id', $id);

		if( $this->db->update('xin_clients_capitation',$data)) {

			return true;

		} else {

			return false;

		}		

	}



	public function update_record_hospital($data, $id){

		$this->db->where('hospital_id', $id);

		if( $this->db->update('xin_hospital',$data)) {

			return true;

		} else {

			return false;

		}		

	}

	function get_clients_search($type_id) {
		$sql = 'SELECT * FROM `xin_clients` where company_name = ?';
		$binds = array($type_id);
		$query = $this->db->query($sql, $binds);
		return $query;
	}
	
	

	function get_diagnose($id) {
		$sql = 'SELECT * FROM `xin_clients_diagnose` where diagnose_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		return $query;
	}

	function get_capitation_info($id) {
		$sql = 'SELECT * FROM `xin_clients_capitation` where id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		return $query;
	}

	function get_capitation_principal($capitation_id) {
		$sql = "SELECT * FROM `xin_clients_capitation` where capitation_id = ? AND type = 'p'" ;
		$binds = array($capitation_id);
		$query = $this->db->query($sql, $binds);
		return $query;
	}

	function get_capitation_check($capitation_id,$name,$type) {
		$sql = 'SELECT * FROM `xin_clients_capitation` where capitation_id = ? AND name = ? AND type = ?';
		$binds = array($capitation_id,$name,$type);
		$query = $this->db->query($sql, $binds);
		return $query;
	}

	function get_clients_encounter($client_id,$from=null,$to=null) {
		if (is_null($from) AND is_null($to)) {
			$sql = "SELECT * FROM `xin_clients_diagnose` where diagnose_client_id = ? AND diagnose_user_type = 'C' AND diagnose_finance_status != ''";
		}else{
			$from = $from." 00:00:01";
			$to = $to." 23:59:59";

			$sql = "SELECT * FROM `xin_clients_diagnose` where diagnose_client_id = ? AND diagnose_user_type = 'C' AND diagnose_finance_status != '' AND diagnose_date_time BETWEEN '$from' AND '$to'";
		}

		$binds = array($client_id);
		$query = $this->db->query($sql, $binds);
		return $query;
	}

	function get_dependants_encounter($client_id,$from=null,$to=null) {
		if (is_null($from) AND is_null($to)) {
			$sql = "SELECT * FROM `xin_clients_diagnose` where diagnose_client_id = ? AND diagnose_user_type = 'D' AND diagnose_finance_status != ''";
		}else{
			$from = $from." 00:00:01";
			$to = $to." 23:59:59";

			$sql = "SELECT * FROM `xin_clients_diagnose` where diagnose_client_id = ? AND diagnose_user_type = 'D' AND diagnose_finance_status != '' AND diagnose_date_time BETWEEN '$from' AND '$to'";
		}

		$binds = array($client_id);
		$query = $this->db->query($sql, $binds);
		return $query;
	}

	function get_capitation_clients_encounter($client_id,$from=null,$to=null) {
		if (is_null($from) AND is_null($to)) {
			$sql = "SELECT * FROM `xin_clients_diagnose` where diagnose_client_id = ? AND is_capitation = '1' AND diagnose_finance_status != ''";
			
		}else{
			$from = $from." 00:00:01";
			$to = $to." 23:59:59";
			$sql = "SELECT * FROM `xin_clients_diagnose` where diagnose_client_id = ? AND is_capitation = '1' AND diagnose_finance_status != '' AND diagnose_date_time BETWEEN '$from' AND '$to'";
		}
		
		$binds = array($client_id);
		$query = $this->db->query($sql, $binds);
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_capitation_hospital($hcp) {
		$sql = "SELECT * FROM `xin_clients_capitation` where hcp_code = ?";
		$binds = array($hcp);
		$query = $this->db->query($sql, $binds);
		// echo $this->db->last_query();die;
		return $query;
	}

	// function get_dependants_encounter($client_id) {
	// 	$sql = "SELECT * FROM `xin_clients_diagnose` where diagnose_client_id = ? AND diagnose_user_type = 'D' AND diagnose_finance_status != ''";
	// 	$binds = array($client_id);
	// 	$query = $this->db->query($sql, $binds);
	// 	return $query;
	// }

	function get_all_no_usage($id,$status)
	{
		if ($status == 'C') {
			$sql = "SELECT * FROM `xin_clients_diagnose` where diagnose_finance_status IS NULL AND diagnose_client_id = ? AND diagnose_user_type = 'C'";
		}else{
			$sql = "SELECT * FROM `xin_clients_diagnose` where diagnose_finance_status IS NULL AND diagnose_client_id = ? AND diagnose_user_type = 'D'";
		}
		$binds = array($id);
		$query = $this->db->query($sql,$binds);
		return $query;	
	}

	function get_all_usage($id,$status)
	{
		if ($status == 'C') {
			$sql = "SELECT * FROM `xin_clients_diagnose` where diagnose_finance_status IS NOT NULL AND diagnose_client_id = ? AND diagnose_user_type = 'C'";
		}else{
			$sql = "SELECT * FROM `xin_clients_diagnose` where diagnose_finance_status IS NOT NULL AND diagnose_client_id = ? AND diagnose_user_type = 'D'";
		}
		$binds = array($id);
		$query = $this->db->query($sql,$binds);
		return $query;	
	}


	function get_clients_encounter_by_organization($oid,$from=null,$to=null) {
		// $sql = "SELECT * FROM `xin_clients_diagnose` where diagnose_client_id = ? AND diagnose_user_type = 'C' AND diagnose_finance_status != '' AND ";
		if (is_null($from) AND is_null($to)) {
			$sql = "SELECT xin_clients_diagnose.diagnose_total_sum AS total_sum FROM `xin_clients_diagnose`,`xin_clients_family`,`xin_clients` WHERE  xin_clients_diagnose.diagnose_client_id = xin_clients_family.clients_family_id AND xin_clients_family.client_id = xin_clients.client_id AND xin_clients.company_name = ? AND xin_clients_diagnose.diagnose_finance_status != ''";
		}else{
			$sql = "SELECT xin_clients_diagnose.diagnose_total_sum AS total_sum FROM `xin_clients_diagnose`,`xin_clients_family`,`xin_clients` WHERE  xin_clients_diagnose.diagnose_client_id = xin_clients_family.clients_family_id AND xin_clients_family.client_id = xin_clients.client_id AND xin_clients.company_name = ? AND xin_clients_diagnose.diagnose_finance_status != '' AND xin_clients_diagnose.diagnose_date_time BETWEEN '$from' AND '$to'";

		}

		$binds = array($oid);
		$query = $this->db->query($sql, $binds);
		// echo $this->db->last_query();
		// print_r($query->result());die;
		return $query;
	}

	function get_clients_filtered($type_id,$date) {
		$sql = 'SELECT * FROM `xin_clients` where company_name = ? AND created_at > ?';
		$binds = array($type_id,$date);
		$query = $this->db->query($sql, $binds);
		// print_r($this->db->last_query());
		// die;
		return $query;
	}

	function get_clients_filtered2($type_id,$date) {
		$sql = 'SELECT * FROM `xin_clients` where company_name = ? AND created_at < ?';
		$binds = array($type_id,$date);
		$query = $this->db->query($sql, $binds);
		// print_r($this->db->last_query());
		// die;
		return $query;
	}

	function get_clients_filtered_between($type_id,$date,$date_before) {
		$sql = 'SELECT * FROM `xin_clients` where company_name = ? AND (created_at BETWEEN ? AND ?)';
		$binds = array($type_id,$date_before,$date);
		$query = $this->db->query($sql, $binds);
		// print_r($this->db->last_query());
		// die;
		return $query;
	}

	function get_hospitals_filtered_between($from,$to) {
		$sql = 'SELECT * FROM `xin_hospital` where created_on BETWEEN ? AND ?';
		$binds = array($from,$to);
		$query = $this->db->query($sql, $binds);
		// print_r($this->db->last_query());
		// die;
		return $query;
	}

	function get_hospitals_filtered_yearly($year) {
		$from = $year."-01-01 00:00:01";
		$to = $year."-12-31 23:59:59";
		$sql = 'SELECT * FROM `xin_hospital` where created_on BETWEEN ? AND ?';
		$binds = array($from,$to);
		$query = $this->db->query($sql, $binds);
		// print_r($this->db->last_query());
		// die;
		return $query;
	}

	function get_hospital_hcp(){
		$sql = 'SELECT * FROM `xin_hospital` where hcp_code IS NOT NULL ORDER BY hospital_name asc';
		// $binds = array($id);
		$query = $this->db->query($sql);
		return $query;	
	}

	function get_clients_subscription($id){
		$sql = 'SELECT * FROM `xin_subscription` where subscription_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		return $query;	
	}

	public function get_organization_info($id){
		$sql = 'SELECT * FROM `xin_organization` where id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		return $query;	
	}
	public function get_organization_info_name($id){
		$sql = 'SELECT name FROM `xin_organization` where id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		$res =$query->row_array();	
		return $res['name'];
	}
	public function get_organization_info1($id){
		
		//$query=$this->db->conn_id->prepare("SELECT * FROM `xin_organization` where id = '".$id."'");
		
		//return $query->result_array();
		$this->db->where('id',$id);
        $query = $this->db->get("xin_organization");
        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            return false;
        }		
	}
    public function get_sub_info($id){

        //$query=$this->db->conn_id->prepare("SELECT * FROM `xin_organization` where id = '".$id."'");

        //return $query->result_array();
        $this->db->where('subscription_id',$id);
          $this->db->select('plan_name');
        $query = $this->db->get("xin_subscription");
        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            return false;
        }
    }
    public function getDiagnoses($id){
        $this->db->where('diagnose_id',$id);
        $this->db->select('diagnose_date');
        $query = $this->db->get("xin_clients_diagnose");
        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            return false;
        }
	}
    public function get_client_info($id){
        $this->db->where('client_id',$id);
        //$this->db->select('company_name','subscription_ids');
        $query = $this->db->get("xin_clients");
        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            return false;
        }
	}
    public function get_client_img($id){
        $this->db->where('client_id',$id);
        $this->db->select('client_profile');
        $query = $this->db->get("xin_clients");
        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            return false;
        }
	}
	public function get_subscribtionname_info($id){
		
		//$query=$this->db->conn_id->prepare("SELECT * FROM `xin_organization` where id = '".$id."'");
		
		//return $query->result_array();
		$this->db->where('subscription_id',$id);
        
        $query = $this->db->get("xin_subscription");
        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            return false;
        }		
	}

	function get_hospital_info($id){
		$sql = 'SELECT * FROM `xin_hospital` where hospital_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		return $query;	
	}
	function get_hospital_info_name($id){
		$sql = 'SELECT hospital_name FROM `xin_hospital` where hospital_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		$row = $query->row_array();	
		return $row['hospital_name']; 
	}
	function get_all_hospital_info(){
		$sql = 'SELECT * FROM `xin_hospital`';
		$query = $this->db->query($sql);
		return $query;	
	}


	function all_clients_of_individual_hospital($id,$from,$to){
		$sql = "SELECT * FROM `xin_clients_diagnose` where diagnose_hospital_id = ? AND (diagnose_date BETWEEN ? AND ?) AND diagnose_finance_status = 1";
		$binds = array($id,$from,$to);
		$query = $this->db->query($sql, $binds);
		return $query;	
	}

	function get_all_organization(){
		$sql = "SELECT * FROM `xin_organization`";
		$query = $this->db->query($sql);
		return $query;	
	}

	function get_drugs_diagnose($id){
		$sql = "SELECT * FROM `xin_clients_diagnose_drugs` WHERE diagnose_maind_id = ?";
		$binds = array($id);
		$query = $this->db->query($sql,$binds);
		return $query;	
	}

	function get_services_diagnose($id){
		$sql = "SELECT * FROM `xin_clients_diagnose_services` WHERE diagnose_mains_id = ?";
		$binds = array($id);
		$query = $this->db->query($sql,$binds);
		return $query;	
	}

	function get_drugs_info($id){
		$sql = "SELECT * FROM `xin_hospital_drugs` WHERE drug_id = ?";
		$binds = array($id);
		$query = $this->db->query($sql,$binds);
		return $query;	
	}

	function get_services_info($id){
		$sql = "SELECT * FROM `xin_services_hospital` WHERE id = ?";
		$binds = array($id);
		$query = $this->db->query($sql,$binds);
		return $query;	
	}

	function get_principal_id($id){
		$sql = "SELECT * FROM `xin_clients_capitation` WHERE capitation_id = ? AND type = 'p'";
		$binds = array($id);
		$query = $this->db->query($sql,$binds);
		return $query;	
	}

	function get_bulk_not_deducted(){
		$sql = "SELECT * FROM `xin_clients_diagnose_bulk` WHERE is_deducted IS NULL";
		$query = $this->db->query($sql);
		return $query;	
	}

	function get_bulk($id = null){
		if ($id != null) {
			$sql = "SELECT * FROM `xin_clients_diagnose_bulk` WHERE id = ?";
			$binds = array($id);
			$query = $this->db->query($sql,$binds);
		}else{
			$sql = "SELECT * FROM `xin_clients_diagnose_bulk`";
			$query = $this->db->query($sql);
		}
		return $query;	
	}

	function get_dob($id,$status){
		if ($status == 'C') {
			$sql = "SELECT dob FROM `xin_clients` WHERE client_id = ?";
		}else{
			$sql = "SELECT dob FROM `xin_clients_family` WHERE clients_family_id = ?";
		}
		$binds = array($id);
		$query = $this->db->query($sql,$binds);
		return $query;		
	}

	function get_dob_capitation($id,$status){
		if ($status == 'p') {
			$sql = "SELECT dob FROM `xin_clients_capitation` WHERE id = ? AND type = 'p'";
		}else{
			$sql = "SELECT dob FROM `xin_clients_capitation` WHERE id = ? AND type = 'd'";
		}
		$binds = array($id);
		$query = $this->db->query($sql,$binds);
		return $query;		
	}

	function check_email($email){
		$sql = "SELECT * FROM `xin_clients` WHERE email = ?";
		$binds = array($email);
		$query = $this->db->query($sql,$binds);
		return $query->num_rows();		
	}

	function update_days($id , $data){
		// $sql = " UPDATE `xin_clients_diagnose` SET `days`= $days where 'id' = $id ";
		$this->db->where('diagnose_id',$id);
		$this->db->update('xin_clients_diagnose',$data);
	}
//update service Qty
	function update_service_qty($id , $data){
		// $sql = " UPDATE `xin_clients_diagnose` SET `days`= $days where 'id' = $id ";
		$this->db->where('diagnose_autos_id',$id);
		$this->db->update('xin_clients_diagnose_services',$data);
	}

	function update_service_green_qty($id , $data){
		// $sql = " UPDATE `xin_clients_diagnose` SET `days`= $days where 'id' = $id ";
		$this->db->where('diagnose_id',$id);
		$this->db->update('xin_clients_diagnose',$data);
	}

	

}

?>
