<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyModel extends CI_Model
{

    var $client_service = "frontend-client";
    var $auth_key = "hmorestapi";

    public function check_auth_client()
    {
        $client_service = $this->input->get_request_header('Client-Service', TRUE);
        $auth_key = $this->input->get_request_header('Auth-Key', TRUE);
        if ($client_service == $this->client_service && $auth_key == $this->auth_key) {
            return true;
        } else {
            return json_output(401, array('status' => 401, 'message' => 'Unauthorized.'));
        }
    }

    public function getHospitalToken($id)
    {
        return $this->db->from('xin_users_authentication')->where('users_id', $id)->get()->row();
    }

    public function login($username, $password)
    {
        $q = $this->db->from('xin_hospital')->where('email', $username)->get()->row();

        if (empty($q)) {
            return array('status' => 204, 'message' => 'Username not found.');
        } else {
            $hashed_password = $q->password;
            $id = $q->hospital_id;

            if ($password == $hashed_password) {
                $last_login = date('Y-m-d H:i:s');
                try {
//                    $string = random_bytes(32);
                    $string = $this->generate_random_string();
                } catch (TypeError $e) {
                    // Well, it's an integer, so this IS unexpected.
                    die("An unexpected error has occurred");
                } catch (Error $e) {
                    // This is also unexpected because 32 is a reasonable integer.
                    die("An unexpected error has occurred");
                } catch (Exception $e) {
                    // If you get this message, the CSPRNG failed hard.
                    die("Could not generate a random string. Is our OS secure?");
                }
                $token = bin2hex($string);
//				$token = md5(rand());
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->trans_start();
                $this->db->where('hospital_id', $id)->update('xin_hospital', array('last_login' => $last_login));
                $this->db->insert('xin_users_authentication', array('users_id' => $id, 'token' => $token, 'expired_at' => $expired_at));
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return array('status' => 500, 'message' => 'Internal server error.');
                } else {
                    $this->db->trans_commit();
                    return array('status' => 200, 'message' => 'Successfully login.', 'hospital_id' => $id, 'token' => $token);
                }
            } else {
                return array('status' => 204, 'message' => 'Wrong password.');
            }
        }
    }

   public function generate_string($strength = 32) {
        $input = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }

        return $random_string;
    }

    public function login_client($username, $password)
    {
        $q = $this->db->from('xin_clients')->where('email', $username)->get()->row();

        if (empty($q)) {
            return array('status' => 204, 'message' => 'Username not found.');
        } else {
            $hashed_password = $q->client_password;
            $id = $q->client_id;
            $password_hash = hash_equals($hashed_password, crypt($password, $hashed_password));
            if ($password_hash) {
                $last_login = date('Y-m-d H:i:s');
                try {
//                    $string = random_bytes(32);
                    $string = $this->generate_random_string(32);
                } catch (TypeError $e) {
                    // Well, it's an integer, so this IS unexpected.
                    die("An unexpected error has occurred");
                } catch (Error $e) {
                    // This is also unexpected because 32 is a reasonable integer.
                    die("An unexpected error has occurred");
                } catch (Exception $e) {
                    // If you get this message, the CSPRNG failed hard.
                    die("Could not generate a random string. Is our OS secure?");
                }
                $token = bin2hex($string);
//                $token = md5(rand());
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->trans_start();
                $this->db->where('client_id', $id)->update('xin_clients', array('last_login_date' => $last_login));
                $this->db->insert('xin_users_authentication', array('users_id' => $id, 'token' => $token, 'expired_at' => $expired_at));
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return array('status' => 500, 'message' => 'Internal server error.');
                } else {
                    $this->db->trans_commit();
                    return array('status' => 200, 'message' => 'Successfully login.', 'client_id' => $id, 'token' => $token);
                }
            } else {
                return array('status' => 204, 'message' => 'Wrong password.');
            }
        }
    }

    public function logout()
    {
        $users_id = $this->input->get_request_header('User-ID', TRUE);
        $token = $this->input->get_request_header('token', TRUE);
        $this->db->where('users_id', $users_id)->where('token', $token)->delete('xin_users_authentication');
        return array('status' => 200, 'message' => 'Successfully logout.');
    }

    public function auth()
    {
        $users_id = $this->input->get_request_header('User-ID', TRUE);
        $token = $this->input->get_request_header('token', TRUE);
        $q = $this->db->from('xin_users_authentication')->where('users_id', $users_id)->where('token', $token)->get()->row();
        if (empty($q)) {
            return json_output(401, array('status' => 401, 'message' => 'Unauthorized 12.'));
        } else {
            if ($q->expired_at < date('Y-m-d H:i:s')) {
                return json_output(401, array('status' => 401, 'message' => 'Your session has been expired.'));
            } else {
                $updated_at = date('Y-m-d H:i:s');
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->where('users_id', $users_id)->where('token', $token)->update('xin_users_authentication', array('expired_at' => $expired_at, 'updated_at' => $updated_at));
                return array('status' => 200, 'message' => 'Authorized.');
            }
        }
    }

    public function hospital_all()
    {
        return $this->db->from('xin_hospital')->order_by('hospital_id', 'desc')->get()->result();
    }

    public function hospital_detail_data($id)
    {
        return $this->db->from('xin_hospital')->where('hospital_id', $id)->order_by('hospital_id', 'desc')->get()->row();
    }


    public function client_all()
    {
        return $this->db->from('xin_clients')->order_by('client_id', 'desc')->get()->result();
    }

    public function client_by_hospital_all($id)
    {
        return $this->db->from('xin_clients')->where('hospital_id', $id)->order_by('client_id', 'desc')->get()->result();
    }

    public function client_detail_data($id)
    {
       $sql= "SELECT xin_organization.name as org_name,xin_clients.* FROM xin_organization,xin_clients WHERE xin_clients.company_name=xin_organization.id and xin_clients.client_id=?";
        $binds = array($id);
        $query = $this->db->query($sql, $binds);
         $result=$query->result();
        return $result[0];
//        return $this->db->from('xin_clients')->where('client_id', $id)->order_by('client_id', 'desc')->get()->row();
    }

    public function client_fammily_detail_data($id)
    {
        return $this->db->from('xin_clients_family')->where('client_id', $id)->order_by('client_id', 'desc')->get()->row();
    }

    public function client_dependant_data($id)
    {
        return $this->db->from('xin_vdependant')->where('client_id', $id)->order_by('client_id', 'desc')->get()->result();
    }

    public function service_hospital_all()
    {
        return $this->db->from('xin_services_hospital')->order_by('id', 'desc')->get()->result();
    }

    public function drugs_hospital_all($id)
    {
        return $this->db->from('xin_services_hospital')->where('hospital_id', $id)->order_by('hospital_id', 'desc')->get()->result();
    }

    public function drugs_detail_hospital_all($id, $drugs_id)
    {
        return $this->db->from('xin_services_hospital')->where("hospital_id='$id' and drug_id='$drugs_id'")->order_by('drug_id', 'desc')->get()->result();
    }

    public function hospital_request_create_data($data)
    {
        $this->db->insert('xin_clients_diagnose', $data);
        $insertedId = $this->db->insert_id();
        return array('status' => 200, 'id' => $insertedId);
    }

    public function insertRequestDrugs($clientId, array $drugs)
    {
        try {
            foreach ($drugs as $drug) {
                $data = array(
                    "diagnose_maind_id" => $clientId,
                    "diagnose_drugs_id" => $drug['drugId']
                );
                $this->db->insert('xin_clients_diagnose_drugs', $data);
            }

            return array('status' => 200, 'message' => 'Data has been created.');
        } catch (\mysql_xdevapi\Exception $exception) {
            return array('status' => 400, 'message' => 'Problem in drugs.' . $exception);
        }

    }

    public function insertRequestServices($clientId, array $services)
    {
        try {
            foreach ($services as $service) {
                $data = array(
                    "diagnose_mains_id" => $clientId,
                    "diagnose_services_id" => $service['serviceId']
                );
                $this->db->insert('xin_clients_diagnose_services', $data);
            }
            return array('status' => 200, 'message' => 'Data has been created.');
        } catch (\mysql_xdevapi\Exception $exception) {
            return array('status' => 400, 'message' => 'Problem in services.' . $exception);

        }

    }

    public function hospital_request_change_data($data, $id)
    {
        $this->db->where('diagnose_id', $id)->update('xin_clients_diagnose', $data);
        return array('status' => 200, 'message' => 'Data has been updated.');
    }

    public function hospital_request_client_get_data($hospital_id)
    {
        return $this->db->from('xin_vsend_request')->where('hospital_id', $hospital_id)->order_by('hospital_id', 'desc')->get()->result();
    }

    public function count_finance_by_hospital()
    {
        return $this->db->from('xin_finance_transaction')->where("transaction_type='income'")->get()->num_rows();
    }

    public function hospital_update_data($id, $data)
    {
        $this->db->where('hospital_id', $id)->update('xin_hospital', $data);
        return array('status' => 200, 'message' => 'Data has been updated.');
    }

    public function client_dependant_update_data($id, $data)
    {
        $this->db->where('client_id', $id)->update('xin_clients', $data);
        return array('status' => 200, 'message' => 'Data has been updated.');
    }

    public function updateClientData($id, $data)
    {
        $this->db->where('client_id', $id)->update('xin_clients', $data);
        return array('status' => 200, 'message' => 'Data has been updated.');
    }


    public function client_dependant_new_data($data)
    {
        $this->db->insert('xin_clients_family', $data);
        return array('status' => 200, 'message' => 'Data has been updated.');
    }

    public function client_request_change_hospital($data)
    {
        $this->db->insert('xin_change_hospital_request', $data);
        return array('status' => 200, 'message' => 'Data has been created.');
    }

    public function count_authorization_dependant_by_hospital($id)
    {
        return $this->db->from('xin_clients_diagnose')->where('diagnose_hospital_id', $id)->get()->num_rows();
    }

    public function count_client_dependant_by_hospital($id)
    {
        return $this->db->from('xin_clients_family')->where('hospital_id', $id)->get()->num_rows();
    }

    public function count_client_request_hospital($id)
    {
        return $this->db->from('xin_change_hospital_request')->where('client_id', $id)->get()->num_rows();
    }

    public function count_client_familly_request_hospital($id)
    {
        return $this->db->from('xin_clients_family_req')->where('client_id', $id)->get()->num_rows();
    }

    public function get_xin_subscription($id)
    {
        return $this->db->from('xin_subscription')->where('subscription_id', $id)->get()->num_rows();
    }

    public function get_list_xin_subscription($id)
    {
        return $this->db->from('xin_subscription')->where('subscription_id', $id)->get()->result();
    }

    public function get_xin_list_provider($id)
    {
        return $this->db->from('xin_list_provider_clients')->where('client_id', $id)->get()->result();
    }

    public function getElectronicsId($id)
    {
        return $this->db->from('xin_venrolle')->where('client_id', $id)->get()->result();
    }
//    public function book_delete_data($id)
//    {
//        $this->db->where('id',$id)->delete('books');
//        return array('status' => 200,'message' => 'Data has been deleted.');
//    }

    public function get_bill_status($id)
    {
        return $this->db->from('xin_clients_diagnose')->where('diagnose_hospital_id', $id)->where('diagnose_bill_status =3 or diagnose_bill_status=4')->get()->num_rows();
    }

    public function get_enrolle($id)
    {
        return $this->db->from('xin_venrolle')->where('client_id', $id)->order_by('hospital_id', 'desc')->get()->result();
    }

    public function get_enrolle_hoospital($id)
    {
        return $this->db->from('xin_venrolle')->where('hospital_id', $id)->order_by('hospital_id', 'desc')->get()->result();
    }

    public function get_location($id)
    {
        return $this->db->from('xin_location')->where('location_id', $id)->where('location_name', $id)->order_by('location_id', 'desc')->get()->row();
    }

    public function hospital_band_detail_data($id)
    {
        $sql = " select hospital_id, hospital_name, location_name, band_id,address from (select h.hospital_id, hospital_name, l.location_name as location_name, band_id,h.address from xin_hospital as h
				 join xin_location as l on (h.location_id = l.location_id), xin_subscription as x 
				 join xin_clients as c on (x.subscription_id = c.subscription_ids) 
				 where c.client_id= ? and band_id in ( 
				 SELECT DISTINCT 
                 SUBSTRING_INDEX(SUBSTRING_INDEX(`s`.`band_types`, ',', `n`.`digit` + 1),',', - 1) AS `val`
            	 FROM ((`hmo`.`xin_clients` `c` 
                 JOIN `hmo`.`xin_subscription` `s` ON (`c`.`subscription_ids` = `s`.`subscription_id`)) 
                 JOIN (SELECT 0 AS `digit` UNION ALL SELECT 1 AS `1` UNION ALL SELECT 2 AS `2` UNION ALL SELECT 3 AS `3` UNION ALL SELECT 4 AS `4` UNION ALL SELECT 5 AS `5` UNION ALL SELECT 6 AS `6`) `n` ON (OCTET_LENGTH(REPLACE(`s`.`band_types`, ',', '')) <= OCTET_LENGTH(`s`.`band_types`) - `n`.`digit`)) 
                 group by val )) as x group by hospital_id, location_name, band_id ";

        $binds = array($id);
        $query = $this->db->query($sql, $binds);

        return $query->result();
    }

    // generate random string

    public function generate_random_string($length = 7)
    {

        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $charactersLength = strlen($characters);

        $randomString = '';

        for ($i = 0; $i < $length; $i++) {

            $randomString .= $characters[rand(0, $charactersLength - 1)];

        }

        return $randomString;

    }

    public function ticket_support($data)
    {
//    	echo var_dump($data);
//    	die();
        $this->db->insert('xin_support_tickets', $data);
        return array('status' => 200, 'message' => 'Data has been created.');
    }

    public function get_list_xin_subscription_detail($id)
    {
        return $this->db->from('xin_subscription_detail')->where('subscription_detail_master_id', $id)->get()->result();
    }

    public function get_xin_organizions($id)
    {
        return $this->db->from('xin_organization')->where('id', $id)->get()->result();
    }

    public function organization_update_data($id, $data)
    {
        $this->db->where('id', $id)->update('xin_organization', $data);
        return array('status' => 200, 'message' => 'Data has been updated.');
    }

    public function ticket_update_data($id, $data)
    {
        $this->db->where('ticket_id', $id)->update('xin_support_tickets', $data);
        return array('status' => 200, 'message' => 'Data has been updated.');
    }

    public function get_xin_support_ticket($id)
    {
        return $this->db->from('xin_support_tickets')->where('employee_id', $id)->get()->result();
    }

    public function get_xin_support_ticket_hospital($id)
    {
        return $this->db->from('xin_support_tickets')->where('is_hc', $id)->get()->result();
    }

    public function get_all_location()
    {
        return $this->db->from('xin_location')->get()->result();
    }

    public function client_update_data($id, $data)
    {
        $this->db->where('client_id', $id)->update('xin_clients', $data);
        return array('status' => 200, 'message' => 'Data has been updated.');
    }


    public function getAllSubscription()
    {
        return $this->db->from('xin_subscription')->get()->result();
    }

    public function getClientSubscription($subscriptionId)
    {
        return $this->db->from('xin_subscription')->where('subscription_id', $subscriptionId)->get()->result();
    }

    public function getClientDependentCount($id)
    {
        return $this->db->from('xin_clients_family')->where('client_id', $id)->get()->num_rows();
    }

    public function getClientComplaintCount($id)
    {
        return $this->db->from('xin_support_tickets')->where('employee_id', $id)->get()->num_rows();
    }

    public function getHospitalCount($clientId)
    {
        return $this->db->from('xin_list_provider_clients')->where('client_id', $clientId)->get()->num_rows();
    }

    public function getTotalSubscription()
    {
        return $this->db->from('xin_subscription')->get()->num_rows();
    }

    public function getTotalChangeHospitalRequest($clientId)
    {
        return $this->db->from('xin_change_hospital_request')->where('client_id', $clientId)->get()->num_rows();
    }

    public function getHospitalClients($hospitalId)
    {
        return $this->db->from('xin_clients')->where('hospital_id', $hospitalId)->order_by('hospital_id', 'desc')->get()->result();

    }

    public function getHospitalCapitation($hospitalCode)
    {
        return $this->db->from('xin_clients_capitation')->where('hcp_code', $hospitalCode)->get()->result();

    }

    public function getHospitalDependentList($hospitalId)
    {
        return $this->db->from('xin_clients_family')->where('hospital_id', $hospitalId)->get()->result();

    }

    public function getHospitalCapitationCount($hospitalCode)
    {
        return $this->db->from('xin_clients_capitation')->where('hcp_code', $hospitalCode)->get()->num_rows();

    }

    public function getHospitalServices($hospitalId)
    {
        return $this->db->from('xin_services_hospital')->where('hospital_id', $hospitalId)->order_by('hospital_id', 'desc')->get()->result();

    }

    public function getHospitalDrugs($hospitalId)
    {
        return $this->db->from('xin_hospital_drugs')->where('hospital_id', $hospitalId)->order_by('hospital_id', 'desc')->get()->result();

    }

    public function getHospitalTotalRequest($id)
    {


        $sql = "SELECT 
                    CASE 
	                    WHEN (xin_clients_diagnose.is_capitation is NULL OR xin_clients_diagnose.is_capitation=0) AND xin_clients_diagnose.diagnose_user_type='C' THEN
                            CONCAT(IFNULL(xin_clients.`name`,''),' ',IFNULL(xin_clients.`last_name`,''),' ',IFNULL(xin_clients.`other_name`,''))	
	                    WHEN (xin_clients_diagnose.is_capitation is NULL OR xin_clients_diagnose.is_capitation=0) AND xin_clients_diagnose.diagnose_user_type='D' THEN
                            CONCAT(IFNULL(xin_clients_family.`name`,''),' ',IFNULL(xin_clients_family.`last_name`,''),' ',IFNULL(xin_clients_family.`other_name`,''))	
	                     WHEN  xin_clients_diagnose.is_capitation=1 THEN
		                    xin_clients_capitation.`name`
                        END  AS 'name',
                        xin_clients_diagnose.diagnose_id,
                        xin_clients_diagnose.diagnose_client_id,
                        xin_clients_diagnose.diagnose_generated_code,
                        xin_clients_diagnose.diagnose_status,
                        xin_clients_diagnose.diagnose_bill_reject_reason,
                        xin_clients_diagnose.diagnose_total_sum,
                        xin_clients_diagnose.diagnose_date,
                        xin_clients_diagnose.diagnose_medical,
                        xin_clients_diagnose.diagnose_investigation,
                        xin_clients_diagnose.diagnose_procedure,
                        xin_clients_diagnose.diagnose_diagnose,
                        xin_clients_diagnose.is_capitation,
                        xin_clients_diagnose.diagnose_user_type,
                        xin_clients_diagnose.diagnose_bill_status,
                        xin_clients_diagnose.diagnose_reject_reason,
                        xin_clients_diagnose.diagnose_date_of_birth

                        FROM xin_clients_diagnose
                        Left JOIN xin_clients ON xin_clients.client_id= xin_clients_diagnose.diagnose_client_id
                        Left JOIN xin_clients_family ON xin_clients_family.clients_family_id= xin_clients_diagnose.diagnose_client_id
                        Left JOIN xin_clients_capitation ON xin_clients_capitation.id= xin_clients_diagnose.diagnose_client_id
                        WHERE xin_clients_diagnose.diagnose_hospital_id=? 
                        AND 
                        (
                        (xin_clients_diagnose.is_capitation=0 or xin_clients_diagnose.is_capitation is NULL)
                        and ((xin_clients_diagnose.diagnose_user_type='C' AND xin_clients.client_id= xin_clients_diagnose.diagnose_client_id) 
                        OR (xin_clients_diagnose.diagnose_user_type='D' AND xin_clients_family.clients_family_id= xin_clients_diagnose.diagnose_client_id))
                        OR (xin_clients_capitation.id=xin_clients_diagnose.diagnose_client_id AND xin_clients_diagnose.is_capitation=1)
                        ) ORDER BY xin_clients_diagnose.diagnose_date DESC";
                          $binds = array($id);
                         $query = $this->db->query($sql, $binds);

                        return $query->result();
    }

    public function getHospitalCapitationRequest($id)
    {


        $sql = "select xin_clients_capitation.name, xin_clients_diagnose.diagnose_id,xin_clients_diagnose.diagnose_client_id,xin_clients_diagnose.diagnose_date,xin_clients_diagnose.diagnose_diagnose,xin_clients_diagnose.diagnose_procedure,xin_clients_diagnose.diagnose_investigation,xin_clients_diagnose.diagnose_medical,xin_clients_diagnose.diagnose_total_sum,xin_clients_diagnose.diagnose_generated_code,xin_clients_diagnose.diagnose_status,xin_clients_diagnose.diagnose_reject_reason,xin_clients_diagnose.diagnose_date,xin_clients_diagnose.diagnose_hospital_id,xin_clients_diagnose.diagnose_bill_status,xin_clients_diagnose.diagnose_generated_code,xin_clients_diagnose.diagnose_reject_reason,xin_clients_diagnose.diagnose_bill_reject_reason,xin_clients_diagnose.is_capitation,xin_clients_diagnose.diagnose_date_of_birth from xin_clients_capitation,xin_clients_diagnose where xin_clients_diagnose.diagnose_hospital_id=? and xin_clients_capitation.id=xin_clients_diagnose.diagnose_client_id and xin_clients_diagnose.is_capitation=?";

        $binds = array($id, 1);
        $query = $this->db->query($sql, $binds);

        return $query->result();
    }

    public function getHospitalAuthBills($id)
    {
        $sql = "SELECT 
                    CASE 
	                    WHEN (xin_clients_diagnose.is_capitation is NULL OR xin_clients_diagnose.is_capitation=0) AND xin_clients_diagnose.diagnose_user_type='C' THEN
                            CONCAT(IFNULL(xin_clients.`name`,''),' ',IFNULL(xin_clients.`last_name`,''),' ',IFNULL(xin_clients.`other_name`,''))	
	                    WHEN (xin_clients_diagnose.is_capitation is NULL OR xin_clients_diagnose.is_capitation=0) AND xin_clients_diagnose.diagnose_user_type='D' THEN
                            CONCAT(IFNULL(xin_clients_family.`name`,''),' ',IFNULL(xin_clients_family.`last_name`,''),' ',IFNULL(xin_clients_family.`other_name`,''))	
	                     WHEN  xin_clients_diagnose.is_capitation=1 THEN
		                    xin_clients_capitation.`name`
                        END  AS 'name',
                        xin_clients_diagnose.diagnose_id,
                        xin_clients_diagnose.diagnose_client_id,
                        xin_clients_diagnose.diagnose_generated_code,
                        xin_clients_diagnose.diagnose_status,
                        xin_clients_diagnose.diagnose_bill_reject_reason,
                        xin_clients_diagnose.diagnose_total_sum,
                        xin_clients_diagnose.diagnose_date,
                        xin_clients_diagnose.diagnose_medical,
                        xin_clients_diagnose.diagnose_investigation,
                        xin_clients_diagnose.diagnose_procedure,
                        xin_clients_diagnose.diagnose_diagnose,
                        xin_clients_diagnose.is_capitation,
                        xin_clients_diagnose.diagnose_user_type,
                        xin_clients_diagnose.diagnose_bill_status,
                        xin_clients_diagnose.diagnose_reject_reason,
                        xin_clients_diagnose.diagnose_date_of_birth

                        FROM xin_clients_diagnose
                        Left JOIN xin_clients ON xin_clients.client_id= xin_clients_diagnose.diagnose_client_id
                        Left JOIN xin_clients_family ON xin_clients_family.clients_family_id= xin_clients_diagnose.diagnose_client_id
                        Left JOIN xin_clients_capitation ON xin_clients_capitation.id= xin_clients_diagnose.diagnose_client_id
                        WHERE xin_clients_diagnose.diagnose_hospital_id=? AND xin_clients_diagnose.amount_deduct_status=? 
                        AND 
                        (
                        (xin_clients_diagnose.is_capitation=0 or xin_clients_diagnose.is_capitation is NULL)
                        and ((xin_clients_diagnose.diagnose_user_type='C' AND xin_clients.client_id= xin_clients_diagnose.diagnose_client_id) 
                        OR (xin_clients_diagnose.diagnose_user_type='D' AND xin_clients_family.clients_family_id= xin_clients_diagnose.diagnose_client_id))
                        OR (xin_clients_capitation.id=xin_clients_diagnose.diagnose_client_id AND xin_clients_diagnose.is_capitation=1)
                        ) ORDER BY xin_clients_diagnose.diagnose_date DESC";

        $binds = array($id,1);
        $query = $this->db->query($sql, $binds);

        return $query->result();
    }


    public function getHospitalApprovedBills($id)
    {
        $sql = "SELECT 
                    CASE 
	                    WHEN (xin_clients_diagnose.is_capitation is NULL OR xin_clients_diagnose.is_capitation=0) AND xin_clients_diagnose.diagnose_user_type='C' THEN
                            CONCAT(IFNULL(xin_clients.`name`,''),' ',IFNULL(xin_clients.`last_name`,''),' ',IFNULL(xin_clients.`other_name`,''))	
	                    WHEN (xin_clients_diagnose.is_capitation is NULL OR xin_clients_diagnose.is_capitation=0) AND xin_clients_diagnose.diagnose_user_type='D' THEN
                            CONCAT(IFNULL(xin_clients_family.`name`,''),' ',IFNULL(xin_clients_family.`last_name`,''),' ',IFNULL(xin_clients_family.`other_name`,''))	
	                     WHEN  xin_clients_diagnose.is_capitation=1 THEN
		                    xin_clients_capitation.`name`
                        END  AS 'name',
                        xin_clients_diagnose.diagnose_id,
                        xin_clients_diagnose.diagnose_client_id,
                        xin_clients_diagnose.diagnose_generated_code,
                        xin_clients_diagnose.diagnose_status,
                        xin_clients_diagnose.diagnose_bill_reject_reason,
                        xin_clients_diagnose.diagnose_total_sum,
                        xin_clients_diagnose.diagnose_date,
                        xin_clients_diagnose.diagnose_medical,
                        xin_clients_diagnose.diagnose_investigation,
                        xin_clients_diagnose.diagnose_procedure,
                        xin_clients_diagnose.diagnose_diagnose,
                        xin_clients_diagnose.is_capitation,
                        xin_clients_diagnose.diagnose_user_type,
                        xin_clients_diagnose.diagnose_bill_status,
                        xin_clients_diagnose.diagnose_reject_reason,
                        xin_clients_diagnose.diagnose_date_of_birth

                        FROM xin_clients_diagnose
                        Left JOIN xin_clients ON xin_clients.client_id= xin_clients_diagnose.diagnose_client_id
                        Left JOIN xin_clients_family ON xin_clients_family.clients_family_id= xin_clients_diagnose.diagnose_client_id
                        Left JOIN xin_clients_capitation ON xin_clients_capitation.id= xin_clients_diagnose.diagnose_client_id
                        Left JOIN xin_finance_new_bill ON xin_finance_new_bill.newbill_tid= xin_clients_diagnose.diagnose_transaction_id
                        WHERE xin_clients_diagnose.diagnose_hospital_id=? AND xin_finance_new_bill.newbill_tid= xin_clients_diagnose.diagnose_transaction_id
                        AND 
                        (
                        (xin_clients_diagnose.is_capitation=0 or xin_clients_diagnose.is_capitation is NULL)
                        and ((xin_clients_diagnose.diagnose_user_type='C' AND xin_clients.client_id= xin_clients_diagnose.diagnose_client_id) 
                        OR (xin_clients_diagnose.diagnose_user_type='D' AND xin_clients_family.clients_family_id= xin_clients_diagnose.diagnose_client_id))
                        OR (xin_clients_capitation.id=xin_clients_diagnose.diagnose_client_id AND xin_clients_diagnose.is_capitation=1)
                        ) ORDER BY xin_clients_diagnose.diagnose_date DESC";

        $binds = array($id);
        $query = $this->db->query($sql, $binds);

        return $query->result();
    }

    public function getCapitationApprovedBills($id)
    {


        $sql = "select xin_clients_capitation.name,xin_clients_diagnose.diagnose_id,xin_clients_diagnose.diagnose_client_id,xin_clients_diagnose.diagnose_date,xin_clients_diagnose.diagnose_diagnose,xin_clients_diagnose.diagnose_procedure,xin_clients_diagnose.diagnose_investigation,xin_clients_diagnose.diagnose_medical,xin_clients_diagnose.diagnose_total_sum,xin_clients_diagnose.diagnose_generated_code,xin_clients_diagnose.diagnose_status,xin_clients_diagnose.diagnose_reject_reason,xin_clients_diagnose.diagnose_date,xin_clients_diagnose.diagnose_hospital_id,xin_clients_diagnose.diagnose_bill_status,xin_clients_diagnose.diagnose_reject_reason,xin_clients_diagnose.diagnose_bill_reject_reason from xin_clients_capitation,xin_clients_diagnose where xin_clients_diagnose.diagnose_hospital_id=? and xin_clients_capitation.id=xin_clients_diagnose.diagnose_client_id and xin_clients_diagnose.amount_deduct_status=? and xin_clients_diagnose.is_capitation=?";

        $binds = array($id, 1, 1);
        $query = $this->db->query($sql, $binds);

        return $query->result();
    }

    public function getClientDiagnoseService($diagnoseId)
    {

        $sql = "select xin_services_hospital.id,xin_services_hospital.service_name,xin_services_hospital.service_price From xin_clients_diagnose_services,xin_services_hospital where xin_clients_diagnose_services.diagnose_services_id=xin_services_hospital.id and xin_clients_diagnose_services.diagnose_mains_id=? ";

        $binds = array($diagnoseId);
        $query = $this->db->query($sql, $binds);

        return $query->result();
    }

    public function getClientDiagnoseDrugs($diagnoseId)
    {

        $sql = "select xin_hospital_drugs.drug_id,xin_hospital_drugs.drug_name,xin_hospital_drugs.drug_price From xin_clients_diagnose_drugs,xin_hospital_drugs where xin_clients_diagnose_drugs.diagnose_drugs_id=xin_hospital_drugs.drug_id and xin_clients_diagnose_drugs.diagnose_maind_id=? ";

        $binds = array($diagnoseId);
        $query = $this->db->query($sql, $binds);

        return $query->result();
    }

    public function getTotalNoOfHospitalRequest($hospitalId)
    {
        return $this->db->from('xin_clients_diagnose')->where('diagnose_hospital_id', $hospitalId)->get()->num_rows();
    }

    public function getTotalNoOfHospitalComplaint($hospitalId)
    {
        return $this->db->from('xin_support_tickets')->where('is_hc', $hospitalId)->get()->num_rows();
    }

    public function getTotalNoOfHospitalEnrollee($hospitalId)
    {
        return $this->db->from('xin_clients')->where('hospital_id', $hospitalId)->get()->num_rows();
    }

    public function getTotalNoOfHospitalDependents($hospitalId)
    {
        return $this->db->from('xin_clients_family')->where('hospital_id', $hospitalId)->get()->num_rows();
    }

    public function getClientState($locationId)
    {
        return $this->db->from('xin_location')->where('location_id', $locationId)->get()->result();
    }

    public function getSubscriptionBands($subscriptionId)
    {
        return $this->db->from('xin_subscription')->where('subscription_id', $subscriptionId)->get()->result();
    }


    function getSubscriptionHospitals($bands)
    {
        $sql = "select xin_hospital.hospital_id,xin_hospital.hospital_name,xin_hospital.address,xin_hospital.band_id,xin_location.location_name from xin_hospital,xin_location where xin_location.location_id=xin_hospital.location_id and xin_hospital.band_id=?";
//        $sql = "select xin_hospital.hospital_id,xin_hospital.hospital_name,xin_hospital.band_id ,xin_hospital.loc_id from xin_hospital where xin_hospital.band_id=?";

        $binds = array($bands);
        $query = $this->db->query($sql, $binds);

        return $query->result();
    }

    function getBandHospitalCount($bands)
    {
        $sql = "select count(*) as hospitalCount from xin_hospital where band_id=?";

        $binds = array($bands);
        $query = $this->db->query($sql, $binds);

        return $query->result();
    }

    function updateRequestStatus($diagnoseId, $requestStatus)
    {
        $this->db->where('diagnose_id', $diagnoseId)->update('xin_clients_diagnose', $requestStatus);
        return array('status' => 200, 'message' => 'Data has been updated.');
    }

    function updateBillStatus($diagnoseId, $billStatus)
    {
        $this->db->where('diagnose_id', $diagnoseId)->update('xin_clients_diagnose', $billStatus);
        return array('status' => 200, 'message' => 'Data has been updated.');
    }

    function getClientHospitalData($hospitalId)
    {
        $sql = "select xin_hospital.hospital_id,xin_hospital.hospital_name,xin_hospital.address,xin_hospital.band_id,xin_location.location_name from xin_hospital,xin_location where xin_location.location_id=xin_hospital.location_id and xin_hospital.hospital_id=?";
//        $sql = "select xin_hospital.hospital_id,xin_hospital.hospital_name,xin_hospital.band_id ,xin_hospital.loc_id from xin_hospital where xin_hospital.band_id=?";

        $binds = array($hospitalId);
        $query = $this->db->query($sql, $binds);

        return $query->result();
    }


    function updateHospitalRequestData($requestId, $data)
    {
        $this->db->where('diagnose_id', $requestId)->update('xin_clients_diagnose', $data);
        return array('status' => 200, 'message' => 'Data has been updated.');
    }


    function deleteRequestDrugs($requestId)
    {
        $this->db->where('diagnose_maind_id', $requestId);
        $this->db->delete('xin_clients_diagnose_drugs');
        return array('status' => 200, 'message' => 'Data has been Deleted.');

    }

    function deleteRequestServices($requestId)
    {
        $this->db->where('diagnose_mains_id', $requestId);
        $this->db->delete('xin_clients_diagnose_services');
        return array('status' => 200, 'message' => 'Data has been Deleted.');

    }

}

