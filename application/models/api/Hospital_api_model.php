<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created_at: 2019-12-10
 * Created_by: Afes Oktavianus
 * Class Hospital_api_model
 */
class Hospital_api_model extends CI_Model {

    //Models
    const hospital_id = "hospital_id";
    const hospital_name = "hospital_name";
    const location_id = "location_id";
    const band_id = "band_id";
    const tarrif = "tarrif";
    const email = "email";
    const password = "password";
    const phone = "phone";
    const created_on = "created_on";
    const logo_img = "logo_img";
    const TABLE = "xin_hospital";

//for inisialisasi.
    public $hospital_id;
    public $hospital_name;
    public $location_id;
    public $band_id;
    public $tarrif;
    public $email;
    public $password;
    public $phone;
    public $created_on;
    public $logo_img;

    var $table = 'xin_hospital';
    var $table2 = 'xin_services_hospital';
    var $primary_key = 'hospital_id';

    /**
     * @param null|array|string $where
     * @param array $order
     * @return array|bool|Hospital_api_model
     */
    public function find_first($where = null, $order = []) {
        //cek order
        if ($order) {
            foreach ($order as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        //cek where
        $data = $where ? $this->db->get_where($this->table, $where) : $this->db->get($this->table);
        $result = $data->num_rows();
        return empty($result) ? FALSE : $data->row();
    }

    /**
     * @param null|array|string $where
     * @param array $order
     * @return array|bool|Hospital_api_model
     */
    public function find($where = null, $order = []) {
        //cek order
        if ($order) {
            foreach ($order as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        //cek where
        $data = $where ? $this->db->get_where($this->table, $where) : $this->db->get($this->table);
        $result = $data->num_rows();
        //return
        return empty($result) ? FALSE : $data->result();
    }

    /**
     * @param null $where
     * @param array $order
     * @return int
     */
    public function count_where($where = null) {
        //cek where
        $data = $where ? $this->db->get_where($this->table, $where) : $this->db->get($this->table);
        //return
        return $data->num_rows();
    }

    public function count() {

        $this->db->from($this->table);
        $sql = $this->db->get();
        $result = $sql->result_array();

        $response = [
            "Message" => "Number of table " . $this->table . " found: " . count($result),
            "Status" => "Success",
            "Count" => $result,
        ];

        return $response;
    }

    /**
     * @param null $column
     * @param null $where
     * @return int
     */
    public function sum($column = null, $where = null) {
        //cek where
        $this->db->select_sum($column, 'total');
        $data = $where ? $this->db->get_where($this->table2, $where) : $this->db->get($this->table2);
        //return
        return $data->row()->total ?: 0;
    }

    /**
     * @param null $column
     * @param null $where
     * @return int
     */
    public function sum_invoice($column = null, $id = null) {
        return $this->db->select_sum($column)->from($this->table2)->where('hospital_id', $id)->get()->row();
    }

    /**
     * @param array $data
     * @return array | Hospital_api_model
     */
    public function select($data = []) {
        if (empty($data)) {
            $data = $this->db->get($this->table);
        } else {
            if (isset($data['column'])) {
                $this->db->select($data['column']);
            }
            if (isset($data['group'])) {
                $this->db->group_by($data['group']);  // Produces: GROUP BY title, date
            }
            if (isset($data['join'])) {
                foreach ($data['join'] as $key => $value) {
                    $this->db->join($key, $value);
                }
            }
            if (isset($data['order'])) {
                foreach ($data['order'] as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
            if (isset($data['limit'])) {
                $this->db->limit($data['limit']);
            }
            $data = isset($data['where']) ? $this->db->get_where($this->table . " a", $data['where']) : $this->db->get($this->table . " a");
        }
        return $data->result();
    }

    /**
     * @param array $data
     * @return array | Hospital_api_model
     */
    public function select_first($data = []) {
        if (empty($data)) {
            $data = $this->db->get($this->table);
        } else {
            if (isset($data['column'])) {
                $this->db->select($data['column']);
            }
            if (isset($data['order'])) {
                foreach ($data['order'] as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
            $this->db->limit(1);
            $data = $data['where'] ? $this->db->get_where($this->table, $data['where']) : $this->db->get($this->table);
        }
        return $data->row();
    }

// response if a field is empty

    public function add_hospital($data) {
        if (empty($data)) {
            return $this->empty_response();
        } else {
            $insert = $this->db->insert("xin_hospital", $data);
            if ($insert) {
                $response['status'] = 200;
                $response['error'] = false;
                $response['message'] = 'Success add records hospital.';
                return $response;
            } else {
                $response['status'] = 502;
                $response['error'] = true;
                $response['message'] = 'Failed add records hospital.';
                return $response;
            }
        }
    }

// function to insert data into the xin_hospital table

    public function empty_response() {
        $response['status'] = 502;
        $response['error'] = true;
        $response['message'] = 'field is empty';
        return $response;
    }

// get records hospital

    public function all_hospital() {
        $all = $this->find();
        if (empty($all)) {
            $response['status'] = 502;
            $response['error'] = true;
        } else {
            $response['status'] = 200;
            $response['error'] = false;
            $response['person'] = $all;
        }
        return $response;
    }

// hapus data hospital
    public function delete_hospital($id) {
        if ($id == '') {
            return $this->empty_response();
        } else {
            $where = array(
                "id" => $id
            );
            $this->db->where($where);
            $delete = $this->db->delete("xin_hospital");
            if ($delete) {
                $response['status'] = 200;
                $response['error'] = false;
                $response['message'] = 'Records hospital success for destroy.';
                return $response;
            } else {
                $response['status'] = 502;
                $response['error'] = true;
                $response['message'] = 'Records hospital failed for destroy..';
                return $response;
            }
        }
    }

// update hospital
    public function update_hospital($id, $name, $address, $phone) {
        if ($id == '' || empty($name) || empty($address) || empty($phone)) {
            return $this->empty_response();
        } else {
            $where = array(
                "id" => $id
            );
            $set = array(
                "name" => $name,
                "address" => $address,
                "phone" => $phone
            );
            $this->db->where($where);
            $update = $this->db->update("xin_hospital", $set);
            if ($update) {
                $response['status'] = 200;
                $response['error'] = false;
                $response['message'] = 'Success records hospital changed.';
                return $response;
            } else {
                $response['status'] = 502;
                $response['error'] = true;
                $response['message'] = 'Failed records hospital changed.';
                return $response;
            }
        }
    }

    var $client_service = "frontend-client";
    var $auth_key = "simplerestapi";

    public function check_auth_client() {
        $client_service = $this->input->get_request_header('Client-Service', TRUE);
        $auth_key = $this->input->get_request_header('Auth-Key', TRUE);
        if ($client_service == $this->client_service && $auth_key == $this->auth_key) {
            return true;
        } else {
            return json_output(401, array('status' => 401, 'message' => 'Unauthorized.'));
        }
    }

    public function login($username, $password) {
        $q = $this->db->from($this->table)->where('hospital_id', $username)->get()->row();
        if (empty($q)) {
            return array('status' => 204, 'message' => 'Username not found.');
        } else {
            $hashed_password = $q->password;
            $id = $q->id;
            if (hash_equals($hashed_password, crypt($password, $hashed_password))) {
                $last_login = date('Y-m-d H:i:s');
                $token = crypt(substr(md5(rand()), 0, 7));
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->trans_start();
                $this->db->where('id', $id)->update($this->table, array('last_login' => $last_login));
                $this->db->insert('xin_users_authentication', array('users_id' => $id, 'token' => $token, 'expired_at' => $expired_at));
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return array('status' => 500, 'message' => 'Internal server error.');
                } else {
                    $this->db->trans_commit();
                    return array('status' => 200, 'message' => 'Successfully login.', 'id' => $id, 'token' => $token);
                }
            } else {
                return array('status' => 204, 'message' => 'Wrong password.');
            }
        }
    }

    public function logout() {
        $users_id = $this->input->get_request_header('hospital_id', TRUE);
        $token = $this->input->get_request_header('Authorization', TRUE);
        $this->db->where('users_id', $users_id)->where('token', $token)->delete('xin_users_authentication');
        return array('status' => 200, 'message' => 'Successfully logout.');
    }

    public function auth() {
        $users_id = $this->input->get_request_header('hospital_id', TRUE);
        $token = $this->input->get_request_header('Authorization', TRUE);
        $q = $this->db->select('expired_at')->from('xin_users_authentication')->where('users_id', $users_id)->where('token', $token)->get()->row();
        if ($q == "") {
            return json_output(401, array('status' => 401, 'message' => 'Unauthorized.'));
        } else {
            if ($q->expired_at < date('Y-m-d H:i:s')) {
                return json_output(401, array('status' => 401, 'message' => 'Your session has expired.'));
            } else {
                $updated_at = date('Y-m-d H:i:s');
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->where('users_id', $users_id)->where('token', $token)->update('xin_users_authentication', array('expired_at' => $expired_at, 'updated_at' => $updated_at));
                return array('status' => 200, 'message' => 'Authorized.');
            }
        }
    }

}
