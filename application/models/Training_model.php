<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class training_model extends CI_Model {

 

    public function __construct()

    {

        parent::__construct();

        $this->load->database();

    }

 

	// get training

	public function get_training() {

	  return $this->db->get("xin_training");

	}

	

	// get training type

	public function get_training_type()

	{

	  return $this->db->get("xin_training_types");

	}

	

	// all training_types

	public function all_training_types() {

	  $query = $this->db->query("SELECT * from xin_training_types");

  	  return $query->result();

	}

	 

	 public function read_training_information($id) {

	

		$sql = 'SELECT * FROM xin_training WHERE training_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return null;

		}

	}

	// get training type by id

	public function read_training_type_information($id) {

	

		$sql = 'SELECT * FROM xin_training_types WHERE training_type_id = ?';

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		

		if ($query->num_rows() > 0) {

			return $query->result();

		} else {

			return null;

		}

	}

	

	// Function to add record in table

	public function add($data){

		$this->db->insert('xin_training', $data);

		if ($this->db->affected_rows() > 0) {

			return $this->db->insert_id();

		} else {

			return false;

		}

	}

	

	// Function to add record in table

	public function add_type($data){

		$this->db->insert('xin_training_types', $data);

		if ($this->db->affected_rows() > 0) {

			return true;

		} else {

			return false;

		}

	}

	

	// Function to Delete selected record from table

	public function delete_record($id){

		$this->db->where('training_id', $id);

		$this->db->delete('xin_training');

		

	}

	

	// Function to Delete selected record from table

	public function delete_type_record($id){

		$this->db->where('training_type_id', $id);

		$this->db->delete('xin_training_types');

		

	}

	

	// Function to update record in table

	public function update_record($data, $id){

		$this->db->where('training_id', $id);

		if( $this->db->update('xin_training',$data)) {

			return true;

		} else {

			return false;

		}		

	}

	

	// Function to update record in table

	public function update_status($data, $id){

		$this->db->where('training_id', $id);

		if( $this->db->update('xin_training',$data)) {

			return true;

		} else {

			return false;

		}		

	}

	

	// Function to update record in table

	public function update_type_record($data, $id){

		$this->db->where('training_type_id', $id);

		if( $this->db->update('xin_training_types',$data)) {

			return true;

		} else {

			return false;

		}		

	}


	public function update_table_record( $id,$column,$data){

		$this->db->where('diagnose_id', $id);
		$dataUpdate = [
			$column => $data
		];
		if( $this->db->update('xin_clients_diagnose',$dataUpdate)) {

			return true;

		} else {

			return false;

		}		

	}

	// get company projects

	public function get_company_training($company_id) {

	

		$sql = 'SELECT * FROM xin_training WHERE company_id = ?';

		$binds = array($company_id);

		$query = $this->db->query($sql, $binds);

		return $query;

	}

	// get employee projects

	public function get_employee_training($id) {

	

		$sql = "SELECT * FROM `xin_training` where employee_id like '%$id,%' or employee_id like '%,$id%' or employee_id = '$id'";

		$binds = array($id);

		$query = $this->db->query($sql, $binds);

		return $query;

	}

	// get enrollee request
	public function get_enrollee_requests() {

		$sql = "SELECT * FROM `xin_clients` where approve_code != 1 ORDER BY created_at DESC";

		$query = $this->db->query($sql);

		return $query;

	}

	public function get_enrollee_requests_filtered($from,$to) {

		$sql = "SELECT * FROM `xin_clients` where approve_code != 1 AND created_at BETWEEN ? AND ? ORDER BY created_at DESC";
		$binds = array($from,$to);
		$query = $this->db->query($sql,$binds);

		return $query;

	}

	public function get_organization() {

		$sql = "SELECT * FROM `xin_organization`";

		$query = $this->db->query($sql);

		return $query;

	}

	public function get_subscription() {

		$sql = "SELECT * FROM `xin_subscription`";

		$query = $this->db->query($sql);

		return $query;

	}

	public function get_state() {

		$sql = "SELECT * FROM `xin_location`";

		$query = $this->db->query($sql);

		return $query;

	}

	// get employee projects

	public function get_hospital_last_id($id) {

		$sql = "SELECT * FROM `xin_hospital` where location_id = ? ORDER BY hospital_id DESC LIMIT 1";

		$binds = array($id);

		$query = $this->db->query($sql, $binds);
		// echo $this->db->last_query();
		// echo "\n";
		// print_r($query->result());die;
		return $query;

	}


	public function insertDataTB($table, $data){
		$this->db->insert($table,$data);
		if($this->db->insert_id() > 0)
			// echo $this->db->last_query();
			return $this->db->insert_id();
		return false;
	}


	public function getAll2($table,$where=null){
		$this->db->select();
		$this->db->from($table);

		if($where !=null){
			$this->db->where($where);
		}
		 
		$query=$this->db->get();
		if($query->num_rows() > 0)
			return $query->result();
		return false;
	}
	public function getAll22($table,$where=null){
		$this->db->limit('200', '1');
		$this->db->select();
		$this->db->from($table);

		if($where !=null){
			$this->db->where($where);
		}
		 
		$query=$this->db->get();
		if($query->num_rows() > 0)
			return $query->result();
		return false;
	}

	public function getAllSub($table,$where=null){
		$this->db->select('subscription_benifit')->from($table);
		if($where !=null){
			$this->db->where($where);
		}
		 
		$query=$this->db->get();
		if($query->num_rows() > 0)
			return $query->result_array();
		return false;
	}
	public function getOneUser($table,$where=null){
		$this->db->select()->from($table);
		if($where !=null){
			$this->db->where($where);
		}
		 
		$query=$this->db->get();
		if($query->num_rows() > 0)
			return $query->result_array();
		return false;
	}
	public function xin_clients_get(){
        $hospital = $this->session->userdata;
        //$this->db->select('xin_c.client_id as xin_c_id,xin_c.hospital_id as hopital_id,xin_c.created_at as c_created_at,xin_c.name as c_name,xin_c.last_name as c_last_name,xin_s.plan_name as subscription_plan,xin_o.name as oranization_name,xin_o.status as org_status');
        $this->db->select('xin_c.client_id as xin_c_id,xin_c.hospital_id as hopital_id,xin_c.created_at as c_created_at,xin_c.name as c_name,xin_c.last_name as c_last_name,xin_s.plan_name as subscription_plan,xin_o.name as oranization_name,xin_o.status as org_status');
        $this->db->from('xin_clients as xin_c');
       // $this->db->join('xin_clients_family as xin_c_f','xin_c_f.client_id=xin_c.client_id','left');
       $this->db->join('xin_organization as xin_o','xin_o.id=xin_c.company_name');
        $this->db->join('xin_subscription as xin_s','xin_s.subscription_id=xin_c.subscription_ids');
        $this->db->where('xin_c.approve_code',1);
       // $this->db->where('xin_c.hospital_id',$hospital['hospital_id']['hospital_id']);
        $this->db->where('xin_c.hospital_id',$hospital['hospital_id']['hospital_id']);
      //  $this->db->where(array('xin_c_f.hospital_id !='=> $hospital['hospital_id']['hospital_id']));
        $query = $this->db->get();
        if($query->num_rows() != 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }

    }
   public function getClientInfo($cid,$hid){
       $this->db->select('xin_clients_diagnose.*,xin_hospital.hospital_name as hospital_name');
       $this->db->from('xin_clients_diagnose');
       $this->db->join('xin_hospital','xin_hospital.hospital_id=xin_clients_diagnose.diagnose_hospital_id');
       $this->db->where(array('diagnose_bill_status'=>3,'diagnose_client_id'=>$cid));
       $this->db->order_by('diagnose_id', 'asc');
       $query = $this->db->get();
       if($query->num_rows() != 0)
       {
          $result=$query->result();


//          echo "<pre>";
//          print_r($result);
//          exit;
          $amount=array();
          $i=1;
          $data=array();
          $total=$query->num_rows();
          foreach ($result as $key=>$value){
              if($i==1){
                  $startDate=$value->diagnose_date;

              }
              if($i==$total){
                  $latest=$value->diagnose_date;
              }
              $i++;
              $amount[]=$value->diagnose_total_sum;
          }

           $this->db->select('xin_clients.name as name,xin_clients.last_name as last_name');
           $this->db->from('xin_clients');
           $this->db->where(array('client_id'=>$cid,'hospital_id'=>$hid));
           $query = $this->db->get();
           if($query->num_rows() != 0)
           {
               $result2=$query->result();
               $data['name']=$result2[0]->name." ".$result2[0]->last_name;
           }
           else{
               $this->db->select('xin_clients_family.name as name,xin_clients_family.last_name as last_name');
               $this->db->from('xin_clients_family');
               $this->db->where(array('clients_family_id'=>$cid,'hospital_id'=>$hid));
               $query = $this->db->get();
               if($query->num_rows() != 0)
               {
                   $result2=$query->result();
                   $data['name']=$result2[0]->name." ".$result2[0]->last_name;
               }
           }



           $data['hospital']=$result[0]->hospital_name;
           $data['total']=$total;
           $data['amount']=array_sum($amount);
           $data['start']=$startDate;
           $data['last']=$latest;

           return $data;
       }
       else
       {
           return false;
       }
   }
    public  function xin_family_clients(){
        $hospital = $this->session->userdata;
        //$this->db->select('xin_c.client_id as xin_c_id,xin_c.hospital_id as hopital_id,xin_c.created_at as c_created_at,xin_c.name as c_name,xin_c.last_name as c_last_name,xin_s.plan_name as subscription_plan,xin_o.name as oranization_name,xin_o.status as org_status');
        $this->db->select('xin_c.client_id as xin_c_id,xin_s.plan_name as subscription_plan,xin_o.name as oranization_name,xin_o.status as org_status,xin_c_f.clients_family_id as clients_family_id,xin_c_f.name as f_name,xin_c_f.last_name as f_last_name,xin_c_f.relation,xin_c_f.created_on');
      //  $this->db->from('xin_clients as xin_c');
        $this->db->from('xin_clients_family as xin_c_f');
        $this->db->join('xin_clients as xin_c','xin_c.client_id=xin_c_f.client_id');
        $this->db->join('xin_organization as xin_o','xin_o.id=xin_c.company_name');
        $this->db->join('xin_subscription as xin_s','xin_s.subscription_id=xin_c.subscription_ids');
        $this->db->where('xin_c.approve_code',1);
        // $this->db->where('xin_c.hospital_id',$hospital['hospital_id']['hospital_id']);
        $this->db->where('xin_c_f.hospital_id',$hospital['hospital_id']['hospital_id']);
        $this->db->where(array('xin_c.hospital_id !='=> $hospital['hospital_id']['hospital_id']));
        $query = $this->db->get();
        if($query->num_rows() != 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }

    }
	public function getAll21($table,$where=null,$hospital_id=null,$organisation_id=null,$fromdate=null,$todate=null){
		$this->db->select()->from($table);
		if($hospital_id != null){
			 $this->db->where('hospital_id',$hospital_id);
		 }
		 if($organisation_id != null){
			 $this->db->where('company_name',$organisation_id);
		 }
		 if($fromdate != null && $todate != null)
		 {
			$this->db->where('created_at >=',$fromdate." 00:00:01");
			$this->db->where('created_at <=',$todate." 23:59:59");
		 }
		if($where !=null){
			$this->db->where($where);
		}
		 
		$query=$this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else{	
		return false;
		}
	}

	public function getAll21_pagination($table,$where=null,$hospital_id=null,$organisation_id=null,$fromdate=null,$todate=null, $limit, $offset){
		$this->db->select("SQL_CALC_FOUND_ROWS *")->from($table);
		if($hospital_id != null){
			 $this->db->where('hospital_id',$hospital_id);
		 }
		 if($organisation_id != null){
			 $this->db->where('company_name',$organisation_id);
		 }
		 if($fromdate != null && $todate != null)
		 {
			$this->db->where('created_at >=',$fromdate." 00:00:01");
			$this->db->where('created_at <=',$todate." 23:59:59");
		 }
		if($where !=null){
			$this->db->where($where);
		}
		$this->db->limit($limit,$offset);
		 
		$query=$this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else{	
		return false;
		}
	}


	public function delete2($table,$where=null){
		if($this->db->where($where)->delete($table))
			return true;
		return false;
	}

	public function update2($table,$where,$data){
		if($this->db->set($data)->where($where)->update($table))
			return true;
		return false;
	}

}

?>