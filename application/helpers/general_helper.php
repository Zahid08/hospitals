<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function initialize_elfinder($value=''){

	$CI =& get_instance();

	$CI->load->helper('path');

	$opts = array(

	    //'debug' => true, 

	    'roots' => array(

	      array( 

	        'driver' => 'LocalFileSystem', 

	        'path'   => './uploads/files_manager/', 

	        'URL'    => site_url('uploads/files_manager').'/'

	        // more elFinder options here

	      ) 

	    )

	);

	return $opts;

}

if ( ! function_exists('get_employee_leave_category'))

{

	function get_employee_leave_category($id_nums,$employee_id) {

		$CI =&	get_instance();

		$sql = "select e.leave_categories,e.user_id,l.leave_type_id,l.type_name from xin_employees as e, xin_leave_type as l where l.leave_type_id IN ($id_nums) and e.user_id = $employee_id";

		$query = $CI->db->query($sql);

		$result = $query->result();

		return $result;

	}

}

if ( ! function_exists('get_sub_departments'))

{

	function get_sub_departments($id) {

		$CI =&	get_instance();

		$sql = "select * from xin_sub_departments where department_id = $id";

		$query = $CI->db->query($sql);

		$result = $query->result();

		return $result;

	}

}

if ( ! function_exists('get_main_departments_employees'))

{

	function get_main_departments_employees() {

		$CI =&	get_instance();

		$sql = "select d.*,e.* from xin_departments as d, xin_employees as e where d.department_id = e.department_id group by e.department_id";

		$query = $CI->db->query($sql);

		$result = $query->result();

		return $result;

	}

}

if ( ! function_exists('get_sub_departments_employees'))

{

	function get_sub_departments_employees($id,$empid) {

		$CI =&	get_instance();

		$sql = "select d.*,e.* from xin_sub_departments as d, xin_employees as e where d.sub_department_id = e.sub_department_id and e.department_id = '".$id."' and e.employee_id!= '".$empid."' group by e.sub_department_id";

		$query = $CI->db->query($sql);

		$result = $query->result();

		return $result;

	}

}

if ( ! function_exists('get_sub_departments_designations'))

{

	function get_sub_departments_designations($id,$empid,$mainid) {

		$CI =&	get_instance();

		$sql = "select d.*,e.* from xin_designations as d, xin_employees as e where d.designation_id = e.designation_id and e.employee_id!= '".$empid."' and e.employee_id!= '".$mainid."' and e.designation_id = '".$id."'";

		$query = $CI->db->query($sql);

		$result = $query->result();

		return $result;

	}

}

if ( ! function_exists('total_salaries_paid'))

{

	function total_salaries_paid() {

			$CI =&	get_instance();

			$CI->db->from('xin_salary_payslips');

			$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->result();

			$tinc = 0;

			foreach($result as $inc){

				$tinc += $inc->net_salary;

			}

			return $tinc;

		}else{

			return 0;

		}

	}



}

if ( ! function_exists('count_leaves_info'))

{

	function count_leaves_info($leave_type_id,$employee_id) {

			$CI =&	get_instance();

			$CI->db->from('xin_leave_applications');

			$CI->db->where('employee_id',$employee_id);

			$CI->db->where('leave_type_id',$leave_type_id);

			$CI->db->where('status!=',3);

			$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->result();

			$tinc = 0;

			foreach($result as $inc){

				$ifrom_date =  $inc->from_date;

				$ito_date =  $inc->to_date;

				$datetime1 = new DateTime($ifrom_date);

				$datetime2 = new DateTime($ito_date);

				$interval = $datetime1->diff($datetime2);

				if(strtotime($inc->from_date) == strtotime($inc->to_date)){

					$tinc +=1;

				} else {

					$tinc += $interval->format('%a');

				}

				

			}

			return $tinc;

		}else{

			return 0;

		}

	}



}

if ( ! function_exists('total_tickets'))

{

	function total_tickets($eid=0) {

		$CI =&	get_instance();

		$CI->load->model('Xin_model');
		
		$role_resources_ids = $CI->Xin_model->user_role_resource();
		$user_info = $CI->Xin_model->read_employee_info($eid);

		$CI->db->from('xin_support_tickets');

		if ($eid!=0) {
			if($user_info[0]->user_role_id!=1 || !in_array('309',$role_resources_ids)){

				$CI->db->where("`employee_id` = $eid");

			} 
		}

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('total_memos'))

{

	function total_memos($id = null) {

		$CI =&	get_instance();

		$CI->db->from('xin_support_memo');

		if ($id != null) {
			$query=$CI->db->where("`made_by` = $id");
		}

			$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('total_open_memos'))

{

	function total_open_memos($id = null) {

		$CI =&	get_instance();

		$CI->db->from('xin_support_memo');

		if ($id != null) {
			$query=$CI->db->where("`made_by` = $id AND approval != 4");
		}

			$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('total_closed_memos'))

{

	function total_closed_memos($id = null) {

		$CI =&	get_instance();

		$CI->db->from('xin_support_memo');

		if ($id != null) {
			$query=$CI->db->where("`made_by` = $id AND approval = 4");
		}

			$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('total_open_tickets'))

{

	function total_open_tickets($eid=0) {

		$CI =&	get_instance();

		$CI->load->model('Xin_model');
		
		$role_resources_ids = $CI->Xin_model->user_role_resource();
		$user_info = $CI->Xin_model->read_employee_info($eid);

		$CI->db->from('xin_support_tickets');

		if ($eid != 0) {

			if($user_info[0]->user_role_id!=1 || !in_array('309',$role_resources_ids)){

				$CI->db->where("`employee_id` = $eid");

			}
		}

		$CI->db->where('ticket_status',1);

		$query = $CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('total_closed_tickets'))

{

	function total_closed_tickets($eid=0) {

		$CI =&	get_instance();

		$CI->load->model('Xin_model');
		
		$role_resources_ids = $CI->Xin_model->user_role_resource();
		$user_info = $CI->Xin_model->read_employee_info($eid);

		$CI->db->from('xin_support_tickets');

		if ($eid != 0) {

			if($user_info[0]->user_role_id!=1 || !in_array('309',$role_resources_ids)){

				$CI->db->where("`employee_id` = $eid");

			}

		}
		
		$CI->db->where('ticket_status',2);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('active_employees'))

{

	function active_employees() {

		$CI =&	get_instance();

		$CI->db->from('xin_employees');

		$CI->db->where('is_active',1);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('inactive_employees'))

{

	function inactive_employees() {

		$CI =&	get_instance();

		$CI->db->from('xin_employees');

		$CI->db->where('is_active',0);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('completed_tasks'))

{

	function completed_tasks() {

		$CI =&	get_instance();

		$CI->db->from('xin_tasks');

		$CI->db->where('task_status',2);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('inprogress_tasks'))

{

	function inprogress_tasks() {

		$CI =&	get_instance();

		$CI->db->from('xin_tasks');

		$CI->db->where('task_status',1);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('total_account_balances'))

{

	function total_account_balances() {

			$CI =&	get_instance();

			$CI->db->from('xin_finance_bankcash');

			$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->result();

			$tinc = 0;

			foreach($result as $inc){

				$tinc += $inc->account_balance;

			}

			return $tinc;

		}else{

			return 0;

		}

	}



}

//after v1.0.11

if ( ! function_exists('system_settings_info'))

{

		function system_settings_info($id) {

			$CI =&	get_instance();

			$CI->db->from('xin_system_setting');

			$CI->db->where('setting_id',$id);

			$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->row();

			return $result;

		}else{

			return "";

		}

	}



}

if ( ! function_exists('xin_company_info'))

{

		function xin_company_info($id) {

			$CI =&	get_instance();

			$CI->db->from('xin_company_info');

			$CI->db->where('company_info_id',$id);

			$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->row();

			return $result;

		}else{

			return "";

		}

	}



}

if ( ! function_exists('read_invoice_record'))

{

		function read_invoice_record($id) {

			$CI =&	get_instance();

			$CI->db->from('xin_hrsale_invoices');

			$CI->db->where('invoice_id',$id);

			$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->row();

			return $result;

		}else{

			return "";

		}

	}

}

if ( ! function_exists('get_invoice_transaction_record'))

{

	function get_invoice_transaction_record($id) {

		$CI =&	get_instance();

		$CI->db->from('xin_finance_transaction');

		$CI->db->where('transaction_type','income');

		$CI->db->where('invoice_id',$id);

		$query=$CI->db->get();

		return $query;

	}

}

if ( ! function_exists('system_currency_sign'))

{

	//set currency sign

	function system_currency_sign($number) {

		

		// get details

		$system_setting = system_settings_info(1);

		// currency code/symbol

		if($system_setting->show_currency=='code'){

			$ar_sc = explode(' -',$system_setting->default_currency_symbol);

			$sc_show = $ar_sc[0];

		} else {

			$ar_sc = explode('- ',$system_setting->default_currency_symbol);

			$sc_show = $ar_sc[1];

		}

		if($system_setting->currency_position=='Prefix'){

			$sign_value = $sc_show.''.$number;

		} else {

			$sign_value = $number.''.$sc_show;

		}

		return $sign_value;

	}

}

//single client 

if ( ! function_exists('clients_invoice_paid_count'))

{

	function clients_invoice_paid_count($cid) {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_invoices');

		$CI->db->where('client_id',$cid);

		$CI->db->where('status',1);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

// all

if ( ! function_exists('all_invoice_paid_count'))

{

	function all_invoice_paid_count() {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_invoices');

		$CI->db->where('status',1);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

// all

if ( ! function_exists('all_invoice_unpaid_count'))

{

	function all_invoice_unpaid_count() {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_invoices');

		$CI->db->where('status',0);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('clients_invoice_unpaid_count'))

{

	function clients_invoice_unpaid_count($cid) {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_invoices');

		$CI->db->where('client_id',$cid);

		$CI->db->where('status',0);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('clients_project_inprogress'))

{

	function clients_project_inprogress($cid) {

		$CI =&	get_instance();

		$CI->db->from('xin_projects');

		$CI->db->where('client_id',$cid);

		$CI->db->where('status',1);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('clients_project_completed'))

{

	function clients_project_completed($cid) {

		$CI =&	get_instance();

		$CI->db->from('xin_projects');

		$CI->db->where('client_id',$cid);

		$CI->db->where('status',2);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('clients_project_notstarted'))

{

	function clients_project_notstarted($cid) {

		$CI =&	get_instance();

		$CI->db->from('xin_projects');

		$CI->db->where('client_id',$cid);

		$CI->db->where('status',0);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('clients_project_deffered'))

{

	function clients_project_deffered($cid) {

		$CI =&	get_instance();

		$CI->db->from('xin_projects');

		$CI->db->where('client_id',$cid);

		$CI->db->where('status',3);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('clients_invoice_paid_amount'))

{

	function clients_invoice_paid_amount($cid) {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_invoices');

		$CI->db->where('client_id',$cid);

		$CI->db->where('status',1);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->result();

			$tinc = 0;

			foreach($result as $inc){

				$tinc += $inc->grand_total;

			}

			return $tinc;

		}else{

			return 0;

		}

	}

}

// all

if ( ! function_exists('all_invoice_paid_amount'))

{

	function all_invoice_paid_amount($from = null, $to = null) {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_invoices');

		if (!is_null($from) AND !is_null($to)) {
			$CI->db->where("created_at BETWEEN '$from'AND '$to'");
		}

		$CI->db->where('status',1);


		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->result();

			$tinc = 0;

			foreach($result as $inc){

				$tinc += $inc->grand_total;

			}

			return $tinc;

		}else{

			return 0;

		}

	}

}

// all

if ( ! function_exists('all_invoice_unpaid_amount'))

{

	function all_invoice_unpaid_amount() {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_invoices');

		$CI->db->where('status',0);

		$query=$CI->db->get();

		// print_r($query->result());die;

		if ($query->num_rows() > 0) {

			$result = $query->result();

			$tinc = 0;

			foreach($result as $inc){

				$tinc += $inc->grand_total;

			}

			return $tinc;

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('clients_invoice_unpaid_amount'))

{

	function clients_invoice_unpaid_amount($cid) {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_invoices');

		$CI->db->where('client_id',$cid);

		$CI->db->where('status',0);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->result();

			$tinc = 0;

			foreach($result as $inc){

				$tinc += $inc->grand_total;

			}

			return $tinc;

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('last_client_invoice_info'))

{

	function last_client_invoice_info() {

		$CI =&	get_instance();

		$sql = 'SELECT * FROM xin_hrsale_invoices order by invoice_id desc limit 1';

		$query = $CI->db->query($sql);		

		if ($query->num_rows() > 0) {

			$inv = $query->result();

			if(!is_null($inv)) {

				return $invid = $inv[0]->invoice_id;

			} else {

				return $invid = 0;

			}

		} else {

			return $invid = 0;

		}

	}

}

if ( ! function_exists('total_travel_expense'))

{

	function total_travel_expense() {

		$CI =&	get_instance();

		$CI->db->from('xin_employee_travels');

		$CI->db->where('status',1);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->result();

			$tinc = 0;

			foreach($result as $inc){

				$tinc += $inc->actual_budget;

			}

			return $tinc;

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('cr_quote_quoted'))

{

	function cr_quote_quoted() {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_quotes');

		$CI->db->where('status',0);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('cr_quote_project_created'))

{

	function cr_quote_project_created() {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_quotes');

		$CI->db->where('status',1);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('cr_quote_inprogress'))

{

	function cr_quote_inprogress() {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_quotes');

		$CI->db->where('status',2);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('cr_quote_project_completed'))

{

	function cr_quote_project_completed() {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_quotes');

		$CI->db->where('status',3);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('cr_quote_invoiced'))

{

	function cr_quote_invoiced() {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_quotes');

		$CI->db->where('status',4);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('cr_quote_paid'))

{

	function cr_quote_paid() {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_quotes');

		$CI->db->where('status',5);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

if ( ! function_exists('cr_quote_deffered'))

{

	function cr_quote_deffered() {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_quotes');

		$CI->db->where('status',6);

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

// Total clients

if ( ! function_exists('total_clients'))

{

	function total_clients() {

		$CI =&	get_instance();

		$CI->db->from('xin_clients');

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

// Total dependants

if ( ! function_exists('total_dependants'))

{

	function total_dependants() {

		$CI =&	get_instance();

		$CI->db->from('xin_clients_family');

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}


// Total individual account

if ( ! function_exists('total_individual_account'))

{

	function total_individual_account() {

		$CI =&	get_instance();

		$CI->db->from('xin_clients');

		$CI->db->where('ind_family','individual');

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}


// Total individual account

if ( ! function_exists('total_family_account'))

{

	function total_family_account() {

		$CI =&	get_instance();

		$CI->db->from('xin_clients');

		$CI->db->where('ind_family','family');

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

// Total individual account

if ( ! function_exists('total_invoices'))

{

	function total_invoices() {

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_invoices');

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

// Total bill payments

if ( ! function_exists('total_bill_payments'))

{

	function total_bill_payments() {

		$CI =&	get_instance();

		$CI->db->from('xin_change_hospital_request');

		$CI->db->where('status','approved');

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

// Total organization

if ( ! function_exists('total_organization'))

{

	function total_organization() {

		$CI =&	get_instance();

		$CI->db->from('xin_organization');

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

// Total Invoice Expire

if ( ! function_exists('total_invoice_expire'))

{

	function total_invoice_expire() {

		$date_now = date_create(date("Y-m-d"));
		$date = date_add($date_now, date_interval_create_from_date_string('90 days'));
		
		$date_now = date_format(date_create(date("Y-m-d")),"Y-m-d");
		$date = date_format($date, "Y-m-d");

		// print_r($date);
		// print_r($date_now);

		// die;

		$CI =&	get_instance();

		$CI->db->from('xin_hrsale_invoices');

		$CI->db->where("invoice_end_date BETWEEN '$date_now' AND '$date'");

		$query=$CI->db->get();

		// print_r($query);die;

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

// Total Hospital

if ( ! function_exists('total_hospital'))

{

	function total_hospital() {

		$CI =&	get_instance();

		$CI->db->from('xin_hospital');

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

// Total Auth Code

if ( ! function_exists('total_auth_code'))

{

	function total_auth_code($code = null) {

		$CI =&	get_instance();

		$CI->db->from('xin_clients_diagnose');
		if (is_null($code)) {
			$CI->db->where("diagnose_status != 2");
		}else{
			$CI->db->where("diagnose_status = 2");
		}

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

// Total Bill

if ( ! function_exists('total_bill'))

{

	function total_bill($approved = null) {

		$CI =&	get_instance();

		$CI->db->from('xin_clients_diagnose');
		if (is_null($approved)) {
			$CI->db->where("diagnose_bill_status = 2");
		}else{
			$CI->db->where("diagnose_bill_status = 3");
		}

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

// Total Enrollee Request

if ( ! function_exists('total_enrollee_request'))

{

	function total_enrollee_request() {

		$CI =&	get_instance();

		$CI->db->from('xin_clients');

		$CI->db->where("approve_code != 1");

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

// Total Hospital

if ( ! function_exists('total_expense'))

{

	function total_expense($from = null, $to = null) {

		$CI =&	get_instance();

		$CI->db->from('xin_finance_transaction');

		if (!is_null($from) AND !is_null($to)) {
			$CI->db->where("created_at BETWEEN '$from' AND '$to'");
		}

		$CI->db->where("transaction_type = 'expense'");

		$query=$CI->db->get();

		$total = array();
		

		if ($query->num_rows() > 0) {
			foreach($query->result() as $e)
			{
				array_push($total, $e->amount);
			}

			return array_sum($total);

		}else{

			return 0;

		}

	}

}

// Total Hospital Expense

if ( ! function_exists('total_hospital_expense'))

{

	function total_hospital_expense($from = null, $to = null) {

		$CI =&	get_instance();

		$CI->db->from('xin_clients_diagnose');

		if (!is_null($from) AND !is_null($to)) {
			$CI->db->where("diagnose_date_time BETWEEN '$from' AND '$to'");
		}

		$CI->db->where('amount_deduct_status = 1');

		$query=$CI->db->get();

		$result = $query->result();

		$total = array();
		if ($result != '') {
			foreach ($result as $r) {
				array_push($total, $r->diagnose_total_sum); 
			}

			return array_sum($total);
		}else{
			return '0';
		}

		

	}

}


// Total Hospital Expense Bulk

if ( ! function_exists('total_hospital_expense_bulk'))

{

	function total_hospital_expense_bulk($from = null, $to = null) {

		$CI =&	get_instance();

		$CI->db->from('xin_clients_diagnose_bulk');

		$CI->db->where('is_deducted = 1');

		$query=$CI->db->get();

		$result = $query->result();

		$total = array();
		if ($result != '') {
			foreach ($result as $r) {
				$ids = explode(",", $r->bulk_id);

				if (!is_null($ids)) {
					foreach ($ids as $id) {
						$CI->db->from('xin_clients_diagnose');
						$CI->db->where("diagnose_id = $id");

						if (!is_null($from) AND !is_null($to)) {
							$CI->db->where("diagnose_date_time BETWEEN '$from' AND '$to'");
						}

						$query=$CI->db->get();

						$result = $query->result();

						if (!empty($result)) {
							array_push($total, $result[0]->diagnose_total_sum);
						}
					}
				}
			}

			return array_sum($total);
		}else{
			return '0';
		}	

		

	}

}

// Total Capitation Principal

if ( ! function_exists('total_capitation_principal'))

{

	function total_capitation_principal($from = null, $to = null) {

		$CI =&	get_instance();

		$CI->db->from('xin_clients_capitation');
		if (!is_null($from) AND !is_null($to)) {
			$CI->db->where("created_at BETWEEN '$from' AND '$to'");
		}

		$CI->db->where("type = 'p'");

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}

// Total Capitation Dependant

if ( ! function_exists('total_capitation_dependant'))

{

	function total_capitation_dependant($from = null, $to = null) {

		$CI =&	get_instance();

		$CI->db->from('xin_clients_capitation');

		if (!is_null($from) AND !is_null($to)) {
			$CI->db->where("created_at BETWEEN '$from' AND '$to'");
		}

		$CI->db->where("type = 'd'");

		$query=$CI->db->get();

		if ($query->num_rows() > 0) {

			return $query->num_rows();

		}else{

			return 0;

		}

	}

}





?>