<?php

 /**

 * NOTICE OF LICENSE

 *

 * This source file is subject to the HRSALE License

 * that is bundled with this package in the file license.txt.

 * It is also available through the world-wide-web at this URL:

 * http://www.hrsale.com/license.txt

 * If you did not receive a copy of the license and are unable to

 * obtain it through the world-wide-web, please send an email

 * to hrsalesoft@gmail.com so we can send you a copy immediately.

 *

 * @author   HRSALE

 * @author-email  hrsalesoft@gmail.com

 * @copyright  Copyright © hrsale.com. All Rights Reserved

 */

defined('BASEPATH') OR exit('No direct script access allowed');



class Profile extends MY_Controller {

	

	 public function __construct() {

        parent::__construct();

		//load the model

		$this->load->model("Employees_model");

		$this->load->model("Xin_model");

		$this->load->model("Department_model");

		$this->load->model("Designation_model");

		$this->load->model("Roles_model");

		$this->load->model("Location_model");

		$this->load->model("Clients_model");
		$this->load->model("Training_model");
		$this->load->model("Tickets_model");
		$this->load->model("Custom_fields_model");

		$this->load->library('email');

	}

	

	/*Function to set JSON output*/

	public function output($Return=array()){

		/*Set response header*/

		header("Access-Control-Allow-Origin: *");

		header("Content-Type: application/json; charset=UTF-8");

		/*Final JSON response*/

		exit(json_encode($Return));

	}

	

	public function index() 
	{



		$session = $this->session->userdata('client_username');

		if(empty($session)){ 

			redirect('client/auth/');

		}

		$result = $this->Clients_model->read_client_info($session['client_id']);

		$data = array(

			'name' => $result[0]->name,

			'client_id' => $result[0]->client_id,

			'client_username' => $result[0]->client_username,

			'email' => $result[0]->email,

			'company_name' => $result[0]->company_name,

			'gender' => $result[0]->gender,

			'website_url' => $result[0]->website_url,

			'contact_number' => $result[0]->contact_number,

			'address_1' => $result[0]->address_1,

			'address_2' => $result[0]->address_2,

			'city' => $result[0]->city,

			'state' => $result[0]->state,

			'zipcode' => $result[0]->zipcode,

			'countryid' => $result[0]->country,

			'is_active' => $result[0]->is_active,

			'title' => $this->Xin_model->site_title(),

			'client_profile' => $result[0]->client_profile,

			'last_login_date' => $result[0]->last_login_date,

			'last_login_date' => $result[0]->last_login_date,

			'last_login_ip' => $result[0]->last_login_ip,


			'all_countries' => $this->Xin_model->get_countries(),

			);

		$data['breadcrumbs'] = $this->lang->line('header_my_profile');

		$data['path_url'] = 'profile_client';

		if(!empty($session)){ 

			$data['subview'] = $this->load->view("client/profile/client_profile", $data, TRUE);

			$this->load->view('client/layout/layout_main', $data); //page load

		} else {

			redirect('client/auth/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

	}




	public function dependants_list() 
	{ 

		$session = $this->session->userdata('client_username');

		if(empty($session)){ 

			redirect('client/auth/');

		}

		$result = $this->Clients_model->read_client_info($session['client_id']);

		$client_id  = $session['client_id'];

		$data['breadcrumbs'] = $this->lang->line('header_my_profile');

		$data['client_data'] = $this->Training_model->getAll2('xin_clients_family',' client_id ="'.$client_id.'"   order by clients_family_id desc');

		$data['hr_dependant_list'] = 'active';

		$data['title'] =  $this->Xin_model->site_title();

		$data['path_url'] = 'profile_client';

		if(!empty($session)){ 

			$data['subview'] = $this->load->view("client/profile/dependant_list", $data, TRUE);

			$this->load->view('client/layout/layout_main', $data); //page load

		} else {

			redirect('client/auth/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

	}



	public function edit_dependent_request() 
	{ 

		$session = $this->session->userdata('client_username');

		if(empty($session)){ 

			redirect('client/auth/');

		}

		$client_id     = $session['client_id'];
		$dependent_id  = $_GET['dependent_id'];


		

		$data['client_data']    = $this->Training_model->getAll2('xin_clients_family',' client_id ="'.$client_id.'" and  clients_family_id ="'.$dependent_id.'" ');

		 

		if(empty($data['client_data']))
		{
			redirect('client/profile/change_dependent_list');
		}
 
		$data['client_p']       = $this->Clients_model->read_client_info($client_id);

		$data['all_locations']  =  $this->Training_model->getAll2('xin_location',' 1 order by location_id desc');


		$data['breadcrumbs'] = $this->lang->line('header_my_profile');

		

		$data['change_dependent_list'] = 'active';

		$data['title'] =  $this->Xin_model->site_title();

		$data['path_url'] = 'profile_client';

		if(!empty($session)){ 

			$data['subview'] = $this->load->view("client/profile/edit_dependent", $data, TRUE);

			$this->load->view('client/layout/layout_main', $data); //page load

		} else {

			redirect('client/auth/');

		}
 

	}




	public function change_dependent_list() 
	{ 

		$session = $this->session->userdata('client_username');

		if(empty($session)){ 

			redirect('client/auth/');

		}

		$result = $this->Clients_model->read_client_info($session['client_id']);

		$client_id  = $session['client_id'];

		$data['breadcrumbs'] = $this->lang->line('header_my_profile');

		$data['client_data'] = $this->Training_model->getAll2('xin_clients_family',' client_id ="'.$client_id.'"   order by clients_family_id desc');

		$data['change_dependent_list'] = 'active';

		$data['title'] =  $this->Xin_model->site_title();

		$data['path_url'] = 'profile_client';

		if(!empty($session)){ 

			$data['subview'] = $this->load->view("client/profile/change_dependent_list", $data, TRUE);

			$this->load->view('client/layout/layout_main', $data); //page load

		} else {

			redirect('client/auth/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

	}


	public function edit_dependent()
	{ 

		$session = $this->session->userdata('client_username');

		if(empty($session)){ 

			redirect('client/auth/');

		}

		$result = $this->Clients_model->read_client_info($session['client_id']);

		$client_id  = $session['client_id'];

		$data['breadcrumbs'] = $this->lang->line('header_my_profile');

		$data['client_data'] = $this->Training_model->getAll2('xin_clients_family',' client_id ="'.$client_id.'"   order by clients_family_id desc');

		$data['change_dependent_list'] = 'active';

		$data['title'] =  $this->Xin_model->site_title();

		$data['path_url'] = 'profile_client';

		if(!empty($session)){ 

			$data['subview'] = $this->load->view("client/profile/change_dependent_list", $data, TRUE);

			$this->load->view('client/layout/layout_main', $data); //page load

		} else {

			redirect('client/auth/');

		}

		 

	}

	public function hospital_list() 
	{ 

		$session = $this->session->userdata('client_username');

		if(empty($session)){ 

			redirect('client/auth/');

		}

		if(isset($_POST['id']) and !empty($_POST['id']))
		{
			$hospital_id = $_POST['id'];
			$reason = $_POST['that_reason'];

			$data_to_insert = array(
				'reason'	   => $reason,
				'hospital_id'  => $hospital_id,
				'client_id'    => $session['client_id'],
				'created_on'   => date("Y-m-d h:i:s"),
			);

			 
			 
			$this->Training_model->insertDataTB('xin_change_hospital_request',$data_to_insert);

			$this->session->set_flashdata('success','Hospital change request has been submitted successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
		$data['result'] = $this->Clients_model->read_client_info($session['client_id']);
 
		$data['breadcrumbs'] = $this->lang->line('header_my_profile'); 
		 

		$data['hr_hospital_list'] = 'active';

		$data['title'] =  $this->Xin_model->site_title();

		$data['path_url'] = 'profile_client';

	 

		if(!empty($session)){ 

			$data['subview'] = $this->load->view("client/profile/hospital_list", $data, TRUE);

			$this->load->view('client/layout/layout_main', $data); //page load

		} else {

			redirect('client/auth/');

		}

		 
	}

	public function ticket_list() 
	{ 

		$session = $this->session->userdata('client_username');


		// print_r($session);die;

		if(empty($session)){ 

			redirect('client/auth/');

		}

		$delete = $this->input->post('_method');

		if (isset($delete)) {
			$id = $this->input->post('id');
			$delete = $this->Xin_model->delete_ticket($id);

			// $this->session->set_flashdata('success-ch','Successfully deleted');
			redirect('client/profile/ticket_list');

		}

		// if(isset($_GET['hospital_id']) and !empty($_GET['hospital_id']))
		// {
		// 	$hospital_id = $_GET['hospital_id'];

		// 	$data_to_insert = array(
		// 		'hospital_id'  => $hospital_id,
		// 		'client_id'    => $session['client_id'],
		// 		'created_on'   => date("Y-m-d h:i:s"),
		// 	);

			 
			 
		// 	$this->Training_model->insertDataTB('xin_change_hospital_request',$data_to_insert);

		// 	$this->session->set_flashdata('success','Hospital change request has been submitted successfully.');
		// 	redirect($_SERVER['HTTP_REFERER']);
		// }
		
		$data['result'] = $this->Clients_model->read_client_info($session['client_id']);
 
		$data['breadcrumbs'] = $this->lang->line('header_my_profile'); 
		 

		$data['hr_ticket_list'] = 'active';

		$data['title'] =  $this->Xin_model->site_title();

		$data['path_url'] = 'profile_client';

		$data['all_ticket'] = $this->Xin_model->get_client_ticket($session['client_id']);
		// echo $this->db->last_query();
		// print_r($data['all_ticket']);die;
	 

		if(!empty($session)){ 

			$data['subview'] = $this->load->view("client/profile/ticket_list", $data, TRUE);

			$this->load->view('client/layout/layout_main', $data); //page load

		} else {

			redirect('client/auth/');

		}

		 
	}

	public function add_ticket() 
	{

		$session = $this->session->userdata('client_username');

		// if($this->input->post('add_type')=='ticket') {		

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$Return['csrf_hash'] = $this->security->get_csrf_hash();

			

		/* Server side PHP input validation */		

		if($this->input->post('subject')==='') {

       		 $Return['error'] = $this->lang->line('xin_employee_error_subject');

		} 

		$description = $this->input->post('description');

		$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);		

		if($Return['error']!=''){

       		$this->output($Return);

    	}

		

		$ticket_code = $this->Xin_model->generate_random_string();

		$module_attributes = $this->Custom_fields_model->tickets_hrsale_module_attributes();

		$count_module_attributes = $this->Custom_fields_model->count_tickets_module_attributes();	

		$i=1;

		if($count_module_attributes > 0){

			 foreach($module_attributes as $mattribute) {

				 if($mattribute->validation == 1){

					 if($i!=1) {

					 } else if($this->input->post($mattribute->attribute)=='') {

						$Return['error'] = $this->lang->line('xin_hrsale_custom_field_the').' '.$mattribute->attribute_label.' '.$this->lang->line('xin_hrsale_custom_field_is_required');

					}

				 }

			 }		

			 if($Return['error']!=''){

				$this->output($Return);

			}	

		}

		$data = array(

		'ticket_code' => $ticket_code,

		'subject' => $this->input->post('subject'),

		'company_id' => null,

		'employee_id' => $session['client_id'],

		'description' => $qt_description,

		'ticket_status' => '1',

		'is_notify' => '1',

		'ticket_priority' => null,

		'created_at' => date('d-m-Y h:i:s'),

		'is_hc' => 1

		);

		$iresult = $this->Tickets_model->add($data);

		if ($iresult) {

			$Return['result'] = $this->lang->line('xin_success_ticket_created');

			$id = $iresult;

			if($count_module_attributes > 0){

				foreach($module_attributes as $mattribute) {

				 	/*$attr_data = array(

						'user_id' => $iresult,

						'module_attributes_id' => $mattribute->custom_field_id,

						'attribute_value' => $this->input->post($mattribute->attribute),

						'created_at' => date('Y-m-d h:i:s')

					);

					$this->Custom_fields_model->add_values($attr_data);*/

					if($mattribute->attribute_type == 'fileupload'){

						if($_FILES[$mattribute->attribute]['size'] != 0) {

							if(is_uploaded_file($_FILES[$mattribute->attribute]['tmp_name'])) {

							//checking image type

								$allowed =  array('png','jpg','jpeg','pdf','gif','xls','doc','xlsx','docx');

								$filename = $_FILES[$mattribute->attribute]['name'];

								$ext = pathinfo($filename, PATHINFO_EXTENSION);

								

								if(in_array($ext,$allowed)){

									$tmp_name = $_FILES[$mattribute->attribute]["tmp_name"];

									$profile = "uploads/custom_files/";

									$set_img = base_url()."uploads/custom_files/";

									// basename() may prevent filesystem traversal attacks;

									// further validation/sanitation of the filename may be appropriate

									$name = basename($_FILES[$mattribute->attribute]["name"]);

									$newfilename = 'custom_file_'.round(microtime(true)).'.'.$ext;

									move_uploaded_file($tmp_name, $profile.$newfilename);

									$fname = $newfilename;	

								}

								$iattr_data = array(

									'user_id' => $id,

									'module_attributes_id' => $mattribute->custom_field_id,

									'attribute_value' => $fname,

									'created_at' => date('Y-m-d h:i:s')

								);

								$this->Custom_fields_model->add_values($iattr_data);

							}

						} else {

							$iattr_data = array(

									'user_id' => $id,

									'module_attributes_id' => $mattribute->custom_field_id,

									'attribute_value' => '',

									'created_at' => date('Y-m-d h:i:s')

								);

								$this->Custom_fields_model->add_values($iattr_data);

						}

					} else if($mattribute->attribute_type == 'multiselect') {

						$multisel_val = $this->input->post($mattribute->attribute);

						if(!empty($multisel_val)){

							$newdata = implode(',', $this->input->post($mattribute->attribute));

							$iattr_data = array(

								'user_id' => $id,

								'module_attributes_id' => $mattribute->custom_field_id,

								'attribute_value' => $newdata,

								'created_at' => date('Y-m-d h:i:s')

							);

							$this->Custom_fields_model->add_values($iattr_data);

						}

					} else {

							if($this->input->post($mattribute->attribute) == ''){

								$file_val = '';

							} else {

								$file_val = $this->input->post($mattribute->attribute);

							}

							$iattr_data = array(

								'user_id' => $id,

								'module_attributes_id' => $mattribute->custom_field_id,

								'attribute_value' => $file_val,

								'created_at' => date('Y-m-d h:i:s')

							);

						$this->Custom_fields_model->add_values($iattr_data);

					}

					/*$attr_orig_value = $this->Custom_fields_model->read_hrsale_module_attributes_values($result,$mattribute->custom_field_id);

					if($attr_orig_value->module_attributes_id != $mattribute->custom_field_id) {

						$this->Custom_fields_model->add_values($attr_data);

					}*/

				 }

			}

			//get setting info 

			$setting = $this->Xin_model->read_setting_info(1);

			if($setting[0]->enable_email_notification == 'yes') {



			// 	$this->email->set_mailtype("html");

			// 	//get company info

			// 	$cinfo = $this->Xin_model->read_company_setting_info(1);

			// 	//get email template

			// 	$template = $this->Xin_model->read_email_template(15);

			// 	//get employee info

			// 	$user_info = $this->Xin_model->read_user_info($this->input->post('employee_id'));

				

			// 	$full_name = $user_info[0]->first_name.' '.$user_info[0]->last_name;

						

			// 	$subject = str_replace('{var ticket_code}',$ticket_code,$template[0]->subject);

			// 	$logo = base_url().'uploads/logo/signin/'.$cinfo[0]->sign_in_logo;

				

			// 	$message = '

			// <div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">

			// <img src="'.$logo.'" title="'.$cinfo[0]->company_name.'"><br>'.str_replace(array("{var site_name}","{var site_url}","{var ticket_code}"),array($cinfo[0]->company_name,site_url(),$ticket_code),htmlspecialchars_decode(stripslashes($template[0]->message))).'</div>';

				

			// 	$this->email->from($user_info[0]->email, $full_name);

			// 	$this->email->to($cinfo[0]->email);

				

			// 	$this->email->subject($subject);

			// 	$this->email->message($message);

				

			// 	$this->email->send();

			}		

		} else {

			$Return['error'] = $this->lang->line('xin_error_msg');

		}

		$this->output($Return);

		exit;

	}

	// }

	 	

	// Validate and update info in database

	public function update() {

	

		// if($this->input->post('_method')=='EDIT') 
		// {

			$id = $this->uri->segment(4);

			// Check validation for user input

			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');

			// $this->form_validation->set_rules('website', 'Website', 'trim|required|xss_clean');

			// $this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean');

			

			$name = $this->input->post('name');

			// $company_name = $this->input->post('company_name');

			$email = $this->input->post('email');

			$contact_number = $this->input->post('contact_number');

			// $website = $this->input->post('website');

			$address_1 = $this->input->post('address_1');

			// $address_2 = $this->input->post('address_2');

			// $city = $this->input->post('city');

			$state = $this->input->post('location');

			// $zipcode = $this->input->post('zipcode');

			// $country = $this->input->post('country');
			$marital_status =  $this->input->post('marital_status');
			$dob            =  $this->input->post('dob');
			$last_name      =  $this->input->post('last_name');
			$other_name     =  $this->input->post('other_name');
			$sex            =  $this->input->post('sex');

			
			$sex            =  $this->input->post('sex');

			$diseases_data  =  '';
			if(!empty($this->input->post('diseases')))
			{
				$diseases_data = implode(',', $this->input->post('diseases'));
			}

			$comment = '';
			if(isset($_POST['comment']))
			{
				$comment = $this->input->post('comment');
			}

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			 

			/* Server side PHP input validation */

			if($name==='') {

				$Return['error'] = $this->lang->line('xin_error_name_field');

			}  else if($contact_number==='') {

				$Return['error'] = $this->lang->line('xin_error_contact_field');

			} else if($email==='') {

				$Return['error'] = $this->lang->line('xin_error_cemail_field');

			}  else if($this->input->post('username')==='') {

				$Return['error'] = $this->lang->line('xin_employee_error_username');

			}else if($this->input->post('address_1')==='') {

				$Return['error'] = 'Resident address is required.';

			}else if($this->input->post('state')==='') {

				$Return['error'] = 'State is required.';

			}else if($this->input->post('marital_status')==='') {

				$Return['error'] = 'Marital status is required.'; 

			}else if($this->input->post('dob')==='') {

				$Return['error'] = 'Birthday is required.';

			}else if($this->input->post('hospital_id')==='') {

				$Return['error'] = 'Hospital is required.';

			}else if($this->input->post('last_name')==='') {

				$Return['error'] = 'Last name is required.';

			} else if($this->input->post('sex')==='') {

				$Return['error'] = 'Sex is required.';

			} 



			/* Check if file uploaded..*/

			else if($_FILES['client_photo']['size'] == 0) {

				 $fname = 'no file';

				 $no_logo_data = array(

					'name' => $this->input->post('name'), 

					'email' => $this->input->post('email'),

					'contact_number' => $this->input->post('contact_number'),

					'sex' => $this->input->post('sex'),

					'address_1'      => $this->input->post('address_1'),

					'marital_status'  => $this->input->post('marital_status'), 

					'state'           => $this->input->post('location'),

					'dob'             => $this->input->post('dob'),

					'last_name'       => $this->input->post('last_name'),

					'other_name'      => $this->input->post('other_name'), 
					'hospital_id'      => $this->input->post('hospital_id'), 

					'diseases'        => $diseases_data, 
					'disease_comment' => $comment, 
				);

			$result = $this->Clients_model->update_record($no_logo_data,$id);
			// $result = TRUE;

			} else {

				if(is_uploaded_file($_FILES['client_photo']['tmp_name'])) {

				//checking image type

				$allowed =  array('png','jpg','jpeg','gif');

				$filename = $_FILES['client_photo']['name'];

				$ext = pathinfo($filename, PATHINFO_EXTENSION);

				

				if(in_array($ext,$allowed)){

					$tmp_name = $_FILES["client_photo"]["tmp_name"];

					$bill_copy = "uploads/clients/";

					// basename() may prevent filesystem traversal attacks;

					// further validation/sanitation of the filename may be appropriate

					$lname = basename($_FILES["client_photo"]["name"]);

					$newfilename = 'client_photo_'.round(microtime(true)).'.'.$ext;

					move_uploaded_file($tmp_name, $bill_copy.$newfilename);

					$fname = $newfilename;

					$data = array( 

						'client_profile' => $fname,

						'name' => $this->input->post('name'), 

						'email' => $this->input->post('email'),

						'contact_number' => $this->input->post('contact_number'),

						'sex' => $this->input->post('sex'),

						'address_1' => $this->input->post('address_1'),

						'marital_status' => $this->input->post('marital_status'), 

						'state' => $this->input->post('location'),

						'dob' => $this->input->post('dob'),

						'last_name' => $this->input->post('last_name'),
						
						'other_name' => $this->input->post('other_name'), 	
						'hospital_id' => $this->input->post('hospital_id'), 	
						'diseases'        => $diseases_data, 
						'disease_comment' => $comment, 

					);

					// update record > model

					$result = $this->Clients_model->update_record($data,$id);
					// $result = TRUE;

				} else {

					$Return['error'] = $this->lang->line('xin_error_attatchment_type');

				}

			}

		}

	

			if($Return['error']!=''){
				// $this->session->set_flashdata('error',$Return['error']);
				// redirect($_SERVER['HTTP_REFERER']);
	       		$this->output($Return); 

	    	}		

				

			if ($result == TRUE) {

				$Return['result'] = $this->lang->line('xin_client_profile_update');

				// $this->session->set_flashdata('success',$this->lang->line('xin_client_profile_update'));
				// redirect($_SERVER['HTTP_REFERER']);



			} else {
				// $this->session->set_flashdata('error',$this->lang->line('xin_error_msg'));
				// redirect($_SERVER['HTTP_REFERER']);

				$Return['error'] = $Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		// }

	}

	// Validate and update info in database // change password

	public function change_password() 
	{ 
		if($this->input->post('type')=='change_password') 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$session = $this->session->userdata('client_username');

			

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

				

			/* Server side PHP input validation */						

			if(trim($this->input->post('new_password'))==='') {

	       		 $Return['error'] = $this->lang->line('xin_employee_error_newpassword');

			} else if(strlen($this->input->post('new_password')) < 6) {

				$Return['error'] = $this->lang->line('xin_employee_error_password_least');

			} else if(trim($this->input->post('new_password_confirm'))==='') {

				 $Return['error'] = $this->lang->line('xin_employee_error_new_cpassword');

			} else if($this->input->post('new_password')!=$this->input->post('new_password_confirm')) {

				 $Return['error'] = $this->lang->line('xin_employee_error_old_new_cpassword');

			}

				

			if($Return['error']!=''){

	       		$this->output($Return);

	    	}

			$options = array('cost' => 12);

			$password_hash = password_hash($this->input->post('new_password'), PASSWORD_BCRYPT, $options);

		

			$data = array(

				'client_password' => $password_hash

			);

			$id = $session['client_id'];

			$result = $this->Clients_model->update_record($data,$id);

			if ($result == TRUE) {

				$Return['result'] = $this->lang->line('xin_client_password_update');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}


	public function profile_detail()
	{ 
		$session = $this->session->userdata('client_username');

		if(empty($session)){ 

			redirect('client/auth/');

		}

		// echo "<pre>";
		// print_r($data['all_locations']);
		// die();
		$result = $this->Clients_model->read_client_info($session['client_id']);

		$organ_id = $result[0]->company_name;
		$company_data  =  $this->Training_model->getAll2('xin_organization',' id = "'.$organ_id.'" ');

		if(isset($company_data[0]->name))
		{
			$company_name = $company_data[0]->name;
		}else{
			$company_name = '--';
		}


		$data = array(

			'name'            => $result[0]->name,

			'client_id'       => $result[0]->client_id,

			'client_username' => $result[0]->client_username,

			'email'        => $result[0]->email,

			'company_name' => $company_name,

			'gender'       => $result[0]->gender,

			'website_url'    => $result[0]->website_url,

			'contact_number' => $result[0]->contact_number,

			'address_1'  => $result[0]->address_1,

			'address_2'  => $result[0]->address_2,

			'city'       => $result[0]->city,

			'state'      => $result[0]->state,

			'zipcode'    => $result[0]->zipcode,

			'countryid'  => $result[0]->country,

			'is_active'  => $result[0]->is_active,

			'title'      => $this->Xin_model->site_title(),

			'client_profile'  => $result[0]->client_profile,

			'last_login_date' => $result[0]->last_login_date,

			'last_login_date' => $result[0]->last_login_date,

			'last_login_ip'   => $result[0]->last_login_ip,
 
			'marital_status'  => $result[0]->marital_status,

			'dob'             => $result[0]->dob,

			'last_name'       => $result[0]->last_name,

			'other_name'      => $result[0]->other_name,

			'sex'             => $result[0]->sex,

			'last_login_ip'   => $result[0]->last_login_ip,

			'ind_family'      => $result[0]->ind_family,

			'subscriptions'   => $result[0]->subscription_ids,

			'hospital_id'     => $result[0]->hospital_id,

			'all_countries'   => $this->Xin_model->get_countries(),

			'all_locations'   =>  $this->Training_model->getAll2('xin_location',' 1 order by location_id desc'),
		);

		// print_r($data['subscriptions']);die;
		$data['hr_profile_detail'] = 'active';
		$data['path_url'] = 'profile_client';

		$data['subview']  = $this->load->view("client/profile/profile_detail", $data, TRUE);

		$this->load->view('client/layout/layout_main', $data); //page load

	}






	public function add_dependant()
	{ 
		$session = $this->session->userdata('client_username');

		if(empty($session)){ 

			redirect('client/auth/');

		}


		$result = $this->Clients_model->read_client_info($session['client_id']);
		
		$data['all_locations']  =  $this->Training_model->getAll2('xin_location',' 1 order by location_id desc'); 

		$data['subscriptions']  =  $result[0]->subscription_ids; 

		$data['hr_profile_detail'] = 'active';

		$data['path_url'] = 'profile_client';

		$data['title']             =  $this->Xin_model->site_title();

		$data['subview']  = $this->load->view("client/profile/add_dependent", $data, TRUE);

		$this->load->view('client/layout/layout_main', $data); //page load

	}


	public function save_dependent()
	{
		if($this->input->post('_method')=='save') 
		{

			$session = $this->session->userdata('client_username');

			if(empty($session)){ 

				redirect('client/auth/');

			} 

			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');

			 
			$contact_number = $this->input->post('contact_number'); 

			$name     = $this->input->post('name');  
			$location = $this->input->post('location');

			$address_1 = $this->input->post('address_1'); 
			
 
			$relation       =  $this->input->post('relation');
			$dob            =  $this->input->post('dob');
			$last_name      =  $this->input->post('last_name');
			$other_name     =  $this->input->post('other_name');
			$sex            =  $this->input->post('sex');
			$relation       =  $this->input->post('relation');

			$from = new DateTime($dob);
			$to   = new DateTime('today');
			$age  = $from->diff($to)->y;


			
			 
			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

				

			/* Server side PHP input validation */
			if( ( $age == 21 or $age > 21)  and $relation  != 'wife' )
			{
				$Return['error'] = 'WE ARE SORRY YOU CAN NOT ADD THIS DEPENDANT AS HE/SHE HAS EXCEEDED AGE LIMITS AS A DEPENDANT KINDLY APPLY HERE TO GET AN INDIVIDUAL PLAN FOR HIM/HER';

			}else if($name==='') {

				$Return['error'] = $this->lang->line('xin_error_name_field');

			}  else if($contact_number==='') {

				$Return['error'] = $this->lang->line('xin_error_contact_field');

			}  

			// else if($this->input->post('address_1')==='') {

			// 	$Return['error'] = 'Resident address is required.';

			// }
			// else if($this->input->post('state')==='') {

			// 	$Return['error'] = 'State is required.';

			// }

			else if($this->input->post('relation')==='') {

				$Return['error'] = 'Relation status is required.'; 

			}else if($this->input->post('dob')==='') {

				$Return['error'] = 'Birthday is required.';

			}else if($this->input->post('hospital_id')==='') {

				$Return['error'] = 'Hospital is required.';

			}else if($this->input->post('last_name')==='') {

				$Return['error'] = 'Last name is required.';

			} else if($this->input->post('sex')==='') {

				$Return['error'] = 'Sex is required.';

			}else if($_FILES['client_photo']['size'] == 0) {

				$Return['error'] = 'Image is required';

			} else 
			{

				if(is_uploaded_file($_FILES['client_photo']['tmp_name'])) 
				{

					//checking image type 
					$allowed  =  array('png','jpg','jpeg','gif');

					$filename =  $_FILES['client_photo']['name'];

					$ext      =  pathinfo($filename, PATHINFO_EXTENSION);

					

					if(in_array($ext,$allowed))
					{

						$tmp_name = $_FILES["client_photo"]["tmp_name"];

						$bill_copy = "uploads/clients/";

						// basename() may prevent filesystem traversal attacks;

						// further validation/sanitation of the filename may be appropriate

						$lname = basename($_FILES["client_photo"]["name"]);

						$newfilename = 'client_photo_'.round(microtime(true)).'.'.$ext;

						move_uploaded_file($tmp_name, $bill_copy.$newfilename);

						$fname = $newfilename; 
					 

						$data = array(

							'client_profile' => $fname,

							'name'           => $this->input->post('name'),  

							'contact_number' => $this->input->post('contact_number'),

							'sex'            => $this->input->post('sex'),

							// 'address_1'      => $this->input->post('address_1'),

							'relation'       => $this->input->post('relation'), 

							// 'location'       => $this->input->post('location'),

							'dob'            => $this->input->post('dob'),

							'last_name'      => $this->input->post('last_name'),

							'other_name'     => $this->input->post('other_name'), 

							'hospital_id'     => $this->input->post('hospital_id'),  

							'created_on'     => Date('Y-m-d h:i:s'), 

							'client_id'      => $session['client_id'], 
						);

						// update record > model

						$result = $this->Clients_model->add_dependant($data);

					} else {

						$Return['error'] = $this->lang->line('xin_error_attatchment_type');

					}

				}

			}


			// /* Check if file uploaded..*/

			// else if($_FILES['client_photo']['size'] == 0) 
			// {

			// 	$fname = 'no file';

			// 	$no_logo_data = array(

			// 		'name'           => $this->input->post('name'),  

			// 		'contact_number' => $this->input->post('contact_number'),

			// 		'sex'            => $this->input->post('sex'),

			// 		'address_1'      => $this->input->post('address_1'),

			// 		'relation'       => $this->input->post('relation'), 

			// 		'location'       => $this->input->post('location'),

			// 		'dob'            => $this->input->post('dob'),

			// 		'last_name'      => $this->input->post('last_name'),

			// 		'other_name'     => $this->input->post('other_name'), 

			// 		'hospital_id'    => '', 

			// 		'created_on'     => Date('Y-m-d h:i:s'), 

			// 		'client_id'      => $session['client_id'], 
			// 	);

			// 	$result = $this->Clients_model->add_dependant($no_logo_data);

			// }
	

			if($Return['error']!='')
			{
				$this->session->set_flashdata('error',$Return['error']);
				redirect($_SERVER['HTTP_REFERER']);

	       		$this->output($Return); 

	    	}		

				

			if ($result == TRUE) 
			{

				$Return['result'] = $this->lang->line('xin_client_profile_update');

				$this->session->set_flashdata('success',$this->lang->line('xin_client_profile_update'));
				redirect($_SERVER['HTTP_REFERER']);



			} else 
			{
				$this->session->set_flashdata('error',$this->lang->line('xin_error_msg'));
				redirect($_SERVER['HTTP_REFERER']);

				$Return['error'] = $Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}
	}




	public function save_req_dependent()
	{
		if($this->input->post('name') ) 
		{

			$session = $this->session->userdata('client_username');

			if(empty($session)){ 

				redirect('client/auth/');

			} 

			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');

			 
			$contact_number = $this->input->post('contact_number'); 

			$name     = $this->input->post('name');  
			$location = $this->input->post('location');

			$address_1 = $this->input->post('address_1'); 
			
 
			$relation       =  $this->input->post('relation');
			$dob            =  $this->input->post('dob');
			$last_name      =  $this->input->post('last_name');
			$other_name     =  $this->input->post('other_name');
			$sex            =  $this->input->post('sex');
			$relation       =  $this->input->post('relation');

			$from = new DateTime($dob);
			$to   = new DateTime('today');
			$age  = $from->diff($to)->y;


			
			 

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

				

			/* Server side PHP input validation */
			if( ( $age == 21 or $age > 21)  and $relation  != 'wife' )
			{
				$Return['error'] = 'WE ARE SORRY YOU CAN NOT ADD THIS DEPENDANT AS HE/SHE HAS EXCEEDED AGE LIMITS AS A DEPENDANT KINDLY APPLY HERE TO GET AN INDIVIDUAL PLAN FOR HIM/HER';

			}else if($name==='') {

				$Return['error'] = $this->lang->line('xin_error_name_field');

			}  else if($contact_number==='') {

				$Return['error'] = $this->lang->line('xin_error_contact_field');

			} else if($this->input->post('relation')==='') {

				$Return['error'] = 'Relation status is required.'; 

			}else if($this->input->post('dob')==='') {

				$Return['error'] = 'Birthday is required.';

			}else if($this->input->post('hospital_id')==='') {

				$Return['error'] = 'Hospital is required.';

			}else if($this->input->post('last_name')==='') {

				$Return['error'] = 'Last name is required.';

			} else if($this->input->post('sex')==='') {

				$Return['error'] = 'Sex is required.';

			} 

			else if($_FILES['client_photo']['size'] == 0) 
			{

				$fname = 'no file';

				$no_logo_data = array( 
					
					'name'           => $this->input->post('name'),  

					'requester_id'   => $this->input->post('requester_id'),  

					'contact_number' => $this->input->post('contact_number'),

					'sex'            => $this->input->post('sex'),

					 
					'relation'       => $this->input->post('relation'), 

					 

					'dob'            => $this->input->post('dob'),

					'last_name'      => $this->input->post('last_name'),

					'other_name'     => $this->input->post('other_name'), 

					'hospital_id'     => $this->input->post('hospital_id'),  

					'created_on'     => Date('Y-m-d h:i:s'), 

					'client_id'      => $session['client_id'], 
				);

				$result = $this->Clients_model->add_dependant2($no_logo_data);

			} else   
			{

				if(is_uploaded_file($_FILES['client_photo']['tmp_name'])) 
				{

					//checking image type 
					$allowed  =  array('png','jpg','jpeg','gif');

					$filename =  $_FILES['client_photo']['name'];

					$ext      =  pathinfo($filename, PATHINFO_EXTENSION);

					

					if(in_array($ext,$allowed))
					{

						$tmp_name = $_FILES["client_photo"]["tmp_name"];

						$bill_copy = "uploads/clients/";

						// basename() may prevent filesystem traversal attacks;

						// further validation/sanitation of the filename may be appropriate

						$lname = basename($_FILES["client_photo"]["name"]);

						$newfilename = 'client_photo_'.round(microtime(true)).'.'.$ext;

						move_uploaded_file($tmp_name, $bill_copy.$newfilename);

						$fname = $newfilename; 
					 

						$data = array(

							'client_profile' => $fname,

							'name'           => $this->input->post('name'),

							'requester_id'   => $this->input->post('requester_id'),  

							'contact_number' => $this->input->post('contact_number'),

							'sex'            => $this->input->post('sex'),

							 

							'relation'       => $this->input->post('relation'), 

						 
							'dob'            => $this->input->post('dob'),

							'last_name'      => $this->input->post('last_name'),

							'other_name'     => $this->input->post('other_name'), 

							'hospital_id'     => $this->input->post('hospital_id'),  

							'created_on'     => Date('Y-m-d h:i:s'), 

							'client_id'      => $session['client_id'], 
						);

						// update record > model

						$result = $this->Clients_model->add_dependant2($data);

					} else {

						$Return['error'] = $this->lang->line('xin_error_attatchment_type');

					}

				}

			}


			// /* Check if file uploaded..*/


	

			if($Return['error']!='')
			{
				$this->session->set_flashdata('error',$Return['error']);
				redirect($_SERVER['HTTP_REFERER']);

	       		$this->output($Return); 

	    	}		

				

			if ($result == TRUE) 
			{

				$Return['result'] = 'Update Request has been send successfully.';

				$this->session->set_flashdata('success','Update request has been send successfully.');
				redirect($_SERVER['HTTP_REFERER']);



			} else 
			{
				$this->session->set_flashdata('error',$this->lang->line('xin_error_msg'));
				redirect($_SERVER['HTTP_REFERER']);

				$Return['error'] = $Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}
	}


}