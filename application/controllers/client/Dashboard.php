<?php

 /**

 * NOTICE OF LICENSE

 *

 * This source file is subject to the HRSALE License

 * that is bundled with this package in the file license.txt.

 * It is also available through the world-wide-web at this URL:

 * http://www.hrsale.com/license.txt

 * If you did not receive a copy of the license and are unable to

 * obtain it through the world-wide-web, please send an email

 * to hrsalesoft@gmail.com so we can send you a copy immediately.

 *

 * @author   HRSALE

 * @author-email  hrsalesoft@gmail.com

 * @copyright  Copyright © hrsale.com. All Rights Reserved

 */

defined('BASEPATH') OR exit('No direct script access allowed');



class Dashboard extends MY_Controller {

	

	public function __construct()

     {

          parent::__construct();

          //load the models

          $this->load->model('Login_model');

		  $this->load->model('Designation_model');

		  $this->load->model('Department_model');

		  $this->load->model('Employees_model');

		  $this->load->model('Xin_model');

		  $this->load->model('Exin_model');

		  $this->load->model('Expense_model');

		  $this->load->model('Timesheet_model');

		  $this->load->model('Travel_model');

		  $this->load->model('Training_model');

		  $this->load->model('Project_model');

		  $this->load->model('Job_post_model');

		  $this->load->model('Goal_tracking_model');

		  $this->load->model('Events_model');

		  $this->load->model('Meetings_model');

		  $this->load->model('Announcement_model');

		  $this->load->model('Clients_model');

		  $this->load->model('Invoices_model');

     }

	

	/*Function to set JSON output*/

	public function output($Return=array()){

		/*Set response header*/

		header("Access-Control-Allow-Origin: *");

		header("Content-Type: application/json; charset=UTF-8");

		/*Final JSON response*/

		exit(json_encode($Return));

	} 

	

	public function index()

	{

		$session = $this->session->userdata('client_username');

		if(empty($session)){ 

			redirect('client/auth/');

		}

		

		$clientinfo = $this->Clients_model->read_client_info($session['client_id']);

		$data = array(

			'title' => $this->Xin_model->site_title(),

			'path_url' => 'dashboard',

			'client_name' => $clientinfo[0]->name,

			);

		$client_id  = $session['client_id'];

		$dependents = $this->Training_model->getAll2('xin_clients_family',' client_id ="'.$client_id.'"  ');


		$total_dependent = 0;
		if(!empty($dependents))
		{
			$total_dependent = count($dependents);
		}
		$data['total_dependent'] = $total_dependent;




		$subs_pkg      = '';

		$total_hospital = 0; 
		if(isset($clientinfo[0]->subscription_ids) and !empty($clientinfo[0]->subscription_ids) )
		{


			$subs = $this->Training_model->getAll2('xin_subscription',' subscription_id = "'.$clientinfo[0]->subscription_ids.'" ');

			if (isset($subs[0]->band_types) AND !empty($subs[0]->band_types)) 
			{
 				$subs_pkg = $subs[0]->plan_name;

			    $bands  = explode(',', $subs[0]->band_types);

			    foreach ($bands as $key2 => $band) 
			    {
			        $hos_data = $this->Training_model->getAll2('xin_hospital',' band_id = "'.$band.'" '); 

			        foreach($hos_data as $hos){

						if(isset($hos->hospital_name))
						{

							$total_hospital++;
						}
			        }
			    }
			} 
		}
		
		

		$data['total_hospital']  = $total_hospital;

		$data['subs_pkg']  = $subs_pkg;
		
		// $data['benefits'] = $benefits;
		// print_r($benefits);die;


		 
		$change_hospital  =  $this->Training_model->getAll2('xin_change_hospital_request',' client_id ="'.$client_id.'"  ');


		$total_hospital_req = 0;
		if(!empty($change_hospital))
		{
			$total_hospital_req = count($change_hospital);
		}

		$data['total_hospital_req'] = $total_hospital_req;




		$change_dependent_req  =  $this->Training_model->getAll2('xin_clients_family_req',' client_id ="'.$client_id.'"  ');


		$total_dependent_req = 0;
		if(!empty($change_dependent_req))
		{
			$total_dependent_req = count($change_dependent_req);
		}

		$data['total_dependent_req'] = $total_dependent_req;







		$data['subview'] = $this->load->view('client/dashboard/index', $data, TRUE);
		// echo "<pre>";
		// echo "string";
		// die();
		$this->load->view('client/layout/layout_main', $data); //page load

	}

	

	// set new language

	public function set_language($language = "") {

        

        $language = ($language != "") ? $language : "english";

        $this->session->set_userdata('site_lang', $language);

        redirect($_SERVER['HTTP_REFERER']);

        

    }

}

