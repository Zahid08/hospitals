<?php

 /**

 * NOTICE OF LICENSE

 *

 * This source file is subject to the HRSALE License

 * that is bundled with this package in the file license.txt.

 * It is also available through the world-wide-web at this URL:

 * http://www.hrsale.com/license.txt

 * If you did not receive a copy of the license and are unable to

 * obtain it through the world-wide-web, please send an email

 * to hrsalesoft@gmail.com so we can send you a copy immediately.

 *

 * @author   HRSALE

 * @author-email  hrsalesoft@gmail.com

 * @copyright  Copyright © hrsale.com. All Rights Reserved

 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Invoices extends MY_Controller

{



   /*Function to set JSON output*/

	public function output($Return=array()){

		/*Set response header*/

		header("Access-Control-Allow-Origin: *");

		header("Content-Type: application/json; charset=UTF-8");

		/*Final JSON response*/

		exit(json_encode($Return));

	}

	

	public function __construct()

     {

          parent::__construct();

          //load the login model

          $this->load->model('Company_model');

		  $this->load->model('Xin_model');

		  $this->load->model("Project_model");

		  $this->load->model("Tax_model");

		  $this->load->model("Invoices_model");
		  	
		  $this->load->model("Finance_model");

		  $this->load->model("Clients_model");

		  $this->load->model("Training_model");

     }

	 

	// invoices page

	public function index() {

	

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		// print_r($session);die;

		$data['title'] = $this->lang->line('xin_invoices_title').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_invoices_title');

		$data['all_projects'] = $this->Project_model->get_projects();

		$data['all_taxes'] = $this->Tax_model->get_all_taxes();

		$data['path_url'] = 'hrsale_invoices';

		$data['all_organizations']   =  $this->Training_model->getAll2('xin_organization',' 1 order by id asc');

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('121',$role_resources_ids)) {

			$data['subview'] = $this->load->view("admin/invoices/invoices_list", $data, TRUE);

			$this->load->view('admin/layout/layout_main', $data); //page load

		} else {

			redirect('admin/dashboard');

		}

	}

	// invoices page

	public function all_invoices_paid() {

	

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->lang->line('xin_invoices_title').' Paid | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_invoices_title'). ' Paid';

		$data['all_projects'] = $this->Project_model->get_projects();

		$data['all_taxes'] = $this->Tax_model->get_all_taxes();

		$data['path_url'] = 'hrsale_invoices2';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('121',$role_resources_ids)) {

			$data['subview'] = $this->load->view("admin/invoices/invoices_paid_list", $data, TRUE);

			$this->load->view('admin/layout/layout_main', $data); //page load

		} else {

			redirect('admin/dashboard');

		}

	}

	// invoices page

	public function all_invoices_unpaid() {

	

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->lang->line('xin_invoices_title').' Unpaid | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_invoices_title'). ' Unpaid';

		$data['all_projects'] = $this->Project_model->get_projects();

		$data['all_taxes'] = $this->Tax_model->get_all_taxes();

		$data['path_url'] = 'hrsale_invoices3';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('121',$role_resources_ids)) {

			$data['subview'] = $this->load->view("admin/invoices/invoices_unpaid_list", $data, TRUE);

			$this->load->view('admin/layout/layout_main', $data); //page load

		} else {

			redirect('admin/dashboard');

		}

	}

	public function get_all_clients_of_organization() {

		$oid = $this->input->post('id');
		// echo "org id is: ".$oid;

		$all_clients = $this->Clients_model->all_clients_of_individual_organization($oid);

		// echo $from." ".$to."<br />";
		// print_r($all_clients);
		// die;
		echo '<option value="">Select Client</option>';
		if(!empty($all_clients)) {
			foreach ($all_clients as $client) {
				echo '<option value="'.$client->client_id.'">'.$client->name.'</option>';
			}
		}


	}

	public function get_client_subscription_amount() {

		$cid = $this->input->post('id');
		// echo "org id is: ".$oid;

		$amount = $this->Clients_model->get_client_subscription_amount($cid);

		// echo $from." ".$to."<br />";
		// print_r($all_clients);
		// die;
		if(!empty($amount)) {
			echo $amount->plan_cost;
			// foreach ($all_clients as $client) {
			// 	echo '<option value="'.$client->client_id.'">'.$client->name.'</option>';
			// }
		}


	}

	// invoice payments page

	public function payments_history() {

	

		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$data['title'] = $this->lang->line('xin_acc_invoice_payments').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_acc_invoice_payments');

		$data['path_url'] = 'xin_invoice_payment';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('121',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/invoices/invoice_payment_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		}

	}

	// create invoice page

	public function create() {

	

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->lang->line('xin_invoice_create').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_invoice_create');

		$data['all_projects'] = $this->Project_model->get_projects();

		$data['all_taxes'] = $this->Tax_model->get_all_taxes();

		$data['path_url'] = 'create_hrsale_invoice';

		$data['all_organizations']  =  $this->Training_model->getAll2('xin_organization',' 1 order by id asc');

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('120',$role_resources_ids)) {

			$data['subview'] = $this->load->view("admin/invoices/create_invoice", $data, TRUE);

			$this->load->view('admin/layout/layout_main', $data); //page load

		} else {

			redirect('admin/dashboard');

		}

	}

	

	public function taxes()

     {

		$data['title'] = $this->lang->line('xin_invoice_tax_types').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_invoice_tax_types');

		$data['path_url'] = 'invoice_taxes';

		$session = $this->session->userdata('username');

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('122',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/invoices/invoice_taxes", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

     }

	 // invoice payment list

	public function invoice_payment_list()

     {



		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("client/invoices/invoice_payment_list", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		

		$role_resources_ids = $this->Xin_model->user_role_resource();

		$user_info = $this->Xin_model->read_user_info($session['user_id']);

		$transaction = $this->Invoices_model->get_client_invoice_payments_all();

		

		$data = array();

		$balance2 = 0;

          foreach($transaction->result() as $r) {

			  

			// transaction date

			$transaction_date = $this->Xin_model->set_date_format($r->transaction_date);

			// get currency

			$total_amount = $this->Xin_model->currency_sign($r->amount);

			// credit

			$cr_dr = $r->dr_cr=="dr" ? "Debit" : "Credit";

			

			$invoice_info = $this->Invoices_model->read_invoice_info($r->invoice_id);

			if(!is_null($invoice_info)){

				$inv_no = $invoice_info[0]->invoice_number;

			} else {

				$inv_no = '--';	

			}

			// payment method 

			$payment_method = $this->Xin_model->read_payment_method($r->payment_method_id);

			if(!is_null($payment_method)){

				$method_name = $payment_method[0]->method_name;

			} else {

				$method_name = '--';	

			}	

			// payment method 

			$clientinfo = $this->Clients_model->read_client_info($r->client_id);

			if(!is_null($clientinfo)){

				$name_name = $clientinfo[0]->name;

			} else {

				$name_name = '--';	

			}

			

			$invoice_number = '<a href="'.site_url().'admin/invoices/view/'.$r->invoice_id.'/">'.$inv_no.'</a>';					

			$data[] = array(

				$invoice_number,

				$name_name,

				$transaction_date,

				$total_amount,

				$method_name,

				$r->description

			);

		  }



          $output = array(

               "draw" => $draw,

                 "recordsTotal" => $transaction->num_rows(),

                 "recordsFiltered" => $transaction->num_rows(),

                 "data" => $data

            );

          echo json_encode($output);

          exit();

     }

	 public function taxes_list()

     {



		$data['title'] = $this->Xin_model->site_title();

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view("admin/invoices/invoice_taxes", $data);

		} else {

			redirect('admin/dashboard');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		

		

		$taxes = $this->Invoices_model->get_taxes();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		$data = array();



          foreach($taxes->result() as $r) {

			

				// get type

				if($r->type == 'fixed'): $type = $this->lang->line('xin_title_tax_fixed'); else: $type = $this->lang->line('xin_title_tax_percent'); endif;

					if(in_array('332',$role_resources_ids)) { //edit

						$edit = '<span data-toggle="tooltip" data-placement="top" title="Edit"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"  data-toggle="modal" data-target=".edit-modal-data"  data-tax_id="'. $r->tax_id . '"><span class="fa fa-pencil"></span></button></span>';

					} else {

						$edit = '';

					}

					if(in_array('333',$role_resources_ids)) { // delete

						$delete = '<span data-toggle="tooltip" data-placement="top" title="Delete"><button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal" data-record-id="'. $r->tax_id . '"><span class="fa fa-trash"></span></button></span>';

					} else {

						$delete = '';

					}

					

					$combhr = $edit.$delete;

					$data[] = array(

						$combhr,

						$r->name,

						$r->rate,

						$type

				   );

			  }



          $output = array(

               "draw" => $draw,

                 "recordsTotal" => $taxes->num_rows(),

                 "recordsFiltered" => $taxes->num_rows(),

                 "data" => $data

            );

          echo json_encode($output);

          exit();

     }

	 

	 // tax data

	public function tax_read()

	{

		

		$data['title'] = $this->Xin_model->site_title();

		$id = $this->input->get('tax_id');

		$result = $this->Invoices_model->read_tax_information($id);

		$data = array(

				'tax_id' => $result[0]->tax_id,

				'name' => $result[0]->name,

				'rate' => $result[0]->rate,

				'type' => $result[0]->type,

				'description' => $result[0]->description

				);

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view('admin/invoices/dialog_tax', $data);

		} else {

			redirect('admin/');

		}

	}

	

	// Validate and add info in database

	public function add_tax() {

	

		if($this->input->post('add_type')=='tax') {		

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$Return['csrf_hash'] = $this->security->get_csrf_hash();

			

		/* Server side PHP input validation */

		$description = $this->input->post('description');

		$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);

		

		if($this->input->post('tax_name')==='') {

       		$Return['error'] = "The tax name field is required.";

		} else if($this->input->post('tax_rate')==='') {

			$Return['error'] = "The tax rate field is required.";

		} else if($this->input->post('tax_type')==='') {

			$Return['error'] = "The tax type field is required.";

		}

				

		if($Return['error']!=''){

       		$this->output($Return);

    	}

	

		$data = array(

		'name' => $this->input->post('tax_name'),

		'rate' => $this->input->post('tax_rate'),

		'type' => $this->input->post('tax_type'),

		'description' => $qt_description,

		'created_at' => date('d-m-Y h:i:s'),

		

		);

		$result = $this->Invoices_model->add_tax_record($data);

		

		if ($result == TRUE) {

			$Return['result'] = 'Product Tax added.';

		} else {

			$Return['error'] = 'Bug. Something went wrong, please try again.';

		}

		$this->output($Return);

		exit;

		}

	}

	

	// Validate and update info in database

	public function update_tax() {

	

		if($this->input->post('edit_type')=='tax') {		

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$Return['csrf_hash'] = $this->security->get_csrf_hash();

		

		$id = $this->uri->segment(4);

			

		/* Server side PHP input validation */

		$description = $this->input->post('description');

		$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);

		

		if($this->input->post('tax_name')==='') {

       		$Return['error'] = "The tax name field is required.";

		} else if($this->input->post('tax_rate')==='') {

			$Return['error'] = "The tax rate field is required.";

		} else if($this->input->post('tax_type')==='') {

			$Return['error'] = "The tax type field is required.";

		}

				

		if($Return['error']!=''){

       		$this->output($Return);

    	}

	

		$data = array(

		'name' => $this->input->post('tax_name'),

		'rate' => $this->input->post('tax_rate'),

		'type' => $this->input->post('tax_type'),

		'description' => $qt_description		

		);

		$result = $this->Invoices_model->update_tax_record($data,$id);

		

		if ($result == TRUE) {

			$Return['result'] = 'Product Tax updated.';

		} else {

			$Return['error'] = 'Bug. Something went wrong, please try again.';

		}

		$this->output($Return);

		exit;

		}

	}

	

	// edit invoice page

	public function edit() {

	

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		

		$data['title'] = $this->Xin_model->site_title();

		$session = $this->session->userdata('username');

		

		$invoice_id = $this->uri->segment(4);

		$invoice_info = $this->Invoices_model->read_invoice_info($invoice_id);

		if(is_null($invoice_info)){

			redirect('admin/invoices');

		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(!in_array('328',$role_resources_ids)) { //edit

			redirect('admin/invoices');

		}

		// get Organization
		$organization = $this->Clients_model->get_organization_info($invoice_info[0]->company_name)->result();

		// get project

		$project = $this->Project_model->read_project_information($invoice_info[0]->project_id);

		// get country

	//	$country = $this->Xin_model->read_country_info($supplier[0]->country_id);

		// get company info

		$company = $this->Xin_model->read_company_setting_info(1);

		// get company > country info

		$ccountry = $this->Xin_model->read_country_info($company[0]->country);

		$data = array(

			'title' => $this->lang->line('xin_title_edit_invoice').' '.$invoice_info[0]->invoice_id,

			'breadcrumbs' => $this->lang->line('xin_title_edit_invoice'),

			'path_url' => 'create_hrsale_invoice',

			'invoice_id' => $invoice_info[0]->invoice_id,

			'invoice_number' => $invoice_info[0]->invoice_number,

			// 'project_id' => $project[0]->project_id,

			'invoice_date' => $invoice_info[0]->invoice_date,

			'invoice_due_date' => $invoice_info[0]->invoice_due_date,

			'invoice_start_date' => $invoice_info[0]->invoice_start_date,

			'invoice_end_date' => $invoice_info[0]->invoice_end_date,

			'sub_total_amount' => $invoice_info[0]->sub_total_amount,

			'discount_type' => $invoice_info[0]->discount_type,

			'discount_figure' => $invoice_info[0]->discount_figure,

			'total_tax' => $invoice_info[0]->total_tax,

			'total_discount' => $invoice_info[0]->total_discount,

			'grand_total' => $invoice_info[0]->grand_total,

			'invoice_note' => $invoice_info[0]->invoice_note,

			'all_projects' => $this->Project_model->get_projects(),

			'all_taxes' => $this->Tax_model->get_all_taxes(),

			'comp_id' => $organization[0]->id

		//	'product_for_purchase_invoice' => $this->Products_model->product_for_purchase_invoice(),

		//	'all_taxes' => $this->Products_model->get_taxes()

			);

		$data['all_organizations'] = $this->Training_model->getAll2('xin_organization',' 1 order by id asc');

		$role_resources_ids = $this->Xin_model->user_role_resource();

		//if(in_array('3',$role_resources_ids)) {

		$data['subview'] = $this->load->view("admin/invoices/edit_invoice", $data, TRUE);

		$this->load->view('admin/layout/layout_main', $data); //page load			

		//} else {

		//	redirect('admin/dashboard/');

		//}		  

     }

	

	// view invoice page

	public function view() {

	

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		

		$data['title'] = $this->Xin_model->site_title();

		$session = $this->session->userdata('username');

		

		$invoice_id = $this->uri->segment(4);

		$invoice_info = $this->Invoices_model->read_invoice_info($invoice_id);

		if(is_null($invoice_info)){

			redirect('admin/invoices');

		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(!in_array('330',$role_resources_ids)) { //view

			redirect('admin/invoices');

		}

		// get project

		$project = $this->Project_model->read_project_information($invoice_info[0]->project_id);

		// get country

	//	$country = $this->Xin_model->read_country_info($supplier[0]->country_id);

		// get company info

		$company = $this->Xin_model->read_company_setting_info(1);
		// print_r($company);die;
		// get organization
		$organization = $this->Clients_model->get_organization_info($invoice_info[0]->company_name)->result();
		$location = $this->Invoices_model->get_location_info($organization[0]->location_id)->result();

		// print_r($organization);die;
		// get company > country info

		$ccountry = $this->Xin_model->read_country_info($company[0]->country);

		

		$data = array(

			'title' => $this->lang->line('xin_view_invoice').' ' .$invoice_info[0]->invoice_id,

			'breadcrumbs' => $this->lang->line('xin_view_invoice'),

			'path_url' => 'create_hrsale_invoice',

			'invoice_id' => $invoice_info[0]->invoice_id,

			'status' => $invoice_info[0]->status,

			'invoice_number' => $invoice_info[0]->invoice_number,

			// 'project_id' => $project[0]->project_id,

			'invoice_date' => $invoice_info[0]->invoice_date,

			'invoice_due_date' => $invoice_info[0]->invoice_due_date,

			'sub_total_amount' => $invoice_info[0]->sub_total_amount,

			'discount_type' => $invoice_info[0]->discount_type,

			'discount_figure' => $invoice_info[0]->discount_figure,

			'total_tax' => $invoice_info[0]->total_tax,

			'total_discount' => $invoice_info[0]->total_discount,

			'grand_total' => $invoice_info[0]->grand_total,

			'invoice_note' => $invoice_info[0]->invoice_note,

			'company_name' => $company[0]->company_name,

			'company_address' => $company[0]->address_1,

			'company_zipcode' => $company[0]->zipcode,

			'company_city' => $location[0]->location_name,

			'company_phone' => $company[0]->phone,

			'company_country' => $ccountry[0]->country_name,

			'name' => $invoice_info[0]->name,

			'client_company_name' => $organization[0]->name,

			'client_profile' => $invoice_info[0]->client_profile,

			'email' => $invoice_info[0]->email,

			'contact_number' => $invoice_info[0]->contact_number,

			'website_url' => $invoice_info[0]->website_url,

			'address_1' => $organization[0]->rc_number,

			'address_2' => $invoice_info[0]->address_2,

			'city' => $invoice_info[0]->city,

			'state' => $invoice_info[0]->state,

			'zipcode' => $invoice_info[0]->zipcode,

			'countryid' => $invoice_info[0]->countryid,

			'all_projects' => $this->Project_model->get_projects(),

			'all_taxes' => $this->Tax_model->get_all_taxes(),

		//	'product_for_purchase_invoice' => $this->Products_model->product_for_purchase_invoice(),

		//	'all_taxes' => $this->Products_model->get_taxes()

			);

		$role_resources_ids = $this->Xin_model->user_role_resource();

		//if(in_array('3',$role_resources_ids)) {

			$data['subview'] = $this->load->view("admin/invoices/invoice_view", $data, TRUE);

			$this->load->view('admin/layout/layout_main', $data); //page load			

		//} else {

		//	redirect('admin/dashboard/');

		//}		  

     }

	 

	public function invoices_list()

    {

		$data['title'] = $this->Xin_model->site_title();

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view("admin/invoices/invoices_list", $data);

		} else {

			redirect('admin/');

		}


		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));
		

		if (!is_null($this->input->get('from'))) {
			$from = $this->input->get('from');
			$to = $this->input->get('to');
			$org = $this->input->get('org');

			if(!is_null($org)){
				$client = $this->Invoices_model->get_invoices_date_range($from,$to,$org);
			}else{
				$client = $this->Invoices_model->get_invoices_date_range($from,$to);

			}
		}
		else{
			$client = $this->Invoices_model->get_invoices();
		}


		$role_resources_ids = $this->Xin_model->user_role_resource();

		$data = array();



          foreach($client->result() as $r) {

			  // get country


			  // get project

			  $project = $this->Project_model->read_project_information($r->project_id); 

			  if(!is_null($project)){

			  	$project_name = $project[0]->title;

			  } else {

				  $project_name = '--';	

			  }

			  // Organization
			  $organization = $this->Clients_model->get_organization_info($r->company_name)->result_array();

			  $organization_name = $organization[0]['name'];

			  // Total Client
			  $client_invoice = $this->checkInvoicePrev($r->invoice_id);
			  $total_client = count($client_invoice);

			  $grand_total = $this->Xin_model->currency_sign($r->grand_total);


			  // Date

			  $invoice_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_date);

			  $invoice_due_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_due_date);	

			  $invoice_start_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_start_date);

			  $invoice_end_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_end_date);

			  //invoice_number

			  $invoice_number = '';

				if(in_array('330',$role_resources_ids)) { //view

					$invoice_number = '<a href="'.site_url().'admin/invoices/view/'.$r->invoice_id.'/">'.$r->invoice_number.'</a>';

				} else {

					$invoice_number = $r->invoice_number;

				}

			  if(in_array('328',$role_resources_ids)) { //edit

				$edit = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_edit').'"><a href="'.site_url().'admin/invoices/edit/'.$r->invoice_id.'/"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"><span class="fa fa-pencil"></span></button></a></span>';

			} else {

				$edit = '';

			}

			if(in_array('329',$role_resources_ids)) { // delete

				$delete = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_delete').'"><button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal" data-record-id="'. $r->invoice_id . '"><span class="fa fa-trash"></span></button></span>';

			} else {

				$delete = '';

			}

			if(in_array('330',$role_resources_ids)) { //view

				$view = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_view').'"><a href="'.site_url().'admin/invoices/view/'.$r->invoice_id.'/"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light""><span class="fa fa-arrow-circle-right"></span></button></a></span>';

			} else {

				$view = '';

			}

			if($r->status == 0){

				$status = '<span class="label label-danger">'.$this->lang->line('xin_payroll_unpaid').'</span>';

			} else if($r->status == 1) {

				$status = '<span class="label label-success">'.$this->lang->line('xin_payment_paid').'</span>';

			} else {

				$status = '<span class="label label-info">'.$this->lang->line('xin_acc_inv_cancelled').'</span>';

			}

			$combhr = $edit.$view.$delete;

		   $data[] = array(

				$combhr,

				$invoice_number,

				$organization_name,

				$grand_total,

				$total_client,

				$invoice_date,

				$invoice_start_date,

				$invoice_end_date,

				$status,

		   );

          }



          $output = array(

               "draw" => $draw,

                 "recordsTotal" => $client->num_rows(),

                 "recordsFiltered" => $client->num_rows(),

                 "data" => $data

            );

          echo json_encode($output);

          exit();

    }

    public function invoices_paid_list()

    {

		$data['title'] = $this->Xin_model->site_title();

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view("admin/invoices/invoices_list", $data);

		} else {

			redirect('admin/');

		}


		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));
		

		if (!is_null($this->input->get('from'))) {
			$from = $this->input->get('from');
			$to = $this->input->get('to');
			$client = $this->Invoices_model->get_invoices_date_range_paid($from,$to);
		}else{
			$client = $this->Invoices_model->get_invoices_paid();
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		$data = array();



          foreach($client->result() as $r) {

			  // get country


			  // get project

			  $project = $this->Project_model->read_project_information($r->project_id); 

			  if(!is_null($project)){

			  	$project_name = $project[0]->title;

			  } else {

				  $project_name = '--';	

			  }

			  // Organization
			  $organization = $this->Clients_model->get_organization_info($r->company_name)->result_array();

			  $organization_name = $organization[0]['name'];

			  // Total Client
			  $client_invoice = $this->checkInvoicePrev($r->invoice_id);
			  $total_client = count($client_invoice);

			  $grand_total = $this->Xin_model->currency_sign($r->grand_total);


			  // Date

			  $invoice_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_date);

			  $invoice_due_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_due_date);	

			  $invoice_start_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_start_date);

			  $invoice_end_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_end_date);

			  //invoice_number

			  $invoice_number = '';

				if(in_array('330',$role_resources_ids)) { //view

					$invoice_number = '<a href="'.site_url().'admin/invoices/view/'.$r->invoice_id.'/">'.$r->invoice_number.'</a>';

				} else {

					$invoice_number = $r->invoice_number;

				}

			  if(in_array('328',$role_resources_ids)) { //edit

				$edit = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_edit').'"><a href="'.site_url().'admin/invoices/edit/'.$r->invoice_id.'/"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"><span class="fa fa-pencil"></span></button></a></span>';

			} else {

				$edit = '';

			}

			if(in_array('329',$role_resources_ids)) { // delete

				$delete = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_delete').'"><button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal" data-record-id="'. $r->invoice_id . '"><span class="fa fa-trash"></span></button></span>';

			} else {

				$delete = '';

			}

			if(in_array('330',$role_resources_ids)) { //view

				$view = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_view').'"><a href="'.site_url().'admin/invoices/view/'.$r->invoice_id.'/"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light""><span class="fa fa-arrow-circle-right"></span></button></a></span>';

			} else {

				$view = '';

			}

			if($r->status == 0){

				$status = '<span class="label label-danger">'.$this->lang->line('xin_payroll_unpaid').'</span>';

			} else if($r->status == 1) {

				$status = '<span class="label label-success">'.$this->lang->line('xin_payment_paid').'</span>';

			} else {

				$status = '<span class="label label-info">'.$this->lang->line('xin_acc_inv_cancelled').'</span>';

			}

			$combhr = $edit.$view.$delete;

		   $data[] = array(

				$combhr,

				$invoice_number,

				$organization_name,

				$grand_total,

				$total_client,

				$invoice_date,

				$invoice_start_date,

				$invoice_end_date,

				$status,

		   );

          }



          $output = array(

               "draw" => $draw,

                 "recordsTotal" => $client->num_rows(),

                 "recordsFiltered" => $client->num_rows(),

                 "data" => $data

            );

          echo json_encode($output);

          exit();

    }

    public function invoices_unpaid_list()

    {

		$data['title'] = $this->Xin_model->site_title();

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view("admin/invoices/invoices_list", $data);

		} else {

			redirect('admin/');

		}


		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));
		

		if (!is_null($this->input->get('from'))) {
			$from = $this->input->get('from');
			$to = $this->input->get('to');
			$client = $this->Invoices_model->get_invoices_date_range_unpaid($from,$to);
		}else{
			$client = $this->Invoices_model->get_invoices_unpaid();
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		$data = array();



          foreach($client->result() as $r) {

			  // get country


			  // get project

			  $project = $this->Project_model->read_project_information($r->project_id); 

			  if(!is_null($project)){

			  	$project_name = $project[0]->title;

			  } else {

				  $project_name = '--';	

			  }

			  // Organization
			  $organization = $this->Clients_model->get_organization_info($r->company_name)->result_array();

			  $organization_name = $organization[0]['name'];

			  // Total Client
			  $client_invoice = $this->checkInvoicePrev($r->invoice_id);
			  $total_client = count($client_invoice);

			  $grand_total = $this->Xin_model->currency_sign($r->grand_total);


			  // Date

			  $invoice_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_date);

			  $invoice_due_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_due_date);	

			  $invoice_start_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_start_date);

			  $invoice_end_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_end_date);

			  //invoice_number

			  $invoice_number = '';

				if(in_array('330',$role_resources_ids)) { //view

					$invoice_number = '<a href="'.site_url().'admin/invoices/view/'.$r->invoice_id.'/">'.$r->invoice_number.'</a>';

				} else {

					$invoice_number = $r->invoice_number;

				}

			  if(in_array('328',$role_resources_ids)) { //edit

				$edit = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_edit').'"><a href="'.site_url().'admin/invoices/edit/'.$r->invoice_id.'/"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"><span class="fa fa-pencil"></span></button></a></span>';

			} else {

				$edit = '';

			}

			if(in_array('329',$role_resources_ids)) { // delete

				$delete = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_delete').'"><button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal" data-record-id="'. $r->invoice_id . '"><span class="fa fa-trash"></span></button></span>';

			} else {

				$delete = '';

			}

			if(in_array('330',$role_resources_ids)) { //view

				$view = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_view').'"><a href="'.site_url().'admin/invoices/view/'.$r->invoice_id.'/"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light""><span class="fa fa-arrow-circle-right"></span></button></a></span>';

			} else {

				$view = '';

			}

			if($r->status == 0){

				$status = '<span class="label label-danger">'.$this->lang->line('xin_payroll_unpaid').'</span>';

			} else if($r->status == 1) {

				$status = '<span class="label label-success">'.$this->lang->line('xin_payment_paid').'</span>';

			} else {

				$status = '<span class="label label-info">'.$this->lang->line('xin_acc_inv_cancelled').'</span>';

			}

			$combhr = $edit.$view.$delete;

		   $data[] = array(

				$combhr,

				$invoice_number,

				$organization_name,

				$grand_total,

				$total_client,

				$invoice_date,

				$invoice_due_date,

				$invoice_start_date,

				$invoice_end_date,

				$status,

		   );

          }



          $output = array(

               "draw" => $draw,

                 "recordsTotal" => $client->num_rows(),

                 "recordsFiltered" => $client->num_rows(),

                 "data" => $data

            );

          echo json_encode($output);

          exit();

    }


    // For invoice about to expire
	public function invoice_expire() 
	{
		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->lang->line('xin_invoices_expire').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_invoices_expire');

		$data['all_projects'] = $this->Project_model->get_projects();

		$data['all_taxes'] = $this->Tax_model->get_all_taxes();

		$data['path_url'] = 'hrsale_invoices';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('121',$role_resources_ids)) {

			$data['subview'] = $this->load->view("admin/invoices/invoice_expire", $data, TRUE);

			$this->load->view('admin/layout/layout_main', $data); //page load

		} else {

			redirect('admin/dashboard');

		}

	}

	public function invoice_expire_list()

    {

		$data['title'] = $this->Xin_model->site_title();

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view("admin/invoices/invoices_list", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		

		

		$client = $this->Invoices_model->get_invoices_expire();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		$data = array();



          foreach($client->result() as $r) {

			  // get country


			  // get project

			  $project = $this->Project_model->read_project_information($r->project_id); 

			  if(!is_null($project)){

			  	$project_name = $project[0]->title;

			  } else {

				  $project_name = '--';	

			  }

			  // Organization
			  $organization = $this->Clients_model->get_organization_info($r->company_name)->result_array();

			  $organization_name = $organization[0]['name'];

			  // Total Client
			  $client_invoice = $this->checkInvoicePrev($r->invoice_id);
			  $total_client = count($client_invoice);

			  $grand_total = $r->grand_total;


			  // Date

			  $invoice_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_date);

			  $invoice_due_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_due_date);	

			  $invoice_start_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_start_date);

			  $invoice_end_date = '<i class="far fa-calendar-alt position-left"></i> '.$this->Xin_model->set_date_format($r->invoice_end_date);

			  //invoice_number

			  $invoice_number = '';

				if(in_array('330',$role_resources_ids)) { //view

					$invoice_number = '<a href="'.site_url().'admin/invoices/view/'.$r->invoice_id.'/">'.$r->invoice_number.'</a>';

				} else {

					$invoice_number = $r->invoice_number;

				}

			  if(in_array('328',$role_resources_ids)) { //edit

				$edit = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_edit').'"><a href="'.site_url().'admin/invoices/edit/'.$r->invoice_id.'/"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"><span class="fa fa-pencil"></span></button></a></span>';

			} else {

				$edit = '';

			}

			if(in_array('329',$role_resources_ids)) { // delete

				$delete = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_delete').'"><button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal" data-record-id="'. $r->invoice_id . '"><span class="fa fa-trash"></span></button></span>';

			} else {

				$delete = '';

			}

			if(in_array('330',$role_resources_ids)) { //view

				$view = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_view').'"><a href="'.site_url().'admin/invoices/view/'.$r->invoice_id.'/"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light""><span class="fa fa-arrow-circle-right"></span></button></a></span>';

			} else {

				$view = '';

			}

			if($r->status == 0){

				$status = '<span class="label label-danger">'.$this->lang->line('xin_payroll_unpaid').'</span>';

			} else if($r->status == 1) {

				$status = '<span class="label label-success">'.$this->lang->line('xin_payment_paid').'</span>';

			} else {

				$status = '<span class="label label-info">'.$this->lang->line('xin_acc_inv_cancelled').'</span>';

			}

			$combhr = $edit.$view.$delete;

		   $data[] = array(

				// $combhr,

				$invoice_number,

				$organization_name,

				$grand_total,

				$total_client,

				$invoice_date,

				$invoice_due_date,

				$invoice_start_date,

				$invoice_end_date,

				$status,

		   );

          }



          $output = array(

               "draw" => $draw,

                 "recordsTotal" => $client->num_rows(),

                 "recordsFiltered" => $client->num_rows(),

                 "data" => $data

            );

          echo json_encode($output);

          exit();

    }	

	// Validate and add info in database

	public function create_new_invoice() {

		// if($this->input->post('add_type')=='invoice_create') {		

		/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

				

			/* Server side PHP input validation */	

			 if($this->input->post('invoice_number')==='') {

	       		$Return['error'] = "The invoice number field is required.";

				$this->session->set_flashdata('numberempty_invoice','Number field cannot be empty');
				
				redirect('admin/invoices/create');

			} else if($this->input->post('invoice_date')==='') {
				$this->session->set_flashdata('dateempty_invoice','Invoice date cannot be empty');
				
				redirect('admin/invoices/create');

	       		$Return['error'] = "The invoice date field is required.";

			} else if($this->input->post('invoice_due_date')==='') {
				$this->session->set_flashdata('duedateempty_invoice','Due date cannot be empty');
				
				redirect('admin/invoices/create');

				$Return['error'] = "The invoice due date field is required.";

			} else if($this->input->post('invoice_start_date')==='') {
				$this->session->set_flashdata('startdateempty_invoice','start date cannot be empty');
				
				redirect('admin/invoices/create');

				$Return['error'] = "The invoice due date field is required.";

			} else if($this->input->post('invoice_end_date')==='') {
				$this->session->set_flashdata('enddateempty_invoice','end date cannot be empty');
				
				redirect('admin/invoices/create');

				$Return['error'] = "The invoice due date field is required.";

			} 

			$j=0; 
			// foreach($this->input->post('item_name') as $items){

			// 		$item_name = $this->input->post('item_name');

			// 		$iname = $item_name[$j];

			// 		// item qty

			// 		$qty = $this->input->post('qty_hrs');

			// 		$qtyhrs = $qty[$j];

			// 		// item price

			// 		$unit_price = $this->input->post('unit_price');

			// 		$price = $unit_price[$j];

					

			// 		if($iname==='') {

			// 			$Return['error'] = "The Item field is required.";

			// 		} else if($qty==='') {

			// 			$Return['error'] = "The Qty/hrs field is required.";

			// 		} else if($price==='' || $price===0) {

			// 			$Return['error'] = $j. " The Price field is required.";

			// 		}

			// 		$j++;

			// }


			$proj_info = $this->Project_model->read_project_information($this->input->post('project'));		
			$organization = $this->Clients_model->get_organization_info($this->input->post('project'))->result_array();

			$grand_total = $this->input->post('fgrand_total');
			$discount = $this->input->post('discount_amount');
			// $grand_total = $grand_total-$discount;

			$data = array(

			'project_id' => $this->input->post('project'),

			'invoice_number' => $this->input->post('invoice_number'),

			'invoice_date' => $this->input->post('invoice_date'),

			'invoice_due_date' => $this->input->post('invoice_due_date'),

			'invoice_start_date' => $this->input->post('invoice_start_date'),

			'invoice_end_date' => $this->input->post('invoice_end_date'),

			'sub_total_amount' => $this->input->post('items_sub_total'),

			'total_tax' => $this->input->post('items_tax_total'),

			'discount_type' => $this->input->post('discount_type'),

			'discount_figure' => $this->input->post('discount_figure'),

			'total_discount' => $discount,

			'grand_total' => $grand_total,

			'invoice_note' => $this->input->post('invoice_note'),

			'name' => $organization[0]['contact_person'],

			'company_name' => $organization[0]['id'],

			// 'client_profile' => $organization[0]['logo_name'],

			// 'email' => $clientinfo[0]->email,

			// 'contact_number' => $clientinfo[0]->contact_number,

			// 'website_url' => $clientinfo[0]->website_url,

			// 'address_1' => $clientinfo[0]->address_1,

			// 'address_2' => $clientinfo[0]->address_2,

			// 'city' => $clientinfo[0]->city,

			// 'state' => $clientinfo[0]->state,

			// 'zipcode' => $clientinfo[0]->zipcode,

			// 'countryid' => $clientinfo[0]->country,

			'status' => '0',

			'created_at' => date('Y-m-d H:i:s')

			);

			$result = $this->Invoices_model->add_invoice_record($data);

			if ($result) {

				// $key=0;

				// foreach($this->input->post('item_name') as $items){

				// 	// $iname = $items['item_name']; 
				// 	$sub_total_item = $this->input->post('sub_total_item');
				// 	$item_sub_total = $sub_total_item[$key];

				// 	$data2 = array(

				// 	'invoice_id' => $result,

				// 	'project_id' => $this->input->post('project'),

				// 	'item_name' => $items,

				// 	'item_sub_total' => $item_sub_total,

				// 	'sub_total_amount' => $this->input->post('items_sub_total'),

				// 	'discount_type' => $this->input->post('discount_type'),

				// 	'discount_figure' => $this->input->post('discount_figure'),

				// 	'total_discount' => $this->input->post('discount_amount'),

				// 	'grand_total' => $this->input->post('fgrand_total'),

				// 	'created_at' => date('d-m-Y H:i:s')

				// 	);

				// 	$result_item = $this->Invoices_model->add_invoice_items_record($data2);
					/* get items info */

					// item name

					// $iname = $items['item_name']; 

					// $item_name = $this->input->post('item_name');

					// $iname = $item_name[$key]; 

					// // item qty

					// $qty = $this->input->post('qty_hrs');

					// $qtyhrs = $qty[$key]; 

					// // item price

					// $unit_price = $this->input->post('unit_price');

					// $price = $unit_price[$key]; 

					// // item tax_id

					// $taxt = $this->input->post('tax_type');

					// $tax_type = $taxt[$key]; 

					// // item tax_rate

					// $tax_rate_item = $this->input->post('tax_rate_item');

					// $tax_rate = $tax_rate_item[$key];

					// item sub_total

					// $sub_total_item = $this->input->post('sub_total_item');

					// $item_sub_total = $sub_total_item[$key];

					// add values  

					// $data2 = array(

					// 'invoice_id' => $result,

					// 'project_id' => $this->input->post('project'),

					// 'item_name' => $iname,

					// // 'item_qty' => $qtyhrs,

					// // 'item_unit_price' => $price,

					// // 'item_tax_type' => $tax_type,

					// // 'item_tax_rate' => $tax_rate,

					// 'item_sub_total' => $item_sub_total,

					// 'sub_total_amount' => $this->input->post('items_sub_total'),

					// // 'total_tax' => $this->input->post('items_tax_total'),

					// 'discount_type' => $this->input->post('discount_type'),

					// 'discount_figure' => $this->input->post('discount_figure'),

					// 'total_discount' => $this->input->post('discount_amount'),

					// 'grand_total' => $this->input->post('fgrand_total'),

					// 'created_at' => date('d-m-Y H:i:s')

					// );

					// $result_item = $this->Invoices_model->add_invoice_items_record($data2);

					

				$this->session->set_flashdata('success_invoice','Debit Note Successfully Created');
				redirect('admin/invoices/create');

				// $key++; }

			} else {
				$this->session->set_flashdata('fail_invoice','Debit Note failed to create, please check again');
				redirect('admin/invoices/create');
			// 	$Return['error'] = 'Bug. Something went wrong, please try again.';

			}

			// $this->output($Return);


			// echo "
			// 	<script>
			// 		toastr.success(JSON.result);

			// 		$('.save').prop('disabled', false);

			// 		$('input[name=\"csrf_hrsale\"]').val(JSON.csrf_hash);

			// 		// window.location = site_url+'invoices/';
			// 	</script>

			// ";

			// exit;

	}

	

	// Validate and add info in database

	public function update_invoice() {
		// if($this->input->post('add_type')=='invoice_create') {		

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$Return['csrf_hash'] = $this->security->get_csrf_hash();

		$id = $this->uri->segment(4);

		/* Server side PHP input validation */	

		if($this->input->post('invoice_number')==='') {

       		$Return['error'] = "The Debit Note number field is required.";

			$this->session->set_flashdata('numberempty_invoice','Number field cannot be empty');
			
			redirect('admin/invoices/edit/'.$id);

		} else if($this->input->post('invoice_date')==='') {
			$this->session->set_flashdata('dateempty_invoice','Debit Note date cannot be empty');
			
			redirect('admin/invoices/edit/'.$id);

       		$Return['error'] = "The invoice date field is required.";

		} else if($this->input->post('invoice_due_date')==='') {
			$this->session->set_flashdata('duedateempty_invoice','Due date cannot be empty');
			
			redirect('admin/invoices/edit/'.$id);

			$Return['error'] = "The invoice due date field is required.";

		} else if($this->input->post('invoice_start_date')==='') {
			$this->session->set_flashdata('startdateempty_invoice','start date cannot be empty');
			
			redirect('admin/invoices/edit/'.$id);

			$Return['error'] = "The invoice due date field is required.";

		} else if($this->input->post('invoice_end_date')==='') {
			$this->session->set_flashdata('enddateempty_invoice','end date cannot be empty');
			
			redirect('admin/invoices/edit/'.$id);

			$Return['error'] = "The Debit Note due date field is required.";

		}

	

		// // add purchase items

		// foreach($this->input->post('item') as $eitem_id=>$key_val){

			

		// 	/* get items info */

		// 	// item qty

		// 	$item_name = $this->input->post('eitem_name');

		// 	$iname = $item_name[$key_val]; 

		// 	// item qty

		// 	$qty = $this->input->post('eqty_hrs');

		// 	$qtyhrs = $qty[$key_val]; 

		// 	// item price

		// 	$unit_price = $this->input->post('eunit_price');

		// 	$price = $unit_price[$key_val]; 

		// 	// item tax_id

		// 	$taxt = $this->input->post('etax_type');

		// 	$tax_type = $taxt[$key_val]; 

		// 	// item tax_rate

		// 	$tax_rate_item = $this->input->post('etax_rate_item');

		// 	$tax_rate = $tax_rate_item[$key_val];

		// 	// item sub_total

		// 	$sub_total_item = $this->input->post('esub_total_item');

		// 	$item_sub_total = $sub_total_item[$key_val];

			

			// // update item values  

			// $data = array(

			// 	'item_name' => $iname,

			// 	'item_qty' => $qtyhrs,

			// 	'item_unit_price' => $price,

			// 	'item_tax_type' => $tax_type,

			// 	'item_tax_rate' => $tax_rate,

			// 	'item_sub_total' => $item_sub_total,

			// 	'sub_total_amount' => $this->input->post('items_sub_total'),

			// 	'total_tax' => $this->input->post('items_tax_total'),

			// 	'discount_type' => $this->input->post('discount_type'),

			// 	'discount_figure' => $this->input->post('discount_figure'),

			// 	'total_discount' => $this->input->post('discount_amount'),

			// 	'grand_total' => $this->input->post('fgrand_total'),

			// );

			// $result_item = $this->Invoices_model->update_invoice_items_record($data,$eitem_id);

			

		// }

		$grand_total = $this->input->post('fgrand_total');

		if (strpos($grand_total, ",")) {
			$grand_total = str_replace(",", "", $grand_total);
		}


		$discount = $this->input->post('discount_amount');
		// $grand_total = $grand_total-$discount;		

		////

		$data = array(

		'project_id' => $this->input->post('project'),

		'invoice_number' => $this->input->post('invoice_number'),

		'invoice_date' => $this->input->post('invoice_date'),

		'invoice_due_date' => $this->input->post('invoice_due_date'),

		'invoice_start_date' => $this->input->post('invoice_start_date'),

		'invoice_end_date' => $this->input->post('invoice_end_date'),

		'sub_total_amount' => $this->input->post('items_sub_total'),

		'total_tax' => $this->input->post('items_tax_total'),

		'discount_type' => $this->input->post('discount_type'),

		'discount_figure' => $this->input->post('discount_figure'),

		'total_discount' => $discount,

		'grand_total' => $grand_total,

		'invoice_note' => $this->input->post('invoice_note'),

		);

		$result = $this->Invoices_model->update_invoice_record($data,$id);

		if ($result) {
			$this->session->set_flashdata('update_invoice','Invoice updated');
				
			redirect('admin/invoices/edit/'.$id);
		}	



		// if($this->input->post('item_name')) {

		// 	$key=0;

		// 	foreach($this->input->post('item_name') as $items){



		// 		/* get items info */

		// 		// item name

		// 		$item_name = $this->input->post('item_name');

		// 		$iname = $item_name[$key]; 

		// 		// item qty

		// 		$qty = $this->input->post('qty_hrs');

		// 		$qtyhrs = $qty[$key]; 

		// 		// item price

		// 		$unit_price = $this->input->post('unit_price');

		// 		$price = $unit_price[$key]; 

		// 		// item tax_id

		// 		$taxt = $this->input->post('tax_type');

		// 		$tax_type = $taxt[$key]; 

		// 		// item tax_rate

		// 		$tax_rate_item = $this->input->post('tax_rate_item');

		// 		$tax_rate = $tax_rate_item[$key];

		// 		// item sub_total

		// 		$sub_total_item = $this->input->post('sub_total_item');

		// 		$item_sub_total = $sub_total_item[$key];

		// 		// add values  

		// 		$data2 = array(

		// 		'invoice_id' => $id,

		// 		'project_id' => $this->input->post('project'),

		// 		'item_name' => $iname,

		// 		'item_qty' => $qtyhrs,

		// 		'item_unit_price' => $price,

		// 		'item_tax_type' => $tax_type,

		// 		'item_tax_rate' => $tax_rate,

		// 		'item_sub_total' => $item_sub_total,

		// 		'sub_total_amount' => $this->input->post('items_sub_total'),

		// 		'total_tax' => $this->input->post('items_tax_total'),

		// 		'discount_type' => $this->input->post('discount_type'),

		// 		'discount_figure' => $this->input->post('discount_figure'),

		// 		'total_discount' => $this->input->post('discount_amount'),

		// 		'grand_total' => $this->input->post('fgrand_total'),

		// 		'created_at' => date('d-m-Y H:i:s')

		// 		);

		// 		$result_item = $this->Invoices_model->add_invoice_items_record($data2);

				

			// $key++; }

			// $Return['result'] = 'Invoice updated.';

		// } else {

			//$Return['error'] = 'Bug. Something went wrong, please try again.';

		// }

		// $Return['result'] = 'Invoice updated.';

		// $this->output($Return);

		// exit;

		}
	

	// delete a purchase record

	public function delete_item() {

		

		if($this->uri->segment(5) == 'isajax') {

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'');

			$id = $this->uri->segment(4);

			

			$result = $this->Invoices_model->delete_invoice_items_record($id);

			if(isset($id)) {

				$Return['result'] = 'Debit Note Item deleted.';

			} else {

				$Return['error'] = 'Bug. Something went wrong, please try again.';

			}

			$this->output($Return);

		}

	}

	

	// delete a purchase record

	public function delete() {

		

		if($this->input->post('is_ajax') == '2') {

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'');

			$id = $this->uri->segment(4);

			

			$result = $this->Invoices_model->delete_record($id);

			if(isset($id)) {

				$result_item = $this->Invoices_model->delete_invoice_items($id);

				$Return['result'] = 'Debit Note deleted.';

			} else {

				$Return['error'] = 'Bug. Something went wrong, please try again.';

			}

			$this->output($Return);

		}

	}

	

	// delete a tax record

	public function tax_delete() {

		if($this->input->post('is_ajax')==='2') {

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'');

			$id = $this->uri->segment(4);

			$result = $this->Invoices_model->delete_tax_record($id);

			if(isset($id)) {

				$Return['result'] = 'Tax deleted.';

			} else {

				$Return['error'] = 'Bug. Something went wrong, please try again.';

			}

			$this->output($Return);

		}

	}

	public function changeStatus(){
		$session = $this->session->userdata('username');
		$status = $this->input->post('statusInvoice');
		$id = $this->input->post('id');
		
		$bankcash_id = $this->input->post('bank');
		$total = $this->input->post('grand_total');
		$balance = $this->Finance_model->get_balance($bankcash_id)->result_array();
		$total2 = $total+$balance[0]['account_balance'];
		// print_r($balance);die;
		$bankdata = array(
			'account_balance' => $total2
		);

		$data = array(
			'status' => $status
		);

		//add transaction income
		$tdata = array(

			'account_id' => $bankcash_id,

			'amount' => $total,

			'transaction_type' => 'income',

			'dr_cr' => 'dr',

			'transaction_date' => date('Y-m-d'),

			'attachment_file' => 'no_file',

			'transaction_cat_id' => '2',

			'sub_deposit_ids' => '',

			'payer_payee_id' => $session['user_id'],

			'payment_method_id' => 1,

			'description' => ' ',

			'reference' => ' ',

			'invoice_id' => $id,

			'created_at' => date('Y-m-d H:i:s')

		);

		$result = $this->Invoices_model->update_invoice_record($data,$id);
		if($result){
			

			$result = $this->Finance_model->add_transactions($tdata);	

			$result = $this->Finance_model->update_bankcash_record($bankdata,$bankcash_id);
			$Return['result'] = "Status updated";
			redirect('admin/invoices/','refresh');
		}
	}

	public function ordinal($number) {
	    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
	    if ((($number % 100) >= 11) && (($number%100) <= 13))
	        return $number. 'th';
	    else
        return $number. $ends[$number % 10];
	}

	public function pdf($id){
		$this->load->library('Pdf');

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
	    $pdf->SetTitle('Debit Note');
	    $pdf->SetTopMargin(20);
	    $pdf->SetLeftMargin(20);
	    $pdf->SetRightMargin(20);
	    $pdf->setFooterMargin(20);
	    $pdf->SetAutoPageBreak(true);
	    $pdf->SetAuthor('Author');
	    $pdf->SetDisplayMode('real', 'default');
	    $pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);	
	    $pdf->AddPage();


		$pdf->Image('/app/uploads/logo/invoice.png', 170, 10, 30, 30, '','', '', true, 150, '', false,false, 1, true);
		$pdf->SetFont('Times','B',12);

		// Invoice

	    $invoice = $this->Invoices_model->get_invoices_info($id)->result_array();
		
		// Date
		$day = date("j");
		$day = $this->ordinal($day);
		$date = date(" F, Y");
		$pdf->Write(0, $day.$date, '', 0, 'L', true, 0, false, false, 0);
		$pdf->Write(5, "", '', 0, 'L', true, 0, false, false, 0);
		
		$pdf->SetFont('Times','',13); 
		$pdf->Write(0, "The MD,", '', 0, 'L', true, 0, false, false, 0);

	
		// Companies
		$company = $this->Company_model->get_companies()->result_array();
		$country = $this->Company_model->get_company_country($company[0]['country'])->result_array();

		// get organization
		$organization = $this->Clients_model->get_organization_info($invoice[0]['company_name'])->result();
		$location = $this->Invoices_model->get_location_info($organization[0]->location_id)->result();

		// print_r($location);die;
		
		$pdf->SetFont('Times','',12); 
		$pdf->Write(0, $organization[0]->name, '', 0, 'L', true, 0, false, false, 0);
		$pdf->Write(0, $organization[0]->rc_number, '', 0, 'L', true, 0, false, false, 0 );
		$pdf->Write(0, $location[0]->location_name, '', 0, 'L', true, 0, false, false, 0);
		
		$pdf->Write(5, "", '', 0, 'L', true, 0, false, false, 0);
	    
	    $pdf->SetFont('Times','B',12); 
		$pdf->Write(0, "Dear Sir/Ma", '', 0, 'L', true, 0, false, false, 0);

		$pdf->Write(5, "", '', 0, 'L', true, 0, false, false, 0);
	    
		$pdf->Write(0, "SUBMISSION OF FINANCIAL QUOTATION FOR HEALTH INSURANCE", '', 0, 'L', true, 0, false, false, 0);

		$pdf->Write(5, "", '', 0, 'L', true, 0, false, false, 0);
	    $pdf->SetFont('Times','',12); 

	    //Invoice date & due date
	    $date = $invoice[0]['invoice_start_date'];
	    $due_date = $invoice[0]['invoice_end_date'];
	    $date = new DateTime($date);
	    $due_date = new DateTime($due_date);
	    $day = $this->ordinal($date->format('j'));
	    $day2 = $this->ordinal($due_date->format('j'));

	    // Invoice duration
		$interval = $date->diff($due_date);

		    $pdf->MultiCell( 0, 0, "Please find below the computation of total premium payable to us by your company for healthcare coverage for ".$day." ".$date->format("F Y")." - ".$day2." ".$due_date->format("F Y")."\n", 0, 'J', false, 1, '', '', true, 0, false, true, 0, 'T',false);
	    
		$pdf->Write(5, "", '', 0, 'L', true, 0, false, false, 0);

	    $pdf->MultiCell( 0, 0, "The computation was based on  the staff data presented to us by your organization.\n", 0, 'J', false, 1, '', '', true, 0, false, true, 0, 'T',false);

		$pdf->Write(5, "", '', 0, 'L', true, 0, false, false, 0);

		setlocale(LC_MONETARY, 'en_NG');

		// Subscription
		$client = $this->checkInvoicePrev($id);

		$sub = array();
		foreach ($client as $c) {
			array_push($sub, $c['subscription_ids']);
		}

		$subs = array_unique($sub);
		$count = array_count_values($sub);

		$toolcopy = '
			<table border="1" cellpadding="5">
				<thead>
					<tr>
						<th width="6%">S/N</th>
						<th width="25%">DESCRIPTION</th>
						<th width="20%">DURATION</th>
						<th>PREMIUM</th>
						<th width="10%">PLAN</th>
						<th width="22%">AMOUNT</th>
					</tr>
				</thead>
				<tbody>';

		$total = array();
		$x = 1;
		foreach($subs as $s){
		$subs = $this->Clients_model->get_clients_subscription($s)->result_array();
		// print_r($subs);

		$toolcopy .= '
					<tr>
						<td width="6%">'.$x.'</td>
						<td width="25%">'.$subs['0']['plan_name'].'</td>
						<td width="20%">';

			if ($interval->days >= 364)  {
				$toolcopy .= '1 Year</td>';
			}else{
				$toolcopy .= $interval->m.' months '.$interval->d.'days</td>';

			}		

		$toolcopy .= ' 
						<td>'.money_format("%!i", $subs['0']['plan_cost']).'</td>
						<td width="10%">'.$count[$s].'</td>
						<td width="22%">'.money_format("%i", ($subs['0']['plan_cost']*$count[$s])).'</td>
					</tr>';
					array_push($total, ($subs['0']['plan_cost']*$count[$s]));
					$x++;
		}

		// print_r($invoice[0]['total_discount'] != 0);die;

		if (($invoice[0]['total_discount']) == 0) {
						$toolcopy.= '
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td colspan="2"><b>TOTAL</b></td>
							<td><b>'.money_format("%i", (array_sum($total))).'</b></td>
						</tr>';
		}else{
			$toolcopy.= '
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td colspan="2"><b>Discount</b></td>
							<td><b>-'.money_format("%i", $invoice[0]['total_discount']).'</b></td>
						</tr>';

			$toolcopy.= '
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td colspan="2"><b>TOTAL </b></td>
							<td><b>'.money_format("%i", (array_sum($total)-$invoice[0]['total_discount'])).'</b></td>
						</tr>';
		}
		

		$toolcopy .= '
		
				</tbody>
			</table>
		';
		$pdf->writeHTML($toolcopy, true, 0, true, 0);

	    $pdf->SetFont('Times','',12);
		$pdf->Write(5, "Kindly pay agreed Premium to Precious Health Care Ltd as detailed hereunder:", '', 0, 'L', true, 0, false, false, 0);
		
	    $pdf->SetFont('Times','B',12);
		$pdf->Write(0, "Account Name	       : LionTech Ltd", '', 0, 'L', true, 0, false, false, 0);
		$pdf->Write(0, "Bank Name        	    : Lion Bank", '', 0, 'L', true, 0, false, false, 0);
		$pdf->Write(0, "Account Number    : 0044000801", '', 0, 'L', true, 0, false, false, 0);
	   	
	   	$pdf->SetFont('Times','',12);
		$pdf->Write(5, "", '', 0, 'L', true, 0, false, false, 0);
		$pdf->Write(5, "Do note that the total premium is exclusive of all taxes (Witholding Tax(WHT), VAT etc.", '', 0, 'L', true, 0, false, false, 0);
		$pdf->Write(5, "", '', 0, 'L', true, 0, false, false, 0);
		
		$pdf->Write(5, "Yours truly,", '', 0, 'L', true, 0, false, false, 0);
		$pdf->Write(5, "", '', 0, 'L', true, 0, false, false, 0);
	   	
	   	$pdf->SetFont('Times','B',12);
		$pdf->Write(5, "Juliet Kennedy", '', 0, 'L', true, 0, false, false, 0);
	   	$pdf->SetFont('Times','',12);
		$pdf->Write(5, "Finance &  Accounts Manager", '', 0, 'L', true, 0, false, false, 0);

	    $pdf->Output('Invoice.pdf', 'I');
	}

	public function getTotal(){
		$id = $this->input->post('id');
		//Check Invoices to get last date
		$client = $this->checkInvoiceBefore($id);

		$sub = array();
		$total = array();
		
		foreach($client as $c){
			array_push($sub,$c['subscription_ids']);		
		}

		foreach($sub as $s){
			$subs = $this->Clients_model->get_clients_subscription($s)->result_array();
			array_push($total, $subs[0]['plan_cost']);
		}

		echo array_sum($total);
	}

	public function getCPT(){
		$id = $this->input->post('id');
		
		//Check Invoices to get last date
		$client = $this->checkInvoiceBefore($id);

		$sub = array();

		foreach($client as $c){
			array_push($sub,$c['subscription_ids']);		
		}

		$subs = array_unique($sub);
		$count = array_count_values($sub);

		$html = "";
		$no=1;
		foreach ($subs as $s) {
			$subs = $this->Clients_model->get_clients_subscription($s)->result_array();
			$html .= "
				<tr>
					<td>$no</td>
					<td>".$count[$s]."</td>
					<td>".$subs[0]['plan_name']."</td>
					<td id='totalAmount'>".($count[$s]*$subs[0]['plan_cost'])."</td>
				</tr>
			";
			$no++;
		}

		// Organization name
		$name = $this->Clients_model->get_organization_info($id)->result_array();

		if (empty($subs)) {
			echo "
				<tr>
					<td colspan='4'>No data for ".$name[0]['name'].". Check the previous invoice</td>
				</tr>

			";
		}else{
			echo $html;
		}
	}

	//Check Last Invoice
	public function checkInvoiceBefore($id){
		$invoice = $this->Invoices_model->get_last_invoice($id)->result_array();

		if ($invoice) {
			$date = $invoice[0]['created_at'];
			// return 'yes';
			return $this->Clients_model->get_clients_filtered($id,$date)->result_array();
		}else{
			// return 'no';
			return $this->Clients_model->get_clients_search($id)->result_array();
		}

	}

	//Check Previous Invoice
	public function checkInvoicePrev($id){
		$invoice = $this->Invoices_model->get_invoices_info($id)->result_array();
		$date = $invoice[0]['created_at'];
		$comp_id = $invoice[0]['company_name'];
	
		$invoice_before = $this->Invoices_model->get_prev_invoice($comp_id,$date)->result_array();

		if ($invoice_before) {
			$date_before = $invoice_before[0]['created_at'];
			// return "ada yang sebelumnya";
			return $this->Clients_model->get_clients_filtered_between($comp_id,$date,$date_before)->result_array();
		}else{
			// return "tidak ada yang sebelumnya";
			return $this->Clients_model->get_clients_filtered2($comp_id,$date)->result_array();
		}

	}
} 

?>