<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the HRSALE License
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.hrsale.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to hrsalesoft@gmail.com so we can send you a copy immediately.
 *
 * @author   HRSALE
 * @author-email  hrsalesoft@gmail.com
 * @copyright  Copyright © hrsale.com. All Rights Reserved
 */

defined('BASEPATH') OR exit('No direct script access allowed');


class Clients extends MY_Controller
{


	public function __construct()
	{

		parent::__construct();

		//load the models

		$this->load->model("Clients_model");

		$this->load->model("Xin_model");
		
		$this->load->model("Training_model");

		$this->load->model("Finance_model");

		$this->load->model('Expense_model');

		$this->load->model('Invoices_model');

		$this->load->model('Employees_model');

		$this->load->model('Department_model');

	}


	/*Function to set JSON output*/

	public function output($Return = array())
	{

		/*Set response header*/

		header("Access-Control-Allow-Origin: *");

		header("Content-Type: application/json; charset=UTF-8");

		/*Final JSON response*/

		exit(json_encode($Return));

	}


	public function index()

	{

		$session = $this->session->userdata('username');

		if (empty($session)) {

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if ($system[0]->module_projects_tasks != 'true') {

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_project_clients') . ' | ' . $this->Xin_model->site_title();

		$data['all_countries'] = $this->Xin_model->get_countries();

		$data['breadcrumbs'] = $this->lang->line('xin_project_clients');

		$data['all_organization']  =  $this->Training_model->get_organization()->result();	 
		
		$data['all_subscription']  =  $this->Training_model->get_subscription()->result();	 
		
		$data['all_state']  =  $this->Training_model->get_state()->result();	

		$data['path_url'] = 'clients';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if (in_array('119', $role_resources_ids)) {

			$data['subview'] = $this->load->view("admin/clients/clients_list", $data, TRUE);

			$this->load->view('admin/layout/layout_main', $data); //page load

		} else {

			redirect('admin/dashboard');

		}

	}
	public function marketting()

	{

		$session = $this->session->userdata('username');

		if (empty($session)) {

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if ($system[0]->module_projects_tasks != 'true') {

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_project_clients') . ' | ' . $this->Xin_model->site_title();

		$data['all_countries'] = $this->Xin_model->get_countries();

		$data['breadcrumbs'] = $this->lang->line('xin_project_clients');

		$data['all_organizations'] = $this->Training_model->getAll22('xin_organization', ' 1 order by id desc');

		$data['all_subscriptions'] = $this->Training_model->getAll22('xin_subscription', ' 1 order by subscription_id desc');

		$data['path_url'] = 'clients';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if (in_array('119', $role_resources_ids)) {

			$data['subview'] = $this->load->view("admin/marketting/marketting", $data, TRUE);

			$this->load->view('admin/layout/layout_main', $data); //page load

		} else {

			redirect('admin/dashboard');

		}

	}
	public function capitation()

	{

		$session = $this->session->userdata('username');

		if (empty($session)) {

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if ($system[0]->module_projects_tasks != 'true') {

			redirect('admin/dashboard');

		}

		$data['title'] = 'NHIS Capitation' . ' | ' . $this->Xin_model->site_title();

		$data['all_countries'] = $this->Xin_model->get_countries();

		$data['breadcrumbs'] = 'NHIS Capitation';

		$data['all_organizations'] = $this->Training_model->getAll2('xin_organization', ' 1 order by id desc');

		$data['all_subscriptions'] = $this->Training_model->getAll2('xin_subscription', ' 1 order by subscription_id desc');
		
		$data['all_hospitals'] = $this->Clients_model->get_hospital_hcp()->result();

		$data['path_url'] = 'clients';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if (in_array('119', $role_resources_ids)) {

			$data['subview'] = $this->load->view("admin/clients/capitation_list", $data, TRUE);

			$this->load->view('admin/layout/layout_main', $data); //page load

		} else {

			redirect('admin/dashboard');

		}

	}


	public function clients_list()

	{
		

		$data['title'] = $this->Xin_model->site_title();

		$session = $this->session->userdata('username');

		if (!empty($session)) {

			$this->load->view("admin/clients/clients_list", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		
		$client = $this->Clients_model->get_clients();
		// print_r($client->result());die;

		$role_resources_ids = $this->Xin_model->user_role_resource();

		$data = array();


		foreach ($client->result() as $r) {

			$detail_pkg_edit = '';
			$detail_pkg = '';


			$all_ids = explode(',', $r->subscription_ids);

			if (!empty($all_ids)) {

				foreach ($all_ids as $sub) {
					$detail = $this->Training_model->getAll2('xin_subscription', ' subscription_id = "' . $sub . '" ');

					if (!empty($detail)) {

						foreach ($detail as $benifit) {
							$detail_pkg .= '<p>  ' . $benifit->plan_name . '</p>';
							$detail_pkg_edit .= '--------' . $benifit->subscription_id . '';
						}
					}
				}
			}


			if (in_array('324', $role_resources_ids)) { //edit

				$edit = '<span data-toggle="tooltip" data-placement="top" title="' . $this->lang->line('xin_edit') . '"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light edit_client_detail "  data-toggle="modal" data-target=".edit-modal-data2"  

						data-client_id = "' . $r->client_id . '"
						data-c_name    = "' . $r->name . '"
						data-c_orga    = "' . $r->company_name . '"
						data-email     = "' . $r->email . '"
						data-phone     = "' . $r->contact_number . '"
						data-status    = "' . $r->is_active . '"
						data-detail_pkg_edit    = "'. $detail_pkg_edit.'"
						data-client_photo = "'.$r->client_profile.'"
						data-c_lname = "'.$r->last_name.'"
						data-c_hospital= "'.$r->hospital_id.'"
						data-gender = "'.$r->sex.'"
						data-address = "'.$r->address_1.'"
						data-dob = "'.$r->dob.'"
						data-c_subs = "'.$r->subscription_ids.'"
						data-marital= "'.$r->marital_status.'"
						data-state = "'.$r->state.'"
						data-diseases = "'.$r->diseases.'"
						data-comment= "'.$r->disease_comment.'"
						><span class="fa fa-pencil"></span></button></span>';

			} else {

				$edit = '';

			}

			if (in_array('325', $role_resources_ids)) { // delete

				$delete = '<span data-toggle="tooltip" data-placement="top" title="' . $this->lang->line('xin_delete') . '"><button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal" data-record-id="' . $r->client_id . '"><span class="fa fa-trash"></span></button></span>';

			} else {

				$delete = '';

			}

			if (in_array('326', $role_resources_ids)) { //view

				$view = '<span data-toggle="tooltip" data-placement="top" title="' . $this->lang->line('xin_view') . '"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light" data-toggle="modal" data-target=".view-modal-data" data-client_id="' . $r->client_id . '"><span class="fa fa-eye"></span></button></span>';

			} else {

				$view = '';

			}

			if ($r->is_active != 1) {
				$status = 'Inactive';
			}else{
				$status = 'Active';
			}

			$combhr = $edit . $view . $delete;

			$company_data = $this->Training_model->getAll2('xin_organization', ' id = "' . $r->company_name . '" ');

			if (isset($company_data[0]->name)) {
				$company_name = $company_data[0]->name;
			} else {
				$company_name = '--';
			}

			$date = strtotime($r->created_at);
			$year = date('Y',$date);
			$date = date('j F Y',$date);

			// Client ID

			$client_id = str_pad($r->client_id, 3, '0', STR_PAD_LEFT);
			$client_id = "PHC/".$year."/".$client_id;

			$data[] = array(

				$combhr,

				$client_id,

				$r->name." ".$r->last_name,

				$company_name,

				$detail_pkg,

				$r->email,

				$r->contact_number,

				$status

			);

		}


		$output = array(

			"draw" => $draw,

			"recordsTotal" => $client->num_rows(),

			"recordsFiltered" => $client->num_rows(),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	}

	public function capitation_list()

	{
		

		$data['title'] = $this->Xin_model->site_title();

		$session = $this->session->userdata('username');

		if (!empty($session)) {

			$this->load->view("admin/clients/capitation_list", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		$from = $this->input->get('from');
		$to = $this->input->get('to');

		if (isset($from)) {
	
			$client = $this->Clients_model->get_capitation($from,$to);
	
		}else{

			$client = $this->Clients_model->get_capitation();

		}
		

		// print_r($client->result());die;

		$role_resources_ids = $this->Xin_model->user_role_resource();

		$user_info = $this->Xin_model->read_user_info($session['user_id']);

		$data = array();


		foreach ($client->result() as $r) {

			$hospital = $this->Training_model->getAll2('xin_hospital', ' hcp_code = "' . $r->hcp_code . '" ');
			
			$hid = $hospital[0]->hospital_id;
			if (!empty($hospital)) {
				$hospital = $hospital[0]->hospital_name;
				// $hospital_id = '-';
			}else{
				$hospital = '-';
				$hid = '-';
			}

			if (in_array('324', $role_resources_ids)) { //edit

				$edit = '<span data-toggle="tooltip" data-placement="top" title="' . $this->lang->line('xin_edit') . '"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light edit_client_detail "  data-toggle="modal" data-target=".edit-modal-data2"  
						data-capitation_id = "'.$r->capitation_id.'"
						data-id = "'.$r->id.'"
						data-hospital_id = "'.$hid.'"
						data-type = "'.$r->type.'"
						data-name = "'.$r->name.'"
						data-pname = "'.$r->principal_name.'"
						data-relation = "'.$r->relation.'"
						data-gender = "'.$r->gender.'"
						data-dob = "'.$r->dob.'"
						><span class="fa fa-pencil"></span></button></span>';

			} else {

				$edit = '';

			}

			if (in_array('325', $role_resources_ids)) { // delete

				$delete = '<span data-toggle="tooltip" data-placement="top" title="' . $this->lang->line('xin_delete') . '"><button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal" data-record-id="' . $r->id . '"><span class="fa fa-trash"></span></button></span>';

			} else {

				$delete = '';

			}

			if (in_array('326', $role_resources_ids)) { //view

				$view = '<span data-toggle="tooltip" data-placement="top" title="' . $this->lang->line('xin_view') . '"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light" data-toggle="modal" data-target=".view-modal-data" data-client_id="' . $r->client_id . '"><span class="fa fa-eye"></span></button></span>';

			} else {

				$view = '';

			}

			$combhr = $edit . $view . $delete;

			if ($r->type == 'p') {
				$type = 'principal';
				$principal_name = '-';
				$relation = '-';
			}else{
				$type = 'dependant';
				$principal_name = $r->principal_name;
				$relation = $r->relation;
			}

			if ($r->gender == 'F') {
				$gender = 'female';
			}else{
				$gender = 'male';
			}


			$dob = explode("/", $r->dob);

			$age = date_diff(date_create($dob[0]."-".$dob[1]."-".$dob[2]), date_create('today'))->y;

			if (in_array('655', $role_resources_ids) || $user_info[0]->user_role_id == 1) {

				$data[] = array(

					$combhr,

					$r->capitation_id,

					$hospital,

					ucfirst($type),

					ucfirst($r->name),

					ucfirst($principal_name),

					$relation,

					ucfirst($gender),

					$age

				);
			}else{

				$data[] = array(

					$r->capitation_id,

					$hospital,

					ucfirst($type),

					ucfirst($r->name),

					ucfirst($principal_name),

					$relation,

					ucfirst($gender),

					$age

				);
			}

		}


		$output = array(

			"draw" => $draw,

			"recordsTotal" => $client->num_rows(),

			"recordsFiltered" => $client->num_rows(),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	}


	public function client_read()

	{

		$session = $this->session->userdata('username');

		if (empty($session)) {

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		$id = $this->input->get('client_id');

		$result = $this->Clients_model->read_client_info($id);

		$data = array(

			'client_id' => $result[0]->client_id,

			'name' => $result[0]->name,

			'company_name' => $result[0]->company_name,

			'client_profile' => $result[0]->client_profile,

			'email' => $result[0]->email,

			'contact_number' => $result[0]->contact_number,

			'website_url' => $result[0]->website_url,

			'address_1' => $result[0]->address_1,

			'address_2' => $result[0]->address_2,

			'city' => $result[0]->city,

			'state' => $result[0]->state,

			'zipcode' => $result[0]->zipcode,

			'countryid' => $result[0]->country,

			'is_active' => $result[0]->is_active,

			'all_countries' => $this->Xin_model->get_countries(),

		);

		$this->load->view('admin/clients/dialog_clients', $data);

	}


	// Validate and add info in database

	public function add_client()
	{


		if ($this->input->post('add_type') == 'client') {

			// Check validation for user input

			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');

			// $this->form_validation->set_rules('website', 'Website', 'trim|required|xss_clean');

			// $this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean');


			$name = $this->input->post('name');

			$company_name = $this->input->post('company_name');

			$email = $this->input->post('email');

			$contact_number = $this->input->post('contact_number');

			$subscription = $this->input->post('subscription_ids');

			$ind_family = $this->input->post('ind_family');


			// $address_1 = $this->input->post('address_1');
			// $address_2 = $this->input->post('address_2');

			// $city = $this->input->post('city');

			// $state = $this->input->post('state');

			// $zipcode = $this->input->post('zipcode');

			// $country = $this->input->post('country');

			$user_id = $this->input->post('user_id');

			$file = $_FILES['client_photo']['tmp_name'];


			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result' => '', 'error' => '', 'csrf_hash' => '');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();


			if (empty($subscription)) {

				$Return['error'] = 'Healh Care plan is required';

			} else {
				$subs = implode(',', $subscription);
			}


			/* Server side PHP input validation */

			if ($name === '') {

				$Return['error'] = $this->lang->line('xin_error_name_field');

			} else if ($ind_family === '') {

				$Return['error'] = 'Individual or Family is required.';

			} /*else if($company_name==='') {

				$Return['error'] = $this->lang->line('xin_employee_error_company_name');

			} else if($contact_number==='') {

				$Return['error'] = $this->lang->line('xin_error_contact_field');

			}*/ else if ($email === '') {

				$Return['error'] = $this->lang->line('xin_error_cemail_field');

			} else if ($this->Xin_model->check_client_email($email) > 0) {

				$Return['error'] = $this->lang->line('xin_check_client_email_error');

			}  /* else if($zipcode==='') {

				$Return['error'] = $this->lang->line('xin_error_zipcode_field');

			}  else if($country==='') {

				$Return['error'] = $this->lang->line('xin_error_country_field');

			} /*else if($this->input->post('username')==='') {

				$Return['error'] = $this->lang->line('xin_employee_error_username');

			} else if($this->input->post('password')==='') {

				$Return['error'] = $this->lang->line('xin_employee_error_password');

			}*/


			/* Check if file uploaded..*/

			else if ($_FILES['client_photo']['size'] == 0) {

				$fname = 'no file';

				$options = array('cost' => 12);

				$password_hash = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);


				$data = array(

					'name' => $this->input->post('name'),

					'company_name' => $this->input->post('company_name'),

					'email' => $this->input->post('email'),

					'subscription_ids' => $subs,

					'client_password' => $password_hash,

					'client_profile' => '',

					'approve_code' => '0',

					'contact_number' => $this->input->post('contact_number'),

					'ind_family' => $ind_family,

					// 'website_url' => $this->input->post('website'),

					// 'address_1' => $this->input->post('address_1'),

					// 'address_2' => $this->input->post('address_2'),

					// 'city' => $this->input->post('city'),

					// 'state' => $this->input->post('state'),

					// 'zipcode' => $this->input->post('zipcode'),

					// 'country' => $this->input->post('country'),

					'is_active' => 1,

					'created_at' => date('Y-m-d H:i:s'),
				);

				$result = $this->Clients_model->add($data);

			} else {

				if (is_uploaded_file($_FILES['client_photo']['tmp_name'])) {

					//checking image type

					$allowed = array('png', 'jpg', 'jpeg', 'gif');

					$filename = $_FILES['client_photo']['name'];

					$ext = pathinfo($filename, PATHINFO_EXTENSION);


					if (in_array($ext, $allowed)) {

						$tmp_name = $_FILES["client_photo"]["tmp_name"];

						$bill_copy = "uploads/clients/";

						// basename() may prevent filesystem traversal attacks;

						// further validation/sanitation of the filename may be appropriate

						$lname = basename($_FILES["client_photo"]["name"]);

						$newfilename = 'client_photo_' . round(microtime(true)) . '.' . $ext;

						move_uploaded_file($tmp_name, $bill_copy . $newfilename);

						$fname = $newfilename;

					} else {

						$Return['error'] = $this->lang->line('xin_error_attatchment_type');

					}

				}

				$options = array('cost' => 12);

				$password_hash = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);


				$data = array(

					'name' => $this->input->post('name'),

					'company_name' => $this->input->post('company_name'),

					'email' => $this->input->post('email'),

					'ind_family' => $ind_family,

					'client_password' => $password_hash,

					'client_profile' => $fname,
					
					'approve_code' => '0',

					'contact_number' => $this->input->post('contact_number'),

					'subscription_ids' => $subs,

					// 'address_1' => $this->input->post('address_1'),

					// 'address_2' => $this->input->post('address_2'),

					// 'city' => $this->input->post('city'),

					// 'state' => $this->input->post('state'),

					// 'zipcode' => $this->input->post('zipcode'),

					// 'country' => $this->input->post('country'),

					'is_active' => 1,

					'created_at' => date('Y-m-d H:i:s'),
				);

				$result = $this->Clients_model->add($data);

			}


			if ($Return['error'] != '') {

				$this->output($Return);

			}


			if ($result == TRUE) {

				$Return['result'] = $this->lang->line('xin_project_client_added');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}


	public function import_clients()
	{


		$Return = array('result' => '', 'error' => '', 'csrf_hash' => '');

		$Return['csrf_hash'] = $this->security->get_csrf_hash();
		//validate whether uploaded file is a csv file

		$csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');


		if ($_FILES['file']['name'] === '') {

			$this->session->set_flashdata('error', 'Error! Allowed file size is 2MB. ');
			redirect($_SERVER['HTTP_REFERER']);

		} else {


			if (in_array($_FILES['file']['type'], $csvMimes)) {

				if (is_uploaded_file($_FILES['file']['tmp_name'])) {
					// check file size


					if (filesize($_FILES['file']['tmp_name']) > 2000000) {


						$this->session->set_flashdata('error', 'Error! Allowed file size is 2MB. ');
						redirect($_SERVER['HTTP_REFERER']);


					} else {
						//open uploaded csv file with read only mode

						$csvFile = fopen($_FILES['file']['tmp_name'], 'r');


						//skip first line
						fgetcsv($csvFile);


						//parse data from csv file line by line
						$count_fail = 0;
						while (($line = fgetcsv($csvFile)) !== FALSE) {
							$options = array('cost' => 12);

							$password_hash = password_hash($line[5], PASSWORD_BCRYPT, $options);

							$date = explode("/", $line[10]);

							// $check_email = $this->Clients_model->check_email($line[4]);
							
							if (count($date) > 2) {
						
								$date = $date[2]."-".$date[0]."-".$date[1];
						
							}else{
						
								$count_fail++;
								continue;
						
							}

							$data = array(

								'name' => $line[0],

								'last_name' => $line[1],

								'other_name' => $line[2],

								'company_name' => $line[3],

								'email' => $line[4],

								'client_password' => $password_hash,

								'subscription_ids' => $line[6],

								'contact_number' => $line[7],

								'ind_family' => $line[8],

								'hospital_id' => $line[9],

								'dob' => $date,

								'sex' => $line[11],

								'is_active' => 1,

								'created_at' => date('Y-m-d H:i:s'),

								'approve_code' => 1
							);

							// print_r($line);

							$result = $this->Clients_model->add($data);

							if ($result == false) {
								$count_fail++;
							}

						}

						//close opened csv file

						fclose($csvFile);

						if ($count_fail > 1) {
							$this->session->set_flashdata('success', 'Success! Data has been imported successfully with '.$count_fail.' failed to import.');
						}else{
							$this->session->set_flashdata('success', 'Success! Data has been imported successfully');
						}


						redirect($_SERVER['HTTP_REFERER']);

					}

				} else {

					$this->session->set_flashdata('error', 'Error! unable to upload file.');
					redirect($_SERVER['HTTP_REFERER']);

				}

			} else {

				$this->session->set_flashdata('error', 'Error! Invalid file format.');
				redirect($_SERVER['HTTP_REFERER']);


			}

		}


	}

	public function import_capitation()
	{


		$Return = array('result' => '', 'error' => '', 'csrf_hash' => '');

		$Return['csrf_hash'] = $this->security->get_csrf_hash();
		//validate whether uploaded file is a csv file

		$csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');


		if ($_FILES['file']['name'] === '') {

			$this->session->set_flashdata('error', 'Error! Allowed file size is 2MB. ');
			redirect($_SERVER['HTTP_REFERER']);

		} else {


			if (in_array($_FILES['file']['type'], $csvMimes)) {

				if (is_uploaded_file($_FILES['file']['tmp_name'])) {
					// check file size


					if (filesize($_FILES['file']['tmp_name']) > 2000000) {


						$this->session->set_flashdata('error', 'Error! Allowed file size is 2MB. ');
						redirect($_SERVER['HTTP_REFERER']);


					} else {
						//open uploaded csv file with read only mode

						$csvFile = fopen($_FILES['file']['tmp_name'], 'r');


						//skip first line
						fgetcsv($csvFile);


						//parse data from csv file line by line
						$count = 0;
						$extra = 0;
						while (($line = fgetcsv($csvFile)) !== FALSE) {
							$data = array(
								'hcp_code' => $line[0],

								'capitation_id' => $line[1],

								'type' => $line[2],

								'name' => $line[3],

								'principal_name' => $line[4],

								'relation' => $line[5],

								'gender' => $line[6],

								'dob' => $line[7],

								'created_at' => date('Y-m-d H:i:s'),
							);
							
							$capitation_exist = $this->check_capitation($line[1],$line[3],$line[2]);


							if ($capitation_exist != 0) {
								$result = $this->Clients_model->update_capitation_record($data,$capitation_exist);
								$count++;
							}else{

								$principal_exist = $this->principal_exist($line[1]);

								if ($principal_exist != 0) {
									// array_push($data, ('is_extra' => 1));
									$data['is_extra'] = 1;
									$extra++;
								}
								
								$result = $this->Clients_model->add_capitation($data);
								

							}	
							// echo $capitation_exist." - ".$count;continue;

						}

						// echo $count; die;

						//close opened csv file

						fclose($csvFile);

						if ($count == 0 && $extra == 0) {
							$this->session->set_flashdata('success', 'Success! Data has been imported successfully');
						}else if ($count != 0 && $extra == 0){
							$this->session->set_flashdata('success', 'Success! Data has been imported successfully with '.$count.' data replaced.');
						}else if ($count == 0 && $extra != 0){
							$this->session->set_flashdata('success', 'Success! Data has been imported successfully with '.$extra.' extra dependant added.');
						}else{
							$this->session->set_flashdata('success', 'Success! Data has been imported successfully with '.$count.' data replaced amd '.$extra.' extra dependant added.');
						}

						redirect($_SERVER['HTTP_REFERER']);

					}

				} else {

					$this->session->set_flashdata('error', 'Error! unable to upload file.');
					redirect($_SERVER['HTTP_REFERER']);

				}

			} else {

				$this->session->set_flashdata('error', 'Error! Invalid file format.');
				redirect($_SERVER['HTTP_REFERER']);


			}

		}


	}

	public function check_capitation($capitation_id,$name,$type)
	{
		$exist = $this->Clients_model->get_capitation_check($capitation_id,$name,$type)->result();

		if (!empty($exist)) {
			return $exist[0]->id;
		}else{
			return 0;
		}
	}

	public function principal_exist($capitation_id)
	{
		$exist = $this->Clients_model->get_capitation_principal($capitation_id)->result();
		if (!empty($exist)) {
			return $exist[0]->id;
		}else{
			return 0;
		}
	}


	// Validate and update info in database

	public function update()
	{
		$options = array('cost' => 12);
		if($this->input->post('password')){
			$password_hash = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);
		}else{
			$password_hash = '';
		}
		if ($this->input->post('edit_type') == 'client') {

			$id = $this->input->post('_token');

			// Check validation for user input

			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');

			// $this->form_validation->set_rules('website', 'Website', 'trim|required|xss_clean');

			// $this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean');


			$name = $this->input->post('name');

			$company_name = $this->input->post('company_name');

			$email = $this->input->post('email');

			$contact_number = $this->input->post('contact_number');

			$subscription_ids = $this->input->post('subscription_ids');

			$ind_family = $this->input->post('ind_family');

			$subs = implode(',', $subscription_ids);

			$address_1 = $this->input->post('address_1');

			$dob = $this->input->post('dob');

			$diseaseComment = $this->input->post('comment');

			$state = $this->input->post('state');

			// $zipcode = $this->input->post('zipcode');

			// $country = $this->input->post('country');


			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result' => '', 'error' => '', 'csrf_hash' => '');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();


			/* Server side PHP input validation */

			if ($name === '') {

				$Return['error'] = $this->lang->line('xin_error_name_field');

			} else if (empty($subs)) {

				$Return['error'] = 'Health Care Plan is required.';

			} else if ($ind_family === '') {

				$Return['error'] = 'Individual/Family is required.';

			}
			/* else if($email==='') {

				$Return['error'] = $this->lang->line('xin_error_cemail_field');

			}
			/*
			else if($this->Xin_model->check_client_email($email) > 1) {

				$Return['error'] = $this->lang->line('xin_check_client_email_error');

			}  else if($city==='') {

				$Return['error'] = $this->lang->line('xin_error_city_field');

			} else if($zipcode==='') {

				$Return['error'] = $this->lang->line('xin_error_zipcode_field');

			}  else if($country==='') {

				$Return['error'] = $this->lang->line('xin_error_country_field');

			} /*else if($this->input->post('username')==='') {

				$Return['error'] = $this->lang->line('xin_employee_error_username');

			}*/


			/* Check if file uploaded..*/

			else if ($_FILES['client_photo']['size'] == 0) {

				//$fname = 'no file';

				$no_logo_data = array(

					'name' => $this->input->post('name'),

					'company_name' => $this->input->post('company_name'),

					'email' => $this->input->post('email'),

					'contact_number' => $this->input->post('contact_number'),

					'subscription_ids' => $subs,

					'ind_family' => $ind_family,

					'client_password'=> $password_hash,
					'address_1' => $this->input->post('address_1'),

					'dob' => $this->input->post('dob'),

					'disease_comment' => $this->input->post('comment'),

					'state' => $this->input->post('state'),

					// 'zipcode' => $this->input->post('zipcode'),

					// 'country' => $this->input->post('country'),

					'is_active' => $this->input->post('status'),

				);
				if($password_hash == ''){
					unset($no_logo_data['client_password']);
				}
				
				$result = $this->Clients_model->update_record($no_logo_data, $id);

			} else {

				if (is_uploaded_file($_FILES['client_photo']['tmp_name'])) {

					//checking image type

					$allowed = array('png', 'jpg', 'jpeg', 'gif');

					$filename = $_FILES['client_photo']['name'];

					$ext = pathinfo($filename, PATHINFO_EXTENSION);
						
					if (in_array($ext, $allowed)) {

						$tmp_name = $_FILES["client_photo"]["tmp_name"];

						$bill_copy = "uploads/clients/";

						// basename() may prevent filesystem traversal attacks;

						// further validation/sanitation of the filename may be appropriate

						$lname = basename($_FILES["client_photo"]["name"]);

						$newfilename = 'client_photo_' . round(microtime(true)) . '.' . $ext;

						move_uploaded_file($tmp_name, $bill_copy . $newfilename);

						$fname = $newfilename;

						$data = array(

							'name' => $this->input->post('name'),

							'company_name' => $this->input->post('company_name'),

							'email' => $this->input->post('email'),

							'ind_family' => $ind_family,

							'client_profile' => $fname,

							'contact_number' => $this->input->post('contact_number'),

							'subscription_ids' => $subs,

							'client_password' => $password_hash,

							// 'website_url' => $this->input->post('website'),

							'address_1' => $this->input->post('address_1'),

							'dob' => $this->input->post('dob'),

							'disease_comment' => $this->input->post('comment'),

							'state' => $this->input->post('state'),

							// 'zipcode' => $this->input->post('zipcode'),

							// 'country' => $this->input->post('country'),

							'is_active' => $this->input->post('status'),

						);

						if($password_hash == ''){
							unset($data['client_password']);
						}
						// update record > model

						$result = $this->Clients_model->update_record($data, $id);

					} else {

						$Return['error'] = $this->lang->line('xin_error_attatchment_type');

					}

				}

			}


			if ($Return['error'] != '') {

				$this->output($Return);

			}


			if ($result == TRUE) {

				$Return['result'] = $this->lang->line('xin_project_client_updated');

				//get setting info

				$setting = $this->Xin_model->read_setting_info(1);

				$company = $this->Xin_model->read_company_setting_info(1);

				if ($setting[0]->enable_email_notification == 'xyes') {

					// load email library

					$this->load->library('email');

					$this->email->set_mailtype("html");


					//get company info

					$cinfo = $this->Xin_model->read_company_setting_info(1);

					//get email template

					$template = $this->Xin_model->read_email_template(11);


					$subject = $template[0]->subject . ' - ' . $cinfo[0]->company_name;

					$logo = base_url() . 'uploads/logo/signin/' . $company[0]->sign_in_logo;


					// get user full name

					$full_name = $this->input->post('name');


					$message = '

			<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">

			<img src="' . $logo . '" title="' . $cinfo[0]->company_name . '"><br>' . str_replace(array("{var site_name}", "{var site_url}", "{var username}", "{var email}", "{var password}"), array($cinfo[0]->company_name, site_url(), $this->input->post('username'), $this->input->post('email'), $this->input->post('password')), htmlspecialchars_decode(stripslashes($template[0]->message))) . '</div>';


					$this->email->from($cinfo[0]->email, $cinfo[0]->company_name);

					$this->email->to($this->input->post('email'));


					$this->email->subject($subject);

					$this->email->message($message);


					$this->email->send();

				}

			} else {

				$Return['error'] = $Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}

	// Validate and update info in database

	public function capitation_update()
	{


		// if ($this->input->post('edit_type') == 'client') {

			$id = $this->input->post('_token');

			// Check validation for user input

			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');

			// $this->form_validation->set_rules('website', 'Website', 'trim|required|xss_clean');

			// $this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean');


			$name = $this->input->post('name');
			$capitation_id = $this->input->post('capitation_id');
			$type = $this->input->post('type');
			$principal_name = $this->input->post('pname');
			$relation = $this->input->post('relation');
			$dob = $this->input->post('dob');
			$gender = $this->input->post('gender');
			$hospital_id = $this->input->post('hospital_id');

			

			$Return = array('result' => '', 'error' => '', 'csrf_hash' => '');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();


			/* Server side PHP input validation */

			if ($name === '') {

				$Return['error'] = $this->lang->line('xin_error_name_field');

			}
			else if ($capitation_id === '')
			{
				$Return['error'] = "Capitation id is required.";

			}

			else if ($hospital_id === '')
			{
				$Return['error'] = "Hospital is required.";

			}

			else if ($type === '')
			{
				$Return['error'] = "Type is required.";

			}

			else if ($gender === '')
			{
				$Return['error'] = "Gender is required.";

			}

			else if ($dob === '')
			{
				$Return['error'] = "DOB is required.";

			}

			if ($Return['error'] != '') {
				$this->output($Return);
			}

			$hospital = $this->Clients_model->get_hospital_info($hospital_id)->result();

			$hcp_code = $hospital[0]->hcp_code;

			if ($principal_name === '') {
				$principal_name = "-";
			}

			if ($relation === '') {
				$relation = "-";
			}


			$data = array(

				'capitation_id' => $capitation_id,

				'hcp_code' => $hcp_code,

				'type' => $type,

				'name' => $name,

				'principal_name' => $principal_name,

				'relation' => $relation,

				'gender' => $gender,

				'dob' => $dob,

			);

				// update record > model

			$result = $this->Clients_model->update_capitation_record($data, $id);


			if ($result == TRUE) {

				$Return['result'] = $this->lang->line('xin_project_client_updated');

			} else {

				$Return['error'] = $Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	// }


	public function delete()
	{


		if ($this->input->post('is_ajax') == 2) {

			$session = $this->session->userdata('username');

			if (empty($session)) {

				redirect('admin/');

			}

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result' => '', 'error' => '', 'csrf_hash' => '');

			$id = $this->uri->segment(4);

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			$result = $this->Clients_model->delete_record($id);

			if (isset($id)) {

				$Return['result'] = $this->lang->line('xin_project_client_deleted');

			} else {

				$Return['error'] = $Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

		}

	}

	public function clients_report() {
		$session = $this->session->userdata('username');

		if (empty($session)) {

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if ($system[0]->module_projects_tasks != 'true') {

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_project_clients') . ' | ' . $this->Xin_model->site_title();

		$data['all_countries'] = $this->Xin_model->get_countries();

		$data['breadcrumbs'] = $this->lang->line('xin_project_clients');

		$data['all_organizations'] = $this->Training_model->getAll2('xin_organization', ' 1 order by id desc');

		$data['all_subscriptions'] = $this->Training_model->getAll2('xin_subscription', ' 1 order by subscription_id desc');

		$data['path_url'] = 'reports_clients';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if (in_array('119', $role_resources_ids)) {

			$data['subview'] = $this->load->view("admin/clients/report_clients_list", $data, TRUE);

			$this->load->view('admin/layout/layout_main', $data); //page load

		} else {

			redirect('admin/dashboard');

		}

	}

	public function report_client_list() {
		$data['title'] = $this->Xin_model->site_title();
		$session = $this->session->userdata('username');
		if (!empty($session)) {
			$this->load->view("admin/clients/report_clients_list", $data);
		} else {
			redirect('admin/');
		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		$client = $this->Clients_model->get_clients_search($this->input->get('id'));
		$role_resources_ids = $this->Xin_model->user_role_resource();
		$data = array();
		foreach ($client->result() as $r) {
			$detail_pkg_edit = '';
			$detail_pkg = '';
			$all_ids = explode(',', $r->subscription_ids);
			if (!empty($all_ids)) {
				foreach ($all_ids as $sub) {
					$detail = $this->Training_model->getAll2('xin_subscription', ' subscription_id = "' . $sub . '" ');
					if (!empty($detail)) {
						foreach ($detail as $benifit) {
							$detail_pkg .= '<p>  ' . $benifit->plan_name . '</p>';
							$detail_pkg_edit .= '--------' . $benifit->subscription_id . '';
						}
					}
				}
			}

			if (in_array('324', $role_resources_ids)) { //edit
				$edit = '<span data-toggle="tooltip" data-placement="top" title="' . $this->lang->line('xin_edit') . '"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light edit_client_detail "  data-toggle="modal" data-target=".edit-modal-data2"
						data-client_id = "' . $r->client_id . '"
						data-c_name    = "' . $r->name . '"
						data-c_orga    = "' . $r->company_name . '"
						data-email     = "' . $r->email . '"
						data-phone     = "' . $r->contact_number . '"
						data-status    = "' . $r->is_active . '"
						data-detail_pkg_edit    = "' . $detail_pkg_edit . '"
						data-ind_family    = "' . $r->ind_family . '"
						data-client_photo = "'.$r->client_profile.'" 
						data-c_lname = "'.$r->last_name.'"
						data-c_hospital= "'.$r->hospital_id.'"
						data-gender = "'.$r->sex.'"
						data-address = "'.$r->address_1.'"
						data-dob = "'.$r->dob.'"
						data-c_subs = "'.$r->subscription_ids.'"
						data-marital = "'.$r->marital_status.'"
						data-state = "'.$r->state.'"
						data-diseases = "'.$r->diseases.'"
						data-comment= "'.$r->disease_comment.'"
						><span class="fa fa-pencil"></span></button></span>';
			} else {
				$edit = '';
			}

			if (in_array('325', $role_resources_ids)) { // delete
				$delete = '<span data-toggle="tooltip" data-placement="top" title="' . $this->lang->line('xin_delete') . '"><button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal" data-record-id="' . $r->client_id . '"><span class="fa fa-trash"></span></button></span>';
			} else {
				$delete = '';
			}

			if (in_array('326', $role_resources_ids)) { //view
				$view = '<span data-toggle="tooltip" data-placement="top" title="' . $this->lang->line('xin_view') . '"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light" data-toggle="modal" data-target=".view-modal-data" data-client_id="' . $r->client_id . '"><span class="fa fa-eye"></span></button></span>';
			} else {
				$view = '';
			}

			$combhr = $edit . $view . $delete;
			$company_data = $this->Training_model->getAll2('xin_organization', ' id = "' . $r->company_name . '" ');

			if (isset($company_data[0]->name)) {
				$company_name = $company_data[0]->name;
			} else {
				$company_name = '--';
			}

		// Created date
		$date = strtotime($r->created_at);
		$year = date('Y',$date);
		$date = date('j F Y',$date);

		// Client ID

		$client_id = str_pad($r->client_id, 3, '0', STR_PAD_LEFT);
		$client_id = "PHC/".$year."/".$client_id;

		// Hospital name
			if (isset($r->hospital_id)) {
				$hospital = $this->Clients_model->get_hospital_info($r->hospital_id)->result_array();
				if (!empty($hospital)) {
					$hospital = $hospital[0]['hospital_name'];
				}else{
					$hospital = "-";
				}
			}else{
				$hospital = "--";
			}

		//Full name

		$name = $r->name." ".$r->last_name;

		if ($r->other_name != '') {
			$name = $name."(".$r->other_name.")";
		}


		$data[] = array(
//				$combhr,
				$client_id,
				$name,
				$hospital,
				$detail_pkg,
				ucfirst($r->ind_family),
				$r->email,
				$r->contact_number,
				$date
			);
		}

		$output = array(
			"draw" => $draw,
			"recordsTotal" => $client->num_rows(),
			"recordsFiltered" => $client->num_rows(),
			"data" => $data
		);
		echo json_encode($output);
		exit();
	}

	public function hospital_reports() {	

		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = 'All Enrollees By Hospital | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = 'All Enrollees  By Hospital';

		$data['path_url'] = 'accounting_expense';

		$data['transaction_amount'] = $this->Clients_model->select_all_transaction_ids();

		$data['all_payees'] = $this->Finance_model->all_payees();

		$data['all_employees'] = $this->Xin_model->all_employees();

		$data['all_companies'] = $this->Xin_model->get_companies();

		$data['all_expense_types'] = $this->Expense_model->all_expense_types();

		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

		$data['all_income_categories_list'] = $this->Finance_model->all_income_categories_list();

		$data['get_all_payment_method'] = $this->Finance_model->get_all_payment_method();

		$data['all_hospital']   =  $this->Training_model->getAll2('xin_hospital',' 1 order by hospital_id asc');

		if($this->input->post("hospital_name")) {
			$from_date = $this->input->post('from_date');
			$to_date = $this->input->post('to_date');
			$data['hospital_id'] = $this->input->post("hospital_name");

			$data['hospital_name'] = $this->Clients_model->get_hospital_info($data['hospital_id'])->result();

			$data['from_date'] = $from_date;
			$data['to_date'] = $to_date;

			$data['all_bill_payments'] = $this->Finance_model->get_all_bill_payments();
			$data['all_clients'] = $this->Clients_model->get_all_clients_hospital_date_range_new($data['hospital_id'],$from_date,$to_date);
			// print_r($data['all_clients']->result());die;
		} else {
			$data['hospital_id'] = '';
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('595',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/clients/hospital_report", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function hospital_capitation_reports() {	

		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = 'NHIS By Hospital | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = 'NHIS  By Hospital';

		$data['path_url'] = 'accounting_expense';

		$data['all_hospital']   =  $this->Training_model->getAll2('xin_hospital',' 1 order by hospital_id asc');

		if($this->input->post("hospital_name")) {
			$data['hospital_id'] = $this->input->post("hospital_name");
			$data['hospital'] = $this->Clients_model->get_hospital_info($data['hospital_id'])->result();
			// $data['all_bill_payments'] = $this->Finance_model->get_all_bill_payments();
			$data['all_clients'] = $this->Clients_model->get_all_capitation_hospital($data['hospital'][0]->hcp_code);
			// print_r($data['all_clients']->result());die;
		} else {
			$data['hospital_id'] = '';
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('76',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/clients/hospital_capitation_report", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function full_capitation_report() {	

		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = 'NHIS Providers | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = 'NIHIS Providers';

		$data['path_url'] = 'accounting_expense';

		$data['all_hospital']   =  $this->Training_model->getAll2('xin_hospital',' 1 order by hcp_code asc');

		if($this->input->post("to")) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			$data['all_hospital']   =  $this->Clients_model->get_hospitals_filtered_between($from,$to)->result();

			// echo $this->db->last_query();die;
		} else {
			$data['hospital_id'] = '';
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('76',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/clients/full_capitation_report", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function yearly_capitation_report() {	

		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = 'NHIS Yearly Report | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = 'NHIS Yearly Report';

		$data['path_url'] = 'accounting_expense';

		$data['all_hospital']   =  $this->Training_model->getAll2('xin_hospital',' 1 order by hcp_code asc');

		$data['year_now'] = '';
		$data['from'] = '';
		$data['to'] = '';

		if($this->input->post("from")) {
			$from = $this->input->post("from");
			$to = $this->input->post("to");
			$year = $this->input->post('year');
			$data['all_hospital']   =  $this->Training_model->getAll2('xin_hospital',' 1 order by hcp_code asc');
			$data['year_now'] = $year;
			$data['from'] = $from;
			$data['to'] = $to;
			// echo $this->db->last_query();die;
		} else {
			$data['hospital_id'] = '';
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('76',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/clients/yearly_capitation_report", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function client_organization_reports() {	

		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = 'Enrollees By Organization | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = 'Enrollees By Organization';

		$data['path_url'] = 'accounting_expense';

		// $data['transaction_amount'] = $this->Clients_model->select_all_transaction_ids();

		// $data['all_payees'] = $this->Finance_model->all_payees();

		// $data['all_employees'] = $this->Xin_model->all_employees();

		// $data['all_companies'] = $this->Xin_model->get_companies();

		// $data['all_expense_types'] = $this->Expense_model->all_expense_types();

		// $data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

		// $data['all_income_categories_list'] = $this->Finance_model->all_income_categories_list();

		// $data['get_all_payment_method'] = $this->Finance_model->get_all_payment_method();

		$data['all_hospital']   =  $this->Training_model->getAll2('xin_hospital',' 1 order by hospital_id asc');

		$data['all_organizations']   =  $this->Training_model->getAll2('xin_organization',' 1 order by id asc');

		if(!is_null($this->input->post("org_id")) AND is_null($this->input->post("to_date"))) {
			$data['org_id'] = $this->input->post("org_id");
			$data['all_clients'] = $this->Clients_model->get_all_clients_organization($data['org_id'])->result();
		} else if (!is_null($this->input->post("org_id")) AND !is_null($this->input->post("to_date"))) {
			$data['org_id'] = $this->input->post("org_id");
			$data['from_date'] = $this->input->post("from_date");
			$data['to_date'] = $this->input->post("to_date");
			$data['all_clients'] = $this->Clients_model->get_all_clients_organization($data['org_id'],$data['from_date'],$data['to_date'])->result();
		} else {
			$data['hospital_id'] = '';
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('600',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/clients/client_by_organization", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function client_dependants_report() {	

		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = 'Dependants by Principals | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = 'Dependants by Principals';

		$data['path_url'] = 'accounting_expense';

		$data['transaction_amount'] = $this->Clients_model->select_all_transaction_ids();

		$data['all_hospital']   =  $this->Training_model->getAll2('xin_hospital',' 1 order by hospital_id asc');
		
		$data['all_clients']   =  $this->Training_model->getAll2('xin_clients',' 1 order by hospital_id asc');

		if($this->input->post("client_id")) {
			$data['client_id'] = $this->input->post("client_id");
			// $data['all_bill_payments'] = $this->Finance_model->get_all_bill_payments();
			// $data['all_clients'] = $this->Clients_model->get_all_clients_hospital($data['hospital_id']);
			$data['all_dependants'] = $this->Clients_model->get_all_clients_dependants($data['client_id'],'sort');

			// print_r($data['all_clients']->result());die;
		} else {
			$data['hospital_id'] = '';
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('596',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/clients/client_dependants_report", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function encounter_report() {	

		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = 'Medical History | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = 'Medical History ';

		$data['path_url'] = 'accounting_expense';

		$data['transaction_amount'] = $this->Clients_model->select_all_transaction_ids();
		
		$clients  =  $this->Training_model->getAll2('xin_clients',' 1 order by hospital_id asc');

		$dependants   =  $this->Training_model->getAll2('xin_clients_family',' 1 order by hospital_id asc');

		$data['all_clients'] = array_merge($clients,$dependants);

		if(!is_null($this->input->post("client_id")) AND is_null($this->input->post("to_date"))) {
			$data['client_id'] = $this->input->post("client_id");
			$data['client_info'] = $this->Clients_model->read_client_info($data['client_id']);
			$data['all_dependants'] = $this->Clients_model->get_all_clients_dependants($data['client_id']);
			$data['all_encounter'] = $this->Clients_model->get_clients_encounter($data['client_id']);
		} else if(!is_null($this->input->post('to_date'))){
			$data['client_id'] = $this->input->post("client_id");
			$data['client_info'] = $this->Clients_model->read_client_info($data['client_id']);
			$data['all_dependants'] = $this->Clients_model->get_all_clients_dependants($data['client_id']);
			$data['all_encounter'] = $this->Clients_model->get_clients_encounter($data['client_id'],$this->input->post('from_date'),$this->input->post('to_date'));
		}

		else {

			$data['hospital_id'] = '';
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('607',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/clients/encounter_report", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function capitation_encounter_report() {	

		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = 'NHIS Enrollees Encounter | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = 'NHIS Enrollees Encounter';

		$data['path_url'] = 'accounting_expense';

		$data['transaction_amount'] = $this->Clients_model->select_all_transaction_ids();
		
		$data['all_clients']  =  $this->Training_model->getAll2('xin_clients_capitation',' 1 order by name asc');

		if(!is_null($this->input->post("client_id")) AND is_null($this->input->post("to_date"))) {
			$data['client_id'] = $this->input->post("client_id");
			$data['client_info'] = $this->Clients_model->read_capitation_client_info($data['client_id']);
			$data['all_encounter'] = $this->Clients_model->get_capitation_clients_encounter($data['client_id']);
		} else if(!is_null($this->input->post("to_date"))){
			$data['client_id'] = $this->input->post("client_id");
			$data['client_info'] = $this->Clients_model->read_capitation_client_info($data['client_id']);
			$data['all_encounter'] = $this->Clients_model->get_capitation_clients_encounter($data['client_id'],$this->input->post('from_date'),$this->input->post('to_date'));
		}

		else {
			$data['hospital_id'] = '';
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('76',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/clients/capitation_encounter_report", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function no_usage_report() {	

		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = 'Enrollees with no Usage | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = 'Enrollees with no Usage';

		$data['path_url'] = 'accounting_expense';

		$data['transaction_amount'] = $this->Clients_model->select_all_transaction_ids();

		$clients  =  $this->Training_model->getAll2('xin_clients',' 1 order by hospital_id asc');
		$dependants  =  $this->Training_model->getAll2('xin_clients_family',' 1 order by hospital_id asc');

		$c_ids = array();
		$d_ids = array();

		foreach ($clients as $client) {
			$c_nusage = $this->Clients_model->get_all_no_usage($client->client_id,'C')->num_rows();
			$c_usage = $this->Clients_model->get_all_usage($client->client_id,'C')->num_rows();

			if ($c_usage == 0 && $c_nusage > 0) {
				array_push($c_ids, $client->client_id);
			}
		}

		foreach ($dependants as $dependant) {
			$c_nusage = $this->Clients_model->get_all_no_usage($dependant->clients_family_id,'D')->num_rows();
			$c_usage = $this->Clients_model->get_all_usage($dependant->clients_family_id,'D')->num_rows();

			if ($c_usage == 0 && $c_nusage > 0) {
				array_push($d_ids, $dependant->clients_family_id);
			}
		}

		$data['all_clients'] = $c_ids;
		$data['all_dependants'] = $d_ids;

		// $data['client_info'] = $this->Clients_model->read_client_info($data['client_id']);
		
		// $data['all_dependants'] = $this->Clients_model->get_all_clients_dependants($data['client_id']);
		
		// $data['all_encounter'] = $this->Clients_model->get_clients_encounter($data['client_id']);

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('608',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/clients/no_usage_report", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function usage_report() {	

		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = 'Enrollees with High Usage | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = 'Enrollees with High Usage';

		$data['path_url'] = 'accounting_expense';

		$data['transaction_amount'] = $this->Clients_model->select_all_transaction_ids();

		if (!is_null($this->input->post('to_date'))) {
			$data['from_date'] = $this->input->post('from_date');
			$data['to_date'] = $this->input->post('to_date');

			// $from_date = $this->input->post('from_date')." 00:00:01";
			// $to_date = $this->input->post('to_date')." 23:59:59";
			// $clients  =  $this->Training_model->getAll2('xin_clients',"1 order by hospital_id asc");
			// $dependants  =  $this->Training_model->getAll2('xin_clients_family',"1 order by hospital_id asc");
		}

		$clients  =  $this->Training_model->getAll2('xin_clients',' 1 order by hospital_id asc');
		$dependants  =  $this->Training_model->getAll2('xin_clients_family',' 1 order by hospital_id asc');
		
		$c_ids = array();
		$d_ids = array();

		foreach ($clients as $client) {
			$c_usage = $this->Clients_model->get_all_usage($client->client_id,'C')->result();
			if (!empty($c_usage)) {
				array_push($c_ids, $c_usage[0]->diagnose_client_id);
			}
		}

		foreach ($dependants as $dependant) {
			$c_usage = $this->Clients_model->get_all_usage($dependant->clients_family_id,'D')->result();
			if (!empty($c_usage)) {
				array_push($d_ids, $c_usage[0]->diagnose_client_id);
			}
		}

		$data['all_clients'] = $c_ids;
		$data['all_dependants'] = $d_ids;

		// $data['client_info'] = $this->Clients_model->read_client_info($data['client_id']);
		
		// $data['all_dependants'] = $this->Clients_model->get_all_clients_dependants($data['client_id']);
		
		// $data['all_encounter'] = $this->Clients_model->get_clients_encounter($data['client_id']);

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('609',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/clients/usage_report", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}	

	public function fetch_profile_id()
	{
		$fetched_result = array();
		$id = $this->input->post('id');

		$client = $this->Clients_model->read_client_info($id);
		$client = $client[0];
		
		$familydeatils = $this->Clients_model->get_all_clients_hospital_date_range_new_family($client->hospital_id,$client->client_id)->result();
		
		$subs = $this->Clients_model->get_clients_subscription($client->subscription_ids)->result();
		$age = date_diff(date_create($client->dob), date_create('today'))->y;

		$balance = $this->Clients_model->get_clients_encounter($client->client_id)->result();

		$expense = array();
		if (!empty($balance)) {
			foreach ($balance as $key => $value) {
				array_push($expense, $value->diagnose_total_sum);
			}
		}

		$balance = $subs[0]->plan_cost - array_sum($expense);
		// print_r($subs);die;
		$percentage = array_sum($expense)/$subs[0]->plan_cost*100;

		if (!empty($client->client_profile)) {
			$profile = site_url('uploads/clients/').$client->client_profile;
			$ada = "ada";
		}else{
			$profile = site_url('uploads/clients/user-profile.png');	
			$ada = "tidak";
		}


		$html = "
			<!--Required resources-->
			<link href=\"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\" rel=\"stylesheet\" />
				
			    <div class=\"container h-100 align-content-center\">
				<div class=\"col-md-12 row\">
				<div class=\"col-md-4\">
					<img class=\"profile-picture rounded-circle mx-auto\" src=\"".$profile."\" height=\"250\" width=\"250\" />
					
			                        <h2 class=\"text-dark h5 font-weight-bold mt-4 mb-1\">
			                            ".$client->name." ".$client->last_name."	
			                        </h2>"."
			                        <p class=\"px-1 mt-4 mb-4 text-muted quote-text\">
			                            ".$age." years old, ".$client->sex.", ".$client->marital_status."
			                        </p>";
									
			                        $html .= "<div class=\"d-flex px-1 w-100 align-items-center text-left\">
			                            <div class=\"w-100\">
			                                <label class=\"mb-1 font-weight-light text-muted small text-uppercase\">HealthCare Plan</label>
			                                <strong class=\"d-block text-warning\">
			                                    <i class=\"fa fa-star\"></i>
			                                    ".$subs[0]->plan_name."
			                                </strong>
			                            </div>
			                        </div>
           	                        <div class=\"d-flex px-1 w-100 align-items-center text-left\">
			                            <div class=\"w-100\">
			                                <label class=\"mb-1 font-weight-light text-muted small text-uppercase\">Encounter Expense</label>
			                                <strong class=\"d-block text-warning\">
			                                    <i class=\"fa fa-money\"></i>
			                                    ".array_sum($expense)."
			                                </strong>
			                            </div>
			                        </div>
              				 		<div class=\"d-flex px-1 w-100 align-items-center text-left\">
			                            <div class=\"w-100\">
			                                <label class=\"mb-1 font-weight-light text-muted small text-uppercase\">Total Balance</label>
			                                <strong class=\"d-block text-warning\">
			                                    <i class=\"fa fa-money\"></i>
			                                    ".$balance."
			                                </strong>
			                            </div>
			                        </div>
			                        <div class=\"d-flex px-1 w-100 align-items-center text-left\">
			                            <div class=\"w-100\">
			                                <label class=\"mb-1 font-weight-light text-muted small text-uppercase\">Percentage Used</label>
			                                <strong class=\"d-block text-warning\">
			                                    <i class=\"fa fa-percent\"></i>
			                                    ".number_format($percentage, 2, '.', '')."
			                                </strong>
			                            </div>
			                        </div>
					</div>
				<div class=\"col-md-6\">";
				if($familydeatils)
									{
										$html .= "<h4>Family Details</h4>";
									foreach($familydeatils as $fd)
									{
										$age1 =  date_diff(date_create($fd->dob), date_create('today'))->y;
										
										$html .="<h2 class=\"text-dark h5 font-weight-bold mt-4 mb-1\">
			                            ".$fd->name." ".$fd->last_name."	
			                        </h2>"."
			                        <p class=\"px-1 mt-4 mb-4 text-muted quote-text\">
			                            ".$age1." years old, ".$fd->sex.", ".$fd->relation."
			                        </p>";
									}
									}
				$html .="</div>
				
				</div>
			    </div>
		";


		echo $html;
	}
	public function fetch_profile_id_family()
	{
		$fetched_result = array();
		$id = $this->input->post('id');

		$client = $this->Clients_model->read_dependant_info($id);
		$client = $client[0];
		
		$client1 = $this->Clients_model->read_client_info($client->client_id);
		$client1 = $client1[0];
		
		$familydeatils = $this->Clients_model->get_all_clients_hospital_date_range_new_family($client->hospital_id,$client->client_id)->result();
		
		$subs = $this->Clients_model->get_clients_subscription($client1->subscription_ids)->result();
		$age = date_diff(date_create($client->dob), date_create('today'))->y;

		$balance = $this->Clients_model->get_clients_encounter($client1->client_id)->result();

		$expense = array();
		if (!empty($balance)) {
			foreach ($balance as $key => $value) {
				array_push($expense, $value->diagnose_total_sum);
			}
		}

		$balance = $subs[0]->plan_cost - array_sum($expense);
		// print_r($subs);die;
		$percentage = array_sum($expense)/$subs[0]->plan_cost*100;

		if (!empty($client->client_profile)) {
			$profile = site_url('uploads/clients/').$client->client_profile;
			$ada = "ada";
		}else{
			$profile = site_url('uploads/clients/user-profile.png');	
			$ada = "tidak";
		}


		$html = "
			<!--Required resources-->
			<link href=\"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\" rel=\"stylesheet\" />
				
			    <div class=\"container h-100 align-content-center\">
				<div class=\"col-md-12 row\">
				<div class=\"col-md-4\">
					<img class=\"profile-picture rounded-circle mx-auto\" src=\"".$profile."\" height=\"250\" width=\"250\" />
					
			                        <h2 class=\"text-dark h5 font-weight-bold mt-4 mb-1\">
			                            ".$client->name." ".$client->last_name."	
			                        </h2>"."
			                        <p class=\"px-1 mt-4 mb-4 text-muted quote-text\">
			                            ".$age." years old, ".$client->sex.", ".$client->relation."
			                        </p>";
									
			                        $html .= "<div class=\"d-flex px-1 w-100 align-items-center text-left\">
			                            <div class=\"w-100\">
			                                <label class=\"mb-1 font-weight-light text-muted small text-uppercase\">HealthCare Plan</label>
			                                <strong class=\"d-block text-warning\">
			                                    <i class=\"fa fa-star\"></i>
			                                    ".$subs[0]->plan_name."
			                                </strong>
			                            </div>
			                        </div>
           	                        <div class=\"d-flex px-1 w-100 align-items-center text-left\">
			                            <div class=\"w-100\">
			                                <label class=\"mb-1 font-weight-light text-muted small text-uppercase\">Encounter Expense</label>
			                                <strong class=\"d-block text-warning\">
			                                    <i class=\"fa fa-money\"></i>
			                                    ".array_sum($expense)."
			                                </strong>
			                            </div>
			                        </div>
              				 		<div class=\"d-flex px-1 w-100 align-items-center text-left\">
			                            <div class=\"w-100\">
			                                <label class=\"mb-1 font-weight-light text-muted small text-uppercase\">Total Balance</label>
			                                <strong class=\"d-block text-warning\">
			                                    <i class=\"fa fa-money\"></i>
			                                    ".$balance."
			                                </strong>
			                            </div>
			                        </div>
			                        <div class=\"d-flex px-1 w-100 align-items-center text-left\">
			                            <div class=\"w-100\">
			                                <label class=\"mb-1 font-weight-light text-muted small text-uppercase\">Percentage Used</label>
			                                <strong class=\"d-block text-warning\">
			                                    <i class=\"fa fa-percent\"></i>
			                                    ".number_format($percentage, 2, '.', '')."
			                                </strong>
			                            </div>
			                        </div>
					</div>
				<div class=\"col-md-6\">";
				if($familydeatils)
									{
										$html .= "<h4>Family Details</h4>";
										$kk =1;
									foreach($familydeatils as $fd)
									{
										if($kk ==1){
											$age1 =  date_diff(date_create($client1->dob), date_create('today'))->y;
										
											$html .="<h2 class=\"text-dark h5 font-weight-bold mt-4 mb-1\">
											".$client1->name." ".$client1->last_name."	
											</h2>"."
											<p class=\"px-1 mt-4 mb-4 text-muted quote-text\">
												".$age1." years old, ".$client1->sex.", PRINCIPAL
											</p>";
										}
										$kk++;
										if($fd->clients_family_id == $client->clients_family_id)
										{
											
											
										}else{
										$age1 =  date_diff(date_create($fd->dob), date_create('today'))->y;
										
										$html .="<h2 class=\"text-dark h5 font-weight-bold mt-4 mb-1\">
			                            ".$fd->name." ".$fd->last_name."	
										</h2>"."
										<p class=\"px-1 mt-4 mb-4 text-muted quote-text\">
											".$age1." years old, ".$fd->sex.", ".$fd->relation."
										</p>";
										}
									}
									}
				$html .="</div>
				
				</div>
			    </div>
		";


		echo $html;
	}
	public function request_code()

	{		

		if(isset($_GET['bill_request']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];
			$code = "";

			$iresult = $this->Clients_model->update_diagnose_status_record('1',$code,$id);
			redirect($_SERVER['HTTP_REFERER']);
		}


		$data['title'] = 'LionTech HMO Software';
		$var = $this->session->userdata;

		// $this->load->view('hospital/clients/dependant_list', $data);	
		$data['all_hospital_drugs']   =  $this->Training_model->getAll2('xin_hospital_drugs',' hospital_id ='.$var['hospital_id']['hospital_id'].'    ');

		$data['xin_services_hospital']   =  $this->Training_model->getAll2('xin_services_hospital',' hospital_id ='.$var['hospital_id']['hospital_id'].'    ');
		$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients($var['hospital_id']['hospital_id'],'');
		$data['query'] = $this->db->last_query();
		$data['clients']   =  $this->Training_model->getAll2('xin_clients',' 1 order by client_id asc');
		$data['subview'] = $this->load->view("hospital/client/index", $data, TRUE);

		$this->load->view('hospital/layout/layout_main', $data); //page load

	}

	public function insert_clients_diagnose()
	{
		// echo "Kennedy Job here data<br />";

		$type = "";
		$services_ids = array();
		$drugs_ids = array();
		$total = 0;
		$var = $this->session->userdata;

		$services = $this->input->post('diagnose_services');
		$drugs = $this->input->post('diagnose_drugs');

		foreach ($services as $key => $value) {
			preg_match('/^(.*\[)(.*)(\])/', $value, $match);
			$arr = preg_split('/\[.*?\]/',$value);
			// echo "Clienttt = ".$arr[0]."<br />";
			array_push($services_ids,$arr[0]);
			$total = $total + $match[2];
			// echo "Clienttt = ".$match[2]."<br />";
			// print_r($arr);
		}
		foreach ($drugs as $key => $value) {
			preg_match('/^(.*\[)(.*)(\])/', $value, $match);
			$arr = preg_split('/\[.*?\]/',$value);
			// echo "Clienttt = ".$arr[0]."<br />";
			array_push($drugs_ids,$arr[0]);
			$total = $total + $match[2];
			// echo "Clienttt = ".$match[2]."<br />";
		}

		// print_r($services_ids);
		// print_r($drugs_ids);
		// echo "Total ww= ".$total."<br />";
		// die;
		$arr = explode(":", $this->input->post('diagnose_client'));
		$arr2 = explode("-", $arr[0]);
		$n = count($arr2);
		if($n == 3) { $type = "C"; } else { $type = "D"; }

		$data = array(
			'diagnose_hospital_id'         => $var['hospital_id']['hospital_id'],
			'diagnose_client_id'         => $arr[1],
			'diagnose_user_type'         => $type,
			'diagnose_date'         => Date('Y-m-d'),
			'diagnose_diagnose'         => $this->input->post('diagnose_diagnose'),
			'diagnose_procedure'         => $this->input->post('diagnose_procedure'),
			'diagnose_investigation'         => $this->input->post('diagnose_investigation'),
			'diagnose_medical'         => $this->input->post('diagnose_medical'),
			'diagnose_date_of_birth'         => $this->input->post('diagnose_date_of_birth'),
			'diagnose_total_sum'         => $total,
			'diagnose_status'         => '1'
		);

		$result = $this->Clients_model->add_diagnose_client($data,$services_ids,$drugs_ids);
		redirect($_SERVER['HTTP_REFERER']);
		// die;
	}

}

