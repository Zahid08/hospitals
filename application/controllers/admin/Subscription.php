<?php

/**

 * NOTICE OF LICENSE

 *

 * This source file is subject to the HRSALE License

 * that is bundled with this package in the file license.txt.

 * It is also available through the world-wide-web at this URL:

 * http://www.hrsale.com/license.txt

 * If you did not receive a copy of the license and are unable to

 * obtain it through the world-wide-web, please send an email

 * to hrsalesoft@gmail.com so we can send you a copy immediately.

 *

 * @author   HRSALE

 * @author-email  hrsalesoft@gmail.com

 * @copyright  Copyright © hrsale.com. All Rights Reserved

 */

defined('BASEPATH') OR exit('No direct script access allowed');



class Subscription extends MY_Controller {

	

	 public function __construct() {

        parent::__construct();

		//load the model

		$this->load->model("Training_model");

		$this->load->model("Xin_model");

		$this->load->model("Trainers_model");

		$this->load->model("Designation_model");

		$this->load->model("Department_model");

		$this->load->model("Custom_fields_model");

	}

	

	/*Function to set JSON output*/

	public function output($Return=array()){

		/*Set response header*/

		header("Access-Control-Allow-Origin: *");

		header("Content-Type: application/json; charset=UTF-8");

		/*Final JSON response*/

		exit(json_encode($Return));

	}

	

	public function index() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] =  'Health Care Plans | '.$this->Xin_model->site_title();

		$data['all_bands']      =  $this->Training_model->getAll2('xin_bands',' 1 order by band_id desc');


		$data['all_subscriptions']  =  $this->Training_model->getAll2('xin_subscription',' 1 order by subscription_id desc');
		$subsription_array =  $this->Training_model->getAllSub('xin_subscription_detail',' 1 order by subscription_detail_id desc');
		$data['all_subscriptions_details_list']  = $subsription_array;

		$data['breadcrumbs']    =  'Health Care Plans';

		$data['path_url']       =  'training';

		 

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('619',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/subscription/subscriptions_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }

 
 

	public function add_subscription() 
	{ 
		 
		if($this->input->post('add_type')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		 		

			/* Server side PHP input validation */

			$plan_name      = $this->input->post('plan_name');
			$plan_cost      = $this->input->post('plan_cost'); 
			$benifit        = $this->input->post('benifit'); 
			$band_id        = $this->input->post('band_id'); 
			$created_on     = date("Y-m-d h:i:s");

	
			if($this->input->post('plan_name')==='') {

	        	$Return['error'] = 'Plan name is required';

			} else if($this->input->post('plan_cost')==='') {

	        	$Return['error'] = 'Plan cost is required';

			}  else if($this->input->post('band_id')==='') {

	        	$Return['error'] = 'Band is required';

			}  

				

			if($Return['error']!=''){

	       		$this->output($Return);

	    	}
 
	    	$band_ids = implode(',', $band_id );
			$data = array( 
				'plan_name'    =>  $plan_name,
				'plan_cost'    =>  $plan_cost,
				'band_types'   =>  $band_ids,
				'created_on'   =>  $created_on, 
			); 

			$iresult = $this->Training_model->insertDataTB('xin_subscription',$data);

			 
			foreach ($benifit as $value) 
			{
				$data = array( 
					'subscription_benifit'            => $value,
					'subscription_detail_master_id'   => $iresult, 
				); 
				$this->Training_model->insertDataTB('xin_subscription_detail',$data);
			}

			if ($iresult) { 
				$Return['result'] = 'Health Care Plan has been added successfully.';	 
			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}


	public function update_subscription() 
	{ 
		 
		if($this->input->post('plan_name')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		 		

			/* Server side PHP input validation */

			$plan_name      = $this->input->post('plan_name');
			$plan_cost      = $this->input->post('plan_cost'); 
			$benifit        = $this->input->post('benifit'); 
			$subs_id        = $this->input->post('subs_id');
			$band_id        = $this->input->post('band_id'); 
			$created_on     = date("Y-m-d h:i:s");

	
			if($this->input->post('plan_name')==='') {

	        	$Return['error'] = 'Plan name is required';

			} else if($this->input->post('plan_cost')==='') {

	        	$Return['error'] = 'Plan cost is required';

			}  else if($this->input->post('band_id')==='') {

	        	$Return['error'] = 'Band is required';

			}  

				

			if($Return['error']!=''){

	       		$this->output($Return);

	    	}
 
	    	$band_ids = implode(',', $band_id );
	    	
			$data = array( 
				'plan_name'    =>  $plan_name,
				'plan_cost'    =>  $plan_cost,
				'band_types'   =>  $band_ids,
				'created_on'   =>  $created_on, 
			); 

			$iresult = $this->Training_model->update2('xin_subscription',' subscription_id='.$subs_id.' ',$data);

			$this->Training_model->delete2('xin_subscription_detail',' subscription_detail_master_id = '.$subs_id.' '); 
			 
			foreach ($benifit as $value) 
			{
				$data = array( 
					'subscription_benifit'            => $value,
					'subscription_detail_master_id'   => $subs_id, 
				); 
				$this->Training_model->insertDataTB('xin_subscription_detail',$data);
			}

			if ($iresult) { 
				$Return['result'] = 'Health Care Plan has been updated successfully.';	 
			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}

	

	// // Validate and update info in database

	// public function update() {

	

	// 	if($this->input->post('edit_type')=='training') {

			

	// 	$id = $this->uri->segment(4);

		

	// 	/* Define return | here result is used to return user data and error for error message */

	// 	$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

	// 	$Return['csrf_hash'] = $this->security->get_csrf_hash();

			

	// 	/* Server side PHP input validation */

	// 	$start_date = $this->input->post('start_date');

	// 	$end_date = $this->input->post('end_date');

	// 	$description = $this->input->post('description');

	// 	$st_date = strtotime($start_date);

	// 	$ed_date = strtotime($end_date);

	// 	$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);

		

	// 	if($this->input->post('company')==='') {

 //        	$Return['error'] = $this->lang->line('error_company_field');

	// 	} else if($this->input->post('training_type')==='') {

 //        	$Return['error'] = $this->lang->line('xin_error_training_type');

	// 	} else if($this->input->post('trainer')==='') {

	// 		$Return['error'] = $this->lang->line('xin_error_trainer_field');

	// 	} else if($this->input->post('start_date')==='') {

	// 		$Return['error'] = $this->lang->line('xin_error_start_date');

	// 	} else if($this->input->post('end_date')==='') {

	// 		$Return['error'] = $this->lang->line('xin_error_end_date');

	// 	} else if($st_date > $ed_date) {

	// 		$Return['error'] = $this->lang->line('xin_error_start_end_date');

	// 	}

				

	// 	if($Return['error']!=''){

 //       		$this->output($Return);

 //    	}

		

	// 	if(isset($_POST['employee_id'])) {

	// 		$employee_ids = implode(',',$_POST['employee_id']);

	// 		$employee_id = $employee_ids;

	// 	} else {

	// 		$employee_id = '';

	// 	}

	// 	$module_attributes = $this->Custom_fields_model->training_hrsale_module_attributes();

	// 	$count_module_attributes = $this->Custom_fields_model->count_training_module_attributes();	

	// 	$i=1;

	// 	if($count_module_attributes > 0){

	// 		 foreach($module_attributes as $mattribute) {

	// 			 if($mattribute->validation == 1){

	// 				 if($i!=1) {

	// 				 } else if($this->input->post($mattribute->attribute)=='') {

	// 					$Return['error'] = $this->lang->line('xin_hrsale_custom_field_the').' '.$mattribute->attribute_label.' '.$this->lang->line('xin_hrsale_custom_field_is_required');

	// 				}

	// 			 }

	// 		 }		

	// 		 if($Return['error']!=''){

	// 			$this->output($Return);

	// 		}	

	// 	}

	// 	$data = array(

	// 	'training_type_id' => $this->input->post('training_type'),

	// 	'company_id' => $this->input->post('company'),

	// 	'trainer_id' => $this->input->post('trainer'),

	// 	'training_cost' => $this->input->post('training_cost'),

	// 	'start_date' => $this->input->post('start_date'),

	// 	'finish_date' => $this->input->post('end_date'),

	// 	'employee_id' => $employee_id,

	// 	'description' => $qt_description

	// 	);

		

	// 	$result = $this->Training_model->update_record($data,$id);		

		

	// 	if ($result == TRUE) {

	// 		$Return['result'] = $this->lang->line('xin_success_training_updated');

	// 		if($count_module_attributes > 0){

	// 		foreach($module_attributes as $mattribute) {

				

	// 			//

	// 			$count_exist_values = $this->Custom_fields_model->count_module_attributes_values($id,$mattribute->custom_field_id);

	// 			if($count_exist_values > 0){

	// 				if($mattribute->attribute_type == 'fileupload'){

	// 					if($_FILES[$mattribute->attribute]['size'] != 0) {

	// 						if(is_uploaded_file($_FILES[$mattribute->attribute]['tmp_name'])) {

	// 						//checking image type

	// 							$allowed =  array('png','jpg','jpeg','pdf','gif','xls','doc','xlsx','docx');

	// 							$filename = $_FILES[$mattribute->attribute]['name'];

	// 							$ext = pathinfo($filename, PATHINFO_EXTENSION);

								

	// 							if(in_array($ext,$allowed)){

	// 								$tmp_name = $_FILES[$mattribute->attribute]["tmp_name"];

	// 								$profile = "uploads/custom_files/";

	// 								$set_img = base_url()."uploads/custom_files/";

	// 								// basename() may prevent filesystem traversal attacks;

	// 								// further validation/sanitation of the filename may be appropriate

	// 								$name = basename($_FILES[$mattribute->attribute]["name"]);

	// 								$newfilename = 'custom_file_'.round(microtime(true)).'.'.$ext;

	// 								move_uploaded_file($tmp_name, $profile.$newfilename);

	// 								$fname = $newfilename;	

	// 							}

	// 							$iattr_data = array(

	// 								'attribute_value' => $fname

	// 							);

	// 							$this->Custom_fields_model->update_att_record($iattr_data, $id,$mattribute->custom_field_id);

	// 						}

							

	// 					} else {

	// 					}

	// 				} else if($mattribute->attribute_type == 'multiselect') {

	// 					$multisel_val = $this->input->post($mattribute->attribute);

	// 					if(!empty($multisel_val)){

	// 						$newdata = implode(',', $this->input->post($mattribute->attribute));

	// 						$iattr_data = array(

	// 							'attribute_value' => $newdata,

	// 						);

	// 						$this->Custom_fields_model->update_att_record($iattr_data, $id,$mattribute->custom_field_id);

	// 					}

	// 				} else {

	// 					$attr_data = array(

	// 						'attribute_value' => $this->input->post($mattribute->attribute),

	// 					);

	// 					$this->Custom_fields_model->update_att_record($attr_data, $id,$mattribute->custom_field_id);

	// 				}

					

	// 			} else {

	// 				if($mattribute->attribute_type == 'fileupload'){

	// 					if($_FILES[$mattribute->attribute]['size'] != 0) {

	// 						if(is_uploaded_file($_FILES[$mattribute->attribute]['tmp_name'])) {

	// 						//checking image type

	// 							$allowed =  array('png','jpg','jpeg','pdf','gif','xls','doc','xlsx','docx');

	// 							$filename = $_FILES[$mattribute->attribute]['name'];

	// 							$ext = pathinfo($filename, PATHINFO_EXTENSION);

								

	// 							if(in_array($ext,$allowed)){

	// 								$tmp_name = $_FILES[$mattribute->attribute]["tmp_name"];

	// 								$profile = "uploads/custom_files/";

	// 								$set_img = base_url()."uploads/custom_files/";

	// 								// basename() may prevent filesystem traversal attacks;

	// 								// further validation/sanitation of the filename may be appropriate

	// 								$name = basename($_FILES[$mattribute->attribute]["name"]);

	// 								$newfilename = 'custom_file_'.round(microtime(true)).'.'.$ext;

	// 								move_uploaded_file($tmp_name, $profile.$newfilename);

	// 								$fname = $newfilename;	

	// 							}

	// 							$iattr_data = array(

	// 								'user_id' => $id,

	// 								'module_attributes_id' => $mattribute->custom_field_id,

	// 								'attribute_value' => $fname,

	// 								'created_at' => date('Y-m-d h:i:s')

	// 							);

	// 							$this->Custom_fields_model->add_values($iattr_data);

	// 						}

	// 					} else {

	// 						if($this->input->post($mattribute->attribute) == ''){

	// 							$file_val = '';

	// 						} else {

	// 							$file_val = $this->input->post($mattribute->attribute);

	// 						}

	// 						$iattr_data = array(

	// 							'user_id' => $id,

	// 							'module_attributes_id' => $mattribute->custom_field_id,

	// 							'created_at' => date('Y-m-d h:i:s')

	// 						);

	// 						$this->Custom_fields_model->add_values($iattr_data);

	// 					}

	// 				} else if($mattribute->attribute_type == 'multiselect') {

	// 					$multisel_val = $this->input->post($mattribute->attribute);

	// 					if(!empty($multisel_val)){

	// 						$newdata = implode(',', $this->input->post($mattribute->attribute));

	// 						$iattr_data = array(

	// 							'user_id' => $id,

	// 							'module_attributes_id' => $mattribute->custom_field_id,

	// 							'attribute_value' => $newdata,

	// 							'created_at' => date('Y-m-d h:i:s')

	// 						);

	// 						$this->Custom_fields_model->add_values($iattr_data);

	// 					}

	// 				} else {

	// 						if($this->input->post($mattribute->attribute) == ''){

	// 							$file_val = '';

	// 						} else {

	// 							$file_val = $this->input->post($mattribute->attribute);

	// 						}

	// 						$iattr_data = array(

	// 							'user_id' => $id,

	// 							'module_attributes_id' => $mattribute->custom_field_id,

	// 							'attribute_value' => $file_val,

	// 							'created_at' => date('Y-m-d h:i:s')

	// 						);

	// 					$this->Custom_fields_model->add_values($iattr_data);

	// 				}

	// 			}

	// 		 }

	// 	}

	// 	} else {

	// 		$Return['error'] = $this->lang->line('xin_error_msg');

	// 	}

	// 	$this->output($Return);

	// 	exit;

	// 	}

	// }

	

	// // training details

	// public function details()

 //     {

	// 	$session = $this->session->userdata('username');

	// 	if(empty($session)){ 

	// 		redirect('admin/');

	// 	}

	// 	$data['title'] = $this->Xin_model->site_title();

	// 	$id = $this->uri->segment(4);

	// 	$result = $this->Training_model->read_training_information($id);

	// 	if(is_null($result)){

	// 		redirect('admin/training');

	// 	}

	// 	// get training type

	// 	$type = $this->Training_model->read_training_type_information($result[0]->training_type_id);

	// 	if(!is_null($type)){

	// 		$itype = $type[0]->type;

	// 	} else {

	// 		$itype = '--';	

	// 	}

	// 	// get trainer

	// 	$trainer = $this->Trainers_model->read_trainer_information($result[0]->trainer_id);

	// 	// trainer full name

	// 	if(!is_null($trainer)){

	// 		$trainer_name = $trainer[0]->first_name.' '.$trainer[0]->last_name;

	// 	} else {

	// 		$trainer_name = '--';	

	// 	}

	// 	$data = array(

	// 			'title' => $this->Xin_model->site_title(),

	// 			'training_id' => $result[0]->training_id,

	// 			'company_id' => $result[0]->company_id,

	// 			'type' => $itype,

	// 			'trainer_name' => $trainer_name,

	// 			'training_cost' => $result[0]->training_cost,

	// 			'start_date' => $result[0]->start_date,

	// 			'finish_date' => $result[0]->finish_date,

	// 			'created_at' => $result[0]->created_at,

	// 			'description' => $result[0]->description,

	// 			'performance' => $result[0]->performance,

	// 			'training_status' => $result[0]->training_status,

	// 			'remarks' => $result[0]->remarks,

	// 			'employee_id' => $result[0]->employee_id,

	// 			'all_employees' => $this->Xin_model->all_employees(),

	// 			'all_companies' => $this->Xin_model->get_companies()

	// 			);

	// 	$data['breadcrumbs'] = $this->lang->line('xin_training_details');

	// 	$data['path_url'] = 'training_details';

	// 	$role_resources_ids = $this->Xin_model->user_role_resource();

	// 	if(in_array('54',$role_resources_ids)) {

	// 		if(!empty($session)){ 

	// 			$data['subview'] = $this->load->view("admin/training/training_details", $data, TRUE);

	// 			$this->load->view('admin/layout/layout_main', $data); //page load

	// 		} else {

	// 			redirect('admin/');

	// 		}

	// 	} else {

	// 		redirect('admin/dashboard');

	// 	}		  

 //     }

	 

	//  // Validate and update info in database

	// public function update_status() {

	

	// 	if($this->input->post('edit_type')=='update_status') {

			

	// 		$id = $this->input->post('token_status');

	// 		/* Define return | here result is used to return user data and error for error message */

	// 		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

	// 		$Return['csrf_hash'] = $this->security->get_csrf_hash();

		

	// 		$data = array(

	// 		'performance' => $this->input->post('performance'),

	// 		'training_status' => $this->input->post('status'),

	// 		'remarks' => $this->input->post('remarks')

	// 		);

			

	// 		$result = $this->Training_model->update_status($data,$id);		

			

	// 		if ($result == TRUE) {

	// 			$Return['result'] = $this->lang->line('xin_success_training_status_updated');

	// 		} else {

	// 			$Return['error'] = $this->lang->line('xin_error_msg');

	// 		}

	// 		$this->output($Return);

	// 		exit;

	// 	}

	// }

	

	public function delete() {

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$id = $_REQUEST['_token'];
		 
		$Return['csrf_hash'] = $this->security->get_csrf_hash();

		$result = $this->Training_model->delete2('xin_subscription',' subscription_id = '.$id.' ');
		$result = $this->Training_model->delete2('xin_subscription_detail',' subscription_detail_master_id = '.$id.' '); 
		if(isset($id)) {

			$Return['result'] = 'Subscription has been deleted successfully';

		} else {

			$Return['error'] = $this->lang->line('xin_error_msg');

		}

		$this->output($Return);

	}

}

