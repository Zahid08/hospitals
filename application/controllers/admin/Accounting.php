<?php

 /**

 * NOTICE OF LICENSE

 *

 * This source file is subject to the HRSALE License

 * that is bundled with this package in the file license.txt.

 * It is also available through the world-wide-web at this URL:

 * http://www.hrsale.com/license.txt

 * If you did not receive a copy of the license and are unable to

 * obtain it through the world-wide-web, please send an email

 * to hrsalesoft@gmail.com so we can send you a copy immediately.

 *

 * @author   HRSALE

 * @author-email  hrsalesoft@gmail.com

 * @copyright  Copyright © hrsale.com. All Rights Reserved

 */

 if ( ! defined('BASEPATH')) exit('No direct script access allowed');



 class Accounting extends MY_Controller

 {



 	/*Function to set JSON output*/

 	public function output($Return=array()){

 		/*Set response header*/

 		header("Access-Control-Allow-Origin: *");

 		header("Content-Type: application/json; charset=UTF-8");

 		/*Final JSON response*/

 		exit(json_encode($Return));

 	}



 	public function __construct() {

 		parent::__construct();

          //load the models

 		$this->load->model('Xin_model');

 		$this->load->model('Finance_model');

 		$this->load->model('Expense_model');

 		$this->load->model('Invoices_model');

 		$this->load->model('Employees_model');

 		$this->load->model('Department_model');

 		$this->load->model('Training_model');

 		$this->load->model('Clients_model');

 	}



 	public function deposit() {



 		$session = $this->session->userdata('username');

 		if(empty($session)){

 			redirect('admin/');

 		}

 		$system = $this->Xin_model->read_setting_info(1);

 		if($system[0]->module_accounting!='true'){

 			redirect('admin/dashboard');

 		}

 		$data['title'] = $this->lang->line('xin_acc_deposit').' | '.$this->Xin_model->site_title();

 		$data['breadcrumbs'] = $this->lang->line('xin_acc_deposit');

 		$data['path_url'] = 'accounting_deposit';

 		$data['all_companies'] = $this->Xin_model->get_companies();

 		$data['all_payers'] = $this->Finance_model->all_payers();

 		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

 		$data['all_income_categories_list'] = $this->Xin_model->get_deposit_type()->result();

 		$data['get_all_payment_method'] = $this->Finance_model->get_all_payment_method();

 		$data['sub_deposit'] = $this->Xin_model->get_sub_deposit_type()->result();

 		$role_resources_ids = $this->Xin_model->user_role_resource();

 		if(in_array('75',$role_resources_ids)) {

 			if(!empty($session)){ 

 				$data['subview'] = $this->load->view("admin/accounting/deposit_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function deposit_multiple() {

 		$session = $this->session->userdata('username');

 		if(empty($session)){

 			redirect('admin/');

 		}

 		$system = $this->Xin_model->read_setting_info(1);

 		if($system[0]->module_accounting!='true'){

 			redirect('admin/dashboard');

 		}

 		$data['title'] = $this->lang->line('xin_acc_deposit').' | '.$this->Xin_model->site_title();

 		$data['breadcrumbs'] = $this->lang->line('xin_acc_deposit');

 		$data['path_url'] = 'accounting_deposit2';

 		$data['all_companies'] = $this->Xin_model->get_companies();

 		$data['all_payers'] = $this->Finance_model->all_payers();

 		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

 		$data['all_income_categories_list'] = $this->Xin_model->get_deposit_type()->result();

 		$data['get_all_payment_method'] = $this->Finance_model->get_all_payment_method();

 		$data['sub_deposit'] = $this->Xin_model->get_sub_deposit_type()->result();

 		$data['inputs'] = $this->input->post('inputs');

 		$role_resources_ids = $this->Xin_model->user_role_resource();

 		if(in_array('75',$role_resources_ids)) {

 			if(!empty($session)){ 

 				$data['subview'] = $this->load->view("admin/accounting/deposit_list_multiple", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function bill_payment_requests()
	{

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		if(isset($_GET['approve']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];
			$last_code = $this->Clients_model->get_last_tcid_code();
			$code = $last_code + 1;

			$iresult = $this->Clients_model->update_finance_bill_status('2',$code,$id);

			$this->session->set_flashdata('success','Bill Payment Approved successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}

		if(isset($_GET['approve_all']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];
			$from = $_GET['from'];
			$to = $_GET['to'];


			$all_clients = $this->Clients_model->all_clients_of_individual_hospital($hospital_id,$from,$to)->result();
			
			if(!empty($all_clients)) {
				$bulk = $this->Clients_model->get_bulk()->result();

				if (!empty($bulk)) {
					$bulk = end($bulk)->id;
				}else{
					$bulk = 1;
				}

				$ids = array();
				foreach ($all_clients as $client) {
					$last_code = $this->Clients_model->get_last_tcid_code();
					$code = $last_code + 1;
					
					$id = $client->diagnose_id;
					array_push($ids, $id);
					$iresult = $this->Clients_model->update_finance_bill_status_bulk('2',$code,$id,$bulk);
				}

				$ids = implode(",", $ids);

				$add_bulk = $this->Clients_model->add_bulk($ids);

				// echo "sudah";die;

				$this->session->set_flashdata('success','Bill updated successfully.');
				redirect($_SERVER['HTTP_REFERER']);
			} else {
				$this->session->set_flashdata('success','Bill has been approved already');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}

		$data['title'] =  'Bill Payment Requests | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');

		$data['all_hospital']   =  $this->Training_model->getAll2('xin_hospital',' 1 order by hospital_id asc');

		$data['breadcrumbs'] = 'Bill Payment Requests';

		$data['path_url'] = 'training';
		$nothing = "";
		if($this->input->post('from_date')) {
			$hid = $this->input->post('hospital_id');
			$from = $this->input->post('from_date');
			$to = $this->input->post('to_date');
			$data['xin_diagnose_clients'] = $this->Clients_model->get_filter_result_of_hospital($hid,$from,$to);
			// echo $this->db->last_query();die;
			$data['from'] = "$from";
			$data['to'] = "$to"; 
			$data['hid'] = "$hid"; 

			$data['query'] = $this->db->last_query();
		}
		// else {
		// 	$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients($nothing,'');
		// }

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('575',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/bill_payment_requests", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function payment_schedule()
	{

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] =  'Payment Schedule | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');

		$data['all_hospital']   =  $this->Training_model->getAll2('xin_hospital',' 1 order by hospital_id asc');

		$data['breadcrumbs'] = 'Payment Schedule';

		$data['path_url'] = 'training';


		if($this->input->post('to_date')) {
			$from = $this->input->post('from_date');
			$to = $this->input->post('to_date');
			$data['xin_diagnose_clients'] = $this->Clients_model->view_all_individual_hospital_diagnose_clients($from,$to);
		}else{
			$data['xin_diagnose_clients'] = $this->Clients_model->view_all_individual_hospital_diagnose_clients();
		}

		// echo $this->db->last_query();die;
		
		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('636',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/payment_schedule", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function all_bills_payments() 
	{

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] =  'All Bills Payments | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');

		$data['breadcrumbs'] = 'All Bill Payments Approved';

		$data['path_url'] = 'training';
		$nothing = "";
		if($this->input->post('from_date')) {
			$from = $this->input->post('from_date');
			$to = $this->input->post('to_date');
			$data['xin_diagnose_clients'] = $this->Clients_model->get_filter_result_transaction_approved($from,$to);
			$data['query'] = $this->db->last_query();
		} else {
			$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients_transaction_approved($nothing,'');
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('579',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/all_bills_payments", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}
	

	public function transfer() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_acc_transfer').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_acc_transfer');

		$data['path_url'] = 'accounting_transfer';

		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

		$data['all_income_categories_list'] = $this->Finance_model->all_income_categories_list();

		$data['get_all_payment_method'] = $this->Finance_model->get_all_payment_method();

		$data['all_expense_ledger'] = $this->Finance_model->get_all_expense_type();

		$data['all_income_ledger'] = $this->Finance_model->get_all_deposit_type();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('592',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/transfer_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	

	public function expense() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_acc_expense').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_acc_expense');

		$data['path_url'] = 'accounting_expense';

		$data['all_payees'] = $this->Finance_model->all_payees();

		$data['all_employees'] = $this->Xin_model->all_employees();

		$data['all_companies'] = $this->Xin_model->get_companies();

		$data['all_expense_types'] = $this->Expense_model->all_expense_types();

		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

		$data['all_income_categories_list'] = $this->Finance_model->all_income_categories_list();

		$data['get_all_payment_method'] = $this->Finance_model->get_all_payment_method();
		
		$data['all_vendors'] = $this->Finance_model->get_vendors()->result();

		$data['sub_expense'] = $this->Xin_model->get_sub_expense_type()->result();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('76',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/expense_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function vendor() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_vendor_title').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_vendor_title');

		$data['path_url'] = 'accounting_vendor';

		$data['all_vendors'] = $this->Finance_model->get_vendors();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('76',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/vendor_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function new_bill_payments() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = 'Single Bill Payments | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = 'New Bill Payments';

		$data['path_url'] = 'accounting_expense';

		$data['transaction_amount'] = $this->Clients_model->select_all_transaction_ids();
		
		$data['bulk_transaction_amount'] = $this->Clients_model->get_bulk_not_deducted()->result();

		// print_r($data['bulk_transaction_amount']);die;

		$hospital = $this->Clients_model->get_all_hospital_info()->result();

		$data['all_payees'] = $this->Finance_model->all_payees();

		$data['all_employees'] = $this->Xin_model->all_employees();

		$data['all_companies'] = $this->Xin_model->get_companies();

		$data['all_expense_types'] = $this->Expense_model->all_expense_types();

		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

		$data['all_income_categories_list'] = $this->Finance_model->all_income_categories_list();

		$data['get_all_payment_method'] = $this->Finance_model->get_all_payment_method();

		if (!is_null($this->input->post('to_date'))) {
			$to_date = $this->input->post('to_date');
			$from_date = $this->input->post('from_date');
			$data['all_bill_payments'] = $this->Finance_model->get_all_bill_payments($from_date,$to_date);
		}else{
			$data['all_bill_payments'] = $this->Finance_model->get_all_bill_payments();
		}

		
		$data['all_bulk_bill_payments'] = $this->Finance_model->get_all_bulk_bill_payments();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('76',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/new_bill_payments", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function new_bulk_bill_payments() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = 'New Bulk Bill Payments | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = 'New Bulk Bill Payments';

		$data['path_url'] = 'accounting_expense';

		$data['transaction_amount'] = $this->Clients_model->select_all_transaction_ids();
		
		$data['bulk_transaction_amount'] = $this->Clients_model->get_bulk_not_deducted()->result();

		// print_r($data['bulk_transaction_amount']);die;

		$hospital = $this->Clients_model->get_all_hospital_info()->result();


		$data['all_payees'] = $this->Finance_model->all_payees();

		$data['all_employees'] = $this->Xin_model->all_employees();

		$data['all_companies'] = $this->Xin_model->get_companies();

		$data['all_expense_types'] = $this->Expense_model->all_expense_types();

		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

		$data['all_income_categories_list'] = $this->Finance_model->all_income_categories_list();

		$data['get_all_payment_method'] = $this->Finance_model->get_all_payment_method();

		$data['all_bill_payments'] = $this->Finance_model->get_all_bill_payments();

		if (!is_null($this->input->post('to_date'))) {
			$from_date = $this->input->post('from_date');
			$to_date = $this->input->post('to_date');
			$data['all_bulk_bill_payments'] = $this->Finance_model->get_all_bulk_bill_payments($from_date,$to_date);
		}else{
			$data['all_bulk_bill_payments'] = $this->Finance_model->get_all_bulk_bill_payments();
		}
		

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('76',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/new_bulk_bill_payments", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function all_bills_reports() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = 'Provider Bills Payment | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = 'Provider Bills Payment';

		$data['path_url'] = 'accounting_expense';

		$data['transaction_amount'] = $this->Clients_model->select_all_transaction_ids();

		$data['all_payees'] = $this->Finance_model->all_payees();

		$data['all_employees'] = $this->Xin_model->all_employees();

		$data['all_companies'] = $this->Xin_model->get_companies();

		$data['all_expense_types'] = $this->Expense_model->all_expense_types();

		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

		$data['all_income_categories_list'] = $this->Finance_model->all_income_categories_list();

		$data['get_all_payment_method'] = $this->Finance_model->get_all_payment_method();

		$data['all_hospital']   =  $this->Training_model->getAll2('xin_hospital',' 1 order by hospital_id asc');

		if($this->input->post("hospital_name")) {
			$data['hospital_id'] = $this->input->post("hospital_name");
			$data['from'] = $this->input->post("from_date");
			$data['to'] = $this->input->post("to_date");
			$data['all_bill_payments'] = $this->Finance_model->get_all_bill_payments();
			$data['all_bulk_bill_payments'] = $this->Finance_model->get_all_bulk_bill_payments();
		} else {
			$data['hospital_id'] = '';
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('533',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/all_bills_reports", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function add_new_bill() {

		if($this->input->post('bill_date')) {		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			

			/* Server side PHP input validation */

			$description = $this->input->post('description');

			$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);

			$bank_cash = $this->Finance_model->read_bankcash_information($this->input->post('bank_cash_id'));

			if($this->input->post('bank_cash_id')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_account_field');

			} else if($this->input->post('deduct_amount')==='') {

				$Return['error'] = $this->lang->line('xin_error_amount_field');

			} else if($this->input->post('deduct_amount') > $bank_cash[0]->account_balance) {

				$Return['error'] = $this->lang->line('xin_acc_error_amount_should_be_less_than_current');

			} else if($this->input->post('bill_date')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_expense_date');

			} else if($_FILES['transaction_file']['size'] == 0) {

				$fname = 'no_file';

			}

			else if(is_uploaded_file($_FILES['transaction_file']['tmp_name'])) {

			//checking image type

				$allowed =  array('png','jpg','jpeg','pdf','gif','txt','doc','docx','xls','xlsx');

				$filename = $_FILES['transaction_file']['name'];

				$ext = pathinfo($filename, PATHINFO_EXTENSION);



				if(in_array($ext,$allowed)){

					$tmp_name = $_FILES["transaction_file"]["tmp_name"];

					$profile = "uploads/accounting/new_bill_transaction/";

					$set_img = base_url()."uploads/accounting/new_bill_transaction/";

				// basename() may prevent filesystem traversal attacks;

				// further validation/sanitation of the filename may be appropriate

					$name = basename($_FILES["transaction_file"]["name"]);

					$newfilename = 'expense_'.round(microtime(true)).'.'.$ext;

					move_uploaded_file($tmp_name, $profile.$newfilename);

					$fname = $newfilename;					

				} else {

					$Return['error'] = $this->lang->line('xin_acc_error_attachment');

				}

			}



			if($Return['error']!=''){

				$this->output($Return);

			}



			$data = array(

				'newbill_tid' => $this->input->post('diagnose_transaction_id'),

				'newbill_account' => $this->input->post('bank_cash_id'),

		// 'transaction_type' => 'expense',

		// 'dr_cr' => 'cr',

				'newbill_date' => $this->input->post('bill_date'),

				'newbill_paymentmethod' => $this->input->post('payment_method'),

				'newbill_ref' => $this->input->post('bill_reference'),

				'newbill_attachfile' => $fname,

				'newbill_description' => $this->input->post('description'),

				'newbill_createdat' => date('Y-m-d H:i:s')

			);

			$result = $this->Finance_model->add_bill($data);
		// $acc_balance = '';
			if ($result == TRUE) {			

			// update data in bank account

				$account_id = $this->Finance_model->read_bankcash_information($this->input->post('bank_cash_id'));

				$acc_balance = $account_id[0]->account_balance - $this->input->post('deduct_amount');

				$data3 = array(

					'account_balance' => $acc_balance

				);

				$this->session->set_flashdata('success','Provider Bill Payment Proccessed Successfully.');

				$Return['result'] = 'Provider Bill Payment Proccessed Successfully.';

				$this->Finance_model->update_bankcash_record($data3,$this->input->post('bank_cash_id'));

				$this->Finance_model->update_deduct_status($this->input->post('diagnose_transaction_id'));


			} else {

				$Return['error'] = $this->lang->line('xin_error');

			}

			$this->output($Return);

		// redirect('accounting/new_bill_payments');
			exit;

		}

	} 

	public function add_new_bulk_bill() 
	{

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$Return['csrf_hash'] = $this->security->get_csrf_hash();

		print_r($this->input->post());		

		/* Server side PHP input validation */

		$description = $this->input->post('description');

		$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);

		$bank_cash = $this->Finance_model->read_bankcash_information($this->input->post('bank_cash_id'));

		if($this->input->post('bank_cash_id')==='') {

			$Return['error'] = $this->lang->line('xin_acc_error_account_field');

		} else if($this->input->post('deduct_amount')==='') {

			$Return['error'] = $this->lang->line('xin_error_amount_field');

		} else if($this->input->post('deduct_amount') > $bank_cash[0]->account_balance) {

			$Return['error'] = $this->lang->line('xin_acc_error_amount_should_be_less_than_current');

		} else if($this->input->post('bill_date')==='') {

			$Return['error'] = $this->lang->line('xin_acc_error_expense_date');

		} else if($_FILES['transaction_file']['size'] == 0) {

			$fname = 'no_file';

		}

		else if(is_uploaded_file($_FILES['transaction_file']['tmp_name'])) {

			//checking image type

			$allowed =  array('png','jpg','jpeg','pdf','gif','txt','doc','docx','xls','xlsx');

			$filename = $_FILES['transaction_file']['name'];

			$ext = pathinfo($filename, PATHINFO_EXTENSION);

			

			if(in_array($ext,$allowed)){

				$tmp_name = $_FILES["transaction_file"]["tmp_name"];

				$profile = "uploads/accounting/new_bill_transaction/";

				$set_img = base_url()."uploads/accounting/new_bill_transaction/";

				// basename() may prevent filesystem traversal attacks;

				// further validation/sanitation of the filename may be appropriate

				$name = basename($_FILES["transaction_file"]["name"]);

				$newfilename = 'expense_'.round(microtime(true)).'.'.$ext;

				move_uploaded_file($tmp_name, $profile.$newfilename);

				$fname = $newfilename;					

			} else {

				$Return['error'] = $this->lang->line('xin_acc_error_attachment');

			}

		}



		if($Return['error']!=''){

			$this->output($Return);

		}

		

		$data = array(

			'newbill_tid' => $this->input->post('diagnose_transaction_id'),

			'newbill_account' => $this->input->post('bank_cash_id'),

		// 'transaction_type' => 'expense',

		// 'dr_cr' => 'cr',

			'newbill_date' => $this->input->post('bill_date'),

			'newbill_paymentmethod' => $this->input->post('payment_method'),

			'newbill_ref' => $this->input->post('bill_reference'),

			'newbill_attachfile' => $fname,

			'newbill_description' => $this->input->post('description'),

			'newbill_createdat' => date('Y-m-d H:i:s'),

			'is_bulk' => 1

		);

		$result = $this->Finance_model->add_bill($data);
		// $acc_balance = '';
		if ($result == TRUE) {			

			// update data in bank account

			$account_id = $this->Finance_model->read_bankcash_information($this->input->post('bank_cash_id'));

			$acc_balance = $account_id[0]->account_balance - $this->input->post('deduct_amount');

			$data3 = array(

				'account_balance' => $acc_balance

			);

			
			$this->Finance_model->update_bankcash_record($data3,$this->input->post('bank_cash_id'));

			$bulk = $this->Clients_model->get_bulk($this->input->post('diagnose_transaction_id'))->result();

			$ids = explode(",", $bulk[0]->bulk_id);

			foreach ($ids as $id) {
				$this->Finance_model->update_deduct_status($id);
			}

			$this->Finance_model->update_bulk_deduct_status($bulk[0]->bulk_id);

			$this->session->set_flashdata('success','New Bill Payment Added Successfully.');

			$Return['result'] = 'New Bill Payment Added Successfully.';


		} else {

			$Return['error'] = $this->lang->line('xin_error');

		}

		$this->output($Return);

		// redirect('accounting/new_bill_payments');
		exit;


	} 

	// get company > employees

	public function get_employees() {

		$loop = $this->input->get('loop');

		$data['title'] = $this->Xin_model->site_title();

		$id = $this->uri->segment(4);

		$deposit_id = $this->uri->segment(5);

		$transaction = $this->Finance_model->get_deposit_by_id($deposit_id)->result();

		$payee_id = '';

		if (!empty($transaction)) {
			$payee_id = $transaction[0]->payer_payee_id;
		}

		$data = array(

			'company_id' => $id,

			'payee_id' => $payee_id,

			'loop' => $loop
		);

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view("admin/accounting/get_employees", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

	}

	public function get_employees2() {

		$data['title'] = $this->Xin_model->site_title();

		$company_id = $this->uri->segment(4);

		$deposit_id = $this->uri->segment(5);

		$transaction = $this->Finance_model->get_deposit_by_id($deposit_id)->result();

		$payee_id = '';

		if (!empty($transaction)) {
			$payee_id = $transaction[0]->payer_payee_id;
		}

		$session = $this->session->userdata('username');

		$html = '';

		if(!empty($session)){ 

			// $this->load->view("admin/accounting/get_employees", $data);
			$result = $this->Department_model->ajax_company_employee_info($company_id);

			if (!empty($result)) {
				foreach($result as $employee)
				{
					$html .= "<option value=\"".$employee->user_id."\""; 
					if(isset($payee_id)){
						if($payee_id == $employee->user_id) $html .= "selected";
					}
					$html .= ">"; 
					$html .= $employee->first_name.' '.$employee->last_name."</option>";
				}
			}

			echo $html;
		} else {

			echo $html;

		}


	}

	 // get company > expense_types

	public function get_company_expense_types() {



		$data['title'] = $this->Xin_model->site_title();

		$id = $this->uri->segment(4);

		

		$data = array(

			'company_id' => $id

		);

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view("admin/accounting/get_company_expense_types", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

	}

	 // get company > expense_types

	public function get_company_subexpense_types() {



		$data['title'] = $this->Xin_model->site_title();

		$id = $this->uri->segment(4);


		$session = $this->session->userdata('username');

		$subexpense = $this->Finance_model->ajax_subexpense_types_info($id);

		$html = '';

		if (!empty($subexpense)) {
			foreach ($subexpense as $sub) {
				$html .= "
				<option value=\"".$sub->id."\">".$sub->name."</option>
				";
			}
		}
		echo $html;
	}

	 // get company > expense_types

	public function get_expense_types() {



		$data['title'] = $this->Xin_model->site_title();

		$id = $this->uri->segment(4);

		

		$data = array(

			'company_id' => $id

		);

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view("admin/accounting/get_expense_types", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

	}

	public function payers() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_acc_payers').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_acc_payers');

		$data['path_url'] = 'accounting_payers';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('81',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/payers_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	

	public function payees() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_acc_payees').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_acc_payees');

		$data['path_url'] = 'accounting_payees';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('80',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/payees_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function bank_cash() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_acc_accounts').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_acc_accounts');

		$data['path_url'] = 'accounting_bank_cash';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('72',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/bank_cash_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	

	public function account_balances() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_acc_account_balances').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_acc_account_balances');

		$data['path_url'] = 'accounting_account_balances';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('73',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/account_balances", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	

	public function transactions() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_acc_view_transactions').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_acc_view_transactions');

		$data['path_url'] = 'accounting_transactions';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('78',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/transaction_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}



	public function read() {

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		$id = $this->input->get('bankcash_id');

		$result = $this->Finance_model->read_bankcash_information($id);

		$data = array(

			'bankcash_id' => $result[0]->bankcash_id,

			'account_name' => $result[0]->account_name,

			'account_balance' => $result[0]->account_balance,

			'account_number' => $result[0]->account_number,

			'branch_code' => $result[0]->branch_code,

			'bank_branch' => $result[0]->bank_branch,

			'created_at' => $result[0]->created_at

		);

		if(!empty($session)){ 

			$this->load->view('admin/accounting/dialog_accounting', $data);

		} else {

			redirect('admin/');

		}

	}

	public function read_payer() {

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		$id = $this->input->get('payer_id');

		$result = $this->Finance_model->read_payer_info($id);

		$data = array(

			'payer_id' => $result[0]->payer_id,

			'payer_name' => $result[0]->payer_name,

			'contact_number' => $result[0]->contact_number,

			'created_at' => $result[0]->created_at

		);

		if(!empty($session)){ 

			$this->load->view('admin/accounting/dialog_accounting', $data);

		} else {

			redirect('admin/');

		}

	}

	

	public function read_payee() {

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		$id = $this->input->get('payee_id');

		$result = $this->Finance_model->read_payee_info($id);

		$data = array(

			'payee_id' => $result[0]->payee_id,

			'payee_name' => $result[0]->payee_name,

			'contact_number' => $result[0]->contact_number,

			'created_at' => $result[0]->created_at

		);

		if(!empty($session)){ 

			$this->load->view('admin/accounting/dialog_accounting', $data);

		} else {

			redirect('admin/');

		}

	}

	// payers list

	public function payers_list()

	{



		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/payers_list", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		

		

		$payer = $this->Finance_model->get_payers();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		$data = array();



		foreach($payer->result() as $r) {



		   // create at

			$created_at = $this->Xin_model->set_date_format($r->created_at);

			if(in_array('368',$role_resources_ids)) { //edit

				$edit = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_edit').'"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"  data-toggle="modal" data-target=".payroll_template_modal"  data-payer_id="'. $r->payer_id . '"><span class="fa fa-pencil"></span></button></span>';

			} else {

				$edit = '';

			}

			if(in_array('369',$role_resources_ids)) { // delete

				$delete = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_delete').'"><button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal" data-record-id="'. $r->payer_id . '"><span class="fa fa-trash"></span></button></span>';

			} else {

				$delete = '';

			}

			

			$combhr = $edit.$delete;

			$data[] = array(

				$combhr,

				$r->payer_name,

				$r->contact_number,

				$created_at

			);

		}



		$output = array(

			"draw" => $draw,

			"recordsTotal" => $payer->num_rows(),

			"recordsFiltered" => $payer->num_rows(),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	}

	 // payees list

	public function payees_list()

	{



		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/payees_list", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		

		

		$payee = $this->Finance_model->get_payees();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		$data = array();



		foreach($payee->result() as $r) {



		   // create at

			$created_at = $this->Xin_model->set_date_format($r->created_at);

			if(in_array('365',$role_resources_ids)) { //edit

				$edit = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_edit').'"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"  data-toggle="modal" data-target=".payroll_template_modal"  data-payee_id="'. $r->payee_id . '"><span class="fa fa-pencil"></span></button></span>';

			} else {

				$edit = '';

			}

			if(in_array('366',$role_resources_ids)) { // delete

				$delete = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_delete').'"><button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal" data-record-id="'. $r->payee_id . '"><span class="fa fa-trash"></span></button></span>';

			} else {

				$delete = '';

			}

			

			$combhr = $edit.$delete;

			$data[] = array(

				$combhr,

				$r->payee_name,

				$r->contact_number,

				$created_at

			);

		}



		$output = array(

			"draw" => $draw,

			"recordsTotal" => $payee->num_rows(),

			"recordsFiltered" => $payee->num_rows(),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	}

	 // bank and cash list

	public function bank_cash_list()

	{



		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/bank_cash_list", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		

		

		$bankcash = $this->Finance_model->get_bankcash();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		$data = array();



		foreach($bankcash->result() as $r) {



			// get currency

			$account_balance = $this->Xin_model->currency_sign($r->account_balance);

			$bank_cash = $this->Finance_model->read_transaction_by_bank_info($r->bankcash_id);

			if(!is_null($bank_cash)){

				$account = '<a data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_acc_ledger_view').'" href="'.site_url('admin/accounting/accounts_ledger/'.$r->bankcash_id.'').'" target="_blank">'.$r->account_name.'</a>';

			} else {

				$account = $r->account_name;

			}

			$edit = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_edit').'"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"  data-toggle="modal" data-target="#edit-modal-data"  data-bankcash_id="'. $r->bankcash_id . '"><span class="fa fa-pencil"></span></button></span>';

			$delete = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_delete').'"><button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal" data-record-id="'. $r->bankcash_id . '"><span class="fa fa-trash"></span></button></span>';

			

			$combhr = $edit.$delete;



			$data[] = array(

				$combhr,

				$account,

				$r->account_number,

				$r->branch_code,

				$account_balance,

				$r->bank_branch

			);

		}



		$output = array(

			"draw" => $draw,

			"recordsTotal" => $bankcash->num_rows(),

			"recordsFiltered" => $bankcash->num_rows(),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	}



	 // account balances list

	public function account_balances_list()

	{



		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/account_balances", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		

		

		$bankcash = $this->Finance_model->get_bankcash();

		

		$data = array();



		foreach($bankcash->result() as $r) {



			  // get currency

			$account_balance = $this->Xin_model->currency_sign($r->account_balance);

			$bank_cash = $this->Finance_model->read_transaction_by_bank_info($r->bankcash_id);

			if(!is_null($bank_cash)){

				$account = '<a data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_acc_ledger_view').'" href="'.site_url('admin/accounting/accounts_ledger/'.$r->bankcash_id.'').'" target="_blank">'.$r->account_name.'</a>';

			} else {

				$account = $r->account_name;

			}



			$data[] = array(

				$account,

				$account_balance

			);

		}



		$output = array(

			"draw" => $draw,

			"recordsTotal" => $bankcash->num_rows(),

			"recordsFiltered" => $bankcash->num_rows(),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	}



	 // deposit list

	public function deposit_list() {



		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/deposit_list", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		$deposit = $this->Finance_model->get_deposit();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		$data = array();



		foreach($deposit->result() as $r) {



			// get amount

			$amount = $this->Xin_model->currency_sign($r->amount);

			// account type

			$acc_type = $this->Finance_model->read_bankcash_information($r->account_id);

			if(!is_null($acc_type)){

				$account = $acc_type[0]->account_name;

			} else {

				$account = '--';	

			}

			

			// get user > added by

			// $payer = $this->Finance_model->read_payer_info($r->payer_payee_id);

			$payer = $this->Finance_model->read_employee_info($r->payer_payee_id);

			// user full name

			if(!is_null($payer)){

				$full_name = $payer[0]->first_name." ".$payer[0]->last_name;

			} else {

				$full_name = '--';	

			}

			

			// deposit date

			$deposit_date = $this->Xin_model->set_date_format($r->transaction_date);

			// category


			if (is_null($r->sub_deposit_ids)) {
				$category = '-';
			}else{
				$sub_category = explode(",", $r->sub_deposit_ids);

				$category = array();

				foreach($sub_category as $sub)
				{
					$sub_deposit = $this->Finance_model->read_sub_deposit($sub);

					if (!empty($sub_deposit)) {
					
						// $deposit_type = $this->Finance_model->read_deposit_type($sub_deposit[0]->deposit_id);
					
						$category[] = $sub_deposit[0]->name;
					}
				}

				$category = implode(", ", $category);

			}

			// $category_id = $this->Finance_model->read_income_category($r->transaction_cat_id);

			// if(!is_null($category_id)){

			// 	$category = $category_id[0]->name;

			// } else {

			// 	$category = '--';	

			// }

			// payment method 

			$payment_method = $this->Xin_model->read_payment_method($r->payment_method_id);

			if(!is_null($payment_method)){

				$method_name = $payment_method[0]->method_name;

			} else {

				$method_name = '--';	

			}

			

			$edit = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_edit').'"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"  data-toggle="modal" data-target=".edit-modal-data"  data-deposit_id="'. $r->transaction_id . '"><span class="fa fa-pencil"></span></button></span>';

			$delete = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_delete').'"><button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal" data-record-id="'. $r->transaction_id . '"><span class="fa fa-trash"></span></button></span>';			

			$combhr = $edit.$delete;



			$data[] = array(

				$combhr,

				$account,

				$full_name,

				$amount,

				$category,

				$r->reference,

				$method_name,

				$deposit_date

			);

		}



		$output = array(

			"draw" => $draw,

			"recordsTotal" => $deposit->num_rows(),

			"recordsFiltered" => $deposit->num_rows(),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	}



	 // expense list

	public function expense_list()

	{

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		$data['all_vendors'] = $this->Finance_model->get_vendors()->result();

		$data['sub_expense'] = $this->Xin_model->get_sub_expense_type()->result();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/expense_list", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		

		

		$expense = $this->Finance_model->get_expense();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		$data = array();



		foreach($expense->result() as $r) {



			// get amount

			$amount = $this->Xin_model->currency_sign($r->amount);

			// account type

			$acc_type = $this->Finance_model->read_bankcash_information($r->account_id);

			if(!is_null($acc_type)){

				$account = $acc_type[0]->account_name;

			} else {

				$account = '--';	

			}

			

			// get user > added by

			//$payee = $this->Finance_model->read_payee_info($r->payer_payee_id);

			$payee = $this->Xin_model->read_user_info($r->payer_payee_id);

			// user full name

			if(!is_null($payee)){

				$full_name = $payee[0]->first_name.' '.$payee[0]->last_name;

			} else {

				$full_name = '--';	

			}



			// deposit date

			$expense_date = $this->Xin_model->set_date_format($r->transaction_date);

			// category

			if (is_null($r->subexpenses)) {
				$category = '-';
			}else{
				$sub_category = explode(",", $r->subexpenses);

				$category = array();

				foreach($sub_category as $sub)
				{
					$sub_expense = $this->Finance_model->read_sub_expense($sub);

					if (!empty($sub_expense)) {
					
						// $expense_type = $this->Finance_model->read_expense_type($sub_expense[0]->expense_id);
					
						$category[] = $sub_expense[0]->name;
					}
				}

				$category = implode(", ", $category);

			}

			// $expense_type = $this->Expense_model->read_expense_type_information($r->transaction_cat_id);

			// if(!is_null($expense_type)){

			// 	$category = $expense_type[0]->name;

			// } else {

			// 	$category = '--';	

			// }

			

			// payment method 

			$payment_method = $this->Xin_model->read_payment_method($r->payment_method_id);

			if(!is_null($payment_method)){

				$method_name = $payment_method[0]->method_name;

			} else {

				$method_name = '--';	

			}

			$edit = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_edit').'"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"  data-toggle="modal" data-target=".edit-modal-data"  data-expense_id="'. $r->transaction_id . '"><span class="fa fa-pencil"></span></button></span>';

			$delete = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_delete').'"><button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal" data-record-id="'. $r->transaction_id . '"><span class="fa fa-trash"></span></button></span>';			

			$combhr = $edit.$delete;



			$data[] = array(

				$combhr,

				$account,

				$full_name,

				$amount,

				$category,

				$r->reference,

				$method_name,

				$expense_date

			);

		}



		$output = array(

			"draw" => $draw,

			"recordsTotal" => $expense->num_rows(),

			"recordsFiltered" => $expense->num_rows(),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	}

    // expense list

	public function vendors_list()

	{

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/vendor_list", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));
		

		$vendors = $this->Finance_model->get_vendors();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		$data = array();


		foreach($vendors->result() as $r) {



			$edit = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_edit').'"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"  data-toggle="modal" data-target=".edit-modal-data"  data-vendors_id="'. $r->id. '"><span class="fa fa-pencil"></span></button></span>';

			$delete = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_delete').'"><button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal" data-record-id="'. $r->id . '"><span class="fa fa-trash"></span></button></span>';			

			$combhr = $edit.$delete;



			$data[] = array(

				$combhr,

				$r->name,

				$r->phone,

				$r->address,

				$r->rc_number,

				$r->description

			);

		}



		$output = array(

			"draw" => $draw,

			"recordsTotal" => $vendors->num_rows(),

			"recordsFiltered" => $vendors->num_rows(),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	}	 



	// transactions list

	public function transaction_list()

	{



		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/transaction_list", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		$transaction = $this->Finance_model->get_transaction();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		$data = array();

		$balance2 = 0;

		foreach($transaction->result() as $r) {



			// transaction date

			$transaction_date = $this->Xin_model->set_date_format($r->transaction_date);

			// get currency

			$total_amount = $this->Xin_model->currency_sign($r->amount);

			// credit

			$cr_dr = $r->dr_cr=="dr" ? "Debit" : "Credit";

			

			// account type
			$type = explode("-", $r->transaction_type);

			// print_r($type);

			if (count($type) < 2) {
				//Account
				$acc_type = $this->Finance_model->read_bankcash_information($r->account_id);

				if(!is_null($acc_type)){

					$account = '<a href="'.site_url('admin/accounting/accounts_ledger/'.$r->account_id.'').'" title="'.$this->lang->line('xin_acc_ledger_view').'" target="_blank">'.$acc_type[0]->account_name.'</a>';

				} else {

					$account = '--';	

				}
			}else{
				//Ledger
				// print_r($type);die;

				if ($type[2] == 'i') {
					//Income

					$deposit = $this->Finance_model->read_deposit_type($r->account_id);

					if (!empty($deposit)) {
						$account = $deposit[0]->name;
					}else{
						$account = "--";
					}

				}else{
					//Expense

					$expense = $this->Finance_model->read_expense_type($r->account_id);

					if (!empty($expense)) {
						$account = $expense[0]->name;
					}else{
						$account = "--";
					}

				}
			}
			

			//

			if($r->dr_cr=="cr"){

				$edit = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_edit').'"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"  data-toggle="modal" data-target=".view-modal-data-bg"  data-deposit_id="'. $r->transaction_id . '"><span class="fa fa-pencil"></span></button></span>';

			} else {

				$edit = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_edit').'"><button type="button" class="btn icon-btn btn-xs btn-default waves-effect waves-light"  data-toggle="modal" data-target=".edit-modal-data"  data-expense_id="'. $r->transaction_id . '"><span class="fa fa-pencil"></span></button></span>';

			}

			$delete = '<span data-toggle="tooltip" data-placement="top" title="'.$this->lang->line('xin_delete').'"><button type="button" class="btn icon-btn btn-xs btn-danger waves-effect waves-light delete" data-toggle="modal" data-target=".delete-modal" data-record-id="'. $r->transaction_id . '"><span class="fa fa-trash"></span></button></span>';			

			$combhr = $edit.$delete;

			

			$data[] = array(

				$transaction_date,

				$account,

				$cr_dr,

				$r->transaction_type,

				$total_amount,

				$r->reference

			);

		}



		$output = array(

			"draw" => $draw,

			"recordsTotal" => $transaction->num_rows(),

			"recordsFiltered" => $transaction->num_rows(),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	}



	 // Validate and add info in database

	public function add_deposit() {


		// if($this->input->post('add_type')=='deposit') {		

			/* Define return | here result is used to return user data and error for error message */

			// print_r($this->input->post());

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			

			/* Server side PHP input validation */

			$description = $this->input->post('description');

			$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);

			if($this->input->post('bank_cash_id')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_account_field');

			} else if($this->input->post('amount')==='') {

				$Return['error'] = $this->lang->line('xin_error_amount_field');

			} else if($this->input->post('deposit_date')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_deposit_date');

			} else if($this->input->post('payee_id')==='') {

				$Return['error'] = "Select a payee";

			} else if($this->input->post('payment_method')==='') {

				$Return['error'] = "Select a payment method";

			} else if($this->input->post('deposit_reference')==='') {

				$Return['error'] = "Select a deposit reference";

			} else if($this->input->post('sub_deposit')==='') {

				$Return['error'] = "Select sub income ledgers";

			}

			else if(isset($_FILES['deposit_file']['name'])) {

			//checking image type

				if(is_uploaded_file($_FILES['deposit_file']['tmp_name'])) {
					$allowed =  array('PNG','JPG','JPEG','PDF','GIF','png','jpg','jpeg','pdf','gif','txt','doc','docx','xls','xlsx');

					$filename = $_FILES['deposit_file']['name'];

					$ext = pathinfo($filename, PATHINFO_EXTENSION);



					if(in_array($ext,$allowed)){

						$tmp_name = $_FILES["deposit_file"]["tmp_name"];

						$profile = "uploads/accounting/deposit/";

						$set_img = base_url()."uploads/accounting/deposit/";

					// basename() may prevent filesystem traversal attacks;

					// further validation/sanitation of the filename may be appropriate

						$name = basename($_FILES["deposit_file"]["name"]);

						$newfilename = 'deposit_'.round(microtime(true)).'.'.$ext;

						move_uploaded_file($tmp_name, $profile.$newfilename);

						$fname = $newfilename;					

					} else {

						$Return['error'] = $this->lang->line('xin_acc_error_attachment');

					}
				}

	
			}

			if (!isset($fname)) {
				$fname = 'no_file';
			}



			if($Return['error']!=''){

				$this->output($Return);

			}

			$sub_deposit = $this->input->post('sub_deposit');

			if (count($sub_deposit) > 1) {
				$sub_deposit = implode(",", $sub_deposit);
			}


			$data = array(

				'account_id' => $this->input->post('bank_cash_id'),

				'company_id' => $this->input->post('company'),

				'amount' => $this->input->post('amount'),

				'transaction_type' => 'income',

				'dr_cr' => 'dr',

				'transaction_date' => $this->input->post('deposit_date'),

				'attachment_file' => $fname,

				'transaction_cat_id' => "1",

				'sub_deposit_ids' => $sub_deposit,

				'payer_payee_id' => $this->input->post('payee_id'),

				'payment_method_id' => $this->input->post('payment_method'),

				'description' => $qt_description,

				'reference' => $this->input->post('deposit_reference'),

				'invoice_id' => 0,

				'created_at' => date('Y-m-d H:i:s')

			);

			// print_r($data);die;
			
			$result = $this->Finance_model->add_transactions($data);
			// $result = TRUE;
			if ($result == TRUE) {			

			// add data to bank account>cr

				$account_id = $this->Finance_model->read_bankcash_information($this->input->post('bank_cash_id'));

				$acc_balance = $account_id[0]->account_balance + $this->input->post('amount');


				$data3 = array(

					'account_balance' => $acc_balance

				);

				$this->Finance_model->update_bankcash_record($data3,$this->input->post('bank_cash_id'));

				$sub_deposits = explode(",", $sub_deposit);

				$deposit_type_id = array();

				foreach($sub_deposits as $sub)
				{

					$deposit_type = $this->Xin_model->get_deposit_type_by_sub($sub)->result();

					if (!empty($deposit_type)) {
						if (!in_array($deposit_type[0]->id, $deposit_type_id)) {

							$amount = $this->input->post('amount');
							$balance = $deposit_type[0]->balance+$amount;
							$data = array(
								'balance' => $balance
							);

							$result = $this->Finance_model->update_deposit_record($data,$deposit_type[0]->id);

							$deposit_type_id[] = $deposit_type[0]->id;

						}
					}
				}

				$Return['result'] = $this->lang->line('xin_acc_success_deposit_added');

			} else {

				$Return['error'] = $this->lang->line('xin_error');

			}

			$this->output($Return);

			exit;


	}

	public function add_deposit_multiple() {


		// if($this->input->post('add_type')=='deposit') {		

			/* Define return | here result is used to return user data and error for error message */

			print_r($this->input->post());die;

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();
			
			$bank_cash_id = $this->input->post('bank_cash_id');
			$amount = $this->input->post('amount');
			$deposit_date = $this->input->post('deposit_date');
			$company = $this->input->post('company');
			$company = $this->input->post('company');

			/* Server side PHP input validation */

			$description = $this->input->post('description');

			$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);

			if($this->input->post('bank_cash_id')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_account_field');

			} else if($this->input->post('amount')==='') {

				$Return['error'] = $this->lang->line('xin_error_amount_field');

			} else if($this->input->post('deposit_date')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_deposit_date');

			} else if($this->input->post('payee_id')==='') {

				$Return['error'] = "Select a payee";

			} else if($this->input->post('payment_method')==='') {

				$Return['error'] = "Select a payment method";

			} else if($this->input->post('deposit_reference')==='') {

				$Return['error'] = "Select a deposit reference";

			} else if($this->input->post('sub_deposit')==='') {

				$Return['error'] = "Select sub income ledgers";

			}

			else if(isset($_FILES['deposit_file']['name'])) {

			//checking image type

				if(is_uploaded_file($_FILES['deposit_file']['tmp_name'])) {
					$allowed =  array('PNG','JPG','JPEG','PDF','GIF','png','jpg','jpeg','pdf','gif','txt','doc','docx','xls','xlsx');

					$filename = $_FILES['deposit_file']['name'];

					$ext = pathinfo($filename, PATHINFO_EXTENSION);



					if(in_array($ext,$allowed)){

						$tmp_name = $_FILES["deposit_file"]["tmp_name"];

						$profile = "uploads/accounting/deposit/";

						$set_img = base_url()."uploads/accounting/deposit/";

					// basename() may prevent filesystem traversal attacks;

					// further validation/sanitation of the filename may be appropriate

						$name = basename($_FILES["deposit_file"]["name"]);

						$newfilename = 'deposit_'.round(microtime(true)).'.'.$ext;

						move_uploaded_file($tmp_name, $profile.$newfilename);

						$fname = $newfilename;					

					} else {

						$Return['error'] = $this->lang->line('xin_acc_error_attachment');

					}
				}

	
			}

			if (!isset($fname)) {
				$fname = 'no_file';
			}



			if($Return['error']!=''){

				$this->output($Return);

			}

			$sub_deposit = $this->input->post('sub_deposit');

			if (count($sub_deposit) > 1) {
				$sub_deposit = implode(",", $sub_deposit);
			}


			$data = array(

				'account_id' => $this->input->post('bank_cash_id'),

				'company_id' => $this->input->post('company'),

				'amount' => $this->input->post('amount'),

				'transaction_type' => 'income',

				'dr_cr' => 'dr',

				'transaction_date' => $this->input->post('deposit_date'),

				'attachment_file' => $fname,

				'transaction_cat_id' => "1",

				'sub_deposit_ids' => $sub_deposit,

				'payer_payee_id' => $this->input->post('payee_id'),

				'payment_method_id' => $this->input->post('payment_method'),

				'description' => $qt_description,

				'reference' => $this->input->post('deposit_reference'),

				'invoice_id' => 0,

				'created_at' => date('Y-m-d H:i:s')

			);

			// print_r($data);die;
			
			$result = $this->Finance_model->add_transactions($data);
			// $result = TRUE;
			if ($result == TRUE) {			

			// add data to bank account>cr

				$account_id = $this->Finance_model->read_bankcash_information($this->input->post('bank_cash_id'));

				$acc_balance = $account_id[0]->account_balance + $this->input->post('amount');


				$data3 = array(

					'account_balance' => $acc_balance

				);

				$this->Finance_model->update_bankcash_record($data3,$this->input->post('bank_cash_id'));

				$sub_deposits = explode(",", $sub_deposit);

				$deposit_type_id = array();

				foreach($sub_deposits as $sub)
				{

					$deposit_type = $this->Xin_model->get_deposit_type_by_sub($sub)->result();

					if (!empty($deposit_type)) {
						if (!in_array($deposit_type[0]->id, $deposit_type_id)) {

							$amount = $this->input->post('amount');
							$balance = $deposit_type[0]->balance+$amount;
							$data = array(
								'balance' => $balance
							);

							$result = $this->Finance_model->update_deposit_record($data,$deposit_type[0]->id);

							$deposit_type_id[] = $deposit_type[0]->id;

						}
					}
				}

				$Return['result'] = $this->lang->line('xin_acc_success_deposit_added');

			} else {

				$Return['error'] = $this->lang->line('xin_error');

			}

			$this->output($Return);

			exit;


	} 

	

	// Validate and add info in database

	public function add_expense() {


		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$Return['csrf_hash'] = $this->security->get_csrf_hash();



		/* Server side PHP input validation */

		$description = $this->input->post('description');

		$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);

		$bank_cash = $this->Finance_model->read_bankcash_information($this->input->post('bank_cash_id'));

		// print_r($bank_cash);

		// print_r($this->input->post());
		// exit;

		if($this->input->post('bank_cash_id')==='') {

			$Return['error'] = $this->lang->line('xin_acc_error_account_field');

		} else if($this->input->post('amount')==='') {

			$Return['error'] = $this->lang->line('xin_error_amount_field');

		} else if($this->input->post('amount') > $bank_cash[0]->account_balance) {

			$Return['error'] = $this->lang->line('xin_acc_error_amount_should_be_less_than_current');

		} else if($this->input->post('expense_date')==='') {

			$Return['error'] = $this->lang->line('xin_acc_error_expense_date');

		} else if($this->input->post('company')==='') {

			$Return['error'] = $this->lang->line('error_company_field');

		} else if($this->input->post('employee_id')==='') {

			$Return['error'] = $this->lang->line('xin_error_employee_id');

		} else if($this->input->post('vendor_id')==='') {

			$Return['error'] = $this->lang->line('xin_vendor_name_error');

		}

		else if(isset($_FILES['expense_file']['name'])) {

			//checking image type

			if (is_uploaded_file($_FILES['expense_file']['tmp_name'])) {
				$allowed =  array('png','jpg','jpeg','pdf','gif','txt','doc','docx','xls','xlsx');

				$filename = $_FILES['expense_file']['name'];

				$ext = pathinfo($filename, PATHINFO_EXTENSION);

				

				if(in_array($ext,$allowed)){

					$tmp_name = $_FILES["expense_file"]["tmp_name"];

					$profile = "uploads/accounting/new_bill_transaction/";

					$set_img = base_url()."uploads/accounting/new_bill_transaction/";

					// basename() may prevent filesystem traversal attacks;

					// further validation/sanitation of the filename may be appropriate

					$name = basename($_FILES["expense_file"]["name"]);

					$newfilename = 'expense_'.round(microtime(true)).'.'.$ext;

					move_uploaded_file($tmp_name, $profile.$newfilename);

					$fname = $newfilename;					

				} else {

					$Return['error'] = $this->lang->line('xin_acc_error_attachment');

				}	
			}

			

		}

		if (!isset($fname)) {
			$fname = 'no_file';
		}



		if($Return['error']!=''){

			$this->output($Return);

		}


		$subexpense	= $this->input->post('subexpense_category');
		$subexpense = implode(",", $subexpense);	

		$data = array(

			'account_id' => $this->input->post('bank_cash_id'),

			'amount' => $this->input->post('amount'),

			'transaction_type' => 'expense',

			'dr_cr' => 'cr',

			'transaction_date' => $this->input->post('expense_date'),

			'attachment_file' => $fname,

			'transaction_cat_id' => '1',

			'subexpenses' => $subexpense,

			'payer_payee_id' => $this->input->post('payee_id'),

			'company_id' => $this->input->post('company'),

			'payment_method_id' => $this->input->post('payment_method'),

			'description' => $qt_description,

			'reference' => $this->input->post('expense_reference'),

			'invoice_id' => 0,

			'vendor_id' => $this->input->post('vendor_id'),

			'created_at' => date('Y-m-d H:i:s')

		);

		// print_r($data);die;

		$result = $this->Finance_model->add_transactions($data);
		// $result = TRUE;

		if ($result == TRUE) {			

			// update data in bank account

			$account_id = $this->Finance_model->read_bankcash_information($this->input->post('bank_cash_id'));

			$acc_balance = $account_id[0]->account_balance - $this->input->post('amount');

			

			$data3 = array(

				'account_balance' => $acc_balance

			);

			$this->Finance_model->update_bankcash_record($data3,$this->input->post('bank_cash_id'));

			$sub_expenses = explode(",", $subexpense);

			$expense_type_id = array();

			foreach($sub_expenses as $sub)
			{

				$expense_type = $this->Xin_model->get_expense_type_by_sub($sub)->result();

				if (!empty($expense_type)) {
					if (!in_array($expense_type[0]->expense_type_id, $expense_type_id)) {

						$amount = $this->input->post('amount');
						$balance = $expense_type[0]->balance+$amount;
						$data = array(
							'balance' => $balance
						);

						$result = $this->Finance_model->update_expense_type_record($data,$expense_type[0]->expense_type_id);

						$expense_type_id[] = $expense_type[0]->expense_type_id;

					}
				}
			}			


			$Return['result'] = $this->lang->line('xin_acc_success_expense_added');


		} else {

			$Return['error'] = $this->lang->line('xin_error');

		}

		$this->output($Return);

		exit;



		

	}


	public function add_vendors() 
	{	

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$Return['csrf_hash'] = $this->security->get_csrf_hash();



		/* Server side PHP input validation */

		if($this->input->post('vendor_name')==='') {

			$Return['error'] = $this->lang->line('xin_vendor_name_error');

		} else if($this->input->post('phone_number')==='') {

			$Return['error'] = $this->lang->line('xin_phone_number_error');

		} else if($this->input->post('address')==='') {

			$Return['error'] = $this->lang->line('xin_address_error');

		} else if($this->input->post('rc_number')==='') {

			$Return['error'] = $this->lang->line('xin_rc_number_error');

		} else if($this->input->post('description')==='') {

			$Return['error'] = $this->lang->line('xin_description_error');

		}

		// else if($_FILES['expense_file']['size'] == 0) {

		// 	$fname = 'no_file';

		// }

		// else if(is_uploaded_file($_FILES['expense_file']['tmp_name'])) {

		// 	//checking image type

		// 	$allowed =  array('png','jpg','jpeg','pdf','gif','txt','doc','docx','xls','xlsx');

		// 	$filename = $_FILES['expense_file']['name'];

		// 	$ext = pathinfo($filename, PATHINFO_EXTENSION);



		// 	if(in_array($ext,$allowed)){

		// 		$tmp_name = $_FILES["expense_file"]["tmp_name"];

		// 		$profile = "uploads/accounting/new_bill_transaction/";

		// 		$set_img = base_url()."uploads/accounting/new_bill_transaction/";

		// 		// basename() may prevent filesystem traversal attacks;

		// 		// further validation/sanitation of the filename may be appropriate

		// 		$name = basename($_FILES["expense_file"]["name"]);

		// 		$newfilename = 'expense_'.round(microtime(true)).'.'.$ext;

		// 		move_uploaded_file($tmp_name, $profile.$newfilename);

		// 		$fname = $newfilename;					

		// 	} else {

		// 		$Return['error'] = $this->lang->line('xin_acc_error_attachment');

		// 	}

		// }



		if($Return['error']!=''){

			$this->output($Return);

		}

		$data = array(

			'name' => $this->input->post('vendor_name'),

			'phone' => $this->input->post('phone_number'),

			'address' => $this->input->post('address'),

			'rc_number' => $this->input->post('rc_number'),

			'description' => $this->input->post('description'),

			'created_at' => date('Y-m-d H:i:s')

		);

		$result = $this->Finance_model->add_vendors($data);

		if ($result == TRUE) {			

			$Return['result'] = $this->lang->line('xin_vendor_added');

		} else {

			$Return['error'] = $this->lang->line('xin_error');

		}

		$this->output($Return);

		exit;

	} 

	

	// Validate and add info in database

	public function add_invoice_payment() {



		if($this->input->post('add_type')=='invoice_payment') {		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			

			/* Server side PHP input validation */

			$description = $this->input->post('description');

			$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);



			$invoice_tr = $this->Finance_model->read_invoice_transaction($this->input->post('invoice_id'));

			if ($invoice_tr->num_rows() > 0) {

				$Return['error'] = $this->lang->line('xin_acc_inv_paid_already');

			} 

			if($Return['error']!=''){

				$this->output($Return);

			}

			if($this->input->post('bank_cash_id')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_account_field');

			} else if($this->input->post('amount')==='') {

				$Return['error'] = $this->lang->line('xin_error_amount_field');

			} else if($this->input->post('add_invoice_date')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_deposit_date');

			} else if($this->input->post('payment_method')==='') {

				$Return['error'] = $this->lang->line('xin_error_makepayment_payment_method');

			}



			if($Return['error']!=''){

				$this->output($Return);

			}

			$invoice_id = $this->input->post('invoice_id');

			$data = array(

				'account_id' => $this->input->post('bank_cash_id'),

				'amount' => $this->input->post('amount'),

				'transaction_type' => 'income',

				'invoice_type' => 'customer',

				'dr_cr' => 'dr',

				'transaction_date' => $this->input->post('add_invoice_date'),

				'attachment_file' => '',

				'transaction_cat_id' => $this->input->post('category_id'),

				'payer_payee_id' => $this->input->post('payer_id'),

				'payment_method_id' => $this->input->post('payment_method'),

				'description' => $qt_description,

				'reference' => $this->input->post('reference'),

				'invoice_id' => $invoice_id,

				'created_at' => date('Y-m-d H:i:s')

			);

			$result = $this->Finance_model->add_transactions($data);

			if ($result == TRUE) {			

			// add data to bank account>cr

				$account_id = $this->Finance_model->read_bankcash_information($this->input->post('bank_cash_id'));

				$acc_balance = $account_id[0]->account_balance + $this->input->post('amount');



				$data3 = array(

					'account_balance' => $acc_balance

				);

				$this->Finance_model->update_bankcash_record($data3,$this->input->post('bank_cash_id'));

				$data = array(

					'status' => 1,

				);

				$result = $this->Invoices_model->update_invoice_record($data,$invoice_id);



				$Return['result'] = $this->lang->line('xin_acc_success_deposit_added');

			} else {

				$Return['error'] = $this->lang->line('xin_error');

			}

			$this->output($Return);

			exit;





		}

	}

	// Validate and add info in database

	public function add_direct_invoice_payment() {



		if($this->input->post('add_type')=='invoice_payment') {		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			

			/* Server side PHP input validation */

			$description = $this->input->post('description');

			$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);



			$invoice_tr = $this->Finance_model->read_invoice_transaction($this->input->post('invoice_id'));

			if ($invoice_tr->num_rows() > 0) {

				$Return['error'] = $this->lang->line('xin_acc_inv_paid_already');

			} 

			if($Return['error']!=''){

				$this->output($Return);

			}

			if($this->input->post('bank_cash_id')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_account_field');

			} else if($this->input->post('amount')==='') {

				$Return['error'] = $this->lang->line('xin_error_amount_field');

			} else if($this->input->post('add_invoice_date')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_deposit_date');

			} else if($this->input->post('payment_method')==='') {

				$Return['error'] = $this->lang->line('xin_error_makepayment_payment_method');

			}



			if($Return['error']!=''){

				$this->output($Return);

			}

			$invoice_id = $this->input->post('invoice_id');

			$data = array(

				'account_id' => $this->input->post('bank_cash_id'),

				'amount' => $this->input->post('amount'),

				'transaction_type' => 'income',

				'invoice_type' => 'direct',

				'dr_cr' => 'dr',

				'transaction_date' => $this->input->post('add_invoice_date'),

				'attachment_file' => '',

				'transaction_cat_id' => $this->input->post('category_id'),

				'payer_payee_id' => $this->input->post('payer_id'),

				'payment_method_id' => $this->input->post('payment_method'),

				'description' => $qt_description,

				'reference' => $this->input->post('reference'),

				'invoice_id' => $invoice_id,

				'created_at' => date('Y-m-d H:i:s')

			);

			$result = $this->Finance_model->add_transactions($data);

			if ($result == TRUE) {			

			// add data to bank account>cr

				$account_id = $this->Finance_model->read_bankcash_information($this->input->post('bank_cash_id'));

				$acc_balance = $account_id[0]->account_balance + $this->input->post('amount');



				$data3 = array(

					'account_balance' => $acc_balance

				);

				$this->Finance_model->update_bankcash_record($data3,$this->input->post('bank_cash_id'));

				$data = array(

					'status' => 1,

				);

				$result = $this->Invoices_model->update_direct_invoice_record($data,$invoice_id);



				$Return['result'] = $this->lang->line('xin_acc_success_deposit_added');

			} else {

				$Return['error'] = $this->lang->line('xin_error');

			}

			$this->output($Return);

			exit;





		}

	}

	

	// Validate and add info in database

	public function add_transfer() {



		if($this->input->post('add_type')=='transfer') {		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			

			/* Server side PHP input validation */

			$description = $this->input->post('description');

			

			$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);

			if ($this->input->post('select_type')==='') 
			{
				# code...
				$Return['error'] = "Type required";
			
			} else if($this->input->post('select_type') == '1')
			{
				

				if($this->input->post('from_bank_cash_id')==='') {

					$Return['error'] = $this->lang->line('xin_acc_error_from_account');

				} else if($this->input->post('to_bank_cash_id')==='') {

					$Return['error'] = $this->lang->line('xin_acc_error_to_account');

				} else if($this->input->post('from_bank_cash_id')== $this->input->post('to_bank_cash_id')) {

					$Return['error'] = $this->lang->line('xin_acc_error_transer_to_same_account');
				}
			
				$balance = $this->input->post('account_balance');

				$balance = str_replace(",", '', $balance);


		
			} else if($this->input->post('select_type') == '2')
			{
				$from_ledger = explode("-", $this->input->post('from_ledger_id'));
				
				$to_ledger = explode("-", $this->input->post('to_ledger_id'));

				if($this->input->post('from_ledger_id')==='') {

					$Return['error'] = "Select from ledger";

				} else if($this->input->post('to_ledger_id')==='') {

					$Return['error'] = "Select to ledger";

				} else if($this->input->post('from_ledger_id')== $this->input->post('to_ledger_id')) {

					$Return['error'] = $this->lang->line('xin_acc_error_transer_to_same_account');
				} else if($from_ledger[1] != $to_ledger[1]){

					$Return['error'] = "Ledger type is not the same";

				}		

				$balance = $this->input->post('ledger_balance');

				$balance = str_replace(",", '', $balance);	

			} 

			if($Return['error']!=''){

				$this->output($Return);

			}

			if($this->input->post('transfer_date')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_transer_date');

			} else if($this->input->post('amount')==='') {

				$Return['error'] = $this->lang->line('xin_error_amount_field');

			} else if($this->input->post('amount') > $balance) {

				$Return['error'] = $this->lang->line('xin_acc_error_amount_should_be_less_than_current')." Balance: ".$balance;

			} else if($this->input->post('payment_method')==='') {

				$Return['error'] = "Select a payment method";

			}



			if($Return['error']!=''){

				$this->output($Return);

			}

			if ($this->input->post('select_type') == '1') {
				// Account
				$from_account_id = $this->Finance_model->read_bankcash_information($this->input->post('from_bank_cash_id'));

				$frm_acc = $from_account_id[0]->account_balance - $this->input->post('amount');



				$to_bank_cash_id = $this->Finance_model->read_bankcash_information($this->input->post('to_bank_cash_id'));

				$to_acc = $to_bank_cash_id[0]->account_balance + $this->input->post('amount');



				$data = array(

					'account_id' => $this->input->post('from_bank_cash_id'),

					'amount' => $this->input->post('amount'),

					'transaction_type' => 'transfer',

					'dr_cr' => 'cr',

					'transaction_date' => $this->input->post('transfer_date'),

					'attachment_file' => '',

					'transaction_cat_id' => 0,

					'payer_payee_id' => 0,

					'payment_method_id' => $this->input->post('payment_method'),

					'description' => $qt_description,

					'reference' => $this->input->post('transfer_reference'),

					'invoice_id' => 0,

					'created_at' => date('Y-m-d H:i:s')

				);

				$result = $this->Finance_model->add_transactions($data);

				$data2 = array(

					'account_balance' => $frm_acc

				);

				$result2 = $this->Finance_model->update_bankcash_record($data2,$this->input->post('from_bank_cash_id'));



				$data3 = array(

					'account_balance' => $to_acc

				);

				$result3 = $this->Finance_model->update_bankcash_record($data3,$this->input->post('to_bank_cash_id'));

				// $result = FALSE;

				if ($result == TRUE) {



					$data4 = array(

						'account_id' => $this->input->post('to_bank_cash_id'),

						'amount' => $this->input->post('amount'),

						'transaction_type' => 'transfer',

						'dr_cr' => 'dr',

						'transaction_date' => $this->input->post('transfer_date'),

						'attachment_file' => '',

						'transaction_cat_id' => 0,

						'payer_payee_id' => 0,

						'payment_method_id' => $this->input->post('payment_method'),

						'description' => $qt_description,

						'reference' => $this->input->post('transfer_reference'),

						'invoice_id' => 0,

						'created_at' => date('Y-m-d H:i:s')

					);

					$result4 = $this->Finance_model->add_transactions($data4);



					$Return['result'] = $this->lang->line('xin_acc_success_transfer_added');

				} else {

					$Return['error'] = $this->lang->line('xin_error');

				}
			
			}else{
				// Ledger
				$from_ledger = explode("-", $this->input->post('from_ledger_id'));
				$to_ledger = explode("-", $this->input->post('to_ledger_id'));

				if ($from_ledger[1] == 'i' AND $to_ledger[1] == 'i') {
					//income
					$ledger_type = 'i';

					$from_account_id = $this->Finance_model->read_deposit_type($from_ledger[0]);

					$frm_acc = $from_account_id[0]->balance - $this->input->post('amount');



					$to_bank_cash_id = $this->Finance_model->read_deposit_type($to_ledger[0]);

					$to_acc = $to_bank_cash_id[0]->balance + $this->input->post('amount');


				}else{
					//expemse
					$ledger_type = 'e';
				
					$from_account_id = $this->Finance_model->read_expense_type($from_ledger[0]);

					$frm_acc = $from_account_id[0]->balance - $this->input->post('amount');



					$to_bank_cash_id = $this->Finance_model->read_expense_type($to_ledger[0]);

					$to_acc = $to_bank_cash_id[0]->balance + $this->input->post('amount');

				}

				$data = array(

					'account_id' => $from_ledger[0],

					'amount' => $this->input->post('amount'),

					'transaction_type' => 'transfer-ledger-'.$ledger_type,

					'dr_cr' => 'cr',

					'transaction_date' => $this->input->post('transfer_date'),

					'attachment_file' => '',

					'transaction_cat_id' => 0,

					'payer_payee_id' => 0,

					'payment_method_id' => $this->input->post('payment_method'),

					'description' => $qt_description,

					'reference' => $this->input->post('transfer_reference'),

					'invoice_id' => 0,

					'created_at' => date('Y-m-d H:i:s')

				);

				$result = $this->Finance_model->add_transactions($data);

				// $result = TRUE;

				if ($result == TRUE) {

					if ($ledger_type == 'i') {
						//Income
						$data2 = array(

							'balance' => $frm_acc

						);

						$result2 = $this->Finance_model->update_deposit_record($data2,$from_ledger[0]);



						$data3 = array(

							'balance' => $to_acc

						);

						$result3 = $this->Finance_model->update_deposit_record($data3,$to_ledger[0]);


					}else{
						//Expense

						//Income
						$data2 = array(

							'balance' => $frm_acc

						);

						$result2 = $this->Finance_model->update_expense_type_record($data2,$from_ledger[0]);

						$data3 = array(

							'balance' => $to_acc

						);

						$result3 = $this->Finance_model->update_expense_type_record($data3,$to_ledger[0]);						

					}


					$data4 = array(

						'account_id' => $to_ledger[0],

						'amount' => $this->input->post('amount'),

						'transaction_type' => 'transfer-ledger-'.$ledger_type,

						'dr_cr' => 'dr',

						'transaction_date' => $this->input->post('transfer_date'),

						'attachment_file' => '',

						'transaction_cat_id' => 0,

						'payer_payee_id' => 0,

						'payment_method_id' => $this->input->post('payment_method'),

						'description' => $qt_description,

						'reference' => $this->input->post('transfer_reference'),

						'invoice_id' => 0,

						'created_at' => date('Y-m-d H:i:s')

					);

					$result4 = $this->Finance_model->add_transactions($data4);



					$Return['result'] = $this->lang->line('xin_acc_success_transfer_added');

				} else {

					$Return['error'] = $this->lang->line('xin_error');

				}
			
			}			

			$this->output($Return);

			exit;

		}

	}

	

	// Validate and add info in database> add bank-cash

	public function add_bankcash() {



		if($this->input->post('add_type')=='bankcash') {		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();



			$bank_branch = $this->input->post('bank_branch');

			$qt_bank_branch = htmlspecialchars(addslashes($bank_branch), ENT_QUOTES);

			

			/* Server side PHP input validation */

			if($this->input->post('account_name')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_account_name_field');

			} else if($this->input->post('account_balance')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_account_balance_field');

			} else if($this->input->post('account_number')==='') {

				$Return['error'] = $this->lang->line('xin_employee_error_acc_number');

			}



			if($Return['error']!=''){

				$this->output($Return);

			}



			$data = array(

				'account_name' => $this->input->post('account_name'),

				'account_balance' => $this->input->post('account_balance'),

				'account_opening_balance' => $this->input->post('account_balance'),

				'account_number' => $this->input->post('account_number'),

				'branch_code' => $this->input->post('branch_code'),

				'bank_branch' => $qt_bank_branch,

				'created_at' => date('d-m-Y h:i:s'),



			);

			$result = $this->Finance_model->add_bankcash($data);

			if ($result == TRUE) {

				$Return['result'] = $this->lang->line('xin_acc_success_bank_cash_added');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	} 

	

	// Validate and add info in database> add bank-cash

	public function bankcash_update() {



		if($this->input->post('edit_type')=='bankcash') {		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$id = $this->uri->segment(4);

			$Return['csrf_hash'] = $this->security->get_csrf_hash();



			$bank_branch = $this->input->post('bank_branch');

			$qt_bank_branch = htmlspecialchars(addslashes($bank_branch), ENT_QUOTES);

			

			/* Server side PHP input validation */

			if($this->input->post('account_name')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_account_name_field');

			} else if($this->input->post('account_balance')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_account_balance_field');

			} else if($this->input->post('account_number')==='') {

				$Return['error'] = $this->lang->line('xin_employee_error_acc_number');

			} 



			if($Return['error']!=''){

				$this->output($Return);

			}



			$data = array(

				'account_name' => $this->input->post('account_name'),

				'account_balance' => $this->input->post('account_balance'),

				'account_number' => $this->input->post('account_number'),

				'branch_code' => $this->input->post('branch_code'),

				'bank_branch' => $qt_bank_branch,

			);

			$result = $this->Finance_model->update_bankcash_record($data,$id);

			if ($result == TRUE) {

				$Return['result'] = $this->lang->line('xin_acc_success_bank_cash_updated');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	} 



	 // delete record

	public function delete() {

		

		if($this->input->post('is_ajax')=='2') {

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$id = $this->uri->segment(4);

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			$result = $this->Finance_model->delete_bankcash_record($id);

			if(isset($id)) {

				$Return['result'] = $this->lang->line('xin_acc_success_bank_cash_deleted');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

		}

	}

	

	// delete record

	public function delete_deposit_transaction() {

		

		if($this->input->post('is_ajax')=='2') {

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$id = $this->uri->segment(4);

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			$result = $this->Finance_model->delete_transaction_record($id);

			if(isset($id)) {

				$Return['result'] = $this->lang->line('xin_acc_success_deposit_deleted');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

		}

	}

	

	// delete record

	public function delete_expense() {

		

		if($this->input->post('is_ajax')=='2') {

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$id = $this->uri->segment(4);

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			$result = $this->Finance_model->delete_transaction_record($id);

			if(isset($id)) {

				$Return['result'] = $this->lang->line('xin_acc_success_expense_deleted');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

		}

	}

	// delete record

	public function delete_vendors() {
		

		if($this->input->post('is_ajax')=='2') {

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$id = $this->uri->segment(4);

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			$result = $this->Finance_model->delete_vendors_record($id);

			if(isset($id)) {

				$Return['result'] = $this->lang->line('xin_vendor_deleted');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

		}

	}

	

	// delete record

	public function delete_transaction() {

		

		if($this->input->post('is_ajax')=='2') {

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$id = $this->uri->segment(4);

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			$result = $this->Finance_model->delete_transaction_record($id);

			if(isset($id)) {

				$Return['result'] = $this->lang->line('xin_acc_transaction_deleted');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

		}

	}

	

	public function read_invoice()

	{

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		$invoice_id = $this->input->get('invoice_id');

		$invoice_type = $this->input->get('data');

		if($invoice_type == 'customer'){

			$result = $this->Invoices_model->read_invoice_info($invoice_id);

		} else {

			$result = $this->Invoices_model->read_direct_invoice_info($invoice_id);

		}

		$data = array(

			'invoice_id' => $result[0]->invoice_id,

			'invoice_number' => $result[0]->invoice_number,

			'grand_total' => $result[0]->grand_total,

			'all_payers' => $this->Customers_model->get_all_customers(),

			'all_bank_cash' => $this->Finance_model->all_bank_cash(),

			'all_income_categories_list' => $this->Finance_model->all_income_categories_list(),

			'get_all_payment_method' => $this->Finance_model->get_all_payment_method()

		);

		if(!empty($session)){ 

			$this->load->view('admin/accounting/dialog_accounting', $data);

		} else {

			redirect('admin/');

		}

	}

	

	public function read_deposit()

	{

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		$id = $this->input->get('deposit_id');

		$result = $this->Finance_model->read_transaction_information($id);

		$data = array(

			'deposit_id' => $result[0]->transaction_id,

			'account_type_id' => $result[0]->account_id,

			'amount' => $result[0]->amount,

			'deposit_date' => $result[0]->transaction_date,

			'categoryid' => $result[0]->transaction_cat_id,
			
			'company_id' => $result[0]->company_id,

			'payer_id' => $result[0]->payer_payee_id,

			'sub_deposit_ids' => explode(",", $result[0]->sub_deposit_ids),

			'payment_method_id' => $result[0]->payment_method_id,

			'deposit_reference' => $result[0]->reference,

			'deposit_file' => $result[0]->attachment_file,

			'description' => $result[0]->description,

			'created_at' => $result[0]->created_at,

			'all_payers' => $this->Finance_model->all_payers(),

			'all_bank_cash' => $this->Finance_model->all_bank_cash(),

			'all_income_categories_list' => $this->Finance_model->all_income_categories_list(),

			'get_all_payment_method' => $this->Finance_model->get_all_payment_method(),
			
			'all_companies' => $this->Xin_model->get_companies(),

			'sub_deposit' => $this->Xin_model->get_sub_deposit_type()->result()

		);

		if(!empty($session)){ 

			$this->load->view('admin/accounting/dialog_accounting', $data);

		} else {

			redirect('admin/');

		}

	}

	public function read_sub_deposit()

	{

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		$id = $this->input->get('id');

		$result = $this->Xin_model->get_deposit_type_by_id($id)->result();

		$html = "";

		foreach ($result as $key => $value) {
			$html .= "
			<option value=\"".$value->id."\">".$value->name."</option>
			";
		}

		echo $html;

	}


	

	// Validate and update info in database

	public function deposit_update() {



		if($this->input->post('edit_type')=='deposit') {

			

			$id = $this->uri->segment(4);



			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			

			/* Server side PHP input validation */

			$description = $this->input->post('description');

			$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);



			if($this->input->post('bank_cash_id')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_account_field');

			} else if($this->input->post('amount')==='') {

				$Return['error'] = $this->lang->line('xin_error_amount_field');

			} else if($this->input->post('deposit_date')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_deposit_date');

			}		

			/* Check if file uploaded..*/

			else if($_FILES['deposit_file']['size'] == 0) {

				$fname = 'no_file';

				$data = array(

					'account_id' => $this->input->post('bank_cash_id'),

					'amount' => $this->input->post('amount'),

					'transaction_date' => $this->input->post('deposit_date'),

					'transaction_cat_id' => $this->input->post('category_id'),

					'payer_payee_id' => $this->input->post('payer_id'),

					'payment_method_id' => $this->input->post('payment_method'),

					'description' => $qt_description,

					'reference' => $this->input->post('deposit_reference'),		

				);

				$result = $this->Finance_model->update_transaction_record($data,$id);

			} else {

				if(is_uploaded_file($_FILES['deposit_file']['tmp_name'])) {

				//checking image type

					$allowed =  array('png','jpg','jpeg','gif');

					$filename = $_FILES['deposit_file']['name'];

					$ext = pathinfo($filename, PATHINFO_EXTENSION);



					if(in_array($ext,$allowed)){

						$tmp_name = $_FILES["deposit_file"]["tmp_name"];

						$bill_copy = "uploads/accounting/deposit/";

					// basename() may prevent filesystem traversal attacks;

					// further validation/sanitation of the filename may be appropriate

						$lname = basename($_FILES["deposit_file"]["name"]);

						$newfilename = 'deposit_'.round(microtime(true)).'.'.$ext;

						move_uploaded_file($tmp_name, $bill_copy.$newfilename);

						$fname = $newfilename;

						$data = array(

							'account_id' => $this->input->post('bank_cash_id'),

							'amount' => $this->input->post('amount'),

							'transaction_date' => $this->input->post('deposit_date'),

							'attachment_file' => $fname,

							'transaction_cat_id' => $this->input->post('category_id'),

							'payer_payee_id' => $this->input->post('payer_id'),

							'payment_method_id' => $this->input->post('payment_method'),

							'description' => $qt_description,

							'reference' => $this->input->post('deposit_reference'),	

						);

					// update record > model

						$result = $this->Finance_model->update_transaction_record($data,$id);

					} else {

						$Return['error'] = $this->lang->line('xin_error_attatchment_type');

					}

				}

			}



			if($Return['error']!=''){

				$this->output($Return);

			}





			if ($result == TRUE) {

				$Return['result'] = $this->lang->line('xin_acc_success_deposit_updated');

			} else {

				$Return['error'] = $this->lang->line('xin_error');

			}

			$this->output($Return);

			exit;

		}

	}

	

	// Validate and update info in database

	public function transaction_update() {



		if($this->input->post('edit_type')=='deposit') {

			

			$id = $this->uri->segment(4);

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			

			/* Server side PHP input validation */

			$description = $this->input->post('description');

			$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);



			if($this->input->post('deposit_date')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_deposit_date');

			}		

			/* Check if file uploaded..*/

			else if($_FILES['deposit_file']['size'] == 0) {

				$fname = 'no_file';

				$data = array(

					'transaction_date' => $this->input->post('deposit_date'),

					'transaction_cat_id' => $this->input->post('category_id'),

					'payer_payee_id' => $this->input->post('payer_id'),

					'payment_method_id' => $this->input->post('payment_method'),

					'description' => $qt_description,

					'reference' => $this->input->post('deposit_reference'),		

				);

				$result = $this->Finance_model->update_transaction_record($data,$id);

			} else {

				if(is_uploaded_file($_FILES['deposit_file']['tmp_name'])) {

				//checking image type

					$allowed =  array('png','jpg','jpeg','gif');

					$filename = $_FILES['deposit_file']['name'];

					$ext = pathinfo($filename, PATHINFO_EXTENSION);



					if(in_array($ext,$allowed)){

						$tmp_name = $_FILES["deposit_file"]["tmp_name"];

						$bill_copy = "uploads/accounting/deposit/";

					// basename() may prevent filesystem traversal attacks;

					// further validation/sanitation of the filename may be appropriate

						$lname = basename($_FILES["deposit_file"]["name"]);

						$newfilename = 'deposit_'.round(microtime(true)).'.'.$ext;

						move_uploaded_file($tmp_name, $bill_copy.$newfilename);

						$fname = $newfilename;

						$data = array(

							'transaction_date' => $this->input->post('deposit_date'),

							'attachment_file' => $fname,

							'transaction_cat_id' => $this->input->post('category_id'),

							'payer_payee_id' => $this->input->post('payer_id'),

							'payment_method_id' => $this->input->post('payment_method'),

							'description' => $qt_description,

							'reference' => $this->input->post('deposit_reference'),	

						);

					// update record > model

						$result = $this->Finance_model->update_transaction_record($data,$id);

					} else {

						$Return['error'] = $this->lang->line('xin_error_attatchment_type');

					}

				}

			}



			if($Return['error']!=''){

				$this->output($Return);

			}





			if ($result == TRUE) {

				$Return['result'] = $this->lang->line('xin_inv_transaction_edited_successfully');

			} else {

				$Return['error'] = $this->lang->line('xin_error');

			}

			$this->output($Return);

			exit;

		}

	}

	

	public function read_expense()

	{

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		$id = $this->input->get('expense_id');

		$result = $this->Finance_model->read_transaction_information($id);

		$data = array(

			'expense_id' => $result[0]->transaction_id,

			'account_type_id' => $result[0]->account_id,

			'amount' => $result[0]->amount,

			'expense_date' => $result[0]->transaction_date,

			'categoryid' => $result[0]->transaction_cat_id,

			'payee_id' => $result[0]->
			_payee_id,

			'company_id' => $result[0]->company_id,
			
			'vendor_id' => $result[0]->vendor_id,
			
			'subexpenses' => explode(",", $result[0]->subexpenses),

			'payment_method_id' => $result[0]->payment_method_id,

			'expense_reference' => $result[0]->reference,

			'expense_file' => $result[0]->attachment_file,

			'description' => $result[0]->description,

			'created_at' => $result[0]->created_at,

			'all_payees' => $this->Finance_model->all_payees(),

			'all_bank_cash' => $this->Finance_model->all_bank_cash(),

			'all_expense_types' => $this->Expense_model->all_expense_types(),

			'all_income_categories_list' => $this->Finance_model->all_income_categories_list(),

			'get_all_payment_method' => $this->Finance_model->get_all_payment_method(),

			'all_employees' => $this->Xin_model->all_employees(),

			'all_companies' => $this->Xin_model->get_companies(),

			'all_vendors' => $this->Finance_model->get_vendors()->result(),

  			'sub_expense' => $this->Xin_model->get_sub_expense_type()->result()

		);

		if(!empty($session)){ 

			$this->load->view('admin/accounting/dialog_accounting', $data);

		} else {

			redirect('admin/');

		}

	}

	public function read_vendors()

	{

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		$id = $this->input->get('vendors_id');

		$result = $this->Finance_model->read_vendor_information($id);

		$data = array(

			'vendors_id' => $result[0]->id,

			'vendor_name' => $result[0]->name,

			'phone_number' => $result[0]->phone,

			'address' => $result[0]->address,

			'rc_number' => $result[0]->rc_number,

			'description' => $result[0]->description,

		);

		if(!empty($session)){ 

			$this->load->view('admin/accounting/dialog_accounting', $data);

		} else {

			redirect('admin/');

		}

	}

	

	// Validate and update info in database

	public function expense_update() {



		if($this->input->post('edit_type')=='expense') {

			

			$id = $this->uri->segment(4);



			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			

			/* Server side PHP input validation */

			$description = $this->input->post('description');

			$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);



			if($this->input->post('bank_cash_id')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_account_field');

			} else if($this->input->post('amount')==='') {

				$Return['error'] = $this->lang->line('xin_error_amount_field');

		} /*else if($this->input->post('amount') > $this->input->post('account_balance')) {

			$Return['error'] = $this->lang->line('xin_acc_error_amount_should_be_less_than_current');

		}*/ else if($this->input->post('expense_date')==='') {

			$Return['error'] = $this->lang->line('xin_acc_error_expense_date');

		} else if($this->input->post('company')==='') {

			$Return['error'] = $this->lang->line('error_company_field');

		} else if($this->input->post('employee_id')==='') {

			$Return['error'] = $this->lang->line('xin_error_employee_id');

		}	

		/* Check if file uploaded..*/

		else if($_FILES['expense_file']['size'] == 0) {

			$fname = 'no_file';

			$data = array(

				'account_id' => $this->input->post('bank_cash_id'),

				'amount' => $this->input->post('amount'),

				'transaction_date' => $this->input->post('expense_date'),

				'transaction_cat_id' => $this->input->post('category_id'),

				'payer_payee_id' => $this->input->post('payee_id'),

				'company_id' => $this->input->post('company'),
	
				'vendor_id' => $this->input->post('vendor_id'),

				'subexpenses' => implode(",", $this->input->post('subexpense_category')),

				'payment_method_id' => $this->input->post('payment_method'),

				'description' => $qt_description,

				'reference' => $this->input->post('expense_reference'),		

			);

			$result = $this->Finance_model->update_transaction_record($data,$id);

		} else {

			if(is_uploaded_file($_FILES['expense_file']['tmp_name'])) {

				//checking image type

				$allowed =  array('png','jpg','jpeg','gif');

				$filename = $_FILES['expense_file']['name'];

				$ext = pathinfo($filename, PATHINFO_EXTENSION);

				

				if(in_array($ext,$allowed)){

					$tmp_name = $_FILES["expense_file"]["tmp_name"];

					$bill_copy = "uploads/accounting/deposit/";

					// basename() may prevent filesystem traversal attacks;

					// further validation/sanitation of the filename may be appropriate

					$lname = basename($_FILES["expense_file"]["name"]);

					$newfilename = 'expense_'.round(microtime(true)).'.'.$ext;

					move_uploaded_file($tmp_name, $bill_copy.$newfilename);

					$fname = $newfilename;

					$data = array(

						'account_id' => $this->input->post('bank_cash_id'),

						'amount' => $this->input->post('amount'),

						'transaction_date' => $this->input->post('expense_date'),

						'attachment_file' => $fname,

						'transaction_cat_id' => $this->input->post('category_id'),

						'payer_payee_id' => $this->input->post('payee_id'),

						'company_id' => $this->input->post('company'),

						'vendor_id' => $this->input->post('vendor_id'),
						
						'subexpenses' => implode(",", $this->input->post('subexpense_category')),

						'payment_method_id' => $this->input->post('payment_method'),

						'description' => $qt_description,

						'reference' => $this->input->post('expense_reference'),	

					);

					// update record > model

					$result = $this->Finance_model->update_transaction_record($data,$id);

				} else {

					$Return['error'] = $this->lang->line('xin_error_attatchment_type');

				}

			}

		}

		

		if($Return['error']!=''){

			$this->output($Return);

		}

		

		if ($result == TRUE) {

			$Return['result'] = $this->lang->line('xin_acc_success_expense_updated');

		} else {

			$Return['error'] = $this->lang->line('xin_error');

		}

		$this->output($Return);

		exit;

	}

}

public function vendors_update() 
{	

	$id = $this->uri->segment(4);



	/* Define return | here result is used to return user data and error for error message */

	$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

	$Return['csrf_hash'] = $this->security->get_csrf_hash();




	/* Server side PHP input validation */

	if($this->input->post('vendor_name')==='') {

		$Return['error'] = $this->lang->line('xin_vendor_name_error');

	} else if($this->input->post('phone_number')==='') {

		$Return['error'] = $this->lang->line('xin_phone_number_error');

	} else if($this->input->post('address')==='') {

		$Return['error'] = $this->lang->line('xin_address_error');

	} else if($this->input->post('rc_number')==='') {

		$Return['error'] = $this->lang->line('xin_rc_number_error');

	} else if($this->input->post('description')==='') {

		$Return['error'] = $this->lang->line('xin_description_error');

	}

		// /* Check if file uploaded..*/

		// else if($_FILES['expense_file']['size'] == 0) {

		// 	 $fname = 'no_file';

		// 	 $data = array(

		// 	'account_id' => $this->input->post('bank_cash_id'),

		// 	'amount' => $this->input->post('amount'),

		// 	'transaction_date' => $this->input->post('expense_date'),

		// 	'transaction_cat_id' => $this->input->post('category_id'),

		// 	'payer_payee_id' => $this->input->post('payee_id'),

		// 	'company_id' => $this->input->post('company'),

		// 	'payment_method_id' => $this->input->post('payment_method'),

		// 	'description' => $qt_description,

		// 	'reference' => $this->input->post('expense_reference'),		

		// 	);

		// 	 $result = $this->Finance_model->update_transaction_record($data,$id);

		// } 

		// else {

		// 	if(is_uploaded_file($_FILES['expense_file']['tmp_name'])) {

		// 		//checking image type

		// 		$allowed =  array('png','jpg','jpeg','gif');

		// 		$filename = $_FILES['expense_file']['name'];

		// 		$ext = pathinfo($filename, PATHINFO_EXTENSION);



		// 		if(in_array($ext,$allowed)){

		// 			$tmp_name = $_FILES["expense_file"]["tmp_name"];

		// 			$bill_copy = "uploads/accounting/deposit/";

		// 			// basename() may prevent filesystem traversal attacks;

		// 			// further validation/sanitation of the filename may be appropriate

		// 			$lname = basename($_FILES["expense_file"]["name"]);

		// 			$newfilename = 'expense_'.round(microtime(true)).'.'.$ext;

		// 			move_uploaded_file($tmp_name, $bill_copy.$newfilename);

		// 			$fname = $newfilename;

		// 			 $data = array(

		// 			'account_id' => $this->input->post('bank_cash_id'),

		// 			'amount' => $this->input->post('amount'),

		// 			'transaction_date' => $this->input->post('expense_date'),

		// 			'attachment_file' => $fname,

		// 			'transaction_cat_id' => $this->input->post('category_id'),

		// 			'payer_payee_id' => $this->input->post('payee_id'),

		// 			'company_id' => $this->input->post('company'),

		// 			'payment_method_id' => $this->input->post('payment_method'),

		// 			'description' => $qt_description,

		// 			'reference' => $this->input->post('expense_reference'),	

		// 			);

		// 			// update record > model

		// 			$result = $this->Finance_model->update_transaction_record($data,$id);

		// 		} else {

		// 			$Return['error'] = $this->lang->line('xin_error_attatchment_type');

		// 		}

		// 	}

		// }



	if($Return['error']!=''){

		$this->output($Return);

	}

	$data = array(

		'name' => $this->input->post('vendor_name'),

		'phone' => $this->input->post('phone_number'),

		'address' => $this->input->post('address'),

		'rc_number' => $this->input->post('rc_number'),

		'description' => $this->input->post('description'),

	);

		// update record > model

	$result = $this->Finance_model->update_vendor_record($data,$id);		

	if ($result == TRUE) {

		$Return['result'] = $this->lang->line('xin_vendor_updated');

	} else {

		$Return['error'] = $this->lang->line('xin_error');

	}

	$this->output($Return);

	exit;

}



	/****AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA*/////	

	public function accounts_ledger() {



		$id = $this->uri->segment(4);

		$bac_id = $this->Finance_model->read_transaction_by_bank_info($id);

		if(is_null($bac_id)){

			redirect('admin/accounting/transactions');

		}

		$system = $this->Xin_model->read_setting_info(1);

		$data['title'] = $this->lang->line('xin_acc_ledger_account').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_acc_ledger_account');

		$data['path_url'] = 'accounting_bankwise_transactions';

		$session = $this->session->userdata('username');

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(!empty($session)){ 

			if(in_array('4',$role_resources_ids)) {

				$data['subview'] = $this->load->view("admin/accounting/ledger_account_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/dashboard');

			}

		} else {

			redirect('admin/');

		}

	}

	

	public function ledger_accounts() {



		$system = $this->Xin_model->read_setting_info(1);

		$data['title'] = $this->lang->line('xin_acc_ledger_account').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_acc_ledger_account');

		$data['path_url'] = 'xin_ledger_accounts';

		$session = $this->session->userdata('username');

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(!empty($session)){ 

			if(in_array('4',$role_resources_ids)) {

				$data['subview'] = $this->load->view("admin/accounting/full_ledger_account_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/dashboard');

			}

		} else {

			redirect('admin/');

		}

	}

	

	public function account_statement() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_acc_account_statement').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_acc_account_statement');

		$data['path_url'] = 'accounting_report_statement';

		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

		$data['all_income_categories_list'] = $this->Finance_model->all_income_categories_list();

		$data['get_all_payment_method'] = $this->Finance_model->get_all_payment_method();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('83',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/report_account_statement", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	

	public function expense_report() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_acc_expense_reports').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_acc_expense_reports');

		$data['path_url'] = 'accounting_report_expense';

		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

		$data['all_companies'] = $this->Xin_model->get_companies();

		$data['all_expense_types'] = $this->Expense_model->all_expense_types();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('84',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/report_expense", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	

	public function income_report() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_acc_income_reports').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_acc_income_reports');

		$data['path_url'] = 'accounting_report_income';

		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

		$data['all_income_categories_list'] = $this->Finance_model->all_income_categories_list();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('85',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/report_income", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function staff_income_report() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = 'Staff '.$this->lang->line('xin_acc_income_reports').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = 'Staff '.$this->lang->line('xin_acc_income_reports');

		$data['path_url'] = 'accounting_report_staff_income';

		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

		$data['all_income_categories_list'] = $this->Finance_model->all_income_categories_list();
		
		$data['all_staff'] = $this->Xin_model->all_staff();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('85',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/report_staff_income", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function profit_loss_report() {	

		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = "Profit Loss Report".' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = "Profit Loss Report";

		$data['path_url'] = 'accounting_report_profit_loss';

		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

		$data['all_income_categories_list'] = $this->Finance_model->all_income_categories_list();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('85',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/report_profit_loss", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function trial_balance_report() {	

		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = "Trial Balance Report".' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = "Trial Balance Report";

		$data['path_url'] = 'accounting_report_trial_balance';

		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

		$data['all_income_categories_list'] = $this->Finance_model->all_income_categories_list();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('85',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/report_trial_balance", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function general_ledger_report() {	

		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = "General Ledger Report".' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = "General Ledger Report";

		$data['path_url'] = 'accounting_report_general_ledger';

		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

		$data['all_income_categories_list'] = $this->Finance_model->all_income_categories_list();

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('85',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/report_general_ledger", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}

	public function profit_loss_report_list()
	{
		$from = $this->input->get('from')." 00:00:01";	
		$to = $this->input->get('to')." 23:59:59";

		$total_invoice = all_invoice_paid_amount($from,$to);

		$income = $this->Finance_model->get_deposit_date_range($from,$to)->result();

		// echo $this->db->last_query();die;

		$total_income = array();

		foreach($income as $i)
		{
			$total_income[] = $i->amount;
		}

		$total_income = array_sum($total_income);

		$capitation_cost = $this->Xin_model->capitation_cost();
		$total_capitation = (total_capitation_principal($from,$to)+total_capitation_dependant($from,$to))*$capitation_cost[0]->cost;

		$total_revenue = $total_invoice+$total_capitation+$total_income;

		$bills = $this->Finance_model->get_all_bill_payments($from,$to);
		$total_bills = array();

		foreach($bills as $bill)
		{
			$diagnose = $this->Clients_model->get_diagnose($bill->newbill_tid)->result();

			if(!empty($diagnose))
			{
				$total_bills[] = $diagnose[0]->diagnose_total_sum;
			}

		}

		$total_bulk_bills = array();

		$bulk_bills = $this->Finance_model->get_all_bulk_bill_payments($from,$to);
		
		// print_r($bulk_bills);die;
		foreach($bulk_bills as $bulk_bill)
		{
			$ids = $this->Clients_model->get_bulk($bulk_bill->newbill_tid)->result();
			if (!empty($ids)) {
				$bulk_id = explode(",",$ids[0]->bulk_id);
				if (!empty($bulk_id)) {
					foreach($bulk_id as $id)
					{
						$diagnose = $this->Clients_model->get_diagnose($id)->result();

						if(!empty($diagnose))
						{
							$total_bulk_bills[] = $diagnose[0]->diagnose_total_sum;
						}					
					}
				}
			}
		}
		

		$hospital_expense = array_sum($total_bills) + array_sum($total_bulk_bills);

		$total_expense = total_expense($from,$to);

		$total_expenses = $hospital_expense+$hospital_expense+$total_expense;

		$grand_total = $total_revenue-$total_expenses;

		if ($grand_total < 0) {
			$class = 'text-red';
			$sign = '-';
		}else{
			$class = 'text-green';
			$sign = '';
		}

		$html = '';
		$html .= "<tr>
		<td colspan=\"2\">Revenue</td>
		</tr>
		<tr>
		<td>Premium Revenue <a class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"return loadModalView2('".$from."','".$to."');\"><i class=\"fa fa-eye\"></i></a></td>
		<td>".$this->Xin_model->currency_sign($total_invoice)."</td>
		</tr>
		<tr>
		<td>Other Revenue <a class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"return loadModalView3('".$from."','".$to."');\"><i class=\"fa fa-eye\"></i></a></td>
		<td>".$this->Xin_model->currency_sign($total_capitation)."</td>
		</tr>
		<tr>
		<td>Other Income <a class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"return loadModalView6('".$from."','".$to."');\"><i class=\"fa fa-eye\"></i></a></td>
		<td>".$this->Xin_model->currency_sign($total_income)."</td>
		</tr>
		<tr>
		<td>Total Revenue</td>
		<td>".$this->Xin_model->currency_sign($total_revenue)."</td>
		</tr>
		<tr>
		<td colspan=\"2\">Expenses</td>
		</tr>
		<tr>
		<td>Medical Expenses <a class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"return loadModalView4('".$from."','".$to."');\"><i class=\"fa fa-eye\"></i></a></td>
		<td>".$this->Xin_model->currency_sign($hospital_expense)."</td>
		</tr>
		<tr>
		<td>Claim Adjusment Expenses Revenue</td>
		<td>".$this->Xin_model->currency_sign($hospital_expense)."</td>
		</tr>
		<tr>
		<td>General Administrative Expenses <a class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"return loadModalView5('".$from."','".$to."');\"><i class=\"fa fa-eye\"></i></a></td>
		<td>".$this->Xin_model->currency_sign($total_expense)."</td>
		</tr>
		<tr>
		<td>Total Expenses</td>
		<td>".$this->Xin_model->currency_sign($total_expenses)."</td>
		</tr>
		<tr>
		<td>NET INCOME/LOSS</td>
		<td class='".$class."'>".$sign.$this->Xin_model->currency_sign($total_revenue-$total_expenses)."</td>
		</tr>";
		$html .= "
		<script>
			 function loadModalView2(from,to){
	          // alert(\"ID is: \" + id);
	          console.log('asd');

	          $.ajax({
	            url      : '".base_url('admin/Accounting/fetch_premium_id')."',
	            method   : 'post',   
	            dataType    : 'text',      
	            data     : {from: from, to: to},
	            success  : function(response){
	             
	              $(\"#fetched_data\").html(response);
	            }
	          });

	    	}
	    </script>
		";

		$html .= "
		<script>
			 function loadModalView3(from,to){
	          // alert(\"ID is: \" + id);
	          console.log('asd');

	          $.ajax({
	            url      : '".base_url('admin/Accounting/fetch_other_id')."',
	            method   : 'post',   
	            dataType    : 'text',      
	            data     : {from: from, to: to},
	            success  : function(response){
	             
	              $(\"#fetched_data\").html(response);
	            }
	          });

	    	}
	    </script>
		";

		$html .= "
		<script>
			 function loadModalView4(from,to){
	          // alert(\"ID is: \" + id);
	          console.log('asd');

	          $.ajax({
	            url      : '".base_url('admin/Accounting/fetch_medical_id')."',
	            method   : 'post',   
	            dataType    : 'text',      
	            data     : {from: from, to: to},
	            success  : function(response){
	             
	              $(\"#fetched_data\").html(response);
	            }
	          });

	    	}
	    </script>
		";

		$html .= "
		<script>
			 function loadModalView5(from,to){
	          // alert(\"ID is: \" + id);
	          console.log('asd');

	          $.ajax({
	            url      : '".base_url('admin/Accounting/fetch_expense_id')."',
	            method   : 'post',   
	            dataType    : 'text',      
	            data     : {from: from, to: to},
	            success  : function(response){
	             
	              $(\"#fetched_data\").html(response);
	            }
	          });

	    	}
	    </script>
		";

		$html .= "
		<script>
			 function loadModalView6(from,to){
	          // alert(\"ID is: \" + id);
	          console.log('asd');

	          $.ajax({
	            url      : '".base_url('admin/Accounting/fetch_income_id')."',
	            method   : 'post',   
	            dataType    : 'text',      
	            data     : {from: from, to: to},
	            success  : function(response){
	             
	              $(\"#fetched_data\").html(response);
	            }
	          });

	    	}
	    </script>
		";
		echo $html;	
	}
	

	public function transfer_report() {



		$session = $this->session->userdata('username');

		if(empty($session)){

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_accounting!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_acc_transfer_report').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_acc_transfer_report');

		$data['path_url'] = 'accounting_report_transfer';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('86',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/accounting/report_transfer", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

	}



	public function read_transfer()

	{

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		$id = $this->input->get('transfer_id');

		$result = $this->Finance_model->read_transfer_information($id);

		$data = array(

			'transfer_id' => $result[0]->transfer_id,

			'from_account_id' => $result[0]->from_account_id,

			'to_account_id' => $result[0]->to_account_id,

			'transfer_date' => $result[0]->transfer_date,

			'transfer_amount' => $result[0]->transfer_amount,

			'payment_method_id' => $result[0]->payment_method,

			'transfer_reference' => $result[0]->transfer_reference,

			'description' => $result[0]->description,

			'created_at' => $result[0]->created_at,

			'all_bank_cash' => $this->Finance_model->all_bank_cash(),

			'get_all_payment_method' => $this->Finance_model->get_all_payment_method()

		);

		if(!empty($session)){ 

			$this->load->view('admin/accounting/dialog_accounting', $data);

		} else {

			redirect('admin/');

		}

	}



	/****Reports ***/



	// report account statement > search

	public function report_statement_list() {



		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/report_account_statement", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));



		$transactions = $this->Finance_model->account_statement_search($this->input->get('from_date'),$this->input->get('to_date'),$this->input->get('account_id'));

		

		$data = array();

		$crd_total = 0; $deb_total = 0;$balance=0; $balance2 = 0;

		foreach($transactions->result() as $r) {



		   // transaction date

			$transaction_date = $this->Xin_model->set_date_format($r->transaction_date);

			// get currency

			$total_amount = $this->Xin_model->currency_sign($r->amount);

			$acc_type = $this->Finance_model->read_bankcash_information($r->account_id);

			if(!is_null($acc_type)){

				$account_balance = $acc_type[0]->account_opening_balance;

			} else {

				$account_balance = 0;	

			}

			

			if($r->dr_cr == 'cr') {

				$balance2 = $balance2 - $r->amount;

			} else {

				$balance2 = $balance2 + $r->amount;

			}

			if($r->credit!=0):

				$crd = $r->credit;

				$crd_total += $crd;

			else:

				$crd = 0;	

				$crd_total += $crd;

			endif;

			if($r->debit!=0):

				$deb = $r->debit;

				$deb_total += $deb;

			else:

				$deb = 0;	

				$deb_total += $deb;

			endif;

			//$fbalance = $account_balance + $balance2;

			

			$data[] = array(

				$transaction_date,

				$r->transaction_type,

				$r->description,

				$this->Xin_model->currency_sign($crd),

				$this->Xin_model->currency_sign($deb)

			);

		}



		$output = array(

			"draw" => $draw,

			"recordsTotal" => $transactions->num_rows(),

			"recordsFiltered" => $transactions->num_rows(),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	}



	 // get statement footer - data

	public function get_statement_footer() {



		$data['title'] = $this->Xin_model->site_title();

		

		$data = array(

			'from_date' => $this->input->get('from_date'),

			'to_date' => $this->input->get('to_date'),

			'account_id' => $this->input->get('account_id')

		);

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view("admin/accounting/footer/get_statement_footer", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

	}

	public function fetch_multiple_income() {

		$data['title'] = $this->Xin_model->site_title();

		// $data = array(

		// 	'from_date' => $this->input->get('from_date'),

		// 	'to_date' => $this->input->get('to_date'),

		// 	'account_id' => $this->input->get('account_id')

		// );

		$data['all_companies'] = $this->Xin_model->get_companies();

 		$data['all_payers'] = $this->Finance_model->all_payers();

 		$data['all_bank_cash'] = $this->Finance_model->all_bank_cash();

 		$data['all_income_categories_list'] = $this->Xin_model->get_deposit_type()->result();

 		$data['get_all_payment_method'] = $this->Finance_model->get_all_payment_method();

 		$data['sub_deposit'] = $this->Xin_model->get_sub_deposit_type()->result();

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view("admin/accounting/footer/get_multiple_income", $data);
			// $data['subview'] = $this->load->view("admin/accounting/footer/get_multiple_income", $data, TRUE);

			// $this->load->view('admin/layout/layout_main', $data); //page load

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

	}



	 // report expense list

	public function report_expense_list()

	{



		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/report_expense", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		

		$expense = $this->Finance_model->get_expense_search($this->input->get('from_date'),$this->input->get('to_date'),$this->input->get('type_id'),$this->input->get('company_id'));

		

		$data = array();



		foreach($expense->result() as $r) {



			// get amount

			$amount = $this->Xin_model->currency_sign($r->amount);

			// account type

			$acc_type = $this->Finance_model->read_bankcash_information($r->account_id);

			if(!is_null($acc_type)){

				$account = $acc_type[0]->account_name;

			} else {

				$account = '--';	

			}

			

			// get payee

			$payee = $this->Xin_model->read_user_info($r->payer_payee_id);

			// user full name

			if(!is_null($payee)){

				$full_name = $payee[0]->first_name.' '.$payee[0]->last_name;

			} else {

				$full_name = '--';	

			}



			// deposit date

			$expense_date = $this->Xin_model->set_date_format($r->transaction_date);

			// category

			$expense_type = $this->Expense_model->read_expense_type_information($r->transaction_cat_id);

			if(!is_null($expense_type)){

				$category = $expense_type[0]->name;

			} else {

				$category = '--';	

			}

			

			$data[] = array(

				$expense_date,

				$category,

				$full_name,

				$amount,

			);

		}



		$output = array(

			"draw" => $draw,

			"recordsTotal" => $expense->num_rows(),

			"recordsFiltered" => $expense->num_rows(),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	}



	 // get expense footer - data

	public function get_expense_footer() {



		$data['title'] = $this->Xin_model->site_title();

		

		$data = array(

			'from_date' => $this->input->get('from_date'),

			'to_date' => $this->input->get('to_date'),

			'type_id' => $this->input->get('type_id'),

			'company_id' => $this->input->get('company_id')

		);

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view("admin/accounting/footer/get_expense_footer", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

	}



	 // income report list

	public function report_income_list() 
	{



		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/report_income", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		$deposit_type = $this->input->get('type_id');

		$from_date = $this->input->get('from_date');

		$to_date = $this->input->get('to_date');

		$data = array();
		$subs = array();
		
		if (is_null($deposit_type)) {
			
			$output = array(

				"draw" => $draw,

				"recordsTotal" => 0,

				"recordsFiltered" => 0,

				"data" => $data

			);

			echo json_encode($output);

			exit();
		}
		else if($deposit_type == 'invoice')
		{
			$incomes = $this->Xin_model->get_all_invoice_income_transaction_filtered($from_date,$to_date);
			// print_r($incomes);die;
			foreach ($incomes as $r) 
				{
					$category = 'invoice';

					// account type

					$acc_type = $this->Finance_model->read_bankcash_information($r->account_id);

					if(!is_null($acc_type)){

						$account = $acc_type[0]->account_name;

					} else {

						$account = '--';	

					}

					$invoice = $this->Finance_model->read_invoice($r->invoice_id)->result();

					if (!empty($invoice)) {
						$organization = $this->Clients_model->get_organization_info($invoice[0]->company_name)->result();
						$organization = $organization[0]->name;
					}else{
						$organization = "-";
					}


					// get payee

					$payee = $this->Xin_model->read_user_info($r->payer_payee_id);

					// user full name

					if(!is_null($payee)){

						$full_name = $payee[0]->first_name.' '.$payee[0]->last_name;

					} else {

						$full_name = '--';	

					}



					// // get payee

					// $payer = $this->Finance_model->read_payer_info($r->payer_payee_id);

					// // user full name

					// if(!is_null($payer)){

					// 	$full_name = $payer[0]->payer_name;

					// } else {

					// 	$full_name = '--';	

					// }



					// deposit date

					$deposit_date = $this->Xin_model->set_date_format($r->transaction_date);


					// get amount
					// $category = array_unique($category);

					// $category = implode(",", $category);

					$amount = $this->Xin_model->currency_sign($r->amount);



					$data[] = array(

						$deposit_date,

						$category,

						$organization,

						$full_name,

						$amount		

					);

					//x

				}

			$output = array(

				"draw" => $draw,

				"recordsTotal" => count($incomes),

				"recordsFiltered" => count($incomes),

				"data" => $data

			);

			echo json_encode($output);

			exit();
		}
		else if($deposit_type == 'all_types')
		{
			$incomes = $this->Xin_model->get_all_income_transaction_filtered($from_date,$to_date);
			if ($deposit_type != 'all_types') {
				$ids = $this->Xin_model->get_sub_deposit_type_id($deposit_type)->result();
			}

			else{
				$ids = $this->Xin_model->get_sub_deposit_type()->result();
			}

			$all_ids = array();
				// print_r($ids);die;
			if (!empty($ids)) {
				foreach($ids as $id)
				{
					array_push($all_ids, $id->id);
				}
			}

			$income_ids = array();

			foreach($incomes as $income)
			{
				if ($income->invoice_id == 0) {
					$sub_deposit_ids = explode(",", $income->sub_deposit_ids);
					
					$check = array_intersect($sub_deposit_ids, $all_ids);

					if (!empty($check)) {
						array_push($income_ids, $income->transaction_id);
					}
				}else{
					array_push($income_ids, $income->transaction_id);
				}

				
					// print_r($check);

					// print_r($sub_deposit_ids);continue;
			}

			// print_r($income_ids);

			// print_r($category);
			foreach ($income_ids as $i) 
			{
				$r = $this->Xin_model->get_transaction($i)->result();
				$r = $r[0];

				$category = array();
				if ($r->invoice_id == 0) {
					$sub_deposit_ids = explode(",", $r->sub_deposit_ids);

					if (!empty($sub_deposit_ids)) 
					{
							// echo "got sub_deposit2";
						foreach($sub_deposit_ids as $sd)
						{
							$sub_deposit = $this->Xin_model->get_deposit_type_by_sub($sd)->result();
							// echo gettype($category);
							if (!empty($sub_deposit)) {
									// print_r($sub_deposit);continue;
									// print_r($sub_deposit);
								array_push($category, $sub_deposit[0]->name);
							}

						}
					}
					$organization = '';
				}else{
					array_push($category, 'invoice');

					// $organization = $this->Clients_model->get_organization_info($income->invoice_id)->result();	

					$invoice = $this->Finance_model->read_invoice($r->invoice_id)->result();

					
				}

				if (!empty($invoice)) {
					$organization = $this->Clients_model->get_organization_info($invoice[0]->company_name)->result();
					$organization_name = $organization[0]->name;
				}else{
					$organization_name = "-";
				}

				
				$category = array_unique($category);

				$category = implode(",", $category);

				// account type

				$acc_type = $this->Finance_model->read_bankcash_information($r->account_id);

				if(!is_null($acc_type)){

					$account = $acc_type[0]->account_name;

				} else {

					$account = '--';	

				}

				// get payee

				$payee = $this->Xin_model->read_user_info($r->payer_payee_id);

				// user full name

				if(!is_null($payee)){

					$full_name = $payee[0]->first_name.' '.$payee[0]->last_name;

				} else {

					$full_name = '--';	

				}

				// // get payee

				// $payer = $this->Finance_model->read_payer_info($r->payer_payee_id);

				// // user full name

				// if(!is_null($payer)){

				// 	$full_name = $payer[0]->payer_name;

				// } else {

				// 	$full_name = '--';	

				// }



				// deposit date

				$deposit_date = $this->Xin_model->set_date_format($r->transaction_date);


				// get amount
				

				$amount = $this->Xin_model->currency_sign($r->amount);



				$data[] = array(

					$deposit_date,

					$category,

					$organization_name,

					$full_name,

					$amount		

				);

				//x

			}



			$output = array(

				"draw" => $draw,

				"recordsTotal" => count($income_ids),

				"recordsFiltered" => count($income_ids),

				"data" => $data

			);

			echo json_encode($output);

			exit();

		}
		else
		{
			$incomes = $this->Xin_model->get_all_income_transaction_filtered($from_date,$to_date);
			if ($deposit_type != 'all_types') {
				$ids = $this->Xin_model->get_sub_deposit_type_id($deposit_type)->result();
			}

			else{
				$ids = $this->Xin_model->get_sub_deposit_type()->result();
			}

			$all_ids = array();
				// print_r($ids);die;
			if (!empty($ids)) {
				foreach($ids as $id)
				{
					array_push($all_ids, $id->id);
				}
			}

			$income_ids = array();

			foreach($incomes as $income)
			{
				$sub_deposit_ids = explode(",", $income->sub_deposit_ids);
				
				$check = array_intersect($sub_deposit_ids, $all_ids);

				if (!empty($check)) {
					array_push($income_ids, $income->transaction_id);
				}
				
			}

			foreach ($income_ids as $i) 
			{
				$r = $this->Xin_model->get_transaction($i)->result();
				$r = $r[0];

				$category = array();
				$sub_deposit_ids = explode(",", $r->sub_deposit_ids);

				if (!empty($sub_deposit_ids)) 
				{
						// echo "got sub_deposit2";
					foreach($sub_deposit_ids as $sd)
					{
						$sub_deposit = $this->Xin_model->get_deposit_type_by_sub($sd)->result();
						// echo gettype($category);
						if (!empty($sub_deposit)) {
								// print_r($sub_deposit);continue;
								// print_r($sub_deposit);
							array_push($category, $sub_deposit[0]->name);
						}

					}
				}

				
				$category = array_unique($category);

				$category = implode(",", $category);

				// account type

				$acc_type = $this->Finance_model->read_bankcash_information($r->account_id);

				if(!is_null($acc_type)){

					$account = $acc_type[0]->account_name;

				} else {

					$account = '--';	

				}

				// get payee

				$payee = $this->Xin_model->read_user_info($r->payer_payee_id);

				// user full name

				if(!is_null($payee)){

					$full_name = $payee[0]->first_name.' '.$payee[0]->last_name;

				} else {

					$full_name = '--';	

				}

				// // get payee

				// $payer = $this->Finance_model->read_payer_info($r->payer_payee_id);

				// // user full name

				// if(!is_null($payer)){

				// 	$full_name = $payer[0]->payer_name;

				// } else {

				// 	$full_name = '--';	

				// }



				// deposit date

				$deposit_date = $this->Xin_model->set_date_format($r->transaction_date);


				// get amount
				

				$amount = $this->Xin_model->currency_sign($r->amount);



				$data[] = array(

					$deposit_date,

					// $account,

					$category,

					"-",

					$full_name,

					$amount		

				);

				//x

			}



			$output = array(

				"draw" => $draw,

				"recordsTotal" => count($income_ids),

				"recordsFiltered" => count($income_ids),

				"data" => $data

			);

			echo json_encode($output);

			exit();

		} 
	}

	 // income report list

	public function report_staff_income_list() 
	{

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/report_staff_income", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		$deposit_type = $this->input->get('type_id');

		$from_date = $this->input->get('from_date');

		$to_date = $this->input->get('to_date');

		$data = array();
		$subs = array();
		
		if (is_null($deposit_type)) {
			
			$output = array(

				"draw" => $draw,

				"recordsTotal" => 0,

				"recordsFiltered" => 0,

				"data" => $data

			);

			echo json_encode($output);

			exit();
		}
		else
		{
			$incomes = $this->Xin_model->get_staff_income_transaction_filtered($from_date,$to_date,$deposit_type);
			// print_r($incomes);die;
			foreach ($incomes as $r) 
			{

				$category = array();

				if ($r->sub_deposit_ids != '') {
				
					$sub_deposit_ids = explode(",", $r->sub_deposit_ids);

					if (!empty($sub_deposit_ids)) 
					{
							// echo "got sub_deposit2";
						foreach($sub_deposit_ids as $sd)
						{
							$sub_deposit = $this->Xin_model->get_deposit_type_by_sub($sd)->result();
							// echo gettype($category);
							if (!empty($sub_deposit)) {
									// print_r($sub_deposit);continue;
									// print_r($sub_deposit);
								array_push($category, $sub_deposit[0]->name);
							}

						}
					}
				}else{
					array_push($category,'invoice');
				}

				
				$category = array_unique($category);

				$category = implode(",", $category);

				// account type

				$acc_type = $this->Finance_model->read_bankcash_information($r->account_id);

				if(!is_null($acc_type)){

					$account = $acc_type[0]->account_name;

				} else {

					$account = '--';	

				}

				// get payee

				$payee = $this->Xin_model->read_user_info($r->payer_payee_id);

				// user full name

				if(!is_null($payee)){

					$full_name = $payee[0]->first_name.' '.$payee[0]->last_name;

				} else {

					$full_name = '--';	

				}

				// // get payee

				// $payer = $this->Finance_model->read_payer_info($r->payer_payee_id);

				// // user full name

				// if(!is_null($payer)){

				// 	$full_name = $payer[0]->payer_name;

				// } else {

				// 	$full_name = '--';	

				// }



				// deposit date

				$deposit_date = $this->Xin_model->set_date_format($r->transaction_date);


				// get amount
				

				$amount = $this->Xin_model->currency_sign($r->amount);



				$data[] = array(

					$deposit_date,

					$account,

					$category,

					$full_name,

					$amount		

				);

				//x

			}



			$output = array(

				"draw" => $draw,

				"recordsTotal" => count($incomes),

				"recordsFiltered" => count($incomes),

				"data" => $data

			);

			echo json_encode($output);

			exit();

		} 
	}

    // income report list

	public function report_trial_balance_list() 
	{



		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/report_trial_balance", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		$deposit = $this->Xin_model->get_deposit_type2()->result();
		$expense = $this->Xin_model->get_expense_type2()->result()	;

		// print_r($expense);die;
		$data = array();


		foreach($deposit as $r) {
			
			$sub_deposit = $this->Xin_model->get_sub_deposit_type_id($r->id)->result();

			$sub_deposit_ids = array();

			foreach($sub_deposit as $sub){
				array_push($sub_deposit_ids, $sub->id);
			}

			$all_deposit_transactions = $this->Xin_model->get_all_income_transaction();

			$amount = array();
			
			foreach($all_deposit_transactions as $s){
				$all_ids = explode(",", $s->sub_deposit_ids);

				$check = array_intersect($all_ids, $sub_deposit_ids);

				if (!empty($check)) {

					array_push($amount, $s->amount);					

				}
			}

			$account_code = $r->account_code;
			$account_name = $r->name;
			$debit_amount = '-';
			$credit_amount = $this->Xin_model->currency_sign(array_sum($amount));

			$data[] = array(

				$account_code,

				$account_name,

				$debit_amount,

				$credit_amount,

			);

			unset($amount);

		}

		foreach($expense as $r) {
			$sub_expense = $this->Xin_model->get_sub_expense_type_id($r->expense_type_id)->result();

			$sub_expense_ids = array();

			foreach($sub_expense as $sub){
				array_push($sub_expense_ids, $sub->id);
			}

			$all_expense_transactions = $this->Xin_model->get_all_expense_transaction();

			$amount = array();
			
			foreach($all_expense_transactions as $s){
				$all_ids = explode(",", $s->subexpenses);

				$check = array_intersect($all_ids, $sub_expense_ids);

				// echo "$s->transaction_id\n";
				// print_r($check);continue;


				if (!empty($check)) {

					array_push($amount, $s->amount);					

				}
			}
			
			$account_code = $r->account_code;
			$account_name = $r->name;
			$debit_amount = $this->Xin_model->currency_sign(array_sum($amount));
			$credit_amount = '-';

			$data[] = array(

				$account_code,

				$account_name,

				$debit_amount,

				$credit_amount,

			);

			unset($amount);
			unset($sub_expense_ids);

		}

		$all_invoices = $this->Invoices_model->get_invoices_paid();

		$total = array();

		if (!empty($all_invoices)) {
			$i = 1;
			foreach($all_invoices->result() as $invoice){
				array_push($total,$invoice->grand_total);

				$data[] = array(

					"9000-".$i,

					$invoice->invoice_number,

					$this->Xin_model->currency_sign($invoice->grand_total),

					"-",

				);
				$i++;
			}
		}

		// $data[] = array(

		// 		"9000-1",

		// 		"Premium generated",

		// 		$this->Xin_model->currency_sign(array_sum($total)),

		// 		"-",

		// 	);

		$output = array(

			"draw" => $draw,

			"recordsTotal" => count($expense)+count($deposit),

			"recordsFiltered" => count($expense)+count($deposit),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	} 

	// income report list

	public function report_general_ledger_list() 
	{

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/report_general_ledger", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		$deposit = $this->Xin_model->get_deposit_type2()->result();
		$expense = $this->Xin_model->get_expense_type2()->result()	;

		// print_r($expense);die;
		$data = array();


		foreach($deposit as $r) {
			
			$sub_balance = array();

			$sub_deposit = $this->Xin_model->get_sub_deposit_type_id($r->id)->result();

			$sub_deposit_ids = array();

			foreach($sub_deposit as $sub){
				array_push($sub_deposit_ids, $sub->id);
				$sub_balance[] = $sub->balance;
			}

			$all_deposit_transactions = $this->Xin_model->get_all_income_transaction();

			$amount = array();
			
			foreach($all_deposit_transactions as $s){
				$all_ids = explode(",", $s->sub_deposit_ids);

				$check = array_intersect($all_ids, $sub_deposit_ids);

				if (!empty($check)) {

					array_push($amount, $s->amount);					

				}
			}

			$account_code = $r->account_code;
			$account_name = $r->name;
			$debit_amount = '-';
			$credit_amount = $this->Xin_model->currency_sign(array_sum($amount));
			$balance = $this->Xin_model->currency_sign(array_sum($sub_balance));

			$data[] = array(

				$account_code,

				$account_name,

				$debit_amount,

				$credit_amount,

				$balance

			);

			unset($amount);
			unset($sub_balance);

		}

		foreach($expense as $r) {
			
			$sub_balance = array();

			$sub_expense = $this->Xin_model->get_sub_expense_type_id($r->expense_type_id)->result();

			$sub_expense_ids = array();

			foreach($sub_expense as $sub){
				array_push($sub_expense_ids, $sub->id);
				$sub_balance[] = $sub->balance;
			}

			$all_expense_transactions = $this->Xin_model->get_all_expense_transaction();

			$amount = array();
			
			foreach($all_expense_transactions as $s){
				$all_ids = explode(",", $s->subexpenses);

				$check = array_intersect($all_ids, $sub_expense_ids);

				// echo "$s->transaction_id\n";
				// print_r($check);continue;


				if (!empty($check)) {

					array_push($amount, $s->amount);					

				}
			}
			
			$account_code = $r->account_code;
			$account_name = $r->name;
			$debit_amount = $this->Xin_model->currency_sign(array_sum($amount));
			$credit_amount = '-';
			$balance = $this->Xin_model->currency_sign(array_sum($sub_balance));

			$data[] = array(

				$account_code,

				$account_name,

				$debit_amount,

				$credit_amount,

				$balance

			);

			unset($amount);
			unset($sub_balance);
			unset($sub_expense_ids);


		}

		$output = array(

			"draw" => $draw,

			"recordsTotal" => count($expense)+count($deposit),

			"recordsFiltered" => count($expense)+count($deposit),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	} 




	// report transfer list

	public function report_transfer_list() {



		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/report_transfer", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));		

		$type = $this->input->get('type');

		if ($type == 'bank') {
			//bank account
			$transfer = $this->Finance_model->get_transfer_search($this->input->get('from_date'),$this->input->get('to_date'))->result();
		}else{
			//ledger
			$transfer = $this->Finance_model->get_transfer_search($this->input->get('from_date'),$this->input->get('to_date'),$type,'i')->result();
			$transfer2 = $this->Finance_model->get_transfer_search($this->input->get('from_date'),$this->input->get('to_date'),$type,'e')->result();
		}		

		$data = array();

		foreach($transfer as $r) {

			// get amount

			$amount = $this->Xin_model->currency_sign($r->amount);

			if($r->dr_cr == 'cr') {

				$xin_acc = $this->lang->line('xin_acc_credit');

			} else {

				$xin_acc = $this->lang->line('xin_acc_debit');

			}			

			// transfer date

			$transfer_date = $this->Xin_model->set_date_format($r->transaction_date);

			// payment method 

			$payment_method = $this->Xin_model->read_payment_method($r->payment_method_id);

			if(!is_null($payment_method)){

				$method_name = $payment_method[0]->method_name;

			} else {

				$method_name = '--';	

			}

			$r->dr_cr=="dr" ? $this->lang->line('xin_acc_debit') : $this->lang->line('xin_acc_credit');

			$r->debit!=NULL ? $db_am = "- ".$this->Xin_model->currency_sign($r->debit) : $db_am ="";

			$r->credit!=NULL ? $cr_am = "+ ".$this->Xin_model->currency_sign($r->credit) : $cr_am ="";

			// account type

			$type = explode("-", $r->transaction_type);

			if (count($type) < 2 ) {
				//bank

				$acc_type = $this->Finance_model->read_bankcash_information($r->account_id);

				if(!is_null($acc_type)){

					$account = $acc_type[0]->account_name;

				} else {

					$account = '--';	

				}	

			}else{
				//ledger
				if ($type[2] == 'i') {
					//income

					$acc_type = $this->Finance_model->read_deposit_type($r->account_id);
				}else{
					//expense
					$acc_type = $this->Finance_model->read_expense_type($r->account_id);

				}

				if(!is_null($acc_type)){

					$account = $acc_type[0]->name;

				} else {

					$account = '--';	

				}						

			}

			

			$data[] = array(

				$transfer_date,

				$r->description,

				$account,

				$xin_acc,

				$db_am,

				$cr_am

			);

		}

		if (isset($transfer2)) {
			foreach($transfer2 as $r) {

				// get amount

				$amount = $this->Xin_model->currency_sign($r->amount);

				if($r->dr_cr == 'cr') {

					$xin_acc = $this->lang->line('xin_acc_credit');

				} else {

					$xin_acc = $this->lang->line('xin_acc_debit');

				}			

				// transfer date

				$transfer_date = $this->Xin_model->set_date_format($r->transaction_date);

				// payment method 

				$payment_method = $this->Xin_model->read_payment_method($r->payment_method_id);

				if(!is_null($payment_method)){

					$method_name = $payment_method[0]->method_name;

				} else {

					$method_name = '--';	

				}

				$r->dr_cr=="dr" ? $this->lang->line('xin_acc_debit') : $this->lang->line('xin_acc_credit');

				$r->debit!=NULL ? $db_am = "- ".$this->Xin_model->currency_sign($r->debit) : $db_am ="";

				$r->credit!=NULL ? $cr_am = "+ ".$this->Xin_model->currency_sign($r->credit) : $cr_am ="";

				// account type

				$type = explode("-", $r->transaction_type);

				if (count($type) < 2 ) {
					//bank

					$acc_type = $this->Finance_model->read_bankcash_information($r->account_id);

					if(!is_null($acc_type)){

						$account = $acc_type[0]->account_name;

					} else {

						$account = '--';	

					}	

				}else{
					//ledger
					if ($type[2] == 'i') {
						//income

						$acc_type = $this->Finance_model->read_deposit_type($r->account_id);
					}else{
						//expense
						$acc_type = $this->Finance_model->read_expense_type($r->account_id);

					}

					if(!is_null($acc_type)){

						$account = $acc_type[0]->name;

					} else {

						$account = '--';	

					}						

				}

				$data[] = array(

					$transfer_date,

					$r->description,

					$account,

					$xin_acc,

					$db_am,

					$cr_am

				);

			}

			$count_transfer2 = count($transfer2);
		}else{
			$count_transfer2 = 0;
		}

		$output = array(

			"draw" => $draw,

			"recordsTotal" => count($transfer)+$count_transfer2,

			"recordsFiltered" => count($transfer)+$count_transfer2,

			"data" => $data

		);

		echo json_encode($output);

		exit();

	} 



	 // report income vs expense > search

	public function report_income_expense_list() {



		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));



		$account_statement = $this->Finance_model->get_income_expense_search($this->input->get('from_date'),$this->input->get('to_date'));

		

		$data = array();

		$debit="";

		$debit_total=0;

		$credit="";

		$credit_total=0;

		foreach($account_statement->result() as $r) {



			if($r->transaction_credit!=0.00 && $r->transaction_credit > 0){

				$credit_total=$credit_total+$r->transaction_credit;

			}

			else if($r->transaction_debit!=0.00 && $r->transaction_debit > 0){

				$debit_total = $debit_total+$r->transaction_debit;

			}

		}



		$totalD = "<b class='pull-right'>".$this->lang->line('xin_acc_total_credit').": ".$this->Xin_model->currency_sign($debit_total)."</b>";

		$totalC = "<b class='pull-right'>".$this->lang->line('xin_acc_total_debit').": ".$this->Xin_model->currency_sign($credit_total)."</b>";

		$data[] = array(

			$totalC.' '.$totalC,

			$totalD.' '.$totalD

		);



		$output = array(

			"draw" => $draw,

			"recordsTotal" => $account_statement->num_rows(),

			"recordsFiltered" => $account_statement->num_rows(),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	}



	 // ***** LISTING 

	 // sales_report_list

	public function accounts_ledger_list() {



		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/full_ledger_account_list", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		

		

		$acc_ledger = $this->Finance_model->get_ledger_accounts($this->input->get('from_date'),$this->input->get('to_date'));

		

		$data = array();

		$crd_total = 0; $deb_total = 0;$balance=0; $balance2 = 0;

		foreach($acc_ledger->result() as $r) {



			// transaction date

			$transaction_date = $this->Xin_model->set_date_format($r->transaction_date);

			// get currency

			$total_amount = $this->Xin_model->currency_sign($r->amount);

			$acc_type = $this->Finance_model->read_bankcash_information($r->account_id);

			if(!is_null($acc_type)){

				$account_balance = $acc_type[0]->account_opening_balance;

			} else {

				$account_balance = 0;	

			}

			

			if($r->dr_cr == 'cr') {

				$balance2 = $balance2 - $r->amount;

			} else {

				$balance2 = $balance2 + $r->amount;

			}

			if($r->credit!=0):

				$crd = $r->credit;

				$crd_total += $crd;

			else:

				$crd = 0;	

				$crd_total += $crd;

			endif;

			if($r->debit!=0):

				$deb = $r->debit;

				$deb_total += $deb;

			else:

				$deb = 0;	

				$deb_total += $deb;

			endif;

			$fbalance = $account_balance + $balance2;

			

			$data[] = array(

				$transaction_date,

				$r->transaction_type,

				$r->description,

				$this->Xin_model->currency_sign($crd),

				$this->Xin_model->currency_sign($deb),

				$this->Xin_model->currency_sign($fbalance)

			);

		}



		$output = array(

			"draw" => $draw,

			"recordsTotal" => $acc_ledger->num_rows(),

			"recordsFiltered" => $acc_ledger->num_rows(),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	} 



	 // get sales footer - data

	public function get_accounts_ledger_footer() {



		$data['title'] = $this->Xin_model->site_title();

		

		$data = array(

			'from_date' => $this->input->get('from_date'),

			'to_date' => $this->input->get('to_date')

		);

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view("admin/accounting/footer/get_ledger_accounts_footer", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

	}

	 // Validate and add info in database

	public function add_payer() {



		if($this->input->post('add_type')=='add_payer') {		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			

			/* Server side PHP input validation */		

			if($this->input->post('payer_name')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_payer_name');

			}



			if($Return['error']!=''){

				$this->output($Return);

			}



			$data = array(

				'payer_name' => $this->input->post('payer_name'),

				'contact_number' => $this->input->post('contact_number'),

				'created_at' => date('d-m-Y h:i:s')

			);



			$result = $this->Finance_model->add_payer_record($data);

			if ($result == TRUE) {

				$Return['result'] = $this->lang->line('xin_acc_success_payer_added');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}

	

	// Validate and add info in database

	public function add_payee() {



		if($this->input->post('add_type')=='add_payee') {		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			

			/* Server side PHP input validation */		

			if($this->input->post('payee_name')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_payee_name');

			}



			if($Return['error']!=''){

				$this->output($Return);

			}



			$data = array(

				'payee_name' => $this->input->post('payee_name'),

				'contact_number' => $this->input->post('contact_number'),

				'created_at' => date('d-m-Y h:i:s')

			);



			$result = $this->Finance_model->add_payee_record($data);

			if ($result == TRUE) {

				$Return['result'] = $this->lang->line('xin_acc_success_payee_added');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}

	

	// Validate and add info in database

	public function update_payer() {



		if($this->input->post('edit_type')=='payer') {		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$id = $this->uri->segment(4);

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			/* Server side PHP input validation */		

			if($this->input->post('payer_name')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_payer_name');

			}



			if($Return['error']!=''){

				$this->output($Return);

			}



			$data = array(

				'payer_name' => $this->input->post('payer_name'),

				'contact_number' => $this->input->post('contact_number'),

			);



			$result = $this->Finance_model->update_payer_record($data,$id);

			if ($id) {

				$Return['result'] = $this->lang->line('xin_acc_success_payer_updated');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}

	

	// Validate and add info in database

	public function update_payee() {



		if($this->input->post('edit_type')=='payee') {		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$id = $this->uri->segment(4);

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			/* Server side PHP input validation */		

			if($this->input->post('payee_name')==='') {

				$Return['error'] = $this->lang->line('xin_acc_error_payee_name');

			}



			if($Return['error']!=''){

				$this->output($Return);

			}



			$data = array(

				'payee_name' => $this->input->post('payee_name'),

				'contact_number' => $this->input->post('contact_number'),

			);



			$result = $this->Finance_model->update_payee_record($data,$id);

			if ($id) {

				$Return['result'] = $this->lang->line('xin_acc_success_payee_updated');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}

	// delete record

	public function delete_payer() {

		

		if($this->input->post('is_ajax')=='2') {

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$id = $this->uri->segment(4);

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			$result = $this->Finance_model->delete_payer_record($id);

			if(isset($id)) {

				$Return['result'] = $this->lang->line('xin_acc_success_payer_deleted');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

		}

	}

	

	// delete record

	public function delete_payee() {

		

		if($this->input->post('is_ajax')=='2') {

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$id = $this->uri->segment(4);

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			$result = $this->Finance_model->delete_payee_record($id);

			if(isset($id)) {

				$Return['result'] = $this->lang->line('xin_acc_success_payee_deleted');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

		}

	}

	// bank wise transactions list

	public function bankwise_transactions_list() {



		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$data['title'] = $this->Xin_model->site_title();

		if(!empty($session)){ 

			$this->load->view("admin/accounting/full_ledger_account_list", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

		

		$ac_id = $this->uri->segment(4);

		$transactions = $this->Finance_model->get_bankwise_transactions($ac_id);

		

		$data = array();

		$balance2 = 0;

		foreach($transactions->result() as $r) {



			// transaction date

			$transaction_date = $this->Xin_model->set_date_format($r->transaction_date);

			// get currency

			$total_amount = $this->Xin_model->currency_sign($r->amount);

			// credit

			//$transaction_credit = $this->Xin_model->currency_sign($r->transaction_credit);

			// debit

			//$transaction_debit = $this->Xin_model->currency_sign($r->transaction_debit);			

			// total credit

			// balance

			/*if($r->transaction_debit == 0) {

				$balance2 = $balance2 - $r->transaction_credit;

			} else {

				$balance2 = $balance2 + $r->transaction_debit;

			}

			$balance = $this->Xin_model->currency_sign($balance2);*/

			

			$data[] = array(

				$transaction_date,

				$r->transaction_type,

				$r->description,

				$total_amount,

				$r->description,

				$r->description

			);

		}



		$output = array(

			"draw" => $draw,

			"recordsTotal" => $transactions->num_rows(),

			"recordsFiltered" => $transactions->num_rows(),

			"data" => $data

		);

		echo json_encode($output);

		exit();

	} 	

	 // get income footer - data

	public function get_income_footer() {



		$data['title'] = $this->Xin_model->site_title();

		$deposit_type = $this->input->get('type_id');

		$from_date = $this->input->get('from_date');

		$to_date = $this->input->get('to_date');

		// $data['deposit'] = $this->Finance_model->get_deposit_search($data['from_date'],$data['to_date'],$data['type_id']);

		$data = array();
		$subs = array();
		
		if (is_null($deposit_type)) {
			$data['deposit'] = '';

		}
		else if($deposit_type == 'invoice')
		{
			$data['deposit'] = $this->Xin_model->get_all_invoice_income_transaction_filtered($from_date,$to_date);
		}
		else if($deposit_type == 'all_types')
		{
			$data['deposit'] = $this->Xin_model->get_all_income_transaction_filtered($from_date,$to_date);
		}
		else
		{
			$incomes = $this->Xin_model->get_all_income_transaction_filtered($from_date,$to_date);
			if ($deposit_type != 'all_types') 
			{
				$ids = $this->Xin_model->get_sub_deposit_type_id($deposit_type)->result();
			}else
			{
				$ids = $this->Xin_model->get_sub_deposit_type()->result();
			}

			$all_ids = array();
				// print_r($ids);die;
			if (!empty($ids)) {
				foreach($ids as $id)
				{
					array_push($all_ids, $id->id);
				}
			}

			$income_ids = array();

			foreach($incomes as $income)
			{
				$sub_deposit_ids = explode(",", $income->sub_deposit_ids);
				
				$check = array_intersect($sub_deposit_ids, $all_ids);

				if (!empty($check)) {
					array_push($income_ids, $income->transaction_id);
				}
				
			}
			$deposits = array();
			foreach ($income_ids as $i) 
			{
				$r = $this->Xin_model->get_transaction($i)->result();
				$r = $r[0];

				array_push($deposits, $r);

				//x

			}

			$data['deposit'] = $deposits;

		} 


		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view("admin/accounting/footer/get_income_footer", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

	}

	public function get_staff_income_footer() {



		$data['title'] = $this->Xin_model->site_title();

		$deposit_type = $this->input->get('type_id');

		$from_date = $this->input->get('from_date');

		$to_date = $this->input->get('to_date');

		// $data['deposit'] = $this->Finance_model->get_deposit_search($data['from_date'],$data['to_date'],$data['type_id']);

		$data = array();
		$subs = array();
		
		if (is_null($deposit_type)) {
			$data['deposit'] = '';

		}
		else
		{
			$data['deposit'] = $this->Xin_model->get_staff_income_transaction_filtered($from_date,$to_date,$deposit_type);

		}

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view("admin/accounting/footer/get_income_footer", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

	}

	 // get transfer footer - data

	public function get_transfer_footer() {



		$data['title'] = $this->Xin_model->site_title();

		

		$data = array(

			'from_date' => $this->input->get('from_date'),

			'to_date' => $this->input->get('to_date'),

			'type' => $this->input->get('type')

		);

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view("admin/accounting/footer/get_transfer_footer", $data);

		} else {

			redirect('admin/');

		}

		// Datatables Variables

		$draw = intval($this->input->get("draw"));

		$start = intval($this->input->get("start"));

		$length = intval($this->input->get("length"));

	}

	public function pdf(){
		$did = explode("-", $this->input->post("did"));
		$hospital_name = $this->input->post("hospital");
		$from = $this->input->post("from");
		$to = $this->input->post("to");

		$from = new DateTime($from);
		$to = new DateTime($to);
		$day = $this->ordinal($from->format('j'));
		$day2 = $this->ordinal($to->format('j'));

		$this->load->library('Pdf');

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('Bill Payment Requests');
		$pdf->SetTopMargin(20);
		$pdf->SetLeftMargin(20);
		$pdf->SetRightMargin(20);
		$pdf->setFooterMargin(20);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);	
		$pdf->AddPage();

		$pdf->Image('/uploads/logo/signin/signin_logo_1573568882.png', 170, 10, 30, 30, '','', '', true, 150, '', false,false, 1, true);
		$pdf->SetFont('Times','',12);

		$pdf->Write(5, "", '', 0, 'L', true, 0, false, false, 0);
		
		$html = "
		<h3>Showing Data of ".$hospital_name." from ".$day." ".$from->format("F Y")." - ".$day2." ".$to->format("F Y")."</h3>
		";

		$html .= "
		<table border=\"1\" align=\"center\">
		<thead>
		<tr>
		<th width=\"5%\">No</th>
		<th>Name</th>
		<th width=\"15%\">Diagnose</th>
		<th>Date Encounter</th>
		<th>Bill</th>
		</tr>
		</thead>
		<tbody>";

		$no = 1;
		$total = array();
		foreach ($did as $d) {
			$fetched_result = $this->Clients_model->view_individual_hospital_diagnose_clients($d);
			$html .= 
			"<tr>
			<td width=\"5%\">".$no."</td>
			<td align=\"center\">";
			if($fetched_result["diagnose_user_type"] == 'C') 
			{ 
				$html .= $fetched_result["cname"]." ".$fetched_result["clname"]." ".$fetched_result["coname"]; 
			} else 
			{ 
				$html .= $fetched_result["dname"]." ".$fetched_result["dlname"]." ".$fetched_result["doname"]; 
			}
			$html .=       "</td>

			<td width=\"15%\">".$fetched_result['diagnose_diagnose']."</td>
			<td>".$fetched_result['diagnose_date']."</td>
			<td>".number_format($fetched_result["diagnose_total_sum"])."</td>
			</tr>";
			$no++;
			array_push($total, $fetched_result["diagnose_total_sum"]);
		}

		$html .= "

		<tr>
		<td colspan=\"5\" align=\"center\">Total Bills: <b>".number_format(array_sum($total))."</b></td>
		</tr>	
		</tbody>
		</table>
		";

		$pdf->writeHTML($html, true, 0, true, 0);

		$pdf->Output('bill_payment_requests.pdf', 'I');
	}

	public function pdf2(){
		// $did = explode("-", $this->input->post("did"));
		$from = $this->input->get("from");
		$to = $this->input->get("to");
		$diagnose = $this->Clients_model->view_all_individual_hospital_diagnose_clients($from,$to);
		// echo $this->db->last_query();
		// print_r($diagnose);
		// echo "asd";
		// echo $diagnose!==true;die;

		$from = new DateTime($from);
		$to = new DateTime($to);
		$day = $this->ordinal($from->format('j'));
		$day2 = $this->ordinal($to->format('j'));

		$this->load->library('Pdf');

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('Payment Schedule');
		$pdf->SetTopMargin(20);
		$pdf->SetLeftMargin(20);
		$pdf->SetRightMargin(20);
		$pdf->setFooterMargin(20);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);	
		$pdf->AddPage();

		$pdf->Image('/uploads/logo/signin/signin_logo_1573568882.png', 170, 10, 30, 30, '','', '', true, 150, '', false,false, 1, true);
		$pdf->SetFont('Times','',12);

		$pdf->Write(5, "", '', 0, 'L', true, 0, false, false, 0);
		
		$html = "
		<h3>Showing Data from ".$day." ".$from->format("F Y")." - ".$day2." ".$to->format("F Y")."</h3>
		";

		$html .= "
		<table border=\"1\" align=\"center\">
		<thead>
			<tr>
				<th>Name</th>
				<th>Diagnose</th>
				<th>Date Encounter</th>
				<th>Hospital Name</th>
                <th>Bank Account</th>
                <th>Bank Name</th>
				<th>Bill</th>
			</tr>
		</thead>
		<tbody>";

		$no = 1;
		$total = array();
		if($diagnose !== false)
		{
			// echo "sds";
			// exit();
			foreach ($diagnose as $fetched_result) {
				$hospital = $this->Clients_model->get_hospital_info($fetched_result->diagnose_hospital_id)->result();

	            if (!empty($hospital)) {
	              // echo "got one";

	              $bank_name = is_null($hospital[0]->bank_name) ? '--' : strtoupper($hospital[0]->bank_name);
	              $bank_account = is_null($hospital[0]->bank_account) ? '--' : strtoupper($hospital[0]->bank_account);
	              $hospital_name = ucwords($hospital[0]->hospital_name);
	            }else{
	              // echo "not one";
	              $bank_name = '--';
	              $bank_account = '--';
	            }

				$html .= 
	                "<tr>
	                    <td>";
	                if($fetched_result->diagnose_user_type == 'C') 
	                { 
	                  $html .= $fetched_result->cname." ".$fetched_result->clname." ".$fetched_result->coname; 
	                } else 
	                { 
	                  $html .= $fetched_result->dname." ".$fetched_result->dlname." ".$fetched_result->doname; 
	                }
	            $html .=  
	                "</td>
	                    <td>".$fetched_result->diagnose_diagnose."</td>
	                    <td>".$fetched_result->diagnose_date."</td>
	                    <td>".$hospital_name."</td>
	                    <td>".$bank_account."</td>
	                    <td>".$bank_name."</td>
	                    <td>".number_format($fetched_result->diagnose_total_sum)."</td>
	                </tr>";
				$no++;
				array_push($total, $fetched_result->diagnose_total_sum);
			}
		}


		$html .= "

		<tr>
		<td colspan=\"7\" align=\"center\">Total Bills: <b>".number_format(array_sum($total))."</b></td>
		</tr>	
		</tbody>
		</table>
		";

		$pdf->writeHTML($html, true, 0, true, 0);

		$pdf->Output('payment_schedule'.$from->format('Y-m-d').'-'.$to->format('Y-m-d').'.pdf', 'D');
	}

	public function pdf3()
	{

		// $did = explode("-", $this->input->post("did"));
		$from = $this->input->get("from");
		$to = $this->input->get("to");
		$diagnose = $this->Clients_model->view_all_individual_hospital_diagnose_clients($from,$to);
		// echo $this->db->last_query();
		// print_r($diagnose);
		// echo "asd";
		// echo $diagnose!==true;die;

		$from = new DateTime($from);
		$to = new DateTime($to);
		$day = $this->ordinal($from->format('j'));
		$day2 = $this->ordinal($to->format('j'));

		$this->load->library('Pdf');

		$pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('General Ledger Trial Balance');
		$pdf->SetTopMargin(20);
		$pdf->SetLeftMargin(20);
		$pdf->SetRightMargin(20);
		$pdf->setFooterMargin(20);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(true);	
		$pdf->AddPage();

		$pdf->Image('/uploads/logo/signin/signin_logo_1573568882.png', 170, 10, 30, 30, '','', '', true, 150, '', false,false, 1, true);
		$pdf->SetFont('Times','B',12);

		$pdf->Write(5, "", '', 0, 'C', true, 0, false, false, 0);
		$pdf->Write(5, "LIONTECH LIMITED", '', 0, 'C', true, 0, false, false, 0);
		$pdf->Write(5, "General Ledger Trial Balance", '', 0, 'C', true, 0, false, false, 0);
		$pdf->Write(5, "As of ".date("M d, Y"), '', 0, 'C', true, 0, false, false, 0);
		$pdf->Write(5, "", '', 0, 'C', true, 0, false, false, 0);
		
		$pdf->SetFont('Times','',12);
		/*
		$html = "
		<h3>Showing Data from ".$day." ".$from->format("F Y")." - ".$day2." ".$to->format("F Y")."</h3>
		";*/

		$html = "
		<table align=\"center\">
		<thead>
			<tr style=\"border-top: 1px solid black; border-bottom: 1px solid black;\">
				<th align=\"left\"><b>Account Code</b></th>
				<th align=\"left\"><b>Account Name</b></th>
				<th align=\"right\"><b>Debit Amt</b></th>
				<th align=\"right\"><b>Credit Amt</b></th>
			</tr>
		</thead>
		<tbody>";

		$deposit = $this->Xin_model->get_deposit_type2()->result();
		$expense = $this->Xin_model->get_expense_type2()->result()	;

		// print_r($expense);die;
		// $data = array();
		$pdf->SetFont('Times','',10);
		$no = 1;
		$total = array();
		$total_debit = array();
		$total_credit = array();

		foreach($deposit as $r) {
			
			$sub_deposit = $this->Xin_model->get_sub_deposit_type_id($r->id)->result();

			$sub_deposit_ids = array();

			foreach($sub_deposit as $sub){
				array_push($sub_deposit_ids, $sub->id);
			}

			$all_deposit_transactions = $this->Xin_model->get_all_income_transaction();

			$amount = array();
			
			foreach($all_deposit_transactions as $s){
				$all_ids = explode(",", $s->sub_deposit_ids);

				$check = array_intersect($all_ids, $sub_deposit_ids);

				if (!empty($check)) {

					array_push($amount, $s->amount);					

				}
			}

			$account_code = $r->account_code;
			$account_name = $r->name;
			$debit_amount = '-';
			$credit_amount = number_format(array_sum($amount));

			array_push($total_credit, array_sum($amount));

			$html .= 
	                "<tr>
	                    <td align=\"left\">".$account_code."</td>
	                    <td align=\"left\">".$account_name."</td>
	                    <td align=\"right\">".$debit_amount."</td>
	                    <td align=\"right\">".$credit_amount."</td>
	                </tr>";
				$no++;

			unset($amount);

		}

		foreach($expense as $r) {
			$sub_expense = $this->Xin_model->get_sub_expense_type_id($r->expense_type_id)->result();

			$sub_expense_ids = array();

			foreach($sub_expense as $sub){
				array_push($sub_expense_ids, $sub->id);
			}

			$all_expense_transactions = $this->Xin_model->get_all_expense_transaction();

			$amount = array();
			
			foreach($all_expense_transactions as $s){
				$all_ids = explode(",", $s->subexpenses);

				$check = array_intersect($all_ids, $sub_expense_ids);

				// echo "$s->transaction_id\n";
				// print_r($check);continue;


				if (!empty($check)) {

					array_push($amount, $s->amount);					

				}
			}
			
			$account_code = $r->account_code;
			$account_name = $r->name;
			$debit_amount = number_format(array_sum($amount));
			$credit_amount = '-';

			array_push($total_debit, array_sum($amount));

			$html .= 
	                "<tr>
	                    <td align=\"left\">".$account_code."</td>
	                    <td align=\"left\">".$account_name."</td>
	                    <td align=\"right\">".$debit_amount."</td>
	                    <td align=\"right\">".$credit_amount."</td>
	                </tr>";
				$no++;

			unset($amount);
			unset($sub_expense_ids);

		}

		//invoice
		$all_invoices = $this->Invoices_model->get_invoices_paid();

		$total = array();

		if (!empty($all_invoices)) {
			$i = 1;
			foreach($all_invoices->result() as $invoice){
				array_push($total,$invoice->grand_total);
				$html .= 
		            "<tr>
		                <td align=\"left\">9000-".$i."</td>
		                <td align=\"left\">".$invoice->invoice_number."</td>
		                <td align=\"right\">".number_format($invoice->grand_total)."</td>
		                <td align=\"right\">-</td>
		            </tr>";
		        $i++;
			}

			array_push($total_debit, array_sum($total));
		}

		// $html .= 
  //           "<tr>
  //               <td align=\"left\">9000-1</td>
  //               <td align=\"left\">Premium generated</td>
  //               <td align=\"right\">".number_format(array_sum($total))."</td>
  //               <td align=\"right\">-</td>
  //           </tr>";


        //all bills paid
        $all_bills = $this->Finance_model->get_all_bill_payments();
        $all_bulk_bills = $this->Finance_model->get_all_bulk_bill_payments();
        $bill_total = array();

        if (!empty($all_bills)) {
        	foreach($all_bills as $bill){
        		$amount = $this->Clients_model->get_diagnose($bill->newbill_tid);
        		// print_r($amount->result()[0]);continue;
        		if (!empty($amount->result())) {
	        		array_push($bill_total, $amount->result()[0]->diagnose_total_sum);
        		}
        	}
        }

        if (!empty($all_bulk_bills)) {
        	foreach($all_bulk_bills as $bill){
        		$amount = $this->Clients_model->get_bulk($bill->newbill_tid)->result();
        		if (!empty($amount)) {
	        		$ids = explode(",", $amount[0]->bulk_id);
	        		foreach($ids as $id){
 			       		$amt = $this->Clients_model->get_diagnose($id);
	        			if (!empty($amt->result())) {
			        		array_push($bill_total, $amt->result()[0]->diagnose_total_sum);
		        		}
	        		}
        		}

        	}
        }

        $html .= 
            "<tr>
                <td align=\"left\">8000-1</td>
                <td align=\"left\">PHIS Claims</td>
                <td align=\"right\">".number_format(array_sum($bill_total))."</td>
                <td align=\"right\">-</td>
            </tr>";

        //capitation
        $all_capitation = $this->Clients_model->get_capitation()->result();
        $cost = $this->Xin_model->capitation_cost();

        $capitation_total = count($all_capitation)*$cost[0]->cost;

        $html .= 
            "<tr>
                <td align=\"left\">8000-2</td>
                <td align=\"left\">NHIS Claims</td>
                <td align=\"right\">".number_format($capitation_total)."</td>
                <td align=\"right\">-</td>
            </tr>";

		$html .= "
			<tr style=\"border-top: 1px solid black\">
				<td></td>
				<td><b>Total</b></td>
				<td align=\"right\"><b>".number_format(array_sum($total_debit))."</b></td>
				<td align=\"right\"><b>".number_format(array_sum($total_credit))."</b></td>
			</tr>

		</tbody>
		</table>
		";

		$pdf->writeHTML($html, true, 0, true, 0);

		// $pdf->SetY(-15);
  //       // Set font
  //       $pdf->SetFont('helvetica', 'I', 8);
  //       // Page number
  //       $pdf->Cell(0, 10, 'Page '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

		$pdf->Output('General Ledger Trial Balance.pdf', 'D');
	}

	public function pdf4($year=2020)
	{

		$from = $year."-01-01 00:00:01";
		$to = $year."-12-31 23:59:59";

		// print_r($diagnose);

		$from2 = new DateTime($from);
		$to2 = new DateTime($to);
		$day = $this->ordinal($from2->format('j'));
		$day2 = $this->ordinal($to2->format('j'));

		$year = $from2->format('y');

		$this->load->library('Pdf');

		$pdf = new Pdf('L', 'mm', 'F4', true, 'UTF-8', false);
		$pdf->SetTitle('General Ledger');
		$pdf->SetTopMargin(5);
		$pdf->SetLeftMargin(10);
		$pdf->SetRightMargin(10);
		// $pdf->SetBottomMargin(20);
		$pdf->setFooterMargin(20);
		$pdf->SetAutoPageBreak(true, 10);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);	
		$pdf->AddPage();

		// $pdf->Image('/uploads/logo/signin/signin_logo_1573568882.png', 170, 10, 30, 30, '','', '', true, 150, '', false,false, 1, true);
		$pdf->SetFont('helvetica','B',12);

		$pdf->Write(5, "", '', 0, 'C', true, 0, false, false, 0);
		$pdf->Write(5, "PRECIOUS HEALTHCARE LIMITED", '', 0, 'C', true, 0, false, false, 0);
		$pdf->Write(5, "General Ledger", '', 0, 'C', true, 0, false, false, 0);
		$pdf->Write(5, "From ".$day.$from2->format(' F Y')." to ".$day2.$to2->format(' F Y'), '', 0, 'C', true, 0, false, false, 0);
		$pdf->Write(5, "", '', 0, 'C', true, 0, false, false, 0);
		
		$pdf->SetFont('helvetica','',12);
		/*
		$html = "
		<h3>Showing Data from ".$day." ".$from->format("F Y")." - ".$day2." ".$to->format("F Y")."</h3>
		";*/

		$html = "
		<table align=\"center\">
		<thead>
			<tr style=\"border-top: 1px solid black; border-bottom: 1px solid black;\">
				<th align=\"left\" width=\"15%\"><b>Account ID</b>\n<b>Account Description</b></th>
				<th align=\"left\" width=\"10%\"><b>Date</b></th>
				<th align=\"left\" width=\"10%\"><b>Ref</b></th>
				<th align=\"left\" width=\"10%\"><b>Jrnl</b></th>
				<th align=\"center\" width=\"17%\"><b>Trans Desc</b></th>
				<th align=\"right\"><b>Debit Amt</b></th>
				<th align=\"right\"><b>Credit Amt</b></th>
				<th align=\"right\"><b>Balance</b></th>
			</tr>
		</thead>
		<tbody>";

		$deposit = $this->Xin_model->get_deposit_type2()->result();
		$expense = $this->Xin_model->get_expense_type2()->result()	;

		// print_r($expense);die;
		// $data = array();
		$pdf->SetFont('helvetica','',10);
		$no = 1;
		$total = array();
		$total_debit = array();
		$total_credit = array();

		// print_r($deposit);die;

		foreach($deposit as $r) {
			
			$sub_deposit = $this->Xin_model->get_sub_deposit_type_id($r->id)->result();

			$sub_deposit_ids = array();

			foreach($sub_deposit as $sub){
				array_push($sub_deposit_ids, $sub->id);
			}

			$all_deposit_transactions = $this->Xin_model->get_all_income_transaction($from,$to);

			$all_deposit_transactions_before = $this->Xin_model->get_all_income_transaction_before($from);

			// print_r($all_deposit_transactions_before);

			$amount = array();

			$deposit_transactions = array();
			
			foreach($all_deposit_transactions as $s){
				$all_ids = explode(",", $s->sub_deposit_ids);

				$check = array_intersect($all_ids, $sub_deposit_ids);

				if (!empty($check)) {

					$deposit_transactions[] = $s;

					// array_push($amount, $s->amount);					
				}
			}

			$deposit_transactions_before = array();

			foreach($all_deposit_transactions_before as $s){
				$all_ids = explode(",", $s->sub_deposit_ids);

				$check = array_intersect($all_ids, $sub_deposit_ids);

				if (!empty($check)) {

					$deposit_transactions_before[] = $s->amount;

					// array_push($amount, $s->amount);					
				}
			}

			// echo "Found ".count($deposit_transactions_before).", total: ".array_sum($deposit_transactions_before);

			$account_code = $r->account_code;
			$account_name = $r->name;
			$date = 'date';
			$ref = '';
			$jrnl = '';
			$description = 'Beginning Balance';

			$debit_amount = '';
			// $credit_amount = number_format(array_sum($amount));
			$credit_amount = '';

			$balance = $r->balance;

			$balance_before = array_sum($deposit_transactions_before);

			array_push($total_credit, array_sum($amount));

			for($i=1;$i<=12;$i++)
			{
				if($i==1){
					$html .= 
	                "<tr>
	                    <td align=\"left\" width=\"15%\">".$account_code."\n".$account_name."</td>
	                    <td align=\"left\" width=\"10%\">".$i."/1/".$year."</td>
	                    <td align=\"left\" width=\"10%\">".$ref."</td>
	                    <td align=\"left\" width=\"10%\">".$jrnl."</td>
	                    <td align=\"left\" width=\"17%\">".$description."</td>
	                    <td align=\"right\">".$debit_amount."</td>
	                    <td align=\"right\">".$credit_amount."</td>
	                    <td align=\"right\">".number_format($balance_before)."</td>
	         	       </tr>";
	
	             	    $amount_month2 = array();
						foreach($deposit_transactions as $dt)
		         	    {
		         	    	$date = new DateTime($dt->created_at);
		         	    	// echo $i." : ".$date->format('n')." : ".$dt->created_at."<br>";
		         	    	if ($date->format('n') == $i) {
		         	    		$amount_month2[] = $dt->amount;
		         	    		$html .= 
				                "<tr>
				                    <td align=\"left\"></td>
				                    <td align=\"left\">".$date->format('n/j/y')."</td>
				                    <td align=\"left\">".$dt->reference."</td>
				                    <td align=\"left\">".$jrnl."</td>
				                    <td align=\"left\">".substr($dt->description, 0,17)."</td>
				                    <td align=\"right\">".$dt->amount."</td>
				                    <td align=\"right\">".$credit_amount."</td>
				                    <td align=\"right\"></td>
				         	       </tr>";
		         	    	}
		         	    }

		         	    if (!empty($amount_month2)) {
	         	    		$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">Current Period Cha</td>
			                    <td align=\"right\">".array_sum($amount_month2)."</td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".array_sum($amount_month2)."</td>
			         	       </tr>";
	         	    		$balance_before = $balance_before+array_sum($amount_month2);
	         	    	}
	         	    
					$no++;
				}else{
					
					$html .= 
	                "<tr>
	                    <td align=\"left\"></td>
	                    <td align=\"left\">".$i."/1/".$year."</td>
	                    <td align=\"left\">".$ref."</td>
	                    <td align=\"left\">".$jrnl."</td>
	                    <td align=\"left\">".$description."</td>
	                    <td align=\"right\">".$debit_amount."</td>
	                    <td align=\"right\">".$credit_amount."</td>
	                    <td align=\"right\">".number_format($balance_before)."</td>
	                </tr>";
	                $amount_month = array();
	                foreach($deposit_transactions as $dt)
	         	    {
	         	    	$date = new DateTime($dt->created_at);
	         	    	// echo $i." : ".$date->format('n')." : ".$dt->created_at."<br>";
	         	    	if ($date->format('n') == $i) {
	         	    		$amount_month[] = $dt->amount;
	         	    		$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">".$date->format('n/j/y')."</td>
			                    <td align=\"left\">".$dt->reference."</td>
			                    <td align=\"left\">".$jrnl."</td>
			                    <td align=\"left\">".substr($dt->description, 0,17)."</td>
			                    <td align=\"right\">".$dt->amount."</td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\"></td>
			         	       </tr>";
	         	    	}
	         	    }

	         	    if (!empty($amount_month)) {
	         	    	$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">Current Period Cha</td>
			                    <td align=\"right\">".array_sum($amount_month)."</td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".array_sum($amount_month)."</td>
			         	       </tr>";
			         	$balance_before = $balance_before+array_sum($amount_month);
	         	    }

					if ($i == 12) {
						$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">".$i."/31/".$year."</td>
			                    <td align=\"left\">".$ref."</td>
			                    <td align=\"left\">".$jrnl."</td>
			                    <td align=\"left\"><b>Ending Balance</b></td>
			                    <td align=\"right\">".$debit_amount."</td>
			                    <td align=\"right\">".$credit_amount."</td>
			                    <td align=\"right\">".number_format($balance_before)."</td>
			                </tr>";
						$html .= "<tr>
							<td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"right\"></td>
		                    <td align=\"right\"></td>
		                    <td align=\"right\"></td>

						</tr>";
					}
					$no++;
				}
								
			}


			// $html .= 
	  //               "<tr>
	  //                   <td align=\"left\">".$account_code."\n".$account_name."</td>
	  //                   <td align=\"left\">".."</td>
	  //                   <td align=\"right\">".$debit_amount."</td>
	  //                   <td align=\"right\">".$credit_amount."</td>
	  //               </tr>";
			// 	$no++;

			unset($amount);

		}

		foreach($expense as $r) {
			$sub_expense = $this->Xin_model->get_sub_expense_type_id($r->expense_type_id)->result();

			$sub_expense_ids = array();

			foreach($sub_expense as $sub){
				array_push($sub_expense_ids, $sub->id);
			}

			$all_expense_transactions = $this->Xin_model->get_all_expense_transaction($from,$to);

			$all_expense_transactions_before = $this->Xin_model->get_all_expense_transaction_before($from);

			$amount = array();

			$expense_transactions = array();
			
			foreach($all_expense_transactions as $s){
				$all_ids = explode(",", $s->subexpenses);

				$check = array_intersect($all_ids, $sub_expense_ids);

				// echo "$s->transaction_id\n";
				// print_r($check);continue;


				if (!empty($check)) {

					$expense_transactions[] = $s;
					// array_push($amount, $s->amount);					

				}
			}

			$expense_transactions_before = array();
			
			foreach($all_expense_transactions_before as $s){
				$all_ids = explode(",", $s->subexpenses);

				$check = array_intersect($all_ids, $sub_expense_ids);

				// echo "$s->transaction_id\n";
				// print_r($check);continue;


				if (!empty($check)) {

					// $expense_transactions[] = $s;
					$expense_transactions_before[] = $s->amount;
					// array_push($amount, $s->amount);					

				}
			}

			// print_r($expense_transactions);

			$account_code = $r->account_code;
			$account_name = $r->name;
			$date = 'date';
			$ref = '';
			$jrnl = '';
			$description = 'Beginning Balance';

			$debit_amount = '';
			// $credit_amount = number_format(array_sum($amount));
			$credit_amount = '';

			$balance = $r->balance;

			$balance_before = array_sum($expense_transactions_before);

			array_push($total_credit, array_sum($amount));

			for($i=1;$i<=12;$i++)
			{
				if($i==1){
					$html .= 
	                "<tr>
	                    <td align=\"left\" width=\"15%\">".$account_code."\n".$account_name."</td>
	                    <td align=\"left\" width=\"10%\">".$i."/1/".$year."</td>
	                    <td align=\"left\" width=\"10%\">".$ref."</td>
	                    <td align=\"left\" width=\"10%\">".$jrnl."</td>
	                    <td align=\"left\" width=\"17%\">".$description."</td>
	                    <td align=\"right\">".$debit_amount."</td>
	                    <td align=\"right\">".$credit_amount."</td>
	                    <td align=\"right\">".number_format($balance_before)."</td>
	         	       </tr>";

	             	    $amount_month2 = array();
						foreach($expense_transactions as $dt)
		         	    {
		         	    	$date = new DateTime($dt->created_at);
		         	    	// echo $i." : ".$date->format('n')." : ".$dt->created_at."<br>";
		         	    	if ($date->format('n') == $i) {
		         	    		$amount_month2[] = $dt->amount;
		         	    		$html .= 
				                "<tr>
				                    <td align=\"left\"></td>
				                    <td align=\"left\">".$date->format('n/j/y')."</td>
				                    <td align=\"left\">".$dt->reference."</td>
				                    <td align=\"left\">".$jrnl."</td>
				                    <td align=\"left\">".substr($dt->description, 0,17)."</td>
				                    <td align=\"right\">".$credit_amount."</td>
				                    <td align=\"right\">".$dt->amount."</td>
				                    <td align=\"right\"></td>
				         	       </tr>";
		         	    	}
		         	    }

		         	    if (!empty($amount_month2)) {
	         	    	$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">Current Period Cha</td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".array_sum($amount_month2)."</td>
			                    <td align=\"right\">".array_sum($amount_month2)."</td>
			         	       </tr>";
	         	    	$balance_before = $balance_before+array_sum($amount_month2);
	         	    	}
	         	    
					$no++;
				}else{
					
					$html .= 
	                "<tr>
	                    <td align=\"left\"></td>
	                    <td align=\"left\">".$i."/1/".$year."</td>
	                    <td align=\"left\">".$ref."</td>
	                    <td align=\"left\">".$jrnl."</td>
	                    <td align=\"left\">".$description."</td>
	                    <td align=\"right\">".$debit_amount."</td>
	                    <td align=\"right\">".$credit_amount."</td>
	                    <td align=\"right\">".number_format($balance_before)."</td>
	                </tr>";

	                $amount_month = array();
	                foreach($expense_transactions as $dt)
	         	    {
	         	    	$date = new DateTime($dt->created_at);
	         	    	// echo $i." : ".$date->format('n')." : ".$dt->created_at."<br>";
	         	    	if ($date->format('n') == $i) {
	         	    		$amount_month[] = $dt->amount;
	         	    		$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">".$date->format('n/j/y')."</td>
			                    <td align=\"left\">".$dt->reference."</td>
			                    <td align=\"left\">".$jrnl."</td>
			                    <td align=\"left\">".substr($dt->description, 0,17)."</td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".$dt->amount."</td>
			                    <td align=\"right\"></td>
			         	       </tr>";
	        	 	    	}
	         	    }

	         	    if (!empty($amount_month)) {
	         	    	$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">Current Period Cha</td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".array_sum($amount_month)."</td>
			                    <td align=\"right\">".array_sum($amount_month)."</td>
			         	       </tr>";
			         	$balance_before = $balance_before + array_sum($amount_month);
	         	    }

					if ($i == 12) {
						$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">".$i."/31/".$year."</td>
			                    <td align=\"left\">".$ref."</td>
			                    <td align=\"left\">".$jrnl."</td>
			                    <td align=\"left\"><b>Ending Balance</b></td>
			                    <td align=\"right\">".$debit_amount."</td>
			                    <td align=\"right\">".$credit_amount."</td>
			                    <td align=\"right\">".number_format($balance_before)."</td>
			                </tr>";
						$html .= "<tr>
							<td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"right\"></td>
		                    <td align=\"right\"></td>
		                    <td align=\"right\"></td>

						</tr>";
					}
					$no++;
				}
								
			}
			
			// $account_code = $r->account_code;
			// $account_name = $r->name;
			// $debit_amount = number_format(array_sum($amount));
			// $credit_amount = '-';

			// array_push($total_debit, array_sum($amount));

			// $html .= 
	  //               "<tr>
	  //                   <td align=\"left\">".$account_code."</td>
	  //                   <td align=\"left\">".$account_name."</td>
	  //                   <td align=\"right\">".$debit_amount."</td>
	  //                   <td align=\"right\">".$credit_amount."</td>
	  //               </tr>";
			// 	$no++;

			unset($amount);
			unset($sub_expense_ids);

		}

		// Invoice
		$all_invoice = $this->Invoices_model->get_invoices_paid($from,$to);
		$all_invoice_before = $this->Invoices_model->get_invoices_paid_before($from);

		$total_invoice_before = array();

		foreach($all_invoice_before->result() as $invoice_before)
		{
			$total_invoice_before[] = $invoice_before->grand_total;
		}

		$balance_before = array_sum($total_invoice_before);

		if(!empty($all_invoice)){
			$description = "Beginning Balance";
			// print_r($all_invoice->result());

			for($i=1;$i<=12;$i++)
			{
				if($i==1){
					$html .= 
	                "<tr>
	                    <td align=\"left\" width=\"15%\">9000 - Invoice</td>
	                    <td align=\"left\" width=\"10%\">".$i."/1/".$year."</td>
	                    <td align=\"left\" width=\"10%\"></td>
	                    <td align=\"left\" width=\"10%\"></td>
	                    <td align=\"left\" width=\"17%\">".$description."</td>
	                    <td align=\"right\"></td>
	                    <td align=\"right\"></td>
	                    <td align=\"right\">".number_format($balance_before)."</td>
	         	       </tr>";

	             	    $amount_month2 = array();
						foreach($all_invoice->result() as $dt)
		         	    {
		         	    	$date = new DateTime($dt->created_at);
		         	    	$organization = $this->Clients_model->get_organization_info($dt->company_name)->result();
		         	    	if (!empty($organization)) {
		         	    		$organization = $organization[0]->name;
		         	    	}else{
		         	    		$organization = "-";
		         	    	}
		         	    	// echo $i." : ".$date->format('n')." : ".$dt->created_at."<br>";
		         	    	if ($date->format('n') == $i) {
		         	    		$amount_month2[] = $dt->grand_total;
		         	    		$html .= 
				                "<tr>
				                    <td align=\"left\"></td>
				                    <td align=\"left\">".$date->format('n/j/y')."</td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\">".substr($organization, 0,17)."</td>
				                    <td align=\"right\"></td>
				                    <td align=\"right\">".$dt->grand_total."</td>
				                    <td align=\"right\"></td>
				         	       </tr>";
		         	    	}
		         	    }

		         	    if (!empty($amount_month2)) {
		         	    	$html .= 
				                "<tr>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\">Current Period Cha</td>
				                    <td align=\"right\"></td>
				                    <td align=\"right\">".array_sum($amount_month2)."</td>
				                    <td align=\"right\">".array_sum($amount_month2)."</td>
				         	       </tr>";
		         	    	$balance_before = $balance_before+array_sum($amount_month2);
	         	    	}
	         	    
					$no++;
				}else{
					
					$html .= 
	                "<tr>
	                    <td align=\"left\"></td>
	                    <td align=\"left\">".$i."/1/".$year."</td>
	                    <td align=\"left\">".$ref."</td>
	                    <td align=\"left\">".$jrnl."</td>
	                    <td align=\"left\">".$description."</td>
	                    <td align=\"right\">".$debit_amount."</td>
	                    <td align=\"right\">".$credit_amount."</td>
	                    <td align=\"right\">".number_format($balance_before)."</td>
	                </tr>";

	                $amount_month = array();
					foreach($all_invoice->result() as $dt)
	         	    {
	         	    	$date = new DateTime($dt->created_at);
	         	    	// echo $i." : ".$date->format('n')." : ".$dt->created_at."<br>";
	         	    	if ($date->format('n') == $i) {
	         	    		$organization = $this->Clients_model->get_organization_info($dt->company_name)->result();
		         	    	if (!empty($organization)) {
		         	    		$organization = $organization[0]->name;
		         	    	}else{
		         	    		$organization = "-";
		         	    	}

	         	    		$amount_month[] = $dt->grand_total;
	         	    		$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">".$date->format('n/j/y')."</td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">".substr($organization, 0,17)."</td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".$dt->grand_total."</td>
			                    <td align=\"right\"></td>
			         	       </tr>";
	         	    	}
	         	    }

	         	    if (!empty($amount_month)) {
	         	    	$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">Current Period Cha</td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".array_sum($amount_month)."</td>
			                    <td align=\"right\">".array_sum($amount_month)."</td>
			         	       </tr>";
	         	    	$balance_before = $balance_before+array_sum($amount_month);
         	    	}

					if ($i == 12) {
						$html .= 
			                "<tr>
			                    <td align=\"left\" width=\"15%\"></td>
			                    <td align=\"left\" width=\"10%\">".$i."/31/".$year."</td>
			                    <td align=\"left\" width=\"10%\"></td>
			                    <td align=\"left\" width=\"10%\"></td>
			                    <td align=\"left\" width=\"17%\"><b>Ending Balance</b></td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".number_format($balance_before)."</td>
			         	       </tr>";
						$html .= "<tr>
							<td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"right\"></td>
		                    <td align=\"right\"></td>
		                    <td align=\"right\"></td>

						</tr>";
					}
					$no++;
				}
								
			}

		}

		// Capitation
		$all_capitation = $this->Clients_model->get_capitation2($from,$to);
		$all_capitation_before = $this->Clients_model->get_capitation2_before($from);

		$cost = $this->Xin_model->capitation_cost();

		$total_capitation_before = count($all_capitation_before->result());

		$total_capitation = count($all_capitation->result());

		$balance_before = $total_capitation_before*$cost[0]->cost;

		if(!empty($all_capitation)){
			$description = "Beginning Balance";
			// print_r($all_invoice->result());

			for($i=1;$i<=12;$i++)
			{
				if($i==1){
					$html .= 
	                "<tr>
	                    <td align=\"left\" width=\"15%\">11000 - NHIS Capitation</td>
	                    <td align=\"left\" width=\"10%\">".$i."/1/".$year."</td>
	                    <td align=\"left\" width=\"10%\"></td>
	                    <td align=\"left\" width=\"10%\"></td>
	                    <td align=\"left\" width=\"17%\">".$description."</td>
	                    <td align=\"right\"></td>
	                    <td align=\"right\"></td>
	                    <td align=\"right\">".number_format($balance_before)."</td>
	         	       </tr>";

	             	    $amount_month2 = 0;
						foreach($all_capitation->result() as $dt)
		         	    {
		         	    	$date = new DateTime($dt->created_at);

		         	    	if ($date->format('n') == $i) {
		         	    		$amount_month2++;
		         	    		$html .= 
				                "<tr>
				                    <td align=\"left\"></td>
				                    <td align=\"left\">".$date->format('n/j/y')."</td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\">NHIS Capitation</td>
				                    <td align=\"right\"></td>
				                    <td align=\"right\">".$cost[0]->cost."</td>
				                    <td align=\"right\"></td>
				         	       </tr>";
		         	    	}
		         	    }

		         	    if ($amount_month2!=0) {
		         	    	$html .= 
				                "<tr>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\">Current Period Cha</td>
				                    <td align=\"right\"></td>
				                    <td align=\"right\">".$total_capitation*$cost[0]->cost."</td>
				                    <td align=\"right\">".$total_capitation*$cost[0]->cost."</td>
				         	       </tr>";
		         	    	$balance_before = $balance_before+($total_capitation*$cost[0]->cost);
	         	    	}
	         	    
					$no++;
				}else{
					
					$html .= 
	                "<tr>
	                    <td align=\"left\"></td>
	                    <td align=\"left\">".$i."/1/".$year."</td>
	                    <td align=\"left\">".$ref."</td>
	                    <td align=\"left\">".$jrnl."</td>
	                    <td align=\"left\">".$description."</td>
	                    <td align=\"right\">".$debit_amount."</td>
	                    <td align=\"right\">".$credit_amount."</td>
	                    <td align=\"right\">".number_format($balance_before)."</td>
	                </tr>";

	                $amount_month = 0;
					foreach($all_capitation->result() as $dt)
	         	    {
	         	    	$date = new DateTime($dt->created_at);

	         	    	if ($date->format('n') == $i) {

	         	    		$amount_month++;
	         	    		$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">".$date->format('n/j/y')."</td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">NHIS Capitation</td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".$cost[0]->cost."</td>
			                    <td align=\"right\"></td>
			         	       </tr>";
	         	    	}
	         	    }

	         	    if (!empty($amount_month)) {
	         	    	$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">Current Period Cha</td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".$total_capitation*$cost[0]->cost."</td>
			                    <td align=\"right\">".$total_capitation*$cost[0]->cost."</td>
			         	       </tr>";
	         	    	$balance_before = $balance_before+($total_capitation*$cost[0]->cost);
         	    	}

					if ($i == 12) {
						$html .= 
			                "<tr>
			                    <td align=\"left\" width=\"15%\"></td>
			                    <td align=\"left\" width=\"10%\">".$i."/31/".$year."</td>
			                    <td align=\"left\" width=\"10%\"></td>
			                    <td align=\"left\" width=\"10%\"></td>
			                    <td align=\"left\" width=\"17%\"><b>Ending Balance</b></td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".number_format($balance_before)."</td>
			         	       </tr>";
						$html .= "<tr>
							<td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"right\"></td>
		                    <td align=\"right\"></td>
		                    <td align=\"right\"></td>

						</tr>";
					}
					$no++;
				}
								
			}

		}

		// Medical Expense
		$diagnose_ids = array();
		$all_bills = $this->Finance_model->get_all_bill_payments($from,$to);
		$all_bulk_bills = $this->Finance_model->get_all_bulk_bill_payments($from,$to);

		$diagnose_ids_before = array();
		$all_bills_before = $this->Finance_model->get_all_bill_payments_before($from);
		$all_bulk_bills_before = $this->Finance_model->get_all_bulk_bill_payments_before($from);

		if (!empty($all_bills)) {
			foreach($all_bills as $bill)
			{
				$diagnose_ids[] = $bill->newbill_tid;
			}
		}

		if (!empty($all_bulk_bills)) {
			foreach($all_bulk_bills as $bulk_bill)
			{
				$bulk = $this->Finance_model->get_bulk_id_from_bill($bulk_bill->newbill_tid);
				if (!empty($bulk)) {
					$bulk_id = explode(",",$bulk[0]->bulk_id);
					foreach($bulk_id as $b)
					{
						$diagnose_ids[] = $b;
					}
				}
			}
		}

		if (!empty($all_bills_before)) {
			foreach($all_bills_before as $bill)
			{
				$diagnose_ids_before[] = $bill->newbill_tid;
			}
		}

		if (!empty($all_bulk_bills_before)) {
			foreach($all_bulk_bills_before as $bulk_bill)
			{
				$bulk = $this->Finance_model->get_bulk_id_from_bill($bulk_bill->newbill_tid);
				if (!empty($bulk)) {
					$bulk_id = explode( ",",$bulk[0]->bulk_id);
					foreach($bulk_id as $b)
					{
						$diagnose_ids_before[] = $b;
					}
				}
			}
		}

		$total_amount_before = array();
		$total_amount_before2 = array();

		foreach($diagnose_ids_before as $d)
		{
			$diagnose = $this->Clients_model->get_diagnose($d)->result();
			if (!empty($diagnose)) {
				if ($diagnose[0]->is_capitation != '' OR $diagnose[0]->is_capitation == 0) 
				{	
					$total_amount_before[] = $diagnose[0]->diagnose_total_sum;
				}else{
					$total_amount_before2[] = $diagnose[0]->diagnose_total_sum;
				}
			}
		}


		$balance_before = array_sum($total_amount_before);

		if(!empty($diagnose_ids)){

			$description = "Beginning Balance";
			// print_r($all_invoice->result());

			for($i=1;$i<=12;$i++)
			{
				if($i==1){
					$html .= 
	                "<tr>
	                    <td align=\"left\" width=\"15%\">8000 - PHIS Fee for Service </td>
	                    <td align=\"left\" width=\"10%\">".$i."/1/".$year."</td>
	                    <td align=\"left\" width=\"10%\"></td>
	                    <td align=\"left\" width=\"10%\"></td>
	                    <td align=\"left\" width=\"17%\">".$description."</td>
	                    <td align=\"right\"></td>
	                    <td align=\"right\"></td>
	                    <td align=\"right\">".number_format($balance_before)."</td>
	         	       </tr>";

	             	    $amount_month2 = array();
						foreach($diagnose_ids as $dt)
		         	    {
		         	    	$dt = $this->Clients_model->get_diagnose($dt)->result();
		         	    	if (empty($dt)) {
		         	    		continue;
		         	    	}else{
		         	    		$dt = $dt[0];
		         	    	}

		         	    	if (is_null($dt->is_capitation) OR $dt->is_capitation == 0) 
		         	    	{
		         	    		continue;
		         	    	}
		         	    	$date = new DateTime($dt->diagnose_date_time);
		         	    	$hospital = $this->Clients_model->get_hospital_info($dt->diagnose_hospital_id)->result();
		         	    	if ($dt->is_capitation==1) 
		         	    	{
	     	    				continue;
		         	    	}
		         	    	// echo $i." : ".$date->format('n')." : ".$dt->created_at."<br>";
		         	    	if ($date->format('n') == $i) {
		         	    		$amount_month2[] = $dt->diagnose_total_sum;
		         	    		$html .= 
				                "<tr>
				                    <td align=\"left\"></td>
				                    <td align=\"left\">".$date->format('n/j/y')."</td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\">".substr($hosptital, 0,17)."</td>
				                    <td align=\"right\"></td>
				                    <td align=\"right\">".$dt->diagnose_total_sum."</td>
				                    <td align=\"right\"></td>
				         	       </tr>";
		         	    	}
		         	    }

		         	    if (!empty($amount_month2)) {
		         	    	$html .= 
				                "<tr>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\">Current Period Cha</td>
				                    <td align=\"right\"></td>
				                    <td align=\"right\">".array_sum($amount_month2)."</td>
				                    <td align=\"right\">".array_sum($amount_month2)."</td>
				         	       </tr>";
		         	    	$balance_before = $balance_before+array_sum($amount_month2);
	         	    	}
	         	    
					$no++;
				}else{
					
					$html .= 
	                "<tr>
	                    <td align=\"left\"></td>
	                    <td align=\"left\">".$i."/1/".$year."</td>
	                    <td align=\"left\">".$ref."</td>
	                    <td align=\"left\">".$jrnl."</td>
	                    <td align=\"left\">".$description."</td>
	                    <td align=\"right\">".$debit_amount."</td>
	                    <td align=\"right\">".$credit_amount."</td>
	                    <td align=\"right\">".number_format($balance_before)."</td>
	                </tr>";

	                $amount_month = array();
					foreach($diagnose_ids as $dt)
	         	    {
	         	    	$dt = $this->Clients_model->get_diagnose($dt)->result();
	         	    	if (empty($dt)) {
	         	    		continue;
	         	    	}else{
	         	    		$dt = $dt[0];
	         	    	}
	         	    	if ($dt->is_capitation==1)
	         	    	{
     	    				continue;
	         	    	}
	         	    	
	         	    	$date = new DateTime($dt->diagnose_date_time);
	         	    	$hospital = $this->Clients_model->get_hospital_info($dt->diagnose_hospital_id)->result();
	         	    	if (!empty($hospital)) {
	         	    		$hospital = $hospital[0]->hospital_name;
	         	    	}else{
	         	    		$hospital = "-";
	         	    	}

	         	    	if ($date->format('n') == $i) {

	         	    		$amount_month[] = $dt->diagnose_total_sum;
	         	    		$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">".$date->format('n/j/y')."</td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">".substr($hospital, 0,17)."</td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".$dt->diagnose_total_sum."</td>
			                    <td align=\"right\"></td>
			         	       </tr>";
	         	    	}
	         	    }

	         	    if (!empty($amount_month)) {
	         	    	$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">Current Period Cha</td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".array_sum($amount_month)."</td>
			                    <td align=\"right\">".array_sum($amount_month)."</td>
			         	       </tr>";
	         	    	$balance_before = $balance_before+array_sum($amount_month);
         	    	}

					if ($i == 12) {
						$html .= 
			                "<tr>
			                    <td align=\"left\" width=\"15%\"></td>
			                    <td align=\"left\" width=\"10%\">".$i."/31/".$year."</td>
			                    <td align=\"left\" width=\"10%\"></td>
			                    <td align=\"left\" width=\"10%\"></td>
			                    <td align=\"left\" width=\"17%\"><b>Ending Balance</b></td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".number_format($balance_before)."</td>
			         	       </tr>";
						$html .= "<tr>
							<td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"right\"></td>
		                    <td align=\"right\"></td>
		                    <td align=\"right\"></td>

						</tr>";
					}
					$no++;
				}
								
			}

		}

		// NHIS
		if(!empty($diagnose_ids)){

			$description = "Beginning Balance";
			// print_r($all_invoice->result());
			$balance_before = array_sum($total_amount_before2);

			for($i=1;$i<=12;$i++)
			{
				if($i==1){
					$html .= 
	                "<tr>
	                    <td align=\"left\" width=\"15%\">7000 - NHIS Fee for Service </td>
	                    <td align=\"left\" width=\"10%\">".$i."/1/".$year."</td>
	                    <td align=\"left\" width=\"10%\"></td>
	                    <td align=\"left\" width=\"10%\"></td>
	                    <td align=\"left\" width=\"17%\">".$description."</td>
	                    <td align=\"right\"></td>
	                    <td align=\"right\"></td>
	                    <td align=\"right\">".number_format($balance_before)."</td>
	         	       </tr>";

	             	    $amount_month2 = array();
						foreach($diagnose_ids as $dt)
		         	    {
		         	    	$dt = $this->Clients_model->get_diagnose($dt)->result();
		         	    	if (empty($dt)) {
		         	    		continue;
		         	    	}else{
		         	    		$dt = $dt[0];
		         	    	}	
		         	    	if ($dt->is_capitation==0 || $dt->is_capitation=='' )
		         	    	{
	         	    			continue;
		         	    	}
		         	    	$date = new DateTime($dt->diagnose_date_time);
		         	    	$hospital = $this->Clients_model->get_hospital_info($dt->diagnose_hospital_id)->result();
		         	    	if (!empty($hospital)) {
		         	    		$hospital = $hospital[0]->hospital_name;
		         	    	}else{
		         	    		$hospital = "-";
		         	    	}
		         	    	// echo $i." : ".$date->format('n')." : ".$dt->created_at."<br>";
		         	    	if ($date->format('n') == $i) {
		         	    		$amount_month2[] = $dt->diagnose_total_sum;
		         	    		$html .= 
				                "<tr>
				                    <td align=\"left\"></td>
				                    <td align=\"left\">".$date->format('n/j/y')."</td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\">".substr($hospital, 0,17)."</td>
				                    <td align=\"right\"></td>
				                    <td align=\"right\">".$dt->diagnose_total_sum."</td>
				                    <td align=\"right\"></td>
				         	       </tr>";
		         	    	}
		         	    }

		         	    if (!empty($amount_month2)) {
		         	    	$html .= 
				                "<tr>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\"></td>
				                    <td align=\"left\">Current Period Cha</td>
				                    <td align=\"right\"></td>
				                    <td align=\"right\">".array_sum($amount_month2)."</td>
				                    <td align=\"right\">".array_sum($amount_month2)."</td>
				         	       </tr>";
		         	    	$balance_before = $balance_before+array_sum($amount_month2);
	         	    	}
	         	    
					$no++;
				}else{
					
					$html .= 
	                "<tr>
	                    <td align=\"left\"></td>
	                    <td align=\"left\">".$i."/1/".$year."</td>
	                    <td align=\"left\">".$ref."</td>
	                    <td align=\"left\">".$jrnl."</td>
	                    <td align=\"left\">".$description."</td>
	                    <td align=\"right\">".$debit_amount."</td>
	                    <td align=\"right\">".$credit_amount."</td>
	                    <td align=\"right\">".number_format($balance_before)."</td>
	                </tr>";

	                $amount_month = array();
					foreach($diagnose_ids as $dt)
	         	    {
	         	    	$dt = $this->Clients_model->get_diagnose($dt)->result();
	         	    	if (empty($dt)) {
	         	    		continue;
	         	    	}else{
	         	    		$dt = $dt[0];
	         	    	}
	         	    	if ($dt->is_capitation==0 || $dt->is_capitation=='' ) 
	         	    	{
         	    			continue;
	         	    	}
	         	    	
	         	    	$date = new DateTime($dt->diagnose_date_time);
	         	    	$hospital = $this->Clients_model->get_hospital_info($dt->diagnose_hospital_id)->result();
	         	    	if (!empty($hospital)) {
	         	    		$hospital = $hospital[0]->hospital_name;
	         	    	}else{
	         	    		$hospital = "-";
	         	    	}

	         	    	if ($date->format('n') == $i) {

	         	    		$amount_month[] = $dt->diagnose_total_sum;
	         	    		$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">".$date->format('n/j/y')."</td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">".substr($hospital, 0,17)."</td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".$dt->diagnose_total_sum."</td>
			                    <td align=\"right\"></td>
			         	       </tr>";
	         	    	}
	         	    }

	         	    if (!empty($amount_month)) {
	         	    	$html .= 
			                "<tr>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\"></td>
			                    <td align=\"left\">Current Period Cha</td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".array_sum($amount_month)."</td>
			                    <td align=\"right\">".array_sum($amount_month)."</td>
			         	       </tr>";
	         	    	$balance_before = $balance_before+array_sum($amount_month);
         	    	}

					if ($i == 12) {
						$html .= 
			                "<tr>
			                    <td align=\"left\" width=\"15%\"></td>
			                    <td align=\"left\" width=\"10%\">".$i."/31/".$year."</td>
			                    <td align=\"left\" width=\"10%\"></td>
			                    <td align=\"left\" width=\"10%\"></td>
			                    <td align=\"left\" width=\"17%\"><b>Ending Balance</b></td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\"></td>
			                    <td align=\"right\">".number_format($balance_before)."</td>
			         	       </tr>";
						$html .= "<tr>
							<td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"left\"></td>
		                    <td align=\"right\"></td>
		                    <td align=\"right\"></td>
		                    <td align=\"right\"></td>

						</tr>";
					}
					$no++;
				}
								
			}

		}


		$html .= "

		</tbody>
		</table>
		";

		$pdf->writeHTML($html, true, 0, true, 0);

		$pdf->Output('General Ledger.pdf', 'D');
	}

	public function ordinal($number) {
		$ends = array('th','st','nd','rd','th','th','th','th','th','th');
		if ((($number % 100) >= 11) && (($number%100) <= 13))
			return $number. 'th';
		else
			return $number. $ends[$number % 10];
	}

	public function fetch_bills_id()
	{
		$id =  $this->input->post('id');
		$id = explode(",", $id);

		$html = '<table class="table table-striped">
			<thead>
				<tr>
					<th>Transaction ID</th>
					<th>Provider</th>
					<th>Name</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody>';

		// print_r($id);
		$total = array();
		foreach ($id as $i) {
			$diagnose = $this->Clients_model->get_diagnose($i)->result();

			$hospital = $this->Clients_model->get_hospital_info($diagnose[0]->diagnose_hospital_id)->result();

			!empty($hospital) ? $hospital_name = $hospital[0]->hospital_name : $hospital_name = '';

			$clients = $this->Clients_model->get_clients_by_id($diagnose[0]->diagnose_client_id)->result();
			!empty($clients) ? $clients_name = $clients[0]->name : $clients_name = '';
			// print_r($diagnose);

			// echo $i;
			// print_r($clients);continue;
			$hcp = preg_replace('/\s+/', '', $hospital[0]->hospital_name);
            $date = date("Ymd", strtotime($diagnose[0]->diagnose_date_time));



			$transaction_id = "TC-HCP-".$hcp."-".$date."-".$diagnose[0]->diagnose_transaction_id;
			$html .= '<tr>
					<td>'.$transaction_id.'</td>
					<td>'.$hospital_name.'</td>
					<td>'.$clients_name.'</td>
					<td>'.$this->Xin_model->currency_sign($diagnose[0]->diagnose_total_sum).'</td>
				</tr>';	

			array_push($total, $diagnose[0]->diagnose_total_sum);
		}

		$html .= '<tr>
					<td colspan="3" align="center"><b>Total</b></td>
					<td><b>'.$this->Xin_model->currency_sign(array_sum($total)).'</b></td>
				</tr>
		';

		$html .= '</tbody>
		</table>';

		echo $html;
	}

	public function fetch_premium_id()
	{
		$from = $this->input->post('from');
		$to = $this->input->post('to');

		$invoices = $this->Invoices_model->get_invoices_date_range_paid2($from,$to);

		$html = '<table class="table table-striped">
			<thead>
				<tr>
					<th>Debit Note Number</th>
					<th>Organization</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody>';

			$total = array();

		if (!empty($invoices)) {
			foreach($invoices->result() as $invoice)
			{
				$organization = $this->Clients_model->get_organization_info($invoice->company_name)->result(); 
				// print_r($invoice);
				$html .= '<tr>
					<td>'.$invoice->invoice_id.'</td>
					<td>'.$organization[0]->name.'</td>
					<td>'.$invoice->invoice_start_date.'</td>
					<td>'.$invoice->invoice_end_date.'</td>
					<td>'.$this->Xin_model->currency_sign($invoice->grand_total).'</td>
				</tr>';	

				$total[] = $invoice->grand_total;


			}
		}

		$html .= '<tr>
					<td colspan="4" align="center"><b>Total</b></td>
					<td><b>'.$this->Xin_model->currency_sign(array_sum($total)).'</b></td>
				</tr>
		';

		$html .= '</tbody>
		</table>';



		echo $html;
	}

	public function fetch_other_id()
	{
		$from = $this->input->post('from');
		$to = $this->input->post('to');

		$capitation = $this->Clients_model->get_capitation($from,$to)->num_rows();
		$cost = $this->Xin_model->capitation_cost();

		$html = '<table class="table table-striped">
			<thead>
				<tr>
					<th>Total Enrollees Capitation</th>
					<th>NHIS Capitation Cost</th>
					<th>Total Cost</th>
				</tr>
			</thead>
			<tbody>';

			$total = array();

		$html .= '<tr>
			<td>'.$capitation.' Person(s)</td>
			<td>'.$cost[0]->cost.'</td>
			<td>'.$this->Xin_model->currency_sign($capitation*$cost[0]->cost).'</td>
		</tr>';	


		$html .= '</tbody>
		</table>';



		echo $html;
	}

	public function fetch_income_id()
	{
		$from = $this->input->post('from');
		$to = $this->input->post('to');

		$deposit = $this->Finance_model->get_deposit_date_range($from,$to)->result();

		$html = '<table class="table table-striped">
			<thead>
				<tr>
					<th>Account</th>
					<th>Payer</th>
					<th>Payment Method</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody>';

		$total = array();

		foreach($deposit as $r)
		{
			$total[] = $r->amount;
			$amount = $this->Xin_model->currency_sign($r->amount);

			// account type

			$acc_type = $this->Finance_model->read_bankcash_information($r->account_id);

			if(!is_null($acc_type)){

				$account = $acc_type[0]->account_name;

			} else {

				$account = '--';	

			}

			

			// get user > added by

			
			$payer = $this->Finance_model->read_employee_info($r->payer_payee_id);

			// user full name

			if(!is_null($payer)){

				$full_name = $payer[0]->first_name." ".$payer[0]->last_name;

			} else {

				$full_name = '--';	

			}

			

			// deposit date

			$deposit_date = $this->Xin_model->set_date_format($r->transaction_date);

			// category


			if (is_null($r->sub_deposit_ids)) {
				$category = '-';
			}else{
				$sub_category = explode(",", $r->sub_deposit_ids);

				$category = array();

				foreach($sub_category as $sub)
				{
					$sub_deposit = $this->Finance_model->read_sub_deposit($sub);

					if (!empty($sub_deposit)) {
					
						$deposit_type = $this->Finance_model->read_deposit_type($sub_deposit[0]->deposit_id);
					
						$category[] = $deposit_type[0]->account_code." ".$sub_deposit[0]->name;
					}
				}

				$category = implode(", ", $category);

			}

			$payment_method = $this->Xin_model->read_payment_method($r->payment_method_id);

			if(!is_null($payment_method)){

				$method_name = $payment_method[0]->method_name;

			} else {

				$method_name = '--';	

			}

			$html .= '<tr>
				<td>'.$account.'</td>
				<td>'.$full_name.'</td>
				<td>'.$method_name.'</td>
				<td>'.$amount.'</td>
			</tr>';	
		}

		$html .= '<tr>
				<td colspan="3" align="center"><b>TOTAL</b></td>
				<td><b>'.$this->Xin_model->currency_sign(array_sum($total)).'</b></td>
			</tr>';

		$html .= '</tbody>
		</table>';



		echo $html;
	}

	public function fetch_medical_id()
	{
		$from = $this->input->post('from');
		$to = $this->input->post('to');

		$html = '<table class="table table-striped">
			<thead>
				<tr>
					<th>Account</th>
					<th>Date</th>
					<th>Payment Method</th>
					<th>Description</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody>';

		$total = array();

		$bills = $this->Finance_model->get_all_bill_payments($from,$to);
		$total_bills = array();

		foreach($bills as $bill)
		{
			$bank = $this->Finance_model->read_bankcash_information($bill->newbill_account);
			$method = $this->Xin_model->read_payment_method($bill->newbill_paymentmethod);
			$diagnose = $this->Clients_model->get_diagnose($bill->newbill_tid)->result();

			if(!empty($diagnose))
			{
				$total_bills[] = $diagnose[0]->diagnose_total_sum;
				$html .= "
					<tr>
						<td>".$bank[0]->account_name."</td>
						<td>".$bill->newbill_date."</td>
						<td>".$method[0]->method_name."</td>
						<td>".$bill->newbill_description."</td>
						<td>".$this->Xin_model->currency_sign($diagnose[0]->diagnose_total_sum)."</td>
					</tr>	

				";
				// $total_bills[] = $diagnose[0]->diagnose_total_sum;
			}


		}

		$total_bulk_bills = array();

		$bulk_bills = $this->Finance_model->get_all_bulk_bill_payments($from,$to);
		
		// print_r($bulk_bills);die;
		foreach($bulk_bills as $bulk_bill)
		{
			$ids = $this->Clients_model->get_bulk($bulk_bill->newbill_tid)->result();
			if (!empty($ids)) {
				$bulk_id = explode(",",$ids[0]->bulk_id);
				if (!empty($bulk_id)) {
					foreach($bulk_id as $id)
					{
						$diagnose = $this->Clients_model->get_diagnose($id)->result();
						// print_r($diagnose);continue;

						$bank = $this->Finance_model->read_bankcash_information($bill->newbill_account);
						$method = $this->Xin_model->read_payment_method($bill->newbill_paymentmethod);
						// $diagnose = $this->Clients_model->get_diagnose($bill->newbill_tid)->result();

						if(!empty($diagnose))
						{
							$total_bills[] = $diagnose[0]->diagnose_total_sum;
							$html .= "
								<tr>
									<td>".$bank[0]->account_name."</td>
									<td>".$bill->newbill_date."</td>
									<td>".$method[0]->method_name."</td>
									<td>".$bill->newbill_description."</td>
									<td>".$this->Xin_model->currency_sign($diagnose[0]->diagnose_total_sum)."</td>
								</tr>	

							";
							$total_bulk_bills[] = $diagnose[0]->diagnose_total_sum;
						}					
					}
				}
			}
		}



		$html .= '<tr>
					<td colspan="4" align="center"><b>Total</b></td>
					<td><b>'.$this->Xin_model->currency_sign(array_sum($total_bills)).'</b></td>
				</tr>
		';

		$html .= '</tbody>
		</table>';



		echo $html;
	}

	public function fetch_expense_id()
	{
		$from = $this->input->post('from');
		$to = $this->input->post('to');

		$expenses = $this->Xin_model->get_all_expense_transaction($from,$to);
		// echo $this->db->last_query();
		$html = '<table class="datatables-demo table table-striped" id="xin_table">
			<thead>
				<tr>
					<th>Account</th>
					<th>Category</th>
					<th>Payment Method</th>
					<th>Date</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody>';

			$total = array();

		if (!empty($expenses)) {
			// print_r($expenses);die;
			foreach($expenses as $expense)
			{
				$bank = $this->Finance_model->read_bankcash_information($expense->account_id);
				$method = $this->Xin_model->read_payment_method($expense->payment_method_id);
				$expense_type = $this->Expense_model->read_expense_type_information($expense->transaction_cat_id);

				if (!empty($expense_type)) {
					$expense_type = $expense_type[0]->name;
				}else{
					$expense_type = "-";
				}

				$html .= "
					<tr>
						<td>".$bank[0]->account_name."</td>
						<td>".$expense_type."</td>
						<td>".$method[0]->method_name."</td>
						<td>".$expense->transaction_date."</td>
						<td>".$this->Xin_model->currency_sign($expense->amount)."</td>
					</tr>

				";

				$total[] = $expense->amount;
			}
		}

		$html .= '<tr>
					<td colspan="4" align="center"><b>Total</b></td>
					<td><b>'.$this->Xin_model->currency_sign(array_sum($total)).'</b></td>
				</tr>
		';

		$html .= '</tbody>
		</table>';



		echo $html;
	}

	public function get_income_form()
	{
		$html = "
		<div class=\"row\">

              <div class=\"col-md-6\">

                <div class=\"form-group\">

                  <label for=\"award_type\">".$this->lang->line('xin_acc_account')."</label>

                  <select name=\"bank_cash_id\" id=\"select2-demo-6\" class=\"form-control\" data-plugin=\"select_hrm\" data-placeholder=\"".$this->lang->line('xin_acc_choose_account_type')."\">

                    <option value=\"\"></option>

                    <?php foreach($all_bank_cash as $bank_cash) {?>

                    <option value=\"<?php echo $bank_cash->bankcash_id;?>\"><?php echo $bank_cash->account_name;?></option>

                    <?php } ?>

                  </select>

                </div>

                <div class=\"row\">

                  <div class=\"col-md-6\">

                    <div class=\"form-group\">

                      <label for=\"month_year\"><?php echo $this->lang->line('xin_amount');?></label>

                      <input class=\"form-control\" name=\"amount\" type=\"text\" placeholder=\"<?php echo $this->lang->line('xin_amount');?>\">

                    </div>

                  </div>

                  <div class=\"col-md-6\">

                    <div class=\"form-group\">

                      <label for=\"deposit_date\"><?php echo $this->lang->line('xin_e_details_date');?></label>

                      <input class=\"form-control date\" placeholder=\"<?php echo date('Y-m-d');?>\" readonly name=\"deposit_date\" type=\"text\">

                    </div>

                  </div>

                </div>

                <div class=\"row\">

                  <?php if($user_info[0]->user_role_id==1 || in_array('314',$role_resources_ids)){ ?>

                  <div class=\"col-md-6\">

                    <?php if($user_info[0]->user_role_id==1){ ?>

                    <div class=\"form-group\">

                      <label for=\"department\"><?php echo $this->lang->line('module_company_title');?></label>

                      <select class=\"form-control\" name=\"company\" id=\"aj_company\" data-plugin=\"select_hrm\" data-placeholder=\"<?php echo $this->lang->line('module_company_title');?>\" required>

                        <option value=\"\"><?php echo $this->lang->line('module_company_title');?></option>
    
                        <?php foreach($all_companies as $company) {?>
                        <option value=\"<?php echo $company->company_id;?>\"> <?php echo $company->name;?></option>

                        <?php } ?>

                      </select>

                    </div>

                    <?php } else {?>

                    <?php $ecompany_id = $user_info[0]->company_id;?>

                    <div class=\"form-group\">

                      <label for=\"department\"><?php echo $this->lang->line('module_company_title');?></label>

                      <select class=\"form-control\" name=\"company\" id=\"aj_company\" data-plugin=\"select_hrm\" data-placeholder=\"<?php echo $this->lang->line('module_company_title');?>\" required>

                        <option value=\"\"><?php echo $this->lang->line('module_company_title');?></option>

                        <?php foreach($all_companies as $company) {?>

                        <?php if($ecompany_id == $company->company_id):?>

                        <option value=\"<?php echo $company->company_id;?>\"> <?php echo $company->name;?></option>

                        <?php endif;?>

                        <?php } ?>

                      </select>

                    </div>

                    <?php } ?>

                  </div>

                  <div class=\"col-md-6\">

                    <div class=\"form-group\" id=\"employee_ajax\">

                      <label for=\"department\"><?php echo $this->lang->line('xin_acc_payee');?></label>

                      <select id=\"payee_id\" name=\"payee_id\" class=\"form-control\" data-plugin=\"select_hrm\" data-placeholder=\"<?php echo $this->lang->line('xin_acc_choose_a_payee');?>\">

                        <option value=\"\"></option>

                      </select>

                    </div>

                  </div>

                  <?php } else {?>

                  <input type=\"hidden\" name=\"payee_id\" id=\"payee_id\" value=\"".$session['user_id']."\" />

                  <?php } ?>

                <!--   <div class=\"col-md-6\">

                    <div class=\"form-group\">

                      <label for=\"employee\"><?php echo $this->lang->line('xin_acc_category');?></label>

                      <select name=\"category_id\" id=\"select2-demo-6\" class=\"form-control category\" data-plugin=\"select_hrm\" data-placeholder=\"<?php echo $this->lang->line('xin_acc_choose_category');?>\">

                        <option value=\"\"></option>

                        <?php foreach($all_income_categories_list as $income_category) {?>

                        <option value=\"<?php echo $income_category->id;?>\"> <?php echo $income_category->name;?></option>

                        <?php } ?>

                      </select>

                    </div>

                  </div> -->

                <!-- <div class=\"col-md-6\">

                    <div class=\"form-group\">

                      <label for=\"employee\"><?php echo $this->lang->line('xin_acc_payer');?></label>

                      <select name=\"payer_id\" id=\"select2-demo-6\" class=\"form-control\" data-plugin=\"select_hrm\" data-placeholder=\"<?php echo $this->lang->line('xin_acc_choose_a_payer');?>\">

                        <option value=\"\"></option>

                        <?php foreach($all_payers as $payer) {?>

                        <option value=\"<?php echo $payer->payer_id;?>\"> <?php echo $payer->payer_name;?></option>

                        <?php } ?>

                      </select>

                    </div>

                </div> -->

                </div>

              </div>

              <div class=\"col-md-6\">

                <div class=\"form-group\">

                  <label for=\"description\"><?php echo $this->lang->line('xin_description');?></label>

                  <textarea class=\"form-control textarea\" placeholder=\"<?php echo $this->lang->line('xin_description');?>\" name=\"description\" cols=\"30\" rows=\"5\" id=\"description\"></textarea>

                </div>

                <div class='form-group'>

                  <fieldset class=\"form-group\">

                    <label for=\"logo\"><?php echo $this->lang->line('xin_acc_attach_file');?></label>

                    <input type=\"file\" class=\"form-control-file\" id=\"deposit_file\" name=\"deposit_file\">

                  </fieldset>

                </div>

              </div>

            </div>

            <div class=\"row\">

              <div class=\"col-md-3\">

                <div class=\"form-group\">

                  <label for=\"payment_method\"><?php echo $this->lang->line('xin_sub_deposit_type');?></label>

                  <select name=\"sub_deposit[]\" id=\"select2-demo-6\" class=\"form-control sub_deposit\" data-plugin=\"select_hrm\" data-placeholder=\"<?php echo $this->lang->line('xin_sub_deposit_type');?>\" multiple placeholder=\"Select Category\">

                    <option value=\"\"></option>

                    <?php foreach ($sub_deposit as $sub): ?>

                    <?php $deposit_type = $this->Finance_model->read_deposit_type($sub->deposit_id); ?>
                    
                    <option value=\"<?php echo $sub->id ;?>\"><?php 
                      if (!empty($deposit_type)) {
                        $deposit_type = $deposit_type[0]->account_code;
                      }else{
                        $deposit_type = \"-\";
                      }
                      echo $deposit_type.\" \".$sub->name ;
                    ?></option>
                      
                    <?php endforeach ?>

                  </select>

                </div>

              </div>

              <div class=\"col-md-3\">

                <div class=\"form-group\">

                  <label for=\"payment_method\"><?php echo $this->lang->line('xin_payment_method');?></label>

                  <select name=\"payment_method\" id=\"select2-demo-6\" class=\"form-control\" data-plugin=\"select_hrm\" data-placeholder=\"<?php echo $this->lang->line('xin_choose_payment_method');?>\">

                    <option value=\"\"></option>

                    <?php foreach($get_all_payment_method as $payment_method) {?>

                    <option value=\"<?php echo $payment_method->payment_method_id;?>\"> <?php echo $payment_method->method_name;?></option>

                    <?php } ?>

                  </select>

                </div>

              </div>

              <div class=\"col-md-3\">

                <div class=\"form-group\">

                  <label for=\"employee\"><?php echo $this->lang->line('xin_acc_ref_no');?></label>

                  <input class=\"form-control\" placeholder=\"<?php echo $this->lang->line('xin_acc_ref_example');?>\" name=\"deposit_reference\" type=\"text\">

                  <br />

                </div>

              </div>

            </div>

		";

		echo $html;
	}

} 

?>