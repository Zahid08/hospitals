<?php

/**

 * NOTICE OF LICENSE

 *

 * This source file is subject to the HRSALE License

 * that is bundled with this package in the file license.txt.

 * It is also available through the world-wide-web at this URL:

 * http://www.hrsale.com/license.txt

 * If you did not receive a copy of the license and are unable to

 * obtain it through the world-wide-web, please send an email

 * to hrsalesoft@gmail.com so we can send you a copy immediately.

 *

 * @author   HRSALE

 * @author-email  hrsalesoft@gmail.com

 * @copyright  Copyright © hrsale.com. All Rights Reserved

 */

defined('BASEPATH') OR exit('No direct script access allowed');



class Hospital extends MY_Controller {

	public $d_sum = 0;
    	public $s_sum = 0;

	 public function __construct() {

        parent::__construct();
		//load the model

		$this->load->model("Training_model");

		$this->load->model("Xin_model");

		$this->load->model("Trainers_model");

		$this->load->model("Designation_model");

		$this->load->model("Department_model");

		$this->load->model("Custom_fields_model");
		
		$this->load->model("Clients_model");

	}

	

	/*Function to set JSON output*/

	public function output($Return=array()){

		/*Set response header*/

		header("Access-Control-Allow-Origin: *");

		header("Content-Type: application/json; charset=UTF-8");

		/*Final JSON response*/

		exit(json_encode($Return));

	}

	

	public function index() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] =  'Hospital Profile | '.$this->Xin_model->site_title();

		$data['all_locations']  =  $this->Training_model->getAll2('xin_location',' 1 order by location_id desc');
		$data['all_bands']      =  $this->Training_model->getAll2('xin_bands',' 1 order by band_id desc');
		$data['all_hospital']   =  $this->Training_model->getAll2('xin_hospital',' 1 order by hospital_id desc');
		
		$data['breadcrumbs'] = 'Hospital Profiles';

		$data['path_url'] = 'training';
 

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('602',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/hospital_lists", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }

 



    public function change_hospital_requests() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		$edata = array(
			'is_notify' => 0,
		);
		$this->Xin_model->update_provider_change_record($edata);

		if(isset($_GET['approve']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];

			$data_to_update = array( 
				'hospital_id' => $hospital_id,  
			);

			$iresult = $this->Training_model->update2('xin_clients',' client_id='.$client_id.' ',$data_to_update);


			$data_to_update2 = array( 
				'status'      => 'approved',
			);

			$iresult2 = $this->Training_model->update2('xin_change_hospital_request',' change_hospital_request_id='.$id.' ',$data_to_update2);

			$this->session->set_flashdata('success','Provider Change Approved successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}


		if(isset($_GET['reject']))
		{ 
			$id           =  $_GET['id'];

			 

			$data_to_update2 = array( 
				'status'      => 'rejected',
			);

			$iresult2 = $this->Training_model->update2('xin_change_hospital_request',' change_hospital_request_id='.$id.' ',$data_to_update2);

			$this->session->set_flashdata('success','Provider Change Rejected successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}

		$data['title'] =  'Hospital Change Requests | '.$this->Xin_model->site_title();

		if (!is_null($this->input->post('to_date'))) {
			$from_date = $this->input->post('from_date');
			$to_date = $this->input->post('to_date');
			
			$data['all_requests']  =  $this->Training_model->getAll2("xin_change_hospital_request"," created_on BETWEEN '$from_date' AND '$to_date' order by change_hospital_request_id desc");
			// echo $this->db->last_query();die;
		
		}else{
		
			$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');

		}

		 
		$data['breadcrumbs'] = 'Hospital Change Requests';

		$data['path_url'] = 'training';
 

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('535',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/change_hospital_requests", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }

    public function approved_hospital_requests() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		$edata = array(
			'is_notify' => 0,
		);
		$this->Xin_model->update_provider_change_record($edata);

		if(isset($_GET['approve']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];

			$data_to_update = array( 
				'hospital_id' => $hospital_id,  
			);

			$iresult = $this->Training_model->update2('xin_clients',' client_id='.$client_id.' ',$data_to_update);


			$data_to_update2 = array( 
				'status'      => 'approved',
			);

			$iresult2 = $this->Training_model->update2('xin_change_hospital_request',' change_hospital_request_id='.$id.' ',$data_to_update2);

			$this->session->set_flashdata('success','Provider Change Approved successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}


		if(isset($_GET['reject']))
		{ 
			$id           =  $_GET['id'];

			 

			$data_to_update2 = array( 
				'status'      => 'rejected',
			);

			$iresult2 = $this->Training_model->update2('xin_change_hospital_request',' change_hospital_request_id='.$id.' ',$data_to_update2);

			$this->session->set_flashdata('success','Provider Change Rejected successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}

		$data['title'] =  'Approved Hospital Change Requests | '.$this->Xin_model->site_title();

		if (!is_null($this->input->post('to_date'))) {
			$from_date = $this->input->post('from_date');
			$to_date = $this->input->post('to_date');
			
			$data['all_requests']  =  $this->Training_model->getAll2("xin_change_hospital_request"," created_on BETWEEN '$from_date' AND '$to_date' order by change_hospital_request_id desc");
			// echo $this->db->last_query();die;
		
		}else{
		
			$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');

		}

		 
		$data['breadcrumbs'] = 'All Approved Hospital Change Requests';

		$data['path_url'] = 'training';
 

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('643',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/approved_hospital_requests", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }

    public function rejected_hospital_requests() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		$edata = array(
			'is_notify' => 0,
		);
		$this->Xin_model->update_provider_change_record($edata);

		if(isset($_GET['approve']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];

			$data_to_update = array( 
				'hospital_id' => $hospital_id,  
			);

			$iresult = $this->Training_model->update2('xin_clients',' client_id='.$client_id.' ',$data_to_update);


			$data_to_update2 = array( 
				'status'      => 'approved',
			);

			$iresult2 = $this->Training_model->update2('xin_change_hospital_request',' change_hospital_request_id='.$id.' ',$data_to_update2);

			$this->session->set_flashdata('success','Provider Change Approved successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}


		if(isset($_GET['reject']))
		{ 
			$id           =  $_GET['id'];

			 

			$data_to_update2 = array( 
				'status'      => 'rejected',
			);

			$iresult2 = $this->Training_model->update2('xin_change_hospital_request',' change_hospital_request_id='.$id.' ',$data_to_update2);

			$this->session->set_flashdata('success','Provider Change Rejected successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}

		$data['title'] =  'All Rejected Hospital Change Requests | '.$this->Xin_model->site_title();

		if (!is_null($this->input->post('to_date'))) {
			$from_date = $this->input->post('from_date');
			$to_date = $this->input->post('to_date');
			
			$data['all_requests']  =  $this->Training_model->getAll2("xin_change_hospital_request"," created_on BETWEEN '$from_date' AND '$to_date' order by change_hospital_request_id desc");
			// echo $this->db->last_query();die;
		
		}else{
		
			$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');

		}

		 
		$data['breadcrumbs'] = 'All Rejected Hospital Change Requests';

		$data['path_url'] = 'training';
 

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('644',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/rejected_hospital_requests", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }

 public function admission_request() 
    {	
    		
		
		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);
		//print_r($system); //die();
		
		$role = 517;

		$check_roles = $this->check_roles($session['user_id'],$role);
		
		$role2 = 518;
		$check_roles2 = $this->check_roles($session['user_id'],$role2);

		if($system[0]->module_training!='true' OR $check_roles !=true OR $check_roles2 != true){

			redirect('admin/dashboard');

		}

		if($this->input->post('that_reason')) {
			$reason = $this->input->post('that_reason');
			$did = $this->input->post('did');
			$hid = $this->input->post('hid');
			$cid = $this->input->post('cid');

			$iresult = $this->Clients_model->diagnose_reject_reason('3',$reason,$did);
			//print_r($iresult); die();
			
				// load email library
				$this->load->library('email');
				$this->email->set_mailtype("html");

				//get company info
				$cinfo = $this->Xin_model->read_company_setting_info(1);
				$company = $this->Xin_model->read_company_setting_info(1);

				//get email template
				// $template = $this->Xin_model->read_email_template(8);
				// $subject = $template[0]->subject.' - '.$cinfo[0]->company_name;
				$subject = "Authorization Code";
				$logo = base_url().'uploads/logo/signin/'.$company[0]->sign_in_logo;

				// get user full name
				$full_name = "Kennedy Job";
				$message = '<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">Sample email for rejecting authorization code request.</div>';

				$this->email->from($cinfo[0]->email, $cinfo[0]->company_name);
				$this->email->to('kennedyjob9999@gmail.com');
				$this->email->subject($subject);
				$this->email->message($message);

				$this->email->send();

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
			// die;
		}

		if(isset($_GET['approve']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];

			$last_code = $this->Clients_model->get_last_auth_code();
			$code = $last_code + 1;
			// $code = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT);
			// $data_to_update = array( 
			// 	'hospital_id' => $hospital_id,  
			// );

			$iresult = $this->Clients_model->update_diagnose_status_record_admission('1',$code,$id);
			//print_r($iresult); die();
				// load email library
				$this->load->library('email');
				$this->email->set_mailtype("html");

				//get company info
				$cinfo = $this->Xin_model->read_company_setting_info(1);
				$company = $this->Xin_model->read_company_setting_info(1);

				//get email template
				// $template = $this->Xin_model->read_email_template(8);
				// $subject = $template[0]->subject.' - '.$cinfo[0]->company_name;
				$subject = "Admission Request";
				$logo = base_url().'uploads/logo/signin/'.$company[0]->sign_in_logo;

				// get user full name
				$full_name = "Kennedy Job";
				$message = '<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">Sample email for authorization code is generated successfully.</div>';

				$this->email->from($cinfo[0]->email, $cinfo[0]->company_name);
				$this->email->to('kennedyjob9999@gmail.com');
				$this->email->subject($subject);
				$this->email->message($message);
				
				$this->email->send();

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}


		// if(isset($_GET['reject']))
		// { 
		// 	$id = $_GET['id'];
		// 	$code = "";

		// 	$iresult = $this->Clients_model->update_diagnose_status_record('3',$code,$id);
		// 	// $data_to_update2 = array( 
		// 	// 	'status'      => 'rejected',
		// 	// );

		// 	// $iresult2 = $this->Training_model->update2('xin_change_hospital_request',' change_hospital_request_id='.$id.' ',$data_to_update2);

		// 	$this->session->set_flashdata('success','Data updated successfully.');
		// 	redirect($_SERVER['HTTP_REFERER']);
		// }

		$data['title'] =  'Admission Requests | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');
		// print_r($data['all_requests']); //die();
		$data['breadcrumbs'] = 'Admission Requests';

		$data['path_url'] = 'training';
 		$nothing = "";

 		$edata = array(
			'is_notify_1' => 0,
		);
		$this->Xin_model->update_diagnose_record($edata);

		if($this->input->post('from_date')) {
			$from = $this->input->post('from_date');
			$to = $this->input->post('to_date');
			$data['xin_diagnose_clients'] = $this->Clients_model->get_filter_result_hospital_admission($from,$to);
			$data['query'] = $this->db->last_query();
		} else {
			$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients_hospital_adminssion($nothing,'');
			$data['query'] = $this->db->last_query();
			
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('517',$role_resources_ids)) {

			if(!empty($session)){ 
				//print_r($data); die();

				$data['subview'] = $this->load->view("admin/hospitals/admission_request", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }
	public function admission_case() 
    {	
    		
		
		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);
		//print_r($system); //die();
		
		$role = 517;

		$check_roles = $this->check_roles($session['user_id'],$role);
		
		$role2 = 518;
		$check_roles2 = $this->check_roles($session['user_id'],$role2);

		if($system[0]->module_training!='true' OR $check_roles !=true OR $check_roles2 != true){

			redirect('admin/dashboard');

		}

		if($this->input->post('that_reason')) {
			$reason = $this->input->post('that_reason');
			$did = $this->input->post('did');
			$hid = $this->input->post('hid');
			$cid = $this->input->post('cid');

			$iresult = $this->Clients_model->diagnose_reject_reason('3',$reason,$did);
			//print_r($iresult); die();
			
				// load email library
				$this->load->library('email');
				$this->email->set_mailtype("html");

				//get company info
				$cinfo = $this->Xin_model->read_company_setting_info(1);
				$company = $this->Xin_model->read_company_setting_info(1);

				//get email template
				// $template = $this->Xin_model->read_email_template(8);
				// $subject = $template[0]->subject.' - '.$cinfo[0]->company_name;
				$subject = "Authorization Code";
				$logo = base_url().'uploads/logo/signin/'.$company[0]->sign_in_logo;

				// get user full name
				$full_name = "Kennedy Job";
				$message = '<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">Sample email for rejecting authorization code request.</div>';

				$this->email->from($cinfo[0]->email, $cinfo[0]->company_name);
				$this->email->to('kennedyjob9999@gmail.com');
				$this->email->subject($subject);
				$this->email->message($message);

				$this->email->send();

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
			// die;
		}

		if(isset($_GET['approve']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];

			$last_code = $this->Clients_model->get_last_auth_code();
			$code = $last_code + 1;
			// $code = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT);
			// $data_to_update = array( 
			// 	'hospital_id' => $hospital_id,  
			// );

			$iresult = $this->Clients_model->update_diagnose_status_record_admission('1',$code,$id);
			//print_r($iresult); die();
				// load email library
				$this->load->library('email');
				$this->email->set_mailtype("html");

				//get company info
				$cinfo = $this->Xin_model->read_company_setting_info(1);
				$company = $this->Xin_model->read_company_setting_info(1);

				//get email template
				// $template = $this->Xin_model->read_email_template(8);
				// $subject = $template[0]->subject.' - '.$cinfo[0]->company_name;
				$subject = "Authorization Code";
				$logo = base_url().'uploads/logo/signin/'.$company[0]->sign_in_logo;

				// get user full name
				$full_name = "Kennedy Job";
				$message = '<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">Sample email for authorization code is generated successfully.</div>';

				$this->email->from($cinfo[0]->email, $cinfo[0]->company_name);
				$this->email->to('kennedyjob9999@gmail.com');
				$this->email->subject($subject);
				$this->email->message($message);
				
				$this->email->send();

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}


		// if(isset($_GET['reject']))
		// { 
		// 	$id = $_GET['id'];
		// 	$code = "";

		// 	$iresult = $this->Clients_model->update_diagnose_status_record('3',$code,$id);
		// 	// $data_to_update2 = array( 
		// 	// 	'status'      => 'rejected',
		// 	// );

		// 	// $iresult2 = $this->Training_model->update2('xin_change_hospital_request',' change_hospital_request_id='.$id.' ',$data_to_update2);

		// 	$this->session->set_flashdata('success','Data updated successfully.');
		// 	redirect($_SERVER['HTTP_REFERER']);
		// }

		$data['title'] =  'Admission Cases | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');
		// print_r($data['all_requests']); //die();
		$data['breadcrumbs'] = 'Admission Cases';

		$data['path_url'] = 'training';
 		$nothing = "";

 		$edata = array(
			'is_notify_1' => 0,
		);
		$this->Xin_model->update_diagnose_record($edata);

		if($this->input->post('from_date')) {
			$from = $this->input->post('from_date');
			$to = $this->input->post('to_date');
			$data['xin_diagnose_clients'] = $this->Clients_model->get_filter_result_hospital_admission_case($from,$to);
			$data['query'] = $this->db->last_query();
		} else {
			$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients_hospital_adminssion_case($nothing,'');
			$data['query'] = $this->db->last_query();
			
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('517',$role_resources_ids)) {

			if(!empty($session)){ 
				//print_r($data); die();

				$data['subview'] = $this->load->view("admin/hospitals/admission_case", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }
    public function diagnose_hospital_clients_requests() 
    {	
    		
		
		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);
		//print_r($system); //die();
		
		$role = 517;

		$check_roles = $this->check_roles($session['user_id'],$role);
		
		$role2 = 518;
		$check_roles2 = $this->check_roles($session['user_id'],$role2);

		if($system[0]->module_training!='true' OR $check_roles !=true OR $check_roles2 != true){

			redirect('admin/dashboard');

		}

		if($this->input->post('that_reason')) {
			$reason = $this->input->post('that_reason');
			$did = $this->input->post('did');
			$hid = $this->input->post('hid');
			$cid = $this->input->post('cid');

			$iresult = $this->Clients_model->diagnose_reject_reason('3',$reason,$did);
			//print_r($iresult); die();
			
				// load email library
				$this->load->library('email');
				$this->email->set_mailtype("html");

				//get company info
				$cinfo = $this->Xin_model->read_company_setting_info(1);
				$company = $this->Xin_model->read_company_setting_info(1);

				//get email template
				// $template = $this->Xin_model->read_email_template(8);
				// $subject = $template[0]->subject.' - '.$cinfo[0]->company_name;
				$subject = "Authorization Code";
				$logo = base_url().'uploads/logo/signin/'.$company[0]->sign_in_logo;

				// get user full name
				$full_name = "Kennedy Job";
				$message = '<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">Sample email for rejecting authorization code request.</div>';

				$this->email->from($cinfo[0]->email, $cinfo[0]->company_name);
				$this->email->to('kennedyjob9999@gmail.com');
				$this->email->subject($subject);
				$this->email->message($message);

				$this->email->send();

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
			// die;
		}

		if(isset($_GET['approve']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];

			$last_code = $this->Clients_model->get_last_auth_code();
			$code = $last_code + 1;
			// $code = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT);
			// $data_to_update = array( 
			// 	'hospital_id' => $hospital_id,  
			// );

			$iresult = $this->Clients_model->update_diagnose_status_record('2',$code,$id);
			//print_r($iresult); die();
				// load email library
				$this->load->library('email');
				$this->email->set_mailtype("html");

				//get company info
				$cinfo = $this->Xin_model->read_company_setting_info(1);
				$company = $this->Xin_model->read_company_setting_info(1);

				//get email template
				// $template = $this->Xin_model->read_email_template(8);
				// $subject = $template[0]->subject.' - '.$cinfo[0]->company_name;
				$subject = "Authorization Code";
				$logo = base_url().'uploads/logo/signin/'.$company[0]->sign_in_logo;

				// get user full name
				$full_name = "Kennedy Job";
				$message = '<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">Sample email for authorization code is generated successfully.</div>';

				$this->email->from($cinfo[0]->email, $cinfo[0]->company_name);
				$this->email->to('kennedyjob9999@gmail.com');
				$this->email->subject($subject);
				$this->email->message($message);
				
				$this->email->send();

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}


		// if(isset($_GET['reject']))
		// { 
		// 	$id = $_GET['id'];
		// 	$code = "";

		// 	$iresult = $this->Clients_model->update_diagnose_status_record('3',$code,$id);
		// 	// $data_to_update2 = array( 
		// 	// 	'status'      => 'rejected',
		// 	// );

		// 	// $iresult2 = $this->Training_model->update2('xin_change_hospital_request',' change_hospital_request_id='.$id.' ',$data_to_update2);

		// 	$this->session->set_flashdata('success','Data updated successfully.');
		// 	redirect($_SERVER['HTTP_REFERER']);
		// }

		$data['title'] =  'Authorization Code Requests | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');
		// print_r($data['all_requests']); //die();
		$data['breadcrumbs'] = 'Authorization Code Requests';

		$data['path_url'] = 'training';
 		$nothing = "";

 		$edata = array(
			'is_notify_1' => 0,
		);
		$this->Xin_model->update_diagnose_record($edata);

		if($this->input->post('from_date')) {
			$from = $this->input->post('from_date');
			$to = $this->input->post('to_date');
			$data['xin_diagnose_clients'] = $this->Clients_model->get_filter_result_hospital($from,$to);
			$data['query'] = $this->db->last_query();
		} else {
			$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients_hospital($nothing,'');
			$data['query'] = $this->db->last_query();
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('517',$role_resources_ids)) {

			if(!empty($session)){ 
				//print_r($data); die();

				$data['subview'] = $this->load->view("admin/hospitals/diagnose_hospital_clients_requests", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }
    
    public function drug_notes() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$this->form_validation->set_rules('that_drugreason', 'Notes', 'trim|required');
		if ($this->form_validation->run() == true) {
		     $main_id = $this->input->post('drug_mid');		     
		     $insertData['drug_id'] = $this->input->post('drug_cid');
                     $insertData['hospital_id'] = $this->input->post('drug_hid');
                     $insertData['client_id'] = $this->input->post('drug_cid');
                     $insertData['notes'] = $this->input->post('that_drugreason');
                     $this->Clients_model->insert_drug_notes($insertData);
                     $this->Clients_model->modify_drug_status($insertData['drug_id']);
                     $total_bill = $this->bill_value($main_id); 
                     $updated = $this->Clients_model->update_bill($total_bill,$main_id); 
					if($this->input->post('redirectflage') ==2)   {          
					redirect('admin/Hospital/all_auth_approved'); 
					}
					else{
						redirect('admin/Hospital/diagnose_hospital_clients_requests'); 
					}
		}
		else{
		    $this->session->set_flashdata('message', 'Reason was not added successfully');
			if($this->input->post('redirectflage') ==2)   {          
					redirect('admin/Hospital/all_auth_approved'); 
					}else{
                    redirect('admin/Hospital/diagnose_hospital_clients_requests'); 
					}					
		}
		
    }
    
    public function bill_value($main_id){

    	$d_sum = $this->Clients_model->drugs_sum($main_id);
		
    	$dr_sum = $d_sum[0]->d_id;
    	$s_sum = $this->Clients_model->service_sum($main_id);
    	$ser_sum = $s_sum[0]->s_id;
    	return $total_bill = $dr_sum + $ser_sum;	
    }
	public function fetch_comment(){
		$diagonal_ids = $this->input->post('id');
		$output = $this->Clients_model->getcomment($diagonal_ids);
		return $output;
	}
    public function admissioncomment()
	{
		 $diagonal_ids = $this->input->post('diagonal_ids');
		 $fromdate = $this->input->post('fromdate');
		 $todate = $this->input->post('todate');
		 $comment = $this->input->post('comment');
		 $arr['fromdate'] = $fromdate;
		 $arr['todate'] = $todate;
		 $arr['comment'] = $comment;
		 $arr['diagnose_ids'] = $diagonal_ids;
		 $this->Clients_model->admissioncomment($arr);
		 redirect('admin/Hospital/admission_case'); 
		
	}
    public function service_notes() 
    {

		$session = $this->session->userdata('username');

		if(empty($session))
		{  
			redirect('admin/'); 
		}
		
		$this->form_validation->set_rules('that_service_reason', 'Notes', 'trim|required');
		if ($this->form_validation->run() == true) {
		     $main_id = $this->input->post('service_mid');
		     $insertData['service_id'] = $this->input->post('service_did'); 
                     $insertData['hospital_id'] = $this->input->post('service_hid');
                     $insertData['client_id'] = $this->input->post('service_mid');
                     $insertData['notes'] = $this->input->post('that_service_reason');
                     $this->Clients_model->insert_service_notes($insertData);
                     $this->Clients_model->modify_service_status($insertData['service_id']);
                     $total_bill = $this->bill_value($main_id); 
                     $updated = $this->Clients_model->update_bill($total_bill,$main_id); 
if($this->input->post('service_flage') == 2)
{			
redirect('admin/Hospital/all_auth_approved'); 		  
}else{
	redirect('admin/Hospital/diagnose_hospital_clients_requests'); 
}                
                     
		}
		else{
		    $this->session->set_flashdata('message', 'Reason has not added successfully');
                  
if($this->input->post('service_flage') == 2)
{			
redirect('admin/Hospital/all_auth_approved'); 		  
}else{				  redirect('admin/Hospital/diagnose_hospital_clients_requests');} 		
		}
		
    }    		    

    public function diagnose_hospital_clients_requests_rejected() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		$role = 521;

		$check_roles = $this->check_roles($session['user_id'],$role);

		if($system[0]->module_training!='true' OR $check_roles != true){

			redirect('admin/dashboard');

		}

		if($this->input->post('that_reason')) {
			$reason = $this->input->post('that_reason');
			$did = $this->input->post('did');
			$hid = $this->input->post('hid');
			$cid = $this->input->post('cid');

			$iresult = $this->Clients_model->diagnose_reject_reason('3',$reason,$did);
			
				// load email library
				$this->load->library('email');
				$this->email->set_mailtype("html");

				//get company info
				$cinfo = $this->Xin_model->read_company_setting_info(1);
				$company = $this->Xin_model->read_company_setting_info(1);

				//get email template
				// $template = $this->Xin_model->read_email_template(8);
				// $subject = $template[0]->subject.' - '.$cinfo[0]->company_name;
				$subject = "Authorization Code";
				$logo = base_url().'uploads/logo/signin/'.$company[0]->sign_in_logo;

				// get user full name
				$full_name = "Kennedy Job";
				$message = '<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">Sample email for rejecting authorization code request.</div>';

				$this->email->from($cinfo[0]->email, $cinfo[0]->company_name);
				$this->email->to('kennedyjob9999@gmail.com');
				$this->email->subject($subject);
				$this->email->message($message);

				$this->email->send();

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
			// die;
		}

		if(isset($_GET['approve']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];

			$last_code = $this->Clients_model->get_last_auth_code();
			$code = $last_code + 1;
			// $code = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT);
			// $data_to_update = array( 
			// 	'hospital_id' => $hospital_id,  
			// );

			$iresult = $this->Clients_model->update_diagnose_status_record('2',$code,$id);
			
				// load email library
				$this->load->library('email');
				$this->email->set_mailtype("html");

				//get company info
				$cinfo = $this->Xin_model->read_company_setting_info(1);
				$company = $this->Xin_model->read_company_setting_info(1);

				//get email template
				// $template = $this->Xin_model->read_email_template(8);
				// $subject = $template[0]->subject.' - '.$cinfo[0]->company_name;
				$subject = "Authorization Code";
				$logo = base_url().'uploads/logo/signin/'.$company[0]->sign_in_logo;

				// get user full name
				$full_name = "Kennedy Job";
				$message = '<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">Sample email for authorization code is generated successfully.</div>';

				$this->email->from($cinfo[0]->email, $cinfo[0]->company_name);
				$this->email->to('kennedyjob9999@gmail.com');
				$this->email->subject($subject);
				$this->email->message($message);
				
				$this->email->send();

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}


		// if(isset($_GET['reject']))
		// { 
		// 	$id = $_GET['id'];
		// 	$code = "";

		// 	$iresult = $this->Clients_model->update_diagnose_status_record('3',$code,$id);
		// 	// $data_to_update2 = array( 
		// 	// 	'status'      => 'rejected',
		// 	// );

		// 	// $iresult2 = $this->Training_model->update2('xin_change_hospital_request',' change_hospital_request_id='.$id.' ',$data_to_update2);

		// 	$this->session->set_flashdata('success','Data updated successfully.');
		// 	redirect($_SERVER['HTTP_REFERER']);
		// }

		$data['title'] =  'Rejected Authorization Code Requests | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');
		 
		$data['breadcrumbs'] = 'Rejected Authorization Code Requests';

		$data['path_url'] = 'training';
 		$nothing = "";

		if($this->input->post('from_date')) {
			$from = $this->input->post('from_date');
			$to = $this->input->post('to_date');
			$data['xin_diagnose_clients'] = $this->Clients_model->get_filter_result_hospital($from,$to);
			$data['query'] = $this->db->last_query();
		} else {
			$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients_hospital($nothing,'');
			$data['query'] = $this->db->last_query();
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('521',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/diagnose_hospital_clients_requests_rejected", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }

    public function diagnose_hospital_clients_requests_admin() 
    {

    	// echo "asd";die;

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		$role = 523;

		$check_roles = $this->check_roles($session['user_id'],$role);

		if($system[0]->module_training!='true' OR $check_roles != true){

			redirect('admin/dashboard');

		}

		if($this->input->post('that_reason')) {
			$reason = $this->input->post('that_reason');
			$did = $this->input->post('did');
			$hid = $this->input->post('hid');
			$cid = $this->input->post('cid');

			$iresult = $this->Clients_model->diagnose_reject_reason('3',$reason,$did);

				// load email library
				$this->load->library('email');
				$this->email->set_mailtype("html");

				//get company info
				$cinfo = $this->Xin_model->read_company_setting_info(1);
				$company = $this->Xin_model->read_company_setting_info(1);

				//get email template
				// $template = $this->Xin_model->read_email_template(8);
				// $subject = $template[0]->subject.' - '.$cinfo[0]->company_name;
				$subject = "Authorization Code";
				$logo = base_url().'uploads/logo/signin/'.$company[0]->sign_in_logo;

				// get user full name
				$full_name = "Kennedy Job";
				$message = '<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">Sample email for rejecting authorization code request.</div>';

				$this->email->from($cinfo[0]->email, $cinfo[0]->company_name);
				$this->email->to('kennedyjob9999@gmail.com');
				$this->email->subject($subject);
				$this->email->message($message);

				$this->email->send();

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
			// die;
		}

		if(isset($_GET['approve']))
		{
			// echo "asd";die;
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];

			$code = "";

			// echo $client_id;
			// echo "\n";
			// echo $hospital_id;
			// echo "\n";
			// echo $id;
			// die;

			$iresult = $this->Clients_model->update_diagnose_status_record('1',$code,$id);
			
			$last_code = $this->Clients_model->get_last_auth_code();
			
			$code = $last_code + 1;
			// $code = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT);
			// $data_to_update = array( 
			// 	'hospital_id' => $hospital_id,  
			// );


			$iresult2 = $this->Clients_model->update_diagnose_status_record('2',$code,$id);

				// load email library
				$this->load->library('email');
				$this->email->set_mailtype("html");

				//get company info
				$cinfo = $this->Xin_model->read_company_setting_info(1);
				$company = $this->Xin_model->read_company_setting_info(1);

				//get email template
				// $template = $this->Xin_model->read_email_template(8);
				// $subject = $template[0]->subject.' - '.$cinfo[0]->company_name;
				$subject = "Authorization Code";
				$logo = base_url().'uploads/logo/signin/'.$company[0]->sign_in_logo;

				// get user full name
				$full_name = "Kennedy Job";
				$message = '<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">Sample email for authorization code is generated successfully.</div>';

				$this->email->from($cinfo[0]->email, $cinfo[0]->company_name);
				$this->email->to('kennedyjob9999@gmail.com');
				$this->email->subject($subject);
				$this->email->message($message);
				
				$this->email->send();

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}


		// if(isset($_GET['reject']))
		// { 
		// 	$id = $_GET['id'];
		// 	$code = "";

		// 	$iresult = $this->Clients_model->update_diagnose_status_record('3',$code,$id);
		// 	// $data_to_update2 = array( 
		// 	// 	'status'      => 'rejected',
		// 	// );

		// 	// $iresult2 = $this->Training_model->update2('xin_change_hospital_request',' change_hospital_request_id='.$id.' ',$data_to_update2);

		// 	$this->session->set_flashdata('success','Data updated successfully.');
		// 	redirect($_SERVER['HTTP_REFERER']);
		// }

		$data['title'] =  'Backend Authorization Code | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');
		 
		$data['breadcrumbs'] = 'Backend Authorization Code';

		$data['path_url'] = 'training';
 		$nothing = "";

		if($this->input->post('from_date')) {
			$from = $this->input->post('from_date');
			$to = $this->input->post('to_date');
			$data['xin_diagnose_clients'] = $this->Clients_model->get_filter_result_admin($from,$to);
			$data['query'] = $this->db->last_query();
		} else {
			$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients_admin($nothing,'');
			$data['query'] = $this->db->last_query();
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('523',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/diagnose_hospital_clients_request_admin", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }


    public function diagnose_hospital_clients_bill_requests() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		if(isset($_GET['approve']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];
			// $data_to_update = array( 
			// 	'hospital_id' => $hospital_id,  
			// );

			$iresult = $this->Clients_model->update_diagnose_bill_status_record('2',$id);

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}

		if(isset($_GET['second_approve']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];


			$iresult = $this->Clients_model->update_diagnose_bill_status_record('3',$id);

				// load email library
				$this->load->library('email');
				$this->email->set_mailtype("html");

				//get company info
				$cinfo = $this->Xin_model->read_company_setting_info(1);
				$company = $this->Xin_model->read_company_setting_info(1);

				//get email template
				// $template = $this->Xin_model->read_email_template(8);
				// $subject = $template[0]->subject.' - '.$cinfo[0]->company_name;
				$subject = "Hospital Bill Approved";
				$logo = base_url().'uploads/logo/signin/'.$company[0]->sign_in_logo;

				// get user full name
				$full_name = "Kennedy Job";
				$message = '<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">Sample email for hospital bill approval.</div>';

				$this->email->from($cinfo[0]->email, $cinfo[0]->company_name);
				$this->email->to('kennedyjob9999@gmail.com');
				$this->email->subject($subject);
				$this->email->message($message);
				
				$this->email->send();

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}

		if($this->input->post("that_reason"))
		{ 
			$reason = $this->input->post('that_reason');
			$did = $this->input->post('bdid');
			$hid = $this->input->post('bhid');
			$cid = $this->input->post('bcid');

			// echo "idfsdf : ".$did." ".$reason; die;
			$iresult = $this->Clients_model->diagnose_reject_reason('4',$reason,$did);

				// load email library
				$this->load->library('email');
				$this->email->set_mailtype("html");

				//get company info
				$cinfo = $this->Xin_model->read_company_setting_info(1);
				$company = $this->Xin_model->read_company_setting_info(1);

				//get email template
				// $template = $this->Xin_model->read_email_template(8);
				// $subject = $template[0]->subject.' - '.$cinfo[0]->company_name;
				$subject = "Hospital Bill Rejected";
				$logo = base_url().'uploads/logo/signin/'.$company[0]->sign_in_logo;

				// get user full name
				$full_name = "Kennedy Job";
				$message = '<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">Sample email for hospital bill rejected.</div>';

				$this->email->from($cinfo[0]->email, $cinfo[0]->company_name);
				$this->email->to('kennedyjob9999@gmail.com');
				$this->email->subject($subject);
				$this->email->message($message);
				
				$this->email->send();

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);

		}

		if($this->input->post("that_note"))
		{ 
			$note = $this->input->post('that_note');
			$did = $this->input->post('nbdid');
			$hid = $this->input->post('nbhid');
			$cid = $this->input->post('nbcid');

			// echo "idfsdf : ".$did." ".$reason; die;
			$iresult = $this->Clients_model->diagnose_reject_reason('0',$note,$did);

			$this->session->set_flashdata('success','Note Added Successfully.');
			redirect($_SERVER['HTTP_REFERER']);

		}

		$edata = array(
			'is_notify_2' => 0,
		);
		$this->Xin_model->update_diagnose_bill_record($edata);

		$data['title'] =  'Providers Bill Requests | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');
		 
		$data['breadcrumbs'] = 'Providers Bill Requests';

		$data['path_url'] = 'training';
 		$nothing = "";

		if($this->input->post('from_date')) {
			$from = $this->input->post('from_date');
			$to = $this->input->post('to_date');
			$data['xin_diagnose_clients'] = $this->Clients_model->get_filter_result($from,$to);
			$data['query'] = $this->db->last_query();
		} else {
			$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients($nothing,'');
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('527',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/diagnose_hospital_clients_bill_requests", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }

    public function diagnose_hospital_clients_bill_requests_rejected() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		if(isset($_GET['approve']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];
			// $data_to_update = array( 
			// 	'hospital_id' => $hospital_id,  
			// );

			$iresult = $this->Clients_model->update_diagnose_bill_status_record('2',$id);

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}

		if(isset($_GET['second_approve']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];


			$iresult = $this->Clients_model->update_diagnose_bill_status_record('3',$id);

				// load email library
				$this->load->library('email');
				$this->email->set_mailtype("html");

				//get company info
				$cinfo = $this->Xin_model->read_company_setting_info(1);
				$company = $this->Xin_model->read_company_setting_info(1);

				//get email template
				// $template = $this->Xin_model->read_email_template(8);
				// $subject = $template[0]->subject.' - '.$cinfo[0]->company_name;
				$subject = "Hospital Bill Approved";
				$logo = base_url().'uploads/logo/signin/'.$company[0]->sign_in_logo;

				// get user full name
				$full_name = "Kennedy Job";
				$message = '<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">Sample email for hospital bill approval.</div>';

				$this->email->from($cinfo[0]->email, $cinfo[0]->company_name);
				$this->email->to('kennedyjob9999@gmail.com');
				$this->email->subject($subject);
				$this->email->message($message);
				
				$this->email->send();

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}

		if($this->input->post("that_reason"))
		{ 
			$reason = $this->input->post('that_reason');
			$did = $this->input->post('bdid');
			$hid = $this->input->post('bhid');
			$cid = $this->input->post('bcid');

			// echo "idfsdf : ".$did." ".$reason; die;
			$iresult = $this->Clients_model->diagnose_reject_reason('4',$reason,$did);

				// load email library
				$this->load->library('email');
				$this->email->set_mailtype("html");

				//get company info
				$cinfo = $this->Xin_model->read_company_setting_info(1);
				$company = $this->Xin_model->read_company_setting_info(1);

				//get email template
				// $template = $this->Xin_model->read_email_template(8);
				// $subject = $template[0]->subject.' - '.$cinfo[0]->company_name;
				$subject = "Hospital Bill Rejected";
				$logo = base_url().'uploads/logo/signin/'.$company[0]->sign_in_logo;

				// get user full name
				$full_name = "Kennedy Job";
				$message = '<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">Sample email for hospital bill rejected.</div>';

				$this->email->from($cinfo[0]->email, $cinfo[0]->company_name);
				$this->email->to('kennedyjob9999@gmail.com');
				$this->email->subject($subject);
				$this->email->message($message);
				
				$this->email->send();

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);

		}

		if($this->input->post("that_note"))
		{ 
			$note = $this->input->post('that_note');
			$did = $this->input->post('nbdid');
			$hid = $this->input->post('nbhid');
			$cid = $this->input->post('nbcid');

			// echo "idfsdf : ".$did." ".$reason; die;
			$iresult = $this->Clients_model->diagnose_reject_reason('0',$note,$did);

			$this->session->set_flashdata('success','Note Added Successfully.');
			redirect($_SERVER['HTTP_REFERER']);

		}

		$data['title'] =  'All Providers Bills Rejected | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');
		 
		$data['breadcrumbs'] = 'All Providers Bills Rejected';

		$data['path_url'] = 'training';
 		$nothing = "";

		if($this->input->post('from_date')) {
			$from = $this->input->post('from_date');
			$to = $this->input->post('to_date');
			$data['xin_diagnose_clients'] = $this->Clients_model->get_filter_result($from,$to);
			$data['query'] = $this->db->last_query();
		} else {
			$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients($nothing,'');
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('531',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/diagnose_hospital_clients_bill_requests_rejected", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }


    public function all_bills_paid() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] =  'All Provider Bills Approved | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');
		 
		$data['breadcrumbs'] = 'All Providers Bills Approved';

		$data['path_url'] = 'training';
 		$nothing = "";
		if($this->input->post('from_date')) {
			$from = $this->input->post('from_date');
			$to = $this->input->post('to_date');
			$data['xin_diagnose_clients'] = $this->Clients_model->get_filter_result($from,$to);
			$data['query'] = $this->db->last_query();
		} else {
			$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients($nothing,'');
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('532',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/all_bills_paid", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }

    public function adjusted_requests() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		$role = 638;

		$check_roles = $this->check_roles($session['user_id'],$role);

		if($system[0]->module_training!='true' OR $check_roles != true){

			redirect('admin/dashboard');

		}

		$data['title'] =  'Adjusted Authorization Code Requests | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' is_adjusted = "1" ');
		//print_r($data['all_requests']); die();
		$data['breadcrumbs'] = 'Adjusted Authorization Code Requests';

		$data['path_url'] = 'training';
 		$nothing = "";

 		$edata = array(
			'is_notify_1' => 0,
		);
		$this->Xin_model->update_diagnose_record($edata);

		
 		$nothing = "";
		if($this->input->post('from_date')) {
			$from = $this->input->post('from_date');
			$to = $this->input->post('to_date');
			$data['xin_diagnose_clients'] = $this->Clients_model->get_adjusted_filter_result($from,$to);
			$data['query'] = $this->db->last_query();
		} else {
			$data['xin_diagnose_clients'] = $this->Clients_model->read_adjusted_individual_hospital_diagnose_clients($nothing,'');
			//print_r($data['xin_diagnose_clients']); //die();
		}
		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('638',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/adjusted_requests", $data, TRUE);
	
				$this->load->view('admin/layout/layout_main', $data); //page load
			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    	}    

    public function all_auth_approved() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		$role = 524;

		$check_roles = $this->check_roles($session['user_id'],$role);

		if($system[0]->module_training!='true' OR $check_roles != true){

			redirect('admin/dashboard');

		}

		$data['title'] =  'Approved Authorization Codes | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');
		 
		$data['breadcrumbs'] = 'Approved Authorization Codes';

		$data['path_url'] = 'training';
 		$nothing = "";
		if($this->input->post('from_date')) {
			$from = $this->input->post('from_date');
			$to = $this->input->post('to_date');
			$data['xin_diagnose_clients'] = $this->Clients_model->get_filter_result($from,$to);
			$data['query'] = $this->db->last_query();
		} else {
			$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients($nothing,'');
			//print_r($data['xin_diagnose_clients']);
		}

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('524',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/all_auth_approved", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }




    public function change_dependent_requests() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		if(isset($_GET['approve']))
		{
			 
			$id            =  $_GET['clients_family_id'];
			$requester_id  =  $_GET['requester_id'];


			$data_to_change  =  $this->Training_model->getAll2('xin_clients_family_req',' clients_family_id = '.$id.' ');
			if(!empty($data_to_change[0]->client_profile) )
			{
				$data_to_upload = array(

					'client_profile' => $data_to_change[0]->client_profile,

					'name'           => $data_to_change[0]->name,  

					'contact_number' => $data_to_change[0]->contact_number,

					'sex'            => $data_to_change[0]->sex,

					'address_1'      => $data_to_change[0]->address_1,

					'relation'       => $data_to_change[0]->relation, 

					'location'       => $data_to_change[0]->location,

					'dob'            => $data_to_change[0]->dob,

					'last_name'      => $data_to_change[0]->last_name,

					'other_name'     => $data_to_change[0]->other_name, 

					'hospital_id'     => $data_to_change[0]->hospital_id,    
				);

			}else{
				$data_to_upload = array(
 

					'name'           => $data_to_change[0]->name,  

					'contact_number' => $data_to_change[0]->contact_number,

					'sex'            => $data_to_change[0]->sex,

					'address_1'      => $data_to_change[0]->address_1,

					'relation'       => $data_to_change[0]->relation, 

					'location'       => $data_to_change[0]->location,

					'dob'            => $data_to_change[0]->dob,

					'last_name'      => $data_to_change[0]->last_name,

					'other_name'     => $data_to_change[0]->other_name, 

					'hospital_id'     => $data_to_change[0]->hospital_id,    
				);
			}
			
			
 
			 
			$iresult = $this->Training_model->update2('xin_clients_family',' clients_family_id='.$requester_id.' ',$data_to_upload);


			$data_to_update2 = array( 
				'status'      => 'approved',
			);

			$iresult2 = $this->Training_model->update2('xin_clients_family_req',' clients_family_id='.$id.' ',$data_to_update2);

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}


		if(isset($_GET['reject']))
		{ 
			$id           =  $_GET['clients_family_id'];

			 

			$data_to_update2 = array( 
				'status'      => 'rejected',
			);

			$iresult2 = $this->Training_model->update2('xin_clients_family_req',' clients_family_id='.$id.' ',$data_to_update2);

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}

		$data['title'] =  'Dependant Change Requests | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_clients_family_req',' 1 order by clients_family_id desc');
		 
		$data['breadcrumbs'] = 'Dependant Change Requests';

		$data['path_url'] = 'training';
 

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('539',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/change_dependent_requests", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }

    public function new_enrollee_requests() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		if(isset($_GET['approve']))
		{
			$id = $_GET['client_id'];
			 
			$data_to_update2 = array( 
				'approve_code'      => '1',
			);

			$iresult2 = $this->Training_model->update2('xin_clients',' client_id='.$id.' ',$data_to_update2);

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}


		if(isset($_GET['reject']))
		{ 
			$id           =  $_GET['client_id'];

			$data_to_update2 = array( 
				'approve_code'      => '2',
			);

			$iresult2 = $this->Training_model->update2('xin_clients',' client_id='.$id.' ',$data_to_update2);

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}

		if (!is_null($this->input->post('to_date'))) {
			// echo "asd";die;
			$from_date = $this->input->post('from_date');
			$to_date = $this->input->post('to_date');
			$data['all_requests']  =  $this->Training_model->get_enrollee_requests_filtered($from_date,$to_date);
			// echo $this->db->last_query();die;

		}else{
			$data['all_requests']  =  $this->Training_model->get_enrollee_requests();
		}

		$data['title'] =  'New Enrollee Requests | '.$this->Xin_model->site_title();

		
		$data['all_organization']  =  $this->Training_model->get_organization()->result();	 
		
		$data['all_subscription']  =  $this->Training_model->get_subscription()->result();	 
		
		$data['all_state']  =  $this->Training_model->get_state()->result();	 
		 
		$data['breadcrumbs'] = 'New Enrollee Requests';

		$data['path_url'] = 'training';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('543',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/new_enrollee_requests", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }

    
	public function read() 
	{

		$data['title'] = $this->Xin_model->site_title();

		$id = $this->input->get('training_id');

		$result = $this->Training_model->read_training_information($id);

		$data = array(

				'title' => $this->Xin_model->site_title(),

				'company_id' => $result[0]->company_id,

				'training_id' => $result[0]->training_id,

				'employee_id' => $result[0]->employee_id,

				'training_type_id' => $result[0]->training_type_id,

				'trainer_id' => $result[0]->trainer_id,

				'start_date' => $result[0]->start_date,

				'finish_date' => $result[0]->finish_date,

				'training_cost' => $result[0]->training_cost,

				'training_status' => $result[0]->training_status,

				'description' => $result[0]->description,

				'performance' => $result[0]->performance,

				'remarks' => $result[0]->remarks,

				'all_employees' => $this->Xin_model->all_employees(),

				'all_training_types' => $this->Training_model->all_training_types(),

				'all_trainers' => $this->Trainers_model->all_trainers(),

				'all_companies' => $this->Xin_model->get_companies()

				);

		$session = $this->session->userdata('username');

		if(!empty($session)){ 

			$this->load->view('admin/training/dialog_training', $data);

		} else {

			redirect('admin/');

		}

	}

	

	// Validate and add info in database

	public function add_hospital() 
	{ 
		 
		if($this->input->post('add_type')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		 		

			/* Server side PHP input validation */

			$hospital_name    = $this->input->post('hospital_name');
			$location_id      = $this->input->post('location_id');
			$band_id          = $this->input->post('band_id');
			$tarrif           = $this->input->post('tarrif');
			$email            = $this->input->post('email');
			$password         = $this->input->post('password');
			$phone            = $this->input->post('phone');
			$bank_name            = $this->input->post('bank_name');
			$bank_account            = $this->input->post('bank_account');

			$created_on       = date("Y-m-d h:i:s");

	
			if($this->input->post('hospital_name')==='') {

	        	$Return['error'] = 'Hospital name is required';

			} else if($this->input->post('location_id')==='') {

	        	$Return['error'] = 'Location is required';

			} else if($this->input->post('band_id')==='') {

	        	$Return['error'] = 'Band is required';

			}   else if($this->input->post('email')==='') {

	        	$Return['error'] = 'Email is required';

			} else if($this->input->post('password')==='') {

	        	$Return['error'] = 'Password is required';

			} else if($this->input->post('phone')==='') {

	        	$Return['error'] = 'Phone is required';

			} else if($this->input->post('bank_name')==='') {

	        	$Return['error'] = 'Bank Name is required';

			} else if($this->input->post('bank_account')==='') {

	        	$Return['error'] = 'Bank account number is required';

			}
			// else if($_FILES['logo']['size'] == 0) {

			// 	$fname = 'no file';

			// 	$Return['error'] = $this->lang->line('xin_error_logo_field');

			// }
			else {
				if(is_uploaded_file($_FILES['logo']['tmp_name'])) {

					//checking image type
					$tmp_name = $_FILES["logo"]["tmp_name"];

					$Return['error'] = $tmp_name;
					die;

					$allowed =  array('png','jpg','jpeg','gif');

					$filename = $_FILES['logo']['name'];

					$ext = pathinfo($filename, PATHINFO_EXTENSION);

					if(in_array($ext,$allowed)){

						if ($_FILES['logo']['size'] == 0) {
							$tmp_name == 'logo_1576535669.jpeg';
						}else{
						}

						$bill_copy = "uploads/hospital/logo/";
						if (!file_exists($bill_copy)) 
						{
						    mkdir($bill_copy, 0777, true);
						}
						// basename() may prevent filesystem traversal attacks;

						// further validation/sanitation of the filename may be appropriate

						$lname = basename($_FILES["logo"]["name"]);

						$newfilename = 'logo_'.round(microtime(true)).'.'.$ext;

						move_uploaded_file($tmp_name, $bill_copy.$newfilename);

						$fname = $newfilename;

					} else {

						$Return['error'] = $this->lang->line('xin_error_attatchment_type');

					}

				}else{
					$fname = 'hospital_icon.png';
				}

			}
		
			// $location = $this->Training_model->getAll2('xin_location',' location_id ='.$location_id)->result();
	    	// $location = substr($location[0]->location_name,0, 3);

	    	// $last_id = $this->Training_model->getAll2('xin_hospital',' location_id ='.$location_id.'order by hospital_id desc limit 1');
	    	// if (!empty($last_id)) {
	    	// 	$last_id = $last_id[0]->loc_id;
	    	// 	$last_id = substr($last_id, 3, 3);
	    	// }else{
	    	// 	$last_id = 0;
	    	// }				
 			// $Return['error'] = $loc_id;
			if($Return['error']!=''){

	       		$this->output($Return);

	    	}
 			
 			$loc_id = $this->loc_id($location_id);

			$data = array( 
				'hospital_name' => $hospital_name,
				'location_id'   => $location_id,
				'band_id'       => $band_id,
				'loc_id'       => $loc_id,
				// 'tarrif'        => $tarrif, 
				'email'         => $email, 
				'password'      => $password, 
				'phone'         => $phone, 
				'bank_name'     => $bank_name, 
				'bank_account'  => $bank_account, 
				'created_on'    => $created_on,  
				'logo_img'      => $fname,  
			);


			$iresult = $this->Training_model->insertDataTB('xin_hospital',$data);

			if ($iresult) { 
				$Return['result'] = 'Hospital has been added successfully.';	 
			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}

	public function loc_id($id){
		$location = $this->Training_model->getAll2('xin_location',' location_id ='.$id);
	    
 	   	$location = substr($location[0]->location_name,0, 3);
 	   	$location = strtolower($location);

	    $last_id = $this->Training_model->get_hospital_last_id($id)->result();
	    	if (!empty($last_id)) {
	    		$last_id = $last_id[0]->loc_id;
	    		$last_id = substr($last_id, 3, 3);
	    	}else{
	    		$last_id = 0;
	    	}
 	   	$id = (intval($last_id))+1;
 	   	$id = str_pad($id, 3,'0',STR_PAD_LEFT);
	    $loc_id = $location.$id;
	    return $loc_id;
	}


	public function update_hospital() 
	{ 
		 
		if($this->input->post('hospital_name')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		 		

			/* Server side PHP input validation */

			$hospital_name    = $this->input->post('hospital_name');
			$location_id      = $this->input->post('location_id');
			$band_id          = $this->input->post('band_id');
			// $tarrif           = $this->input->post('tarrif');
			$email            = $this->input->post('email');
			$password         = $this->input->post('password');
			$phone            = $this->input->post('phone');
			$bank_name        = $this->input->post('bank_name');
			$bank_account     = $this->input->post('bank_account');

			$id               = $this->input->post('hospital_id');

			$created_on       = date("Y-m-d h:i:s");

	
			if($this->input->post('hospital_name')==='') {

	        	$Return['error'] = 'Hospital name is required';

			} else if($this->input->post('location_id')==='') {

	        	$Return['error'] = 'Location is required';

			} else if($this->input->post('band_id')==='') {

	        	$Return['error'] = 'Band is required';

			} else if($this->input->post('email')==='') {

	        	$Return['error'] = 'Email is required';

			} else if($this->input->post('password')==='') {

	        	$Return['error'] = 'Password is required';

			} else if($this->input->post('phone')==='') {

	        	$Return['error'] = 'Phone is required';

			} else if($this->input->post('bank_name')==='') {

	        	$Return['error'] = 'Bank name is required';

			} else if($this->input->post('bank_account')==='') {

	        	$Return['error'] = 'Bank account number is required';

			}
				if($_FILES['logo']['size'] != 0) 
				{

					if(is_uploaded_file($_FILES['logo']['tmp_name'])) {

						//checking image type

						$allowed =  array('png','jpg','jpeg','gif');

						$filename = $_FILES['logo']['name'];

						$ext = pathinfo($filename, PATHINFO_EXTENSION);

						

						if(in_array($ext,$allowed)){

							$tmp_name = $_FILES["logo"]["tmp_name"];

							$bill_copy = "uploads/hospital/logo/";
							if (!file_exists($bill_copy)) 
							{
							    mkdir($bill_copy, 0777, true);
							}
							// basename() may prevent filesystem traversal attacks;

							// further validation/sanitation of the filename may be appropriate

							$lname = basename($_FILES["logo"]["name"]);

							$newfilename = 'logo_'.round(microtime(true)).'.'.$ext;

							move_uploaded_file($tmp_name, $bill_copy.$newfilename);

							$fname = $newfilename;

						} else {

							$Return['error'] = $this->lang->line('xin_error_attatchment_type');

						}

					}

				}
		

				

			if($Return['error']!=''){

	       		$this->output($Return);

	    	}
 			

 			if(isset($fname) and !empty($fname))
 			{
 				$data = array( 
					'hospital_name' => $hospital_name,
					'location_id'   => $location_id,
					'band_id'       => $band_id,
					// 'tarrif'        => $tarrif, 
					'email'         => $email, 
					'password'      => $password, 
					'phone'         => $phone, 
					'bank_name'     => $bank_name, 
					'bank_account'  => $bank_account, 
					'created_on'    => $created_on,  
					'logo_img'      => $fname,

				);	
 			}else {
 				$data = array( 
					'hospital_name' => $hospital_name,
					'location_id'   => $location_id,
					'band_id'       => $band_id,
					// 'tarrif'        => $tarrif, 
					'email'         => $email, 
					'password'      => $password, 
					'phone'         => $phone, 
					'bank_name'     => $bank_name, 
					'bank_account'  => $bank_account,
					'created_on'    => $created_on,  
				);
 			}

			


			$iresult = $this->Training_model->update2('xin_hospital',' hospital_id='.$id.' ',$data);

			if ($iresult) { 
				$Return['result'] = 'Hospital has been updated successfully.';	 
			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}

	
 	public function delete_hospital() 
 	{

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$id = $_REQUEST['_token'];
		 
		$Return['csrf_hash'] = $this->security->get_csrf_hash();

		$result = $this->Training_model->delete2('xin_hospital',' hospital_id = '.$id.' ');
		 
		if(isset($id)) {

			$Return['result'] = 'Hospital been deleted successfully';

		} else {

			$Return['error'] = $this->lang->line('xin_error_msg');

		}

		$this->output($Return);

	}


	public function delete_drug() 
 	{

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$id = $_REQUEST['_token'];
		 
		$Return['csrf_hash'] = $this->security->get_csrf_hash();

		$result = $this->Training_model->delete2('xin_hospital_drugs',' drug_id = '.$id.' ');
		 
		if(isset($id)) {

			$Return['result'] = 'Drug been deleted successfully';

		} else {

			$Return['error'] = $this->lang->line('xin_error_msg');

		}

		$this->output($Return);

	}

	

	// training details

	public function details($id = null) 
 	{
 		 

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}


		if($id == '')
		{
			redirect('admin/Hospital');
		}else{

			$data['title'] = $this->Xin_model->site_title();

			$data['all_hospital_detail']   =  $this->Training_model->getAll2('xin_hospital',' hospital_id ='.$id.'    ');

			$data['all_hospital_drugs']   =  $this->Training_model->getAll2('xin_hospital_drugs',' hospital_id ='.$id.'    ');

			$data['xin_services_hospital']   =  $this->Training_model->getAll2('xin_services_hospital',' hospital_id ='.$id.'    ');


			$data['breadcrumbs'] = 'Hospital Profile';

			$data['path_url'] = 'training_details';

			$role_resources_ids = $this->Xin_model->user_role_resource();

			if(in_array('602',$role_resources_ids)) 
			{

				if(!empty($session)){ 

					$data['subview'] = $this->load->view("admin/hospitals/hospital_details", $data,TRUE);

					$this->load->view('admin/layout/layout_main', $data); //page load

				} else {

					redirect('admin/');

				}

			}else {

				redirect('admin/dashboard');

			} 

		}    

    }

    public function organization_detail($id = null) 
 	{
 		 

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}


		if($id == '')
		{
			redirect('admin/Hospital/organizations');
		}else{

			$data['title'] = $this->Xin_model->site_title();

			$data['all_hospital_detail']   =  $this->Training_model->getAll2('xin_hospital',' hospital_id ='.$id.'    ');

			$data['all_hospital_drugs']   =  $this->Training_model->getAll2('xin_hospital_drugs',' hospital_id ='.$id.'    ');

			$data['xin_services_hospital']   =  $this->Training_model->getAll2('xin_services_hospital',' hospital_id ='.$id.'    ');

			$data['all_organizations']   =  $this->Training_model->getAll2('xin_organization',' id ='.$id.'    ');


			$data['breadcrumbs'] = 'Organization Profile';

			$data['path_url'] = 'training_details';

			$role_resources_ids = $this->Xin_model->user_role_resource();

			if(in_array('622',$role_resources_ids)) 
			{

				if(!empty($session)){ 

					$data['subview'] = $this->load->view("admin/hospitals/organization/organization_detail", $data,TRUE);

					$this->load->view('admin/layout/layout_main', $data); //page load

				} else {

					redirect('admin/');

				}

			}else {

				redirect('admin/dashboard');

			} 

		}    

    }

	 

	 // Validate and update info in database

	public function update_status() {

	

		if($this->input->post('edit_type')=='update_status') {

			

			$id = $this->input->post('token_status');

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		

			$data = array(

			'performance' => $this->input->post('performance'),

			'training_status' => $this->input->post('status'),

			'remarks' => $this->input->post('remarks')

			);

			

			$result = $this->Training_model->update_status($data,$id);		

			

			if ($result == TRUE) {

				$Return['result'] = $this->lang->line('xin_success_training_status_updated');

			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}

	

	public function delete() {

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$id = $this->uri->segment(4);

		$Return['csrf_hash'] = $this->security->get_csrf_hash();

		$result = $this->Training_model->delete_record($id);

		if(isset($id)) {

			$Return['result'] = $this->lang->line('xin_success_training_deleted');

		} else {

			$Return['error'] = $this->lang->line('xin_error_msg');

		}

		$this->output($Return);

	}



	public function bands() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){ 
			redirect('admin/dashboard'); 
		}

		$data['title']       =  'Bands | '.$this->Xin_model->site_title();

		$data['all_bands']   =  $this->Training_model->getAll2('xin_bands',' 1 order by band_id desc');

		$data['breadcrumbs'] = 'Band';

		$data['path_url']    = 'training';
 

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('54',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/bands/bands_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }

    public function add_band()
    { 
    	 
	 	 

		if($this->input->post('add_type')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		 		

			/* Server side PHP input validation */

			$band_name    = $this->input->post('band_name');

			$created_on   = date("Y-m-d h:i:s");

	
			if($this->input->post('band_name')==='') {

	        	$Return['error'] = 'Brand name is required';

			}  

				

			if($Return['error']!=''){

	       		$this->output($Return);

	    	}
 

			$data = array(

				'band_name' => $band_name,

				'created_on' => $created_on, 

			);


			$iresult = $this->Training_model->insertDataTB('xin_bands',$data);

			if ($iresult) { 
				$Return['result'] = 'Band has been added successfully.';	 
			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}


	 public function update_band()
    { 
    	 
	 	 

		if($this->input->post('band_name')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		 		

			/* Server side PHP input validation */

			$band_name    = $this->input->post('band_name');
			$band_id      = $this->input->post('band_id');

			$created_on   = date("Y-m-d h:i:s");

	
			if($this->input->post('band_name')==='') {

	        	$Return['error'] = 'Brand name is required';

			}  

				

			if($Return['error']!=''){

	       		$this->output($Return);

	    	}
 

			$data = array(

				'band_name' => $band_name,

				'created_on' => $created_on, 

			);


			$iresult = $this->Training_model->update2('xin_bands','band_id = '.$band_id.' ',$data);

			if ($iresult) { 
				$Return['result'] = 'Band has been updated successfully.';	 
			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}

	public function delete_bands() {

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$id = $_REQUEST['_token'];
		 
		$Return['csrf_hash'] = $this->security->get_csrf_hash();

		$result = $this->Training_model->delete2('xin_bands',' band_id = '.$id.' ');
		 
		if(isset($id)) {

			$Return['result'] = 'Band has been deleted successfully';

		} else {

			$Return['error'] = $this->lang->line('xin_error_msg');

		}

		$this->output($Return);

	}



	public function locations() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] =  'Location | '.$this->Xin_model->site_title();

		$data['all_locations']  =  $this->Training_model->getAll2('xin_location',' 1 order by location_id desc');

		$data['breadcrumbs'] = 'Location';

		$data['path_url'] = 'training'; 
		 
		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('54',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/locations/location_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }


	public function add_location()
    { 
     
		if($this->input->post('add_type')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		 		

			/* Server side PHP input validation */

			$location_name    = $this->input->post('location_name');

			$created_on       = date("Y-m-d h:i:s");

	
			if($this->input->post('band_name')==='') {

	        	$Return['error'] = 'location name is required';

			}  

				

			if($Return['error']!=''){

	       		$this->output($Return);

	    	}
 

			$data = array(

				'location_name' => $location_name,

				'created_on' => $created_on, 

			);


			$iresult = $this->Training_model->insertDataTB('xin_location',$data);

			if ($iresult) { 
				$Return['result'] = 'Location has been added successfully.';	 
			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}


	public function update_location()
    { 
     
		if($this->input->post('location_name')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		 		

			/* Server side PHP input validation */

			$location_name    = $this->input->post('location_name');
			$id               = $this->input->post('location_id');

			$created_on       = date("Y-m-d h:i:s");

	
			if($this->input->post('band_name')==='') {

	        	$Return['error'] = 'location name is required';

			}  

				

			if($Return['error']!=''){

	       		$this->output($Return);

	    	}
 

			$data = array(

				'location_name' => $location_name,

				'created_on' => $created_on, 

			);


			$iresult = $this->Training_model->update2('xin_location',' location_id ='.$id.' ',$data);

			if ($iresult) { 
				$Return['result'] = 'Location has been updated successfully.';	 
			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}


	public function delete_location() {

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$id = $_REQUEST['_token'];
		 
		$Return['csrf_hash'] = $this->security->get_csrf_hash();

		$result = $this->Training_model->delete2('xin_location',' location_id = '.$id.' ');
		 
		if(isset($id)) {

			$Return['result'] = 'Location has been deleted successfully';

		} else {
			
			$Return['error'] = $this->lang->line('xin_error_msg');

		}

		$this->output($Return);

	}


	public function organizations() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] =  'Organization | '.$this->Xin_model->site_title();

		$data['all_organizations']  =  $this->Training_model->getAll2('xin_organization',' 1 order by id desc');

		$data['all_business']  =  $this->Training_model->getAll2('business_type',' 1 order by business_id desc');


		$data['all_locations']  =  $this->Training_model->getAll2('xin_location',' 1 order by location_id desc');

		$data['breadcrumbs'] = 'Organization';

		$data['path_url'] = 'training'; 
		 
		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('622',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/organization/organization_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }


	public function add_organization()
    { 
     
		if($this->input->post('add_type')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		 		

			/* Server side PHP input validation */

			$name              = $this->input->post('name');
			$location_id       = $this->input->post('location_id');
			$contact_person    = $this->input->post('contact_person');
			$rc_number         = $this->input->post('rc_number');
			$type_business     = $this->input->post('type_business'); 
			$comm_date     	   = $this->input->post('comm_date'); 
			$created_on        = date("Y-m-d h:i:s");

	
			if($this->input->post('name')==='') {

	        	$Return['error'] = 'Organization name is required';

			} else if($this->input->post('location_id')==='') {

	        	$Return['error'] = 'location is required';

			}else if($this->input->post('contact_person')==='') {

	        	$Return['error'] = 'Contact person is required';

			}else if($this->input->post('rc_number')==='') {

	        	$Return['error'] = 'RC number is required';

			}else if($this->input->post('type_business')==='') {

	        	$Return['error'] = 'Type of business is required';

			} /* Check if file uploaded..*/

			else if($_FILES['logo']['size'] == 0) {

				$fname = 'no file';

			} else {

				if(is_uploaded_file($_FILES['logo']['tmp_name'])) {

					//checking image type

					$allowed =  array('png','jpg','jpeg','gif');

					$filename = $_FILES['logo']['name'];

					$ext = pathinfo($filename, PATHINFO_EXTENSION);

					

					if(in_array($ext,$allowed)){

						$tmp_name = $_FILES["logo"]["tmp_name"];

						$bill_copy = "uploads/organization/logo/";
						if (!file_exists($bill_copy)) 
						{
						    mkdir($bill_copy, 0777, true);
						}
						// basename() may prevent filesystem traversal attacks;

						// further validation/sanitation of the filename may be appropriate

						$lname = basename($_FILES["logo"]["name"]);

						$newfilename = 'logo_'.round(microtime(true)).'.'.$ext;

						move_uploaded_file($tmp_name, $bill_copy.$newfilename);

						$fname = $newfilename;

					} else {

						$Return['error'] = $this->lang->line('xin_error_attatchment_type');

					}

				}

			}


				

			if($Return['error']!=''){

	       		$this->output($Return);

	    	}
 





			$data = array(

				'name'           => $name,
				'location_id'    => $location_id,
				'rc_number'      => $rc_number,
				'contact_person' => $contact_person,
				'type_business'  => $type_business,
				'logo_name'      => $fname,
				'comm_date' 	 => $comm_date,

				'created_on' => $created_on

			);


			$iresult = $this->Training_model->insertDataTB('xin_organization',$data);

			if ($iresult) { 
				$Return['result'] = 'Organization has been added successfully.';	 
			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}



	public function update_organization()
    { 
     
		if($this->input->post('name')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		 		

			/* Server side PHP input validation */

			$organization_id   = $this->input->post('organization_id');
			$name              = $this->input->post('name');
			$location_id       = $this->input->post('location_id');
			$contact_person    = $this->input->post('contact_person');
			$rc_number         = $this->input->post('rc_number');
			$type_business     = $this->input->post('type_business'); 
			$created_on        = date("Y-m-d h:i:s");

	
			if($this->input->post('name')==='') {

	        	$Return['error'] = 'Organization name is required';

			} else if($this->input->post('location_id')==='') {

	        	$Return['error'] = 'location is required';

			}else if($this->input->post('contact_person')==='') {

	        	$Return['error'] = 'Contact person is required';

			}else if($this->input->post('rc_number')==='') {

	        	$Return['error'] = 'RC number is required';

			}else if($this->input->post('type_business')==='') {

	        	$Return['error'] = 'Type of business is required';

			} /* Check if file uploaded..*/

		  	if($_FILES['logo']['size'] != 0)  
		  	{

				if(is_uploaded_file($_FILES['logo']['tmp_name'])) 
				{

					//checking image type

					$allowed =  array('png','jpg','jpeg','gif');

					$filename = $_FILES['logo']['name'];

					$ext = pathinfo($filename, PATHINFO_EXTENSION);

					

					if(in_array($ext,$allowed)){

						$tmp_name = $_FILES["logo"]["tmp_name"];

						$bill_copy = "uploads/organization/logo/";
						if (!file_exists($bill_copy)) 
						{
						    mkdir($bill_copy, 0777, true);
						}
						// basename() may prevent filesystem traversal attacks;

						// further validation/sanitation of the filename may be appropriate

						$lname = basename($_FILES["logo"]["name"]);

						$newfilename = 'logo_'.round(microtime(true)).'.'.$ext;

						move_uploaded_file($tmp_name, $bill_copy.$newfilename);

						$fname = $newfilename;

					} else {

						$Return['error'] = $this->lang->line('xin_error_attatchment_type');

					}

				}

			}


				

			if($Return['error']!=''){

	       		$this->output($Return);

	    	}
 



	    	if(isset($fname) and !empty($fname))
	    	{
	    		$data = array( 
					'name'           => $name,
					'location_id'    => $location_id,
					'rc_number'      => $rc_number,
					'contact_person' => $contact_person,
					'type_business'  => $type_business,
					'logo_name'      => $fname, 
					'created_on'     => $created_on,  
				);

	    	}else {
	    		$data = array( 
					'name'           => $name,
					'location_id'    => $location_id,
					'rc_number'      => $rc_number,
					'contact_person' => $contact_person,
					'type_business'  => $type_business, 
					'created_on'     => $created_on,  
				);
	    	}

			


			$iresult = $this->Training_model->update2('xin_organization',' id ='.$organization_id.' ',$data);

			if ($iresult) { 
				$Return['result'] = 'Organization has been updated successfully.';	 
			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}

	public function update_organization_status()
    { 
     
			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		 		

			/* Server side PHP input validation */

			$id = $this->input->post('id');

			$status   = $this->input->post('status');

	
			if($this->input->post('status')==='') {

	        	$Return['error'] = 'Choose a status';

			}


				

			if($Return['error']!=''){

	       		$this->output($Return);

	    	}
 

    		$data = array( 
				'status'           => $status,
			);


			$iresult = $this->Training_model->update2('xin_organization',' id ='.$id.' ',$data);

			if ($iresult) { 
				$Return['result'] = 'Status has been updated successfully.';	 
			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

	}

	public function delete_organization() {

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$id = $_REQUEST['_token'];
		 
		$Return['csrf_hash'] = $this->security->get_csrf_hash();

		$result = $this->Training_model->delete2('xin_organization',' id = '.$id.' ');
		 
		if(isset($id)) {

			$Return['result'] = 'Organization been deleted successfully';

		} else {

			$Return['error'] = $this->lang->line('xin_error_msg');

		}

		$this->output($Return);

	}




	public function business_type() 
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] =  'Business | '.$this->Xin_model->site_title();

		$data['all_business']  =  $this->Training_model->getAll2('business_type',' 1 order by business_id desc');

		$data['breadcrumbs'] = 'Business';

		$data['path_url'] = 'training'; 
		 
		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('54',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/hospitals/business/business_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }


	public function add_business_type()
    { 
     
		if($this->input->post('add_type')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		 		

			/* Server side PHP input validation */

			$business_name    = $this->input->post('business_name');

			$created_on       = date("Y-m-d h:i:s");

	
			if($this->input->post('band_name')==='') {

	        	$Return['error'] = 'Business type is required';

			}  

				

			if($Return['error']!=''){

	       		$this->output($Return);

	    	}
 

			$data = array(

				'business_name' => $business_name,

				'created_on' => $created_on, 

			);


			$iresult = $this->Training_model->insertDataTB('business_type',$data);

			if ($iresult) { 
				$Return['result'] = 'Business type has been added successfully.';	 
			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}


	public function update_business()
    { 
     
		if($this->input->post('business_name')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		 		

			/* Server side PHP input validation */

			$business_name    = $this->input->post('business_name');
			$business_id      = $this->input->post('business_id');

			$created_on       = date("Y-m-d h:i:s");

	
			if($this->input->post('band_name')==='') {

	        	$Return['error'] = 'Business type is required';

			}  

				

			if($Return['error']!=''){

	       		$this->output($Return);

	    	}
 

			$data = array(

				'business_name' => $business_name,

				'created_on' => $created_on, 

			);


			$iresult = $this->Training_model->update2('business_type','business_id = '.$business_id.' ',$data);

			if ($iresult) { 
				$Return['result'] = 'Business type has been updated successfully.';	 
			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}


	public function delete_business() {

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$id = $_REQUEST['_token'];
		 
		$Return['csrf_hash'] = $this->security->get_csrf_hash();

		$result = $this->Training_model->delete2('business_type',' business_id = '.$id.' ');
		 
		if(isset($id)) {

			$Return['result'] = 'Business type has been deleted successfully';

		} else {

			$Return['error'] = $this->lang->line('xin_error_msg');

		}

		$this->output($Return);

	}











	public function import_drugs()
	{



		if($this->input->post('hospital_id')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();
			//validate whether uploaded file is a csv file

	   		$csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');

			$hospital_id = $this->input->post('hospital_id');

			if($_FILES['file']['name']==='') 
			{

				$this->session->set_flashdata('error','Error! Allowed file size is 2MB. ');
				redirect($_SERVER['HTTP_REFERER']);

			} else 
			{

				if(in_array($_FILES['file']['type'],$csvMimes))
				{

					if(is_uploaded_file($_FILES['file']['tmp_name']))
					{ 
						// check file size

						if(filesize($_FILES['file']['tmp_name']) > 2000000) {


							$this->session->set_flashdata('error','Error! Allowed file size is 2MB. ');
							redirect($_SERVER['HTTP_REFERER']);
							 

						} else 
						{ 
							//open uploaded csv file with read only mode

							$csvFile = fopen($_FILES['file']['tmp_name'], 'r');

							

							//skip first line 
							fgetcsv($csvFile);

							

							//parse data from csv file line by line

							while(($line = fgetcsv($csvFile)) !== FALSE)
							{

							 	if (isset($line[0]) and isset($line[1])) 
							 	{
							 		$data = array(

										'drug_name' => htmlentities($line[0]),

										'drug_price' => htmlentities($line[1]), 

										'hospital_id' => $hospital_id,  

										'created_on' => date('Y-m-d h:i:s')

									);

									$last_insert_id = $this->Training_model->insertDataTB('xin_hospital_drugs',$data); 
							 	}

								 

							}					

							//close opened csv file

							fclose($csvFile);

			

							$this->session->set_flashdata('success','Success! Data has been imported successfully');
							redirect($_SERVER['HTTP_REFERER']);

						}

					}else{

						$this->session->set_flashdata('error','Error! unable to upload file.');
						redirect($_SERVER['HTTP_REFERER']);

					}

				}else{

					$this->session->set_flashdata('error','Error! Invalid file format.');
					redirect($_SERVER['HTTP_REFERER']);
					 

				}

			}  
		}
	}



	public function import_hospital_services()
	{



		if($this->input->post('hospital_id')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();
			//validate whether uploaded file is a csv file

	   		$csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');

			$hospital_id = $this->input->post('hospital_id');

			if($_FILES['file']['name']==='') 
			{

				$this->session->set_flashdata('error','Error! Allowed file size is 2MB. ');
				redirect($_SERVER['HTTP_REFERER']);

			} else 
			{

				if(in_array($_FILES['file']['type'],$csvMimes))
				{

					if(is_uploaded_file($_FILES['file']['tmp_name']))
					{ 
						// check file size

						if(filesize($_FILES['file']['tmp_name']) > 2000000) {


							$this->session->set_flashdata('error','Error! Allowed file size is 2MB. ');
							redirect($_SERVER['HTTP_REFERER']);
							 

						} else 
						{ 
							//open uploaded csv file with read only mode

							$csvFile = fopen($_FILES['file']['tmp_name'], 'r');

							

							//skip first line 
							fgetcsv($csvFile);

							

							//parse data from csv file line by line

							while(($line = fgetcsv($csvFile)) !== FALSE)
							{

							

								$data = array(

									'service_name' => $line[0],

									'service_price' => $line[1], 

									'hospital_id' => $hospital_id,  

									'created_on' => date('Y-m-d h:i:s')

								);

								$last_insert_id = $this->Training_model->insertDataTB('xin_services_hospital',$data);  

							}					

							//close opened csv file

							fclose($csvFile);

			

							$this->session->set_flashdata('success','Success! Data has been imported successfully');
							redirect($_SERVER['HTTP_REFERER']);

						}

					}else{

						$this->session->set_flashdata('error','Error! unable to upload file.');
						redirect($_SERVER['HTTP_REFERER']);

					}

				}else{

					$this->session->set_flashdata('error','Error! Invalid file format.');
					redirect($_SERVER['HTTP_REFERER']);
					 

				}

			}  
		}
	}


	public function update_services()
    { 
    	 
	 	 

		if($this->input->post('service_name')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		 		


			/* Server side PHP input validation */

			$service_name       = $this->input->post('service_name');
			$service_price      = $this->input->post('service_price');
			$service_id_edit    = $this->input->post('service_id_edit');

			 

	
			if($this->input->post('service_name')==='') {

	        	$Return['error'] = 'Service name is required';

			} else if($this->input->post('service_price')==='') {

	        	$Return['error'] = 'Service price is required';

			}  

				

			if($Return['error']!=''){

	       		$this->output($Return);

	    	}
 

			$data = array(

				'service_name'  => $service_name,
				'service_price' => $service_price, 

			);


			$iresult = $this->Training_model->update2('xin_services_hospital','id = '.$service_id_edit.' ',$data);

			if ($iresult) { 
				$Return['result'] = 'Service has been updated successfully.';	
				$this->session->set_flashdata('success','Service has been updated successfully.');
				redirect($_SERVER['HTTP_REFERER']); 
			} else {
				$this->session->set_flashdata('error',$this->lang->line('xin_error_msg'));
				redirect($_SERVER['HTTP_REFERER']); 
				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}


	public function update_drug()
    { 
    	 
	 	 

		if($this->input->post('drug_nam')) 
		{		

			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

		 		

			/* Server side PHP input validation */

			$drug_nam       = $this->input->post('drug_nam');
			$drug_price     = $this->input->post('drug_price');
			$drug_id_edit   = $this->input->post('drug_id_edit');

			 

	
			if($this->input->post('drug_nam')==='') {

	        	$Return['error'] = 'Drug name is required';

			} else if($this->input->post('drug_price')==='') {

	        	$Return['error'] = 'Drug price is required';

			}  

				

			if($Return['error']!=''){

	       		$this->output($Return);

	    	}
 

			$data = array(

				'drug_name' => $drug_nam,
				'drug_price' => $drug_price, 

			);


			$iresult = $this->Training_model->update2('xin_hospital_drugs','drug_id = '.$drug_id_edit.' ',$data);

			if ($iresult) { 
				$Return['result'] = 'Drug has been updated successfully.';	 
			} else {

				$Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}



	public function delete_service() 
 	{

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$id = $_REQUEST['_token'];
		 
		$Return['csrf_hash'] = $this->security->get_csrf_hash();

		$result = $this->Training_model->delete2('xin_services_hospital',' id = '.$id.' ');
		 
		if(isset($id)) {

			$Return['result'] = 'Service been deleted successfully'; 
			$this->session->set_flashdata('success','Service has been deleted successfully.');
			redirect($_SERVER['HTTP_REFERER']); 

		} else {
			$Return['result'] = $this->lang->line('xin_error_msg');	
			$this->session->set_flashdata('error',$this->lang->line('xin_error_msg'));
			redirect($_SERVER['HTTP_REFERER']); 

		}

		$this->output($Return);

	}

	
	public function request_client()
	{
		$hid = $this->input->post('id');

		$clients = $this->Clients_model->get_all_clients_hospital($hid)->result();
		$dependants = $this->Clients_model->get_all_clients_dependants_hospital($hid)->result();

		$hospital = $this->Clients_model->get_hospital_info($hid)->result();
		if (!empty($hospital)) {
			$c_capitations = $this->Clients_model->get_all_clients_capitation_hospital($hospital[0]->hcp_code,'p')->result();
			$d_capitations = $this->Clients_model->get_all_clients_capitation_hospital($hospital[0]->hcp_code,'d')->result();
		}

		// array_merge($clients,$dependants);

		$html = '';
		foreach ($clients as $key => $value) {
			$html .= "
				<option value=\"PHC/".date("Y",strtotime($value->created_at))."/00".$value->client_id.":".$value->client_id."\">
				  ".ucfirst($value->name)." ".ucfirst($value->last_name)."(".ucfirst("PHC/".date("Y",strtotime($value->created_at))."/00".$value->client_id).")(Principal)</option>
			";
		}

		if(!empty($dependants)) {
            foreach ($dependants as $key => $dependant)
            {
                
             	$html .= "<option value=\"PHC/".date("Y",strtotime($dependant->created_on))."/00".$dependant->client_id."/".$dependant->clients_family_id.":".$dependant->clients_family_id."\">".ucfirst($dependant->name)." ".ucfirst($dependant->last_name). "(PHC/".date("Y",strtotime($dependant->created_on))."/00".$dependant->client_id."/".$dependant->clients_family_id.")(". ucfirst($dependant->relation).")"."</option>
             	";           
          
            }
        }

        foreach ($c_capitations as $c_cap) {
			$html .= "
				<option value=\"PHC/".date("Y",strtotime($c_cap->created_at))."/00".$c_cap->id.":".$c_cap->id.":1"."\">
				  ".ucfirst($c_cap->name)."(".ucfirst("PHC/".$c_cap->capitation_id).")(Principal-NHIS)</option>
			";
		}

		if(!empty($d_capitations)) {
            foreach ($d_capitations as $d_cap)
            {
            	$principal_id = $this->Clients_model->get_principal_id($d_cap->capitation_id)->result();
            	// print_r($principal_id);die;
                
             	$html .= "<option value=\"PHC/".$principal_id[0]->id."-00".$d_cap->id.":".$d_cap->id.":1"."\">".ucfirst($d_cap->name)."(PHC/".$principal_id[0]->capitation_id."".$d_cap->capitation_id.")(". ucfirst($d_cap->relation)."-NHIS)"."</option>
             	";           
          
            }
        }

		echo $html;
	}

	public function request_drug()
	{
		$hid = $this->input->post('id');

		$drugs = $this->Training_model->getAll2('xin_hospital_drugs',' hospital_id ='.$hid);

		$html = '';

		if (!empty($drugs)) {
			foreach ($drugs as $key => $value) {
				$html .= "
					<option value=\"".$value->drug_id."[".$value->drug_price."]\">".$value->drug_name."(₦".$value->drug_price.")</option>
				";
			}
		}

		echo $html;
	}

	public function request_service()
	{
		$hid = $this->input->post('id');

		$services = $this->Training_model->getAll2('xin_services_hospital',' hospital_id ='.$hid);

		$html = '';
		if (!empty($services)) {
			foreach ($services as $key => $value) {
				$html .= "
					<option value=\"".$value->id."[".$value->service_price."]\">".$value->service_name."(₦".$value->service_price.")</option>
				";
			}		
		}
		

		echo $html;
	}

	public function request_code()

	{		
		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		$role = 522;

		$check_roles = $this->check_roles($session['user_id'],$role);

		if($system[0]->module_training!='true' OR $check_roles != true){

			redirect('admin/dashboard');

		}

		$data['title'] =  'Request Code | '.$this->Xin_model->site_title();
		
		$data['breadcrumbs'] = 'Request Code';

		$data['path_url'] = 'training';
 

		if(isset($_GET['bill_request']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];
			$code = "";

			$iresult = $this->Clients_model->update_diagnose_status_record('1',$code,$id);
			redirect($_SERVER['HTTP_REFERER']);
		}


		// $data['title'] = 'HR Software';
		// $var = $this->session->userdata;

		// $this->load->view('hospital/clients/dependant_list', $data);	
		
		// $data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients($var['hospital_id']['hospital_id'],'');
		// $data['query'] = $this->db->last_query();
		$data['clients']   =  $this->Training_model->getAll2('xin_clients',' 1 order by client_id asc');
		// $data['dependants']   =  $this->Training_model->getAll2('xin_clients_family',' 1 order by clients_family_id asc');
		$data['hospitals']   =  $this->Training_model->getAll2('xin_hospital',' 1 order by hospital_id asc');
		
		// print_r($data['hospitals']);die;

		$data['subview'] = $this->load->view("admin/hospitals/request_code", $data, TRUE);

		$this->load->view('admin/layout/layout_main', $data); //page load

	}

	public function insert_clients_diagnose_admin()
	{
		// echo "Wahid Akram here data<br />";

		$type = "";
		$services_ids = array();
		$drugs_ids = array();
		$total = 0;
		$var = $this->session->userdata;

		$services = $this->input->post('diagnose_services');
		$drugs = $this->input->post('diagnose_drugs');

		// print_r($services);die;

		foreach ($services as $key => $value) {
			preg_match('/^(.*\[)(.*)(\])/', $value, $match);
			$arr = preg_split('/\[.*?\]/',$value);
			// echo "Clienttt = ".$arr[0]."<br />";
			array_push($services_ids,$arr[0]);
			$total = $total + $match[2];
			// echo "Clienttt = ".$match[2]."<br />";
			// print_r($arr);
		}

		foreach ($drugs as $key => $value) {
			preg_match('/^(.*\[)(.*)(\])/', $value, $match);
			$arr = preg_split('/\[.*?\]/',$value);
			// echo "Clienttt = ".$arr[0]."<br />";
			array_push($drugs_ids,$arr[0]);
			$total = $total + $match[2];
			// echo "Clienttt = ".$match[2]."<br />";
		}

		// print_r($services_ids);
		// print_r($drugs_ids);
		// echo "Total ww= ".$total."<br />";
		// die;
		$arr = explode(":", $this->input->post('diagnose_client'));
		$arr2 = explode("-", $arr[0]);
		$n = count($arr2);
		if($n == 3) { $type = "C"; } else { $type = "D"; }
		if (isset($arr[2])) {
			$capitation = 1;
		}else{
			$capitation = '';
		}

		$data = array(
			'diagnose_hospital_id'         => $this->input->post('hospital'),
			'diagnose_client_id'         => $arr[1],
			'diagnose_user_type'         => $type,
			'diagnose_date'         => Date('Y-m-d'),
			'diagnose_diagnose'         => $this->input->post('diagnose_diagnose'),
			'diagnose_procedure'         => $this->input->post('diagnose_procedure'),
			'diagnose_investigation'         => $this->input->post('diagnose_investigation'),
			'diagnose_medical'         => $this->input->post('diagnose_medical'),
			'diagnose_date_of_birth'         => $this->input->post('diagnose_date_of_birth'),
			'diagnose_total_sum'         => $total,
			'diagnose_status'         => '1',
			'is_admin'         => '1',
			'is_notify_1' => '1',
			'is_capitation'         => $capitation 
		);

		// print_r($data);die;

		$result = $this->Clients_model->add_diagnose_client($data,$services_ids,$drugs_ids);
		redirect($_SERVER['HTTP_REFERER']);
		// die;
	}

	public function client_update() {

		if($this->input->post('_method')=='EDIT') 
		{
			$Return['error'] = $this->lang->line('xin_error_name_field');
			$this->Output($Return);
			echo "asd";die;

			$id = $this->uri->segment(4);

			// Check validation for user input

			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');

			// $this->form_validation->set_rules('website', 'Website', 'trim|required|xss_clean');

			// $this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean');

			

			$name = $this->input->post('name');

			$last_name = $this->input->post('last_name');

			$email = $this->input->post('email');

			$contact_number = $this->input->post('contact_number');

			$address_1 = $this->input->post('address_1');

			$state = $this->input->post('location');

			$marital_status =  $this->input->post('marital_status');

			$dob            =  $this->input->post('dob');

			$gender            =  $this->input->post('gender');

			$company_name = $this->input->post('company_name');

			$state = $this->input->post('state');

			$subs = $this->input->post('subscription_ids');

			$status = $this->input->post('status');

			$ind_family = $this->input->post('ind_family');

			// $other_name     =  $this->input->post('other_name');

			// $address_2 = $this->input->post('address_2');

			// $city = $this->input->post('city');

			// $website = $this->input->post('website');

			// $zipcode = $this->input->post('zipcode');

			// $country = $this->input->post('country');


			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			 

			/* Server side PHP input validation */

			if($name==='') {

				$Return['error'] = $this->lang->line('xin_error_name_field');

			}  else if($contact_number==='') {

				$Return['error'] = $this->lang->line('xin_error_contact_field');

			} else if($email==='') {

				$Return['error'] = $this->lang->line('xin_error_cemail_field');

			}  

			// else if($this->input->post('username')==='') {

			// 	$Return['error'] = $this->lang->line('xin_employee_error_username');

			// }

			// else if($this->input->post('address_1')==='') {

			// 	$Return['error'] = 'Resident address is required.';

			// }else if($this->input->post('state')==='') {

			// 	$Return['error'] = 'State is required.';

			// }else if($this->input->post('marital_status')==='') {

			// 	$Return['error'] = 'Marital status is required.'; 

			// }
			else if($this->input->post('dob')==='') {

				$Return['error'] = 'Birthday is required.';

			}else if($this->input->post('hospital_id')==='') {

				$Return['error'] = 'Hospital is required.';

			}else if($this->input->post('last_name')==='') {

				$Return['error'] = 'Last name is required.';

			} else if($this->input->post('sex')==='') {

				$Return['error'] = 'Sex is required.';

			} 



			/* Check if file uploaded..*/

			else if($_FILES['client_photo']['size'] == 0) {

				 $fname = 'no file';

				 $no_logo_data = array(

					'name' => $this->input->post('name'), 

					'email' => $this->input->post('email'),

					'contact_number' => $this->input->post('contact_number'),

					'sex' => $this->input->post('sex'),

					'address_1'      => $this->input->post('address_1'),

					'marital_status'  => $this->input->post('marital_status'), 

					'state'           => $this->input->post('location'),

					'dob'             => $this->input->post('dob'),

					'last_name'       => $this->input->post('last_name'),

					'other_name'      => $this->input->post('other_name'), 
					'hospital_id'      => $this->input->post('hospital_id'), 

					'diseases'        => $diseases_data, 
					'disease_comment' => $comment, 
				);

			$result = $this->Clients_model->update_record($no_logo_data,$id);

		} else {

			if(is_uploaded_file($_FILES['client_photo']['tmp_name'])) {

				//checking image type

				$allowed =  array('png','jpg','jpeg','gif');

				$filename = $_FILES['client_photo']['name'];

				$ext = pathinfo($filename, PATHINFO_EXTENSION);

				

				if(in_array($ext,$allowed)){

					$tmp_name = $_FILES["client_photo"]["tmp_name"];

					$bill_copy = "uploads/clients/";

					// basename() may prevent filesystem traversal attacks;

					// further validation/sanitation of the filename may be appropriate

					$lname = basename($_FILES["client_photo"]["name"]);

					$newfilename = 'client_photo_'.round(microtime(true)).'.'.$ext;

					move_uploaded_file($tmp_name, $bill_copy.$newfilename);

					$fname = $newfilename;

					$data = array( 

						'client_profile' => $fname,

						'name' => $this->input->post('name'), 

						'email' => $this->input->post('email'),

						'contact_number' => $this->input->post('contact_number'),

						'sex' => $this->input->post('sex'),

						'address_1' => $this->input->post('address_1'),

						'marital_status' => $this->input->post('marital_status'), 

						'state' => $this->input->post('location'),

						'dob' => $this->input->post('dob'),

						'last_name' => $this->input->post('last_name'),
						
						'other_name' => $this->input->post('other_name'), 	
						'hospital_id' => $this->input->post('hospital_id'), 	
						'diseases'        => $diseases_data, 
						'disease_comment' => $comment, 

					);

					// update record > model

					$result = $this->Clients_model->update_record($data,$id);

				} else {

					$Return['error'] = $this->lang->line('xin_error_attatchment_type');

				}

			}

		}

	

			if($Return['error']!=''){
				$this->session->set_flashdata('error',$Return['error']);
				redirect($_SERVER['HTTP_REFERER']);
	       		$this->output($Return); 

	    	}		

				

			if ($result == TRUE) {

				$Return['result'] = $this->lang->line('xin_client_profile_update');

				$this->session->set_flashdata('success',$this->lang->line('xin_client_profile_update'));
				redirect($_SERVER['HTTP_REFERER']);



			} else {
				$this->session->set_flashdata('error',$this->lang->line('xin_error_msg'));
				redirect($_SERVER['HTTP_REFERER']);

				$Return['error'] = $Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

		}

	}

	public function new_client_update() {

			$Return['error'] = $this->lang->line('xin_error_name_field');
			$this->Output($Return);
			echo "asd";die;

			$id = $this->uri->segment(4);

			// Check validation for user input

			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');

			// $this->form_validation->set_rules('website', 'Website', 'trim|required|xss_clean');

			// $this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean');

			

			$name = $this->input->post('name');

			$last_name = $this->input->post('last_name');
			
			$other_name = $this->input->post('other_name');

			$email = $this->input->post('email');

			$contact_number = $this->input->post('contact_number');

			$address_1 = $this->input->post('address_1');

			$state = $this->input->post('state');

			$marital_status =  $this->input->post('marital_status');

			$dob            =  $this->input->post('dob');

			$gender            =  $this->input->post('gender');

			$company_name = $this->input->post('company_name');

			$state = $this->input->post('state');

			$subs = $this->input->post('subscription_ids');

			$status = $this->input->post('status');

			$ind_family = $this->input->post('ind_family');
			
			$hospital = $this->input->post('hospital_id');

			$diseases = $this->input->post('diseases');
			
			$comment = $this->input->post('comment');


			// $other_name     =  $this->input->post('other_name');

			// $address_2 = $this->input->post('address_2');

			// $city = $this->input->post('city');

			// $website = $this->input->post('website');

			// $zipcode = $this->input->post('zipcode');

			// $country = $this->input->post('country');


			/* Define return | here result is used to return user data and error for error message */

			$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

			$Return['csrf_hash'] = $this->security->get_csrf_hash();

			 

			/* Server side PHP input validation */

			if($name==='') {

				$Return['error'] = $this->lang->line('xin_error_name_field');

			}  else if($contact_number==='') {

				$Return['error'] = $this->lang->line('xin_error_contact_field');

			} else if($email==='') {

				$Return['error'] = $this->lang->line('xin_error_cemail_field');

			}  

			// else if($this->input->post('username')==='') {

			// 	$Return['error'] = $this->lang->line('xin_employee_error_username');

			// }

			// else if($this->input->post('address_1')==='') {

			// 	$Return['error'] = 'Resident address is required.';

			// }else if($this->input->post('state')==='') {

			// 	$Return['error'] = 'State is required.';

			// }else if($this->input->post('marital_status')==='') {

			// 	$Return['error'] = 'Marital status is required.'; 

			// }
			else if($this->input->post('dob')==='') {

				$Return['error'] = 'Birthday is required.';

			}else if($this->input->post('hospital_id')==='') {

				$Return['error'] = 'Hospital is required.';

			}else if($this->input->post('last_name')==='') {

				$Return['error'] = 'Last name is required.';

			} else if($this->input->post('sex')==='') {

				$Return['error'] = 'Sex is required.';

			} 



			/* Check if file uploaded..*/

			else if($_FILES['client_photo']['size'] == 0) {

				 $fname = 'no file';

				 $profile = array(
				 	'client_profile' => $fname
				 );

			// $result = $this->Clients_model->update_record($no_logo_data,$id);

		} else {

			if(is_uploaded_file($_FILES['client_photo']['tmp_name'])) {

				//checking image type

				$allowed =  array('png','jpg','jpeg','gif');

				$filename = $_FILES['client_photo']['name'];

				$ext = pathinfo($filename, PATHINFO_EXTENSION);

				

				if(in_array($ext,$allowed)){

					$tmp_name = $_FILES["client_photo"]["tmp_name"];

					$bill_copy = "uploads/clients/";

					// basename() may prevent filesystem traversal attacks;

					// further validation/sanitation of the filename may be appropriate

					$lname = basename($_FILES["client_photo"]["name"]);

					$newfilename = 'client_photo_'.round(microtime(true)).'.'.$ext;

					move_uploaded_file($tmp_name, $bill_copy.$newfilename);

					$fname = $newfilename;

					$profile = array( 

						'client_profile' => $fname, 

					);

					// update record > model


				} else {

					$Return['error'] = $this->lang->line('xin_error_attatchment_type');

				}

			}

		}

	

			if($Return['error']!=''){
				$this->session->set_flashdata('error',$Return['error']);
				redirect($_SERVER['HTTP_REFERER']);
	       		$this->output($Return); 

	    	}		

	    	array_merge($data,$profile);
				
			$result = $this->Clients_model->update_record($data,$id);

			if ($result == TRUE) {

				$Return['result'] = $this->lang->line('xin_client_profile_update');

				$this->session->set_flashdata('success',$this->lang->line('xin_client_profile_update'));
				redirect($_SERVER['HTTP_REFERER']);



			} else {
				$this->session->set_flashdata('error',$this->lang->line('xin_error_msg'));
				redirect($_SERVER['HTTP_REFERER']);

				$Return['error'] = $Return['error'] = $this->lang->line('xin_error_msg');

			}

			$this->output($Return);

			exit;

	}

	public function import_hospitals()
	{


		$Return = array('result' => '', 'error' => '', 'csrf_hash' => '');

		$Return['csrf_hash'] = $this->security->get_csrf_hash();
		//validate whether uploaded file is a csv file

		$csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');


		if ($_FILES['file']['name'] === '') {

			$this->session->set_flashdata('error', 'Error! Allowed file size is 2MB. ');
			redirect($_SERVER['HTTP_REFERER']);

		} else {


			if (in_array($_FILES['file']['type'], $csvMimes)) {

				if (is_uploaded_file($_FILES['file']['tmp_name'])) {
					// check file size


					if (filesize($_FILES['file']['tmp_name']) > 2000000) {


						$this->session->set_flashdata('error', 'Error! Allowed file size is 2MB. ');
						redirect($_SERVER['HTTP_REFERER']);


					} else {
						//open uploaded csv file with read only mode

						$csvFile = fopen($_FILES['file']['tmp_name'], 'r');


						//skip first line
						fgetcsv($csvFile);


						//parse data from csv file line by line

						while (($line = fgetcsv($csvFile)) !== FALSE) {
							$options = array('cost' => 12);
							$loc_id = $this->loc_id($line[1]);
							$data = array(

								'hospital_name' => $line[0],

								'location_id' => $line[1],

								'loc_id' => $loc_id,

								'band_id' => $line[2],

								'email' => $line[3],

								'password' => $line[4],

								'phone' => $line[5],

								'logo_img' => 'hospital_icon.png',

								'hcp_code' => $line[6],

								'bank_name'     => $line[7], 
					
								'bank_account'  => $line[8],

								'created_on' => date('Y-m-d H:i:s'),
							);

							$result = $this->Xin_model->add($data);


						}

						//close opened csv file

						fclose($csvFile);


						$this->session->set_flashdata('success', 'Success! Data has been imported successfully');
						redirect("/admin/Hospital");

					}

				} else {

					$this->session->set_flashdata('error', 'Error! unable to upload file.');
					redirect($_SERVER['HTTP_REFERER']);

				}

			} else {

				$this->session->set_flashdata('error', 'Error! Invalid file format.');
				redirect($_SERVER['HTTP_REFERER']);


			}

		}


	}

	public function import_organization()
	{


		$Return = array('result' => '', 'error' => '', 'csrf_hash' => '');

		$Return['csrf_hash'] = $this->security->get_csrf_hash();
		//validate whether uploaded file is a csv file

		$csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');


		if ($_FILES['file']['name'] === '') {

			$this->session->set_flashdata('error', 'Error! Allowed file size is 2MB. ');
			redirect($_SERVER['HTTP_REFERER']);

		} else {


			if (in_array($_FILES['file']['type'], $csvMimes)) {

				if (is_uploaded_file($_FILES['file']['tmp_name'])) {
					// check file size


					if (filesize($_FILES['file']['tmp_name']) > 2000000) {


						$this->session->set_flashdata('error', 'Error! Allowed file size is 2MB. ');
						redirect($_SERVER['HTTP_REFERER']);


					} else {
						//open uploaded csv file with read only mode

						$csvFile = fopen($_FILES['file']['tmp_name'], 'r');


						//skip first line
						fgetcsv($csvFile);


						//parse data from csv file line by line

						while (($line = fgetcsv($csvFile)) !== FALSE) {

							if ($line[5] != FALSE) {
								$date = date_create($line[5]);
								$comm_date = date_format($date,"Y-m-d");
							}else{
								$comm_date = '';
							}
	
							$data = array(

								'name' => $line[0],

								'location_id' => $line[1],

								'contact_person' => $line[2],

								'logo_name' => 'logo_1576582579.jpg',

								'rc_number' => $line[3],

								'type_business' => $line[4],

								'created_on' => date('Y-m-d H:i:s'),

								'comm_date' => $comm_date
							);

							$result = $this->Xin_model->add_organization($data);

						}

						//close opened csv file

						fclose($csvFile);

						$this->session->set_flashdata('success', 'Success! Data has been imported successfully');
						redirect("/admin/Hospital/organizations");

					}

				} else {

					$this->session->set_flashdata('error', 'Error! unable to upload file.');
					redirect($_SERVER['HTTP_REFERER']);

				}

			} else {

				$this->session->set_flashdata('error', 'Error! Invalid file format.');
				redirect($_SERVER['HTTP_REFERER']);


			}

		}


	}

	public function get_hospital_by_subs()
	{
		$id = $this->input->post('id');
		$bands = $this->Xin_model->get_subscription_band($id);
		$bands = explode(",", $bands[0]->band_types);
		$hids = array();

		// print_r($bands);die;

		foreach ($bands as $key => $value) {
			$hospitals = $this->Xin_model->get_hospital_band($value);
			foreach($hospitals as $hospital){
				array_push($hids, $hospital->hospital_id);
			}
		}

		$html = '';

		// print_r($hids);
		foreach ($hids as $hospital) {
			$hospital_info = $this->Xin_model->get_hospital_info($hospital);

			$html .= "<option value=\"".$hospital."\">".$hospital_info[0]->hospital_name."</option>";
		}

		echo $html;
	}

	public function check_roles($id, $role)
	{
		$user_info = $this->Xin_model->read_user_info($id);
		if ($user_info[0]->user_role_id) {
			return true;
		}else{
			$role_user = $this->Xin_model->read_user_role_info($user_info[0]->user_role_id);

			if(!is_null($role_user)){

		  		$role_resources_ids = explode(',',$role_user[0]->role_resources);

		    } else {

	     	  	$role_resources_ids = explode(',',0);	
	 
		    }

		    if (in_array($role, $role_resources_ids)) {
		    	return 1;
		    }else{
		    	return 0;
		    }	
		}
		

	}

	public function fetch_client_plan()
	{
		$id = explode(":",$this->input->post('id'));

		if (count($id) <3) {
			//regular
			$cid = explode("/", $id[0]);
			if (count($cid) < 4) {
				//principal
				$client = $this->Clients_model->get_clients_by_id($id[1])->result();

				if (!empty($client)) {
					$subs = $this->Clients_model->get_clients_subscription($client[0]->subscription_ids)->result();
				}

			}else{
				//dependant
				$cid = str_replace("0",'', $cid[2]);
				
				$client = $this->Clients_model->get_clients_by_id($cid)->result();
				
				if (!empty($client)) {
					$subs = $this->Clients_model->get_clients_subscription($client[0]->subscription_ids)->result();
				}
			}

			// if (!empty($subs)) {
			// 	echo "Plan: ".$subs[0]->plan_name;
			// }else{
			// 	echo "Plan: -";
			// }

		}else{
			//capitation
			// echo "Plan: NHIS";
		}

		// echo "count: ".count($id)."\n";

		// print_r($id);
	}
}

