<?php

 /**

 * NOTICE OF LICENSE

 *

 * This source file is subject to the HRSALE License

 * that is bundled with this package in the file license.txt.

 * It is also available through the world-wide-web at this URL:

 * http://www.hrsale.com/license.txt

 * If you did not receive a copy of the license and are unable to

 * obtain it through the world-wide-web, please send an email

 * to hrsalesoft@gmail.com so we can send you a copy immediately.

 *

 * @author   HRSALE

 * @author-email  hrsalesoft@gmail.com

 * @copyright  Copyright © hrsale.com. All Rights Reserved

 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Organization extends MY_Controller

{



   /*Function to set JSON output*/

	public function output($Return=array()){

		/*Set response header*/

		header("Access-Control-Allow-Origin: *");

		header("Content-Type: application/json; charset=UTF-8");

		/*Final JSON response*/

		exit(json_encode($Return));

	}

	

	public function __construct()

     {

          parent::__construct();

          //load the login model

          $this->load->model('Company_model');

		  $this->load->model("Department_model");

		  $this->load->model("Designation_model");

		  $this->load->model('Xin_model');

		  $this->load->model('Clients_model');
		  
		  $this->load->model('Training_model');

		  $this->load->model('Invoices_model');

     }

	 

	//chart page //

	public function chart() {

	

		$session = $this->session->userdata('username');

		if(empty($session)){ 

			redirect('admin/');

		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_orgchart!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] = $this->lang->line('xin_org_chart_title').' | '.$this->Xin_model->site_title();

		$data['breadcrumbs'] = $this->lang->line('xin_org_chart_title');

		$data['path_url'] = 'organization_chart';

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('96',$role_resources_ids)) {

			$data['subview'] = $this->load->view("admin/orgchart/orgchart", $data, TRUE);

			$this->load->view('admin/layout/layout_main', $data); //page load

		} else {

			redirect('admin/dashboard');

		}

	}

	
    public function organization_list()
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		if(isset($_GET['approve']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];
			$last_code = $this->Clients_model->get_last_tcid_code();
			$code = $last_code + 1;
			// $data_to_update = array( 
			// 	'hospital_id' => $hospital_id,  
			// );

			$iresult = $this->Clients_model->update_finance_bill_status('2',$code,$id);


			// $data_to_update2 = array( 
			// 	'status'      => 'approved',
			// );

			// $iresult2 = $this->Training_model->update2('xin_change_hospital_request',' change_hospital_request_id='.$id.' ',$data_to_update2);

			$this->session->set_flashdata('success','Data updated successfully.');
			redirect($_SERVER['HTTP_REFERER']);
		}

		if(isset($_GET['approve_all']))
		{
			$client_id    =  $_GET['client_id'];
			$hospital_id  =  $_GET['hospital_id'];
			$id           =  $_GET['id'];
			$from = $_GET['from'];
			$to = $_GET['to'];


			$all_clients = $this->Clients_model->all_clients_of_individual_hospital($hospital_id,$from,$to)->result();

			// print_r($all_clients);die;
			// echo $from." ".$to."<br />";
			// print_r($all_clients);
			// die;
			if(!empty($all_clients)) {
				foreach ($all_clients as $client) {
					$last_code = $this->Clients_model->get_last_tcid_code();
					$code = $last_code + 1;

					$id = $client->diagnose_id;

					$iresult = $this->Clients_model->update_finance_bill_status('2',$code,$id);
				}
				// die;
				// $last_code = $this->Clients_model->get_last_tcid_code();
				// $code = $last_code + 1;


				$this->session->set_flashdata('success','Data updated successfully.');
				redirect($_SERVER['HTTP_REFERER']);
			} else {
				$this->session->set_flashdata('success','Data failed to update.');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}

		// if(isset($_GET['reject']))
		// { 
		// 	$id =  $_GET['id'];
			 

		// 	$iresult = $this->Clients_model->update_diagnose_bill_status_record('4',$code,$id);
		// 	// $data_to_update2 = array( 
		// 	// 	'status'      => 'rejected',
		// 	// );

		// 	// $iresult2 = $this->Training_model->update2('xin_change_hospital_request',' change_hospital_request_id='.$id.' ',$data_to_update2);

		// 	$this->session->set_flashdata('success','Data updated successfully.');
		// 	redirect($_SERVER['HTTP_REFERER']);
		// }

		$data['title'] =  'Usage for Organization | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');

		$data['all_hospital']   =  $this->Training_model->getAll2('xin_hospital',' 1 order by hospital_id asc');
		
		$data['all_organization']   =  $this->Training_model->getAll2('xin_organization');
		 
		$data['breadcrumbs'] = 'Usage for Organization';

		$data['path_url'] = 'training';
 		$nothing = "";
		
		if($this->input->post('from_date')) {
			$hid = $this->input->post('hospital_id');
			$oid = $this->input->post('organization_id');
			$from = $this->input->post('from_date');
			$to = $this->input->post('to_date');

			// get organization expenses
			$total = array();
			$client = $this->Clients_model->get_clients_encounter_by_organization($oid)->result();
			
			if (!empty($client)) {
				foreach ($client as $key => $value) {
					array_push($total, $value->total_sum);
				}
			}
			
			$data['organization_expense'] = array_sum($total);

			$data['xin_diagnose_clients'] = $this->Clients_model->get_filter_result_of_hospital($hid,$from,$to);

			$data['organization_result'] = $this->Invoices_model->get_invoice_filtered($oid,$from,$to)->result();


			$data['from'] = "$from";
			$data['to'] = "$to"; 
			$data['hid'] = "$hid"; 

			$data['query'] = $this->db->last_query();
		}
		// else {
		// 	$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients($nothing,'');
		// }

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('604',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/company/organization_list", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }

    public function organization_encounter()
    {

		$session = $this->session->userdata('username');

		if(empty($session)){  
			redirect('admin/'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('admin/dashboard');

		}

		$data['title'] =  'Usage for all Organizations | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' 1 order by change_hospital_request_id desc');

		$data['all_hospital']   =  $this->Training_model->getAll2('xin_hospital',' 1 order by hospital_id asc');
		
		$data['all_organization']   =  $this->Training_model->getAll2('xin_organization');
		 
		$data['breadcrumbs'] = 'Organizations Usage';

		$data['path_url'] = 'training';
 		$nothing = "";
		
		if(!is_null($this->input->post('from_date'))) {
			$data['from_date'] = $this->input->post('from_date');
			$data['to_date'] = $this->input->post('to_date');
			
			// $data['organization_expense'] = array_sum($total);

			// $data['xin_diagnose_clients'] = $this->Clients_model->get_filter_result_of_hospital($hid,$from,$to);

			// $data['organization_result'] = $this->Invoices_model->get_invoice_filtered($oid,$from,$to)->result();

			// $data['from'] = "$from";
			// $data['to'] = "$to"; 
			// $data['hid'] = "$hid"; 

			// $data['query'] = $this->db->last_query();
		}
		// else {
		// 	$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients($nothing,'');
		// }

		$role_resources_ids = $this->Xin_model->user_role_resource();

		if(in_array('605',$role_resources_ids)) {

			if(!empty($session)){ 

				$data['subview'] = $this->load->view("admin/company/organization_encounter", $data, TRUE);

				$this->load->view('admin/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}

		} else {

			redirect('admin/dashboard');

		}

    }
	

	

} 

?>