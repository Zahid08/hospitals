<?php

 /**

 * NOTICE OF LICENSE

 *

 * This source file is subject to the HRSALE License

 * that is bundled with this package in the file license.txt.

 * It is also available through the world-wide-web at this URL:

 * http://www.hrsale.com/license.txt

 * If you did not receive a copy of the license and are unable to

 * obtain it through the world-wide-web, please send an email

 * to hrsalesoft@gmail.com so we can send you a copy immediately.

 *

 * @author   HRSALE

 * @author-email  hrsalesoft@gmail.com

 * @copyright  Copyright © hrsale.com. All Rights Reserved

 */

 if ( ! defined('BASEPATH')) exit('No direct script access allowed');



 class Clients extends MY_Controller
 {
 	public function __construct()
 	{

 		parent::__construct();

			//load the model		
 		$this->load->model('Login_model');

 		$this->load->model('Employees_model');

 		$this->load->model('Users_model');

 		$this->load->library('email');

 		$this->load->model("Xin_model");

 		$this->load->model("Designation_model");

 		$this->load->model("Department_model");

 		$this->load->model("Location_model");

 		$this->load->model("Clients_model");
 		$this->load->model("Training_model");
 		$this->load->model("Tickets_model");
 		$this->load->model("Custom_fields_model");
 	}
 	/*Function to set JSON output*/
 	public function output($Return=array())
 	{

 		/*Set response header*/

 		header("Access-Control-Allow-Origin: *");

 		header("Content-Type: application/json; charset=UTF-8");

 		/*Final JSON response*/

 		exit(json_encode($Return));

 	}



 	public function index()

 	{		

 		if(isset($_GET['bill_request']))
 		{
 			$client_id    =  $_GET['client_id'];
 			$hospital_id  =  $_GET['hospital_id'];
 			$id           =  $_GET['id'];
 			$code = "";

 			$iresult = $this->Clients_model->update_diagnose_status_record('1',$code,$id);
 			redirect($_SERVER['HTTP_REFERER']);
 		}


 		$data['title'] = 'Process Encounter';
 		$var = $this->session->userdata;

		// print_r($var);die;
		// $this->load->view('hospital/clients/dependant_list', $data);	
 		$data['all_hospital_drugs']   =  $this->Training_model->getAll2('xin_hospital_drugs',' hospital_id ='.$var['hospital_id']['hospital_id'].'    ');

 		$data['xin_services_hospital']   =  $this->Training_model->getAll2('xin_services_hospital',' hospital_id ='.$var['hospital_id']['hospital_id'].'    ');
 		$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients($var['hospital_id']['hospital_id'],'');		

 		$data['query'] = $this->db->last_query();
 		$data['clients']   =  $this->Training_model->getAll2('xin_clients','approve_code = 1'.' ');

 		$hospital = $this->Clients_model->get_hospital_info($var['hospital_id']['hospital_id'])->result();


 		$data['capitation'] = $this->Clients_model->get_capitation_hospital($hospital[0]->hcp_code)->result();

		// print_r($data['capitation']);die;
 		$data['subview'] = $this->load->view("hospital/client/index", $data, TRUE);

		$this->load->view('hospital/layout/layout_main', $data); //page load

	}

	public function insert_clients_diagnose()
	{
		// echo "Wahid Akram here data<br />";
		if($this->input->post('hrsale_form_primary_case')){
			// echo "Wahid Akram here data<br />";

			$type = "";
			$services_ids = array();
			$drugs_ids = array();
			$total = 0;
			$total1 = 0;
			$var = $this->session->userdata;

			$services = $this->input->post('diagnose_services');
		// print_r($services); die();
			$drugs = $this->input->post('diagnose_drugs');

			$quantity = $this->input->post('quantity');


		//$mulitplevaluedrugs = ltrim($this->input->post('mulitplevaluedrugs'),'₦');


			if($services){
				foreach ($services as $key => $value) {
					preg_match('/^(.*\[)(.*)(\])/', $value, $match);
					$arr = preg_split('/\[.*?\]/',$value);
			// echo "Clienttt = ".$arr[0]."<br />";
					array_push($services_ids,$arr[0]);
					$total = $total + $match[2];
			// echo "Clienttt = ".$match[2]."<br />";
			// print_r($arr);
				}
			}
			if($drugs){
				foreach ($drugs as $key => $value) {
					preg_match('/^(.*\[)(.*)(\])/', $value, $match);
					$arr = preg_split('/\[.*?\]/',$value);
			// echo "Clienttt = ".$arr[0]."<br />";
					array_push($drugs_ids,$arr[0]);
					$total = $total + ($match[2]*$quantity[$key]);
					$total1 = $total1 + ($match[2]*$quantity[$key]);
			// echo "Clienttt = ".$match[2]."<br />";
				}
			}
		// print_r($services_ids);
		// print_r($drugs_ids);
		// echo "Total ww= ".$total."<br />";
		// die;
			$arr = explode(":", $this->input->post('diagnose_client'));
			$arr2 = explode("-", $arr[0]);
			$n = count($arr2);
			if($n == 3) { $type = "C"; } else { $type = "D"; }
			if (isset($arr[2])) {
				$capitation = 1;
			}else{
				$capitation = '';
			}
$last_code = $this->Clients_model->get_last_auth_code();
			$code = $last_code + 1;
		// print_r($arr);die;
			$data = array(
				'diagnose_hospital_id'         => $var['hospital_id']['hospital_id'],
				'diagnose_client_id'         => $arr[1],
				'diagnose_user_type'         => $type,
				'diagnose_date'         => Date('Y-m-d'),
				'diagnose_diagnose'         => $this->input->post('diagnose_diagnose'),
				'diagnose_procedure'         => $this->input->post('diagnose_procedure'),
				'diagnose_investigation'         => $this->input->post('diagnose_investigation'),
				'diagnose_medical'         => $this->input->post('diagnose_medical'),
				'diagnose_date_of_birth'         => $this->input->post('diagnose_date_of_birth'),
				'diagnose_total_sum'         => $total,
				'diagnose_multiple_drugs_value'         =>  $total1,
			//'diagnose_quantity'         => $this->input->post('quantity'),
				'diagnose_status'         => '1',
				'is_capitation' 		  => $capitation,
				'diagnose_generated_code'	 => $code,
				'diagnose_status'      		 => '2',
				'diagnose_status_approve_by' => 'primarycase',
				'is_notify_2' 				 => 1,
				'diagnose_primary_case'	=> 'YES'
			);

			$result = $this->Clients_model->add_diagnose_client1($data,$services_ids,$drugs_ids,$quantity);
		redirect($_SERVER['HTTP_REFERER']);
		die;



		}
		else{
			$type = "";
			$services_ids = array();
			$drugs_ids = array();
			$total = 0;
			$total1 = 0;
			$var = $this->session->userdata;

			$services = $this->input->post('diagnose_services');
		// print_r($services); die();
			$drugs = $this->input->post('diagnose_drugs');

			$quantity = $this->input->post('quantity');


		//$mulitplevaluedrugs = ltrim($this->input->post('mulitplevaluedrugs'),'₦');


			if($services){
				foreach ($services as $key => $value) {
					preg_match('/^(.*\[)(.*)(\])/', $value, $match);
					$arr = preg_split('/\[.*?\]/',$value);
			// echo "Clienttt = ".$arr[0]."<br />";
					array_push($services_ids,$arr[0]);
					$total = $total + $match[2];
			// echo "Clienttt = ".$match[2]."<br />";
			// print_r($arr);
				}
			}
			if($drugs){
				foreach ($drugs as $key => $value) {
					preg_match('/^(.*\[)(.*)(\])/', $value, $match);
					$arr = preg_split('/\[.*?\]/',$value);
			// echo "Clienttt = ".$arr[0]."<br />";
					array_push($drugs_ids,$arr[0]);
					$total = $total + ($match[2]*$quantity[$key]);
					$total1 = $total1 + ($match[2]*$quantity[$key]);
			// echo "Clienttt = ".$match[2]."<br />";
				}
			}
		// print_r($services_ids);
		// print_r($drugs_ids);
		// echo "Total ww= ".$total."<br />";
		// die;
			$arr = explode(":", $this->input->post('diagnose_client'));
			$arr2 = explode("-", $arr[0]);
			$n = count($arr2);
			if($n == 3) { $type = "C"; } else { $type = "D"; }
			if (isset($arr[2])) {
				$capitation = 1;
			}else{
				$capitation = '';
			}

		// print_r($arr);die;
			$data = array(
				'diagnose_hospital_id'         => $var['hospital_id']['hospital_id'],
				'diagnose_client_id'         => $arr[1],
				'diagnose_user_type'         => $type,
				'diagnose_date'         => Date('Y-m-d'),
				'diagnose_diagnose'         => $this->input->post('diagnose_diagnose'),
				'diagnose_procedure'         => $this->input->post('diagnose_procedure'),
				'diagnose_investigation'         => $this->input->post('diagnose_investigation'),
				'diagnose_medical'         => $this->input->post('diagnose_medical'),
				'diagnose_date_of_birth'         => $this->input->post('diagnose_date_of_birth'),
				'diagnose_total_sum'         => $total,
				'diagnose_multiple_drugs_value'         =>  $total1,
			//'diagnose_quantity'         => $this->input->post('quantity'),
				'diagnose_status'         => '1',
				'is_capitation' 		  => $capitation
			);

			$result = $this->Clients_model->add_diagnose_client1($data,$services_ids,$drugs_ids,$quantity);
			redirect($_SERVER['HTTP_REFERER']);
		// die;
		}

		
	}

	public function addmorederviceanddrugs1()
	{
		//addmorederviceanddrugs
		
		
		$strs = $this->input->post('strs');
		foreach(explode('&', $strs) as $value)
		{
			$value1 = explode('=', $value);
			if($value1[0]=='diagnose_drugs_more[]')
			{
				$data['drugs'][] = $value1[1];
			}
			if($value1[0]=='quantitymore[]')
			{
				$data['quant'][] = $value1[1];
			}
			if($value1[0]=='diagnose_services_more[]')
			{
				$data['service'][] = $value1[1];
			}
			if($value1[0]=='hostel_idss')
			{
				$data['hostel_idss'] = $value1[1];
			}
			if($value1[0]=='sendadmissionflage')
			{
				$data['sendadmissionflage'] = $value1[1];
			}
			if($value[0]=='days')
			{
				$data['days'] = $value1[1];
			}
		}

		$services = $data['service'];
		$drugs = $data['drugs'];
		$qunatity = $data['quant'];
		$hostelids = $data['hostel_idss'];
		$sendadmissionflage = $data['sendadmissionflage'];
		$days = $data['days'];
		

		$rgh = $this->Clients_model->getoneinfo($hostelids);
		$services_ids = array();
		$drugs_ids = array();
		$total = 0;
		$total1 = 0;
		if($services){
			foreach ($services as $key => $value) {
				preg_match('/^(.*\[)(.*)(\])/', $value, $match);
				$arr = preg_split('/\[.*?\]/',$value);
			// echo "Clienttt = ".$arr[0]."<br />";
				array_push($services_ids,$arr[0]);
				$total = $total + $match[2];
			// echo "Clienttt = ".$match[2]."<br />";
			// print_r($arr);
			}
		}
		if($drugs){
			foreach ($drugs as $key => $value) {
				preg_match('/^(.*\[)(.*)(\])/', $value, $match);
				$arr = preg_split('/\[.*?\]/',$value);
				// echo "Clienttt = ".$arr[0]."<br />";
				array_push($drugs_ids,$arr[0]);
				$total = $total + ($match[2]*$qunatity[$key]);
				$total1 = $total1 + ($match[2]*$qunatity[$key]);
				
				// echo "Clienttt = ".$match[2]."<br />";
			}
		}
		$sume = $rgh->diagnose_total_sum +$total;
		$sume1 = $rgh->diagnose_multiple_drugs_value +$total1;
		$data = array(
			
			'diagnose_total_sum'         => $sume,
			'diagnose_multiple_drugs_value'         =>  $sume1,

		);
		$this->db->where('diagnose_id', $hostelids);
		$this->db->update('xin_clients_diagnose', $data);
		foreach ($services_ids as $key => $value) {
			$data1 = array(
				'diagnose_mains_id'         => $hostelids,
				'diagnose_services_id'         => $value
			);
			$this->db->insert('xin_clients_diagnose_services', $data1);
		}
		foreach ($drugs_ids as $key => $value) {
			$data2 = array(
				'diagnose_maind_id'         => $hostelids,
				'diagnose_quantity'         => $qunatity[$key],
				'diagnose_drugs_id'         => $value
			);
			$this->db->insert('xin_clients_diagnose_drugs', $data2);
		}
		if($sendadmissionflage == 2){
			$discharge_date = date('Y-m-d H:i:s',strtotime('+'.$days.' days',strtotime(date('Y-m-d H:i:s'))));
			$data23 = array(
				'send_admission_status'         => $sendadmissionflage,
				'admission_date'         => date('Y-m-d H:i:s'),
				'days'         => $days,
				'discharge_date'         => $discharge_date
			);
			$this->db->where('diagnose_id', $hostelids);
			$this->db->update('xin_clients_diagnose', $data23);
		}
		

		echo 'sucess';
	}
	public function addmorederviceanddrugs()
	{
		//addmorederviceanddrugs
		
		
		$strs = $this->input->post('strs');
		foreach(explode('&', $strs) as $value)
		{
			$value1 = explode('=', $value);
			if($value1[0]=='diagnose_drugs_more[]')
			{
				$data['drugs'][] = $value1[1];
			}
			if($value1[0]=='quantitymore[]')
			{
				$data['quant'][] = $value1[1];
			}
			if($value1[0]=='diagnose_services_more[]')
			{
				$data['service'][] = $value1[1];
			}
			if($value1[0]=='hostel_idss')
			{
				$data['hostel_idss'] = $value1[1];
			}
		}

		$services = $data['service'];
		$drugs = $data['drugs'];
		$qunatity = $data['quant'];
		$hostelids = $data['hostel_idss'];
		

		$rgh = $this->Clients_model->getoneinfo($hostelids);
		$services_ids = array();
		$drugs_ids = array();
		$total = 0;
		$total1 = 0;
		if($services){
			foreach ($services as $key => $value) {
				preg_match('/^(.*\[)(.*)(\])/', $value, $match);
				$arr = preg_split('/\[.*?\]/',$value);
			// echo "Clienttt = ".$arr[0]."<br />";
				array_push($services_ids,$arr[0]);
				$total = $total + $match[2];
			// echo "Clienttt = ".$match[2]."<br />";
			// print_r($arr);
			}
		}
		if($drugs){
			foreach ($drugs as $key => $value) {
				preg_match('/^(.*\[)(.*)(\])/', $value, $match);
				$arr = preg_split('/\[.*?\]/',$value);
				// echo "Clienttt = ".$arr[0]."<br />";
				array_push($drugs_ids,$arr[0]);
				$total = $total + ($match[2]*$qunatity[$key]);
				$total1 = $total1 + ($match[2]*$qunatity[$key]);
				
				// echo "Clienttt = ".$match[2]."<br />";
			}
		}
		$sume = $rgh->diagnose_total_sum +$total;
		$sume1 = $rgh->diagnose_multiple_drugs_value +$total1;
		$data = array(
			
			'diagnose_total_sum'         => $sume,
			'diagnose_multiple_drugs_value'         =>  $sume1,

		);
		$this->db->where('diagnose_id', $hostelids);
		$this->db->update('xin_clients_diagnose', $data);
		foreach ($services_ids as $key => $value) {
			$data1 = array(
				'diagnose_mains_id'         => $hostelids,
				'diagnose_services_id'         => $value
			);
			$this->db->insert('xin_clients_diagnose_services', $data1);
		}
		foreach ($drugs_ids as $key => $value) {
			$data2 = array(
				'diagnose_maind_id'         => $hostelids,
				'diagnose_quantity'         => $qunatity[$key],
				'diagnose_drugs_id'         => $value
			);
			$this->db->insert('xin_clients_diagnose_drugs', $data2);
		}

		echo 'sucess';
	}
	public function multipledrugsfetch(){
		
		$drugs = $this->input->post("drugs");
		$quan = $this->input->post("quantity");
		if($quan ==""){
			$quan=1;
		}
		$total=0;
		foreach ($drugs as $key => $value) {
			preg_match('/^(.*\[)(.*)(\])/', $value, $match);
			$arr = preg_split('/\[.*?\]/',$value);
			
			$total = $total + $match[2];
			
			// echo "Clienttt = ".$match[2]."<br />";
		}
		echo $total*$quan;
	}
	public function edit_diagnose()
	{		

		if($this->input->post("diagnose_client")) {

			$type = "";
			$services_ids = array();
			$drugs_ids = array();
			$total = 0;
			$var = $this->session->userdata;

			$services = $this->input->post('diagnose_services');
			$drugs = $this->input->post('diagnose_drugs');

			foreach ($services as $key => $value) {
				preg_match('/^(.*\[)(.*)(\])/', $value, $match);
				$arr = preg_split('/\[.*?\]/',$value);
				// echo "Clienttt = ".$arr[0]."<br />";
				array_push($services_ids,$arr[0]);
				$total = $total + $match[2];
				// echo "Clienttt = ".$match[2]."<br />";
				// print_r($arr);
			}
			foreach ($drugs as $key => $value) {
				preg_match('/^(.*\[)(.*)(\])/', $value, $match);
				$arr = preg_split('/\[.*?\]/',$value);
				// echo "Clienttt = ".$arr[0]."<br />";
				array_push($drugs_ids,$arr[0]);
				$total = $total + $match[2];
				// echo "Clienttt = ".$match[2]."<br />";
			}

			// print_r($services_ids);
			// print_r($drugs_ids);
		// echo "Total ww= ".$total."<br />";
		// die;
			$arr = explode(":", $this->input->post('diagnose_client'));
			$arr2 = explode("-", $arr[0]);
			$n = count($arr2);
			if($n == 3) { $type = "C"; } else { $type = "D"; }

			if($this->input->post("diagnose_rejection") == "bill") {
				$data = array(
					'diagnose_hospital_id' => $var['hospital_id']['hospital_id'],
					'diagnose_client_id' => $arr[1],
					'diagnose_user_type' => $type,
					'diagnose_date' => Date('Y-m-d'),
					'diagnose_diagnose' => $this->input->post('diagnose_diagnose'),
					'diagnose_procedure' => $this->input->post('diagnose_procedure'),
					'diagnose_investigation' => $this->input->post('diagnose_investigation'),
					'diagnose_medical' => $this->input->post('diagnose_medical'),
					'diagnose_total_sum' => $total,
					'diagnose_bill_status' => '1',
					'diagnose_bill_approve_by' => NULL
				);
			} else {
				$data = array(
					'diagnose_hospital_id' => $var['hospital_id']['hospital_id'],
					'diagnose_client_id' => $arr[1],
					'diagnose_user_type' => $type,
					'diagnose_date' => Date('Y-m-d'),
					'diagnose_diagnose' => $this->input->post('diagnose_diagnose'),
					'diagnose_procedure' => $this->input->post('diagnose_procedure'),
					'diagnose_investigation' => $this->input->post('diagnose_investigation'),
					'diagnose_medical' => $this->input->post('diagnose_medical'),
					'diagnose_total_sum' => $total,
					'diagnose_status' => '1',
					'diagnose_status_approve_by' => NULL
				);
			}

			// print_r($data); echo "<br />";
			// print_r($services_ids); echo "<br />";
			// print_r($drugs_ids); echo "<br />";
			// die;
			// $iresult = $this->Clients_model->update_auth_status_again('1',$_GET['eid']);
			$result = $this->Clients_model->edit_diagnose_client($data,$services_ids,$drugs_ids,$_GET['eid']);
			redirect('hospital/clients/index');
		}
		// die;
		if(isset($_GET['id'])) {
		// echo "wahidd page accesseddd..";die;
			$data['title'] = 'Process Encounter';
			$var = $this->session->userdata;

			// $this->load->view('hospital/clients/dependant_list', $data);	
			$data['all_hospital_drugs']   =  $this->Training_model->getAll2('xin_hospital_drugs',' hospital_id ='.$var['hospital_id']['hospital_id'].'    ');

			$data['xin_services_hospital']   =  $this->Training_model->getAll2('xin_services_hospital',' hospital_id ='.$var['hospital_id']['hospital_id'].'    ');
			$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients2($var['hospital_id']['hospital_id'],$_GET['id']);

			$data['query'] = $this->db->last_query();
			$data['clients']   =  $this->Training_model->getAll2('xin_clients',' 1 order by client_id asc');

			// print_r($data['xin_diagnose_clients']);
			$data['subview'] = $this->load->view("hospital/client/edit_diagnose", $data, TRUE);

			$this->load->view('hospital/layout/layout_main', $data); //page load
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	public function fetch_diagnose_id()
	{
		$is_hospital = $this->input->post('hospital');

		if ($is_hospital != true || is_null($is_hospital)) {
			$role_resources_ids = $this->Xin_model->user_role_resource();

			if (in_array("640", $role_resources_ids)) {
				$delete_drug = true;
			}else{
				$delete_drug = false;

			}

			// echo "Delete drug: ".$delete_drug;

			if (in_array("641", $role_resources_ids)) {
				$delete_service = true;
			}else{
				$delete_service = false;
			}
		}

		$url= $_SERVER['HTTP_REFERER'];
		
		$fetched_result = array();
		$did = $this->input->post('id');

		$diagnose = $this->Clients_model->get_diagnose($did)->result();
		
		$fetched_result = $this->Clients_model->view_individual_hospital_diagnose_clients($did,$diagnose[0]->is_capitation,$diagnose[0]->diagnose_client_id);		
		// print_R($fetched_result);

		$services = $this->Clients_model->read_individual_hospital_diagnose_services($fetched_result["diagnose_id"]);
		$drugs = $this->Clients_model->read_individual_hospital_diagnose_drugs($fetched_result["diagnose_id"]);

		$actual_drugs = $this->Clients_model->read_individual_hospital_diagnose_actual_drugs($fetched_result["diagnose_id"]);
		$actual_services = $this->Clients_model->read_individual_hospital_diagnose_actual_services($fetched_result["diagnose_id"]);
		
		$actual_drug_bill = $this->Clients_model->actual_drugs_sum($did);
		$actual_service_bill = $this->Clients_model->actual_service_sum($did); 
		$actual_bill= $actual_drug_bill[0]->d_id + $actual_service_bill[0]->s_id;

		$hcp = preg_replace('/\s+/', '', $fetched_result["hospital_name"]);
		$date = date("Ymd", strtotime($fetched_result["diagnose_date_time"]));
		
		$days_ago_20 = date('Y-m-d', strtotime('+20 days', strtotime($date)));
		
		
		
		echo '
		
		<style>
		.modal-lg {
			width: 85% !important;
		}
		th.bg-dark.text-center.asd{
			width:50%;
		}
		</style><div class="row"><div class="col-lg-12"><div class="col-lg-4"><h5 class="text-info"><b>Generated Code:</b>'; if(empty($fetched_result["diagnose_generated_code"])) echo "-----"; else echo "HCP-".$hcp."-".$fetched_result["diagnose_generated_code"]."-".$date."-".$fetched_result["diagnose_generated_code"]; 
		echo'</h5>
		</div>
		<div class="col-lg-4 text-center">
		<h5 class="text-info"><b>Hospital:</b> '; if(empty($fetched_result["hospital_name"])) echo "-----"; else echo $fetched_result["hospital_name"]; echo'</h5>
		</div>
		<div class="col-lg-4" style="text-align:right;"> 
		<h5 class="text-info"><b>Date:</b> '.$fetched_result["diagnose_date"].'</h5>
		</div></div>';
		if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests" || $url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests_admin" || $url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests" || $url == "https://care.precioushmo.com/app/admin/Hospital/all_auth_approved" || $url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests_rejected" || $url == "https://care.precioushmo.com/app/admin/Hospital/all_bills_paid" || $url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_bill_requests_rejected" || $url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_bill_requests"){
			echo '<div class="col-md-12 text-center">
			<h5><a href="javascript:;" style="color:red;font-weight:600;" onclick="return getinfodiv();" id="getinfodiv">Enrollee Profile Summary</a></h5>
			</div>
			<div class="col-md-12" id="infodiv" style="display:none;">
			<div class="col-lg-6"><h5 class="text-info"><b>Phone Number : </b>'; 

			$result1q = $this->Clients_model->get_clients_by_id($fetched_result["diagnose_client_id"])->row();
//echo $fetched_result['phone'];

			if($result1q && $result1q->ind_family=='individual'){echo $result1q->contact_number;}elseif($result1q && $result1q->ind_family=='family'){echo $result1q->contact_number;}else{$result1qq = $this->Clients_model->get_clients_by_id($fetched_result["client_id"])->row();echo $result1qq->contact_number;}
			if($result1q){$sex = $result1q->sex;}else{$sex = $fetched_result['sex'];}

			echo'</h5>
			</div>
			<div class="col-lg-6" style="text-align:right;"> 
			<h5 class="text-info"><b>Gender : </b> '.ucfirst($sex).'</h5>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-6"><h5 class="text-info"><b>Organization : </b>';

			$result1q = $this->Clients_model->get_clients_by_id($fetched_result["client_id"])->row();

			$organization = $fetched_result['company_name'];
			if(trim($organization) ==""){
				$organization = $result1q->company_name;
			}
			$organizations = $this->Clients_model->get_organization_info1($organization);
			echo $organizations['name'];
			echo'</h5>
			</div>
			<div class="col-lg-6" style="text-align:right;"> 
			<h5 class="text-info"><b>Plan type : </b> ';
			$sub_ids = $fetched_result['subscription_ids'];
			if(trim($sub_ids) =="")
			{
				$sub_ids = $result1q->subscription_ids;
			}
			$sub_ids = $this->Clients_model->get_subscribtionname_info($sub_ids);
			echo $sub_ids['plan_name'];
			echo '</h5>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-6"><h5 class="text-info"><b>Relationship : </b>'; 
			$result1q = $this->Clients_model->get_clients_by_id($fetched_result["diagnose_client_id"])->row();
			if($result1q && $result1q->ind_family == 'individual'){$rela = 'principal';}elseif($result1q && $result1q->ind_family == 'family'){$rela = 'principal';}else{$rela = $fetched_result['relation'];}
			echo ucfirst($rela);
			echo'</h5>
			</div>

			</div>';
		}
		echo '<div class="col-lg-12">

		<table class="table" style="padding: 100px; border: 0px;">
		<tbody>

		<tr>
		<td class="col-lg-2" align="left"><h5><b>Enrollee Name:</b></h5></td>
		<td align="left">'; if ($fetched_result["is_capitation"] != 1) {
			if($fetched_result["diagnose_user_type"] == 'C') { echo $fetched_result["cname"]." ".$fetched_result["clname"]." ".$fetched_result["coname"]; } else {  if ($fetched_result["cname"] != '' && $fetched_result["clname"] != '') { echo $fetched_result["cname"]." ".$fetched_result["clname"]." ".$fetched_result["coname"]; }else{echo $fetched_result["dname"]." ".$fetched_result["dlname"]." ".$fetched_result["doname"];} }

		}else{
			echo $fetched_result["cap_name"];
			} echo '</td>
			<td class="col-lg-2" align="right"><h5><b>Diagnose:</b></h5></td>
			<td align="left">'.$fetched_result["diagnose_diagnose"].'</td>
			</tr>
			<tr>
			<td class="col-lg-2" align="left"><h5><b>Procedure:</b></h5></td>
			<td align="left">'.$fetched_result["diagnose_procedure"].'</td>
			<td class="col-lg-2" align="right"><h5><b>Investigation:</b></h5></td>
			<td align="left">'.$fetched_result["diagnose_investigation"].'</td>
			</tr>
			<tr>
			<td class="col-lg-2" align="left"><h5><b>Medical:</b></h5></td>
			<td colspan="3" align="left">'.$fetched_result["diagnose_medical"].'</td>
			</tr>';
			if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
				echo '
				<tr>
				<th colspan="2" class="bg-primary text-center" style="background-color: #343a40; color: #FFF;">Updated Bill</th>
				<th colspan="3" class="text-center bg-primary" style="background-color: #343a40; color: #FFF;">Actual Bill</th>
				</tr>';
			}
			if(($url == "https://care.precioushmo.com/app/admin/Hospital/all_auth_approved"  && $days_ago_20 > date('Y-m-d')))
			{
				$all_hospital_drugs   =  $this->Training_model->getAll2('xin_hospital_drugs',' hospital_id ='.$did.'    ');

				$xin_services_hospital   =  $this->Training_model->getAll2('xin_services_hospital',' hospital_id ='.$did.'    ');


				echo '
				<tr><th colspan="5"><button name="addmoredrugusaddservice" class="btn btn-success" onclick="return getmoredurd('.$did.');">Add Drugs & Services</button></th><tr><th colspan="5">

				<style>.select2.select2-container{width:100% !important;}</style><div class="form-body row" style="display:none;" id="drugsservices">
				<form id="frms" name="frms">
				<div class="col-md-12" style="margin-top:10px" >
				<div class="col-md-6">
				<div class="form-group">

				<label for="services">Select Services</label>
				<select class="form-control" name="diagnose_services_more[]" data-plugin="select_hrm" data-placeholder="Select Services" multiple id="servicesmodel"> 

				<option value="">Select Services</option>';

				if (isset($xin_services_hospital) and !empty($xin_services_hospital)) { 


					foreach ($xin_services_hospital as $key => $value): 
						echo '<option value="'.$value->id.' ['.$value->service_price.']">'.$value->service_name.' (₦'.$value->service_price.')</option>';

					endforeach;
				}

				echo '</select>
				</div>

				</div>
				<div class="col-md-6">
				<div class="row form-group" id="divmore1">
				<div class="col-md-7">
				<label for="last_name">Select Drug</label>
				<select class="form-control" name="diagnose_drugs_more[]" data-plugin="select_hrm" data-placeholder="Select Drugs" id="drugsmore1"> 

				<option value="">Select Drug</option>';

				if (isset($all_hospital_drugs) and !empty($all_hospital_drugs)) {


					foreach ($all_hospital_drugs as $key => $value):


						echo '<option value="'.$value->drug_id.'['.$value->drug_price.']">'.$value->drug_name.'(₦'.$value->drug_price.')'.'</option>';

					endforeach;
				}

				echo '<select>
				</div>
				<div class="col-md-3">
				<label for="last_name">Quantity</label>
				<input type="number" name="quantitymore[]" id="quantitymore1" class="form-control" min="1" value="1">
				</div>
				<div class="col-md-2">
				<label for="last_name" class="padding"></label>
				<input type="button" name="addmore" id="addmoremore" onclick="return getaddmoremore(2,'.$did.')" value="+" class="btn btn-success">

				</div>

				</div>


				</div>

				</div>
				</br>
				<div class="col-md-12">
				<input type="hidden" name="hostel_idss" id="hostel_idss" value="'.$did.'">

				<button type="button" class="btn btn-success" onclick="return adddrugsservice();">UPDATE</button></div>
				</form>
				</div>
				</th></tr>
				';
			}
			echo '
			<tr>
			<th colspan="';
			if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
				echo '1';
			} else{
				echo '2'; } echo '" class="bg-dark text-center asd" style="background-color: #343a40; color: #FFF;">DRUGS</th>
				<th colspan="';
				if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
					echo '1';
				} else{
					echo '2'; } echo '" class="text-center bg-dark" style="background-color: #343a40; color: #FFF;">SERVICES</th>';
					if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
						echo '
						<th colspan="1" class="bg-dark text-center asd" style="background-color: #343a40; color: #FFF;">DRUGS</th>
						<th colspan="1" class="text-center bg-dark" style="background-color: #343a40; color: #FFF;">SERVICES</th>';
					}
					echo '
					</tr>
					<tr>
					<td colspan="';
					if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
						echo '1';
					} else{
						echo '2'; } echo '">
						<table class="table table-bordered ';
						if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
							echo 'col-lg-3';
						} else{
							echo 'col-lg-6'; } echo '">
							<thead>
							<tr>
							<th>Name</th><th>Quantity</th>
							<th>Price</th>';
							if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests" || ($url == "https://care.precioushmo.com/app/admin/Hospital/all_auth_approved" && $days_ago_20 > date('Y-m-d'))){
								if ($delete_drug != false) {
									echo '<th>Action</th>';
								}
							}
							echo '
							</tr>
							</thead>
							<tbody>';
							if(!empty($drugs)) {
								foreach ($drugs as $key => $dvalue) { 
									echo '
									<tr>
									<div class="drug_row">
									<td>'.$dvalue->drug_name.'</td>
									<td>'.$dvalue->diagnose_quantity.'</td>
									<td>'.$dvalue->diagnose_quantity*$dvalue->drug_price.'</td>
									</div>';
									if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests" || ($url == "https://care.precioushmo.com/app/admin/Hospital/all_auth_approved" && $days_ago_20 > date('Y-m-d')))
									{

										if ($delete_drug != false)
										{
											echo '<td><a data-toggle="modal" data-target="#drug_reasonModal" onclick="return loadDrugReasonModal('.$dvalue->drug_id.','.$dvalue->hospital_id.','.$dvalue->diagnose_autod_id.','.$dvalue->diagnose_maind_id.')"><span class="close drug_close" style="float:left;color:red;">x</span></a></td>';
										}
									}
									echo '
									</tr>';
								}
							}
							echo '
							</tbody>
							</table>
							</td>
							<td colspan="';
							if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
								echo '1';
							} else{
								echo '2'; } echo '">
								<table class="table table-bordered ';
								if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
									echo 'col-lg-3';
								} else{
									echo 'col-lg-6'; } echo '">
									<thead>
									<tr>
									<th>Name</th>
									<th>Price</th>';
									if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests"  || ($url == "https://care.precioushmo.com/app/admin/Hospital/all_auth_approved"  && $days_ago_20 > date('Y-m-d'))){
										if ($delete_service != false) {
											echo '<th>Action</th>';
										}
									}
									echo '
									</tr>
									</thead>
									<tbody>';
									if(!empty($services)) {
                              foreach ($services as $key => $svalue) { //print_r($svalue); //die();
                              	echo '
                              	<tr>
                              	<div class="service_row">
                              	<td>'.$svalue->service_name.'</td>
                              	<td>'.number_format($svalue->service_price).'</td>

                              	</div>';

                              	if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests" || ($url == "https://care.precioushmo.com/app/admin/Hospital/all_auth_approved"  && $days_ago_20 > date('Y-m-d'))){
                              		if ($delete_service != false) {
                              			echo '<td><a data-toggle="modal" data-target="#service_reasonModal" onclick="return loadServiceReasonModal('.$svalue->diagnose_autos_id.','.$svalue->hospital_id.','.$svalue->diagnose_mains_id.','.$svalue->diagnose_services_id.');"><span class="close service_close" style="float:left;color:red;">x</span></td>';
                              		}
                              	}

                              	echo '</tr>';
                              }
                          }
                          echo '
                          </tbody>
                          </table>
                          </td>';
                          if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                          	echo '
                          	<td colspan="1">
                          	<table class="table table-bordered ';
                          	if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                          		echo 'col-lg-3';
                          	} else{
                          		echo 'col-lg-6'; } echo '">
                          		<thead>
                          		<tr>
                          		<th>Name</th>
                          		<th>Price</th>';
                          		if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests"){
                          			echo '
                          			<th>Action</th>';
                          		}
                          		echo '
                          		</tr>
                          		</thead>
                          		<tbody>';
                          		if(!empty($actual_drugs)) {
                              foreach ($actual_drugs as $key => $actual_dvalue) { //print_r($actual_dvalue);
                              	echo '
                              	<tr>
                              	<div class="drug_row">'; 
                              	if($actual_dvalue->diagnose_drug_status == 0){
                              		echo
                              		'<td>'.$actual_dvalue->drug_name;
                              		echo '</td>
                              		<td>';
                              		echo number_format($actual_dvalue->drug_price);
                              	}
                              	else{ 
                              		echo
                              		'<td>';                                    
                                    //echo $actual_dvalue->drug_name; 
                              		echo '<a class="" data-toggle="modal" data-target="#drug_reasonModal" onclick="return drug_note('.$actual_dvalue->diagnose_autod_id.');">'.$actual_dvalue->drug_name.'</a></td>
                              		<td>';
                              		echo number_format($actual_dvalue->drug_price);

                              	}
                              	echo '</td>
                              	</div>';
                              	if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests"){
                              		echo '
                              		<td><a data-toggle="modal" data-target="#drug_reasonModal" onclick="return loadDrugReasonModal('.$dvalue->drug_id.','.$dvalue->hospital_id.','.$dvalue->diagnose_autod_id.','.$dvalue->diagnose_maind_id.')"><span class="close drug_close" style="float:left;color:red;">x</span></a></td>';
                              	}
                              	echo '
                              	</tr>';
                              }
                          }
                          echo '
                          </tbody>
                          </table>
                          </td>
                          <td colspan="1">
                          <table class="table table-bordered ';
                          if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                          	echo 'col-lg-3';
                          } else{
                          	echo 'col-lg-6'; } echo '">
                          	<thead>
                          	<tr>
                          	<th>Name</th>
                          	<th>Price</th>';
                          	if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests"){
                          		echo '
                          		<th>Action</th>';
                          	}
                          	echo '
                          	</tr>
                          	</thead>
                          	<tbody>';
                          	if(!empty($actual_services)) {
                              foreach ($actual_services as $key => $actual_svalue) { //print_r($actual_svalue->diagnose_service_status);
                              	echo '
                              	<tr>
                              	<div class="service_row">';
                              	if($actual_svalue->diagnose_service_status== 0){
                              		echo
                              		'<td>'.$actual_svalue->service_name.'</td>
                              		<td>'.number_format($actual_svalue->service_price).'</td>';
                              	} else{
                              		echo
                              		'<td>';
                                   //print_r($actual_svalue);
                              		echo '<a class="" data-toggle="modal" data-target="#service_reasonModal" onclick="return service_note('.$actual_svalue->diagnose_autos_id.');">'.$actual_svalue->service_name.'</a></td>
                              		<td>'.number_format($actual_svalue->service_price).'</td>';
                              	}
                              	echo '</div>';

                              	if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests"){
                              		echo '<td><a data-toggle="modal" data-target="#service_reasonModal" onclick="return loadServiceReasonModal('.$svalue->diagnose_autos_id.','.$svalue->hospital_id.','.$svalue->diagnose_mains_id.','.$svalue->diagnose_services_id.');"><span class="close service_close" style="float:left;color:red;">x</span></td>';
                              	}

                              	echo '</tr>';
                              }
                          }
                          echo '
                          </tbody>
                          </table>
                          </td>';
                      }
                      echo '
                      </tr>
                      <tr>
                      <td colspan="';
                      if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                      	echo '1';
                      } else{
                      	echo '2'; } echo '" align="right"><h3 class="text-danger">Total Bill: </h3></td>
                      	<td colspan="1" align="left"><h3 class="text-danger">'.number_format($fetched_result["diagnose_total_sum"]).'.00</h3></td>';
                      	if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                      		echo '
                      		<td colspan="1" align="right"><h3 class="text-danger">Total Bill: </h3></td>
                      		<td colspan="1" align="left"><h3 class="text-danger">'.number_format($actual_bill).'.00</h3></td>';
                      		} echo '
                      		</tr>
                      		</tbody>
                      		</table>
                      		</div>
                      		</div></div>';
                      	}
                      	public function fetch_diagnose_id_1()
                      	{
                      		$is_hospital = $this->input->post('hospital');

                      		if ($is_hospital != true || is_null($is_hospital)) {
                      			$role_resources_ids = $this->Xin_model->user_role_resource();

                      			if (in_array("640", $role_resources_ids)) {
                      				$delete_drug = true;
                      			}else{
                      				$delete_drug = false;

                      			}

			// echo "Delete drug: ".$delete_drug;

                      			if (in_array("641", $role_resources_ids)) {
                      				$delete_service = true;
                      			}else{
                      				$delete_service = false;
                      			}
                      		}

                      		$url= $_SERVER['HTTP_REFERER'];

                      		$fetched_result = array();
                      		$did = $this->input->post('id');

                      		$diagnose = $this->Clients_model->get_diagnose($did)->result();

                      		$fetched_result = $this->Clients_model->view_individual_hospital_diagnose_clients($did,$diagnose[0]->is_capitation,$diagnose[0]->diagnose_client_id);		
		// print_R($fetched_result);

                      		$services = $this->Clients_model->read_individual_hospital_diagnose_services($fetched_result["diagnose_id"]);
                      		$drugs = $this->Clients_model->read_individual_hospital_diagnose_drugs($fetched_result["diagnose_id"]);

                      		$actual_drugs = $this->Clients_model->read_individual_hospital_diagnose_actual_drugs($fetched_result["diagnose_id"]);
                      		$actual_services = $this->Clients_model->read_individual_hospital_diagnose_actual_services($fetched_result["diagnose_id"]);

                      		$actual_drug_bill = $this->Clients_model->actual_drugs_sum($did);
                      		$actual_service_bill = $this->Clients_model->actual_service_sum($did); 
                      		$actual_bill= $actual_drug_bill[0]->d_id + $actual_service_bill[0]->s_id;

                      		$hcp = preg_replace('/\s+/', '', $fetched_result["hospital_name"]);
                      		$date = date("Ymd", strtotime($fetched_result["diagnose_date_time"]));

                      		$days_ago_20 = date('Y-m-d', strtotime('+20 days', strtotime($date)));



                      		echo '

                      		<style>
                      		.modal-lg {
                      			width: 85% !important;
                      		}
                      		th.bg-dark.text-center.asd{
                      			width:50%;
                      		}
                      		</style><div class="row"><div class="col-lg-12"><div class="col-lg-4"><h5 class="text-info"><b>Generated Code:</b>'; if(empty($fetched_result["diagnose_generated_code"])) echo "-----"; else echo "HCP-".$hcp."-".$fetched_result["diagnose_generated_code"]."-".$date."-".$fetched_result["diagnose_generated_code"]; 
                      		echo'</h5>
                      		</div>
                      		<div class="col-lg-4 text-center">
                      		<h5 class="text-info"><b>Hospital:</b> '; if(empty($fetched_result["hospital_name"])) echo "-----"; else echo $fetched_result["hospital_name"]; echo'</h5>
                      		</div>
                      		<div class="col-lg-4" style="text-align:right;"> 
                      		<h5 class="text-info"><b>Date:</b> '.$fetched_result["diagnose_date"].'</h5>
                      		</div></div>';
                      		if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests" || $url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests_admin" || $url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests" || $url == "https://care.precioushmo.com/app/admin/Hospital/all_auth_approved" || $url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests_rejected" || $url == "https://care.precioushmo.com/app/admin/Hospital/all_bills_paid" || $url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_bill_requests_rejected" || $url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_bill_requests"){
                      			echo '<div class="col-md-12 text-center">
                      			<h5><a href="javascript:;" style="color:red;font-weight:600;" onclick="return getinfodiv();" id="getinfodiv">Enrollee Profile Summary</a></h5>
                      			</div>
                      			<div class="col-md-12" id="infodiv" style="display:none;">
                      			<div class="col-lg-6"><h5 class="text-info"><b>Phone Number : </b>'; 

                      			$result1q = $this->Clients_model->get_clients_by_id($fetched_result["diagnose_client_id"])->row();
//echo $fetched_result['phone'];

                      			if($result1q && $result1q->ind_family=='individual'){echo $result1q->contact_number;}elseif($result1q && $result1q->ind_family=='family'){echo $result1q->contact_number;}else{$result1qq = $this->Clients_model->get_clients_by_id($fetched_result["client_id"])->row();echo $result1qq->contact_number;}
                      			if($result1q){$sex = $result1q->sex;}else{$sex = $fetched_result['sex'];}

                      			echo'</h5>
                      			</div>
                      			<div class="col-lg-6" style="text-align:right;"> 
                      			<h5 class="text-info"><b>Gender : </b> '.ucfirst($sex).'</h5>
                      			</div>
                      			<div class="clearfix"></div>
                      			<div class="col-lg-6"><h5 class="text-info"><b>Organization : </b>';

                      			$result1q = $this->Clients_model->get_clients_by_id($fetched_result["client_id"])->row();

                      			$organization = $fetched_result['company_name'];
                      			if(trim($organization) ==""){
                      				$organization = $result1q->company_name;
                      			}
                      			$organizations = $this->Clients_model->get_organization_info1($organization);
                      			echo $organizations['name'];
                      			echo'</h5>
                      			</div>
                      			<div class="col-lg-6" style="text-align:right;"> 
                      			<h5 class="text-info"><b>Plan type : </b> ';
                      			$sub_ids = $fetched_result['subscription_ids'];
                      			if(trim($sub_ids) =="")
                      			{
                      				$sub_ids = $result1q->subscription_ids;
                      			}
                      			$sub_ids = $this->Clients_model->get_subscribtionname_info($sub_ids);
                      			echo $sub_ids['plan_name'];
                      			echo '</h5>
                      			</div>
                      			<div class="clearfix"></div>
                      			<div class="col-lg-6"><h5 class="text-info"><b>Relationship : </b>'; 
                      			$result1q = $this->Clients_model->get_clients_by_id($fetched_result["diagnose_client_id"])->row();
                      			if($result1q && $result1q->ind_family == 'individual'){$rela = 'principal';}elseif($result1q && $result1q->ind_family == 'family'){$rela = 'principal';}else{$rela = $fetched_result['relation'];}
                      			echo ucfirst($rela);
                      			echo'</h5>
                      			</div>

                      			</div>';
                      		}
                      		echo '<div class="col-lg-12">

                      		<table class="table" style="padding: 100px; border: 0px;">
                      		<tbody>

                      		<tr>
                      		<td class="col-lg-2" align="left"><h5><b>Enrollee Name:</b></h5></td>
                      		<td align="left">'; if ($fetched_result["is_capitation"] != 1) {
                      			if($fetched_result["diagnose_user_type"] == 'C') { echo $fetched_result["cname"]." ".$fetched_result["clname"]." ".$fetched_result["coname"]; } else {  if ($fetched_result["cname"] != '' && $fetched_result["clname"] != '') { echo $fetched_result["cname"]." ".$fetched_result["clname"]." ".$fetched_result["coname"]; }else{echo $fetched_result["dname"]." ".$fetched_result["dlname"]." ".$fetched_result["doname"];} }

                      		}else{
                      			echo $fetched_result["cap_name"];
                      			} echo '</td>
                      			<td class="col-lg-2" align="right"><h5><b>Diagnose:</b></h5></td>
                      			<td align="left">'.$fetched_result["diagnose_diagnose"].'</td>
                      			</tr>
                      			<tr>
                      			<td class="col-lg-2" align="left"><h5><b>Procedure:</b></h5></td>
                      			<td align="left">'.$fetched_result["diagnose_procedure"].'</td>
                      			<td class="col-lg-2" align="right"><h5><b>Investigation:</b></h5></td>
                      			<td align="left">'.$fetched_result["diagnose_investigation"].'</td>
                      			</tr>
                      			<tr>
                      			<td class="col-lg-2" align="left"><h5><b>Medical:</b></h5></td>
                      			<td colspan="3" align="left">'.$fetched_result["diagnose_medical"].'</td>
                      			</tr>';
                      			if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                      				echo '
                      				<tr>
                      				<th colspan="2" class="bg-primary text-center" style="background-color: #343a40; color: #FFF;">Updated Bill</th>
                      				<th colspan="3" class="text-center bg-primary" style="background-color: #343a40; color: #FFF;">Actual Bill</th>
                      				</tr>';
                      			}
                      			if(($url == "https://care.precioushmo.com/app/admin/Hospital/all_auth_approved"  && $days_ago_20 > date('Y-m-d')))
                      			{
                      				$all_hospital_drugs   =  $this->Training_model->getAll2('xin_hospital_drugs',' hospital_id ='.$did.'    ');

                      				$xin_services_hospital   =  $this->Training_model->getAll2('xin_services_hospital',' hospital_id ='.$did.'    ');


                      				echo '
                      				<tr><th colspan="5"><button name="addmoredrugusaddservice" class="btn btn-success" onclick="return getmoredurd('.$did.');">Add Drugs & Services</button></th><tr><th colspan="5">

                      				<style>.select2.select2-container{width:100% !important;}</style><div class="form-body row" style="display:none;" id="drugsservices">
                      				<form id="frms" name="frms">
                      				<div class="col-md-12" style="margin-top:10px" >
                      				<div class="col-md-6">
                      				<div class="form-group">

                      				<label for="services">Select Services</label>
                      				<select class="form-control" name="diagnose_services_more[]" data-plugin="select_hrm" data-placeholder="Select Services" multiple id="servicesmodel"> 

                      				<option value="">Select Services</option>';

                      				if (isset($xin_services_hospital) and !empty($xin_services_hospital)) { 


                      					foreach ($xin_services_hospital as $key => $value): 
                      						echo '<option value="'.$value->id.' ['.$value->service_price.']">'.$value->service_name.' (₦'.$value->service_price.')</option>';

                      					endforeach;
                      				}

                      				echo '</select>
                      				</div>

                      				</div>
                      				<div class="col-md-6">
                      				<div class="row form-group" id="divmore1">
                      				<div class="col-md-7">
                      				<label for="last_name">Select Drug</label>
                      				<select class="form-control" name="diagnose_drugs_more[]" data-plugin="select_hrm" data-placeholder="Select Drugs" id="drugsmore1"> 

                      				<option value="">Select Drug</option>';

                      				if (isset($all_hospital_drugs) and !empty($all_hospital_drugs)) {


                      					foreach ($all_hospital_drugs as $key => $value):


                      						echo '<option value="'.$value->drug_id.'['.$value->drug_price.']">'.$value->drug_name.'(₦'.$value->drug_price.')'.'</option>';

                      					endforeach;
                      				}

                      				echo '<select>
                      				</div>
                      				<div class="col-md-3">
                      				<label for="last_name">Quantity</label>
                      				<input type="number" name="quantitymore[]" id="quantitymore1" class="form-control" min="1" value="1">
                      				</div>
                      				<div class="col-md-2">
                      				<label for="last_name" class="padding"></label>
                      				<input type="button" name="addmore" id="addmoremore" onclick="return getaddmoremore(2,'.$did.')" value="+" class="btn btn-success">

                      				</div>

                      				</div>


                      				</div>

                      				</div>
                      				</br>
                      				<div class="col-md-12">
                      				<input type="hidden" name="hostel_idss" id="hostel_idss" value="'.$did.'">

                      				<button type="button" class="btn btn-success" onclick="return adddrugsservice();">UPDATE</button></div>
                      				</form>
                      				</div>
                      				</th></tr>
                      				';
                      			}
                      			echo '
                      			<tr>
                      			<th colspan="';
                      			if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                      				echo '1';
                      			} else{
                      				echo '2'; } echo '" class="bg-dark text-center asd" style="background-color: #343a40; color: #FFF;">DRUGS</th>
                      				<th colspan="';
                      				if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                      					echo '1';
                      				} else{
                      					echo '2'; } echo '" class="text-center bg-dark" style="background-color: #343a40; color: #FFF;">SERVICES</th>';
                      					if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                      						echo '
                      						<th colspan="1" class="bg-dark text-center asd" style="background-color: #343a40; color: #FFF;">DRUGS</th>
                      						<th colspan="1" class="text-center bg-dark" style="background-color: #343a40; color: #FFF;">SERVICES</th>';
                      					}
                      					echo '
                      					</tr>
                      					<tr>
                      					<td colspan="';
                      					if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                      						echo '1';
                      					} else{
                      						echo '2'; } echo '">
                      						<table class="table table-bordered ';
                      						if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                      							echo 'col-lg-3';
                      						} else{
                      							echo 'col-lg-6'; } echo '">
                      							<thead>
                      							<tr>
                      							<th>Name</th><th>Quantity</th>
                      							<th>Price</th>';
                      							if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests" || ($url == "https://care.precioushmo.com/app/admin/Hospital/all_auth_approved" && $days_ago_20 > date('Y-m-d'))){
                      								if ($delete_drug != false) {
                      									echo '<th>Action</th>';
                      								}
                      							}
                      							echo '
                      							</tr>
                      							</thead>
                      							<tbody>';
                      							if(!empty($drugs)) {
                      								foreach ($drugs as $key => $dvalue) { 
                      									echo '
                      									<tr>
                      									<div class="drug_row">
                      									<td>'.$dvalue->drug_name.'</td>
                      									<td>'.$dvalue->diagnose_quantity.'</td>
                      									<td>'.$dvalue->diagnose_quantity*$dvalue->drug_price.'</td>
                      									</div>';
                      									if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests" || ($url == "https://care.precioushmo.com/app/admin/Hospital/all_auth_approved" && $days_ago_20 > date('Y-m-d')))
                      									{

                      										if ($delete_drug != false)
                      										{
                      											echo '<td><a data-toggle="modal" data-target="#drug_reasonModal" onclick="return loadDrugReasonModal('.$dvalue->drug_id.','.$dvalue->hospital_id.','.$dvalue->diagnose_autod_id.','.$dvalue->diagnose_maind_id.')"><span class="close drug_close" style="float:left;color:red;">x</span></a></td>';
                      										}
                      									}
                      									echo '
                      									</tr>';
                      								}
                      							}
                      							echo '
                      							</tbody>
                      							</table>
                      							</td>
                      							<td colspan="';
                      							if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                      								echo '1';
                      							} else{
                      								echo '2'; } echo '">
                      								<table class="table table-bordered ';
                      								if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                      									echo 'col-lg-3';
                      								} else{
                      									echo 'col-lg-6'; } echo '">
                      									<thead>
                      									<tr>
                      									<th>Name</th>
                      									<th>Price</th>';
                      									if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests"  || ($url == "https://care.precioushmo.com/app/admin/Hospital/all_auth_approved"  && $days_ago_20 > date('Y-m-d'))){
                      										if ($delete_service != false) {
                      											echo '<th>Action</th>';
                      										}
                      									}
                      									echo '
                      									</tr>
                      									</thead>
                      									<tbody>';
                      									if(!empty($services)) {
                              foreach ($services as $key => $svalue) { //print_r($svalue); //die();
                              	echo '
                              	<tr>
                              	<div class="service_row">
                              	<td>'.$svalue->service_name.'</td>
                              	<td>'.number_format($svalue->service_price).'</td>

                              	</div>';

                              	if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests" || ($url == "https://care.precioushmo.com/app/admin/Hospital/all_auth_approved"  && $days_ago_20 > date('Y-m-d'))){
                              		if ($delete_service != false) {
                              			echo '<td><a data-toggle="modal" data-target="#service_reasonModal" onclick="return loadServiceReasonModal('.$svalue->diagnose_autos_id.','.$svalue->hospital_id.','.$svalue->diagnose_mains_id.','.$svalue->diagnose_services_id.');"><span class="close service_close" style="float:left;color:red;">x</span></td>';
                              		}
                              	}

                              	echo '</tr>';
                              }
                          }
                          echo '
                          </tbody>
                          </table>
                          </td>';
                          if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                          	echo '
                          	<td colspan="1">
                          	<table class="table table-bordered ';
                          	if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                          		echo 'col-lg-3';
                          	} else{
                          		echo 'col-lg-6'; } echo '">
                          		<thead>
                          		<tr>
                          		<th>Name</th>
                          		<th>Price</th>';
                          		if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests"){
                          			echo '
                          			<th>Action</th>';
                          		}
                          		echo '
                          		</tr>
                          		</thead>
                          		<tbody>';
                          		if(!empty($actual_drugs)) {
                              foreach ($actual_drugs as $key => $actual_dvalue) { //print_r($actual_dvalue);
                              	echo '
                              	<tr>
                              	<div class="drug_row">'; 
                              	if($actual_dvalue->diagnose_drug_status == 0){
                              		echo
                              		'<td>'.$actual_dvalue->drug_name;
                              		echo '</td>
                              		<td>';
                              		echo number_format($actual_dvalue->drug_price);
                              	}
                              	else{ 
                              		echo
                              		'<td>';                                    
                                    //echo $actual_dvalue->drug_name; 
                              		echo '<a class="" data-toggle="modal" data-target="#drug_reasonModal" onclick="return drug_note('.$actual_dvalue->diagnose_autod_id.');">'.$actual_dvalue->drug_name.'</a></td>
                              		<td>';
                              		echo number_format($actual_dvalue->drug_price);

                              	}
                              	echo '</td>
                              	</div>';
                              	if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests"){
                              		echo '
                              		<td><a data-toggle="modal" data-target="#drug_reasonModal" onclick="return loadDrugReasonModal('.$dvalue->drug_id.','.$dvalue->hospital_id.','.$dvalue->diagnose_autod_id.','.$dvalue->diagnose_maind_id.')"><span class="close drug_close" style="float:left;color:red;">x</span></a></td>';
                              	}
                              	echo '
                              	</tr>';
                              }
                          }
                          echo '
                          </tbody>
                          </table>
                          </td>
                          <td colspan="1">
                          <table class="table table-bordered ';
                          if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                          	echo 'col-lg-3';
                          } else{
                          	echo 'col-lg-6'; } echo '">
                          	<thead>
                          	<tr>
                          	<th>Name</th>
                          	<th>Price</th>';
                          	if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests"){
                          		echo '
                          		<th>Action</th>';
                          	}
                          	echo '
                          	</tr>
                          	</thead>
                          	<tbody>';
                          	if(!empty($actual_services)) {
                              foreach ($actual_services as $key => $actual_svalue) { //print_r($actual_svalue->diagnose_service_status);
                              	echo '
                              	<tr>
                              	<div class="service_row">';
                              	if($actual_svalue->diagnose_service_status== 0){
                              		echo
                              		'<td>'.$actual_svalue->service_name.'</td>
                              		<td>'.number_format($actual_svalue->service_price).'</td>';
                              	} else{
                              		echo
                              		'<td>';
                                   //print_r($actual_svalue);
                              		echo '<a class="" data-toggle="modal" data-target="#service_reasonModal" onclick="return service_note('.$actual_svalue->diagnose_autos_id.');">'.$actual_svalue->service_name.'</a></td>
                              		<td>'.number_format($actual_svalue->service_price).'</td>';
                              	}
                              	echo '</div>';

                              	if($url == "https://care.precioushmo.com/app/admin/Hospital/diagnose_hospital_clients_requests"){
                              		echo '<td><a data-toggle="modal" data-target="#service_reasonModal" onclick="return loadServiceReasonModal('.$svalue->diagnose_autos_id.','.$svalue->hospital_id.','.$svalue->diagnose_mains_id.','.$svalue->diagnose_services_id.');"><span class="close service_close" style="float:left;color:red;">x</span></td>';
                              	}

                              	echo '</tr>';
                              }
                          }
                          echo '
                          </tbody>
                          </table>
                          </td>';
                      }
                      echo '
                      </tr>
                      <tr>
                      <td colspan="';
                      if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                      	echo '1';
                      } else{
                      	echo '2'; } echo '" align="right"><h3 class="text-danger">Total Bill: </h3></td>
                      	<td colspan="1" align="left"><h3 class="text-danger">'.number_format($fetched_result["diagnose_total_sum"]).'.00</h3></td>';
                      	if($url == "https://care.precioushmo.com/app/admin/Hospital/adjusted_requests"){
                      		echo '
                      		<td colspan="1" align="right"><h3 class="text-danger">Total Bill: </h3></td>
                      		<td colspan="1" align="left"><h3 class="text-danger">'.number_format($actual_bill).'.00</h3></td>';
                      		} echo '
                      		</tr>
                      		</tbody>
                      		</table>
                      		</div>
                      		</div></div>';
                      	}
                      	public function fetch_service_note()
                      	{
                      		$id = $this->input->post('id');
                      		$fetch_service_notes= $this->Clients_model->fetch_service_note($id);
                      		print_r($fetch_service_notes[0]->notes); 
                      	}
                      	public function fetch_drug_note()
                      	{
                      		$id = $this->input->post('id');
                      		$fetch_drug_notes = $this->Clients_model->fetch_drug_note($id);
                      		print_r($fetch_drug_notes[0]->notes); 
                      	}
                      	public function fetch_diagnose_all()
                      	{
                      		$fetched_result = array();
		// $did = $this->input->post('id');
		// $did = explode(",", $did);
		// print_r($did);die;
                      		$from = $this->input->post('from');
                      		$to = $this->input->post('to');
                      		$hid = $this->input->post('hid');

                      		$clients = $this->Clients_model->get_filter_result_of_hospital($hid,$from,$to);
                      		$did = array();
		// print_r($clients);die;
                      		foreach($clients as $client)
                      		{
                      			$did[] = $client->diagnose_id;
                      		} 

		// Hospital info
                      		$hospital = $this->Clients_model->read_hospital_info($hid);


                      		$html = "
                      		<h3>Showing Data of ".$hospital[0]->hospital_name." from ".$from." to ".$to."</h3>
                      		";

                      		$html .= "
                      		<table class=\"table table-striped\">
                      		<thead>
                      		<tr>
                      		<th>No</th>
                      		<th>Name</th>
                      		<th>Diagnose</th>
                      		<th>Date Encounter</th>
                      		<th>Bill</th>
                      		</tr>
                      		</thead>
                      		<tbody>";


                      		$no = 1;
                      		$total = array();
                      		foreach ($did as $d) {
                      			$fetched_result = $this->Clients_model->view_individual_hospital_diagnose_clients($d);
                      			$html .= 
                      			"<tr>
                      			<td>".$no."</td>
                      			<td>";
                      			if($fetched_result["diagnose_user_type"] == 'C') 
                      			{ 
                      				$html .= $fetched_result["cname"]." ".$fetched_result["clname"]." ".$fetched_result["coname"]; 
                      			} else 
                      			{ 
                      				$html .= $fetched_result["dname"]." ".$fetched_result["dlname"]." ".$fetched_result["doname"]; 
                      			}
                      			$html .=       "</td>

                      			<td>".$fetched_result['diagnose_diagnose']."</td>
                      			<td>".$fetched_result['diagnose_date']."</td>
                      			<td>".number_format($fetched_result["diagnose_total_sum"])."</td>
                      			</tr>";
                      			$no++;
                      			array_push($total, $fetched_result["diagnose_total_sum"]);
                      		}

                      		$html .= "

                      		<tr>
                      		<td colspan=\"5\" align=\"center\">Total Bills: <b>".number_format(array_sum($total))."</b></td>
                      		</tr>	
                      		</tbody>
                      		</table>
                      		";

                      		$did = implode("-", $did);

                      		$html .= "
                      		<form action=\"".site_url('admin/accounting/pdf')."\" method=\"post\" target=\"_blank\">
                      		<input type=\"text\" name=\"did\" value=\"".$did."\" hidden>
                      		<input type=\"text\" name=\"hospital\" value=\"".$hospital[0]->hospital_name."\" hidden>
                      		<input type=\"text\" name=\"from\" value=\"".$from."\" hidden>
                      		<input type=\"text\" name=\"to\" value=\"".$to."\" hidden>

                      		<button type=\"submit\" class=\"btn btn-default\">Export To PDF</button>
                      		</form>

                      		";

                      		echo $html;

                      	}



                      	public function fetch_reason()
                      	{
                      		$fetched_result = array();
                      		$did = $this->input->post('id');
                      		$for = $this->input->post('type');

                      		$fetched_result = $this->Clients_model->view_individual_hospital_diagnose_clients($did);
		// echo $this->db->last_query();
                      		if($for == 1) {
                      			$arr = explode(',',$fetched_result["diagnose_reject_reason"]);
                      			$n = 1;
                      			foreach($arr as $ar) {
                      				echo '<h5><b>Rejection Reason '.$n++.':</b></h5><p>'.$ar.'</p><br />';
                      			}
                      		} else if($for == 2) {
                      			$arr = explode(',',$fetched_result["diagnose_bill_reject_reason"]);
                      			$n = 1;
                      			foreach($arr as $ar) {
                      				echo '<h5><b>Rejection Reason '.$n++.':</b></h5><p>'.$ar.'</p><br />';
                      			}
                      		} else {
                      			echo '<p>'.$fetched_result["diagnose_bill_note"].'</p>';
                      		}
                      	}

                      	public function get_dob()
                      	{
                      		$id = $this->input->post('id');

		// echo strlen($id);die;
		// if(strlen($id) < 18){
		// 	$id = substr($id, -1);
		// 	$dob = $this->Clients_model->get_dob($id,'C')->result();	
		// }else{
		// 	$dob = $this->Clients_model->get_dob($id,'D')->result();	
		// }

                      		$arr = explode(":", $id);
                      		$id = $arr[1];
		//print_r($arr);
                      		if (count($arr) > 2) {
			$arr2 = explode("/", $arr[0]);//explode("-", $arr[0]);
			if (count($arr2) > 3) {
				$dob = $this->Clients_model->get_dob_capitation($id,'d')->result();
				$dob = explode("/", $dob[0]->dob);
				$dob = $dob[2].$dob[1].$dob[0];
			}else{
				// echo "yes";
				$dob = $this->Clients_model->get_dob_capitation($id,'p')->result();
				$dob = explode("/", $dob[0]->dob);
				$dob = $dob[2].$dob[1].$dob[0];
			}
		}else{
			$arr2 = explode("/", $arr[0]);//explode("-", $arr[0]);
			
			if (count($arr2) > 3) {
				$dob = $this->Clients_model->get_dob($id,'D')->result();
				$dob = $dob[0]->dob;
			}else{
				//echo $id;
				//echo '=>';
				$dob = $this->Clients_model->get_dob($id,'C')->result();
				//print_r($dob);
				$dob = $dob[0]->dob;
			}
		}

		echo $dob;
	}

	public function ticket_list() 
	{ 

		$session = $this->session->userdata('hospital_name');

		// print_r($session);die;

		if(empty($session)){ 

			redirect('hospital/auth/');

		}

		$delete = $this->input->post('_method');

		if (isset($delete)) {
			$id = $this->input->post('id');
			// echo  $id;die;
			$delete = $this->Xin_model->delete_ticket($id);

			// $this->session->set_flashdata('success-ch','Successfully deleted');
			redirect('hospital/clients/ticket_list');

		}

		// if(isset($_GET['hospital_id']) and !empty($_GET['hospital_id']))
		// {
		// 	$hospital_id = $_GET['hospital_id'];

		// 	$data_to_insert = array(
		// 		'hospital_id'  => $hospital_id,
		// 		'client_id'    => $session['client_id'],
		// 		'created_on'   => date("Y-m-d h:i:s"),
		// 	);



		// 	$this->Training_model->insertDataTB('xin_change_hospital_request',$data_to_insert);

		// 	$this->session->set_flashdata('success','Hospital change request has been submitted successfully.');
		// 	redirect($_SERVER['HTTP_REFERER']);
		// }
		
		// $data['result'] = $this->Clients_model->read_client_info($session['client_id']);

		$data['breadcrumbs'] = $this->lang->line('header_my_profile'); 


		$data['hr_ticket_list'] = 'active';

		$data['title'] =  $this->Xin_model->site_title();

		$data['path_url'] = 'profile_client';

		$data['all_ticket'] = $this->Xin_model->get_hospital_ticket($session['hospital_id']);
		// echo $this->db->last_query();
		// print_r($data['all_ticket']);die;


		if(!empty($session)){ 

			$data['subview'] = $this->load->view("hospital/client/ticket_list", $data, TRUE);

			$this->load->view('hospital/layout/layout_main', $data); //page load

		} else {

			redirect('hopsital/auth/');

		}


	}

	public function add_ticket() 
	{

		$session = $this->session->userdata('hospital_name');

		// if($this->input->post('add_type')=='ticket') {		

		/* Define return | here result is used to return user data and error for error message */

		$Return = array('result'=>'', 'error'=>'', 'csrf_hash'=>'');

		$Return['csrf_hash'] = $this->security->get_csrf_hash();



		/* Server side PHP input validation */		

		if($this->input->post('subject')==='') {

			$Return['error'] = $this->lang->line('xin_employee_error_subject');

		} 

		$description = $this->input->post('description');

		$qt_description = htmlspecialchars(addslashes($description), ENT_QUOTES);		

		if($Return['error']!=''){

			$this->output($Return);

		}

		

		$ticket_code = $this->Xin_model->generate_random_string();

		$module_attributes = $this->Custom_fields_model->tickets_hrsale_module_attributes();

		$count_module_attributes = $this->Custom_fields_model->count_tickets_module_attributes();	

		$i=1;

		if($count_module_attributes > 0){

			foreach($module_attributes as $mattribute) {

				if($mattribute->validation == 1){

					if($i!=1) {

					} else if($this->input->post($mattribute->attribute)=='') {

						$Return['error'] = $this->lang->line('xin_hrsale_custom_field_the').' '.$mattribute->attribute_label.' '.$this->lang->line('xin_hrsale_custom_field_is_required');

					}

				}

			}		

			if($Return['error']!=''){

				$this->output($Return);

			}	

		}

		$data = array(

			'ticket_code' => $ticket_code,

			'subject' => $this->input->post('subject'),

			'company_id' => null,

			'employee_id' => $session['hospital_id'],

			'description' => $qt_description,

			'ticket_status' => '1',

			'is_notify' => '1',

			'ticket_priority' => null,

			'created_at' => date('d-m-Y h:i:s'),

			'is_hc' => 2

		);

		$iresult = $this->Tickets_model->add($data);

		if ($iresult) {

			$Return['result'] = $this->lang->line('xin_success_ticket_created');

			$id = $iresult;

			if($count_module_attributes > 0){

				foreach($module_attributes as $mattribute) {

				 	/*$attr_data = array(

						'user_id' => $iresult,

						'module_attributes_id' => $mattribute->custom_field_id,

						'attribute_value' => $this->input->post($mattribute->attribute),

						'created_at' => date('Y-m-d h:i:s')

					);

					$this->Custom_fields_model->add_values($attr_data);*/

					if($mattribute->attribute_type == 'fileupload'){

						if($_FILES[$mattribute->attribute]['size'] != 0) {

							if(is_uploaded_file($_FILES[$mattribute->attribute]['tmp_name'])) {

							//checking image type

								$allowed =  array('png','jpg','jpeg','pdf','gif','xls','doc','xlsx','docx');

								$filename = $_FILES[$mattribute->attribute]['name'];

								$ext = pathinfo($filename, PATHINFO_EXTENSION);

								

								if(in_array($ext,$allowed)){

									$tmp_name = $_FILES[$mattribute->attribute]["tmp_name"];

									$profile = "uploads/custom_files/";

									$set_img = base_url()."uploads/custom_files/";

									// basename() may prevent filesystem traversal attacks;

									// further validation/sanitation of the filename may be appropriate

									$name = basename($_FILES[$mattribute->attribute]["name"]);

									$newfilename = 'custom_file_'.round(microtime(true)).'.'.$ext;

									move_uploaded_file($tmp_name, $profile.$newfilename);

									$fname = $newfilename;	

								}

								$iattr_data = array(

									'user_id' => $id,

									'module_attributes_id' => $mattribute->custom_field_id,

									'attribute_value' => $fname,

									'created_at' => date('Y-m-d h:i:s')

								);

								$this->Custom_fields_model->add_values($iattr_data);

							}

						} else {

							$iattr_data = array(

								'user_id' => $id,

								'module_attributes_id' => $mattribute->custom_field_id,

								'attribute_value' => '',

								'created_at' => date('Y-m-d h:i:s')

							);

							$this->Custom_fields_model->add_values($iattr_data);

						}

					} else if($mattribute->attribute_type == 'multiselect') {

						$multisel_val = $this->input->post($mattribute->attribute);

						if(!empty($multisel_val)){

							$newdata = implode(',', $this->input->post($mattribute->attribute));

							$iattr_data = array(

								'user_id' => $id,

								'module_attributes_id' => $mattribute->custom_field_id,

								'attribute_value' => $newdata,

								'created_at' => date('Y-m-d h:i:s')

							);

							$this->Custom_fields_model->add_values($iattr_data);

						}

					} else {

						if($this->input->post($mattribute->attribute) == ''){

							$file_val = '';

						} else {

							$file_val = $this->input->post($mattribute->attribute);

						}

						$iattr_data = array(

							'user_id' => $id,

							'module_attributes_id' => $mattribute->custom_field_id,

							'attribute_value' => $file_val,

							'created_at' => date('Y-m-d h:i:s')

						);

						$this->Custom_fields_model->add_values($iattr_data);

					}

					/*$attr_orig_value = $this->Custom_fields_model->read_hrsale_module_attributes_values($result,$mattribute->custom_field_id);

					if($attr_orig_value->module_attributes_id != $mattribute->custom_field_id) {

						$this->Custom_fields_model->add_values($attr_data);

					}*/

				}

			}

			//get setting info 

			$setting = $this->Xin_model->read_setting_info(1);

			if($setting[0]->enable_email_notification == 'yes') {



			// 	$this->email->set_mailtype("html");

			// 	//get company info

			// 	$cinfo = $this->Xin_model->read_company_setting_info(1);

			// 	//get email template

			// 	$template = $this->Xin_model->read_email_template(15);

			// 	//get employee info

			// 	$user_info = $this->Xin_model->read_user_info($this->input->post('employee_id'));

				

			// 	$full_name = $user_info[0]->first_name.' '.$user_info[0]->last_name;



			// 	$subject = str_replace('{var ticket_code}',$ticket_code,$template[0]->subject);

			// 	$logo = base_url().'uploads/logo/signin/'.$cinfo[0]->sign_in_logo;

				

			// 	$message = '

			// <div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;padding: 20px;">

			// <img src="'.$logo.'" title="'.$cinfo[0]->company_name.'"><br>'.str_replace(array("{var site_name}","{var site_url}","{var ticket_code}"),array($cinfo[0]->company_name,site_url(),$ticket_code),htmlspecialchars_decode(stripslashes($template[0]->message))).'</div>';

				

			// 	$this->email->from($user_info[0]->email, $full_name);

			// 	$this->email->to($cinfo[0]->email);

				

			// 	$this->email->subject($subject);

			// 	$this->email->message($message);

				

			// 	$this->email->send();

			}		

		} else {

			$Return['error'] = $this->lang->line('xin_error_msg');

		}

		$this->output($Return);

		exit;

	}
	public function addmoredivemore(){
		$value1 = $this->input->post('value');
		$var = $this->session->userdata;	
		$all_hospital_drugs =  $this->Training_model->getAll2('xin_hospital_drugs',' hospital_id ='.$var['hospital_id']['hospital_id'].'    ');
		
		$output = '<div class="row form-group" id="divmore'.$value1.'">';
		$output .='<div class="col-md-7">
		<label for="last_name">Select Drugs</label>
		<select class="form-control select2" name="diagnose_drugs_more[]" data-plugin="select_hrm" data-placeholder="Select Drugs" id="drugsmore'.$value1.'"> 

		<option value="">Select Drugs</option>';


		if (isset($all_hospital_drugs) and !empty($all_hospital_drugs)) {


			foreach ($all_hospital_drugs as $key => $value):

				$share = $value->drug_id.'['.$value->drug_price.']'; 

				$share1 = $value->drug_name.'(₦'.$value->drug_price.')';


				$output .='<option value="'.$share.'">'.$share1.'</option>';

			endforeach;
		}

		$output .='</select>
		</div>
		<div class="col-md-3">
		<label for="last_name">Quantity</label>
		<input type="number" name="quantitymore[]" id="quantitymore'.$value1.'" class="form-control" min="1" value="1">
		</div>
		<div class="col-md-2">
		<label for="last_name" class="padding"></label>
		<input type="button" name="removemore" id="removemoremore'.$value1.'" onclick="return removedivmore('.$value1.')" value="-" class="btn btn-warning">
		</div></div>';
		echo $output;
	}
	public function addmoredivemore1(){
		$value1 = $this->input->post('value');
		$var =$this->input->post('hospitalids');;	
		$all_hospital_drugs =  $this->Training_model->getAll2('xin_hospital_drugs',' hospital_id ='.$var.'    ');
		
		$output = '<div class="row form-group" id="divmore'.$value1.'">';
		$output .='<div class="col-md-7">
		<label for="last_name">Select Drugs</label>
		<select class="form-control select2" name="diagnose_drugs_more[]" data-plugin="select_hrm" data-placeholder="Select Drugs" id="drugsmore'.$value1.'"> 

		<option value="">Select Drugs</option>';


		if (isset($all_hospital_drugs) and !empty($all_hospital_drugs)) {


			foreach ($all_hospital_drugs as $key => $value):

				$share = $value->drug_id.'['.$value->drug_price.']'; 

				$share1 = $value->drug_name.'(₦'.$value->drug_price.')';


				$output .='<option value="'.$share.'">'.$share1.'</option>';

			endforeach;
		}

		$output .='</select>
		</div>
		<div class="col-md-3">
		<label for="last_name">Quantity</label>
		<input type="number" name="quantitymore[]" id="quantitymore'.$value1.'" class="form-control" min="1" value="1">
		</div>
		<div class="col-md-2">
		<label for="last_name" class="padding"></label>
		<input type="button" name="removemore" id="removemoremore'.$value1.'" onclick="return removedivmore('.$value1.')" value="-" class="btn btn-warning">
		</div></div>';
		echo $output;
	}
	public function addmoredive(){
		$value1 = $this->input->post('value');
		$var = $this->session->userdata;	
		$all_hospital_drugs =  $this->Training_model->getAll2('xin_hospital_drugs',' hospital_id ='.$var['hospital_id']['hospital_id'].'    ');
		
		$output = '<div class="row" id="div'.$value1.'">';
		$output .='<div class="col-md-8">
		<label for="last_name">Select Drugs</label>
		<select class="form-control select2 select4" name="diagnose_drugs[]" data-plugin="select_hrm" data-placeholder="Select Drugs" id="drugs'.$value1.'"> 

		<option value="">Select Drugs</option>';


		if (isset($all_hospital_drugs) and !empty($all_hospital_drugs)) {


			foreach ($all_hospital_drugs as $key => $value):

				$share = $value->drug_id.'['.$value->drug_price.']'; 

				$share1 = $value->drug_name.'(₦'.$value->drug_price.')';


				$output .='<option value="'.$share.'">'.$share1.'</option>';

			endforeach;
		}

		$output .='</select>
		</div>
		<div class="col-md-2">
		<label for="last_name">Quantity</label>
		<input type="number" name="quantity[]" id="quantity'.$value1.'" class="form-control" min="1" value="1">
		</div>
		<div class="col-md-2">
		<label for="last_name" class="padding"></label>
		<input type="button" name="addmore" id="removemore'.$value1.'" onclick="return removediv('.$value1.')" value="-" class="btn btn-warning">
		</div></div>';
		echo $output;
	}
	public function all_bills_paid() 
	{

		$session = $this->session->userdata('hospital_name');

		if(empty($session)){  
			redirect('hospital/auth'); 
		}

		$system = $this->Xin_model->read_setting_info(1);

		if($system[0]->module_training!='true'){

			redirect('hospital/dashboard');

		}

		$data['title'] =  'All Bills Paid | '.$this->Xin_model->site_title();

		$data['all_requests']  =  $this->Training_model->getAll2('xin_change_hospital_request',' hospital_id = '.$session['hospital_id'].' AND status = \'approved\' order by created_on desc');

		$data['breadcrumbs'] = 'All Providers Bills Process';

		$data['path_url'] = 'training';
		$nothing = "";
		if($this->input->post('from_date')) {
			$from = $this->input->post('from_date');
			$to = $this->input->post('to_date');
			$data['xin_diagnose_clients'] = $this->Clients_model->get_filter_result_of_hospital($session['hospital_id'],$from,$to);
			$data['query'] = $this->db->last_query();
		} else {
			$data['xin_diagnose_clients'] = $this->Clients_model->read_individual_hospital_diagnose_clients($session['hospital_id'],'');
			// echo "asd";die;
		}
			// echo $this->db->last_query();die;

		if(!empty($session)){ 

			$data['subview'] = $this->load->view("hospital/client/all_bills_paid", $data, TRUE);

				$this->load->view('hospital/layout/layout_main', $data); //page load

			} else {

				redirect('admin/');

			}
		// $role_resources_ids = $this->Xin_model->user_role_resource();

		// if(in_array('54',$role_resources_ids)) {


		// } else {

		// 	redirect('admin/dashboard');

		// }

		}
	}