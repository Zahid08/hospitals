<?php

 /**

 * NOTICE OF LICENSE

 *

 * This source file is subject to the HRSALE License

 * that is bundled with this package in the file license.txt.

 * It is also available through the world-wide-web at this URL:

 * http://www.hrsale.com/license.txt

 * If you did not receive a copy of the license and are unable to

 * obtain it through the world-wide-web, please send an email

 * to hrsalesoft@gmail.com so we can send you a copy immediately.

 *

 * @author   HRSALE

 * @author-email  hrsalesoft@gmail.com

 * @copyright  Copyright © hrsale.com. All Rights Reserved

 */

defined('BASEPATH') OR exit('No direct script access allowed');



class Dashboard extends MY_Controller {

	

	public function __construct()

     {

          parent::__construct();

          //load the models

          $this->load->model('Login_model');

		  $this->load->model('Designation_model');

		  $this->load->model('Department_model');

		  $this->load->model('Employees_model');

		  $this->load->model('Xin_model');

		  $this->load->model('Exin_model');

		  $this->load->model('Expense_model');

		  $this->load->model('Timesheet_model');

		  $this->load->model('Travel_model');

		  $this->load->model('Training_model');

		  $this->load->model('Project_model');

		  $this->load->model('Job_post_model');

		  $this->load->model('Goal_tracking_model');

		  $this->load->model('Events_model');

		  $this->load->model('Meetings_model');

		  $this->load->model('Announcement_model');

		  $this->load->model('Clients_model');

		  $this->load->model('Invoices_model');

     }

	

	/*Function to set JSON output*/

	public function output($Return=array()){

		/*Set response header*/

		header("Access-Control-Allow-Origin: *");

		header("Content-Type: application/json; charset=UTF-8");

		/*Final JSON response*/

		exit(json_encode($Return));

	} 

	

	public function index()

	{

		$session = $this->session->userdata('hospital_name');

		if(empty($session)){ 

			redirect('hospital/auth/');

		}

		$hid = $this->session->userdata;
		$hospitalinfo = $this->Clients_model->read_hospital_info($session['hospital_id']);

		$data = array(

			'title' => $this->Xin_model->site_title(),

			'path_url' => 'dashboard',

			'hospital_name' => $hospitalinfo[0]->hospital_name,

			);

		$data['total_clients'] = $this->Clients_model->total_clients($hid['hospital_id']['hospital_id']);
		$data['total_dependants'] = $this->Clients_model->total_dependants($hid['hospital_id']['hospital_id']);
		$data['total_auths'] = $this->Clients_model->total_auths($hid['hospital_id']['hospital_id']);
		$data['total_bills'] = $this->Clients_model->total_bills($hid['hospital_id']['hospital_id']);
		$hospital = $this->Clients_model->get_hospital_info($hid['hospital_id']['hospital_id'])->result();
		$data['total_capitation'] = $this->Clients_model->get_all_capitation_hospital($hospital[0]->hcp_code)->num_rows();

		$data['subview'] = $this->load->view('hospital/dashboard/index', $data, TRUE);
		// echo "<pre>";
		// echo "string";
		// die();
		$this->load->view('hospital/layout/layout_main', $data); //page load

	}

	

	// set new language

	public function set_language($language = "") {

        

        $language = ($language != "") ? $language : "english";

        $this->session->set_userdata('site_lang', $language);

        redirect($_SERVER['HTTP_REFERER']);

        

    }

}

