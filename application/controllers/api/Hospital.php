<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Hospital extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api/MyModel');
    }


    function testingServer()
    {
        echo "this is filezella testing";
    }

    public function index()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->hospital_all();
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function detail($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(4) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->hospital_detail_data($id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function create_request()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $params = json_decode(file_get_contents('php://input'), TRUE);
                    $params['diagnose_status'] = 0; // for get request status
                    if ($params['diagnose_hospital_id'] == "" || $params['diagnose_client_id'] == "" || $params['diagnose_diagnose'] == ""
                        || $params['diagnose_procedure'] == "" || $params['diagnose_investigation'] == "" || $params['diagnose_medical'] == ""
                        || $params['diagnose_total_sum'] == "") {
                        $respStatus = 400;
                        $resp = array('status' => 400, 'message' => 'diagnose can\'t empty');
                    } else {
                        $resp = $this->MyModel->hospital_request_create_data($params);
                    }
                    json_output($respStatus, $resp);
                }
            }
        }
    }

    public function update_request()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $params = json_decode(file_get_contents('php://input'), TRUE);
                    $params['diagnose_status'] = 0; // for get request status
                    if ($params['diagnose_hospital_id'] == "" || $params['diagnose_client_id'] == "" || $params['diagnose_diagnose'] == ""
                        || $params['diagnose_procedure'] == "" || $params['diagnose_investigation'] == "" || $params['diagnose_medical'] == ""
                        || $params['diagnose_total_sum'] == "") {
                        $respStatus = 400;
                        $resp = array('status' => 400, 'message' => 'diagnose can\'t empty');
                    } else {
                        $resp = $this->MyModel->hospital_request_create_data($params);
                    }
                    json_output($respStatus, $resp);
                }
            }
        }
    }

    public function detail_request($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(4) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->hospital_request_client_get_data($id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function total_bill($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->get_bill_status($id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function update($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'PUT' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE) {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $params = json_decode(file_get_contents('php://input'), TRUE);
                    $params['last_modified'] = date('Y-m-d H:i:s');
                    if ($params['name'] == "" || $params['email'] == "") {
                        $respStatus = 400;
                        $resp = array('status' => 400, 'message' => 'Name & Email can\'t empty');
                    } else {
                        $resp = $this->MyModel->hospital_update_data($id, $params);
                    }
                    json_output($respStatus, $resp);
                }
            }
        }
    }

    public function update_password($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $options = array('cost' => 12);
        if ($method != 'PUT' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE) {
            json_output(400, array('status' => 400, 'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $params = json_decode(file_get_contents('php://input'), TRUE);
                    $params['last_modified'] = date('Y-m-d H:i:s');
                    if ($params['password'] == "") {
                        $respStatus = 400;
                        $resp = array('status' => 400, 'message' => 'Password can\'t empty');
                    } else {
                        $password_hash = password_hash($params['password'], PASSWORD_BCRYPT, $options);
                        $params['password'] = $password_hash;
                        $resp = $this->MyModel->hospital_update_data($id, $params);
                    }
                    json_output($respStatus, $resp);
                }
            }
        }
    }

    public function updateHospitalPassword($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $options = array('cost' => 12);
        if ($method != 'PUT') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.12'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $params = json_decode(file_get_contents('php://input'), TRUE);
                    $params['last_modified'] = date('Y-m-d H:i:s');
                    if ($params['password'] == "") {
                        $respStatus = 400;
                        $resp = array('status' => 400, 'message' => 'Password can\'t empty  pass: ' . $params['password']);
                    } else {
                        $password_hash = password_hash($params['password'], PASSWORD_BCRYPT, $options);
//                        $params['password'] = $password_hash;  // because while creating and login password in not encrypted
                        $params['password'] = $params['password'];
                        $resp = $this->MyModel->hospital_update_data($id, $params);
                    }
                    json_output($respStatus, $resp);

                }
            }
        }
    }

    public function getDataForRequestPage($hospitalId)
    {
//        $resp = $this->MyModel->service_hospital_all();

        $hospitalData = $this->MyModel->hospital_detail_data($hospitalId);
        $hcp_code = $hospitalData->hcp_code;

        $capitationList = $this->MyModel->getHospitalCapitation($hcp_code);
        $hospitalDependentList = $this->MyModel->getHospitalDependentList($hospitalId);
        $servicesList = $this->MyModel->getHospitalServices($hospitalId);
        $drugList = $this->MyModel->getHospitalDrugs($hospitalId);
        $clientList = $this->MyModel->getHospitalClients($hospitalId);

        $response = array(
            "clientList" => $clientList,
            "hospitalDependentList" => $hospitalDependentList,
            "capitationList" => $capitationList,
            "drugList" => $drugList,
            "serviceList" => $servicesList
        );

        json_output(200, $response);
    }

    public function addHospitalRequest($hospitalId)
    {
        header('Content-type: application/json');


        $data = json_decode(file_get_contents('php://input'), true);
        $drugs = json_decode($data['drugs'], true);
        $services = json_decode($data['services'], true);
        $clientId = $data['clientId'];
        //--------------- Check constraint locally --------------//
        $requestData = array(
            "diagnose_hospital_id" => $hospitalId,
            "diagnose_client_id" => $data['clientId'],
            "diagnose_diagnose" => $data['diagnose'],
            "diagnose_procedure" => $data['procedure'],
            "diagnose_medical" => $data['medical'],
            "diagnose_investigation" => $data['investigation'],
            "diagnose_total_sum" => $data['totalSum'],
            "diagnose_date" => $data['investigationDate'],
            "diagnose_date_of_birth" => $data['dateOfBirth'],
            "is_capitation" => $data['capitationStatus'],
            "diagnose_user_type" => $data['diagnose_user_type'],
        );

        $insertRequest = $this->MyModel->hospital_request_create_data($requestData);
        if ($insertRequest['status'] == 200) {
            // ------ Insert drugs ------
            $insertDrugs = $this->MyModel->insertRequestDrugs($insertRequest['id'], $drugs);
            if ($insertDrugs['status'] == 200) {
                //----------- Insert services --------//
                $insertServices = $this->MyModel->insertRequestServices($insertRequest['id'], $services);
                json_output(200, $insertServices);
            } else {
                json_output(200, $insertDrugs);
            }

        } else {
            json_output(200, $insertRequest);

        }
    }

    public function updateHospitalRequest($hospitalId)
    {
        header('Content-type: application/json');


        $data = json_decode(file_get_contents('php://input'), true);
        $drugs = json_decode($data['drugs'], true);
        $services = json_decode($data['services'], true);
        $requestId = $data['requestId'];
        //--------------- Check constraint locally --------------//
        $requestData = array(
            "diagnose_hospital_id" => $hospitalId,
            "diagnose_client_id" => $data['clientId'],
            "diagnose_diagnose" => $data['diagnose'],
            "diagnose_procedure" => $data['procedure'],
            "diagnose_medical" => $data['medical'],
            "diagnose_investigation" => $data['investigation'],
            "diagnose_total_sum" => $data['totalSum'],
            "diagnose_date" => $data['investigationDate'],
            "diagnose_date_of_birth" => $data['dateOfBirth'],
            "is_capitation" => $data['capitationStatus'],
            "diagnose_user_type" => $data['diagnose_user_type']
        );

        //-------- First delete all drugs and services of this request


        $updateRequest = $this->MyModel->updateHospitalRequestData($requestId, $requestData);
        if ($updateRequest['status'] == 200) {
            //--------- DELETE DRUGS ---------

            $deleteDrugs = $this->MyModel->deleteRequestDrugs($requestId);
            if ($deleteDrugs['status'] == 200) {
                $deleteServices = $this->MyModel->deleteRequestServices($requestId);
                if ($deleteServices['status'] == 200) {
                    // ------ Insert drugs ------
                    $insertDrugs = $this->MyModel->insertRequestDrugs($requestId, $drugs);
                    if ($insertDrugs['status'] == 200) {
                        //----------- Insert services --------//
                        $insertServices = $this->MyModel->insertRequestServices($requestId, $services);
                        json_output(200, $insertServices);
                    } else {
                        json_output(200, $insertDrugs);
                    }
                } else {
                    json_output(200, "Delete Services failed");

                }
            } else {
                json_output(200, "Delete Drugs failed");

            }
        } else {
            json_output(200, "Update request failed");

        }
    }


    function getHospitalTotalRequest($hospitalId)
    {
        $resp = $this->MyModel->getHospitalTotalRequest($hospitalId);
        $responseData = array(
            "requestList" => $resp,
        );

        json_output(200, $responseData);
    }

    function getHospitalAuthBills($hospitalId)
    {
        $clientBills = $this->MyModel->getHospitalAuthBills($hospitalId);

        json_output(200, array(
            "clientBills"=>$clientBills,
        ));
    }

    function getHospitalApprovedBills($hospitalId)
    {
        $clientBills = $this->MyModel->getHospitalApprovedBills($hospitalId);

        json_output(200, array(
            "clientBills"=>$clientBills,
        ));
    }

    function getClientDiagnoseServiceAndDrugs($diagnoseId)
    {
        $services = $this->MyModel->getClientDiagnoseService($diagnoseId);
        $drugs = $this->MyModel->getClientDiagnoseDrugs($diagnoseId);

        $resp = array(
            "services" => $services,
            "drugs" => $drugs
        );

        json_output(200, $resp);
    }


    public function total_client_dependant($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(5) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->count_client_dependant_by_hospital($id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function total_authorization_hospital($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(5) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->count_authorization_dependant_by_hospital($id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function create_new_tickets($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $params1 = json_decode(file_get_contents('php://input'), TRUE);
                    $params['created_at'] = date('Y-m-d H:i:s');
                    $params['ticket_code'] = $this->MyModel->generate_random_string();
                    $params['is_hc'] = $id;
                    if ($params1['subject'] = "" || $params1['description'] == "") {
                        $respStatus = 400;
                        $resp = array('status' => 400, 'message' => 'Subject or Description can\'t empty');
                    } else {
                        $params['subject'] = $params1['subject_'];
                        $params['description'] = $params1['description'];
                        $params['ticket_status'] = 1;
                        $resp = $this->MyModel->ticket_support($params);
                    }
                    json_output($respStatus, $resp);
                }
            }
        }
    }

    public function get_enrolle_client_family($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(4) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->get_enrolle_hoospital($id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function get_service_hospital()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->service_hospital_all();
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function get_service_drugs_hospital($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(4) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->drugs_hospital_all($id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function update_tickets($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'PUT') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $params1 = json_decode(file_get_contents('php://input'), TRUE);
                    $params['created_at'] = date('Y-m-d H:i:s');
                    if ($params1['subject'] = "" || $params1['description'] == "") {
                        $respStatus = 400;
                        $resp = array('status' => 400, 'message' => 'Subject or Description can\'t empty');
                    } else {
                        $params['subject'] = $params1['subject_'];
                        $params['description'] = $params1['description'];
                        $resp = $this->MyModel->ticket_update_data($id, $params);
                    }
                    json_output($respStatus, $resp);
                }
            }
        }
    }

    public function get_tickets($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
////					if ($response['status'] == 200) {
                    $resp = $this->MyModel->get_xin_support_ticket_hospital($id);
                    json_output(500, $resp);
                }
            }
        }
//		}
    }

    public function get_location()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    if ($response['status'] == 200) {
                        $resp = $this->MyModel->get_all_location();
                        json_output($response['status'], $resp);
                    }
                }
            }
        }
    }

    function getHospitalDashboardData($hospitalId)
    {
        $hospitalEncounter = $this->MyModel->getTotalNoOfHospitalRequest($hospitalId);
        $hospitalComplaint = $this->MyModel->getTotalNoOfHospitalComplaint($hospitalId);
        $hospitalClient = $this->MyModel->getTotalNoOfHospitalEnrollee($hospitalId);
        $dependentList = $this->MyModel->getTotalNoOfHospitalDependents($hospitalId);

        $hospitalData = $this->MyModel->hospital_detail_data($hospitalId);
        $hcp_code = $hospitalData->hcp_code;
        $capitationCount = $this->MyModel->getHospitalCapitationCount($hcp_code);

        $mResponse = array(
            "hospitalRequest" => $hospitalEncounter,
            "hospitalComplaint" => $hospitalComplaint,
            "hospitalClient" => $hospitalClient + $capitationCount+$dependentList
        );

        json_output(200, $mResponse);
    }


    function updateRequestStatus($diagnoseId)
    {
        $data = array(
            'diagnose_status' => 1
        );
        $resp = $this->MyModel->updateRequestStatus($diagnoseId, $data);
        json_output($resp['status'], $resp);
    }


    function updateBillStatus($diagnoseId)
    {
        $data = array(
            'diagnose_bill_status' => 1
        );
        $resp = $this->MyModel->updateBillStatus($diagnoseId, $data);
        json_output($resp['status'], $resp);
    }


}

