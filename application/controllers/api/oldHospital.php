<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Hospital extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api/MyModel');
    }

    public function index()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET'){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                if($response['status'] == 200){
                    $resp = $this->MyModel->hospital_all();
                    json_output($response['status'],$resp);
                }
            }
        }
    }

    public function detail($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET' || $this->uri->segment(4) == ''){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                if($response['status'] == 200){
                    $resp = $this->MyModel->hospital_detail_data($id);
                    json_output($response['status'],$resp);
                }
            }
        }
    }

    public function create_request()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if($response['status'] == 200){
                    $params = json_decode(file_get_contents('php://input'), TRUE);
                    $params['diagnose_status'] = 0; // for get request status
                    if ($params['diagnose_hospital_id'] == "" || $params['diagnose_client_id'] == "" || $params['diagnose_diagnose'] == ""
                            || $params['diagnose_procedure'] == ""|| $params['diagnose_investigation'] == ""|| $params['diagnose_medical'] == ""
                            || $params['diagnose_total_sum'] == "" ) {
                        $respStatus = 400;
                        $resp = array('status' => 400,'message' =>  'diagnose can\'t empty');
                    } else {
                        $resp = $this->MyModel->hospital_request_create_data($params);
                    }
                    json_output($respStatus,$resp);
                }
            }
        }
    }
...
	public function update_request()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if($check_auth_client == true){
				$response = $this->MyModel->auth();
				$respStatus = $response['status'];
				if($response['status'] == 200){
					$params = json_decode(file_get_contents('php://input'), TRUE);
					$params['diagnose_status'] = 0; // for get request status
					if ($params['diagnose_hospital_id'] == "" || $params['diagnose_client_id'] == "" || $params['diagnose_diagnose'] == ""
						|| $params['diagnose_procedure'] == ""|| $params['diagnose_investigation'] == ""|| $params['diagnose_medical'] == ""
						|| $params['diagnose_total_sum'] == "" ) {
						$respStatus = 400;
						$resp = array('status' => 400,'message' =>  'diagnose can\'t empty');
					} else {
						$resp = $this->MyModel->hospital_request_create_data($params);
					}
					json_output($respStatus,$resp);
				}
			}
		}
	}

    public function detail_request($id) {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET' || $this->uri->segment(4) == ''){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                if($response['status'] == 200){
                    $resp = $this->MyModel->hospital_request_client_get_data($id);
                    json_output($response['status'],$resp);
                }
            }
        }
    }

    public function total_bill($id) {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET'){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                if($response['status'] == 200){
                    $resp = $this->MyModel->get_bill_status($id);
                    json_output($response['status'],$resp);
                }
            }
        }
    }

    public function update($id) {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'PUT' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if($response['status'] == 200){
                    $params = json_decode(file_get_contents('php://input'), TRUE);
                    $params['last_modified'] = date('Y-m-d H:i:s');
                    if ($params['name'] == "" || $params['email'] == "" ) {
                        $respStatus = 400;
                        $resp = array('status' => 400,'message' =>  'Name & Email can\'t empty');
                    } else {
                        $resp = $this->MyModel->hospital_update_data($id,$params);
                    }
                    json_output($respStatus,$resp);
                }
            }
        }
    }

    public function update_password($id) {
        $method = $_SERVER['REQUEST_METHOD'];
        $options = array('cost' => 12);
        if($method != 'PUT' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if($response['status'] == 200){
                    $params = json_decode(file_get_contents('php://input'), TRUE);
                    $params['last_modified'] = date('Y-m-d H:i:s');
                    if ($params['password'] == "" ) {
                        $respStatus = 400;
                        $resp = array('status' => 400,'message' =>  'Password can\'t empty');
                    } else {
                        $password_hash = password_hash($params['password'], PASSWORD_BCRYPT, $options);
                        $params['password'] = $password_hash;
                        $resp = $this->MyModel->hospital_update_data($id,$params);
                    }
                    json_output($respStatus,$resp);
                }
            }
        }
    }
    
    public function total_client_dependant($id) {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET' || $this->uri->segment(5) == '' ){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                if($response['status'] == 200){
                    $resp = $this->MyModel->count_client_dependant_by_hospital($id);
                    json_output($response['status'],$resp);
                }
            }
        }
    }
    
    public function total_authorization_hospital($id) {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET' || $this->uri->segment(5) == '' ){
            json_output(400,array('status' => 400,'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if($check_auth_client == true){
                $response = $this->MyModel->auth();
                if($response['status'] == 200){
                    $resp = $this->MyModel->count_authorization_dependant_by_hospital($id);
                    json_output($response['status'],$resp);
                }
            }
        }
    }

	public function create_new_tickets($id) {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'POST') {
			json_output(400, array('status' => 400, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->MyModel->auth();
				$respStatus = $response['status'];
				if ($response['status'] == 200) {
					$params = json_decode(file_get_contents('php://input'), TRUE);
					$params['created_at'] = date('Y-m-d H:i:s');
					$params['ticket_code'] = $this->MyModel->generate_random_string();
					$params['company_id'] == $id;
					if ($params['ticket_priority'] == "" || $params['description'] == "" ) {
						$respStatus = 400;
						$resp = array('status' => 400, 'message' => 'Priority or Description can\'t empty');
					} else {
						$ticket_id = $params['ticket_priority'];
						if ($ticket_id < 1 || $ticket_id > 4 ) {
							$resp = array('status' => 400, 'message' => 'Priority not found');
						}else {
							$resp = $this->MyModel->ticket_support($params);
						}
					}
					json_output($respStatus, $resp);
				}
			}
		}
	}

	public function get_enrolle_client_family($id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET' || $this->uri->segment(4) == '') {
			json_output(400, array('status' => 400, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->MyModel->auth();
				if ($response['status'] == 200) {
					$resp = $this->MyModel->get_enrolle_hoospital($id);
					json_output($response['status'], $resp);
				}
			}
		}
	}

	public function get_service_hospital()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 400, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->MyModel->auth();
				if ($response['status'] == 200) {
					$resp = $this->MyModel->service_hospital_all();
					json_output($response['status'], $resp);
				}
			}
		}
	}

	public function get_service_drugs_hospital($id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET' || $this->uri->segment(4) == '') {
			json_output(400, array('status' => 400, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->MyModel->auth();
				if ($response['status'] == 200) {
					$resp = $this->MyModel->drugs_hospital_all($id);
					json_output($response['status'], $resp);
				}
			}
		}
	}
}
