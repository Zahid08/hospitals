<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Client
 *
 * @author root
 */
class Client extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api/MyModel');
    }

    public function index()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->client_all();
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function detail($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->client_detail_data($id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function client_familly($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->client_fammily_detail_data($id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function client_dependant($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->client_dependant_data($id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function updateClientData($clientId)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $filename = "";
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {

            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {

                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {


                    $firstName = $this->input->post("name");
                    $lastName = $this->input->post("last_name");
                    $otherName = $this->input->post("other_name");
                    $email = $this->input->post("email");
                    $contact_number = $this->input->post("contact_number");
                    $org_id = $this->input->post("org_id");
                    $residentAddress = $this->input->post("address_1");
                    $hospitalId = $this->input->post("hospital_id");
                    $stateId = $this->input->post("state");
                    $sex = $this->input->post("sex");
                    $dob = $this->input->post("dob");
                    $maritalStatus = $this->input->post("marital_status");
                    $diseases = $this->input->post("diseases");
                    $diseasesComments = $this->input->post("disease_comment");

                    $message="Data received from url";

                    if ($firstName == "" || $lastName == "" || $email == "" || $contact_number == ""
                        || $hospitalId == "" || $stateId == "" || $sex == "" ||
                        $dob == "" || $maritalStatus == ""
                    ) {
                        $resp = array('status' => 400, 'message' => 'Please provide complete data');
                    } else {
                        if (!file_exists($_FILES['client_profile']['tmp_name']) || !is_uploaded_file($_FILES['client_profile']['tmp_name'])) {
                            $filename = "";
                        } else {
                            $message="file exists";
                            $image = $_FILES["client_profile"]["name"];

                            $ext = pathinfo($image, PATHINFO_EXTENSION);

                            $image_old = $_FILES["client_profile"]["tmp_name"];

                            $filename = 'client_photo_' . round(microtime(true)) . '.' . $ext;

                            //rename file name with random number
                            $path = "uploads/clients/";

                            //image uploading folder path
                            move_uploaded_file($image_old, $path . $filename);
                        }

                        $data = array(
                            'name' => $firstName,
                            'last_name' => $lastName,
                            'other_name' => $otherName,
                            'email' => $email,
                            'contact_number' => $contact_number,
                            'company_name' => $org_id,
                            'dob' => $dob,
                            'hospital_id' => $hospitalId,
                            'sex' => $sex,
                            'state' => $stateId,
                            'marital_status' => $maritalStatus,
                            'diseases' => $diseases,
                            'address_1' => $residentAddress,
                            'disease_comment' => $diseasesComments,
                            'client_profile' => $filename,
                            'last_modified' => date('Y-m-d H:i:s'),
                        );
                        $resp = $this->MyModel->updateClientData($clientId, $data);
                    }
                    $mResponse = array(
                        "response" => $resp,
                        "profileImage" => $filename,
                        "date" => $dob,
                    );
                    json_output($respStatus, $mResponse);

                }
            }
        }

    }


    public function update_dependant($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST' || $this->uri->segment(5) == '' || is_numeric($this->uri->segment(5)) == FALSE) {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $name = $this->input->post("name");
                    $contact_number = $this->input->post("contact_number");
                    $relation = $this->input->post("relation");
                    $location = $this->input->post("location");
                    $dob = $this->input->post("dob");
                    $hospital_id = $this->input->post("hospital_id");
                    $last_name = $this->input->post("last_name");
                    $sex = $this->input->post("sex");
                    $other_name = $this->input->post("other_name");
                    $diseases_data = '';
                    if (!empty($this->input->post('diseases'))) {
                        $diseases_data = implode(',', $this->input->post('diseases'));
                    }
                    $comment = '';
                    if (isset($_POST['comment'])) {
                        $comment = $this->input->post('comment');
                    }

                    $from = new DateTime($dob);
                    $to = new DateTime('today');
                    $age = $from->diff($to)->y;
                    if (($age == 21 or $age > 21) and $relation != 'wife') {
                        $respStatus = 400;
                        $resp = array('status' => 400, 'message' => 'WE ARE SORRY YOU CAN NOT ADD THIS DEPENDANT AS HE/SHE HAS EXCEEDED AGE LIMITS AS A DEPENDANT KINDLY APPLY HERE TO GET AN INDIVIDUAL PLAN FOR HIM/HER');
                    } else if ($name == "" || $contact_number == "" || $relation == "" || $location == "" || $dob == "" || $hospital_id == "" ||
                        $last_name == "" || $sex == "" ) {
                        $resp = array('status' => 400, 'message' => 'Name or Hospital can\'t empty');
                    } else {
                        if (!file_exists($_FILES['client_profile']['tmp_name']) || !is_uploaded_file($_FILES['client_profile']['tmp_name'])) {
                            $filename = "";
                        } else {
                            $image = $_FILES["client_profile"]["name"];

                            $ext = pathinfo($image, PATHINFO_EXTENSION);

                            $image_old = $_FILES["client_profile"]["tmp_name"];

                            $filename = 'client_photo_' . round(microtime(true)) . '.' . $ext;

                            //rename file name with random number
                            $path = "uploads/clients/" . $filename;

                            //image uploading folder path
                            move_uploaded_file($image_old, $path . $filename);
                        }
                        // image is bind and upload to respective folde
                        $data = array(
                            'name' => $name,
                            'contact_number' => $contact_number,
                            'relation' => $relation,
                            'location' => $location,
                            'dob' => $dob,
                            'hospital_id' => $hospital_id,
                            'last_name' => $last_name,
                            'sex' => $sex,
                            'other_name' => $other_name,
                            'diseases' => $diseases_data,
                            'disease_comment' => $comment,
                            'client_profile' => $filename,
                            'created_on' => date('Y-m-d H:i:s'),
                        );

                        $resp = $this->MyModel->client_dependant_update_data($id, $data);
                    }
                    json_output($respStatus, $resp);
                }
            }
        }
    }

    public function new_dependant($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $name = $this->input->post("name");
                    $contact_number = $this->input->post("contact_number");
                    $relation = $this->input->post("relation");
                    $location = $this->input->post("location");
                    $dob = $this->input->post("dob");
                    $hospital_id = $this->input->post("hospital_id");
                    $last_name = $this->input->post("last_name");
                    $sex = $this->input->post("sex");
                    $other_name = $this->input->post("other_name");
                    $diseases_data = '';
                    if (!empty($this->input->post('diseases'))) {
                        $diseases_data = implode(',', $this->input->post('diseases'));
                    }
                    $comment = '';
                    if (isset($_POST['comment'])) {
                        $comment = $this->input->post('comment');
                    }

                    $from = new DateTime($dob);
                    $to = new DateTime('today');
                    $age = $from->diff($to)->y;
                    if (($age == 21 or $age > 21) and $relation != 'wife') {
                        $respStatus = 400;
                        $resp = array('status' => 400, 'message' => 'WE ARE SORRY YOU CAN NOT ADD THIS DEPENDANT AS HE/SHE HAS EXCEEDED AGE LIMITS AS A DEPENDANT KINDLY APPLY HERE TO GET AN INDIVIDUAL PLAN FOR HIM/HER');
                    } else if ($name == "" || $contact_number == "" || $relation == "" || $location == "" || $dob == "" || $hospital_id == "" ||
                        $last_name == "" || $sex == "") {
                        $resp = array('status' => 400, 'message' => 'Name or Hospital can\'t empty');
                    } else {
                        $image = $_FILES["client_profile"]["name"];

                        $ext = pathinfo($image, PATHINFO_EXTENSION);

                        $image_old = $_FILES["client_profile"]["tmp_name"];

                        $filename = 'client_photo_' . round(microtime(true)) . '.' . $ext;

                        //rename file name with random number
                        $path = "uploads/clients/" . $filename;

                        //image uploading folder path
                        move_uploaded_file($image_old, $path . $filename);

                        $data = array(
                            'name' => $name,
                            'contact_number' => $contact_number,
                            'relation' => $relation,
                            'location' => $location,
                            'dob' => $dob,
                            'hospital_id' => $hospital_id,
                            'last_name' => $last_name,
                            'sex' => $sex,
                            'other_name' => $other_name,
                            'client_profile' => $filename,
                            'created_on' => date('Y-m-d H:i:s'),
                            'client_id' => $id,
//							'diseases'        => $diseases_data,
//							'disease_comment' => $comment,
                        );
                        $resp = $this->MyModel->client_dependant_new_data($data);
                    }
                    json_output($respStatus, $resp);
                }
            }
        }
    }


    public function update_password($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $options = array('cost' => 12);
        if ($method != 'PUT' || $this->uri->segment(4) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $params = json_decode(file_get_contents('php://input'), TRUE);
                    $params['last_modified'] = date('Y-m-d H:i:s');
                    if ($params['client_password'] == "") {
                        $respStatus = 400;
                        $resp = array('status' => 400, 'message' => 'Password can\'t empty');
                    } else {
                        $password_hash = password_hash($params['client_password'], PASSWORD_BCRYPT, $options);
                        $params['client_password'] = $password_hash;
                        $resp = $this->MyModel->client_dependant_update_data($id, $params);
                    }
                    json_output($respStatus, $resp);
                }
            }
        }
    }


    public function request_change_hospital()
    {

        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $params = json_decode(file_get_contents('php://input'), TRUE);
                    $params['created_on'] = date('Y-m-d H:i:s');
                    if ($params['client_id'] == "" || $params['hospital_id'] == "") {
                        $respStatus = 400;
                        $resp = array('status' => 400, 'message' => 'Client and Hospital can\'t empty');
                    } else {
                        $resp = $this->MyModel->client_request_change_hospital($params);
                    }
                    json_output($respStatus, $resp);
                }
            }
        }
    }

    /**
     * use for dashboard api
     * get total dependant
     * @param $client_id
     */
    public function total_dependant($client_id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(4) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->client_fammily_detail_data($client_id);
                    if (empty($resp)) {
                        json_output(400, array('status' => 400, 'message' => 'Bad request.'));
                    } else {
                        json_output($respStatus, $resp);
                    }
                }
            }
        }
    }

    /**
     * use for dashboard api
     * get total total hospital
     * @param $client_id
     */
    public function total_hospital($client_id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(4) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                $total_hospital = 0;
                if ($response['status'] == 200) {
                    $clientinfo = $this->MyModel->client_detail_data($client_id);
                    if (empty($clientinfo)) {
                        json_output(201, array('status' => 201, 'message' => 'Data Missing.'));
                    } else {
                        if (isset($clientinfo->subscription_ids) and !empty($clientinfo->subscription_ids)) {
                            $subs = $this->MyModel->get_xin_subscription($clientinfo->subscription_ids);
                            if (isset($subs->band_types) AND !empty($subs->band_types)) {
                                $subs_pkg = $subs->plan_name;
                                $bands = explode(',', $subs->band_types);
                                foreach ($bands as $key2 => $band) {
                                    $hos_data = $this->MyModel->hospital_detail_data($band);
                                    if (isset($hos_data->hospital_name)) {
                                        $total_hospital++;
                                    }
                                }
                            }
                        }
                        if ($total_hospital == 0) {
                            json_output(201, array('status' => 201, 'Total Hospital' => $total_hospital));
                        } else {
                            json_output(200, array('status' => 200, 'Total Hospital' => $total_hospital));
                        }
                    }
                }
            }
        }
    }

    /**
     * use for dashboard api
     * get total request
     * @param $client_id
     */
    public function total_hospital_request($client_id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(6) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->count_client_request_hospital($client_id);
                    json_output($respStatus, $resp);
                }
            }
        }
    }

    /**
     * use for dashboard api
     * get total dependent request
     * @param $client_id
     */
    public function total_dependant_request($client_id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(6) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->count_client_familly_request_hospital($client_id);
                    json_output($respStatus, $resp);
                }
            }
        }
    }

    /**
     * use for dashboard api
     * get list hospital provider
     * @param $client_id
     */
    public function list_hospital($client_id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(4) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                $hos_data = "";
                if ($response['status'] == 200) {
                    $clientinfo = $this->MyModel->hospital_band_detail_data($client_id);
                    json_output(200, $clientinfo);
                }
            }
        }
    }

    public function get_enrolle_client_family($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(4) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->get_enrolle($id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function upload_img()
    {
        $image = base64_decode($this->input->post("img_front"));
        $image_name = md5(uniqid(rand(), true));
        $filename = $image_name . '.' . 'png';
        //rename file name with random number
        $path = "uploads/clients/" . $filename;
        //image uploading folder path
        file_put_contents($path . $filename, $image);
        // image is bind and upload to respective folde

        $data_insert = array('front_img' => $filename);

        $success = $this->add_model->insert_img($data_insert);
        if ($success) {
            $b = "User Registered Successfully..";
        } else {
            $b = "Some Error Occured. Please Try Again..";
        }
        echo json_encode($b);
    }

    public function create_new_tickets($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $params1 = json_decode(file_get_contents('php://input'), TRUE);
                    $params['created_at'] = date('Y-m-d H:i:s');
                    $params['ticket_code'] = $this->MyModel->generate_random_string();
                    $params['employee_id'] = $id;
                    if ($params1['subject'] = "" || $params1['description'] == "") {
                        $respStatus = 400;
                        $resp = array('status' => 400, 'message' => 'Subject or Description can\'t empty');
                    } else {
                        $params['subject'] = $params1['subject_'];
                        $params['description'] = $params1['description'];
                        $params['ticket_status'] = 1;
                        $resp = $this->MyModel->ticket_support($params);
                    }
                    json_output($respStatus, $resp);
                }
            }
        }
    }

    public function get_xin_subscription_detail($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(4) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->get_list_xin_subscription_detail($id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function get_xin_organizations($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(4) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $resp = $this->MyModel->get_xin_organizions($id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function update_xin_organization($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $options = array('cost' => 12);
        if ($method != 'PUT' || $this->uri->segment(5) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $params = json_decode(file_get_contents('php://input'), TRUE);
                    $params['last_modified'] = date('Y-m-d H:i:s');
                    if ($params['name'] == "" || $params['location_id'] == "" || $params['location_id'] == "" || $params['contact_person'] == ""
                        || $params['rc_number'] == "" || $params['type_business'] == "" || $params['comm_date'] == "") {
                        $respStatus = 400;
                        $resp = array('status' => 400, 'message' => 'Data organization can\'t empty');
                    } else {
                        $resp = $this->MyModel->organization_update_data($id, $params);
                    }
                    json_output($respStatus, $resp);
                }
            }
        }
    }

    public function update_tickets($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'PUT') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $params1 = json_decode(file_get_contents('php://input'), TRUE);
                    $params['created_at'] = date('Y-m-d H:i:s');
                    if ($params1['subject'] = "" || $params1['description'] == "") {
                        $respStatus = 400;
                        $resp = array('status' => 400, 'message' => 'Subject or Description can\'t empty');
                    } else {
                        $params['subject'] = $params1['subject_'];
                        $params['description'] = $params1['description'];
                        $resp = $this->MyModel->ticket_update_data($id, $params);
                    }
                    json_output($respStatus, $resp);
                }
            }
        }
    }

    public function get_tickets($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    if ($response['status'] == 200) {
                        $resp = $this->MyModel->get_xin_support_ticket($id);
                        json_output($response['status'], $resp);
                    }
                }
            }
        }
    }

    public function get_location()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    if ($response['status'] == 200) {
                        $resp = $this->MyModel->get_all_location();
                        json_output($response['status'], $resp);
                    }
                }
            }
        }
    }

    public function update_client($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {

                    $name = $this->input->post('name');
                    $email_ = $this->input->post('email');
                    $contact_number = $this->input->post('contact_number');
                    $sex = $this->input->post('sex');
                    $address_1 = $this->input->post('address_1');
                    $marital_status = $this->input->post('marital_status');
                    $state = $this->input->post('location');
                    $dob = $this->input->post('dob');
                    $last_name = $this->input->post('last_name');
                    $other_name = $this->input->post('other_name');
                    $hospital_id = $this->input->post('hospital_id');

                    $diseases_data = '';
                    if (!empty($this->input->post('diseases'))) {
                        $diseases_data = $this->input->post('diseases');
                    }
                    $comment = '';
                    if (isset($_POST['comment'])) {
                        $comment = $this->input->post('comment');
                    }
                    if (!file_exists($_FILES['client_profile']['tmp_name']) || !is_uploaded_file($_FILES['client_profile']['tmp_name'])) {
                        $filename = "";
                    } else {
                        $image = $_FILES["client_profile"]["name"];

                        $ext = pathinfo($image, PATHINFO_EXTENSION);

                        $image_old = $_FILES["client_profile"]["tmp_name"];

                        $filename = 'client_photo_' . round(microtime(true)) . '.' . $ext;

                        //rename file name with random number
                        $path = "uploads/clients/" . $filename;

                        //image uploading folder path
                        move_uploaded_file($image_old, $path . $filename);
                    }

                    $data = array(
                        'client_profile' => $filename,
                        'name' => $name,
                        'email' => $email_,
                        'contact_number' => $contact_number,
                        'sex' => $sex,
                        'address_1' => $address_1,
                        'marital_status' => $marital_status,
                        'state' => $state,
                        'dob' => $dob,
                        'last_name' => $last_name,
                        'other_name' => $other_name,
                        'hospital_id' => $hospital_id,
                        'diseases' => $diseases_data,
                        'disease_comment' => $comment,
                    );
                    $resp = $this->MyModel->client_update_data($id, $data);
//					}
                    json_output($respStatus, $resp);
                }
            }
        }
    }


    public function getAllSubscription($subscriptionId)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {
                    $subscriptionList = $this->MyModel->getAllSubscription();
                    $clientSubscription = $this->MyModel->getClientSubscription($subscriptionId);

                    $resp = array(
                        "subsList" => $subscriptionList,
                        "subsClient" => $clientSubscription
                    );

                    json_output(200, $resp);
                }
            }
        }
    }

    public function getNoForClientDashboard($clientId)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->MyModel->auth();
                if ($response['status'] == 200) {

                    $dependentCount = $this->MyModel->getClientDependentCount($clientId);
                    $clientComplaintCount = $this->MyModel->getClientComplaintCount($clientId);
                    $subscriptionCount = $this->MyModel->getTotalSubscription();
                    $changeHospitalRequest = $this->MyModel->getTotalChangeHospitalRequest($clientId);

                    $clientDetail = $this->MyModel->client_detail_data($clientId);
                    $subscription = $clientDetail->subscription_ids;

                    $getClientBands = $this->MyModel->getSubscriptionBands($subscription);
                    $bands = $getClientBands[0]->band_types;
                    $bands_split = explode(",", $bands);
                    $resultList = array();

                    $hospitalCount = 0;
                    foreach ($bands_split as $band) {
                        $cou = $this->MyModel->getBandHospitalCount($band);
                        $hospitalCount = $hospitalCount + $cou[0]->hospitalCount;
                    }


                    $mResponse = array(
                        "hospitalCount" => $hospitalCount,
                        "dependentCount" => $dependentCount,
                        "complaintCount" => $clientComplaintCount,
                        "subscriptionCount" => $subscriptionCount,
                        "changeHospitalRequest" => $changeHospitalRequest
                    );

                    json_output(200, $mResponse);
                }
            }
        }
    }

//    public function getClientHospitalList($client_id)
//    {
//        $method = $_SERVER['REQUEST_METHOD'];
//        if ($method != 'GET' || $this->uri->segment(4) == '') {
//            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
//        } else {
//            $check_auth_client = $this->MyModel->check_auth_client();
//            if ($check_auth_client == true) {
//                $response = $this->MyModel->auth();
//                $respStatus = $response['status'];
//                $hos_data = "";
//                if ($response['status'] == 200) {
//                    $clientinfo = $this->MyModel->get_xin_list_provider($client_id);
//                    json_output(200, $clientinfo);
//                }
//            }
//        }
//    }

    function getElectronicId($clientID)
    {
//        $method = $_SERVER['REQUEST_METHOD'];
//        if ($method != 'GET' || $this->uri->segment(4) == '') {
//            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
//        } else {
//            $check_auth_client = $this->MyModel->check_auth_client();
//            if ($check_auth_client == true) {
//                $response = $this->MyModel->auth();
//                $respStatus = $response['status'];
//                $hos_data = "";
//                if ($response['status'] == 200) {
        $clientinfo = $this->MyModel->getElectronicsId($clientID);
        $obeject = $clientinfo[0];
        $resp = $this->MyModel->hospital_detail_data($obeject->hospital_id);
        $clientDetail = $this->MyModel->client_detail_data($clientID);
        $stateId = $clientDetail->state;
        $state = "";

        $hospitalName="";

        if($resp!=null){
            $hospitalName=$resp->hospital_name;
        }

        if ($stateId == "") {
            $state = "";
        } else {
            $stateResponse = $this->MyModel->getClientState($stateId);
            $state = $stateResponse[0]->location_name;
//        $state=$stateResponse->location_name;
        }

        $responseData = array(
            "hospital_name" => $hospitalName,
            "client_id" => $obeject->enrollee_id,
            "state" => $state

        );


        json_output(200, $responseData);
//                }
//            }
//        }
    }


    function getClientHospitalList($clientId)
    {
        $clientDetail = $this->MyModel->client_detail_data($clientId);
        $subscription = $clientDetail->subscription_ids;

        $getClientBands = $this->MyModel->getSubscriptionBands($subscription);
        $bands = $getClientBands[0]->band_types;
        $bands_split = explode(",", $bands);
        $resultList = array();

        $count = 0;
        foreach ($bands_split as $band) {
            $cou = $this->MyModel->getSubscriptionHospitals($band);
            array_push($resultList, $cou);
        }

        $resultListHospitalList = array();

        foreach ($resultList as $result) {
            foreach ($result as $hospitalObject) {
                array_push($resultListHospitalList, $hospitalObject);
            }
        }

        $response = array(
            "count" => sizeof($resultListHospitalList),
            "hospitalList" => $resultListHospitalList,
        );

        json_output(200, $response);


    }


    function getClientHospitalDetail($hospitalId)
    {
        $response = $this->MyModel->getClientHospitalData($hospitalId);
        json_output(200, $response);

    }

//    function getSubscriptionHospitalCount($clientId)
//    {
//
//       $clientDetail = $this->MyModel->client_detail_data($clientId);
//    $subscription = $clientDetail->subscription_ids;
//
//        $getClientBands = $this->MyModel->getSubscriptionBands($subscription);
//        $bands = $getClientBands[0]->band_types;
//        $bands_split = explode(",", $bands);
//        $resultList = array();
//
//        $count = 0;
//        foreach ($bands_split as $band) {
//            $cou = $this->MyModel->getBandHospitalCount($band);
//            $count = $count + $cou[0]->hospitalCount;
//        }
//
//        $ew = array(
//            "band" => $count
//        );
//
//        json_output(200, $ew);
//
//
//    }


}

